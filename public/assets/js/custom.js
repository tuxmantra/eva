var url = location.protocol+"//"+location.hostname+"/";
$( ".usertype_config" ).change(function() {

	var utype = $(this).children("option:selected").val();

     $.post(url+'getUserConfig', { utype: utype }).done(function (data) {
          $('#configDiv').html(data);
          $('#save_config').show();
    });
});

$( ".usertype_usercreate" ).change(function() {

     var utype = $(this).children("option:selected").val();
     $.post(url+'/getUserPartner', { utype: utype }).done(function (data) {
         if(data != '0'){
            $('#partner_list').show();
            $('#partner_list_inner').html(data);
            $('#partner').selectpicker('refresh');
         }else{
           $('#partner_list').hide();
           $('#partner_list_inner').html('');
           $('#device_list').hide();
           $('#device_list_inner').html('');
         }
    });

    $.post(url+'getCategoryPartner', { utype: utype }).done(function (data) {
        if(data == '1'){
              $('#partner_device_status').val('1');
        }else{
              $('#partner_device_status').val('');
        }
    });
});

function updatePartnerDevice(){
    var partner = $(".partner option:selected").val();
     $.post(url+'getPartnerDevice', { partner: partner }).done(function (data) {
         if(data != '0'){
            $('#device_list').show();
            $('#device_list_inner').html(data);
            $('#device').selectpicker('refresh');
         }else{
            $('#device_list').hide();
            $('#device_list_inner').html('');
         }
    });
}

function goBack() {
  window.history.back();
}

function validateUser(){
/*  $('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var utype     = $(".usertype_usercreate option:selected").val();
  var name      = $('#name').val();
  var email     = $('#email').val();
  var mobile    = $('#mobile').val();
  var password  = $('#password').val();
  var address   = $('#address').val();
  
  var status_partner_device = $('#partner_device_status').val();
  if(status_partner_device == 1){
            var partner     = $(".partner option:selected").val();
            var device      = $(".device option:selected").val();
            if(partner == '' || partner == 'undefined'){
                 $('#cli_message').html("Partner Field required.").show();
                 return false;
            }else if(device == '' || device == 'undefined'){
                 $('#cli_message').html("Device Field required.").show();
                 return false;
            }
  }
  if(utype == '' || name == '' || email == '' || mobile == '' || password == '' || address == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else if(mobile.length < 5){
       $('#cli_message').html("Mobile lenght minimum 6 digits.").show();
      return false;
  }else if(validateEmail(email) != true){
      $('#cli_message').html("Email format incorrect.").show();
      return false;
  }else if(password.length < 6){
      $('#cli_message').html("Password lenght minimum 6.").show();
      return false;
  }else if(i == 1){
      $('#cli_message').html("Partner Field required.").show();
      return false;
  }else if(status_partner_device == 1){
      $('#cli_message').html("Partner Or Device Field required.").show();
      return false;
  }else{
      return true;
  }
}

function editUser(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var utype     = $(".usertype_usercreate option:selected").val();
  var name      = $('#name').val();
  var email     = $('#email').val();
  var mobile    = $('#mobile').val();
  var password  = $('#password').val();
  var address   = $('#address').val();
  
  var status_partner_device = $('#partner_device_status').val();
  if(status_partner_device == 1){
            var partner     = $(".partner option:selected").val();
            var device      = $(".device option:selected").val();
            if(partner == '' || partner == 'undefined'){
                 $('#cli_message').html("Partner Field required.").show();
                 return false;
            }else if(device == '' || device == 'undefined'){
                 $('#cli_message').html("Device Field required.").show();
                 return false;
            }
  }
  if(utype == '' || name == '' || email == '' || mobile == '' || address == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else if(mobile.length < 5){
       $('#cli_message').html("Mobile lenght minimum 6 digits.").show();
      return false;
  }else if(validateEmail(email) != true){
      $('#cli_message').html("Email format incorrect.").show();
      return false;
  }else if(password != ''){
      if(password.length < 6){
          $('#cli_message').html("Password lenght minimum 6.").show();
          return false;
      }
  }else if(i == 1){
      $('#cli_message').html("Partner Field required.").show();
      return false;
  }else if(status_partner_device == 1){
      $('#cli_message').html("Partner Or Device Field required.").show();
      return false;
  }else{
      return true;
  }
}

function editCoreData(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var title    = $('#title').val();
  var device1  = $('#device1').val();
  var device2  = $('#device2').val();
  var decimal  = $('#decimal').val();
  var unit     = $('#unit').val();
  if(title == '' || device1 == '' || device2 == '' || decimal == '' || unit == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else{
      return true;
  }
}
function editValuevalid(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var title    = $('#title').val();
  var device1  = $('#device1').val();
  var device2  = $('#device2').val();
  var decimal  = $('#decimal').val();
  var unit     = $('#unit').val();
  var type     = $('#type').val();
  if(title == '' || device1 == '' || device2 == '' || decimal == '' || unit == '' || type == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else{
      return true;
  }
}
function editDescValid(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var title     = $('#title').val();
  var sub_title = $("#subtitl").val();
  if(title == '' || sub_title == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else{
      return true;
  }
}
function validateCore(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var title    = $('#title').val();
  var device1  = $('#device1').val();
  var device2  = $('#device2').val();
  var decimal  = $('#decimal').val();
  var unit     = $('#unit').val();
  if(title == '' || device1 == '' || device2 == '' || decimal == '' || unit == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else{
      return true;
  }
}
function validateDesc(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
 
  var title      = $('#title').val();
  var sub_title  = $('#sub_titl').val();
  if(title == '' || sub_title == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else{
      return true;
  }
}
function validateValue(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var title    = $('#title').val();
  var device1  = $('#device1').val();
  var device2  = $('#device2').val();
  var decimal  = $('#decimal').val();
  var unit     = $('#unit').val();
  var type     = $('#type').val();
  if(title == '' || device1 == '' || device2 == '' || decimal == '' || unit == '' || type == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else{
      return true;
  }
}

function updateSystemSettings(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('.serve_message').hide();
  $('.cli_message').hide();

  var name      = $('#name').val();
  var email     = $('#email').val();
  var mobile    = $('#mobile').val();
  var address   = $('#address').val();
  var website   = $('#address').val();
  var pincode   = $('#pincode').val();
  var location  = $('#location').val();
  var city      = $('#city').val();
  var callist   = $('#callist').val();

  if(name == '' || email == '' || mobile == '' || address == '' || website == '' || pincode == '' || location == '' || city == '' || callist == ''){
      $('.cli_message').html("Fields required.").show();
      return false;
  }else if(mobile.length < 5){
       $('.cli_message').html("Mobile lenght minimum 6 digits.").show();
      return false;
  }else if(pincode.length <= 4){
       $('.cli_message').html("pincode lenght minimum 5 digits.").show();
      return false;
  }else if(validateEmail(email) != true){
      $('.cli_message').html("Email format incorrect.").show();
      return false;
  }else{
      return true;
  }
}

function createDevice(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  var flag = 0;
  $('#serve_message').hide();
  var device_id   = $('#device_id').val();
  var name        = $('#name').val();
  var desc        = $('#desc').val();
  var partner_id  = $('#partner_id').val();
  var type        = $('#type').val();
  var status      = $('#status').val();
  $('#cli_message').hide();
  if(device_id == '' || name == '' || desc == '' || partner_id == '' || type == '' || status == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else{
    return true;
  }
}

function validateAlert(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  var flag = 0;
  $('#serve_message').hide();
  var alert_category      = $('#alert_category').val();
  var alert_sub_category  = $('#alert_sub_category').val();
  var type                = $('#type').val();
  var desc                = $('#desc').val();
  var msg                 = $('#msg').val();
  var status              = $('#status').val();
  $('#cli_message').hide();
  if(alert_category == '' || alert_sub_category == '' || type == '' || desc == '' || msg == '' || status == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else{
    return true;
  }
}

function processVehicle(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  return true;
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function changeFrace(){
  var chkBox = document.getElementById('show');
    if (chkBox.checked)
    {
        document.getElementById('password').type = 'text';
        document.getElementById('confir').type = 'text';
    }else{
        document.getElementById('password').type = 'password';
        document.getElementById('confir').type = 'password';
    } 
}

function validPassword(){
    var password = $('#password').val();
    var confir   = $('#confir').val();
      $('#cli_message').hide();
    if(password == '' || confir == ''){
        $('#cli_message').html("Fields required.").show();
        return false;
    }else if(password.length < 6 || confir.length < 6){
        $('#cli_message').html("Password length minimum 6.").show();
        return false;
    }else if(password != confir){
        $('#cli_message').html("Password not matching.").show();
        return false;
    }else{
        $('#cli_message').html("Wait.").show();
        return true;
    }
}

function doSomething(){
  $('#success').hide();
	var val = [];
    $(':checkbox:checked').each(function(i){
        val[i] = $(this).val();
    });
    var cal = [];
    $("input:checkbox:not(:checked)").each(function (i) {
            cal[i] = $(this).val();
    });

    var utype = $(".usertype_config option:selected").val();
    $.ajax({
	    type:'POST',
	    url: url+'update-config-usertype',
	    data: { 'check': val, 'uncheck': cal ,'utype': utype },
	    success: function(data){
        /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
	      $('#success').show();
	    }
    });
}

$( ".createconfig" ).change(function() {

    var val = $(this).find('option:selected').text();
    var label=$('#new_config :selected').parent().attr('label');
	$('#hiddenconfig').show();
    $('#hiddenlabel').show();
    $('#hiddenconfig').val(val);
    $('#hiddenlabel').val(label);

});

function createconfig(){
  $('#cli_message').hide();
  $('#newconfig').hide();
  var val = $('#hiddenconfig').val();
  var lab = $('#hiddenlabel').val();
  if(val != ''){
        $.ajax({
          type:'POST',
          url: url+'create-config',
          data: { 'val': val, 'lab': lab },
          success: function(data){
              $('#newconfig').show();
              $('#output').show();
              $('#create').val(data);
          }
        });
  }else{
    $('#cli_message').show();
  }

}

function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(120);
                };

                reader.readAsDataURL(input.files[0]);
            }
}


$( ".loadVehicleData" ).change(function() {
      var vehData = $(this).children("option:selected").val();
      $.ajax({
          type:'POST',
          url:  url+'load-list-vehicle-specification',
          data: { 'vehData': vehData },
          success: function(data){
              $('#listLoad').html(data);
          }
        });

});

$( ".manufact" ).change(function() {
  $('#model').html('').selectpicker('refresh');
  $('#variant').html('').selectpicker('refresh');
     var mfd = $(this).children("option:selected").val();
     $.post(url+'getModelList', { mfd: mfd }).done(function (data) {
            $('#model').html(data).selectpicker('refresh');
    });
});

$( ".model" ).change(function() {
     $('#variant').html('').selectpicker('refresh');
     var mfd   = $(".manufact option:selected").val();
     var model = $(this).children("option:selected").val();
     $.post(url+'getVarientList', { 'mfd': mfd, 'model': model }).done(function (data) {
            $('#variant').html(data).selectpicker('refresh');
    });
});

$( ".alert_category" ).change(function() {
  $('#alert_sub_category').html('').selectpicker('refresh');
     var alertCategory = $(this).children("option:selected").val();
     $.post(url+'get-alert-sub-category', { 'alertCategory' : alertCategory, 'action': 'add' }).done(function (data) {
            $('#alert_sub_category').html(data).selectpicker('refresh');
    });
});

// Developer 2 Code Js

function clearDateBeforeDb(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var date      = $("#datetimepicker4").val();
  var device_id = $("#device_id").val();
  var r = confirm("Are You Sure You want to delete that record?");
  if(r == true){
   $.ajax({
          type:'POST',
          url:  url+'clear-db-optimize',
          data: { 'date1': date,
                  'device_id' : device_id  },
          success: function(data){
           /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
           $('#success').show();
          }
        });
  }
  else{
  alert("Not Deleted");
  }
}
function clearDateBeforeRange(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var fdate      = $("#datetimepicker5").val();
  var device_idr = $("#device_idr").val();
   $.ajax({
          type:'POST',
          url:  url+'clear-db-optimize-range',
          data: { 'date2': fdate,
                  'device_id2' : device_idr  },
          success: function(data){
           /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
           $('#success1').show();
          }
        });
}
function editActiveValid(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var ac_code    = $('#ac_code').val();
  var used_by    = $("#used_by").val();
  var mc_id      = $("#mc_id").val();
  var exp_dt     = $("#exp_dt").val();
  var used_email = $("#used_email").val();
  if(ac_code == '' || used_by == '' || exp_dt == '' || used_email == ''){
      $('#cli_message').html("Fields required.").show();
      return false;
  }else{
      return true;
  }
}
function validateActive(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var ac_code = $('#ac_code').val();
  var used_by = $('#used_by').val();
  var exp_dt  = $('#datetimepicker4').val();
  var u_email = $('#u_email').val();
  if(ac_code == '' || used_by == '' || exp_dt == '' || u_email == ''){
    $('#cli_message').html("Fields required.").show();
    return false;
  }else{
      return true;
  }
}