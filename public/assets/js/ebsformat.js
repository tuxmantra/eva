/**
* AES JSON formatter for CryptoJS
*
* @author BrainFooLong (bfldev.com)
* @link https://github.com/brainfoolong/cryptojs-aes-php
*/

var CryptoJSAesJson = {
    stringify: function (cipherParams) {
		
        var j = {se8uijyjmcjc78cklioduyenkmsk783bdkd97sjsnjs78snssjhsnjs6jssjjnsbbsbjsknk654456cuRkU5kvIp2DuXhRmWifhlEWFhzbSKivTrRWMqH0207T2sjJL1mP1dj2FQwKv9RUsGXyDruYA5f9qfaQoEDQLF3vsReQa: cipherParams.ciphertext.toString(CryptoJS.enc.Base64),hxTubBpxXO8oMaNKNvFm:'9QNvcFhV0s9QNvcFhV0s',IHDeb2P9MJUJoDNdRfG5:'bfRnJmUHxfoGl0CfHamC',WZfiZiuJrdXTwRAS6fA9:'8uYngMvH6G1fbhU6wVij',GaJqk4YWX80o6vkgR3AD:'EflSiHA7KdyWHfNx9qfx',N0NRJ8HZAiJ5oJotvXu6:'gzH1uZwhuI86DAflFaFr',BwR4CQYFK1KYjFFX5vOL:'pTgR9x7wIWKs5TYalmtE',pTpp1gB6fKmiriOy7r0C:'V0h1BtvFGcTuba3oGPVd'};
        if (cipherParams.iv) j.axioqu89qopqio7st67z0op1jkn78snssjhsnjs6jssjjnsbbsbjsknk654456cuRkU5kvIp2DuXhRmWifhlEWFhzbSKivTrRWMqH0207T2sjJL1mP1dj2FQwKv = cipherParams.iv.toString();
        if (cipherParams.salt) j.qioioq7staxu867z0op1jkn9qop78snssjhsnjs6jssjjnsbbsbjsknk654456cuRkU5kvIp2DuXhRmWifhlEWFhzbSKivTrRWMqH0207T2sjJL1mP1dj2FQwKv = cipherParams.salt.toString();
        return JSON.stringify(j).replace(/\s/g, '');
    },
    parse: function (jsonStr) {
        var j = JSON.parse(jsonStr);
        var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.se8uijyjmcjc78cklioduyenkmsk783bdkd97sjsnjs78snssjhsnjs6jssjjnsbbsbjsknk654456cuRkU5kvIp2DuXhRmWifhlEWFhzbSKivTrRWMqH0207T2sjJL1mP1dj2FQwKv9RUsGXyDruYA5f9qfaQoEDQLF3vsReQa)});
        if (j.axioqu89qopqio7st67z0op1jkn78snssjhsnjs6jssjjnsbbsbjsknk654456cuRkU5kvIp2DuXhRmWifhlEWFhzbSKivTrRWMqH0207T2sjJL1mP1dj2FQwKv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.axioqu89qopqio7st67z0op1jkn78snssjhsnjs6jssjjnsbbsbjsknk654456cuRkU5kvIp2DuXhRmWifhlEWFhzbSKivTrRWMqH0207T2sjJL1mP1dj2FQwKv);
        if (j.qioioq7staxu867z0op1jkn9qop78snssjhsnjs6jssjjnsbbsbjsknk654456cuRkU5kvIp2DuXhRmWifhlEWFhzbSKivTrRWMqH0207T2sjJL1mP1dj2FQwKv) cipherParams.salt = CryptoJS.enc.Hex.parse(j.qioioq7staxu867z0op1jkn9qop78snssjhsnjs6jssjjnsbbsbjsknk654456cuRkU5kvIp2DuXhRmWifhlEWFhzbSKivTrRWMqH0207T2sjJL1mP1dj2FQwKv);
        return cipherParams;
    }
}
