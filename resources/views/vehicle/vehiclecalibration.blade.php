  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                    
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>

    <div class="col-md-12" align="right">
              <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Vehicle Calibration</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <form method="post" action="{{url('/')}}/update-vehicle-calibration" autocomplete="off" class="form-horizontal">
                   <input type="hidden" value="{{$form_token}}" name="form_token">
                   <input type="hidden" value="{{App\Helpers\Helper::crypt(1,$vehicle_data->id)}}" name="vehId">
                   <input type="hidden" value="{{App\Helpers\Helper::crypt(1,$vehicle_data->device)}}" name="did">
                  <?php                       
                    $pDVar     = ($vehicle_data->fuel_tpe == 1) ? "p" : "d";
                    $f1        = "calval".$pDVar;     
                    $calValp   = json_decode($setting_data->$f1,true);
                  ?>
                    <div class="row" style="margin-top: 25px; margin-bottom: 25px;" align="center">
                   <div class="col-md-12">
                     <hr style="width: 100%;">
                     <label class="col-form-label">Calibration Values - <?php echo ($pDVar == 'p')? "Petrol":"Diesel"; ?></label>
                     <hr style="width: 100%;"> 
                    </div>
                    </div>

                
                <input type="hidden" name="pDVar" value="<?= $pDVar ?>">

                <div class="row">
                <?php
                // print_r( $vehicleData );die;
                $callistN = ($cal_list->callist);
                $callist  =  preg_split ('/$\R?^/m',$callistN);
                for($xi=0;$xi<count($callist);$xi++)
                {
                    $i       = $xi;
                    $calval  = json_decode($vehicle_data->calval,true);
                    $var     = trim($callist[$i]);         
                    $varI    = $var;
                    if($callist[$i] != ""){ 
                    ?>
                          <div class="col-md-3">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-weight: 600; font-size: 11px;     color: black;"><?=$callist[$i]?></label>
                              <br>
                              <?php if(!empty($calval)){ ?>
                              <?php if(array_key_exists($varI,$calval)){ ?>
                              <input type="text" value="<?=$calval[$varI]?>" id="calval<?=$i?>" name="<?=$var?>" class="form-control" placeholder="">
                              <?php }else{ ?>
                              <input type="text" value="" id="calval<?=$i?>" name="<?=$var?>" class="form-control" placeholder="">
                              <?php } ?>
                              <?php } ?>
                              
                            </div>
                          </div>
                <?php
                }}
                ?>       
                       
                </div>
           
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      
            <?php if(!empty($calval)){ ?>
            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
       </form>
     
    </div>
  </div>
   
  <!--   Core JS Files   -->
 @include('default.footer')
  @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif