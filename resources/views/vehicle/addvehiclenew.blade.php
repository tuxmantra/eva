  @include('default.header')
                <style type="text/css">
    .form-control{
      margin-top: 15px;
    }
    .bmd-form-group .form-control, .bmd-form-group label, .bmd-form-group input {
    line-height: 1.1;
}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    /* width: 220px; */

}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
     width: 100%; 
}
.card .card-header-primary .card-icon,
.card .card-header-primary .card-text,
.card .card-header-primary:not(.card-header-icon):not(.card-header-text),
.card.bg-primary,
.card.card-rotate.bg-primary .front,
.card.card-rotate.bg-primary .back {
  background: linear-gradient(60deg, #ffffff, #ffffff);
}
.card {
    box-shadow: 0 1px 16px 0 rgba(0, 0, 0, 0.14);
}
  </style>
                <div class="card-body ">
                  <h3>Vehicle Info</h3>
                  <form method="post" action="{{url('/')}}/process-vehicle-data" class="form-horizontal" name="myForm" id="myForm">
                  <input type="hidden" value="{{$form_token}}" name="form_token">
                  <input type="hidden" name="idVal" value="<?php if(!empty($vehicledata)){ ?>{{App\Helpers\Helper::crypt(1,$vehicledata->id)}} <?php } ?>">
                  <input type="hidden" name="did" value="{{App\Helpers\Helper::crypt(1,$did)}}">
                    <div class="row">
                      
                       <div class="col-md-4">
                            <div class="form-group ">
                              <label class="col-form-label" style="font-size: 14px;">Manufacturer</label>
                             <br>
                               <select class="selectpicker manufact" data-style="select-with-transition" title="Select Manufacturer" data-size="7" name="manufact" id="manufact" tabindex="-98">
                                  <option disabled="">Manufacturer</option>
                                  @if (!empty($mfdList))
                                  @foreach ($mfdList as $key => $mfd)
                                  <option <?php if(!empty($vehicledata)){ if($vehicledata->manufact == $key){ ?> selected <?php }} ?> value="{{$key}}">{{$mfd}}</option>
                                  @endforeach
                                  @endif
                                </select>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Model</label>
                              <br>
                               <select class="selectpicker model" data-style="select-with-transition" title="Select Model" data-size="7" name="model" id="model" tabindex="-98" onchange="variant_list()">
                                  <option disabled="">Model</option>
                                </select>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Variant</label>
                              <br>
                                   <select class="selectpicker variant" data-style="select-with-transition" title="Select Variant" data-size="7" name="variant" id="variant" tabindex="-98" onchange="getvehicletype()">
                                      <option disabled="">Variant</option>
                                   </select>
                            </div>
                          </div>
                    </div>
                     <div class="row">
                       
                          <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                               
                                <label class="col-form-label" style="font-size: 14px;">Fuel Type</label>
                                
                                <select class="selectpicker fuel" data-style="select-with-transition"  title="Select Fuel Type" data-size="7" name="fuel" id="fuel" tabindex="-98" onchange="getEngineCap();">
                                    <option disabled="">Fuel Type</option>
                                    @if (!empty($fuel))
                                    @foreach ($fuel as $fu)
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->fuel_tpe == $fu->fuel_type_id){ ?> selected <?php }} ?> value="{{$fu->fuel_type_id}}">{{$fu->fuel_name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                              </div>
                              <!-- <label class="col-form-label" style="font-size: 14px;">Fuel Type</label>
                              <br>
                              
                              <select class="selectpicker fuel" data-style="select-with-transition" title="Select Fuel Type" data-size="7" name="fuel" id="fuel" tabindex="-98">
                                    <option disabled="">Fuel Type</option>
                                    @if (!empty($fuel))
                                    @foreach ($fuel as $fu)
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->fuel_tpe == $fu->fuel_type_id){ ?> selected <?php }} ?> value="{{$fu->fuel_type_id}}">{{$fu->fuel_name}}</option>
                                    @endforeach
                                    @endif
                              </select> -->
                        
                           
                          </div>
                          <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Engine Capacity</label>
                              <br>
                              
                                <select class="selectpicker engcap" data-style="select-with-transition" title="Select Engine Capacity" data-size="7" name="engcap" id="engcap" tabindex="-98">
                                    <option disabled="">Engine Capacity</option>
                                    @if (!empty($engcap))
                                    @foreach ($engcap as $key => $value)
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->eng_cap == $key){ ?> selected <?php }} ?> value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                    @endif
                                </select>
                        
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Transmission</label>
                              <br>
                              <input type="text" value="<?php if(!empty($vehicledata)){ echo $vehicledata->trans_type; }else{ echo "Manual"; } ?>" id="transmission" name="transmission" class="form-control" placeholder="Enter Transmission">

                            </div>
                          </div>
                    </div>
                    <div class="row">
                       <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Year Of Manufacturer</label>
                              <br>
                              
                               <select class="selectpicker manu_year" data-style="select-with-transition" title="Select Manufacture Year" data-size="7" name="manu_year" id="manu_year" tabindex="-98">
                                    <option disabled="">Manufacture Year</option>
                                    @for ($i=date('Y');$i>=1900;$i--)
                                         <option <?php if(!empty($vehicledata)){ if($vehicledata->manufact_yr == $i){ ?> selected <?php }} ?> value="{{$i}}">{{$i}}</option>
                                    @endfor
                              </select>

                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Date Of Purchase</label>
                              <br>
                              <?php if(!empty($vehicledata)){ $dateofpurchase = date('m/d/Y',strtotime($vehicledata->purchase_dt)); } ?>
                                <input type="text" name="dateofpurchase" id="dateofpurchase" class="form-control datepicker" value="<?php if(!empty($vehicledata)){ echo $dateofpurchase; } ?>" placeholder="">

                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Distance Travelled Since Purchase</label>
                              <br>
                              
                              <input type="text" value="<?php if(!empty($vehicledata)){ echo $vehicledata->travel_purchase; } ?>" id="distance" name="distance" class="form-control" placeholder="">
                        
                            </div>
                          </div>
                    </div>
                    <div class="row">
                       <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Registration Number</label>
                              <br>
                              
                              <input type="text" value="<?php if(!empty($vehicledata)){ echo $vehicledata->reg_no; } ?>" id="registration" name="registration" class="form-control" placeholder="">

                            </div>
                          </div>

                         <!--  <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Vehicle Insured</label>
                              <br>
                              
                              <select class="selectpicker insure" data-style="select-with-transition" title="Select Manufacture Year" data-size="7" name="insure" id="insure" tabindex="-98">
                                    <option disabled="">Vehicle Insured</option>
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->vehicle_insure == 1){ ?> selected <?php } } ?> value="1">Yes</option>
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->vehicle_insure == 0){ ?> selected <?php } } ?> value="0">No</option>
                                  
                              </select>
                        
                            </div>
                          </div> -->
                          <!-- <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Renewal Date if Insured</label>
                              <br>
                              <?php if(!empty($vehicledata)){ $renew_dt_insure = date('m/d/Y',strtotime($vehicledata->renew_dt_insure)); } ?>
                               <input type="text" name="renewalinsure" id="renewalinsure" class="form-control datepicker" value="<?php if(!empty($vehicledata)){ echo $renew_dt_insure; } ?>" placeholder="">
                        
                            </div>
                          </div>-->
                    <!-- </div> -->
                    
                       <!-- <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Date of Last Service</label>
                              <br>
                               <?php if(!empty($vehicledata)){ $lastservicedate = date('m/d/Y',strtotime($vehicledata->last_serv_dt)); } ?>
                              <input type="text" name="lastservicedate" id="lastservicedate" class="form-control datepicker" value="<?php if(!empty($vehicledata)){ echo $lastservicedate; } ?>" placeholder="">

                            </div>
                          </div> -->
                         <!--  <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Kms at Last Service</label>
                              <br>
                              
                              <input type="text" value="<?php if(!empty($vehicledata)){ echo $vehicledata->travel_service; } ?>" id="kmlastservice" name="kmlastservice" class="form-control" placeholder="">
                        
                            </div>
                          </div>
                        </div> -->
                        <!-- <div class="row"> -->
                          <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Status</label>
                              <br>
                              
                               <select class="selectpicker status" data-style="select-with-transition" title="Select Status" data-size="7" name="status" id="status" tabindex="-98">
                                    <option disabled="">Status</option>
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->status == 1){ ?> selected <?php } } ?> value="1">Active</option>
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->status == 0){ ?> selected <?php } } ?> value="0">Inactive</option>
                                  
                              </select>
                        
                            </div>
                          </div>
                    
                       <!-- <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Date of Oil Change</label>
                              <br>
                              <?php if(!empty($vehicledata)){ $oilchangedate = date('m/d/Y',strtotime($vehicledata->oil_dt)); } ?>
                               <input type="text" name="oilchangedate" id="oilchangedate" class="form-control datepicker" value="<?php if(!empty($vehicledata)){ echo $oilchangedate; } ?>" placeholder="">


                            </div>
                          </div> -->
                          <!-- <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">KMs at Oil chnage</label>
                              <br>
                              
                              <input type="text" value="<?php if(!empty($vehicledata)){ echo $vehicledata->oil_dist; } ?>" id="kmatoilchange" name="kmatoilchange" class="form-control" placeholder="">
                        
                            </div>
                          </div>
                        </div> -->
                        <!-- <div class="row"> -->
                          <div class="col-md-4">
                                         <div class="form-group bmd-form-group">
                               <label class="col-form-label" style="font-size: 14px;">City</label>
                              <br>
                              <select class="selectpicker city" data-style="select-with-transition" title="Select City" data-size="7" name="city" id="city" tabindex="-98">
                                    <option disabled="">City List</option>
                                    @if (!empty($citylist))
                                    <?php $cI = 0; ?>
                                    @foreach ($citylist as $value)
                                    <?php
                                          $cI++;
                                          $city = explode(',',$value);
                                         
                                    ?>
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->city == $cI){ ?> selected <?php }} ?> value="{{$cI}}">{{$city[0]}}</option>
                                    @endforeach
                                    @endif
                                </select>
                        
                            </div>
                          </div>
                          <div class="col-md-4">
                                             <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Calibration</label>
                              <br>
                              
                              <select class="selectpicker calib" data-style="select-with-transition" title="Select Calibration" data-size="7" name="calib" id="calib" tabindex="-98">
                                    <option disabled="">Status</option>
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->calib == 1){ ?> selected <?php } } ?> value="1">Active</option>
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->calib == 0){ ?> selected <?php } } ?> value="0">Inactive</option>
                                  
                              </select>

                            </div>
                          </div>
                    
                       <div class="col-md-4">
                                        <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Monitoring on drives</label>
                              <br>
                              
                              <input type="number" id="moniter" name="moniter" class="form-control" placeholder="" value="<?php if(!empty($vehicledata)){ echo $vehicledata->monitor; } ?>">
                        
                            </div>
                      </div>
                          
                          
                    </div>
                    

                    <div class="row">
                       <div class="col-md-4">
                               <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Chassis Number</label>
                              <br>
                              
                              <input type="text" id="chassis_num" name="chassis_num" class="form-control" placeholder="" value="<?php if(!empty($vehicledata)){ echo $vehicledata->chassis_num; } ?>">
                        
                            </div>
                      </div>
                    <div class="col-md-4">
                               <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Engine Number</label>
                              <br>
                              
                              <input type="text" id="engine_num" name="engine_num" class="form-control" placeholder="" value="<?php if(!empty($vehicledata)){ echo $vehicledata->engine_num; } ?>">
                        
                            </div>
                      </div>
                          <div class="col-md-4">
                 
                          </div>
                    </div>
                    <div class="row">
                       <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Any Modifications On The Vehicle</label>
                              <br>
                              
                              <textarea class="form-control" name="modification" id="modification" rows="5"><?php if(!empty($vehicledata)){ echo $vehicledata->modific; } ?></textarea>

                            </div>
                          </div>
              
                    
                       <div class="col-md-6">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Any Notable Component History</label>
                              <br>
                              
                              <textarea class="form-control" name="note" id="note" rows="5"><?php if(!empty($vehicledata)){ echo $vehicledata->note; } ?></textarea>
                         
                            </div>
                          </div>
                 
                    </div>


                
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" onclick="functionsubmit()" class="btn btn-fill btn-rose">Save</button>

            </div>
          </div>
        </div>
      </div>
       </form>
      
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

  <script type="text/javascript">
    $('select').selectpicker();
    $( ".loadVehicleData" ).change(function() {
      var vehData = $(this).children("option:selected").val();
      $.ajax({
          type:'POST',
          url:  url+'load-list-vehicle-specification',
          data: { 'vehData': vehData },
          success: function(data){
              $('#listLoad').html(data);
          }
        });

});

$( ".manufact" ).change(function() {
  $('#model').html('').selectpicker('refresh');
  $('#variant').html('').selectpicker('refresh');
     var mfd = $(this).children("option:selected").val();
     $.post(url+'getModelList', { mfd: mfd }).done(function (data) {
            $('#model').html(data).selectpicker('refresh');
    });
});

function variant_list() {
     $('#variant').html('').selectpicker('refresh');
     var mfd   = $(".manufact option:selected").val();
     var model = $(".model option:selected").val();
     $.post(url+'getVarientList', { 'mfd': mfd, 'model': model }).done(function (data) {
            $('#variant').html(data).selectpicker('refresh');
    });  
};
  </script>
 @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif
                      <script type="text/javascript">
     function functionsubmit(){
        // alert('Hi');
      document.getElementById("myForm").submit();
    }
    </script>
 <script type="text/javascript">
    $(document).ready(function() {
      var IsNull= '@Session["msg"]'!= null; 
      var str='<?=session('msg') ?>';
      if(str !=''){
         // var msg= ''; 
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: '<?=session('msg') ?>',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
        <?php
             session()->forget('msg');
             // session(['msg' => '']);
         ?>
      }


      // initialise Datetimepicker and Sliders
      md.initFormExtendedDatetimepickers();
      if ($('.slider').length != 0) {
        md.initSliders();
      }

    <?php if(!empty($vehicledata)){ ?>

      loadvehicle();
      
    <?php } ?>
    });

    function loadvehicle(){
    
         var mfd = $("#manufact option:selected").val();
         <?php if(!empty($vehicledata)){ ?>
         <?php $vehicledatamodel = ($vehicledata->model != '')? $vehicledata->model:""; ?>
         <?php }else{ ?>
         <?php $vehicledatamodel = ""; ?>
         <?php } ?>
         $.post('{{url('/')}}/getModelList', { mfd: mfd }).done(function (data) {
                $('#model').html(data).selectpicker('refresh');
                $('#model').val('<?= $vehicledatamodel ?>').selectpicker('refresh');
                loadvehicle1();
          });
    }

    function loadvehicle1(){
         var mfd   = $("#manufact option:selected").val();
         var model = $("#model option:selected").val();
         <?php if(!empty($vehicledata)){ ?>
         <?php $vehicledatavarient = ($vehicledata->varient != '')? $vehicledata->varient:""; ?>
         <?php }else{ ?>
         <?php $vehicledatavarient = ""; ?>
         <?php } ?>

         $.post('{{url('/')}}/getVarientList', { 'mfd': mfd, 'model': model }).done(function (data) {
                $('#variant').html(data).selectpicker('refresh');
                $('#variant').val('<?= $vehicledatavarient ?>').selectpicker('refresh');
         });
    }
    function getvehicletype()
     {
        // $('#fuel').selectpicker('deselectAll');

         $('#fuel').html('<option disabled>Fuel Type</option><option value = "1">Petrol</option><option value = "2">Diesel</option>').selectpicker('refresh'); 
         $('#engcap').html('<option disabled>Engine Capacity</option>').selectpicker('refresh'); 
         // alert('Hi');
     }
    function getEngineCap(){
         var mfd     = $(".manufact option:selected").val();
         var model   = $(".model option:selected").val();
         var variant = $(".variant option:selected").val();
         var fuel = $(".fuel option:selected").val(); 
        

         $.post('{{url('/')}}/getEngineCap', { 'mfd': mfd, 'model': model , 'variant' : variant ,'fuel' : fuel }).done(function (data) {
                $('#engcap').html(data).selectpicker('refresh');
                // $('#engcap').val('<?= $vehicledatavarient ?>').selectpicker('refresh');
         });
    }
 </script>