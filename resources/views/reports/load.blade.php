            <div class="row">
              <div class="col-md-8">
             <ul class="nav nav-pills nav-pills-warning" role="tablist">
                        <li class="nav-item">
                           <a class="nav-link active" onclick="lastDrive()" data-toggle="tab" href="#link1" role="tablist">
                           REPORTS HISTORY
                           </a>
                        </li>
                        <?php if(session('userdata')['ucategory'] !=4){ ?>
                        <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#link2" role="tablist">
                           TABLE S1
                           </a>
                        </li>
                        
                        <?php } ?>
                        <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#link5" role="tablist">
                           UPLOAD NEW REPORT
                           </a>
                        </li>
                        <?php if(session('userdata')['ucategory'] ==4){ ?>
                        <!-- <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#link6" role="tablist">
                           SHOW ALL
                           </a>
                        </li> -->
                      <?php } ?>
                     </ul>
                   </div>
                     <div class="col-md-4" >
                           
                            <div class="col-md-12" >
                               <input class="form-control" type="text" name="daterange1" value="" style="margin-top: -6px;" />
                            </div>
                             
                            <div class="col-md-12" >
                               <button class="btn btn-primary btn-sm" id="directReport" onclick="loadCronRepoByDid('<?= $did ?>')" disabled="disabled">Generate Report</button>
                            </div>

                            <div class="col-md-12">
                              <input type="hidden" name="hiddenStart" id="hiddenStart" value="">
                              <input type="hidden" name="hiddenEnd" id="hiddenEnd" value="">
                            </div>
                          
                          </div>
                        </div>
                     <div class="tab-content tab-space" style=" margin-top: -13px;">
                        <div class="tab-pane active" id="link1">
                      <!--     <div class="col-md-12" align="right">
                           
                            <div class="col-md-4" style="float: left;">
                               <input class="form-control" type="text" name="daterange1" value="" />
                            </div>
                            <div class="col-md-4" style="float: left;">
                               <button class="btn btn-primary btn-sm" id="directReport" onclick="loadCronRepoByDid('<?= $did ?>')" disabled="disabled">Generate Report</button>
                            </div>
                            <div class="col-md-4" style="float: left;">
                              <input type="hidden" name="hiddenStart" id="hiddenStart" value="">
                              <input type="hidden" name="hiddenEnd" id="hiddenEnd" value="">
                            </div>
                           
                          </div>
                          <br> --><br>
                           <div class="material-datatables">
                              <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                 <thead>
                                    <tr>
                                       <th class="table-heading-font">S.No</th>
                                       <th class="table-heading-font">Drive No</th>
                                       <th class="table-heading-font">Vehicle</th>
                                       <th class="table-heading-font">Date</th>
                                       
                                       <th class="table-heading-font">Monitoring Report</th>
                                    </tr>
                                 </thead>
                                 
                                 <tbody>
                                    @if (!empty($report_list))
                                    <?php $id = 1; ?>
                                    @foreach ($report_list as $list)  
                                    <tr>
                                       <td style="background-color: #f5f5f5;"><?= $id ?></td>
                                       <td style="background-color: #ffffff;">{{$list->drive}}</td>
                                       <td style="background-color: #ffffff;">{{$list->vehicle}}</td>
                                       <td style="background-color: #ffffff;">
                                 
                                       <?php echo date('Y-m-d',strtotime($list->dt)); ?>
                                        

                                       </td>
                                       
                                       <td style="background-color: #ffffff;" align="center">
                                         
                                              <a href="javascript:void(0)" onclick="loadMonRepo('<?= $list->id ?>')"><i class="material-icons">cloud_download
                                              </i>
                                              </a>
                                          
                                          

                                       </td>
                                    </tr>
                                    <?php $id++; ?>  
                                    @endforeach
                                    @else
                                    @endif
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <div class="tab-pane" id="link2">
                           <div class="container-fluid">
                              <div class="continues">
                                 <input type="hidden" name="hiddenStart1" id="hiddenStart1" value="">
         
                              <input type="hidden" name="hiddenEnd2" id="hiddenEnd2" value="">
                              
                                 <div class="row classes">

                                     <!-- Heat Graph 1 start -->
                                            <div class="col-md-4" >
                                            <div class="form-group">
                                             
                                              <select class="selectpicker valueType1" data-style="select-with-transition" title="Select Column Para" data-size="7" name="valueType1" id="valueType1" tabindex="-98">
                                                <option disabled="">Select Column Para</option>
                                                @if (!empty($valuePartList))
                                                @foreach ($valuePartList as $vpl)
                                                <option <?php if($vpl->id == '4'){ ?> selected <?php } ?>  value="{{$vpl->id}}">{{$vpl->title}}</option>
                                                @endforeach
                                                @endif
                                              </select>
                                            </div>
                                            </div>
                                              <div class="col-md-4" >
                                            <div class="form-group">
                                              <select class="selectpicker valueType2" data-style="select-with-transition" title="Select Row Para" data-size="7" name="valueType2" id="valueType2" tabindex="-98">
                                                <option disabled="">Select Row Para</option>
                                                @if (!empty($valuePartList))
                                                @foreach ($valuePartList as $vpl)
                                                <option <?php if($vpl->id == '5'){ ?> selected <?php } ?> value="{{$vpl->id}}">{{$vpl->title}}</option>
                                                @endforeach
                                                @endif
                                              </select>
                                            </div>
                                          </div>
                                           <div class="col-md-4" >
                                            <input class="form-control" type="text" name="daterange" value="" />
                                            </div>
                                             <div class="col-md-4" >
                                            <div class="form-group">
                         
                                            <select class="selectpicker rangecolumn" data-style="select-with-transition" title="Select Columns" data-size="7" name="rangecolumn" id="rangecolumn" tabindex="-98">
                                              <option disabled="">Select Columns</option>
                                              <option value="5">5 Column</option>
                                              <option value="6">6 Column</option>
                                              <option value="7">7 Column</option>
                                              <option value="8">8 Column</option>
                                              <option value="9">9 Column</option>
                                              <option value="10">10 Column</option>
                                              <option value="11">11 Column</option>
                                              <option value="12">12 Column</option>
                                              <option value="13">13 Column</option>
                                              <option value="14">14 Column</option>
                                              <option value="15">15 Column</option>
                                            </select>
                                          </div>
                                       </div>
                                         <div class="col-md-4" >
                                          <div class="form-group">
                         
                                            <select class="selectpicker rangerow" data-style="select-with-transition" title="Select Rows" data-size="7" name="rangerow" id="rangerow" tabindex="-98">
                                              <option disabled="">Select Rows</option>
                                              <option value="5">5 Rows</option>
                                               <option value="6">6 Rows</option>
                                                <option value="7">7 Rows</option>
                                                <option value="8">8 Rows</option>
                                                 <option value="9">9 Rows</option>
                                                  <option value="10">10 Rows</option>
                                                   <option value="11">11 Rows</option>
                                                    <option value="12">12 Rows</option>
                                                     <option value="13">13 Rows</option>
                                                     <option value="14">14 Rows</option>
                                                     <option value="15">15 Rows</option>
                                            </select>
                                          </div>
                                       </div>
                   

                                            <div class="col-md-12" align="center" style="margin-bottom: 20px;">
          <span  id="heat_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500; display: none;">Heat Map Generated.</span>
         </div>                

                    
                   
                   
                   <div class="col-md-12" style="margin-bottom: 30px;" id="place">

                   </div>
                      
                   <div class="col-md-12" style="margin-bottom: 30px;" id="place1">

                   </div>
                    <div class="col-md-12" align="left" style="margin-bottom: 20px;">
                                  <button class="btn btn-primary btn-sm" id="HeatReport1" onclick="loadHeatRepoByDid1('<?= $did ?>')" disabled="disabled">Generate Report</button>
                    </div>   
                   


                                     <!-- Heat Graph 1 end -->
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                         <div class="tab-pane" id="link5">
                           <div class="container-fluid">
                              <div class="continues">
                                 <div class="row classes">
                                    <div class="col-md-12" style="margin-bottom: 200px; padding: 50px;">
                                      <br>
                                      <form action="file-upload-report" enctype="multipart/form-data" method="post" name="file_upload" id="file_upload" onsubmit="return validateUpload();">
                                      <div class="col-md-4" style="float: left;">
                                        <input type="text" name="type" id="type" value="" class="form-control" placeholder="Enter Report Type/Name">
                                      </div>
                                      <div class="col-md-5" style="float: left;">
                                    
                                      <input type="hidden" id="device_id" name="device_id" value="<?= $did ?>">
                                      <input type="file" accept="application/pdf" name="report" id="report" class="form-control">
                                    
                                      </div>
                                      <div class="col-md-3" style="float: left;">
                                         <button class="btn btn-primary btn-sm">Upload</button>
                                      </div>
                                      </form>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>

                        <div class="tab-pane" id="link6">
                            
                         
                           <!-- Active reports start -->
                          <br><br>
                <div class="col-md-12">
                   <div class="col-md-2" style="float: right;" align="right"><a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="deleteRecords()">Delete</a></div>
                   <div class="col-md-3" style="float: right;" align="right"><a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="loadInactiveReport()">InActive Records</a></div>
                </div>  
                          <br><br>
                           <div class="material-datatables">
                              <table id="datatables1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                 <thead>
                                    <tr>
                                       <th>&nbsp;&nbsp;</th>
                                       <th class="table-heading-font">S.No</th>
                                       <th class="table-heading-font">Report No</th>
                                       <th class="table-heading-font">Type</th>
                                       <th class="table-heading-font">Date</th>
                                       <th class="table-heading-font">User Name</th>
                                       <th class="table-heading-font">Device ID</th>
                                       <th class="table-heading-font">Monitoring Report</th>
                                    </tr>
                                 </thead>
                                 <tfoot>
                                    <tr>
                                      <th>&nbsp;&nbsp;</th>
                                       <th class="table-heading-font">S.No</th>
                                       <th class="table-heading-font">Report No</th>
                                       <th class="table-heading-font">Type</th>
                                       <th class="table-heading-font">Date</th>
                                       <th class="table-heading-font">User Name</th>
                                       <th class="table-heading-font">Device ID</th>
                                       <th class="table-heading-font">Monitoring Report</th>
                                    </tr>
                                 </tfoot>
                                 <tbody>
                                    @if (!empty($report_all))
                                    <?php $id = 1; ?>
                                    @foreach ($report_all as $list)  
                                    <tr>
                                       <td><input type="checkbox" name="active[]" value="{{$list->id}}"></td>
                                       <td style="background-color: #f5f5f5;"><?= $id ?></td>

                                       <td style="background-color: #ffffff;">R {{$list->id}} <?php if($list->mctype == 3){
                                             
                                       echo date( "Ymd", strtotime( $list->ad_dt . "-1 day"));

                                       }else{ echo date('Ymd',strtotime($list->ad_dt));  } ?></td>

                                       <td style="background-color: #f5f5f5;">{{$list->type}}</td>

                                       <td style="background-color: #ffffff;">
                                 
                                       <?php echo date('Y-m-d',strtotime($list->ad_dt)); ?>
                                        

                                      </td>
                                      <td style="background-color: #f5f5f5;" align="center">{{App\Helpers\Helper::getUserByDeviceId ($list->did)}}</td>
                                      <td style="background-color: #f5f5f5;" align="center">{{$list->devid}}</td>


                                       <td style="background-color: #ffffff;" align="center">
                                          <?php if($list->mctype == 1){ ?>
                                             <a target="_blank" href="" onclick="loadCalibRepo(<?= $list->idmc ?>)"><i class="material-icons">cloud_download
                                             </i>
                                             </a>
                                          <?php }elseif($list->mctype == 2){ ?>
                                              <a href="javascript:void(0)" onclick="loadMonRepo('<?= $list->idmc ?>')"><i class="material-icons">cloud_download
                                              </i>
                                              </a>
                                          <?php }elseif($list->mctype == 3){ ?>
                                               <?php $dt = explode(' ',$list->ad_dt); ?>
                                               <?php $dp = date( "Y-m-d", strtotime( $dt[0] . "-1 day")) ?>
                                               <a href="javascript:void(0)" onclick="loadCronRepo('<?= $dp ?>')"><i class="material-icons">cloud_download
                                               </i>
                                               </a>
                                          <?php }else{ ?>
                                               <a href="{{url('/')}}/public/reports/<?= $list->report_file ?>" download><i class="material-icons">cloud_download
                                               </i>
                                               </a>
                                          <?php } ?>
                                          

                                       </td>
                                    </tr>
                                    <?php $id++; ?>  
                                    @endforeach
                                    @else
                                    @endif
                                 </tbody>
                              </table>
                           </div>

                           <!-- Active reorts End -->

                        </div>

          
                     </div>
                     <div class="col-md-12" id="loadHeat11" style="padding: 1px; "></div>
                     <!--  <div id="loadHeat1" class="alert alert-info" style="height: 7%;
    width: 178%;
    margin-top: -576px;
    color: black;
    z-index: 1000000;
    box-shadow: none;
    background-color: white; display: none;">
                   
                    <div class="col-md-12" id="loadHeat11" style="margin-top: -20px;
        padding: 2px;
    /* height: 233px; */
    margin-top: -20px;
    width: 161%;
    max-width: 161%;
    background-color: white;
    box-shadow: 0 4px 20px 0px rgba(0, 0, 0, 0.14), 0 7px 10px -5px rgba(0, 188, 212, 0.4);" >
                  </div>
                  
                     
                   </div> -->

<!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="http://localhost/enginecal/public/assets/js/plugins/bootstrap-selectpicker.js"></script>

<script type="text/javascript">

  function validateUpload(){
       var type   = $('#type').val();
       var report = $('#report').val();
       if(type == ''){
        $('#cli_message').html("File Type/Name missing.");
            return false;
       }else if(report == ''){
        $('#cli_message').html("File not attached.");
            return false;
       }
  }

  function closemap(){
   // alert('hi');
    $('#loadHeat11').hide();
    
  }

  function loadHeatRepoByDid1(did){

      $('#heat_message').hide();
      var valuePart1 = $(".valueType1 option:selected").val();
      var valuePart2 = $(".valueType2 option:selected").val();
       var namevaluePart1 = $(".valueType1 option:selected").text();
      var namevaluePart2 = $(".valueType2 option:selected").text();
      var col        = [];
      var row        = [];
      var start      = $("#hiddenStart1").val();
      var end        = $("#hiddenEnd2").val();

      for(var i = 1; i<=15;i++){
         if($("#colmd"+i).val() != '' && $("#colmd"+i).val() != null){
            col.push($("#colmd"+i).val());
         }
      }

      for(var i = 1; i<=15;i++){
         if($("#rowmd"+i).val() != '' && $("#rowmd"+i).val() != null){
            row.push($("#rowmd"+i).val());
         }
      }

      $.post('{{url('/')}}/loadHeatGraph1', { 'did': did, 'valuePart1': valuePart1, 'valuePart2': valuePart2, 'start': start, 'end': end, 'col': col, 'row': row,'namevaluePart1':namevaluePart1,'namevaluePart2':namevaluePart2 }).done(function (data) {
            
            $('#loadHeat11').show();
            $('#loadHeat11').html(data);
            swal({
                      title: 'Success!',
                      text: 'Heat Map Generated.',
                      type: 'success',
                      confirmButtonClass: "btn btn-success",
                      buttonsStyling: false
                    })

      });
  }

  function loadInactiveReport(){
        var did = $('#device_id').val();
        $.post('{{url('/')}}/loadReportInactive', { status: '1', did: did }).done(function (data) {
           $('#link6').html(data);
        });
  }

  function loadActiveReport(){
        var did = $('#device_id').val();
        $.post('{{url('/')}}/loadReportActive', { status: '1', did: did }).done(function (data) {
           $('#link6').html(data);
        });
  }

  function deleteRecords(){

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Yes, delete it!',
        buttonsStyling: false
      }).then(function(e) {

        var val = [];
        $(':checkbox:checked').each(function(i){
            val[i] = $(this).val();
        });

         if(e.dismiss == 'cancel'){
             
         }

        if(e.value == true){
            $.post('{{url('/')}}/delete-reports', { did: '<?= $did ?>', status: '1', data: val }).done(function (data) {
                    swal({
                      title: 'Deleted!',
                      text: 'Your file has been deleted.',
                      type: 'success',
                      confirmButtonClass: "btn btn-success",
                      buttonsStyling: false
                    })
                    setTimeout(function() {
                      location.reload();
                    }, 3000)  
              })
        }

      }).catch(swal.noop)
  }

$(document).ready(function() {

      $( ".rangecolumn" ).change(function() {
            var count = $(this).children("option:selected").val();
            $('#place').html('');
            var width=97.5/count;
            for(var i=1;i<=count;i++){
                 $('#place').append('<div class="col-md-1" style=" max-width: '+width+'%; float: left;padding-left: 1px; padding-right: 0px;"><span>Cl'+i+'</span><input style="width: 100%;" type=text class="input form-control" id=colmd' + i + ' ' +
                    'value="' + i + '" /></div>');
            }
       });

      $( ".rangerow" ).change(function() {
            var count = $(this).children("option:selected").val();
            // $('#place1').show();
            var width=97.5/count;
            $('#place1').html('');
            for(var i=1;i<=count;i++){
                 $('#place1').append('<div class="col-md-1" style=" max-width: '+width+'%; float: left;padding-left: 0px; padding-right: 1px;"><span>Rw'+i+'</span><input style="width: 100%;" type=text class="input form-control" id=rowmd' + i + ' ' +
                    'value="' + i + '" /></div>');
            }
       });

      $('#valueType1').selectpicker('refresh');
      $('#valueType2').selectpicker('refresh');
      $('#rangecolumn').selectpicker('refresh');
      $('#rangerow').selectpicker('refresh');

      $('#datatables').DataTable({
           "pagingType": "full_numbers",
           "lengthMenu": [
             [10, 25, 50, -1],
             [10, 25, 50, "All"]
           ],
           responsive: true,
           language: {
             search: "_INPUT_",
             searchPlaceholder: "Search records",
           }
      });

        $('#datatables1').DataTable({
           "pagingType": "full_numbers",
           "lengthMenu": [
             [10, 25, 50, -1],
             [10, 25, 50, "All"]
           ],

           responsive: true,
           language: {
             search: "_INPUT_",
             searchPlaceholder: "Search records",
           }
       });

       // value part1 on chnage create input elements

       $( ".usertype_usercreate" ).change(function() {

            var utype = $(this).children("option:selected").val();

       });


       // value part2 on change create input elements


}); 

// Load Calib Report
function loadCalibRepo(calib_id){
   var did = '<?= $did ?>';
   window.open("{{url('/')}}/calibReport/"+calib_id+"/"+did, "_blank");
}

// Load Moni Report
function loadMonRepo(mon_id){
   var did = '<?= $did ?>';
   window.open("{{url('/')}}/monReport/"+mon_id+"/"+did, "_blank");
}

// Load Cron Report
function loadCronRepo(date){
   var did = '<?= $did ?>';
   window.open("{{url('/')}}/tvsCronReport/"+did+"/"+date+"/1", "_blank");
}

function loadCronRepoByDid(date){

   var did   = '<?= $did ?>';
   var start = $('#hiddenStart').val();
   var end   = $('#hiddenEnd').val();
   var date  = start+";"+end;
   window.open("{{url('/')}}/tvsCronReport/"+did+"/"+date+"/2", "_blank");
}

</script>   

<script type="text/javascript">
$('input[name="dates"]').daterangepicker();

$(function() {
  $('input[name="daterange"]').daterangepicker({
    dateLimit: { days : 5 },
    opens: 'left',
     timePicker: true,
     timePicker24Hour: true
  }, function(start, end, label) {
     var start = start.format('YYYY-MM-DD HH:mm:00');
     var end   = end.format('YYYY-MM-DD HH:mm:59');
     $('#hiddenStart1').val(start);
     $('#hiddenEnd2').val(end);
     if(start != '' && end != ''){
         $('#HeatReport1').prop("disabled", false);
     }
  });
});


$(function() {
  $('input[name="daterange1"]').daterangepicker({
    dateLimit: { days : 5 },
    opens: 'left',
     timePicker: true,
     timePicker24Hour: true,
     days: -1
  }, function(start, end, label) {
     var start = start.format('YYYY-MM-DD HH:mm:00');
     var end   = end.format('YYYY-MM-DD HH:mm:59');
     $('#hiddenStart').val(start);
     $('#hiddenEnd').val(end);
     if(start != '' && end != ''){
         $('#directReport').prop("disabled", false);
     }
  });
});

</script>