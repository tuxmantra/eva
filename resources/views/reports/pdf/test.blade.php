  
  <!-- <link href="{{url('/')}}/public/assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" /> -->
  <style type="text/css">
    .table 
    {
      width: 100%;
      max-width: 100%;
      margin-bottom: 1rem;
      background-color: #ffff;
      /*border-collapse: collapse;*/
    }

    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td 
    {
      padding: 12px 8px;
      vertical-align: middle;
      /*border-color: #ddd;*/
      border-collapse: collapse;
      border: 1px solid #ddd;
      text-align: center;
    }
    
    
  </style>
<?php 

$url ="../public";
// print_r($testData);
$location = '';
if($testData[0]->plant_id == 1)
{
	$location = 'HOSUR';
}
else if($testData[0]->plant_id == 2)
{
	$location = 'MYSORE';
}
else if($testData[0]->plant_id == 3)
{
	$location = 'HIMACHAL PRADESH';
}
else if($testData[0]->plant_id == 4)
{
	$location = '3W HOSUR';
}
else if($testData[0]->plant_id == 5)
{
	$location = 'MAHABHARATH';
}
else if($testData[0]->plant_id == 6)
{
	$location = 'JAWA BARAT';
}

// shift 
$shift = '';
if($testData[0]->shift == 1)
{
	$shift = '1st';
}
else if($testData[0]->shift == 2)
{
	$shift = '2nd';
}

// result pass or fail
$test_result = '';
$color = '';
if($testData[0]->test_result == 1)
{
	$test_result = 'Pass';
	$color = '#00ff00';
}
else if($testData[0]->test_result == 0)
{
	$test_result = 'Fail';
	$color = '#eb262d';
}
$content = '<table class="table">
    <tbody>
        <tr>
            <td class="text-center"><b>Location</b></td>
            <td class="text-center">'.$location.'</td>
            <td class="text-center"><b>DateTime<b></td>
            <td class="text-center">'.$testData[0]->start_time.' </td>
            <td class="text-center"><b>Shift<b></td>
            <td class="text-center">'.$shift.'</td>
        </tr>
        <tr>
        <td class="text-center" colspan="6" style="text-transform: uppercase; background-color:#4b9ddb; "><b>Operator Details</b></td>
        </tr>
        <tr>
            <td class="text-center" ><b>Name</b></td>
            <td class="text-center" colspan="2">'.$testData[0]->rider_name.'</td>
            <td class="text-center" ><b>Employee ID</b></td>
            <td class="text-center" colspan="2">XXXXXX</td>
            
        </tr>
        <tr>
        <td class="text-center" colspan="6" style="text-transform: uppercase;  background-color:#4b9ddb;"><b>Vehicle Description</b></td>
        </tr>
        <tr>
            <td class="text-center" colspan="2"><b>Model-Varient</b></td>
            <td class="text-center" colspan="4">'.$testData[0]->vehicles.' </td>
        </tr>
        <tr>
            <td class="text-center" colspan="2"><b>Vehicle Identification Number</b></td>
            <td class="text-center" colspan="4">'.$testData[0]->vin_no.'</td>
            
        </tr>

        <tr>
        <td class="text-center" colspan="6" style=" background-color:#4b9ddb;"><b>TEST DATA</b></td>
        </tr> 
        <tr>
            <td class="text-center" colspan="2" ><b>Engine Coolant Temperature</b></td>
            <td class="text-center"><b>Start</b></td>
            <td class="text-center"> '.$testData[0]->coolant_start.' deg C</td>
            <td class="text-center"><b>End</b></td>
            <td class="text-center"> '.$testData[0]->coolant_end.' deg C</td> 
        </tr> 
        <tr> 
            <td class="text-center" colspan="6" style=" background-color:#4b9ddb;"><b>RESULTS</b></td>
        </tr>
        
        <tr>
            <td class="text-center" colspan="2"><b>Parameters</b></td>
            <td class="text-center" colspan="2"><b>Value</b></td>
            <td class="text-center" colspan="2"><b>Status</b></td>
        </tr> 
        <tr>
            <td class="text-center" colspan="2"><b>Fuel Mileage</b></td>
            <td class="text-center" colspan="2">'.$testData[0]->milage.' Km/Ltrs</td>
            <td class="text-center" colspan="2" rowspan="2" style="color:'.$color .';">'.$test_result.'</td>
        </tr> 
        <tr>
            <td class="text-center" colspan="2"><b>Fuel Consumed</b></td>
            <td class="text-center" colspan="2">'.$testData[0]->fuel_cons.' Ltrs</td>
            
        </tr> 
        <tr>
            <td class="text-center" colspan="6" style=" background-color:#4b9ddb;"><b>COMMENTS</b></td>
        </tr>
        <tr>
            <td class="text-center" colspan="6" rowspan="3"></td>
        </tr>
    </tbody>
</table>';

$html ="<html> 
<head>
  <style>
    @page { margin: 50px 20px; }
    #header { font-family: Arial, Helvetica, sans-serif; position: fixed; left: 0px; top: -60px; right: 0px; height: 60px;  border-bottom: 1px solid #000; }
    #footer { position: fixed; left: 0px; bottom: 10px; right: 0px; font-size: 10px; }
    #footer p {font-size: 10px;}
    #footer .page:after { content: counter(page, upper-roman);  }
    #content{font-family: Arial, Helvetica, sans-serif;}
    .hed{text-align:center; padding-top:20px;}
    p{font-family: Arial, Helvetica, sans-serif; font-size:13px; margin: 2px 0px 2px 0px;}
    div{font-size:13px;}
    td{font-size:13px;}
    body{font-size:13px;}
    h5{font-size:14px; margin: 0px; padding:2px; padding-bottom:5px;}
    h6{font-size:15px; margin: 0px; padding:0px; padding-top:2px; font-weight: normal;}
    h2{font-size:26px; margin-top: 0px; padding-top:0px;}
    span{border:1px solid #000; padding:2px;}
    ol{font-size:12px; margin: 2px 0px 2px 0px; padding:0px 5px 0px 25px;}
  </style>
<body>
  <div id='header'>
    <table width='100%'>
    <tr>
        <td width='20%'><img height='40' src='$url/images/img/1-TVS.png'> </td>
        <td width='60%' class='hed'><h2>Fuel Consumption Report</h2></td>        
        <td width='20%' style='text-align: right'><img height='40' src='$url/images/img/2-TVS.png'></td>
    </tr>
    </table>
  </div>
  <div id='content'>
    <p>
         $content   
    </p>  
  </div>
  
  </body>
</html>";
echo $html;


// <tr>
//     <td class="text-center" colspan="6" style=" background-color:#4b9ddb;"><b>AMBIENT CONDITION</b></td>
// </tr>             
// <tr>
//     <td class="text-center"><b>Temperature</b></td>
//     <td class="text-center"> '.$testData[0]->temp.' deg C</td>
//     <td class="text-center"><b>Pressure</b></td>
//     <td class="text-center"> '.$testData[0]->pressure.' kPa</td>
//     <td class="text-center"><b>Humidity</b></td>
//     <td class="text-center"> '.$testData[0]->humidity.' %</td>  
// </tr>

