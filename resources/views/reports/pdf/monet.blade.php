<?php
$conn = new mysqli('localhost', 'chikmagalur', 'id$0Wroy', 'mullayanagiri');

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sqlSett = "select id from device where device_id = '$did'";
$dty    = $conn->query($sqlSett);
$uio    = $dty->fetch_assoc();
$dataRep['id'] = $uio['id'];
// die($sqlSett);
$sqlSett = "select * from vehicle_setting";
$vehSett = $conn->query($sqlSett);
$vehSett = $vehSett->fetch_array();
$sql     = "select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='$did'";
$vehicle = $conn->query($sql);
$vehicle = $vehicle->fetch_array();


$sql    = "select * from device_monitor where id='$cali'";
$data   = $conn->query($sql);
$data   = $data->fetch_assoc();
if(!empty($data)){


$driver = getdriver($did,$conn);
$veh    = getvehicle($did,$conn,$vehSett,$vehicle);
$mfdID  = $veh[0]['manufact'];
$sqlmdl = "SELECT * FROM `vehinfo` where mfd ='$mfdID'";
$datamdl= $conn->query($sqlmdl);
$datamdl= $datamdl->fetch_assoc();
$datavar= explode(';',$datamdl['var']);
$datamdl= explode(';',$datamdl['model']);

$vType  = $veh[0]['fuel_tpe'];

$manufacturer = explode(';',$veh[1]['manufacturer']);
$dataRep['cust'] = $driver['uname'];
$mfd = explode('_', $manufacturer[$veh[0]['manufact']]);
$dataRep['mfd'] = $mfd[0];
$dataRep['model'] = $datamdl[$veh[0]['model']] . " ".$datavar[$veh[0]['varient']]." " . $veh[0]['manufact_yr'];
$dataRep['dt'] = date('d M Y H:i:s',strtotime($data['ad_dt']));
$dataRep['regno'] = $veh[0]['reg_no'];
$dataRep['odomet'] = $veh[0]['travel_purchase'];
$dataRep['devid']  = $did;
if($vType == 1)
{
   $dataRep['fuel'] = 'Petrol';
}else
{
   $dataRep['fuel'] = 'Diesel';
}
//Check if turbo available
$Turbo_Avail_C = getCaliPramVal('TC_Status_C',$dataRep['devid'],$vehSett,$vehicle,$conn);

//Weather conditions
$press = $data['press'];
$temp = $data['temp'];
$hum = $data['hmdty'];
//$dataRep['atm'] = $press. " " .$temp. " " .$hum;
$dataRep['atm'] = $press. " kPa, " .$temp. " degC, " .$hum. "% ";

//Battery System
$batHel = array();
$batHel['rate'] = 6;
$batHel1['msg'] = "";
$batHel2['msg'] = "";
if($data['bat_st'] < 10)
{
    $data['bat_st'] = $data['bat_st'];
    if($data['bat_st'] == 5)
    {
        $batHel2['msg'] = "";
    }
    else
    {
        $batHel2['msg'] = "Alternator Okay.";
    }
}
elseif($data['bat_st'] < 20)
{
    $data['bat_st'] = $data['bat_st'] - 10;
    $batHel2['msg'] = "Please check the Alternator.";
}
elseif($data['bat_st'] < 30)
{
    $data['bat_st'] = $data['bat_st'] - 20;
    $batHel2['msg'] = "Please check the Alternator Voltage Regulator.";
}
elseif($data['bat_st'] > 30)
{
    $data['bat_st'] = $data['bat_st'] - 30;
    $batHel2['msg'] = "Running voltage low. Please check Charging System.";
}

//Get Battery System comments
if($data['bat_st'] == 1)
{
    $batHel['rate'] = "1";
    $batHel['con'] = "Critical";
    $batHel1['msg'] = "\"Battery Critical\" Please check distilled water level and drive to charge. Also consider checking the Starter Motor. If Battery is more than 3 years old consider replacement.";
}
elseif($data['bat_st'] == 2)
{
    $batHel['rate'] = "2";
    $batHel['con'] = "Bad";
    $batHel1['msg'] = "\"Battery Bad\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
}
elseif($data['bat_st'] == 3)
{
    $batHel['rate'] = "3";
    $batHel['con'] = "Okay";
    $batHel1['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge.";
}
elseif($data['bat_st'] == 4)
{
    $batHel['rate'] = "4";
    $batHel['con'] = "Good";
    $batHel1['msg'] = "\"Battery Fine\" Please drive to charge.";
}
elseif($data['bat_st'] == 5)
{
    $batHel['rate'] = "5";
    $batHel['con'] = "Good";
    $batHel1['msg'] = "";
}
//Final Battery Comment
if(($batHel['rate'] < 3) && ($data['bat_st'] > 10))
{
    $batHel['msg'] = $batHel1['msg']. $batHel2['msg'];
}
elseif($data['bat_st'] > 10)
{
    $batHel['msg'] = $batHel2['msg']. $batHel1['msg'];
}
else
{
    $batHel['msg'] = $batHel1['msg']. $batHel2['msg'];
}

//Turbo
$turbo =array();
$turbo['rate'] = 6;
$turbo['msg'] = "";
if($data['idlb_c'] == 1)
{
    if($data['pekb_c'] == 1)
    {
        $turbo['rate'] = "1";
        $turbo['con'] = "Critical";
        $turbo['msg'] = "Turbo boost pressure build-up critical. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
    }
    elseif($data['pekb_c'] == 2)
    {
        $turbo['rate'] = "2";
        $turbo['con'] = "Bad";
        $turbo['msg'] = "Turbo boost pressure build-up poor. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
    }
    elseif($data['pekb_c'] == 3)
    {
        $turbo['rate'] = "3";
        $turbo['con'] = "Okay";
        $turbo['msg'] = "Turbo boost pressure build-up below average. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
    }
}
elseif($data['idlb_c'] == 2)
{
    if($data['pekb_c'] == 1)
    {
        $turbo['rate'] = "1";
        $turbo['con'] = "Critical";
        $turbo['msg'] = "Turbo boost pressure build-up critical. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
    }
    elseif($data['pekb_c'] == 2)
    {
        $turbo['rate'] = "3";
        $turbo['con'] = "Okay";
        $turbo['msg'] = "Turbo boost pressure build-up below average. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
    }
    elseif($data['pekb_c'] == 3)
    {
        $turbo['rate'] = "4";
        $turbo['con'] = "Okay-Good";
        $turbo['msg'] = "Turbo boost pressure build-up average. Clean the air filter element. Eva will keep an eye on it during the next monitoring cycle.";
    }
}
elseif($data['idlb_c'] == 3)
{
    if($data['pekb_c'] == 1)
    {
        $turbo['rate'] = "2";
        $turbo['con'] = "Bad";
        $turbo['msg'] = "Turbo boost pressure build-up poor. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
    }
    elseif($data['pekb_c'] == 2)
    {
        $turbo['rate'] = "3";
        $turbo['con'] = "Okay";
        $turbo['msg'] = "Turbo boost pressure build-up below average. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
    }
    elseif($data['pekb_c'] == 3)
    {
        $turbo['rate'] = "5";
        $turbo['con'] = "Good";
        $msg = $data['pekb_m'];
        if(($msg == "Not Applicable") || ($msg == ""))
        {
            $turbo['msg'] = "";
        }
        else
        {
            $turbo['msg'] = "Possible axial play in turbocharger. Visit Manufacturer Dealership whenever possible.";
        }
    }
}
elseif($data['idlb_c'] == 0 || $data['pekb_c'] == 0 || $data['idlb_c'] == "" || $data['pekb_c'] == "")
{
    $turbo['rate'] = "6";
    $turbo['con'] = "";
    $turbo['msg'] = "Test Conditions Not Reached";
}
//$turbo['msg'] = (trim($data['idlb_m']) != trim($data['pekb_m']) ) ? trim($data['idlb_m']) ."<br/>". trim($data['pekb_m']) : trim($data['pekb_m']);

//Air
$air =array();
$air['rate'] = 6;
$air['msg'] = "";
if($data['idlbc_v'] == 1)
{
    if($data['peakb_v'] == 1)
    {
        $air['rate'] = "1";
        $air['con'] = "Critical";
        $air['msg'] = "Air system pressure build-up critical. Replace air filter element. Check Throttle Body. Visit the Manufacturer Dealership whenever possible.";
    }
    elseif($data['peakb_v'] == 2)
    {
        $air['rate'] = "2";
        $air['con'] = "Bad";
        $air['msg'] = "Air system pressure build-up poor. Clean/replace air filter element. Check Throttle Body. Visit the Manufacturer Dealership whenever possible.";
    }
    elseif($data['peakb_v'] == 3)
    {
        $air['rate'] = "3";
        $air['con'] = "Okay";
        $air['msg'] = "Air system pressure build-up below average. Clean/replace air filter element. Check Throttle Body.";
    }
}
elseif($data['idlbc_v'] == 2)
{
    if($data['peakb_v'] == 1)
    {
        $air['rate'] = "1";
        $air['con'] = "Critical";
        $air['msg'] = "Air system pressure build-up critical. Replace air filter element. Check Throttle Body. Visit the Manufacturer Dealership whenever possible.";
    }
    elseif($data['peakb_v'] == 2)
    {
        $air['rate'] = "3";
        $air['con'] = "Okay";
        $air['msg'] = "Air system pressure build-up below average. Clean/replace air filter element. Check Throttle Body.";
    }
    elseif($data['peakb_v'] == 3)
    {
        $air['rate'] = "4";
        $air['con'] = "Okay-Good";
        $air['msg'] = "Air system pressure build-up average. Clean the air filter element. Eva will keep an eye on it during the next monitoring cycle.";
    }
}
elseif($data['idlbc_v'] == 3)
{
    if($data['peakb_v'] == 1)
    {
        $air['rate'] = "2";
        $air['con'] = "Bad";
        $air['msg'] = "Air system pressure build-up poor. Clean/replace air filter element. Check Throttle Body. Visit the Manufacturer Dealership whenever possible.";
    }
    elseif($data['peakb_v'] == 2)
    {
        $air['rate'] = "3";
        $air['con'] = "Okay";
        $air['msg'] = "Air system pressure build-up below average. Clean/replace air filter element. Check Throttle Body.";
    }
    elseif($data['peakb_v'] == 3)
    {
        $air['rate'] = "5";
        $air['con'] = "Good";
        $air['msg'] = "";
    }
}
elseif($data['idlbc_v'] == 0 || $data['peakb_v'] == 0 || $data['idlbc_v'] == "" || $data['peakb_v'] == "")
{
    $air['rate'] = "6";
    $air['con'] = "";
    $air['msg'] = "Test Conditions Not Reached";
}
//$air['msg'] = (trim($data['idlbc_m']) != trim($data['peakb_m']) ) ? trim($data['idlbc_m']) ."<br/>". trim($data['peakb_m']) :  trim($data['peakb_m']);

//Fuel
$fuel =array();
$fuel['rate'] = 6;
$fuel['msg'] = "";
if($data['idl_crp'] == 1)
{
    if($data['peak_crp'] == 1)
    {
        $fuel['rate'] = "1";
        $fuel['con'] = "Critical";
        $fuel['msg'] = "Fuel pressure critical. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
    }
    elseif($data['peak_crp'] == 2)
    {
        $fuel['rate'] = "2";
        $fuel['con'] = "Bad";
        $fuel['msg'] = "Fuel pressure low. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
    }
    elseif($data['peak_crp'] == 3)
    {
        $fuel['rate'] = "3";
        $fuel['con'] = "Okay";
        $fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
    }
}
elseif($data['idl_crp'] == 2)
{
    if($data['peak_crp'] == 1)
    {
        $fuel['rate'] = "1";
        $fuel['con'] = "Critical";
        $fuel['msg'] = "Fuel pressure critical. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
    }
    elseif($data['peak_crp'] == 2)
    {
        $fuel['rate'] = "3";
        $fuel['con'] = "Okay";
        $fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
    }
    elseif($data['peak_crp'] == 3)
    {
        $fuel['rate'] = "4";
        $fuel['con'] = "Okay-Good";
        $fuel['msg'] = "Fuel pressure build-up average. Eva will keep an eye on it during the next monitoring cycle.";
    }
}
elseif($data['idl_crp'] == 3)
{
    if($data['peak_crp'] == 1)
    {
        $fuel['rate'] = "2";
        $fuel['con'] = "Bad";
        $fuel['msg'] = "Fuel pressure low. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
    }
    elseif($data['peak_crp'] == 2)
    {
        $fuel['rate'] = "3";
        $fuel['con'] = "Okay";
        $fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
    }
    elseif($data['peak_crp'] == 3)
    {
        $fuel['rate'] = "5";
        $fuel['con'] = "Good";
        $fuel['msg'] = "";
    }
}
elseif($data['idl_crp'] == "" || $data['peak_crp'] == "" || $data['idl_crp'] == 0 || $data['peak_crp'] == 0)
{
    $fuel['rate'] = "6";
    $fuel['con'] = "";
    $fuel['msg'] = "Test Conditions Not Reached";
}
//$fuel['msg'] = $data['crpm'];

//Back Pressure
// $backPre = array();
// if($data['ergsmd_ms'] == 1)
// {
//     $backPre['rate'] = "1";
//     $backPre['con'] = "Critical";
//     $backPre['msg'] = "Exhaust back-pressure build-up very high. Try driving at high speeds continuously for about 3-5 mins on the freeway. Please exercise caution.";
// }
// elseif($data['ergsmd_ms'] == 2)
// {
//     $backPre['rate'] = "2";
//     $backPre['con'] = "Bad";
//     $backPre['msg'] = "Exhaust back-pressure build-up high. Try driving at high speeds continuously for about 2-4 mins on the freeway. Please exercise caution.";
// }
// elseif($data['ergsmd_ms'] == 3)
// {
//     $backPre['rate'] = "3";
//     $backPre['con'] = "Okay";
//     $backPre['msg'] = "Exhaust back-pressure build-up high. Try driving at high speeds continuously for about 2-3 mins on the freeway. Please exercise caution.";
// }
// elseif($data['ergsmd_ms'] == 4)
// {
//     $backPre['rate'] = "4";
//     $backPre['con'] = "Okay-Good";
//     $backPre['msg'] = "Exhaust back-pressure build-up marginally high. Eva will keep an eye on it during the next monitoring cycle.";
// }
// elseif($data['ergsmd_ms'] == 5)
// {
//     $backPre['rate'] = "5";
//     $backPre['con'] = "Good";
//     $backPre['msg'] = "";
// }
// else 
// {
//     $backPre['rate'] = "";
//     $backPre['con'] = "";
//     $backPre['msg'] = $data['ergsmd_ms'];
// }

//Combustion Pressure
$copPres = array();
$copPres['rate'] = 6;
if($data['comp_pres'] == 1)
{
    $copPres['rate'] = "1";
    $copPres['con'] = "Critical";
}
elseif($data['comp_pres'] == 2)
{
    $copPres['rate'] = "2";
    $copPres['con'] = "Bad";
}
elseif($data['comp_pres'] == 3)
{
    $copPres['rate'] = "3";
    $copPres['con'] = "Okay";
}
elseif($data['comp_pres'] == 4)
{
    $copPres['rate'] = "4";
    $copPres['con'] = "Okay-Good";
}
elseif($data['comp_pres'] == 5)
{
    $copPres['rate'] = "5";
    $copPres['con'] = "Good";
}
else
{
    $copPres['msg'] = $data['comp_pres'];
}
//If MAF not available the torque calculated is wrong and hence gets the wrong rating. So we ignoring this rating
if($data['max_drive_torque'] <= 10 && $data['dist'] == "")
{
    $copPres['rate'] = "6";
}

//rpmOscillationAnomalyAnalysis
$rcOcil = array();
$rcOcil['rate'] = 6;
$rcOcil1['msg'] = "";
$rcOcil['msg']  = "";
if($data['eng_block_ej'] == 1)
{
    $rcOcil['rate'] = "1";
    $rcOcil['con'] = "Critical";
    if($dataRep['fuel'] == "Petrol")
    {
        if($copPres['rate'] <= 2)
        {
            $rcOcil1['msg'] = "Engine block bore failure detected. Check Injector bank and ignition system too. If white/excessive tail-pipe smoke observed, possible bore failure.";
        }
        elseif($copPres['rate'] == 3)
        {
            $rcOcil1['msg'] = "Engine block bore deterioration detected. Check Injector bank and ignition system too. If white/excessive tail-pipe smoke observed, possible bore deterioration.";
        }
        elseif($copPres['rate'] == 4)
        {
            $rcOcil1['msg'] = "Injector clogging or spark plug fouling detected. Accordingly, use fuel system flush/injector additives, or replace spark plugs. Decarbonize the EGR system. Check throttle body and belt tensioner.";
        }
        else
        {
            $rcOcil1['msg'] = "Clean spark plugs and check Ignition system. Clean fuel system, use fuel system flush/injector additives. Clean throttle body. Check belt tensioner.";
        }
    }
    else 
    {
        if($copPres['rate'] <= 2)
        {
            $rcOcil1['msg'] = "Engine block bore failure detected. Check injector bank too. If white/excessive tail-pipe smoke observed, possible bore failure.";
        }
        elseif($copPres['rate'] == 3)
        {
            $rcOcil1['msg'] = "Engine block bore deterioration detected. Check injector bank too. If white/excessive tail-pipe smoke observed, possible bore deterioration.";
        }
        elseif($copPres['rate'] == 4)
        {
            $rcOcil1['msg'] = "Injector clogging detected. Use fuel system flush/injector additives. Decarbonize the EGR. Check belt tensioner.";
        }
        else
        {
            $rcOcil1['msg'] = "Clean fuel injectors. Use fuel system flush/injector additives. Check belt tensioner.";
        }
    }
}
elseif($data['eng_block_ej'] == 2)
{
    $rcOcil['rate'] = "2";
    $rcOcil['con'] = "Bad";
    if($dataRep['fuel'] == "Petrol")
    {
        if($copPres['rate'] <= 2)
        {
            $rcOcil1['msg'] = "Engine block bore deterioration detected. Check Injector bank and ignition system too. If white/excessive tail-pipe smoke observed, possible bore failure.";
        }
        elseif($copPres['rate'] == 3)
        {
            $rcOcil1['msg'] = "Injector or Ignition system issue detected. Please check respective wiring too.";
        }
        else
        {
            $rcOcil1['msg'] = "Clean spark plugs and use fuel system flush/injector additives. Clean throttle body. Consider checking belt tensioner";
        }
    }
    else 
    {
        if($copPres['rate'] <= 2)
        {
            $rcOcil1['msg'] = "Engine block bore deterioration detected. Check injector bank too. If white/excessive tail-pipe smoke observed, possible bore failure.";
        }
        elseif($copPres['rate'] == 3)
        {
            $rcOcil1['msg'] = "Injector clogging detected. Use fuel system flush/injector additives. Decarbonize the EGR. Check belt tensioner.";
        }
        else
        {
            $rcOcil1['msg'] = "Clean fuel injectors. Use fuel system flush/injector additives. Check belt tensioner.";
        }
    }
}
elseif($data['eng_block_ej'] == 3)
{
    $rcOcil['rate'] = "3";
    $rcOcil['con'] = "Okay";
    if($dataRep['fuel'] == "Petrol")
    {
        if($copPres['rate'] == 1)
        {
            $rcOcil1['msg'] = "Engine block bore deterioration or Injector clogging or spark plug fouling detected. Consider checking/servicing them.";
        }
        elseif(($copPres['rate'] == 2) || ($copPres['rate'] == 3))
        {
            $rcOcil1['msg'] = "Injector clogging or spark plug fouling detected. Accordingly, use fuel system flush/injector additives, or replace spark plugs. Decarbonize the EGR system. Check throttle body and belt tensioner.";
        }
        else
        {
            $rcOcil1['msg'] = "Clean spark plugs and use fuel system flush/injector additives. Consider checking throttle body and belt tensioner.";
        }
    }
    else 
    {
        if(($copPres['rate'] == 1) || ($copPres['rate'] == 2))
        {
            $rcOcil1['msg'] = "Engine block bore deterioration or Injector issue detected. Consider checking/servicing them.";
        }
        elseif($copPres['rate'] == 3)
        {
            $rcOcil1['msg'] = "Injector clogging detected. Use fuel system flush/injector additives. Decarbonize the EGR. Check belt tensioner.";
        }
        else
        {
            $rcOcil1['msg'] = "Clean fuel injectors. Use fuel system flush/injector additives. Consider checking belt tensioner.";
        }
    }
}
elseif($data['eng_block_ej'] == 4)
{
    $rcOcil['rate'] = "4";
    $rcOcil['con'] = "Okay-Good";
    if($dataRep['fuel'] == "Petrol")
    {
        if($copPres['rate'] == 1)
        {
            $rcOcil1['msg'] = "Clean or replace spark plugs. Use fuel system flush/injector additives. Consider decarbonising the EGR system.";
        }
        elseif(($copPres['rate'] == 2) || ($copPres['rate'] == 3))
        {
            $rcOcil1['msg'] = "Clean or replace spark plugs. Use fuel system flush/injector additives. Consider decarbonising the EGR system.";
        }
        else
        {
            $rcOcil1['msg'] = "Clean spark plugs. Consider fuel system cleaning. Use fuel system flush/injector additives.";
        }
    }
    else 
    {
        if($copPres['rate'] == 1)
        {
            $rcOcil1['msg'] = "Injector clogging detected. Use fuel system flush/injector additives. Decarbonize the EGR system.";
        }
        elseif(($copPres['rate'] == 2) || ($copPres['rate'] == 3))
        {
            $rcOcil1['msg'] = "Injector clogging detected. Use fuel system flush/injector additives. Decarbonize the EGR system.";
        }
        else
        {
            $rcOcil1['msg'] = "Consider fuel system flush/injector additives.";
        }
    }
}
elseif($data['eng_block_ej'] == 5)
{
    $rcOcil['rate'] = "5";
    $rcOcil['con'] = "Good";
    $rcOcil1['msg'] = "";
}

//falseOdometerReading
$falOdMr = array();
$falOdMr['rate'] = "6";
$falOdMr['con'] = "";
$falOdMr['msg'] = $data['falsemeter'];

//getVehSpeedSence
$vehSpeed = array();
$vehSpeed['rate'] = "6";
$vehSpeed['con'] = "";
$vehSpeed['msg'] = ($data['vehspeed'] <= 0) ? "Vehicle Speed Sensor Failure possibility" : "Vehicle Speed Sensor Okay";

//Coolant System
$coolTemp = array();
$coolTemp['rate'] = "6";
$coolTemp['msg'] = "";
if($data['coolp'] == 1)
{
    $coolTemp['rate'] = "1";
    $coolTemp['con'] = "Critical";
    $coolTemp['msg'] = "Engine cooling poor. Check Radiator Fan functioning. Check Coolant and Engine Oil Level. Consider spray washing the radiator. Visit the Manufacturer Dealership whenever possible.";
}
elseif($data['coolp'] == 2)
{
    $coolTemp['rate'] = "2";
    $coolTemp['con'] = "Bad";
    $coolTemp['msg'] = "Engine cooling inadequate. Check Radiator Fan functioning. Check Coolant and Engine Oil Level. Top-up Coolant and Engine Oil if low. Consider spray washing the radiator.";
}
elseif($data['coolp'] == 3)
{
    $coolTemp['rate'] = "3";
    $coolTemp['con'] = "Okay";
    $coolTemp['msg'] = "Engine cooling below average. Check Coolant and Engine Oil Level. Top-up Coolant and Engine Oil if low. Consider spray washing the radiator.";
}
elseif($data['coolp'] == 4)
{
    $coolTemp['rate'] = "4";
    $coolTemp['con'] = "Okay-Good";
    $coolTemp['msg'] = "Engine cooling average. Check Coolant and Engine Oil Level.";
}
elseif($data['coolp'] == 5)
{
    $coolTemp['rate'] = "5";
    $coolTemp['con'] = "Good";
    $coolTemp['msg'] = "";
}
else
{
    $coolTemp['msg'] = $data['coolm'];
}

//Following logic added to increase combustion pressure score if all other systems are having a good score.
if($copPres['rate'] < 3)
{
    if((($turbo['rate'] == 4 || $turbo['rate'] == 5) || ($air['rate'] == 4 || $air['rate'] == 5)) && $rcOcil['rate'] >= 4 && $fuel['rate'] >= 4)
    {
        $copPres['rate'] = 3;
    }
    else
    {
        //do nothing;
    }
}
else
{
    //do nothing;
}
//To update the score into the database
$compscore = $copPres['rate'];
$sqlUp     = "update device_calib set comp_pres = '$compscore' where id='$cali'";
$conn->query($sqlUp);

//combustion pressure message
if($copPres['rate'] == 5)
{
    $copPres2['msg'] = "";
}
elseif($copPres['rate'] <= 4)
{
    if($vType == 2)
    {
        if(($turbo['rate'] == 5 && $rcOcil['rate'] <= 4 && $fuel['rate'] == 5) || 
            ($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] > 5) || 
            ($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] == 5) || 
            ($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4) ||
            ($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] > 5) ||
            ($turbo['rate'] == 5 && $rcOcil['rate'] == 5 && $fuel['rate'] == 5) ||
            ($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4))
        {
            if($data['ocilv'] >= 1 && $rcOcil['rate'] <= 2)
            {
                //$copPres['msg'] = "Your engine's combustion pressures are not good. This is attributed to the engine's compression pressure 
                //which may be due to engine block bore deterioration or an injector issue.";
                $copPres2['msg'] = "This is attributed to the engine's compression pressure which may be due to engine block bore deterioration 
                or an injector issue. If white/excessive tail-pipe smoke observed, possible bore failure.";
            }
            else 
            {
                $n = array();

                $n = array($turbo['rate'],$fuel['rate']);
                $nVal = min($n);

                if($n[0] == $n[1])
                {
                    if($nVal <= 3)
                    {
                        $copPres2['msg'] = "Please consider checking the High Pressure Fuel System and/or Air System.";
                    }
                    elseif($nVal == 5)
                    {
                        $copPres2['msg'] = "Please consider checking/cleaning the EGR System.";
                    }
                    else
                    {
                        //do nothing;
                    }
                }
                else
                {
                    if($fuel['rate'] <= 0 || $fuel['rate'] > 5)
                    {
                        if($n[0] == 5)
                        {   
                            //$copPres['msg'] = "Your engine's combustion pressures are not good.";
                            $copPres2['msg'] = "We could not check the High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
                        }
                        else 
                        {
                            $copPres2['msg'] = "Please consider checking the Turbocharger System.";
                        }
                    }
                    else 
                    {
                        if($n[0] > $n[1])
                        {
                            $copPres2['msg'] = "Please consider checking the High Pressure Fuel System.";
                        }
                        else
                        {
                            $copPres2['msg'] = "Please consider checking the Turbocharger System.";
                        }
                    }
                }
            }
        }
        else
        {
            //do nothing
        }
    }
    else
    {
        if($Turbo_Avail_C == "No")
        {
            if(($air['rate'] == 5 && $rcOcil['rate'] <= 4) || ($air['rate'] <= 4 && $rcOcil['rate'] <= 5) || 
            ($air['rate'] <= 4 && $rcOcil['rate'] <= 5) || ($air['rate'] == 5 && $rcOcil['rate'] <= 5) || 
            ($air['rate'] <= 4 && $rcOcil['rate'] <= 5))
            {
                if($copPres['rate'] <= 3 && $rcOcil['rate'] <= 1)
                {
                    $copPres2['msg'] = "This is attributed to the engine's compression pressure which may be due to 
                    engine block bore deterioration or there may be an injector issue. If white/excessive tail-pipe smoke observed, 
                    possible bore failure.";
                }
                else
                {
                    if($air['rate'] <= 4)
                    {
                        if($rcOcil['rate'] <= 4 && $air['rate'] == 4)
                        {
                            $copPres2['msg'] = "Please consider checking the Air System and fuel injectors.";
                        }
                        else
                        {
                            $copPres2['msg'] = "Please consider checking the Air System.";
                        }
                    }
                    elseif($rcOcil['rate'] <= 4)
                    {
                        $copPres2['msg'] = "Please consider checking the fuel injectors and spark plugs.";
                        //do nothing;
                        //$copPres['msg'] = "Your engine's combustion pressures are not good. We could not check your High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
                        //$copPres['msg'] = "Combustion pressure build up average. Consider General Engine Service";
                    }
                }
            }
        }
        else
        {
            if(($turbo['rate'] == 5 && $rcOcil['rate'] <= 4 && $fuel['rate'] == 5) || 
                ($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] > 5) || 
                ($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] == 5) || 
                ($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4) ||
                ($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] > 5) ||
                ($turbo['rate'] == 5 && $rcOcil['rate'] == 5 && $fuel['rate'] == 5) ||
                ($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4))
            {
                if($copPres['rate'] <= 3 && $rcOcil['rate'] <= 2)
                {
                    //$copPres['msg'] = "Your engine's combustion pressures are not good. This is attributed to the engine's compression pressure 
                    //which may be due to engine block bore deterioration or an injector issue.";
                    $copPres2['msg'] = "This is attributed to the engine's compression pressure which may be due to engine block bore deterioration 
                    or an injector issue. If white/excessive tail-pipe smoke observed, possible bore failure.";
                }
                else 
                {
                    $n = array();
                    $n = array($turbo['rate'],$fuel['rate']);
                    $nVal = min($n);
    
                    if($n[0] == $n[1])
                    {
                        if($nVal <= 3)
                        {
                            $copPres2['msg'] = "Please consider checking the High Pressure Fuel System and/or the Turbocharger System.";
                        }
                        elseif($nVal == 5)
                        {
                            $copPres2['msg'] = "Please consider checking/cleaning the EGR System.";
                        }
                        else
                        {
                            //do nothing;
                        }
                    }
                    else
                    {
                        if($fuel['rate'] <= 0 || $fuel['rate'] > 5)
                        {
                            if($n[0] == 5)
                            {   
                                //$copPres['msg'] = "Your engine's combustion pressures are not good.";
                                $copPres2['msg'] = "We could not check the High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
                            }
                            else 
                            {
                                $copPres2['msg'] = "Please consider checking the Turbocharger System.";
                            }
                        }
                        else 
                        {
                            if($n[0] > $n[1])
                            {
                                $copPres2['msg'] = "Please consider checking the High Pressure Fuel System.";
                            }
                            else
                            {
                                $copPres2['msg'] = "Please consider checking the Turbocharger System.";
                            }
                        }
                    }
                }
            }
            else
            {
                //do nothing
            }
        }
    }
}

//if($copPres['msg'] == "")
//{
    $copPres[0] = $copPres['rate'];
    if($copPres['rate'] == 5)
    {
        $copPres1['msg'] = "";
        $copPres3['msg'] = "";
    }
    elseif($copPres['rate'] == 4)
    {
        $copPres1['msg'] = "Combustion pressure build-up average.";
        $copPres3['msg'] = "Consider general engine service.";
    }
    elseif($copPres['rate'] == 3)
    {
        $copPres1['msg'] = "Combustion pressure build-up below peak capability.";
        $copPres3['msg'] = "";
    }
    elseif($copPres['rate'] == 2)
    {
        $copPres1['msg'] = "Combustion pressure build-up low.";
        $copPres3['msg'] = "Visit the Manufacturer Dealership whenever possible.";
    }
    elseif($copPres['rate'] == 1)
    {
        $copPres1['msg'] = "Combustion pressure build-up critical.";
        $copPres3['msg'] = "Visit the Manufacturer Dealership whenever possible.";
    }else{
        $copPres1['msg'] = "";
        $copPres3['msg'] = "";
    }
    
    if(empty($copPres2['msg'])){
        $copPres2['msg'] = "";
    }
    
    $copPres['msg'] = $copPres1['msg'].$copPres2['msg'].$copPres3['msg'];
    //print_r($copPres['msg'].$copPres2['msg'].$copPres3['msg']);


//All systems in good health
$goodhealth['msg'] = "";
if($batHel['rate'] == 5 && $rcOcil['rate'] == 5 && $copPres['rate'] == 5 && (($fuel['rate'] == 5 && $turbo['rate'] == 5) ||
($fuel['rate'] <= 0 && $turbo['rate'] == 5) || ($air['rate'] == 5)) && $coolTemp['rate'] == 5)
{
    $goodhealth['msg'] = "All Engine's Systems Healthy.";
}

//Tuning Detection
$tuning['msg'] = $data['tune_msg'];

$varMSG = "";
$varMSG .= ($batHel['msg'] != "" && strlen($batHel['msg']) >= 2) ? "<li>Battery System : ".$batHel['msg']."</li>" : "";
$varMSG .= ($rcOcil['msg'] != "" && strlen($rcOcil['msg']) >= 2) ? "<li>Rpm Oscillation Anomaly Analysis : ".$rcOcil['msg']."</li>" : "";
$varMSG .= ($copPres['msg'] != "" && strlen($copPres['msg']) >= 2) ? "<li>Combustion Pressure : ".$copPres['msg']."</li>" : "";
$varMSG .= ($fuel['msg'] != "" && strlen($fuel['msg']) >= 2) ? "<li>Fuel System : ".$fuel['msg']."</li>" : "";

$vTypeTxt = "Turbocharger System";
$vTypeVal = "";
if($vType == 1)
{
    if($Turbo_Avail_C == "No")
    {
        $varMSG .= ($air['msg'] != "" && strlen($air['msg']) >= 2) ? "<li>Air System : ".$air['msg']."</li>" : "";
        $vTypeTxt = "Air System";
        $vTypeVal = $air['rate'];
    }
    else
    {
        $varMSG .= ($turbo['msg'] != "" && strlen($turbo['msg']) >= 2) ? "<li>Turbocharger System : ".$turbo['msg']."</li>" : "";
        $vTypeTxt = "Turbocharger System";
        $vTypeVal = $turbo['rate'];
    }
}
else 
{
    $varMSG .= ($turbo['msg'] != "" && strlen($turbo['msg']) >= 2) ? "<li>Turbocharger System : ".$turbo['msg']."</li>" : "";
    $vTypeTxt = "Turbocharger System";
    $vTypeVal = $turbo['rate'];
}

$varMSG .= ($coolTemp['msg'] != "" && strlen($coolTemp['msg']) >= 2) ? "<li>Coolant System : ".$coolTemp['msg']."</li>" : "";

$varMSG .= ($falOdMr['msg'] != "" && strlen($falOdMr['msg']) >= 2) ? "<li>".$falOdMr['msg']."</li>" : "";
$varMSG .= ($vehSpeed['msg'] != "" && strlen($vehSpeed['msg']) >= 2) ? "<li>".$vehSpeed['msg']."</li>" : "";
$varMSG .= ($goodhealth['msg'] != "" && strlen($goodhealth['msg']) >= 2) ? "<li>".$goodhealth['msg']."</li>" : "";
$varMSG .= ($tuning['msg'] != "" && strlen($tuning['msg']) >= 2) ? "<li>".$tuning['msg']."</li>" : "";

$varMSG .= (!empty($veh[0]['dtcv']) && strlen($veh[0]['dtcv']) >= 2) ? "<li>DTCs Present : ".$dataRep['dtc']."</li>" : "";
$fname = "";
if(!empty($dataRep['type'])){
  $fname = str_replace(" ", "-", $dataRep['type']);
}
$fname = str_replace(":", "-", $fname)."-".$dataRep['devid'];
$fname = ($dataRep['id'] != "") ? $fname.".pdf" : $dataRep['devid']."-".$dataRep['id'].".pdf";

$url = url('/')."/public";
$content = "<table width='100%'>
            <!--<tr>
                <td align='center'> <h2>Engine Monetring Report</h2> </td>
            </tr>-->
            <tr>
                <td>
                    <table width='100%'>
                        <tr>
                            <td width='30%' valign='top'> 
                                <div style='border:1px solid #000; margin-right: 5px; border-radius: 5px; padding: 5px; width: 95%; vertical-align: middle; '>
                                    <table width='100%'>
                                        <tr>
                                            <td valign='center' align='center' width='10%'><img src='$url/images/img/name1.png'></td>
                                            <td valign='center' align='center' width='90%' ><h6>".$dataRep['cust']."</h6></td>
                                        </tr>
                                    </table>
                                </div>  
                            </td>
                            <td width='30%'> 
                                <div style='border:1px solid #000; margin-right: 5px; border-radius: 5px; padding: 5px; width: 95%; vertical-align: middle;'>
                                    <table width='100%'>
                                    <tr>
                                        <td valign='center' align='center' width='10%'><img src='$url/images/img/name2.png'></td>
                                        <td valign='center' align='center' width='90%' ><h6>".$dataRep['mfd']." ".$dataRep['model']."</h6></td>
                                    </tr>
                                    </table>
                                </div>  
                            </td>
                            <td width='30%'> 
                                <div style='border:1px solid #000; margin-right: 5px; border-radius: 5px; padding: 5px; width: 95%; vertical-align: middle;'>
                                    <table width='100%'>
                                    <tr>
                                        <td valign='center' align='center' width='10%'><img src='$url/images/img/name3.png'></td>
                                        <td valign='center' align='center' width='90%' ><h6>".date('d-m-Y',strtotime($dataRep['dt']))."</h6></td>
                                    </tr>
                                    </table>
                                </div>  
                            </td>
                        </tr>
                        <tr>
                            <td width='30%'> 
                                <div style='border:1px solid #000; margin-right: 5px; border-radius: 5px; padding: 5px; width: 95%;'>
                                    <table width='100%'>
                                    <tr>
                                        <td valign='center' align='center' width='10%'><img src='$url/images/img/name4.png'></td>
                                        <td valign='center' align='center' width='90%' ><h6>".$dataRep['regno']."</h6></td>
                                    </tr>
                                    </table>
                                </div>  
                            </td>
                            <td width='30%'> 
                                <div style='border:1px solid #000; margin-right: 5px; border-radius: 5px; padding: 5px; width: 95%;'>
                                    <table width='100%'>
                                    <tr>
                                        <td valign='center' align='center' width='10%'><img src='$url/images/img/name5.png'></td>
                                        <td valign='center' align='center' width='90%' ><h6>".$dataRep['atm']."</h6></td>
                                    </tr>
                                    </table>
                                </div>  
                            </td>
                            <td width='30%'> 
                                <div style='border:1px solid #000; margin-right: 5px; border-radius: 5px; padding: 5px; width: 95%;'>
                                    <table width='100%'>
                                    <tr>
                                        <td valign='center' align='center' width='10%'><img src='$url/images/img/name6.png'></td>
                                        <td valign='center' align='center' width='90%' ><h6>".$dataRep['odomet']."</h6></td>
                                    </tr>
                                    </table>
                                </div>  
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width='100%' cellspacing='15' >
                        <tr>
                            <td width='25%' style='border:1px solid #000; margin: 2px 2px 2px 2px; text-align: center;'>
                                <img width='200' src='$url/images/img/".$batHel['rate'].".jpg'>
                                <h5><span>Battery System</span></h5>
                            </td>
                            <td width='25%' style='border:1px solid #000; margin: 2px 2px 2px 2px; text-align: center;'>
                                <img width='200' src='$url/images/img/".$rcOcil['rate'].".jpg'>
                                <h5><span>RPM Oscillation Analysis</span></h5>
                            </td>
                            <td width='25%' style='border:1px solid #000; margin: 2px 2px 2px 2px; text-align: center;'>
                                <img width='200' src='$url/images/img/".$copPres['rate'].".jpg'>
                                <h5><span>Combustion Pressure</span></h5>
                            </td>
                            <td width='25%' style='border:1px solid #000; margin: 2px 2px 2px 2px; text-align: center;'>
                                <img width='200' src='$url/images/img/".$fuel['rate'].".jpg'>
                                <h5><span>Fuel System</span></h5>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <table width='100%' cellspacing='15'>
                        <tr>
                            <td width='25%' style='border:1px solid #000; margin: 2px 2px 2px 2px; text-align: center;'>
                                <img width='200' src='$url/images/img/".$vTypeVal.".jpg'>
                                <h5><span>".$vTypeTxt."</span></h5>
                            </td>
                            <td width='50%' style='border:1px solid #000; margin: 2px 2px 2px 2px; vertical-align: top;'>
                                <div style=' background-color: lightblue; width: 100%; font-weight: bold;'>Extra Parameters / Comments:</div>
                                <div>
                                    <ol>
                                        $varMSG
                                    </ol>                       
                                </div>
                            </td>
                            
                            <td width='25%' style='border:1px solid #000; margin: 2px 2px 2px 2px; text-align: center;'>
                                <img width='200' src='$url/images/img/".$coolTemp['rate'].".jpg'>
                                <h5><span>Coolant System</span></h5>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>";

$html ="<html>
<head>
  <style>
    @page { margin: 50px 20px; }
    #header { font-family: Arial, Helvetica, sans-serif; position: fixed; left: 0px; top: -60px; right: 0px; height: 60px;  border-bottom: 1px solid #000; }
    #footer { position: fixed; left: 0px; bottom: 10px; right: 0px; font-size: 10px; }
    #footer p {font-size: 10px;}
    #footer .page:after { content: counter(page, upper-roman);  }
    #content{font-family: Arial, Helvetica, sans-serif;}
    .hed{color:#CCCCCC; text-align:center; padding-top:20px;}
    p{font-family: Arial, Helvetica, sans-serif; font-size:13px; margin: 2px 0px 2px 0px;}
    div{font-size:13px;}
    td{font-size:13px;}
    body{font-size:13px;}
    h5{font-size:14px; margin: 0px; padding:2px; padding-bottom:5px;}
    h6{font-size:15px; margin: 0px; padding:0px; padding-top:2px; font-weight: normal;}
    h2{font-size:26px; margin-top: 0px; padding-top:0px;}
    span{border:1px solid #000; padding:2px;}
    ol{font-size:12px; margin: 2px 0px 2px 0px; padding:0px 2px 0px 25px;}
  </style>
<body>
  <div id='header'>
    <table width='100%'>
    <tr>
        <td width='30%'><img height='40' src='$url/images/img/insight.jpg'> </td>
        <td width='60%' class='hed'><h2>Engine Monitoring Report</h2></td>      
        <td width='30%' style='text-align: right;'><img height='40' src='$url/images/img/logo.jpg'></td>
    </tr>
    </table>
  </div>
  <div id='content'>
    <p style='padding-top:5px;'>
        $content        
    </p>  
  </div>
  <div id='footer'>
    <p>
    <table width='100%'>
    <tr>
        <td width='85%' style='padding:0px 1px 0px 1px;'><strong>Confidential. All rights and IP held by EngineCAL.</strong></td> 
        <td width='10%' style='text-align:center; padding:0px 1px 0px 1px;'><strong>Page 1</strong></td><br/>
    </tr>
    <tr>
        <td width='100%' style='text-align:left; font-size:9.5px;'>
        Disclaimer: This document contains the engine output data of the car for the environmental conditions on that particular date and time,<!-- all stated above,--> using engine specific advanced propriety Software Programs developed by EngineCAL.<br/>
        <!--0-3:Overhaul/Component Failure; 4-5:System/Systems performance below average. Major Repair/Component failure forseen; 6-7:Specific Repair/Specific Service required; 8-9:Good car. General Service (All filters clean/replace, all fluids top-up/replace, spark plug clean/replace, wheel alignment etc.) required; 10:Great car.-->
        </td>
    </tr>  
    </table>
    </p>
  </div>
  </body>
</html>";
}else{
    $html = "No Data Present in Table.";
}
echo $html;
/*------------------------------------------------------------------------------------------*/

function getdriver($deviceid,$conn)
{
     $sql = "select u.* from users as u, device as d where d.id=u.device and d.device_id='$deviceid'";
     $data = $conn->query($sql);
     return $data->fetch_array();
}

function getvehicle($deviceid,$conn,$vehSett,$vehicle)
{
         

         $pDVar = ($vehicle['fuel_tpe'] == 1) ? "p" : "d";
         $f1 = "co2fact$pDVar";
         $vehicle['co2fact']   = getCaliPramVal('Co2_Cnv_Fac_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['oill_day']  = getCaliPramVal('Oil_Life_Days_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['oill_dist'] = getCaliPramVal('Oil_Life_Kms_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['dust_env']  = getCaliPramVal('DustEnv_Status_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['nfact']     = getCaliPramVal('Eng_eff_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['ve']        = getCaliPramVal('Vol_eff_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['ed']        = getCaliPramVal('Eng_Disp_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['int_cool']  = getCaliPramVal('IC_Status_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['egr_cool']  = getCaliPramVal('EgrC_Status_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['egr_cmd']   = getCaliPramVal('Egr_Status_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['lmt1']      = getCaliPramVal('Eff_RPM_Limit1_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['lmt2']      = getCaliPramVal('Eff_RPM_Limit2_C',$deviceid,$vehSett,$vehicle,$conn);
         $vehicle['lmt3']      = getCaliPramVal('Eff_RPM_Limit3_C',$deviceid,$vehSett,$vehicle,$conn);
         return array($vehicle,$vehSett);
}

function getCaliPramVal($param,$deviceID,$vehSett,$vehicle,$conn)
{
     
     $pDVar = ($vehicle['fuel_tpe'] == 1) ? "p" : "d";
     $f1 = "calval$pDVar";
     $vsetVal = json_decode($vehSett[$f1],true);
     $calval = json_decode($vehicle['calval'],true);
     
     $val = trim($param); 
     $retVal = (trim($calval[$val]) != "") ? $calval[$val] : $vsetVal[$val];
     return $retVal;
     //return array($vehicle,$vehSett);
}

?>
