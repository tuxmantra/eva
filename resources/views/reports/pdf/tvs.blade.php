<?php $url = url('/')."/public"; ?>

<htm>
<head>
<title>Test</title>

<link href="{{url('/')}}/public/tvscss/bootstrap.min.css" rel="stylesheet">
<style>

body{
    font-size: 14px;
}

.table-borderless tbody tr td{
    border: none;
}

.table-two tbody tr td {
    width: 50%;
}

.page-break {
    page-break-after: always;
}

.left {
    left: 0;
    -webkit-transform-origin: 0 50%;
       -moz-transform-origin: 0 50%;
        -ms-transform-origin: 0 50%;
         -o-transform-origin: 0 50%;
            transform-origin: 0 50%;
    -webkit-transform: rotate(-90deg) translate(-50%, 50%);
       -moz-transform: rotate(-90deg) translate(-50%, 50%);
        -ms-transform: rotate(0deg) translate(-50%, 50%);
         -o-transform: rotate(-90deg) translate(-50%, 50%);
            transform: rotate(-90deg) translate(-50%, 50%);
}

.right {
    right: 0;
    -webkit-transform-origin: 0 50%;
       -moz-transform-origin: 0 50%;
        -ms-transform-origin: 0 50%;
         -o-transform-origin: 0 50%;
            transform-origin: 0 50%;
    -webkit-transform: rotate(-90deg) translate(-50%, 50%);
       -moz-transform: rotate(-90deg) translate(-50%, 50%);
        -ms-transform: rotate(0deg) translate(-50%, 50%);
         -o-transform: rotate(-90deg) translate(-50%, 50%);
            transform: rotate(-90deg) translate(-50%, 50%);
}

.td-align {
    margin-left: -301px;
    padding-left: 193px;
    margin-right: -54px;
    margin-top: -180px;
    font-size: 10px;
}

.td-align1 {
    margin-left: -14px;
    /*padding-left: 0px;*/
    margin-right: 7px;
    /*transform: rotate(-90deg);*/
    /*padding-right: 3px;*/
}

.td-align2 {

    margin-left: -15px;
    /*margin-top: 10px;*/
    margin-right: -12px;
 /*   transform: rotate(-90deg);*/
    /*padding-right: -4px;*/
}

.heat-map2 tbody tr td {
    /*padding-top: 2px;
    padding-bottom: 2px;*/
    text-align: center;
    /*padding-right: 2px;
    padding-left: 0px;*/
    width: 10%;
    font-size: 8.5px;
}

.heat-map1{
    text-align: center;
}

.no-padding {
    padding: 0 !important;
}

.no-padding img {
    height: 70px !important;
}

.scale-icon {
    padding: 0 3em;
}

.icon-box {
    padding: 20px;
    border: #cdcdca 1px solid;
}

.table-header {
    font-size: 15px !important;
    font-weight: bold !important;
    padding-left: 200px !important;
}

</style>

</head>
<body>
 
<div class="container">
<!-- Container Start -->  

<div class="row">
	<div style="padding: 50px; padding-top: 5px !important;"> 
	    <div style="width: 100%">
		    <div style="float: left; text-align: center; width: 20%;">
		        <!-- <img src="<?= $url ?>/images/img/1-TVS.png"> -->
		    </div>
		    <div style="float: left; text-align: center; width: 60%; margin-top: -10px;">
		        <h1><u>Customer Usage Analysis Report</u></h1>
		    </div>
		    <div style="float: left; text-align: center; width: 20%;">
		         <!-- <img src="<?= $url ?>/images/img/2-TVS.png"> -->
		    </div>
		</div>
	</div>
</div>

<!--------------------First Section Start -------------->
<div class="container">
<div class="row">
    <div class="col-md-12" style="height: 30px; background-color: grey; padding-left: 30px; text-align: left; font-size: 15px; color: #fff; font-weight: bold; width: 100%;">
        <p style="float: left; padding-top:3px;">Vehicle Details</p>
    </div>
    
    <div class="col-md-12">
    <table class="table table-borderless" style="margin-bottom: 30px;">
        <tr>
            <td>
                <table class="table table-borderless table-two">
                    <tr >
                        <td style="padding:0; margin: 0;" >Model</td>
                        <td style="padding:0; margin: 0;" >: <?php if($model != ""){ echo $model; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Vehicle Reg Number</td>
                        <td style="padding:0; margin: 0;" >: <?php if($registration != ""){ echo $registration; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Date of Sale</td>
                        <td style="padding:0; margin: 0;" >: <?php if($date_purchase != ""){ echo date('Y-M-d', strtotime($date_purchase)); }else{ echo " -"; } ?></td>
                    </tr>
                </table>
            </td>
            <td>
               <table class="table table-borderless table-two">
                    <tr >
                        <td style="padding:0; margin: 0;" >Variant</td>
                        <td style="padding:0; margin: 0;" >: <?php if($varient != ""){ echo $varient; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Chassis Number</td>
                        <td style="padding:0; margin: 0;" >: <?php if($chassis_num != ""){ echo $chassis_num; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Odometer</td>
                        <td style="padding:0; margin: 0;" >: <?php if($odometer != ""){ echo $odometer." km"; }else{ echo " -"; } ?></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-borderless table-two">
                    <tr >
                        <td style="padding:0; margin: 0;" >Engine CC</td>
                        <td style="padding:0; margin: 0;" >: <?php if($engine_cc != ""){ echo $engine_cc." CC"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Engine Number</td>
                        <td style="padding:0; margin: 0;" >: <?php if($engine_num != ""){ echo $engine_num; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;">Last Service Date</td>
                        <td style="padding:0; margin: 0;" >: <?php if($last_service != ""){ echo date('Y-M-d', strtotime($last_service)); }else{ echo " -"; } ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>  
    </div>

</div>
</div>
<!--------------------First Section End -------------->
<br><br><br><br><br>
<!--------------------Second Section Start -------------->
<div class="container">
<div class="row">
    <div class="col-md-12" style="height: 30px; background-color: grey; margin-top: 40px; padding-left: 30px; text-align: left; font-size: 15px; color: #fff; font-weight: bold; width: 100%;">
        <p style="float: left; font-weight:bold">Drive Summary</p>
    </div>

    <div class="col-md-12">
    <table class="table table-borderless">
        <tr>
            <td >
                <table class="table table-borderless table-two">
                    <tr >
                        <td style="padding:0; margin: 0;">Date</td>
                        <td style="padding:0; margin: 0;">: <?php if($date_summery != ""){ echo $date_summery; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0; font-size: 15px; font-weight: bold;" >Mileage</td>
                        <td style="padding:0; margin: 0; font-size: 15px; font-weight: bold;" >: <?php if($milage != ""){ echo $milage." kmpl"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" ></td>
                    </tr>
                </table>
            </td>
            <td>
               <table class="table table-borderless table-two">
                    <tr >
                        <td style="padding:0; margin: 0;" >Distance</td>
                        <td style="padding:0; margin: 0;" >: <?php if($distance != ""){ echo round($distance,2)." km"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" ></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" ></td>
                    </tr>
                </table>
            </td><td>
                <table class="table table-borderless table-two">
                    <tr >
                        <td style="padding:0; margin: 0;" > Ignition On Duration</td>
                        <td style="padding:0; margin: 0;" >: <?php if($duration != ""){ echo gmdate('H:i:s',$duration); }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" > Engine On Duration</td>
                        <td style="padding:0; margin: 0;" >: <?php if($run_dur != ""){ echo gmdate('H:i:s',$run_dur); }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" ></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table> 
    </div>
</div>
</div>     
<!--------------------Second Section End -------------->
<br><br><br><br>
<!--------------------Third Section Start -------------->
<div class="container">
<div class="row">
    <div class="col-md-12" style="height: 30px; background-color: grey; margin-top: 40px; padding-left: 30px; text-align: left; font-size: 15px; color: #fff; font-weight: bold; width: 100%;">
        <p style="float: left;">Vehicle Usage Statistics</p>
    </div>
    <div class="col-md-12">
    <table class="table table-borderless">
        <tr>
            <td>
                <table class="table table-borderless table-two">
                    <tr >
                        <td style="padding:0; margin: 0;" >Avg Distance</td>
                        <td style="padding:0; margin: 0;" >: <?php if($avg_distance != ""){ echo round($avg_distance,1)." km/day"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Max Distance</td>
                        <td style="padding:0; margin: 0;" >: <?php if($max_distance != ""){ echo round($max_distance,1)." km in a day"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Min Distance</td>
                        <td style="padding:0; margin: 0;" >: <?php if($min_distance != ""){ echo round($min_distance,1)." km in a day"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Avg Speed</td>
                        <td style="padding:0; margin: 0;" >: <?php if($avg_speed != ""){ echo round($avg_speed,1)." kmph "; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Max Speed</td>
                        <td style="padding:0; margin: 0;" >: <?php if($max_speed != ""){ echo round($max_speed)." kmph"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >StdDev Speed</td>
                        <td style="padding:0; margin: 0;" >: <?php if($stdDevVehSpd != ""){ echo round($stdDevVehSpd)." kmph"; }else{ echo " -"; } ?></td>
                    </tr>
                </table>
            </td>
            <td>
               <table class="table table-borderless table-two">
                    <tr >
                        <td style="padding:0; margin: 0;" >Idle Time</td>
                        <td style="padding:0; margin: 0;" >: <?php echo round($idle_time)." %"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Acceleration Time</td>
                        <td style="padding:0; margin: 0;" >: <?php if($accelaration_time != ""){ echo round($accelaration_time)." %"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr style="border-bottom :none !important;">
                        <td style="padding:0; margin: 0;" >Cruise Time</td>
                        <td style="padding:0; margin: 0;" >: <?php if($cruise_time != ""){ echo round($cruise_time)." %"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr style="border-top :none !important;" >
                        <td style="padding:0; margin: 0;" >Braking Time</td>
                        <td style="padding:0; margin: 0;" >: <?php if($brake_time != ""){ echo round($brake_time)." %"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" ></td>
                    </tr>
                </table>
            </td><td>
                <table class="table table-borderless table-two">
                     <tr >
                        <td style="padding:0; margin: 0;" >No. of Engine Starts</td>
                        <td style="padding:0; margin: 0;" >: <?php if($duration != ""){ echo $no_starts." /100 km"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Avg Cranks</td>
                        <td style="padding:0; margin: 0;" >: <?php if($avg_cranks != ""){ echo round($avg_cranks)." /start"; }else{ echo " 0"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Max Cranks</td>
                        <td style="padding:0; margin: 0;" >: <?php if($max_cranks != ""){ echo $max_cranks." /start"; }else{ echo " 0"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Cranks > 3 to start</td>
                        <td style="padding:0; margin: 0;" >: <?php if($crankGreater != ""){ echo $crankGreater." /100 km"; }else{ echo " 0"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >No. of Stalls</td>
                        <td style="padding:0; margin: 0;" >: <?php if($duration != ""){ echo $no_stalls." /100 km"; }else{ echo " 0"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table> 
    </div>
</div>
</div>     
<!--------------------Third Section End -------------->
<br><br><br><br><br><br><br><br>
<!--------------------Fourth Section Start -------------->
<div class="container">
<div class="row">
    <div class="col-md-12" style="height: 30px; background-color: grey; margin-top: 40px; padding-left: 30px; text-align: left; font-size: 15px; color: #fff; font-weight: bold; width: 100%;">
        <p style="float: left;">Engine Usage Statistics</p>
    </div>
    <div class="col-md-12">
    <table class="table table-borderless">
        <tr>
            <td>
                <table class="table table-borderless">
                    <tr>
                        <td style="padding:0; margin: 0;">Avg RPM</td>
                        <td  style="padding:0; margin: 0;">: <?php if($avg_rpm != ""){ echo $avg_rpm; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Max RPM</td>
                        <td style="padding:0; margin: 0;" >: <?php if($max_rpm != ""){ echo $max_rpm; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td  style="padding:0; margin: 0;">Min RPM</td>
                        <td style="padding:0; margin: 0;" >: <?php if($min_rpm != ""){ echo $min_rpm; }else{ echo " -"; } ?></td>
                    </tr>
                     <tr >
                        <td style="padding:0; margin: 0;" >StdDev RPM</td>
                        <td style="padding:0; margin: 0;" >: <?php if($stdDevRpm != ""){ echo $stdDevRpm; }else{ echo " -"; } ?></td>
                    </tr>
                     <tr >
                        <td  style="padding:0; margin: 0;">Rev Limit Crossing</td>
                        <td style="padding:0; margin: 0;" >: <?php if($lmt_cross != ""){ echo $lmt_cross." /100 km"; }else{ echo " 0"; } ?></td>
                    </tr>
                     <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" ></td>
                    </tr>
                </table>
            </td>
            <td>
               <table class=" table table-borderless">
                    <tr>
                        <td style="padding:0; margin: 0;" >Avg Throttle</td>
                        <td style="padding:0; margin: 0;" >: <?php if($avg_throttle != ""){ echo round($avg_throttle)." %"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Max Throttle</td>
                        <td style="padding:0; margin: 0;" >: <?php if($max_throttle != ""){ echo round($max_throttle)." %"; }else{ echo " -"; } ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Min Throttle</td>
                        <td style="padding:0; margin: 0;" >: <?php if($min_throttle != ""){ echo round($min_throttle)." %"; }else{ echo " -"; } ?></td>
                    </tr>
                     <tr >
                        <td style="padding:0; margin: 0;" >StdDev Throttle</td>
                        <td style="padding:0; margin: 0;" >: <?php if($stdDevThrottle != ""){ echo round($stdDevThrottle)." %"; }else{ echo " -"; } ?></td>
                    </tr>
                     <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" ></td>
                    </tr>
                     <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" ></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-borderless">
                  <tr>
                        <td  style="padding:0; margin: 0;">Power Delivery</td>
                        <td  style="padding:0; margin: 0;">: <?php echo $power_del." KW/km"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Torque Delivery</td>
                        <td style="padding:0; margin: 0;" >: <?php echo $torque_del." Nm/km"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Max Power</td>
                        <td style="padding:0; margin: 0;" >: <?php if($max_power != ""){ echo $max_power." KW"; }else{ echo " -"; } ?></td>
                    </tr>
                     <tr >
                        <td style="padding:0; margin: 0;" >Max Torque (corrected)</td>
                        <td style="padding:0; margin: 0;" >: <?php if($max_torque != ""){ echo $max_torque." Nm"; }else{ echo " -"; } ?></td>
                    </tr>
                     <tr >
                        <td style="padding:0; margin: 0;" >Avg Coolant Temp</td>
                        <td style="padding:0; margin: 0;" >: <?php if($avg_cool != ""){ echo $avg_cool." C"; }else{ echo " -"; } ?></td>
                    </tr>
                     <tr >
                        <td style="padding:0; margin: 0;" >Max Coolant Temp</td>
                        <td style="padding:0; margin: 0;" >: <?php if($max_cool != ""){ echo $max_cool." C"; }else{ echo " -"; } ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>  
    </div>
</div>
</div>    
<!--------------------Fourth Section End -------------->
<br><br><br><br><br><br><br><br><br>
<!--------------------Fifth Section Start -------------->
<?php if(!empty($table1) && !empty($table2)){ ?>
<div class="container">
<div class="row">
	<?php if(!empty($table1)){ ?>
    <div class="col-md-6" style="width: 48%; float: left; font-size: 8.5px; padding: 4px;">
        <!--<table class="table table-borderless heat-map1">-->
            <!-- <p style="height: 0; widows: 0; padding: 0; margin: 0; transform: rotate(0deg); ">Vehicle Speed Change(kmph/sec) </p> -->
        <table class="table table-borderless heat-map1">  
            <tr>
                <td class="left" rowspan="15"><h6><b>Acceleration (m/s²)</b></h6></td>
                <td class="text-center table-header" colspan="11">Vehicle Speed (kmph)</td>
            </tr>
            <tr>
                <td></td>
                <td>10</td>
                <td>20</td>
                <td>30</td>
                <td>40</td>
                <td>50</td>
                <td>60</td>
                <td>70</td>
                <td>80</td>
                <td>90</td>
                <td>>90</td>
                <td><p class="td-align1">Time %</p></td>
                <td><p class="td-align2">Time %<br>(cum)</p></td>
            </tr>
            
            <?php foreach($table1 as $ley => $tb){ ?>
            <tr>
                <td style="text-align: center;">
                <?php
	                if($ley == 0){
	                   echo "&gt;5";
	                }elseif($ley == 1){
	                   echo "3to5";
	                }elseif($ley == 2){
	                   echo "2to3";
	                }elseif($ley == 3){
	                   echo "1to2";
	                }elseif($ley == 4){
	                   echo ".15to1";
	                }elseif($ley == 5){
	                   echo "-.15to.15";
	                }elseif($ley == 6){
	                   echo "-.15to-1";
	                }elseif($ley == 7){
	                   echo "-1to-2";
	                }elseif($ley == 8){
	                   echo "-2to-3";
	                }elseif($ley == 9){
	                   echo "-3to-5";
	                }elseif($ley == 10){
	                   echo "&lt;-5";
	                }
                ?>
                </td>
                <?php $tot = array(); ?>
                <?php foreach($tb as $key => $t){ ?>
                <?php $tot[] = $t; ?>
                <td style="background-color: <?php if($t == 0){ ?> #5db744; <?php }elseif($t == $maxHeatMap1){ ?> red; <?php }else{ ?> #ffea55; <?php } ?>"><?= $t ?></td>
                <?php } ?>
                <td><?php echo array_sum($tot); ?></td>
                <?php $cuml1[] = array_sum($tot); ?>
                <td><?php echo array_sum($cuml1); ?></td>
                <?php $tot = ""; ?>
            </tr>
            <?php } ?>
             
            <tr>
                <td style="text-align: left !important; font-size: 7px;">Time %</td>
                <?php foreach($vertical_sum1 as $vrt){ ?>
                <td><?= $vrt ?></td>
                <?php } ?>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: left !important; font-size: 7px;">Time %(cum)</td>
                <?php foreach($vertical_sum1 as $vrt){ ?>
                <?php $cuml2[] = $vrt; ?>
                <td><?= array_sum($cuml2); ?></td>
                <?php } ?>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
	<?php } else { ?>
	<div class="col-md-6" style="width: 48%; float: left; font-size: 8.5px; padding: 4px;">
	</div>
	<?php } ?>
    
	<?php if(!empty($table2)){ ?>
    <div class="col-md-6" style="width: 48%; float: right; font-size: 8.5px; padding: 4px;">
        <!--<table class="table table-borderless heat-map1">-->
        <table class="table table-borderless heat-map1">
            <tr>
                <td class="left" rowspan="15"><h6><b>Engine Load (%)</b></h6></td>
                <td class="text-center table-header" colspan="11">Engine Speed (RPM)</td>
            </tr>
            <tr>
                <td></td>
                <td>1000</td>
                <td>2000</td>
                <td>3000</td>
                <td>4000</td>
                <td>5000</td>
                <td>6000</td>
                <td>7000</td>
                <td>8000</td>
                <td>9000</td>
                <td>>9000</td>
                <td><p class="td-align1" style="text-align: center;">Time<br>%</p></td>
                <td><p class="td-align2">Time %<br>(cum)</p></td>
            </tr>
            <?php $p = 1; ?>
            
            <?php foreach($table2 as $tb){ ?>
            <tr>
                <td style="text-align: center;"><?= $p++."0"; ?></td>
                <?php $tot = array(); ?>
                <?php foreach($tb as $key => $t){ ?>
                <?php $tot[] = $t; ?>
                <td style="background-color: <?php if($t == 0){ ?> #5db744; <?php }elseif($t == $maxHeatMap2){ ?> red; <?php }else{ ?> #ffea55; <?php } ?>"><?= $t ?></td>
                <?php } ?>
                <td><?php echo array_sum($tot); ?></td>
                <?php $cuml3[] = array_sum($tot); ?>
                <td><?php echo array_sum($cuml3); ?></td>
                <?php $tot = ""; ?>
            </tr>
            <?php } ?>
            
            <tr>
                <td style="text-align: right !important; font-size: 7px;">Time %;</td>
                <?php foreach($vertical_sum2 as $vrt){ ?>
                <td><?= $vrt ?></td>
                <?php } ?>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: right !important; font-size: 7px;">Time %;(cum)</td>
                <?php foreach($vertical_sum2 as $vrt){ ?>
                <?php $cuml4[] = $vrt; ?>
                <td><?= array_sum($cuml4); ?></td>
                <?php } ?>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <?php } else { ?>
	<div class="col-md-6" style="width: 48%; float: right; font-size: 8.5px; padding: 4px;">
	</div>
	<?php } ?>
</div>
</div>    
    <!--------------------Fifth Section End -------------->
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <br><br><br><br>
<?php } ?>    
    <!--------------------Sixth Section Start -------------->
<div class="container">
<div class="row">
    <div class="col-md-12" style="height: 30px; background-color: grey; margin-top: 40px; padding-left: 30px; text-align: left; font-size: 15px; color: #fff; font-weight: bold; width: 100%;">
        <p style="float: left;">Transmission Usage Statistics</p>
    </div>
    <div class="col-md-12">
    <table class="table table-borderless" style="width: 100%;" >
        <tr>
            <td style="width: 66.6667%">
                <table class="table table-borderless"> 
                     <tr>
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" >Gear 0</td>
                        <td style="padding:0; margin: 0;" >Gear 1</td>
                        <td style="padding:0; margin: 0;" >Gear 2</td>
                        <td style="padding:0; margin: 0;" >Gear 3</td>
                        <td style="padding:0; margin: 0;" >Gear 4</td>
                        <td style="padding:0; margin: 0;" >Gear 5</td>
                        <td style="padding:0; margin: 0;" >Gear 6</td>
                    </tr>
                    <tr>
                        <td style="padding:0; margin: 0;" >Min RPM</td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($gear->gr0_min_rpm)? $gear->gr0_min_rpm:"-" ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($gear->gr1_min_rpm)? $gear->gr1_min_rpm:"-"; ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($gear->gr2_min_rpm)? $gear->gr2_min_rpm:"-" ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($gear->gr3_min_rpm)? $gear->gr3_min_rpm:"-"; ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($gear->gr4_min_rpm)? $gear->gr4_min_rpm:"-"; ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($gear->gr5_min_rpm)? $gear->gr5_min_rpm:"-"; ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($gear->gr6_min_rpm)? $gear->gr6_min_rpm:"-"; ?></td>
                    </tr>
                    <tr>
                         <td style="padding:0; margin: 0;" >Avg RPM</td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($avg_rpm_gear0)? round($avg_rpm_gear0):"-"; ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($avg_rpm_gear1)? round($avg_rpm_gear1):"-"; ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($avg_rpm_gear2)? round($avg_rpm_gear2):"-"; ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($avg_rpm_gear3)? round($avg_rpm_gear3):"-"; ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($avg_rpm_gear4)? round($avg_rpm_gear4):"-"; ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($avg_rpm_gear5)? round($avg_rpm_gear5):"-"; ?></td>
                        <td style="padding:0; margin: 0;" ><?php echo !empty($avg_rpm_gear6)? round($avg_rpm_gear6):"-"; ?></td>
                    </tr>
                    <tr>
                        <td style="padding:0; margin: 0;">Max RPM</td>
                        <td style="padding:0; margin: 0;"><?php echo !empty($gear->gr0_max_rpm)? $gear->gr0_max_rpm:"-" ?></td>
                        <td style="padding:0; margin: 0;"><?php echo !empty($gear->gr1_max_rpm)? $gear->gr1_max_rpm:"-"; ?></td>
                        <td style="padding:0; margin: 0;"><?php echo !empty($gear->gr2_max_rpm)? $gear->gr2_max_rpm:"-" ?></td>
                        <td style="padding:0; margin: 0;"><?php echo !empty($gear->gr3_max_rpm)? $gear->gr3_max_rpm:"-"; ?></td>
                        <td style="padding:0; margin: 0;"><?php echo !empty($gear->gr4_max_rpm)? $gear->gr4_max_rpm:"-"; ?></td>
                        <td style="padding:0; margin: 0;"><?php echo !empty($gear->gr5_max_rpm)? $gear->gr5_max_rpm:"-"; ?></td>
                        <td style="padding:0; margin: 0;"><?php echo !empty($gear->gr6_max_rpm)? $gear->gr6_max_rpm:"-"; ?></td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="table table-borderless">
                     <tr >
                        <td style="padding:0; margin: 0;" >No of Gear Shift</td>
                        <td style="padding:0; margin: 0;" >: <?php echo $gear_shift." /100 km"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Half Clutchtime</td>
                        <td style="padding:0; margin: 0;" >: <?php echo round($half_clutch,2)." %"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Clutch Power loss</td>
                        <td  style="padding:0; margin: 0;">: <?php echo round($pwr_loss,2)." kW/100 km"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >False neutral events</td>
                        <td style="padding:0; margin: 0;" >: <?php echo $netural." /100 km"; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>
</div>
</div>      
<!--------------------Sixth Section End -------------->
 <?php if(!empty($table4)){ ?>
<br><br><br><br><br><br>
<!--------------------Seventh Section Start -------------->

<div class="container">
<div class="row">

 <div class="col-md-6" style="width: 48%; float: left; font-size:8px; padding: 2px;">
    <!--<table class="table table-borderless heat-map2">-->
    <table class="table table-borderless heat-map2">
            <tr>
                <td class="left" rowspan="15" colspan="1"><h6><b>To</b></h6></td>
                <td colspan="7" class="text-left table-header" style="margin-left: -100px;">Gear Shift /100 km</td>       
            </tr>
            <tr>
                <td rowspan="1" class="no-padding"><h6><b>From</b></h6></td>
                <td>0</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
                <td>6</td>
            </tr>

            <?php for($i=0;$i<=6;$i++){ ?>
            <tr>
                <td><?= $i; ?></td>
                <td <?php if(in_array($i."_0",$table3)){ ?> style="background-color: #e54a77;" <?php } ?>><?php if(in_array($i."_0",$table3)){ echo $tab3_data[array_search($i."_0", $table3)]; } ?></td>
                <td <?php if(in_array($i."_1",$table3)){ ?> style="background-color: #e54a77;" <?php } ?>><?php if(in_array($i."_1",$table3)){ echo $tab3_data[array_search($i."_1", $table3)]; } ?></td>
                <td <?php if(in_array($i."_2",$table3)){ ?> style="background-color: #e54a77;" <?php } ?>><?php if(in_array($i."_2",$table3)){ echo $tab3_data[array_search($i."_2", $table3)]; } ?></td>
                <td <?php if(in_array($i."_3",$table3)){ ?> style="background-color: #e54a77;" <?php } ?>><?php if(in_array($i."_3",$table3)){ echo $tab3_data[array_search($i."_3", $table3)]; } ?></td>
                <td <?php if(in_array($i."_4",$table3)){ ?> style="background-color: #e54a77;" <?php } ?>><?php if(in_array($i."_4",$table3)){ echo $tab3_data[array_search($i."_4", $table3)]; } ?></td>
                <td <?php if(in_array($i."_5",$table3)){ ?> style="background-color: #e54a77;" <?php } ?>><?php if(in_array($i."_5",$table3)){ echo $tab3_data[array_search($i."_5", $table3)]; } ?></td>
                <td <?php if(in_array($i."_6",$table3)){ ?> style="background-color: #e54a77;" <?php } ?>><?php if(in_array($i."_6",$table3)){ echo $tab3_data[array_search($i."_6", $table3)]; } ?></td>
            </tr>
            <?php } ?>

        </table>
    </div>

    <div class="col-md-6" style="width: 48%; float: right; font-size: 8px; padding: 2px;">
        <!--<table class="table table-borderless heat-map1">-->
        <table class="table table-borderless heat-map1">
            <tr>
                <td class="left" rowspan="15"><h6><b>Gear</b></h6></td>
                <td class="text-center table-header" colspan="11">Engine Speed (RPM)</td>
            </tr>
            <tr>
                <td></td>
                <td>1000</td>
                <td>2000</td>
                <td>3000</td>
                <td>4000</td>
                <td>5000</td>
                <td>6000</td>
                <td>7000</td>
                <td>8000</td>
                <td>9000</td>
                <td>>9000</td>
                <td><p class="td-align1">Time %</p></td>
                <td><p class="td-align2">Time %<br>(cum)</p></td>
            </tr>
            <?php $p = 0; ?>
            
            <?php foreach($table4 as $tb){ ?>
            <tr>
               <!--  <td class="left" rowspan="11"><p class="td-align ">Acceleration (m/sec<sup>2</sup>)</p></td> -->
                <td style="text-align: right;"><?= $p++; ?></td>
                <?php $tot = array(); ?>
                <?php foreach($tb as $key => $t){ ?>
                <?php $tot[] = $t; ?>
                <td style="background-color: <?php if($t == 0){ ?> #5db744; <?php }elseif($t == $maxHeatMap4){ ?> red; <?php }else{ ?> #ffea55; <?php } ?>"><?= $t ?></td>
                <?php } ?>
                <td><?php echo array_sum($tot); ?></td>
                <?php $cuml7[] = array_sum($tot); ?>
                <td><?php echo array_sum($cuml7); ?></td>
                <?php $tot = ""; ?>
            </tr>
            <?php } ?>
            
            <tr>
                <td style="text-align: right;">Time %</td>
                <?php foreach($vertical_sum4 as $vrt){ ?>
                <td ><?= $vrt ?></td>
                <?php } ?>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: right;">Time %(cum)</td>
                <?php foreach($vertical_sum4 as $vrt){ ?>
                <?php $cuml8[] = $vrt; ?>
                <td><?= array_sum($cuml8); ?></td>
                <?php } ?>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
</div>    
<?php } ?>

<!--------------------Seventh Section End -------------->
<br><br><br><br><br><br>
<br><br><br><br><br><br>
<br><br><br><br><br><br><br>
<!--------------------Eighth Section Start -------------->
<div class="container">
<div class="row">
    <div class="col-md-12" style="height: 30px; background-color: grey; margin-top: 40px; padding-left: 30px; text-align: left; font-size: 15px; color: #fff; font-weight: bold; width: 100%;">
        <p style="float: left;">Charging System Usage Statistics</p>
    </div>
    <div class="col-md-12">
    <table class="table table-borderless" >
        <tr>
            <td>
                <table class="table table-borderless">
                    <tr >
                        <td style="padding:0; margin: 0;" >Avg Voltage</td>
                        <td style="padding:0; margin: 0;" >: <?php echo $avg_voltage." V"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Avg of Lowest Voltages</td>
                        <td style="padding:0; margin: 0;" >: <?php echo $avg_voltage_low." V"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;"  ></td>
                    </tr>
                </table>
            </td>
            <td>
               <table class="table table-borderless">
                    <tr >
                        <td style="padding:0; margin: 0;" >Max Voltage</td>
                        <td style="padding:0; margin: 0;" >: <?php echo $max_batt_volt." V"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Min Voltage</td>
                        <td style="padding:0; margin: 0;" >: <?php echo $min_batt_volt." V"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" ></td>
                    </tr>
                </table>
            </td><td>
                <table class="table table-borderless">
                    <tr >
                        <td style="padding:0; margin: 0;" >Voltage &lt; low limit</td>
                        <td style="padding:0; margin: 0;" >: <?php echo $avg_voltage_low_less." times/100km"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" >Voltage &gt; high limit</td>
                        <td style="padding:0; margin: 0;" >: <?php echo $avg_voltage_low_high." times/100km"; ?></td>
                    </tr>
                    <tr >
                        <td style="padding:0; margin: 0;" ></td>
                        <td style="padding:0; margin: 0;" ></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table> 
    </div>
</div>
</div>     
<!--------------------Eighth Section End -------------->
<br><br><br><br><br><br>
<!--------------------Nine Section Start -------------->
<div class="container">
<div class="row">
    <div class="col-md-12" style="height: 30px; background-color: grey; margin-top: 70px; padding-left: 30px; text-align: left; color: white; font-weight: bold; width: 100%;">
        <p style="float: left;">Health Prognosis</p>
    </div>
</div>
</div>    
<br><br>    
    <!-- <table class="table table-borderless">
        <tr>
            <td>
                 <h5>Battery and Charging System Health</h5>
                <?php $batteryHealth// = $batteryHealth? $batteryHealth:"6";  ?>
                <img src="<?= $url ?>/images/img/rating/<?= $batteryHealth ?>.png" width="1500%" class="scale-icon">
                <h6>Comments:</h6>
            </td>
            <td>
                <h5>Clutch Health</h5>
                <img src="<?= $url ?>/images/img/rating/6.png" width="1500%"class="scale-icon">
                <h6>Comments: Paramater Not Supported</h6>
            </td>
            <td>
                <h5>Injector and Spark Plug Health</h5>
                <?php $injectorHealth//  = $injectorHealth? $injectorHealth:"6";  ?>
                <img src="<?= $url ?>/images/img/rating/<?= $injectorHealth ?>.png" width="1500%" class="scale-icon">
                <h6>Comments:</h6>
            </td>
            <td>
        </tr>
        <tr>
            <td>
                <h5>Air Filter Health</h5>
                     <?php $airHealth// = $airHealth? $airHealth:"6";  ?>
                    <img src="<?= $url ?>/images/img/rating/<?= $airHealth ?>.png" width="  1500%" class="scale-icon">
                    <h6>Comments:</h6>
            </td>
            <td>
                <h5>Oil Health</h5>
                <?php $oilHealth// = $oilHealth? $oilHealth:"6";  ?>
                <img src="<?= $url ?>/images/img/rating/<?= $oilHealth ?>.png" width="  1500%" class="scale-icon">
                <h6>Comments:</h6>
            </td>
            <td>
                <h5>Cooling System Health</h5>
                <?php $coolingHealth//  = $coolingHealth? $coolingHealth:"6";  ?>
                <img src="<?= $url ?>/images/img/rating/<?= $coolingHealth ?>.png" width="  1500%"class="scale-icon">
                <h6>Comments:</h6>
            </td>
            <td>
        </tr>
        <tr>
            <td>
               
                <h5>Gear Health</h5>
                <img src="<?= $url ?>/images/img/rating/6.png" width="1500%" class="scale-icon">
                <h6>Comments: Parameter Not Supported</h6>
            
            </td>
            <td>
                
                <h5>Vibration Health</h5>
                <img src="<?= $url ?>/images/img/rating/6.png" width="1500%" class="scale-icon">
                <h6>Comments: Initial;Final</h6>
           
            </td>
            <td>
                
            </td>
            <td>
        </tr>
    </table> -->
    <table class="table table-borderless">
        <tr>
            <td style="padding:0; margin: 0;">
                <table class="table table-borderless" style="padding:0; margin: 0;">
                    <tr>
                        <td style="padding:0; margin: 0;">
                            <h5  style="font-size: 16px; font-weight: bold;">Battery Health</h5> 
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:0; margin: 0;" valign="center" align="center">
                             <?php $batteryHealth  = $batteryHealth? $batteryHealth:"6";  ?>
                            <img style="vertical-align: middle;" src="<?= $url ?>/images/img/rating/<?= $batteryHealth ?>.png" width=" 2000%" class="scale-icon">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:0; margin: 0;">
                            <h6>Comments: </h6>
                            <?= $batteryHealthMsg ?>
                        </td>
                    </tr>
                </table>    
            </td>

            <td style="padding:0; margin: 0;">
                <table class="table table-borderless" style="padding:0; margin: 0;">
                    <tr>
                        <td style="padding:0; margin: 0;">
                           <h5 style="font-size: 18px; font-weight: bold;">Clutch Health</h5>
                
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="center" style="padding:0; margin: 0;">
                             <img src="<?= $url ?>/images/img/rating/6.png" width="  2000%"class="scale-icon">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:0; margin: 0;">
                            <h6>Comments: Paramater Not Supported</h6>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
        <div class="page-break"></div>
        <table class="table table-borderless">
        <tr>
            <td style="padding:0; margin: 0;">
                <table class="table table-borderless" style="padding:0; margin: 0;">
                    <tr>
                        <td style="padding:0; margin: 0;">
                          <h5 style="font-size: 18px; font-weight: bold;">Injector and Spark Plug Health</h5>
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="center" style="padding:0; margin: 0;">
                             <?php $injectorHealth  = $injectorHealth? $injectorHealth:"6";  ?>
                        <img src="<?= $url ?>/images/img/rating/<?= $injectorHealth ?>.png" width=" 2000%" class="scale-icon">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:0; margin: 0;">
                           <h6>Comments: </h6>
                           <?= $injectorHealthMsg ?>
                        </td>
                    </tr>
                </table>   
            </td>

            <td style="padding:0; margin: 0;"><table class="table table-borderless" style="padding:0; margin: 0;">
                    <tr>
                        <td style="padding:0; margin: 0;">
                          <h5 style="font-size: 18px; font-weight: bold;">Air Filter Health</h5>
                
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="center" style="padding:0; margin: 0;">
                             <?php $airHealth  = $airHealth? $airHealth:"6";  ?>
                             <img src="<?= $url ?>/images/img/rating/<?= $airHealth ?>.png" width="  2000%" class="scale-icon">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:0; margin: 0;"> 
                           <h6>Comments: </h6>
                           <?= $airHealthMsg ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding:0; margin: 0;">
                <table class="table table-borderless" style="padding:0; margin: 0;">
                    <tr>
                        <td style="padding:0; margin: 0;">
                          <h5 style="font-size: 18px; font-weight: bold;">Oil Health</h5>
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="center">
                             <?php $oilHealth  = $oilHealth? $oilHealth:"6";  ?>
                            <img src="<?= $url ?>/images/img/rating/<?= $oilHealth ?>.png" width="  2000%" class="scale-icon">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:0; margin: 0;">
                           <h6>Comments:</h6>
                        </td>
                    </tr>
                </table>
            </td>

            <td style="padding:0; margin: 0;">
                <table class="table table-borderless" style="padding:0; margin: 0;">
                    <tr>
                        <td style="padding:0; margin: 0;">
                           <h5 style="font-size: 18px; font-weight: bold;">Cooling System Health</h5>
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="center" style="padding:0; margin: 0;">
                             <?php $coolingHealth  = $coolingHealth? $coolingHealth:"6";  ?>
                <img src="<?= $url ?>/images/img/rating/<?= $coolingHealth ?>.png" width="  2000%"class="scale-icon">
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:0; margin: 0;">
                           <h6 >Comments: </h6>
                           <?= $coolingHealthMsg ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td style="padding:0; margin: 0;">
                 <table class="table table-borderless" style="padding:0; margin: 0;">
                    <tr>
                        <td>
                           <h5 style="font-size: 18px; font-weight: bold;">Gear Health</h5>
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="center">
                            <img src="<?= $url ?>/images/img/rating/6.png" width=" 2000%" class="scale-icon">
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <h6>Comments: Parameter Not Supported</h6>
                        </td>
                    </tr>
                </table>
            </td>

            <td style="padding:0; margin: 0;">
                <table class="table table-borderless" style="padding:0; margin: 0;">
                    <tr>
                        <td>
                          <h5 style="font-size: 18px; font-weight: bold;">Vibration Health</h5>
                        </td>
                    </tr>
                    <tr>
                        <td valign="center" align="center">
                            <img src="<?= $url ?>/images/img/rating/6.png" width=" 2000%" class="scale-icon">
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <h6>Comments: <br>Initial: <br> Final:</h6>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

<!-- <div class="container">
<div class="row">
            <div class="col-md-12">
                <div class="col-md-6 icon-box" style="float: left; width: 45%;">
                    <h5 style="font-size: 16px; font-weight: bold;">Battery and Charging System Health</h5>
                    <?php $batteryHealth//  = $batteryHealth? $batteryHealth:"6";  ?>
                    <img src="<?= $url ?>/images/img/rating/<?= $batteryHealth ?>.png" width="  70%" class="scale-icon">
                    <h6>Comments:</h6>
                </div>
                <div class="col-md-6 icon-box" style="float: left; width: 45%;">
                    <h5 style="font-size: 16px; font-weight: bold;">Clutch Health</h5>
                    <img src="<?= $url ?>/images/img/rating/6.png" width="  70%"class="scale-icon">
                    <h6>Comments: Paramater Not Supported</h6>
                </div>
            </div>
</div>    
</div>
<div class="page-break"></div>
<div class="container">
<div class="row">
            <div class="col-md-12">
                <div class="col-md-6 icon-box" style="float: left; width: 45%;">
                    <h5 style="font-size: 16px; font-weight: bold;">Injector and Spark Plug Health</h5>
                    <?php $injectorHealth//  = $injectorHealth? $injectorHealth:"6";  ?>
                    <img src="<?= $url ?>/images/img/rating/<?= $injectorHealth ?>.png" width="  70%" class="scale-icon">
                    <h6>Comments:</h6>
                </div>
                <div class="col-md-6 icon-box" style="float: left; width: 45%;">
                    <h5 style="font-size: 16px; font-weight: bold;">Air Filter Health</h5>
                     <?php $airHealth//  = $airHealth? $airHealth:"6";  ?>
                    <img src="<?= $url ?>/images/img/rating/<?= $airHealth ?>.png" width="  70%" class="scale-icon">
                    <h6>Comments:</h6>
                </div>
            </div>
</div>    
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div class="container">
<div class="row">  
<div class="col-md-12">          
            <div class="col-md-6 icon-box" style="float: left; width: 45%;">
                <h5 style="font-size: 16px; font-weight: bold;">Oil Health</h5>
                <?php $oilHealth//  = $oilHealth? $oilHealth:"6";  ?>
                <img src="<?= $url ?>/images/img/rating/<?= $oilHealth ?>.png" width="  70%" class="scale-icon">
                <h6>Comments:</h6>
            </div>
            <div class="col-md-6 icon-box" style="float: left; width: 45%;">
                <h5 style="font-size: 16px; font-weight: bold;">Cooling System Health</h5>
                <?php $coolingHealth//  = $coolingHealth? $coolingHealth:"6";  ?>
                <img src="<?= $url ?>/images/img/rating/<?= $coolingHealth ?>.png" width="  70%"class="scale-icon">
                <h6>Comments:</h6>
            </div>
</div>
</div>    
</div>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div class="container">
<div class="row">     
<div class="col-md-12">          
            <div class="col-md-6 icon-box" style="float: left; width: 45%;">
                <h5 style="font-size: 16px; font-weight: bold;">Gear Health</h5>
                <img src="<?= $url ?>/images/img/rating/6.png" width="  70%" class="scale-icon">
                <h6>Comments: Parameter Not Supported</h6>
            </div>
            <div class="col-md-6 icon-box" style="float: left; width: 45%;">
                <h5 style="font-size: 16px; font-weight: bold;">Vibration Health</h5>
                <img src="<?= $url ?>/images/img/rating/6.png" width=" 70%" class="scale-icon">
                <h6>Comments: <br>Initial: <br> Final:</h6>
            </div>
</div>            
</div>    
</div> -->
<!--------------------Nine Section End -------------->
    <!-- Container Close -->   
</div>   
</body>
</htm>