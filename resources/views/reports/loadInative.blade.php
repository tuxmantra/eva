                           <!-- Active reports start -->
                <br>
         <div class="col-md-12" align="center">
            <span id="cli_message1" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
         </div>
                <br>
                <div class="col-md-12">
                   <div class="col-md-4" style="float: left;" align="right"><a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="deleteRecords()">Delete</a></div>
                   <div class="col-md-4" style="float: left;" align="right"><a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="loadActiveReport()">Active Records</a></div>
                   <div class="col-md-4" style="float: left;" align="right"><a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="getReportToActivate()">Activate Records</a></div>
                </div>  
                          <br><br>
                           <div class="material-datatables">
                              <table id="datatables1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                 <thead>
                                    <tr>
                                       <th>&nbsp;&nbsp;</th>
                                       <th class="table-heading-font">S.No</th>
                                       <th class="table-heading-font">Report No</th>
                                       <th class="table-heading-font">Type</th>
                                       <th class="table-heading-font">Date</th>
                                       <th class="table-heading-font">User Name</th>
                                       <th class="table-heading-font">Device ID</th>
                                       <th class="table-heading-font">Monitoring Report</th>
                                    </tr>
                                 </thead>
                                 <tfoot>
                                    <tr>
                                      <th>&nbsp;&nbsp;</th>
                                       <th class="table-heading-font">S.No</th>
                                       <th class="table-heading-font">Report No</th>
                                       <th class="table-heading-font">Type</th>
                                       <th class="table-heading-font">Date</th>
                                       <th class="table-heading-font">User Name</th>
                                       <th class="table-heading-font">Device ID</th>
                                       <th class="table-heading-font">Monitoring Report</th>
                                    </tr>
                                 </tfoot>
                                 <tbody>
                     
                                    @if (!empty($report_inactive))
                                    <?php $id = 1; ?>
                                    @foreach ($report_inactive as $list)  
                                    <tr>
                                      <td><input type="checkbox" name="inactive[]" value="{{$list->id}}"></td>
                                       <td style="background-color: #f5f5f5;"><?= $id ?></td>

                                       <td style="background-color: #ffffff;">R {{$list->id}} <?php if($list->mctype == 3){
                                             
                                       echo date( "Ymd", strtotime( $list->ad_dt . "-1 day"));

                                       }else{ echo date('Ymd',strtotime($list->ad_dt));  } ?></td>

                                       <td style="background-color: #f5f5f5;">{{$list->type}}</td>

                                       <td style="background-color: #ffffff;">
                                 
                                       <?php echo date('Y-m-d',strtotime($list->ad_dt)); ?>
                                        

                                      </td>
                                      <td style="background-color: #f5f5f5;" align="center">{{App\Helpers\Helper::getUserByDeviceId ($list->did)}}</td>
                                      <td style="background-color: #f5f5f5;" align="center">{{$list->devid}}</td>


                                       <td style="background-color: #ffffff;" align="center">
                                          <?php if($list->mctype == 1){ ?>
                                             <a target="_blank" href="" onclick="loadCalibRepo(<?= $list->idmc ?>)"><i class="material-icons">cloud_download
                                             </i>
                                             </a>
                                          <?php }elseif($list->mctype == 2){ ?>
                                              <a href="javascript:void(0)" onclick="loadMonRepo('<?= $list->idmc ?>')"><i class="material-icons">cloud_download
                                              </i>
                                              </a>
                                          <?php }elseif($list->mctype == 3){ ?>
                                               <?php $dt = explode(' ',$list->ad_dt); ?>
                                               <?php $dp = date( "Y-m-d", strtotime( $dt[0] . "-1 day")) ?>
                                               <a href="javascript:void(0)" onclick="loadCronRepo('<?= $dp ?>')"><i class="material-icons">cloud_download
                                               </i>
                                               </a>
                                          <?php }else{ ?>
                                               <a href="{{url('/')}}/public/reports/<?= $list->report_file ?>" download><i class="material-icons">cloud_download
                                               </i>
                                               </a>
                                          <?php } ?>
                                          

                                       </td>
                                    </tr>
                                    <?php $id++; ?>  
                                    @endforeach
                                    @else
                                    @endif

                                 </tbody>
                              </table>
                           </div>

                           <!-- Active reorts End -->

<script type="text/javascript">
function deleteRecords(){

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Yes, delete it!',
        buttonsStyling: false
      }).then(function(e) {

        var val = [];
        $(':checkbox:checked').each(function(i){
            val[i] = $(this).val();
        });

         if(e.dismiss == 'cancel'){
             
         }

        if(e.value == true){
            $.post('{{url('/')}}/delete-reports', { did: '<?= $did ?>', status: '3', data: val }).done(function (data) {
                    swal({
                      title: 'Deleted!',
                      text: 'Your file has been deleted.',
                      type: 'success',
                      confirmButtonClass: "btn btn-success",
                      buttonsStyling: false
                    })
                    setTimeout(function() {
                      location.reload();
                    }, 3000)  
              })
        }

      }).catch(swal.noop)
  }
  // function deleteRecords(){
  //     var val = [];
  //     $(':checkbox:checked').each(function(i){
  //         val[i] = $(this).val();
  //     });
  //     $.post('{{url('/')}}/delete-reports', { did: '<?= $did ?>', status: '3', data: val }).done(function (data) {
  //          $('#link6').html(data);
  //     });
  // }

  function getReportToActivate(){
      var val = [];
      $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
      });
      if(val !=''){
         Swal.fire({
                title: 'Are you sure?',
                text: "You want to activate reports!",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#eb262d',
                cancelButtonColor: '#eb262d',
                confirmButtonText: 'Yes, Upload it!'
            }).then((result) => {
              if(result.value){

             $.post('{{url('/')}}/activate-reports', { did: '<?= $did ?>', data: val }).done(function (data) {
          if(data != 1){
              $('#link6').html(data);
              Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   'Records Activated.',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
              // $('#cli_message1').html("Records Activated.");
          }else{
             Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   'Please select some records.',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
              // $('#cli_message1').html("Please select some records.");
          }
      });
              }
              // location.reload();
            })
     
  }else{
  Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   'Please select some records.',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
}
}


   $(document).ready(function() {
        $('#datatables1').DataTable({
           "pagingType": "full_numbers",
           "lengthMenu": [
             [10, 25, 50, -1],
             [10, 25, 50, "All"]
           ],
        
           responsive: true,
           language: {
             search: "_INPUT_",
             searchPlaceholder: "Search records",
           }
       });
}); 
</script>