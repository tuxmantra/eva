  <!-- Active reports start -->
  <?php 
  $colm=count($cols)+1;
  $width= 100/($colm+3);
  $rowc=count($rows)+5;
  ?>
  <style type="text/css">
    div.vertical
{
 margin-left: -31px;
 position: absolute;
 width: auto;
 transform: rotate(-90deg);
 -webkit-transform: rotate(-90deg); /* Safari/Chrome */
 -moz-transform: rotate(-90deg); /* Firefox */
 -o-transform: rotate(-90deg); /* Opera */
 -ms-transform: rotate(-90deg); /* IE 9 */
}

th.vertical
{
 height: 150px;
 line-height: 14px;
 padding-bottom: 20px;
 text-align: center;

}
.dtHorizontalExampleWrapper {
max-width: 600px;
margin: 0 auto;
}
#dtHorizontalExample th, td {
white-space: nowrap;
}

table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
bottom: .5em;
}

  </style>
  <br>

           <div class="col-md-12" align="center">
             <button type="button" id="close" class="btn btn-sm  pull-right" style="background-color: #34495E;" onclick="closemap()" class="float-left"> 
                      <i class="material-icons" style="color: white">close</i> 
                    </button>
           <table id="dtHorizontalExample" class="table table-striped table-bordered table-sm" cellspacing="0"
  width="100%">
            <tr>
               <th class="vertical" style="width: 5%" rowspan="<?=$rowc ?>"><div class="vertical"><?=$namevaluePart2?> (<?=$unit2?>)</div></th>
            </tr>
            <tr>
                <td class="text-center table-header" colspan="<?=$colm ?>" style="font-weight: bold;"><?=$namevaluePart1?> (<?=$unit1?>)</td>
                <td class="left" rowspan="2" style="font-weight: bold;"><p class="td-align1" >Time %</p></td>
                <td class="left" rowspan="2" style="font-weight: bold;"><p class="td-align2" >Time %(cum)</p></td>
            </tr>
            <tr>
                <td style="width: 5%; text-align: center;"></td>
                <?php for($i=0;$i<=count($cols)-1;$i++){ ?>
                  <td style="text-align:center; width: <?=$width?>%;font-weight: bold;"><?= $cols[$i]; ?></td>
                <?php } ?>

            </tr>
            
            <?php foreach($table as $d => $tb){ ?>
            <tr>
               <!--  <td class="left" rowspan="11"><p class="td-align ">Acceleration (m/sec<sup>2</sup>)</p></td> -->
                <td style="text-align: center; font-weight: bold;"><?= $rows[$d]; ?></td>
                <?php $tot = array(); ?>
                <?php foreach($tb as $key => $t){ ?>
                <?php $tot[] = $t; ?>
                <td style=" text-align:center; background-color: <?php if($t == 0){ ?> #5db744; <?php }elseif($t == $maxHeatMap){ ?> red; <?php }else{ ?> #ffea55; <?php } ?>"><?= $t ?></td>
                <?php } ?>
                <td style="text-align:center;"><?php echo array_sum($tot); ?></td>
                <?php $cuml7[] = array_sum($tot); ?>
                <td style="text-align:center;"><?php echo array_sum($cuml7); ?></td>
                <?php $tot = ""; ?>
            </tr>
            <?php } ?>
            
            <tr>
                <td style="text-align: right; width: 6%; font-weight: bold;"><p class="td-align2" >Time %</p></td>
                <?php foreach($vertical_sum as $vrt){ ?>
                <td style="text-align:center;"><?= $vrt ?></td>
                <?php } ?>
                <td style="text-align:center;"></td>
                <td style="text-align:center;"></td>
                
            </tr>
            <tr>
                <td style="text-align: right; width: 6%; font-weight: bold;">Time %(cum)</td>
                <?php foreach($vertical_sum as $vrt){ ?>
                <?php $cuml8[] = $vrt; ?>
                <td style="text-align: center; "><?= array_sum($cuml8); ?></td>
                <?php } ?>
                <td></td>
                <td></td>
                
            </tr>
        </table>
          
  </div>
<script type="text/javascript">
  $(document).ready(function () {
$('#dtHorizontalExample').DataTable({
"scrollX": true
});
$('.dataTables_length').addClass('bs-select');
});
</script>
<!-- Active reorts End -->