  <!-- Active reports start -->
  <br>
  <div class="col-md-12" align="center">
          
            <span id="cli_message1" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
                <br>
                <div class="col-md-12">
                   <div class="col-md-4" style="float: left;" align="right"><a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="deleteRecords()">Delete</a></div>
                   <div class="col-md-4" style="float: left;" align="right"><a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="loadInactiveReport()">InActive Records</a></div>
                </div>  
                          <br><br>
                           <div class="material-datatables">
                              <table id="datatables1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                 <thead>
                                    <tr>
                                       <th>&nbsp;&nbsp;</th>
                                       <th class="table-heading-font">S.No</th>
                                       <th class="table-heading-font">Report No</th>
                                       <th class="table-heading-font">Type</th>
                                       <th class="table-heading-font">Date</th>
                                       <th class="table-heading-font">User Name</th>
                                       <th class="table-heading-font">Device ID</th>
                                       <th class="table-heading-font">Monitoring Report</th>
                                    </tr>
                                 </thead>
                                 <tfoot>
                                    <tr>
                                      <th>&nbsp;&nbsp;</th>
                                       <th class="table-heading-font">S.No</th>
                                       <th class="table-heading-font">Report No</th>
                                       <th class="table-heading-font">Type</th>
                                       <th class="table-heading-font">Date</th>
                                       <th class="table-heading-font">User Name</th>
                                       <th class="table-heading-font">Device ID</th>
                                       <th class="table-heading-font">Monitoring Report</th>
                                    </tr>
                                 </tfoot>
                                 <tbody>
                                    @if (!empty($report_active))
                                    <?php $id = 1; ?>
                                    @foreach ($report_active as $list)  
                                    <tr>
                                       <td><input type="checkbox" name="active[]" value="{{$list->id}}"></td>
                                       <td style="background-color: #f5f5f5;"><?= $id ?></td>

                                       <td style="background-color: #ffffff;">R {{$list->id}} <?php if($list->mctype == 3){
                                             
                                       echo date( "Ymd", strtotime( $list->ad_dt . "-1 day"));

                                       }else{ echo date('Ymd',strtotime($list->ad_dt));  } ?></td>

                                       <td style="background-color: #f5f5f5;">{{$list->type}}</td>

                                       <td style="background-color: #ffffff;">
                                 
                                       <?php echo date('Y-m-d',strtotime($list->ad_dt)); ?>
                                        

                                      </td>
                                      <td style="background-color: #f5f5f5;" align="center">{{App\Helpers\Helper::getUserByDeviceId ($list->did)}}</td>
                                      <td style="background-color: #f5f5f5;" align="center">{{$list->devid}}</td>


                                       <td style="background-color: #ffffff;" align="center">
                                          <?php if($list->mctype == 1){ ?>
                                             <a target="_blank" href="" onclick="loadCalibRepo(<?= $list->idmc ?>)"><i class="material-icons">cloud_download
                                             </i>
                                             </a>
                                          <?php }elseif($list->mctype == 2){ ?>
                                              <a href="javascript:void(0)" onclick="loadMonRepo('<?= $list->idmc ?>')"><i class="material-icons">cloud_download
                                              </i>
                                              </a>
                                          <?php }elseif($list->mctype == 3){ ?>
                                               <?php $dt = explode(' ',$list->ad_dt); ?>
                                               <?php $dp = date( "Y-m-d", strtotime( $dt[0] . "-1 day")) ?>
                                               <a href="javascript:void(0)" onclick="loadCronRepo('<?= $dp ?>')"><i class="material-icons">cloud_download
                                               </i>
                                               </a>
                                          <?php }else{ ?>
                                               <a href="{{url('/')}}/public/reports/<?= $list->report_file ?>" download><i class="material-icons">cloud_download
                                               </i>
                                               </a>
                                          <?php } ?>
                                          

                                       </td>
                                    </tr>
                                    <?php $id++; ?>  
                                    @endforeach
                                    @else
                                    @endif
                                 </tbody>
                              </table>
                           </div>

<!-- Active reorts End -->

<script type="text/javascript">
function deleteRecords(){

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Yes, delete it!',
        buttonsStyling: false
      }).then(function(e) {

        var val = [];
        $(':checkbox:checked').each(function(i){
            val[i] = $(this).val();
        });

         if(e.dismiss == 'cancel'){
             
         }

        if(e.value == true){
            $.post('{{url('/')}}/delete-reports', { did: '<?= $did ?>', status: '2', data: val }).done(function (data) {
                    swal({
                      title: 'Deleted!',
                      text: 'Your file has been deleted.',
                      type: 'success',
                      confirmButtonClass: "btn btn-success",
                      buttonsStyling: false
                    })
                    setTimeout(function() {
                      location.reload();
                    }, 3000)  
              })
        }

      }).catch(swal.noop)
  }
  // function deleteRecords(){
  //     var val = [];
  //     $(':checkbox:checked').each(function(i){
  //         val[i] = $(this).val();
  //     });
  //     $.post('{{url('/')}}/delete-reports', { did: '<?= $did ?>', status: '2', data: val }).done(function (data) {
  //          $('#link6').html(data);
  //     });
  // }

   $(document).ready(function() {
        $('#datatables1').DataTable({
           "pagingType": "full_numbers",
           "lengthMenu": [
             [10, 25, 50, -1],
             [10, 25, 50, "All"]
           ],
           responsive: true,
           language: {
             search: "_INPUT_",
             searchPlaceholder: "Search records",
           }
       });
}); 
</script>