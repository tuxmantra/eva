  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <?php 
use App\Model\Fetch;
  ?>
  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
    tfoot {
     display: table-header-group !important;

    }
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    .dataTables_filter
    {
      display: none;
    }
  </style>
      <!-- End Navbar -->
       
      <div class="content" class="margin-top:10px"  >
        <div class="container-fluid">
          <div class="row">
                    <div class="col-md-12" align="center">
              
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>

          <div class="col-md-12" align="right">
            <!-- <a href="{{url('/')}}/add-user" title="Add User" class="btn btn-primary btn-sm">Add</a> -->
            
          </div>
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">library_books</i>
                  </div>
                  <h4 class="card-title">Reports</h4>
                </div>
                <div class="card-body">
                  <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>
                  <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                       
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">User</th>
                          <th class="table-heading-font">Vehicle</th>
                          <th class="table-heading-font">VIN No</th>
                          <th class="table-heading-font">Date</th>
                          <th class="table-heading-font">Operator</th>
                          <th class="table-heading-font">Result</th>
                          <th class="table-heading-font">Actions</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                       
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">User</th>
                          <th class="table-heading-font">Vehicle</th>
                          <th class="table-heading-font">VIN No</th>
                          <th class="table-heading-font">Date</th>
                          <th class="table-heading-font">Operator</th>
                          <th class="table-heading-font">Result</th>
                          <th class="table-heading-font">Actions</th>
                        </tr>
                      </tfoot>
                      
                      <tbody>
                        @if (!empty($reports))
                        <?php $resultc = ""; $id = 1; ?>
                        @foreach ($reports as $report) 
                         <?php if($resultc == 0){$resultc=2;}elseif($resultc ==1){$resultc=1;} 
                         $user = Fetch::getUserDeviceEmail($report->device_id);
                          ?>
                        <tr>
                         
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$id}}</td>
                          <td style="background-color: #ffffff;" class="table-heading-font">{{$user[0]->uname}}</td>
                          <td style="background-color: #f5f5f5;" class="table-heading-font">{{$report->vehicle}}</td>
                          <td style="background-color: #ffffff;" class="table-heading-font">{{$report->vin_no}}</td>
                        
                          <td class="table-heading-font" style="background-color: #f5f5f5;;">{{$report->dt}}</td>
                          <td class="table-heading-font" style="background-color: #ffffff;"> {{$report->rider_name}}</td>
                          <td style="background-color: #f5f5f5;" class="table-heading-font"><?php if($report->test_result == 1){ echo "Passed";}else{ echo "Failed";} ?></td>
                          <td class="table-heading-font" style="background-color: #ffffff;">
                          <a data-toggle="tooltip" title="Graph" href="/eva?vin_no={{$report->vin_no}}&devID={{$report->device_id}}&date={{$report->dt}}&vehicle={{$report->vehicle}}&result={{$resultc}}" class="btn btn-link btn-danger btn-just-icon edit"><i class="material-icons">insights</i></a>
                          <a data-toggle="tooltip" title="Download" target="_blank" href="/testreport/{{$report->id}}/{{$report->device_id}}" class="btn btn-link btn-danger btn-just-icon edit"><i class="material-icons">cloud_download</i></a>
                          
                        </tr>
                        <?php $id++; ?>  
                        @endforeach
                        @else
                           <tr><td colspan="9" align="center">No test Exist.</td></tr>
                        @endif
                       
                       
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>

    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
 @if (Session::has('message'))
  <script type="text/javascript">
            Swal.fire({
            type: 'info',
            title: 'Info',
            text: '{!! session('message') !!}',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })

  </script>
  @endif
 <script>
    $(document).ready(function() {

      
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true
      });

       var table = $('#datatables').DataTable();

      $('#datatables tfoot th').each( function () {
        var title = $(this).text();
        if(title == "S.No"){
           title = "Serial";
        }
        if(title == "Result"){
            var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                .appendTo( $(this).empty() )
                .on( 'change', function () {

                    var val = $(this).val();

                    table.column( 6 ) //Only the first column
                        .search( val ? '^'+$(this).val()+'$' : val, true, false )
                        .draw();
                } );

                table.column( 6 ).data().unique().sort().each( function ( d, j ) {
                if(d != '-'){
                  select.append( '<option value="'+d+'">'+d+'</option>' );
                }else{
                  select.append( '<option value="'+d+'">Nil</option>' );
                }
            } );
        }
        else if(title == "Vehicle")
        {
            var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                .appendTo( $(this).empty() )
                .on( 'change', function () {

                    var val = $(this).val();

                    table.column( 2 ) //Only the first column
                        .search( val ? '^'+$(this).val()+'$' : val, true, false )
                        .draw();
                } );

                table.column( 2 ).data().unique().sort().each( function ( d, j ) {
                if(d != '-'){
                  select.append( '<option value="'+d+'">'+d+'</option>' );
                }else{
                  select.append( '<option value="'+d+'">Nil</option>' );
                }
            } );
        }
        else
        {
           $(this).html( '<input class="'+title+'_tp" type="text" placeholder=" '+title+'" />' );
        }
      });

      table.columns([1,2,3,4,5,6]).every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
          });
      });


     

/*      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });*/
    });
  </script>