
                 <div class="col-md-12">
                   <canvas id="myChart"   
                  width="400" height="200"></canvas>
  </div>
                  
                 <script src="{{url('/')}}/public/assets/js/plugins/Chart.js"></script>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: <?=$date1?>,
        datasets: [{
            label: '<?=$mark?>',
            yLabel: '<?=$yLabel?>',
            data: <?=$values1?>,
           
            backgroundColor: [
                'rgba(202, 223, 230,1)'
                
            ],
            borderColor: [
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
               'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                 'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
               'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                 'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
               'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)'
            ],
            borderWidth: 4
        }]
    },
   options: {
      responsive: true,
       elements: {
        line: {
            tension: 0
        }
    },
      scales: {
        xAxes: [ {
          display: true,
          scaleLabel: {
            display: true,
            labelString: '<?=$yLabel?>'
          },
          ticks: {
            major: {
              fontStyle: 'bold',
              fontColor: '#FF0000'
            }
          }
        } ],
        yAxes: [ {
          display: true,
          ticks: {
                max: 5,
                min: 0,
                maxTicksLimit : 5
            },
          scaleLabel: {
            display: true,
            labelString: '<?=$mark?>'
          }
        } ]
      }
    }

});
</script>