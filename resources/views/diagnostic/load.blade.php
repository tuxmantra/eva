 <style type="text/css">
   .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 12px 8px !important;
    vertical-align: middle;
    border-color: #ddd;
}

    .fa-star
    {
     color: #d2d2d2;
    }
    .rate-popover {
      color: #c4c4c4;
    }

    .oneStar {
      color: #3d381c;
    }

    .twoStars {
      color: rgb(235 38 45);
    }

    .threeStars {

      color: #c2aa36;
    }

    .fourStars {
      color: #e2c327;
    }

    .fiveStars {
      color: #f3cb06;
    }
 </style>

                            <div class="col-md-12" id="load_data">
                              <ul class="nav nav-pills nav-pills-warning" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" onclick="lastDrive()" data-toggle="tab" href="#link1" role="tablist">
                                      Remote Diagnostics
                                  </a>
                                </li>
                                <?php  $userdata=Session::get('userdata'); 
                                       $utype=$userdata['utype']; 
                                        if($utype == 1 || $utype == 2){
                                       ?>
                                <li class="nav-item">
                                  <a class="nav-link" onclick="getgraph()" id="graphb" data-toggle="tab" href="#link4" role="tablist">
                                     Health Trend 
                                  </a>
                                </li>
                                 
                                <li class="nav-item">
                                  <a class="nav-link" onclick="fiveDrive()" data-toggle="tab" href="#link2" role="tablist">
                                    Calibration Health
                                  </a>
                                </li>
                                <?php } ?>
                                <li class="nav-item">
                                  <a class="nav-link" onclick="tenDrive()" data-toggle="tab" href="#link3" role="tablist">
                                    Health Monitoring
                                  </a>
                                </li>
                              
                              </ul>
                            <div class="tab-content tab-space">


                    <!----------------- Start Diagnostics --------------------> 
                    <div class="tab-pane active" id="link1">
                      <div class="container-fluid">
                        <div class="continues">
                          <div class="row classes">
                            <br><br>
                            <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                              <tr>
                                <td class="tdr tdl" colspan="1">
                                  <h2>Active DTC:</h2>
                                </td>
                              
                                <!--<td class="tdr">
                                  <h2>Oil Life Remaining:</h2>
                                </td>-->
                              </tr>
                              <tr>
                                <td class="tdr tdl alC" colspan="1">
                                  <h4><?=(trim($dtcTxt) != "") ? $dtcTxt : "No DTC" ;?></h4>
                                </td>
                              
                              </tr>
                               <?php  $userdata=Session::get('userdata'); 
                                       $utype=$userdata['utype'];
                                       if($utype == 1){

                                  
                                ?>
                              <tr>
                                <td class="tdr tdl">
                                  <h3>CO2 Emissions:</h3>
                                </td>
                                <td class="tdr">
                                  <h3>Number of Cold Starts:</h3>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdrb tdl">
                                  <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report col bdrRD"> 
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report col bdrRD">
                                      Last Drive:
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report col bdrRD">      
                                      <?=round($co2)?> g/km (<?=$score?>%)
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report col bdrRD">
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report col">
                                      Average:
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report col">
                                      <?=round($co2Avg)?> g/km
                                    </div>
                                  </div>
                                </td>
                                <td class="tdrb tdl tdr alC">
                                  <?php echo $cols == "" ? "0" :$cols; ?>
                                  <!--<h3><?=$cols?></h3>-->
                                </td>
                              </tr>
                              <tr>


                                <td class="tdr tdrb tdl" colspan="2">
                                  <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding ">  
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding ">
                                      <h3>Coolant System</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding">     
                                      <?php echo empty($mainMoniter[0]->tempmsg)? "OK" :$mainMoniter[0]->tempmsg; ?>
                                    </div>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding">     
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding ">
                                      <h3>Air System</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding">
                                      <?php echo empty($mainMoniter[0]->mapimsg)? "OK": $mainMoniter[0]->mapimsg;?>
                                    </div>
                                  </div>
                                  <!--<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding ">  
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding ">
                                      <h3>Fuel Pressure System</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding">     
                                      <?php echo empty($mainMoniter[0]->crpmsg)? "OK" :$mainMoniter[0]->crpmsg;?>
                                    </div>
                                  </div> -->  
                                </td> 
                              </tr>
                              
                              <!--<tr>
                                  <td class="tdr tdrb tdl" colspan="2">
                                  
                                  <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding">     
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding ">
                                    
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 report no-padding">
                                    
                                    </div>
                                  </div>    
                                </td> 
                              </tr>-->
                              
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>

                                <!----------------- End Diagnostics -------------------->


                                <!----------------- Calib Start Diagnostics -------------------->
                    <div class="tab-pane" id="link2">
                      <div class="container-fluid">
                        <div class="continues">
                          <div class="row classes">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                              
                              <tr>
                                <th class="tdr tdrA tdl th-clr" style="width:20%">
                                  Component
                                </td>
                                <th class="tdr tdrB th-clr">
                                  Ratings
                                </th>
                                <th class="tdr tdrC th-clr" style="width:20%">
                                  Condition
                                </th>
                                <th class="tdr tdrD th-clr">
                                  Message
                                </th>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl ">
                                  Battery Health
                                </td>
                                <td class="tdr">
                                  <?=$batHel['rate']?>
                                </td>
                                <td class="tdr">
                                  <?=$batHel['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$batHel['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Turbocharger Health
                                </td>
                                <td class="tdr">
                                  <?=$turbo['rate']?>
                                </td>
                                <td class="tdr">
                                  <?=$turbo['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$turbo['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Air System
                                </td>
                                <td class="tdr">
                                  <?=$air['rate']?>
                                </td>
                                <td class="tdr">
                                  <?=$air['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$air['msg']?>
                                </td>
                                  
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Fuel System
                                </td>
                                <td class="tdr">
                                  <?=$fuel['rate']?>
                                </td>
                                <td class="tdr">
                                  <?=$fuel['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$fuel['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Combustion Pressure
                                </td>
                                <td class="tdr">
                                  <?=$copPres['rate']?>
                                </td>
                                <td class="tdr">
                                  <?=$copPres['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$copPres['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Rpm Oscillation Anomaly Analysis 
                                </td>
                                <td class="tdr">
                                  <?=$rcOcil['rate']?>
                                </td>
                                <td class="tdr">
                                  <?=$rcOcil['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$rcOcil['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  False Odometer Reading Detection
                                </td>
                                <td class="tdr">
                                  <?=$falOdMr['rate']?>
                                </td>
                                <td class="tdr">
                                  <?=$falOdMr['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$falOdMr['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Engine Cooling System
                                </td>
                                <td class="tdr">
                                  <?=$coolTemp['rate']?>
                                </td>
                                <td class="tdr">
                                  <?=$coolTemp['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$coolTemp['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdrb tdl">
                                  Vehicle Speed Sensor Malfunction
                                </td>
                                <td class="tdr tdrb">
                                  
                                </td>
                                <td class="tdr tdrb">
                                  
                                </td>
                                <td class="tdr tdrb">
                                  <?=$vehSpeed['msg']?>
                                </td>
                              </tr>
                            <?php } ?>
                            </table>    
                          </div>
                        </div>
                      </div>  
                    </div>
                                <!----------------- Calib End Diagnostics -------------------->
                            <!----------------- Monitoring Start Diagnostics -------------------->
                    <div class="tab-pane" id="link3">
                      <div class="container-fluid">
                        <div class="continues">
                          <div class="row classes">
                            <table id="datatables1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                              
                              <tr>
                                <th class="tdr tdrA tdl th-clr" style="width:20%">
                                  Component
                                </td>
                                <th class="tdr tdrB th-clr" style="width:13%">
                                  Ratings
                                </th>
                                <th class="tdr tdrC th-clr" style="width:20%">
                                  Condition
                                </th>
                                <th class="tdr tdrD th-clr">
                                  Message
                                </th>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl ">
                                  Battery Health
                                </td>
                                <td class="tdr">
                                  <?=$batHel1['rate']?>
                                  <?php echo ratingstring($batHel1['rate']); ?>
                                </td>
                                <td class="tdr">
                                  <?=$batHel1['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$batHel1['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Turbocharger Health

                                </td>
                                <td class="tdr">
                                  <?=$turbo1['rate']?>
                                  <?php echo ratingstring($turbo1['rate']); ?>
                                </td>
                                <td class="tdr">
                                  <?=$turbo1['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$turbo1['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Air System
                                </td>
                                <td class="tdr">
                                  <?=$air1['rate']?>
                                  <?php echo ratingstring($air1['rate']); ?>
                                </td>
                                <td class="tdr">
                                  <?=$air1['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$air1['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Fuel System
                                </td>
                                <td class="tdr">
                                  <?=$fuel1['rate']?>
                                  <?php echo ratingstring($fuel1['rate']); ?>
                                </td>
                                <td class="tdr">
                                  <?=$fuel1['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$fuel1['msg']?>
                                </td>
                              </tr>
                              
                            
                              
                              <tr>
                                <td class="tdr tdl">
                                  Combustion Pressure
                                </td>
                                <td class="tdr">
                                  <?=$copPres1['rate']?>
                                  <?php echo ratingstring($copPres1['rate']); ?>
                                </td>
                                <td class="tdr">
                                  <?=$copPres1['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$copPres1['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Rpm Oscillation Anomaly Analysis 
                                </td>
                                <td class="tdr">
                                  <?=$rcOcil1['rate']?>
                                  <?php echo ratingstring($rcOcil1['rate']); ?>
                                </td>
                                <td class="tdr">
                                  <?=$rcOcil1['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$rcOcil1['msg']?>
                                </td>
                              </tr>

                              <tr>
                                <td class="tdr tdl">
                                  False Odometer Reading Detection
                                </td>
                                <td class="tdr">
                                  
                                </td>
                                <td class="tdr">
                                  
                                </td>
                                <td class="tdr">
                                  <?=$falOdMr1['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdl">
                                  Engine Cooling System
                                </td>
                                <td class="tdr">
                                  <?=$coolTemp1['rate']?>
                                   <?php echo ratingstring($coolTemp1['rate']); ?>
                                </td>
                                <td class="tdr">
                                  <?=$coolTemp1['con']?>
                                </td>
                                <td class="tdr">
                                  <?=$coolTemp1['msg']?>
                                </td>
                              </tr>
                              
                              <tr>
                                <td class="tdr tdrb tdl">
                                  Vehicle Speed Sensor malfunction
                                </td>
                                <td class="tdr tdrb">
                                  
                                </td>
                                <td class="tdr tdrb">
                                  
                                </td>
                                <td class="tdr tdrb">
                                  <?=$vehSpeed1['msg']?>
                                </td>
                              </tr>
                              
                            </table>
                          </div>
                        </div>
                      </div>  
                    </div>
                    <!----------------- Monitoring End Diagnostics -------------------->
                    <!-- health trend -->
                     <div class="tab-pane" id="link4">
                       <div class=""> 
                       <div class="row">
                    
                       <input type="hidden" id="devid" name="devid" value="<?= $did ?>">

                      <div class="col-md-4">
                         
                          <select    onchange="getgraph()"  title="Select Parameter Type" data-size="7" name="ptype" id="ptype" tabindex="-98" >
                             
                               <option value="1" <?php if($option==''){echo 'selected'; } ?>  >Battery Health</option>
                               <option value="2"   >Turbocharger Health</option>
                               <option value="3"  <?php if($option=='16'){echo 'selected'; } ?>  >Air System</option>
                               <option value="4" <?php if($option=='5'){echo 'selected'; } ?>  >Fuel System</option>
                               <option value="5"   >Combustion Pressure</option>
                               <option value="6"   >Rpm Oscillation Anomaly Analysis</option>
                              <!--  <option value="7"   >False Odometer Reading Detection</option> -->
                               <option value="8" <?php if($option=='2'){echo 'selected'; } ?>  >Engine Cooling System</option>
                              <!--  <option value="9"   >Vehicle Speed Sensor Malfunction</option> -->
                             
                          </select>
                        
                      </div>

                      <div class="col-md-3" >

                          <select  id="year" onchange="getgraph()" data-size="7" title="Single Select">
                            <option value="0">Pick Year</option>
                            <option <?php if(!empty($year)){ if($year == '2018'){ ?> selected <?php } } ?> value="2018">Year 2018</option>
                            <option <?php if(!empty($year)){ if($year == '2019'){ ?> selected <?php } } ?> value="2019">Year 2019</option>
                            <option <?php if(!empty($year)){ if($year == '2020'){ ?> selected <?php } } ?>   value="2020">Year 2020</option>
                            <option <?php if(!empty($year)){ if($year == '2021'){ ?> selected <?php } } ?> value="2021" selected>Year 2021</option>
                            <option <?php if(!empty($year)){ if($year == '2022'){ ?> selected <?php } } ?> value="2022">Year 2022</option>
                            <option <?php if(!empty($year)){ if($year == '2023'){ ?> selected <?php } } ?> value="2023">Year 2023</option>
                            <option <?php if(!empty($year)){ if($year == '2024'){ ?> selected <?php } } ?> value="2024">Year 2024</option>
                            <option <?php if(!empty($year)){ if($year == '2025'){ ?> selected <?php } } ?> value="2025">Year 2025</option>
                          </select>
                  </div>
                   <div class="col-md-3" style="margin-left: 51px;">

                          <select  id="month" onchange="getgraph()" data-size="7" title="Single Select">
                            <option value="0" selected>Pick Month</option>
                            <option  value="1">January</option>
                            <option  value="2">February</option>
                            <option  value="3">March</option>
                            <option  value="4">April</option>
                            <option  value="5">May</option>
                            <option  value="6">June</option>
                            <option  value="7">July</option>
                            <option  value="8">August</option>
                            <option  value="9">September</option>
                            <option  value="10">October</option>
                            <option  value="11">November</option>
                            <option  value="12">December</option>
                          </select>
                  </div>
                  </div>

                    
                  </div>
                   <div class="card-body " id="graph"> 
                   </div>
                    
                    </div>
                    <!-- end of health trend -->
                </div>
              </div>
<?php 
function ratingstring($rating)
{
   $stringrate='';
  if($rating == 1)
  {
    $stringrate ='<span id="rateMe">
    <i class="fa fa-star twoStars" data-index="0" data-html="true" data-toggle="popover"
      data-placement="top" title="Very bad"></i>
    <i class="fa fa-star " data-index="1" data-html="true" data-toggle="popover"
      data-placement="top" title="Poor"></i>
    <i class="fa fa-star" data-index="2" data-html="true" data-toggle="popover"
      data-placement="top" title="OK"></i>
    <i class="fa fa-star" data-index="3" data-html="true" data-toggle="popover"
      data-placement="top" title="Good"></i>
    <i class="fa fa-star" data-index="4" data-html="true" data-toggle="popover"
      data-placement="top" title="Excellent"></i>
  </span>';
  }
  elseif($rating == 2)
  {
    $stringrate ='<span id="rateMe">
    <i class="fa fa-star twoStars" data-index="0" data-html="true" data-toggle="popover"
      data-placement="top" title="Very bad"></i>
    <i class="fa fa-star twoStars" data-index="1" data-html="true" data-toggle="popover"
      data-placement="top" title="Poor"></i>
    <i class="fa fa-star" data-index="2" data-html="true" data-toggle="popover"
      data-placement="top" title="OK"></i>
    <i class="fa fa-star" data-index="3" data-html="true" data-toggle="popover"
      data-placement="top" title="Good"></i>
    <i class="fa fa-star" data-index="4" data-html="true" data-toggle="popover"
      data-placement="top" title="Excellent"></i>
  </span>';
  }
  elseif($rating == 3)
  {
    $stringrate ='<span id="rateMe">
    <i class="fa fa-star threeStars" data-index="0" data-html="true" data-toggle="popover"
      data-placement="top" title="Very bad"></i>
    <i class="fa fa-star threeStars" data-index="1" data-html="true" data-toggle="popover"
      data-placement="top" title="Poor"></i>
    <i class="fa fa-star threeStars" data-index="2" data-html="true" data-toggle="popover"
      data-placement="top" title="OK"></i>
    <i class="fa fa-star" data-index="3" data-html="true" data-toggle="popover"
      data-placement="top" title="Good"></i>
    <i class="fa fa-star" data-index="4" data-html="true" data-toggle="popover"
      data-placement="top" title="Excellent"></i>
  </span>';

  }
  elseif($rating == 4)
  {
    $stringrate ='<span id="rateMe">
    <i class="fa fa-star fourStars" data-index="0" data-html="true" data-toggle="popover"
      data-placement="top" title="Very bad"></i>
    <i class="fa fa-star fourStars" data-index="1" data-html="true" data-toggle="popover"
      data-placement="top" title="Poor"></i>
    <i class="fa fa-star fourStars" data-index="2" data-html="true" data-toggle="popover"
      data-placement="top" title="OK"></i>
    <i class="fa fa-star fourStars" data-index="3" data-html="true" data-toggle="popover"
      data-placement="top" title="Good"></i>
    <i class="fa fa-star " data-index="4" data-html="true" data-toggle="popover"
      data-placement="top" title="Excellent"></i>
  </span>';
  
  }
  elseif($rating == 5)
  {
    $stringrate ='<span id="rateMe">
    <i class="fa fa-star fiveStars" data-index="0" data-html="true" data-toggle="popover"
      data-placement="top" title="Very bad"></i>
    <i class="fa fa-star fiveStars" data-index="1" data-html="true" data-toggle="popover"
      data-placement="top" title="Poor"></i>
    <i class="fa fa-star fiveStars" data-index="2" data-html="true" data-toggle="popover"
      data-placement="top" title="OK"></i>
    <i class="fa fa-star fiveStars" data-index="3" data-html="true" data-toggle="popover"
      data-placement="top" title="Good"></i>
    <i class="fa fa-star fiveStars" data-index="4" data-html="true" data-toggle="popover"
      data-placement="top" title="Excellent"></i>
  </span>';
  
  }
  return $stringrate;
}
?>
<script type="text/javascript">
  $('select').selectpicker();

               function getgraph(){
                    /*  var date = $('#datetimepicker12').datepicker('getFormattedDate');*/
                    var e = document.getElementById("ptype");
                    var ptype = e.options[e.selectedIndex].value;
                    // alert(ptype); //ID002
                    var e1 = document.getElementById("year");
                    var year = e1.options[e1.selectedIndex].value;
                    // alert(year);
                    var e2 = document.getElementById("month");
                    var month = e2.options[e2.selectedIndex].value;
                   
                      
                      var did   ="{{$did}}";
                       // var option   ="{{$option}}";
                      // alert(did);
                  $.post('{{url('/')}}/getgraph', { did: did, ptype:ptype,year:year,month:month }).done(function (data) {
                           $('#graph').html(data);
                  });
              }
              // setTimeout(() => { console.log("World!"); }, 2000);
              var option   ="{{$option}}";
              if(option != ''){
                document.getElementById('graphb').click();
              } 


$(document).ready(function() {
   
     // $('#datatables').DataTable({
     //   "pagingType": "full_numbers",
     //   "lengthMenu": [
     //     [10, 25, 50, -1],
     //     [10, 25, 50, "All"]
     //   ],
     //   responsive: true,
     //   language: {
     //     search: "_INPUT_",
     //     searchvalue: "Search records",
     //   }
     // });

     //   $('#datatables1').DataTable({
     //   "pagingType": "full_numbers",
     //   "lengthMenu": [
     //     [10, 25, 50, -1],
     //     [10, 25, 50, "All"]
     //   ],
     //   responsive: true,
     //   language: {
     //     search: "_INPUT_",
     //     searchvalue: "Search records",
     //   }
     // });
   });

     </script>