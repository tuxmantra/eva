  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu');
      <!-- End Navbar -->

      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
              <span id="listLoad" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
            <div class="col-md-12" id="success" align="center" style="display: none;">
                <span style="color: red; font-size: 12px; font-weight: 500;">Date Deleted Successfully</span>
            </div>
            <div class="col-md-12" id="unsuccess" align="center" style="display: none;">
                <span style="color: red; font-size: 12px; font-weight: 500;">Data Not Deleted...</span>
            </div>
            <div class="col-md-12" id="req" align="center" style="display: none;">
                <span style="color: red; font-size: 12px; font-weight: 500;">Fields required...</span>
            </div>
                <!-- <div class="col-md-12" align="right">
            <button class="btn btn-primary btn-sm" onclick="goBack()">Go Back</button>
          </div> -->
          <div class="modal fade" id="your-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><b>Are You Sure You want to delete that record</b></h4>
     

            </div>
            <div class="modal-body">
            <button class="btn btn-primary pull-right" onclick="myFun();">Delete</button>
           <button type="reset" class="btn btn-danger pull-right" onClick="window.location='{{url('/')}}/database-optimization';">Cancel</button>



            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="your-modal-range" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><b>Are You Sure You want to delete the record before the date</b></h4>
     

            </div>
            <div class="modal-body">
            <button class="btn btn-primary pull-right" onclick="myFun1();">Delete</button>
           <button type="reset" class="btn btn-danger pull-right" onClick="window.location='{{url('/')}}/database-optimization';">Cancel</button>



            </div>

        </div>
    </div>
</div>
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Clear Data Befor Date</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <form method="post" name="myform" id="myform" autocomplete="off" class="form-horizontal">
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Date Before</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                                <input type='text' placeholder="Date Before" class="form-control" id='datetimepicker4' />
                   
                </div>
                        </div>
                      </div>
                      <div class="row">
                      <label class="col-sm-2 col-form-label">Device Id</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="device_id" id="device_id" placeholder="Enter Device Id">
                        </div>
                      </div>
                    </div>
                    <!-- <button class='btn btn-danger btn-xs' type="submit" name="remove_levels" value="delete"><span class="fa fa-times"></span> delete</button> -->
                    <button type="button" class="btn btn-fill btn-rose pull-right delete" name="remove_levels" id="dlt" onclick="clearDateBeforeDate();">Clear</button>
                 
                  </form>
                </div>
              </div>     
            </div>
            <div class="col-md-12" id="success1" align="center" style="display: none;">
                <span style="color: red; font-size: 12px; font-weight: 500;">Data Range Deleted Successfully...</span>
            </div>
            
                  <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Clear Data Date Range</h4>
                  </div>
                </div>
                 <div class="card-body ">
                  <form method="post" action="create-user" autocomplete="off" class="form-horizontal">
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Date Range</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                                <input data-validate="NotEmpty" data-label="Date Range" class="form-control"  type="text" placeholder="Date Range" id="datetimepicker5" name="datetimepicker5" onkeyup="this.value='<?=date('DD/MM/YYYY - DD/MM/YYYY')?>'" />
                   
                </div>
                        </div>
                      </div>
                      <div class="row">
                      <label class="col-sm-2 col-form-label">Device Id</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="device_id" id="device_idr" placeholder="Enter Device Id">
                        </div>
                      </div>
                    </div>
                    <button onclick="clearDateBeforeRange_new();"  type="button" class="btn btn-fill btn-rose pull-right">Clear</button>
                 
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
            
      

          </div>
        </div>
      </div>
       </form>

    </div>
  </div>
 <!--  <div class="fixed-plugin">
    <div class="dropdown show-dropdown">
      <a href="#" data-toggle="dropdown">
        <i class="fa fa-cog fa-2x"> </i>
      </a>
      <ul class="dropdown-menu">
        <li class="header-title"> Sidebar Filters</li>
        <li class="adjustments-line">
          <a href="javascript:void(0)" class="switch-trigger active-color">
            <div class="badge-colors ml-auto mr-auto">
              <span class="badge filter badge-purple" data-color="purple"></span>
              <span class="badge filter badge-azure" data-color="azure"></span>
              <span class="badge filter badge-green" data-color="green"></span>
              <span class="badge filter badge-warning" data-color="orange"></span>
              <span class="badge filter badge-danger" data-color="danger"></span>
              <span class="badge filter badge-rose active" data-color="rose"></span>
            </div>
            <div class="clearfix"></div>
          </a>
        </li>
        <li class="header-title">Sidebar Background</li>
        <li class="adjustments-line">
          <a href="javascript:void(0)" class="switch-trigger background-color">
            <div class="ml-auto mr-auto">
              <span class="badge filter badge-black active" data-background-color="black"></span>
              <span class="badge filter badge-white" data-background-color="white"></span>
              <span class="badge filter badge-red" data-background-color="red"></span>
            </div>
            <div class="clearfix"></div>
          </a>
        </li>
        <li class="adjustments-line">
          <a href="javascript:void(0)" class="switch-trigger">
            <p>Sidebar Mini</p>
            <label class="ml-auto">
              <div class="togglebutton switch-sidebar-mini">
                <label>
                  <input type="checkbox">
                  <span class="toggle"></span>
                </label>
              </div>
            </label>
            <div class="clearfix"></div>
          </a>
        </li>
        <li class="adjustments-line">
          <a href="javascript:void(0)" class="switch-trigger">
            <p>Sidebar Images</p>
            <label class="switch-mini ml-auto">
              <div class="togglebutton switch-sidebar-image">
                <label>
                  <input type="checkbox" checked="">
                  <span class="toggle"></span>
                </label>
              </div>
            </label>
            <div class="clearfix"></div>
          </a>
        </li>
        <li class="header-title">Images</li>
        <li class="active">
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img src="public/assets/img/sidebar-1.jpg" alt="">
          </a>
        </li>
        <li>
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img src="public/assets/img/sidebar-2.jpg" alt="">
          </a>
        </li>
        <li>
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img src="public/assets/img/sidebar-3.jpg" alt="">
          </a>
        </li>
        <li>
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img src="public/assets/img/sidebar-4.jpg" alt="">
          </a>
        </li>
        <li class="button-container">
          <a href="https://www.creative-tim.com/product/material-dashboard-pro" target="_blank" class="btn btn-rose btn-block btn-fill">Buy Now</a>
          <a href="https://demos.creative-tim.com/material-dashboard-pro/docs/2.1/getting-started/introduction.html" target="_blank" class="btn btn-default btn-block">
            Documentation
          </a>
          <a href="https://www.creative-tim.com/product/material-dashboard" target="_blank" class="btn btn-info btn-block">
            Get Free Demo!
          </a>
        </li>
        <li class="button-container github-star">
          <a class="github-button" href="https://github.com/creativetimofficial/ct-material-dashboard-pro" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star ntkme/github-buttons on GitHub">Star</a>
        </li>
        <li class="header-title">Thank you for 95 shares!</li>
        <li class="button-container text-center">
          <button id="twitter" class="btn btn-round btn-twitter"><i class="fa fa-twitter"></i> &middot; 45</button>
          <button id="facebook" class="btn btn-round btn-facebook"><i class="fa fa-facebook-f"></i> &middot; 50</button>
          <br>
          <br>
        </li>
      </ul>
    </div>
  </div> -->
  <!--   Core JS Files   -->
 @include('default.footer')
 <script>
  function clearDateBeforeRange_new(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var fdate      = $("#datetimepicker5").val();
  var device_idr = $("#device_idr").val();
   $.ajax({
          type:'POST',
          url:  url+'clear-db-optimize-range',
          data: { 'date2': fdate,
                  'device_id2' : device_idr  },
          success: function(data){
           /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
            Swal.fire({
            type: 'success',
            title: 'Success',
            text: 'Data Range Deleted Successfully',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
           // $('#success1').show();
          }
        });
}
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker({
                  format: 'DD/MM/YYYY'
                });
            });
          </script>
          <script type="text/javascript">
          
               $(document).ready(function () { 
    //alert( moment());    
        $('#datetimepicker5').daterangepicker(
          {format: 'DD/MM/YYYY',
            maxDate: moment().add(30, 'days'),
          startDate: moment().add(-30, 'days'),
          endDate: moment().add(30, 'days')
          },          
          function(start, end) {
          //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
          //actDriveStatiDataDate();
        }       
        ); 
    });
  

        </script>
   <script>
    $(function() {
          $('#dlt').click(function(event) {
             $('#success').hide();
             $('#unsuccess').hide();
             $('#req').hide();
             var date      = $("#datetimepicker4").val();
             var device_id = $("#device_id").val();
             if(date == '' || device_id == ''){
               Swal.fire({
            type: 'info',
            title: 'Info',
            text: 'Fields required',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
                 // $('#req').show();
             }else{
                 event.preventDefault();
                 $('#your-modal').modal('toggle');
             }
             
             });
    });
</script>
<script type="text/javascript">
  function myFun(){
        var date      = $("#datetimepicker4").val();
        var device_id = $("#device_id").val();

        $.ajax({
          type: 'POST',
          url:  url+'clear-db-optimize',
            data: { 'date1': date,
                  'device_id' : device_id  },
        success: function(data) {
           $('#your-modal').modal('toggle');
           if(data == 1){
         $("#success").show();
           }
           else{
             $("#unsuccess").show();
           }
          }
        });

  }
   function myFun1(){
     var fdate      = $("#datetimepicker5").val();
     var device_idr = $("#device_idr").val();

        $.ajax({
          type:'POST',
          url:  url+'clear-db-optimize-range',
          data: { 'date2': fdate,
                  'device_id2' : device_idr  },
         success: function(data) {
           $('#your-modal-range').modal('toggle');
           if(data == 1){
         $("#success").show();
           }
           else{
             $("#unsuccess").show();
           }
          }
        });

  }
</script>