  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="center">
              <span class="bmd-form-group serve_message" style="color: red; font-size: 13px; font-weight: 500;">
                     
                    
                   
              </span>
              <span class="bmd-form-group cli_message" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
            

                
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">System Settings</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <form method="post" action="update-system-settings" autocomplete="off" class="form-horizontal" onsubmit="return updateSystemSettings_new()" enctype="multipart/form-data">
                   <input type="hidden" value="{{$form_token}}" name="form_token">
   
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Name</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="{{$setting_data->sett_name}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Mobile</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="number" name="mobile" id="mobile" class="form-control" placeholder="Enter Mobile" value="{{$setting_data->sett_phn}}">
                        </div>
                      </div>
                    </div>
                    

                     <div class="row">
                      <label class="col-sm-2 col-form-label">Address</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea autocomplete="off" class="form-control" cols="5" rows="5" name="address" id="address">{{$setting_data->sett_addr}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Website</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="website" id="website" placeholder="Enter Website" value="{{$setting_data->sett_web}}">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Email</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="false" type="text" name="email" value="{{$setting_data->sett_email}}" id="email" class="form-control" placeholder="Enter Email">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Pincode</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="false" type="number" name="pincode" value="{{$setting_data->sett_pin}}" id="pincode" class="form-control" placeholder="Enter Pincode">
                        </div>
                      </div>
                    </div>

                     <div class="row">
                      <label class="col-sm-2 col-form-label">Location</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="false" type="text" name="location" value="{{$setting_data->sett_loc}}" id="location" class="form-control" placeholder="Enter Location">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">City</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="false" type="text" name="city" value="{{$setting_data->sett_city}}" id="city" class="form-control" placeholder="Enter City">
                        </div>
                      </div>
                    </div>
                    

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Calibration Variables</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea autocomplete="off" class="form-control" cols="20" rows="20" name="callist" id="callist">{{$setting_data->callist}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Logo</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <div>
                          <span class="btn btn-rose btn-round btn-file">
                            <span class="fileinput-new">Select image</span>
                            
                            <input type="hidden"><input onchange="readURL(this);" type="file" name="logo" id="logo" accept="image/*">
                          <div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 81.7969px; top: 14.5px; background-color: rgb(255, 255, 255); transform: scale(17.6992);"></div></div></span>

                          <button  class="btn btn-danger btn-round " onClick="delete_logo($setting_data->sett_id);"><i class="fa fa-times"></i> Remove</button>
                        </div>
                        <img style="width: 150px; height: 120px;" id="blah" src="{{url('/').'/public/uploads/'.$setting_data->sett_logo}}" alt="your image" />
                        </div>
                      </div>
                    </div>
           
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>

            </div>
                        <div class="col-md-12" align="center">
              <span class="bmd-form-group serve_message" style="color: red; font-size: 13px; font-weight: 500;">
                     
                     
              </span>
              <span class="bmd-form-group cli_message" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          </div>
        </div>
      </div>
       </form>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
 @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif

 <script>
  function updateSystemSettings_new(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('.serve_message').hide();
  $('.cli_message').hide();

  var name      = $('#name').val();
  var email     = $('#email').val();
  var mobile    = $('#mobile').val();
  var address   = $('#address').val();
  var website   = $('#address').val();
  var pincode   = $('#pincode').val();
  var location  = $('#location').val();
  var city      = $('#city').val();
  var callist   = $('#callist').val();

  if(name == '' || email == '' || mobile == '' || address == '' || website == '' || pincode == '' || location == '' || city == '' || callist == ''){
      Swal.fire({
            type: 'info',
            title: 'info',
            text: 'Fields required.',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
      
      return false;
  }else if(mobile.length < 5){
        Swal.fire({
            type: 'info',
            title: 'info',
            text: 'Mobile lenght minimum 6 digits.',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
      return false;
  }else if(pincode.length <= 4){
    Swal.fire({
            type: 'info',
            title: 'info',
            text: 'pincode lenght minimum 5 digits',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
      return false;
  }else if(validateEmail(email) != true){
    Swal.fire({
            type: 'info',
            title: 'info',
            text: 'Email format incorrect.',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
     
      return false;
  }else{
      return true;
  }
}

function delete_logo(id){
 {
  // alert('Hi');
  Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#eb262d',
                cancelButtonColor: '#eb262d',
                confirmButtonText: 'Yes, Delete it!'
            }).then((result) => {
              if(result.value){
                         $.ajax({
                      type:'POST',
                      url: '{{url('/')}}/system-settings-delete-logo',
                      data: { 'id': id},
                      success: function (data) {
                        // location.reload();
                      }
                    });
              }
            })

      }

      }


 </script>