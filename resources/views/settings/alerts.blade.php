  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
  </style>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
                    <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
            <div class="col-md-12" align="right">
              <a href="{{url('/')}}/add-alerts" title="Add Alert" class="btn btn-primary btn-sm">Add</a>
              <button class="btn btn-primary btn-sm" onclick="goBack()">Go Back</button>
            </div>
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  <h4 class="card-title">Mail Alerts</h4>
                </div>
                <div class="card-body">
                  <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>
                  <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="table-heading-font" style="background-color: lemonchiffon; font-weight: 400;">S.No</th>
                          <th class="table-heading-font" style="background-color: floralwhite; font-weight: 400;">Category</th>
                          <th class="table-heading-font" style="background-color: floralwhite; font-weight: 400;">Alert Sub Category</th>
                          <th class="table-heading-font" style="background-color: beige; font-weight: 400;">Type</th>
                        
                          <th class="table-heading-font" style="background-color: lemonchiffon; font-weight: 400;">Admin</th>
                          <th class="table-heading-font" style="background-color: floralwhite; font-weight: 400;">User</th>
                          <th class="table-heading-font" style="background-color: cornsilk; font-weight: 400;">Approval</th>
                            <th class="table-heading-font" style="background-color: floralwhite; font-weight: 400;">Status</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th class="table-heading-font" style="background-color: lemonchiffon; font-weight: 400;">S.No</th>
                          <th class="table-heading-font" style="background-color: floralwhite; font-weight: 400;">Category</th>
                          <th class="table-heading-font" style="background-color: floralwhite; font-weight: 400;">Alert Sub Category</th>
                          <th class="table-heading-font" style="background-color: beige; font-weight: 400;">Type</th>
                          <th class="table-heading-font" style="background-color: lemonchiffon; font-weight: 400;">Admin</th>
                          <th class="table-heading-font" style="background-color: floralwhite; font-weight: 400;">User</th>
                          <th class="table-heading-font" style="background-color: cornsilk; font-weight: 400;">Approval</th>
                          <th class="table-heading-font" style="background-color: floralwhite; font-weight: 400;">Status</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @if (!empty($alert_mails))
                        <?php $id = 1; ?>
                        @foreach ($alert_mails as $asc)  
                        <tr>
                         
                          <td class="table-heading-font" style="background-color: lemonchiffon;">{{$id}}</td>
                          <td style="background-color: floralwhite;" class="table-heading-font">{{App\Helpers\Helper::getAlertCategoryName($asc->alert_category_id)}}</td>
                          <td style="background-color: beige;" class="table-heading-font">{{App\Helpers\Helper::getAlertSubCategoryName($asc->alert_sub_category_id)}}</td>
                          <td style="background-color: lemonchiffon;" class="table-heading-font">{{App\Helpers\Helper::getAlertStatusName($asc->alert_status)}}</td>
                          <td style="background-color: cornsilk;" class="table-heading-font"><input type="checkbox" <?php if($asc->to_admin == 1){ ?> checked <?php } ?> ></td>
                          <td style="background-color: cornsilk;" class="table-heading-font"><input type="checkbox" <?php if($asc->to_user == 1){ ?> checked <?php } ?> ></td>
                          <td style="background-color: cornsilk;" class="table-heading-font"><input type="checkbox" <?php if($asc->to_approval == 1){ ?> checked <?php } ?> ></td>
                          <td style="background-color: cornsilk;" class="table-heading-font">{{App\Helpers\Helper::getStatus($asc->status)}}</td>
        
                        </tr>
                        <?php $id++; ?>  
                        @endforeach
                        @else
                           <tr><td colspan="9" align="center">No Alerts Exist.</td></tr>
                        @endif
                       
                       
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href="https://www.creative-tim.com">
                  Creative Tim
                </a>
              </li>
              <li>
                <a href="https://creative-tim.com/presentation">
                  About Us
                </a>
              </li>
              <li>
                <a href="http://blog.creative-tim.com">
                  Blog
                </a>
              </li>
              <li>
                <a href="https://www.creative-tim.com/license">
                  Licenses
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
          </div>
        </div>
      </footer>
    </div>
  </div>
  <div class="fixed-plugin">
    <div class="dropdown show-dropdown">
      <a href="#" data-toggle="dropdown">
        <i class="fa fa-cog fa-2x"> </i>
      </a>
      <ul class="dropdown-menu">
        <li class="header-title"> Sidebar Filters</li>
        <li class="adjustments-line">
          <a href="javascript:void(0)" class="switch-trigger active-color">
            <div class="badge-colors ml-auto mr-auto">
              <span class="badge filter badge-purple" data-color="purple"></span>
              <span class="badge filter badge-azure" data-color="azure"></span>
              <span class="badge filter badge-green" data-color="green"></span>
              <span class="badge filter badge-warning" data-color="orange"></span>
              <span class="badge filter badge-danger" data-color="danger"></span>
              <span class="badge filter badge-rose active" data-color="rose"></span>
            </div>
            <div class="clearfix"></div>
          </a>
        </li>
        <li class="header-title">Sidebar Background</li>
        <li class="adjustments-line">
          <a href="javascript:void(0)" class="switch-trigger background-color">
            <div class="ml-auto mr-auto">
              <span class="badge filter badge-black active" data-background-color="black"></span>
              <span class="badge filter badge-white" data-background-color="white"></span>
              <span class="badge filter badge-red" data-background-color="red"></span>
            </div>
            <div class="clearfix"></div>
          </a>
        </li>
        <li class="adjustments-line">
          <a href="javascript:void(0)" class="switch-trigger">
            <p>Sidebar Mini</p>
            <label class="ml-auto">
              <div class="togglebutton switch-sidebar-mini">
                <label>
                  <input type="checkbox">
                  <span class="toggle"></span>
                </label>
              </div>
            </label>
            <div class="clearfix"></div>
          </a>
        </li>
        <li class="adjustments-line">
          <a href="javascript:void(0)" class="switch-trigger">
            <p>Sidebar Images</p>
            <label class="switch-mini ml-auto">
              <div class="togglebutton switch-sidebar-image">
                <label>
                  <input type="checkbox" checked="">
                  <span class="toggle"></span>
                </label>
              </div>
            </label>
            <div class="clearfix"></div>
          </a>
        </li>
        <li class="header-title">Images</li>
        <li class="active">
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img src="public/assets/img/sidebar-1.jpg" alt="">
          </a>
        </li>
        <li>
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img src="public/assets/img/sidebar-2.jpg" alt="">
          </a>
        </li>
        <li>
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img src="public/assets/img/sidebar-3.jpg" alt="">
          </a>
        </li>
        <li>
          <a class="img-holder switch-trigger" href="javascript:void(0)">
            <img src="public/assets/img/sidebar-4.jpg" alt="">
          </a>
        </li>
        <li class="button-container">
          <a href="https://www.creative-tim.com/product/material-dashboard-pro" target="_blank" class="btn btn-rose btn-block btn-fill">Buy Now</a>
          <a href="https://demos.creative-tim.com/material-dashboard-pro/docs/2.1/getting-started/introduction.html" target="_blank" class="btn btn-default btn-block">
            Documentation
          </a>
          <a href="https://www.creative-tim.com/product/material-dashboard" target="_blank" class="btn btn-info btn-block">
            Get Free Demo!
          </a>
        </li>
        <li class="button-container github-star">
          <a class="github-button" href="https://github.com/creativetimofficial/ct-material-dashboard-pro" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star ntkme/github-buttons on GitHub">Star</a>
        </li>
        <li class="header-title">Thank you for 95 shares!</li>
        <li class="button-container text-center">
          <button id="twitter" class="btn btn-round btn-twitter"><i class="fa fa-twitter"></i> &middot; 45</button>
          <button id="facebook" class="btn btn-round btn-facebook"><i class="fa fa-facebook-f"></i> &middot; 50</button>
          <br>
          <br>
        </li>
      </ul>
    </div>
  </div>
  <!--   Core JS Files   -->
 @include('default.footer')
 <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });


/*      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });*/
    });
  </script>