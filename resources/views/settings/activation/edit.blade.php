  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="right">
              <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
            <div class="col-md-12" align="right">
            
            </div>
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Edit Active</h4>
                  </div>
                </div>

                <div class="card-body ">
                  <form method="post" action="{{url('/')}}/edit-active-save" autocomplete="off" class="form-horizontal" onsubmit="return editActiveValid_new();">
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Activation Code</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                         <input type="hidden" name="cid" value="{{App\Helpers\Helper::crypt(1,$active_data[0]->id)}}">
                         <input type="hidden" value="{{$form_token}}" name="form_token">
                          <input autocomplete="off" type="text" name="ac_code" id="ac_code" class="form-control" placeholder="Enter Activation Code" value="{{$active_data[0]->activation_code}}">
                        </div>
                      </div>
                    </div>
   
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Used By(Enter If Status is Used)</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="used_by" id="used_by" placeholder="Enter Used By" value="{{$active_data[0]->used_by}}">
                        </div>
                      </div>
                    </div>
                      <div class="row" style="margin-top: 25px;">
                      <label class="col-sm-2 col-form-label label-checkbox">Mac Id(Enter If Status Is Used)</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="mc_id" id="mc_id" placeholder="Enter Mac Id" value="{{$active_data[0]->mac_id}}">
                        </div>
                      </div>
                    </div>
                      <div class="row" style="margin-top: 25px;">
                      <label class="col-sm-2 col-form-label label-checkbox">Expiry Date</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="date" class="form-control" name="exp_dt" id="exp_dt" placeholder="Enter Expiry Date" value="{{$active_data[0]->expdt}}">
                        </div>
                      </div>
                    </div>
                      <div class="row" style="margin-top: 25px;">
                      <label class="col-sm-2 col-form-label label-checkbox">Used Email</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="email" class="form-control" name="used_email" id="used_email" placeholder="Enter Used Email" value="{{$active_data[0]->used_by}}">
                        </div>
                      </div>
                    </div>
                    <div class="row" style="margin-top: 25px;">
                      <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-10 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" 
                            value="1" <?php if($active_data[0]->status == 1){ ?> checked <?php } ?>> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0" <?php if($active_data[0]->status == 0){ ?> checked <?php } ?>> Inactive
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="2" <?php if($active_data[0]->status == 2){ ?> checked <?php } ?>> In Use
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>
              <button type="submit" class="btn btn-fill btn-rose" name="savem" value="savem">Save &amp; Send Mail</button>

            </div>
                   <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                 
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          </div>
        </div>
      </div>
       </form>
      
    </div>
  </div>
  
 @include('default.footer')
 @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif
 <script type="text/javascript">
   function editActiveValid_new(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var ac_code    = $('#ac_code').val();
  var used_by    = $("#used_by").val();
  var mc_id      = $("#mc_id").val();
  var exp_dt     = $("#exp_dt").val();
  var used_email = $("#used_email").val();
  if(ac_code == '' || used_by == '' || exp_dt == '' || used_email == ''){
      Swal.fire({
            type: 'info',
            title: 'Info',
            text: 'Fields required',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
      return false;
  }else{
      return true;
  }
}
 </script>
