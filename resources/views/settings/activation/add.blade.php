  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="right">
              <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
            <div class="col-md-12" align="right">
            
            </div>
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Add Active Data</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <form method="post" action="{{url('/')}}/create-active-type" autocomplete="off" class="form-horizontal" onsubmit="return validateActive_new()">
                    <div class="row">
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input type="hidden" value="{{$form_token}}" name="form_token">
                        
                        </div>
                      </div>
                    </div>
    
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Activation Code</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="ac_code" id="ac_code" value="{{$form_token1}}">
                        </div>
                      </div>
                    </div>
                         <div class="row">
                      <label class="col-sm-2 col-form-label">Used By</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" name="used_by" id="used_by" class="form-control" placeholder="Enter If Status Is Used">
                        </div>
                      </div>
                    </div>
                       <div class="row">
                      <label class="col-sm-2 col-form-label">Mac Id</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" name="mc_id" id="mc_id" class="form-control" placeholder="Enter Mac Id If Status Is Used">
                        </div>
                      </div>
                    </div>
                       <div class="row">
                      <label class="col-sm-2 col-form-label">Expiry Date</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input type='text' class="form-control" name="exp_dt" id='datetimepicker4' />
                        </div>
                      </div>
                    </div>
                       <div class="row">
                      <label class="col-sm-2 col-form-label">Used Email</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" name="u_email" id="u_email" class="form-control" placeholder="Enter Used Email">
                        </div>
                      </div>
                    </div>
                      
                
                 

                    <div class="row" style="margin-top: 25px;">
                      <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-10 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="1" checked> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0"> Inactive
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                         <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="2"> In Use
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>
                    </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose" name="save">Save</button>
              <button type="submit" class="btn btn-fill btn-rose" name="savem" value="savem">Save &amp; Send Mail</button>

            </div>
                  <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                    
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          </div>
        </div>
      </div>
       </form>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
  @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif
 <script type="text/javascript">
            function validateActive_new(){
            /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
            $('#serve_message').hide();
            $('#cli_message').hide();
            var ac_code = $('#ac_code').val();
            var used_by = $('#used_by').val();
            var exp_dt  = $('#datetimepicker4').val();
            var u_email = $('#u_email').val();
            if(ac_code == '' || used_by == '' || exp_dt == '' || u_email == ''){
             Swal.fire({
                                        type: 'info',
                                        title: 'info',
                                        text:   'Fields Required',
                                         confirmButtonColor: '#eb262d',
                                        // footer: '<a href>Why do I have this issue?</a>'
                                      })
              return false;
            }else{
                return true;
            }
          }

            $(function () {
                $('#datetimepicker4').datetimepicker({
                  format: 'DD/MM/YYYY'
                });
            });
          </script>