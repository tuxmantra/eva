  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                    
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>

               
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Vehicle Settings</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <form method="post" action="update-vehicle-settings" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
                   <input type="hidden" value="{{$form_token}}" name="form_token">
                   <input type="hidden" value="{{$setting_data->id}}" name="sett_id">
          

                     <div class="row">
                      <label class="col-sm-2 col-form-label">Manufacturer</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea autocomplete="off" class="form-control" cols="5" rows="5" name="manufacturer" id="manufacturer">{{$setting_data->manufacturer}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Engine Capacity</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea autocomplete="off" class="form-control" cols="5" rows="5" name="capacity" id="capacity">{{$setting_data->engine_capacity}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">City List</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea autocomplete="off" class="form-control" cols="5" rows="5" name="city" id="city">{{$setting_data->citylist}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row" style="margin-top: 25px; margin-bottom: 25px;" align="center">
                   <div class="col-md-12">
                     <hr style="width: 100%;">
                     <label class="col-form-label">Callibration Values</label>
                     <hr style="width: 100%;"> 
                    </div>
                    </div>

                <div class="row">
               <?php 
                $callistN  = $cal_list->callist;
                $callist   =  preg_split ('/$\R?^/m',$callistN);
                 
                for($xi=0;$xi<count($callist);$xi++){

                $i = $xi;
                $calvalp = json_decode($setting_data->calvalp,true);
                $calvald = json_decode($setting_data->calvald,true);

                $var = trim($callist[$i]);
                $varI = $var;
                if($callist[$i] != ""){ 
                ?>
                          <div class="col-md-3">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-weight: 600; font-size: 11px;     color: black;"><?=$callist[$i]?> Petrol</label>
                              <br>
                              <?php if(array_key_exists($varI,$calvalp)){ ?>
                              <input type="text" value="<?=$calvalp[$varI]?>" id="calvalp<?=$i?>" name="<?=$var?>p" class="form-control" placeholder="">
                              <?php }else{ ?>
                              <input type="text" value="" id="calvalp<?=$i?>" name="<?=$var?>p" class="form-control" placeholder="">
                              <?php } ?>
                              
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-weight: 600; font-size: 11px;     color: black;"><?=$callist[$i]?> Diesel</label>
                              <br>
                              <?php if(array_key_exists($varI,$calvald)){ ?>
                              <input type="text" value="<?=$calvald[$varI]?>" id="calvald<?=$i?>" name="<?=$var?>d" class="form-control" placeholder="">
                              <?php }else{ ?>
                              <input type="text" value="" id="calvald<?=$i?>" name="<?=$var?>d" class="form-control" placeholder="">
                              <?php } ?>
                            </div>
                          </div>
                <?php
                }}
                ?>       
                        
                       
                    </div>
           
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>

            </div>
                    <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                     
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          </div>
        </div>
      </div>
       </form>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
 @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif