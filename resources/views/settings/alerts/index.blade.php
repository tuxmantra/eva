  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')

  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
  button.btn.dropdown-toggle.btn-light {
    background: transparent;
    color: #000;
    box-shadow: none;
    padding: 7px;
  }
    /*tfoot {
     display: table-header-group !important;
     background-color: blanchedalmond;
    }*/
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    .ScrollStyle
   {
       max-height: 300px;
       overflow-y: scroll;
   }
   /* tr div {
     width: auto !important;
     z-index: auto !important;
   }*/
   /*.tab-pane tbody {
     display: block;
     max-height: 230px;
     overflow-y: scroll;
   }*/
   .non-highlighted-cal-dates{
 background-color: gainsboro;
}
.tox-statusbar__branding{
  display: none;
}
a { color: inherit; }
a.link1{ color: #eb262d ; }
.dataTables_filter
{
  display: none;
}
.table thead tr th {
    
    width: fit-content !important;
}
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">
               <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>


          <input type="hidden" name="exp" id="exp" value="{{$did}}">
              
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">notification_important</i>
                  </div>
                  <h4 class="card-title">Alerts</h4>
                </div>
                
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-12">
                      <ul class="nav nav-pills nav-pills-warning" role="tablist">
                      <li class="nav-item">
                        <a  class="nav-link active ";    id="activeone" value="1" data-toggle="tab" onclick="failedTest()" href="#link1" role="tablist">
                         Failed Test
                        </a> 
                      </li>

                       <li class="nav-item">
                        <a class="nav-link ";  id="activefive" value="6" data-toggle="tab" onclick="IncompleteTest()" href="#link5" role="tablist">
                         Incomplete Test
                        </a>
                      </li>
                      
                    </ul>
                    <hr>
                      <div class="card-body" id="load_data" >

                 </div>
                  <!-- <div class="card-body" id="load_data1">

                 </div> -->
                    </div>
                  
             
                </div>
                
               </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
     
    </div>
  </div>
 
  <!--   Core JS Files   -->
 @include('default.footer')


<script type="text/javascript">
  $(document).ready(function() 
  {
    failedTest();

  });
function failedTest()
{
  $.get('{{url('/')}}/getFailedTest', { }).done(function (data) 
  {
                 
      $('#load_data').html(data);

      var table = $('#datatables').DataTable({
        "columnDefs": [
    { "width": "20%", "targets": 0 }
  ],
        "ordering": false,
          "pagingType": "full_numbers",
          "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
          ],
          responsive: true
        });

        $('#datatables thead tr').clone(true).appendTo( '#datatables thead' );
        $('#datatables thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title != 'S.No' && title != 'Actions')
            {
              if(title == "Result"){
                    var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                        .appendTo( $(this).empty() )
                        .on( 'change', function () {

                            var val = $(this).val();

                            table.column( 6 ) //Only the first column
                                .search( val ? '^'+$(this).val()+'$' : val, true, false )
                                .draw();
                        } );

                        table.column( 6 ).data().unique().sort().each( function ( d, j ) {
                        if(d != '-'){
                          select.append( '<option value="'+d+'">'+d+'</option>' );
                        }else{
                          select.append( '<option value="'+d+'">Nil</option>' );
                        }
                    } );
                }
                else if(title == "Vehicle")
                {
                    var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                        .appendTo( $(this).empty() )
                        .on( 'change', function () {

                            var val = $(this).val();

                            table.column( 2 ) //Only the first column
                                .search( val ? '^'+$(this).val()+'$' : val, true, false )
                                .draw();
                        } );

                        table.column( 2 ).data().unique().sort().each( function ( d, j ) {
                        if(d != '-'){
                          select.append( '<option value="'+d+'">'+d+'</option>' );
                        }else{
                          select.append( '<option value="'+d+'">Nil</option>' );
                        }
                    } );
                }
                else
                {
                  $(this).html( '<input type="text" placeholder="'+title+'"  />' );
           
                  $( 'input', this ).on( 'keyup change', function () {
                      if ( table.column(i).search() !== this.value ) {
                          table
                              .column(i)
                              .search( this.value )
                              .draw();
                      }
                  } );
                }
            }
        } );
 
        // var table = $('#datatables').DataTable( {
        //     orderCellsTop: true,
        //     fixedHeader: true
        //  } );
   });
}
function IncompleteTest()
{
  $.get('{{url('/')}}/getIncompleateTest', { }).done(function (data) 
  {
                 
      $('#load_data').html(data);

      var table = $('#datatables').DataTable({
        "columnDefs": [
              { "width": "20%", "targets": 0 }
            ],
        "ordering": false,
          "pagingType": "full_numbers",
          "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
          ],
          responsive: true
        });

        $('#datatables thead tr').clone(true).appendTo( '#datatables thead' );
        $('#datatables thead tr:eq(1) th').each( function (i) {
            var title = $(this).text();
            if(title != 'S.No' && title != 'Actions')
            {
              if(title == "Status"){
                    var select = $('<select class="deep" ><option value="">'+title+'</option></select>')
                        .appendTo( $(this).empty() )
                        .on( 'change', function () {

                            var val = $(this).val();

                            table.column( 6 ) //Only the first column
                                .search( val ? '^'+$(this).val()+'$' : val, true, false )
                                .draw();
                        } );

                        table.column( 6 ).data().unique().sort().each( function ( d, j ) {
                        if(d != '-'){
                          select.append( '<option value="'+d+'">'+d+'</option>' );
                        }else{
                          select.append( '<option value="'+d+'">Nil</option>' );
                        }
                    } );
                }
                else if(title == "Vehicle")
                {
                    var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                        .appendTo( $(this).empty() )
                        .on( 'change', function () {

                            var val = $(this).val();

                            table.column( 2 ) //Only the first column
                                .search( val ? '^'+$(this).val()+'$' : val, true, false )
                                .draw();
                        } );

                        table.column( 2 ).data().unique().sort().each( function ( d, j ) {
                        if(d != '-'){
                          select.append( '<option value="'+d+'">'+d+'</option>' );
                        }else{
                          select.append( '<option value="'+d+'">Nil</option>' );
                        }
                    } );
                }
                else
                  if(title == "Date")
                  {
                    $(this).html( '<input type="text" placeholder="'+title+'"/>' );
             
                    $( 'input', this ).on( 'keyup change', function () { 
                        if ( table.column(i).search() !== this.value ) {
                            table
                                .column(i)
                                .search( this.value )
                                .draw();
                        }
                    } );
                  }
                  else
                    if(title == "User")
                    {
                      $(this).html( '<input type="text" placeholder="'+title+'" />' );
               
                      $( 'input', this ).on( 'keyup change', function () { 
                          if ( table.column(i).search() !== this.value ) {
                              table
                                  .column(i)
                                  .search( this.value )
                                  .draw();
                          }
                      } );
                    }
                    else
                    {
                      $(this).html( '<input type="text" placeholder="'+title+'"   />' );
               
                      $( 'input', this ).on( 'keyup change', function () { 
                          if ( table.column(i).search() !== this.value ) {
                              table
                                  .column(i)
                                  .search( this.value )
                                  .draw();
                          }
                      } );
                    }
            }
            
        } );
 
        // var table = $('#datatables').DataTable( {
        //     orderCellsTop: true,
        //     fixedHeader: true
        //  } );
   });
}
</script>