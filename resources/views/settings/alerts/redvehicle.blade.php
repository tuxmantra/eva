<div class="material-datatables">
  <!-- <h3>{{$title}}</h3> -->
                    <table id="datatablesvehicle" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Device ID</th>
                          <th class="table-heading-font">Description</th>
                          <th class="table-heading-font">Score</th>
                          <th class="table-heading-font">Comments</th>
                           <th class="table-heading-font">Action</th>
                          
                        </tr>
                      </thead>
                      <tfoot>
                        <!-- <tr>
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Device ID</th>
                          <th class="table-heading-font">Description</th>
                          <th class="table-heading-font">Score</th>
                        
                          <th class="table-heading-font">Distance</th>
                          <th class="table-heading-font">Comments</th>
                          <th class="table-heading-font">Action</th>
                          
                        </tr> -->
                      </tfoot>
                      <tbody>
                        @if (!empty($redvehicle))
                        <?php //print_r($redvehicle); ?>
                        <?php $id = 1; ?>
                        @foreach ($redvehicle as $value) 
                        <tr>
                          <td class="table-heading-font" style="background-color: #f5f5f5; width:7%;">{{$id}}</td>
                          <td class="table-heading-font" style="width:10%;">
                            <a href="remote-diagnostics?option={{$value->satype}}&devid={{$value->devid}}">
                            {{$value->devid}}
                            </a>
                          </td>
                             <td class="table-heading-font" style="background-color: #f5f5f5; width:10%;">{{$value->name}}</td>
                          
                             <td class="table-heading-font" style=" width:8%; background-color: <?php if($value->rating == 1){echo '#eb2621';}elseif($value->rating == 2){echo '#FF4500';}elseif($value->rating == 3){echo '#f3e714';}?>; color: #333333;">{{$value->rating}}</td>
                             <td class="table-heading-font" style="">{{$value->msg}}</td>
                             <td class="table-heading-font" style="background-color: #f5f5f5;">
                             <a href="remote-diagnostics?option={{$value->satype}}&devid={{$value->devid}}">
                             <span class="material-icons" style="color: #eb262d">
                              remove_red_eye
                              </span>
                               </a>
                             </td>
        
                        </tr>
                        <?php $id++; ?>  
                        @endforeach
                        @else
                           <tr><td colspan="9" align="center">No Alerts Exist.</td></tr>
                        @endif
                       
                       
                      </tbody>
                    </table>
                  </div>

<script>
  $(document).ready(function()  
  {
    $('#datatablesvehicle').DataTable(
    {
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: 
      {
        search: "_INPUT_",
        searchPlaceholder: "Search records",
      }
    });
                  
  });
</script>