  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
        
             <div class="col-md-12" align="right">
              <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
            

            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Add Alert Mails</h4>
                  </div>
                </div>
                <div class="card-body ">
          <form method="post" action="{{url('/')}}/update-mail-alert" class="form-horizontal" onsubmit="return validateAlert_new();">
                  <input type="hidden" value="{{$form_token}}" name="form_token"> 
                  <input type="hidden" name="alert_mail_id" value="{{App\Helpers\Helper::crypt(1,$alert_data[0]->alert_mail_id)}}">                
       
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Category</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                         
                          <select class="selectpicker alert_category" data-style="select-with-transition" title="Select Alert Category" data-size="7" name="alert_category" id="alert_category" tabindex="-98">
                            <option disabled="">Category</option>
                            @if (!empty($alert_category))
                            @foreach ($alert_category as $al)
                            <option <?php if($alert_data[0]->alert_category_id == $al->alert_category_id){ ?> selected <?php } ?> value="{{$al->alert_category_id}}">{{$al->name}}</option>
                            @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Sub Category</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <select class="selectpicker alert_sub_category" data-style="select-with-transition" title="Select Alert Sub Category" data-size="7" name="alert_sub_category" id="alert_sub_category" tabindex="-98">
                            <option disabled="">Sub Category</option>
                           
                          </select>
                        </div>
                      </div>
                    </div>

                     <div class="row">
                      <label class="col-sm-2 col-form-label">Type</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <select class="selectpicker" data-style="select-with-transition" title="Select Alert Type" data-size="7" name="type" id="type" tabindex="-98">
                            <option disabled="">Alert Type</option>

                            @if (!empty($alert_type))
                            @foreach ($alert_type as $at)
                            <option <?php if($alert_data[0]->alert_type == $at->alert_type_id){ ?> selected <?php } ?> value="{{$at->alert_type_id}}">{{$at->alert_type_name}}</option>
                            @endforeach
                            @endif
                           
                          </select>
                        </div>
                      </div>
                    </div>

                    
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Description</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea class="form-control" cols="5" rows="5" name="desc" id="desc">{{$alert_data[0]->desc}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Message</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea class="form-control" cols="5" rows="5" name="msg" id="msg">{{$alert_data[0]->msg}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label label-checkbox">Mail To</label>
                      <div class="col-sm-10 checkbox-radios">
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" <?php if($alert_data[0]->to_admin == 1){ ?> checked <?php } ?> type="checkbox" name="to_admin" value="1"> Admin
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" <?php if($alert_data[0]->to_user == 1){ ?> checked <?php } ?> type="checkbox" name="to_user" value="1"> User
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" <?php if($alert_data[0]->to_approval == 1){ ?> checked <?php } ?> type="checkbox" name="to_approval" value="1"> Approval
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>
  
                    <div class="row">
                      <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-10 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="1" <?php if($alert_data[0]->status == 1){ ?> checked <?php } ?>> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0"> In Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>

                
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>

            </div>
                <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                     
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          </div>
        </div>
      </div>
       </form>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
 @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif
 <script type="text/javascript">
   function validateAlert_new(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  var flag = 0;
  $('#serve_message').hide();
  var alert_category      = $('#alert_category').val();
  var alert_sub_category  = $('#alert_sub_category').val();
  var type                = $('#type').val();
  var desc                = $('#desc').val();
  var msg                 = $('#msg').val();
  var status              = $('#status').val();
  $('#cli_message').hide();
  if(alert_category == '' || alert_sub_category == '' || type == '' || desc == '' || msg == '' || status == ''){
    Swal.fire({
            type: 'info',
            title: 'info',
            text: 'Fields required.',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
      return false;
  }else{
    return true;
  }
}
 </script>

 <script type="text/javascript">
  $( document ).ready(function() {
         var alertCategory = $(".alert_category option:selected").val();
         $.post('{{url('/')}}/get-alert-sub-category', { 'alertCategory' : alertCategory, 'action': 'edit' }).done(function (data) {
                $('#alert_sub_category').html(data).selectpicker('refresh');
                $('#alert_sub_category').val('<?= $alert_data[0]->alert_sub_category_id ?>').selectpicker('refresh');
      });
  });
   </script>