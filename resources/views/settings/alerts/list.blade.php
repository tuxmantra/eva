  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
  </style>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
                    <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                    
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
            <div class="col-md-12" align="right">
              <a href="{{url('/')}}/add-mail-alert" title="Add Alert" class="btn btn-primary btn-sm">Add</a>
              
            </div>
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  <h4 class="card-title">Mail Alerts List</h4>
                </div>
                <div class="card-body">
                  <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>
                  <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Category</th>
                          <th class="table-heading-font">Alert Sub Category</th>
                          <th class="table-heading-font">Type</th>
                        
                          <th class="table-heading-font">Admin</th>
                          <th class="table-heading-font">User</th>
                          <th class="table-heading-font">Approval</th>
                          <th class="table-heading-font">Status</th>
                          <th class="table-heading-font">Action</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Category</th>
                          <th class="table-heading-font">Alert Sub Category</th>
                          <th class="table-heading-font">Type</th>
                          <th class="table-heading-font">Admin</th>
                          <th class="table-heading-font">User</th>
                          <th class="table-heading-font">Approval</th>
                          <th class="table-heading-font">Status</th>
                           <th class="table-heading-font">Action</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @if (!empty($alert_mails))
                        <?php $id = 1; ?>
                        @foreach ($alert_mails as $asc)  
                        <tr>
                         
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$id}}</td>
                          <td style="background-color: #ffffff;" class="table-heading-font">{{App\Helpers\Helper::getAlertCategoryName($asc->alert_category_id)}}</td>
                          <td style="background-color: #f5f5f5;" class="table-heading-font">{{App\Helpers\Helper::getAlertSubCategoryName($asc->alert_sub_category_id)}}</td>
                          <td style="background-color: #ffffff;" class="table-heading-font">{{App\Helpers\Helper::getAlertTypeName($asc->alert_type)}}</td>
                          <td style="background-color: #f5f5f5;" class="table-heading-font"><input type="checkbox" <?php if($asc->to_admin == 1){ ?> checked <?php } ?> ></td>
                          <td style="background-color: #ffffff;" class="table-heading-font"><input type="checkbox" <?php if($asc->to_user == 1){ ?> checked <?php } ?> ></td>
                          <td style="background-color: #f5f5f5;" class="table-heading-font"><input type="checkbox" <?php if($asc->to_approval == 1){ ?> checked <?php } ?> ></td>
                          <td style="background-color: #ffffff;" class="table-heading-font">{{App\Helpers\Helper::getStatus($asc->status)}}</td>
                           <td class="text-right" style="background-color: #f5f5f5;">
                            <a href="<?= url('/')."/edit-mail-alert/"; ?>{{App\Helpers\Helper::crypt(1,$asc->alert_mail_id)}}" class="btn btn-link btn-info btn-just-icon like"><i class="material-icons">edit</i></a>
                          </td>
        
                        </tr>
                        <?php $id++; ?>  
                        @endforeach
                        @else
                           <tr><td colspan="9" align="center">No Alerts Exist.</td></tr>
                        @endif
                       
                       
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
 @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif
 <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });


/*      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });*/
    });
  </script>