<div class="material-datatables">
  <h3>{{$title}}</h3>
                    <table id="datatablesvehicle1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">System</th>
                          <th class="table-heading-font">Operater</th>
                          <th class="table-heading-font">Value</th>
                        
                          <!-- <th class="table-heading-font">Distance</th> -->
                          <th class="table-heading-font">Action</th>
                          
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                           <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">System</th>
                          <th class="table-heading-font">Operater</th>
                          <th class="table-heading-font">Value</th>
                        
                          <!-- <th class="table-heading-font">Distance</th> -->
                          <th class="table-heading-font">Action</th>
                          
                        </tr>
                      </tfoot>
                      <tbody>
                         @if (!empty($parameter))
                        <?php $id = 1; ?>
                        @foreach ($parameter as $value) 
                        <tr>
                         
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$id}}</td>
                          <td class="table-heading-font"  style="background-color: #f5f5f5; width:80%"><input class="form-control" type="text" name="" value="{{$value->name}}"></td>
                           <td class="table-heading-font" style="background-color: #f5f5f5;"><input class="form-control" type="text" name="operator_{{$value->alert_sub_category_id}}" id="operator_{{$value->alert_sub_category_id}}" value="{{$value->operator}}"></td>
                          
                            <td class="table-heading-font" style="background-color: #f5f5f5;"><input class="form-control" type="text" name="value_{{$value->alert_sub_category_id}}" id="value_{{$value->alert_sub_category_id}}" value="{{$value->value}}"></td>
                             <td class="table-heading-font" style="background-color: #f5f5f5;">
                         <?php  $check=Fetch::checkpe($value->alert_sub_category_id,$did);
                                print_r($check); die;
                          ?>
                              <button class="btn btn-primary btn-sm" onclick="save({{$value->alert_sub_category_id}})">Save</button></td>
                          
        
                        </tr>
                        <?php $id++; ?>  
                        @endforeach
                       @endif

                       
                       
                      </tbody>
                    </table>
                  </div>

                   <script>
    $(document).ready(function() {
      $('#datatablesvehicle1').DataTable({
        "pagingType": "full_numbers",
                    "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
                  });
                    
    });
  </script>