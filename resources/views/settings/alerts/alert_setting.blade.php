@include('default.header')
  @include('default.sidebar')
  @include('default.submenu')

  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
  button.btn.dropdown-toggle.btn-light {
    background: transparent;
    color: #000;
    box-shadow: none;
    padding: 7px;
  }
    /*tfoot {
     display: table-header-group !important;
     background-color: blanchedalmond;
    }*/
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    .ScrollStyle
   {
       max-height: 300px;
       overflow-y: scroll;
   }
   /* tr div {
     width: auto !important;
     z-index: auto !important;
   }*/
   /*.tab-pane tbody {
     display: block;
     max-height: 230px;
     overflow-y: scroll;
   }*/
   .non-highlighted-cal-dates{
 background-color: gainsboro;
}
.tox-statusbar__branding{
  display: none;
}
a { color: inherit; }
a.link1{ color: #eb262d ; }
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">
               <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>


      
              
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">notification_important</i>
                  </div>
                  <h4 class="card-title">{{$title}}</h4>
                </div>


          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-sm-4">
                  
                </div>
                <div class="col-sm-8">
                 <a href="alert_setting_mul" class="btn-sm pull-right btn-primary btn" >System Operator</a> 
                </div>
              </div>
                <div class="material-datatables">

                  <table id="datatablesvehicle1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                    <thead>
                      <tr>
                        <th class="table-heading-font">S.No</th>
                        <th class="table-heading-font">System</th>
                        <!-- <th class="table-heading-font">DeviceID</th>  -->
                        <th class="table-heading-font">DB Table</th>
                        <th class="table-heading-font">DB Column</th>
                        <th class="table-heading-font">Action</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th class="table-heading-font">S.No</th>
                        <th class="table-heading-font">System</th>
                        <!-- <th class="table-heading-font">DeviceID</th>  -->
                        <th class="table-heading-font">DB Table</th>
                        <th class="table-heading-font">DB Column</th>
                        <th class="table-heading-font">Action</th>
                      </tr>
                    </tfoot>
                    <tbody>
                    @if (!empty($parameter))
                        <?php $id = 1; ?>
                      @foreach ($parameter as $value)

                        <tr>
                           <!-- <form method="post" action="{{url('/')}}/savesystemeditmul" autocomplete="off" class="form-horizontal"> -->
                          <td  style="background-color: #f5f5f5;width:6%">{{$id}}
                          </td>
                          
                          <td style="background-color: #f5f5f5; width:50%">
                          	<div class="form-group">
                          	 <input class="form-control" type="text" name="name" value="{{$value->name}}">
                             
                            </div>
                          </td>
                            
                         <!--  <td> 

                              <select class="" multiple="multiple" data-live-search="true" data-style="select-with-transition" title="Select Device ID" data-size="7" name="dtype[]" id="dtype" tabindex="-98" >
                                  <option value="-1" selected>Select All</option>
                                  @foreach ($device as $dev)
                                  <option value="{{$dev->device_id}}">{{$dev->device_id}} </option>
                                  @endforeach
                              </select>
                            
                         
                          </td> -->

                          <td>
                          	<div class="form-group">
                              <input class="form-control" type="text" name="operator_{{$value->alert_sub_category_id}}" id="operator_{{$value->alert_sub_category_id}}" value="{{$value->db_table}}"></td>
                            </div>
                          </td>
                           
                          <td style="background-color: #f5f5f5;">
                            <div class="form-group">
                            	<input  type="text" class="form-control" name="value_{{$value->alert_sub_category_id}}" id="value_{{$value->alert_sub_category_id}}" value="{{$value->db_column}}"> 
                            	<input  type="hidden" name="id" id="id" value="{{$value->alert_sub_category_id}}">
                            </div>
                          </td>

                          <td class="table-heading-font" style="background-color: #f5f5f5;">
                            <button type="submit" class="btn btn-sm btn-primary" onclick="save({{$value->alert_sub_category_id}})" >Save</button>
                          </td>
<!-- </form> -->
                        </tr>
                        <?php $id++;  ?>   
                      @endforeach 
                    @endif

                       
                       
                    </tbody> 
                  </table>
                </div>
              </div>
            </div>
          </div>
      </div>
  </div>
</div>
@include('default.footer')
<script>
   

    $(document).ready(function() {




      $('#datatablesvehicle1').DataTable({
        "pagingType": "full_numbers",
         "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
      });               
    });

  function systemedit(){
    $.post('{{url('/')}}/systemedit', {  }).done(function (data) {
      $('#load_data').html(data);
    });
  }

function save(id){
  
    var table  = $("#operator_"+id).val();
    var column = $("#value_"+id).val();
   
   

   $.post('{{url('/')}}/savesystemeditmul', { id:id,column:column,table:table }).done(function (data) {
                           // $('#load_data').html(data);
                           Swal.fire({
            type: 'info',
            title: 'Info',
            text: 'Data Saved Successfully',
             confirmButtonColor: '#eb262d',
            
          });
                  });

  
}
  </script>