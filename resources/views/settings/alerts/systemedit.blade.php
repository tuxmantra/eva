<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;

?>
<style type="text/css">
  .custom-select {
    display: inline-block;
    /* width: 100%; */
    /* height: calc(2.4375rem + 2px); */
    padding: 0.375rem 1.75rem 0.375rem 0.75rem;
    /* line-height: 1.5; */
    /* color: #495057; */
    /* vertical-align: middle; */
     background: #ffff; 
    /* background-size: 8px 10px; */
    border: 0px solid #d2d2d2;
    /* border-radius: 0.25rem; */
    appearance: none;
}
</style>

<div class="material-datatables">
  <p>DeviceID: {{$did}}</p>
                    <table id="datatablesvehicle1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="table-heading-font" style="width:10% !important">S.No</th>
                          <th class="table-heading-font">System</th>
                          <th class="table-heading-font">Operater</th>
                          <th class="table-heading-font">Value</th>
                        
                          <!-- <th class="table-heading-font">Distance</th> -->
                          <th class="table-heading-font">Action</th>
                          
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th class="table-heading-font" style="width:7% !important">S.No</th>
                          <th class="table-heading-font">System</th>
                          <th class="table-heading-font">Operater</th>
                          <th class="table-heading-font">Value</th>
                        
                          <!-- <th class="table-heading-font">Distance</th> -->
                          <th class="table-heading-font">Action</th>
                          
                        </tr>
                      </tfoot>
                      <tbody>
                         @if (!empty($parameter))
                        <?php $id = 1; ?>
                        @foreach ($parameter as $value) 
                        <tr>
                          <?php  
                          // print_r($value); 
                          $check=Fetch::checkpe($value->alert_sub_category_id,$did);
                                // print_r(Fetch::checkpe($value->alert_sub_category_id,$did)); die;
                          if(empty($check)){
                          ?>
                         
                          <td class="table-heading-font" style="background-color: #f5f5f5; width:6%;">{{$id}}</td>
                          <td class="table-heading-font"  style="background-color: #f5f5f5; width:70%;"><input class="form-control" type="text" name="" value="{{$value->name}}" readonly></td>
                           <td class="table-heading-font" style="background-color: #f5f5f5; width:10%;">
                           
                          
     <select class="custom-select"  id="operator_{{$value->alert_sub_category_id}}"  >

                                     
   <!--  <select class="selectpicker" id="operator_{{$value->alert_sub_category_id}}"> -->
      <option value="" > select </option>
      <option value="<"> < </option>
      <option   value="<=" > = </option>
      <option  value="<="> <= </option>
       <option  value=">"> > </option>
        <option  value=">="> >= </option>
      
    </select>
  
  
  
                            <!-- <input class="form-control" type="text" name="operator_{{$value->alert_sub_category_id}}" id="operator_{{$value->alert_sub_category_id}}" value="" > --></td>
                          
                            <td class="table-heading-font" style="background-color: #f5f5f5; width:10%;"><input class="form-control" type="text" name="value_{{$value->alert_sub_category_id}}" id="value_{{$value->alert_sub_category_id}}" value=""></td>
                             
                             <td class="table-heading-font" style="background-color: #f5f5f5;"><button class="btn btn-primary btn-sm" onclick="create({{$value->alert_sub_category_id}})">Create</button></td>
                           <?php }else{ ?>

                             <td class="table-heading-font" style="background-color: #f5f5f5;width:6%;">{{$id}}</td>
                          <td class="table-heading-font"  style="background-color: #f5f5f5; width:70%"><input class="form-control" type="text" name="" value="{{$value->name}}"></td>
                           <td class="table-heading-font" style="background-color: #f5f5f5; width:10%;">
                            <!-- <input class="form-control" type="text" name="operator_{{$value->alert_sub_category_id}}" id="operator_{{$value->alert_sub_category_id}}" value="{{$check[0]->operator}}"> -->

                              <select class="custom-select"  id="operator_{{$value->alert_sub_category_id}}"  >

                                     
                         <!--  <select class="selectpicker" id="operator_{{$value->alert_sub_category_id}}"> -->
                            <option value="" > select </option>
                            <option <?php if($check[0]->operator=='<'){echo "selected";} ?> value="<"> < </option>
                            <option <?php if($check[0]->operator=='='){echo "selected";} ?>  value="<=" > = </option>
                            <option <?php if($check[0]->operator=='<='){echo "selected";} ?> value="<="> <= </option>
                             <option <?php if($check[0]->operator=='>'){echo "selected";} ?>  value=">" > > </option>
                            <option <?php if($check[0]->operator=='>='){echo "selected";} ?> value=">="> >= </option>
                            
                          </select>


                           </td>
                          
                            <td class="table-heading-font" style="background-color: #f5f5f5; width:10%;"><input class="form-control" type="text" name="value_{{$value->alert_sub_category_id}}" id="value_{{$value->alert_sub_category_id}}" value="{{$check[0]->value}}"></td>
                             
                             <td class="table-heading-font" style="background-color: #f5f5f5;"><button class="btn btn-primary btn-sm" onclick="save({{$value->alert_sub_category_id}})">Update</button></td>

                           <?php } ?> 
                          
        
                        </tr>
                        <?php $id++; ?>  
                        @endforeach
                       @endif

                       
                       
                      </tbody>
                    </table>
                  </div>

                   <script>
    $(document).ready(function() {
      $('#datatablesvehicle1').DataTable({
        "pagingType": "full_numbers",
                    "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
                  });
                    
    });
  </script>
  