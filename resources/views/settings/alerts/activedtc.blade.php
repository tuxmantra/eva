 <div class="material-datatables">
  <!-- <h3>{{$title}}</h3> -->
                    <table id="datatablesvehicle" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th class="table-heading-font"><b>S.No</b></th>
                          <th class="table-heading-font"><b>Device ID</b></th>
                          <th class="table-heading-font"><b>Customer</b></th>
                          <th class="table-heading-font"><b>DTC</b></th>
                          <th class="table-heading-font"><b>Description</b></th>
                        
                          <th class="table-heading-font"><b>Severity</b></th>
                          <th class="table-heading-font"><b>Remedy</b></th>
                          
                        </tr>
                      </thead>
                     <!--  <tfoot>
                        <tr>
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Device ID</th>
                          <th class="table-heading-font">Customer</th>
                          <th class="table-heading-font">DTC</th>
                          <th class="table-heading-font">Description</th>
                        
                          <th class="table-heading-font">Severity</th>
                          <th class="table-heading-font">Remedy</th>
                          
                        </tr>
                      </tfoot> -->
                      <tbody>
                        @if (!empty($redveicle))
                        <?php $id = 1; ?>
                        @foreach ($redveicle as $value) 
                        <?php if(!empty($value)){ ?>
                        <tr>
                         
                          <td class="table-heading-font" style="background-color: #f5f5f5; width:7%;">{{$id}}</td>
                          <td class="table-heading-font" style="background-color: #f5f5f5;width:10%;"><?php echo $value['device_id']; ?></td>
                          <td class="table-heading-font" style="background-color: #f5f5f5;width:10%;"><?php echo $value['customer']; ?></td>
                          <td class="table-heading-font" style="background-color: #f5f5f5;width:10%;"><?=$value['dtc_num'] ?></td>
                          <td class="table-heading-font" style="background-color: #f5f5f5;"><?=$value['description'] ?></td>
                          <td class="table-heading-font" style="color: white; width:8%;background-color: <?php if($value['rating'] == 1){echo '#eb2621';}elseif($value['rating'] == 2){echo '#FF4500';}elseif($value['rating'] == 3){echo '#f3e714';}?>;"><?=$value['rating'] ?></td>
                             <td class="table-heading-font" style="background-color: #f5f5f5;"><?=$value['remedies'] ?></td>
                          
        
                        </tr>
                        <?php $id++; ?> 
                        <?php } ?> 
                        @endforeach
                        @else
                           <tr><td colspan="9" align="center">No Alerts Exist.</td></tr>
                        @endif
                       
                       
                      </tbody>
                    </table>
                  </div>

                   <script>
    $(document).ready(function() {
      $('#datatablesvehicle').DataTable({
        "pagingType": "full_numbers",
                    "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
                  });
                    
    });
  </script>