@include('default.header')


  @include('default.sidebar')


  @include('default.submenu')


  <style type="text/css">


    .table-heading-font{


        font-size: 13px !important;


    }


    tfoot {


     display: table-header-group !important;


    }


    tfoot input {


      width: 100%;


    }


 

    .deep{


      cursor: pointer;


      padding: 2%;


      border-radius: 2px;


      background: #fff;


      color: grey;


      position: relative;


    }


    .spano{


      padding: 16px 16px 8px 0px;


      font-size: 13px;


    }


    .dataTables_filter span{


      position: relative !important; 


    }


   .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {


    width: 220px;


    argin-top: -10px;


    margin-top: -9px;


}


 

  </style>


      <!-- End Navbar -->


 

      <div class="content">


        <div class="container-fluid">


          <div class="row">


                    <div class="col-md-12" align="center">


              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">


 

                     


 

              </span>


              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>


            </div>


 

 

            <div class="col-md-12">


              <div class="card">


                <div class="card-header card-header-primary card-header-icon">


                  <div class="card-icon">


                    <i class="material-icons">assignment</i>


                  </div>


                  <h4 class="card-title">Specification List</h4>


                </div>


                <div class="card-body">


                  <div class="toolbar" >


                    <div class="row" >


                      <div class="col-md-12">


                        <span id="process" style="font-size: 12px; color: red; display: none;">Please Wait import in process...<img style="width: 6%;" class="img-responsive" src="{{url('/')}}/public/assets/img/load.gif"></span>


                      </div>


                      <div class="col-md-9" >


                      <form method="post" name="imo" class="form-horizontal" id="imo" action="{{url('/')}}/upload-vehicle-csv" enctype="multipart/form-data">    


                      <label>Import Vehicle Data</label><br>


                      <input type="file" class="form-form-control" accept=".csv" name="upload_csv" id="upload_csv" title="Add CSV File">


                      </form>


                </div>


 

 

 

                    <div class="col-md-3">


                      <label>Manufacturer List</label><br>


 

                       <select class="selectpicker loadVehicleData" data-style="select-with-transition" title="Select Vehicle Data" data-size="7" name="vehicle_data" tabindex="-98">


                            @if (!empty($mfdList))


                            @for ($i = 0; $i < count($mfdList); $i++)


                            <option <?php if($i==0){ ?> selected <?php } ?> value="{{$i}}">{{$mfdList[$i]}}</option>


                            @endfor


                            @endif


 

                    </select>


                    </div>


                  </div>


                 </div>


                 <hr>


                  <div class="material-datatables" id="listLoad">


 

                  </div>


 

                </div>


                <!-- end content-->


              </div>


                <div class="col-md-12">


                     <span class="spano"><b>F-Type</b> = Fuel Type</span>



                     <span class="spano"><b>E-Capacity</b> = Engine Capacity</span>


                     <span class="spano"><b>M-Torque(Nm)</b> = max Torque (Nm)</span>


                     <span class="spano"><b>M-Power(bhp)</b> = Max Power (bhp)</span>


                  </div>


              <!--  end card  -->


            </div>


            <!-- end col-md-12 -->


          </div>


          <!-- end row -->


        </div>


      </div>


 

    </div>


  </div>


 

  <!--   Core JS Files   -->


@include('default.footer')
@if (Session::has('message'))
  <script>
     Swal.fire({
        type: 'info',
        title: 'info',
        text:   '<?= session('message') ?>',
         confirmButtonColor: '#eb262d',
        // footer: '<a href>Why do I have this issue?</a>'
      })
  </script>
     
@endif

 <script>
 

 

 

    $(document).ready(function() {


 

 

 

 

        $.ajax({


          type:'POST',


          url:  '{{url('/')}}/load-list-vehicle-specification',


          data: { 'vehData': '0' },


          success: function(data){


              $('#listLoad').html(data);


          }


        });


 

 

 

        $( "#upload_csv" ).change(function() {


 

 

            event.preventDefault();


       Swal.fire({


                title: 'Are you sure?',


                text: "You want to Upload file!",


                type: 'question',


                showCancelButton: true,


                confirmButtonColor: '#eb262d',


                cancelButtonColor: '#eb262d',


                confirmButtonText: 'Yes, Upload it!'


            }).then((result) => {


              if(result.value){


                          $('#process').show();


                          $( "#imo" ).submit();


              }


              // location.reload();


            })


 

           // $('#process').show();


           // $( "#imo" ).submit();


        });


 

 

/*      // Edit record


      table.on('click', '.edit', function() {


        $tr = $(this).closest('tr');


        var data = table.row($tr).data();


        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');


      });


 

      // Delete a record


      table.on('click', '.remove', function(e) {


        $tr = $(this).closest('tr');


        table.row($tr).remove().draw();


        e.preventDefault();


      });


 

      //Like record


      table.on('click', '.like', function() {


        alert('You clicked on Like button');


      });*/


    });


  </script>