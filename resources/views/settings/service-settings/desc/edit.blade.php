  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
                <div class="col-md-12" align="right">
            <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
          </div>
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Edit Description</h4>
                  </div>
                </div>

                <div class="card-body ">
                  <form method="post" action="{{url('/')}}/edit-desc-save" autocomplete="off" class="form-horizontal" onsubmit="return editDescValid_new();">
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Title</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                         <input type="hidden" name="cid" value="{{App\Helpers\Helper::crypt(1,$desc_data[0]->id)}}">
                         <input type="hidden" value="{{$form_token}}" name="form_token">
                          <input autocomplete="off" type="text" name="title" id="title" class="form-control" placeholder="Enter Title" value="{{$desc_data[0]->title}}">
                        </div>
                      </div>
                    </div>
   
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Sub Title</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="subtitl" id="subtitl" placeholder="Enter sub Title" value="{{$desc_data[0]->sub_title}}">
                        </div>
                      </div>
                    </div>
                      <div class="row" style="margin-top: 25px;">
                      <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-10 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" 
                            value="1" <?php if($desc_data[0]->status == 1){ ?> checked <?php } ?>> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0" <?php if($desc_data[0]->status == 0){ ?> checked <?php } ?>> In Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>

            </div>
          </div>
        </div>
      </div>
       </form>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
  @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif

 <script type="text/javascript">
   function editDescValid_new(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var title     = $('#title').val();
  var sub_title = $("#subtitl").val();
  if(title == '' || sub_title == ''){
      Swal.fire({
            type: 'info',
            title: 'info',
            text: 'Fields required.',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
      return false;
  }else{
      return true;
  }
}
 </script>
