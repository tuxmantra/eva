
                   <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                       
                          <th class="table-heading-font">Model</th>
                          <th class="table-heading-font">F-Type</th>
                          <th class="table-heading-font">Varients</th>
                          <th class="table-heading-font">E-Capacity</th>
                          <th class="table-heading-font">M-Torque(Nm)</th>
                          <th class="table-heading-font">@RPM</th>
                          <th class="table-heading-font">M-Power(bhp)</th>
                          <th class="table-heading-font">@RPM</th>
                          <th class="table-heading-font">Max Speed</th>
                          <th class="table-heading-font">Body Type</th>
                          <th class="table-heading-font">MAP</th>
                          <th class="table-heading-font">MFG Y</th>
                     <!--      disabled-sorting -->
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                        
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                          <th class="table-heading-font"></th>
                     <!--      disabled-sorting -->
                        </tr>
                      </tfoot>
                      <tbody>
                        @if (!empty($vehicleSpec))

                        @foreach ($vehicleSpec as $vs)  
                        <tr>
                         
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$modList[$vs->model]}}</td>
                          <td class="table-heading-font" style="background-color: #ffffff;">{{$fuel[$vs->fuel]}}</td>
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$varList[$vs->varient]}}</td>
                          <td class="table-heading-font" style="background-color: #ffffff;">{{$engCapList[$vs->engcap]}}</td>
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$vs->maxtog}}</td>
                          <td class="table-heading-font" style="background-color: #ffffff;">{{$vs->rpm}}</td>
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$vs->maxpow}}</td>
                          <td class="table-heading-font" style="background-color: #ffffff;">{{$vs->rpm2}}</td>
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$vs->maxspeed}}</td>
                          <td class="table-heading-font" style="background-color: #ffffff;">{{$vs->btype}}</td>
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$vs->map}}</td>  
                          <td class="table-heading-font" style="background-color: #ffffff;"><?php echo !empty($vs->mfg_yr)? $vs->mfg_yr:"-"; ?></td>                      
                        </tr>

                        @endforeach
                        @else
                           <tr><td colspan="9" align="center">No Details Exist.</td></tr>
                        @endif
                       
                       
                      </tbody>
                    </table>

                    <script type="text/javascript">
                        $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "targets": 'no-sort',
        "bSort": false,
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });


                    </script>