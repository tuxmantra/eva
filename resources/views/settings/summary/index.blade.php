  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')

  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
  button.btn.dropdown-toggle.btn-light {
    background: transparent;
    color: #000;
    box-shadow: none;
    padding: 7px;
  }
    /*tfoot {
     display: table-header-group !important;
     background-color: blanchedalmond;
    }*/
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    .ScrollStyle
   {
       max-height: 300px;
       overflow-y: scroll;
   }
   /* tr div {
     width: auto !important;
     z-index: auto !important;
   }*/
   /*.tab-pane tbody {
     display: block;
     max-height: 230px;
     overflow-y: scroll;
   }*/
   .non-highlighted-cal-dates{
 background-color: gainsboro;
}
.tox-statusbar__branding{
  display: none;
}
a { color: inherit; }
a.link1{ color: #eb262d ; }
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">
               <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>


          <input type="hidden" name="exp" id="exp" value="{{$did}}">
              
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">notification_important</i>
                  </div>
                  <h4 class="card-title">Alerts</h4>
                </div>
                
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-8">
                      <div class="card-body" id="load_data">

                 </div>
                    </div>
                    <div class="col-md-4">
                      <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>
                  <div class="col-md-12">
                  <div style="float: left;">
                  </div>
                  <div style="float: left;">
                  <div class="ScrollStyle tab-pane active" id="profile">
                  <input type="text" name="search" id="search" placeholder="Search Devices" class="form-control">
                      <table class="table" id="list_device">
                       <tbody>
                         <?php if($user_category == 1 || $user_category == 2 || $user_category == 3){ ?>
                         <?php if(!empty($device_list)){ ?>
                           <?php foreach($device_list as $key => $dl){ ?>
                         <tr id="row_<?= $dl->device_id ?>" onclick="device_row('<?= $dl->device_id ?>');">

                           <td><p style="font-size: 13px; margin-bottom: 0;"><a class="nav-link" href="javascript:void(0)"><?php echo $dl->device_name." (Device ID ".$dl->device_id.")
                           "; ?>{{App\Helpers\Helper::getManufacturerById($dl->manu)}}</p></a></td>
                         </tr>
                         <?php } ?>
                       <?php }else{ ?>
                         <tr>
                           <td>Empty</td>
                         </tr>
                         <?php } ?>
                         <?php }else{ ?>
                            <tr>

                           <td>Sign contract for "What are conference organizers afraid of?"</td>

                         </tr>

                         <?php } ?>

                       </tbody>
                     </table>
                    </div>
                  </div>
                  </div>
                    </div>
                  </div>
                  
             
                </div>
                <hr>
                <div class="col-md-12" align="center"> 
                <span id="loading_si" style="display: none;color: red;font-size: 12px;font-weight: bold;text-align: center;">Wait in process....</span>
                </div>

               
          <div class="card-body">
                 <div class="col-md-12">
             <div class="card ">
               <div class="card-header ">
                 
               </div>
               <div class="card-body" id="load_data">

                 </div>

               </div>
             </div>
           </div>
               </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
     
    </div>
  </div>
 
  <!--   Core JS Files   -->
 @include('default.footer')
 <div class="modal fade " id="myModal" role="dialog">
      <div class="modal-dialog modal-lg" style=" min-height: 400px;">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">            
            <button type="button" class="close" data-dismiss="modal">&times;</button> 
            <h4 class="modal-title">Send Alert</h4>      
          </div>
          <div class="modal-body" >
            <p id="alertPop"><div style="min-height: 400px;"> 
  <form name="msgFrm" id="msgFrm" action="{{url('/')}}/alert-user-form" method="post">
  <input type="hidden" name="email_user" id="email_user" value="">
  <input type="hidden" name="color" id="color" value="">
    <input type="hidden" class="devid" id="devid" name="devid" value="<?= $did ?>"/>
    <input type="hidden" id="ftype" name="ftype" value="1"/>
    <input type="hidden" id="mtype" name="atype" value="1"/>
    <input type="hidden" id="isdel" name="isdel" value=""/>
    <textarea class="form-control mceEditor" style="height:40px;" rows="5" id="msg" name="msg" >
    </textarea> <br/>
    <button type="submit" class="btn btn-fill btn-rose" name="send_mail">Send</button>
    <button type="submit" class="btn btn-fill btn-rose" name="delete_data">Delete</button>
    <span id="proID" style="color:red;">&nbsp;</span>
    
  </form>
</div></p>
          </div>
          
        </div>
        
      </div>
    </div>
     <div class="modal fade " id="modalAlert" role="dialog">
      <div class="modal-dialog modal-lg" style=" min-height: 400px;">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">            
            <button type="button" class="close" data-dismiss="modal">&times;</button> 
            <h4 class="modal-title">Send Alert</h4>      
          </div>
          <div class="modal-body" >
            <p id="alertPop"><div style="min-height: 400px;"> 
  <form name="msgFrm" id="msgFrm" action="{{url('/')}}/alert-user-form-all" method="post">
  <input type="hidden" name="email_user" id="email_user" value="">
  <input type="hidden" name="idval" id="idval" value="">
  <input type="hidden" name="color" id="color" value="">
    <input type="hidden" class="devid" id="devid" name="devid" value="<?= $did ?>"/>
    <input type="hidden" id="ftype" name="ftype" value="1"/>
    <input type="hidden" id="mtype" name="atype" value="1"/>
    <input type="hidden" id="isdel" name="isdel" value=""/>
    <textarea class="form-control mceEditor" style="height:40px;" rows="5" id="msga" name="msga" >
    </textarea> <br/>
    <button type="submit" class="btn btn-fill btn-rose" name="send_mail">Send</button>
    <button type="submit" class="btn btn-fill btn-rose" name="delete_data">Delete</button>
    <span id="proID" style="color:red;">&nbsp;</span>
    
  </form>
</div></p>
          </div>
          
        </div>
        
      </div>
    </div>

  <script type="text/javascript">
   $(document).ready(function() {

    $( "#search" ).keyup(function() {
        var did    = $('#devid').val();
        var search = $('#search').val();
        $.post('{{url('/')}}/getDeviceList', { search: search, did: did }).done(function (data) {
            $('#list_device').html(data);
        });
    });
          
        var exp_c = '5';
        var did   = '<?= $did ?>';
        
        $("#loading_si").show();
        $('tr').css({ 'background-color' : '#fff'});
        $('#row_'+did).css({ 'background-color' : 'aliceblue'});
        $("#exp").val(did);
         $.ajax({
                type:'POST',
                url:  '{{url('/')}}/get-alert-load',
                data: {'did': did, 'exp': exp_c, 'status': '1'},
                success: function(data) {
                  $("#loading_si").hide();
                    $("#load_data").html(data);
                    $("#expc").selectpicker('refresh');
                    $('#datatable1').DataTable({ 
                    "pagingType": "full_numbers",
                    "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
                  });
                    $('#datatable2').DataTable({ 
                    "pagingType": "full_numbers",
                    "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
                  });
            }
        });
   });

  function device_row(did){
      var exp = $("#expc").val();
      $('tr').css({ 'background-color' : '#fff'});
      $('#row_'+did).css({ 'background-color' : 'aliceblue'});
      $("#exp").val(did);
      $(".devid").val(did);
      $.ajax({
              type:'POST',
              url:  '{{url('/')}}/get-alert-load',
              data: {'did': did, 'exp': exp, 'status': '2'},
              success: function(data) {
                  $("#load_data").html(data);
                  $("#expc").selectpicker('refresh');
                  $('#datatable1').DataTable({
                  "pagingType": "full_numbers",
                  "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                  ],
                  responsive: true,
                  language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                  }
                });
                  $('#datatable2').DataTable({ 
                  "pagingType": "full_numbers",
                  "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                  ],
                  responsive: true,
                  language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                  }
                });
          }
      });
  }

  function active_expiry(exp){
    var expc = $("#expc").val();
    var did  = $("#exp").val();
    // var expc = $("#expc option:selected").val();
    // $("#expc").selected();
     $.ajax({
              type:'POST',
              url:  '{{url('/')}}/get-alert-load',
              data: {'did': did, 'exp': expc, 'status': '3'},
             success: function(data) {
                  $("#load_data").html(data);
                  $("#expc").selectpicker('refresh');
                  $('#datatable1').DataTable({ 
                  "pagingType": "full_numbers",
                  "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                  ],
                  responsive: true,
                  language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                  }
                });
                  $('#datatable2').DataTable({ 
                  "pagingType": "full_numbers",
                  "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                  ],
                  responsive: true,
                  language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                  }
                });
          }
      });


  }

function callAlertPop(email,color,devid)
{
  $("#devid").val(devid);
    $.ajax({
            type:'POST',
            url:  '{{url('/')}}/alert-pop',
            data: {'email': email, 'color': color,'devidd': devid},
           success: function(data) {
            var json         = JSON.parse(data);
            var activeEditor = tinyMCE.get('msg');
            $('#email_user').val(json.email);
            $('#color').val(json.color);
            activeEditor.setContent(json.msg);
        }
    });
 }

 function callAlertPopExp(id,devid)
 {
    $("#devid").val(devid);
    $("#idval").val(id);
    $.ajax({
            type:'POST',
            url:  '{{url('/')}}/alert-pop-msg',
            data: {'devidd': devid, 'id': id},
           success: function(data) {
            var json         = JSON.parse(data);
            var activeEditor = tinyMCE.get('msga');
            activeEditor.setContent(json.msg);
        }
    });
 }


function callSendAlertExpDel(del,url)
{
  confirmTxt = (del == 1) ? "Do you want to delete this alert" : " Do you want to send theis alert";
  var delVal1 = $("#delV1").val();
  var delVal2 = $("#delV2").val();
  var didV = $("#didV").val();
  var didVal = $("#didVal").val();
  //$('#proID').html('<img height="100" src="http://localhost/enginecalold/assets/images/loading.gif"/>');
  if(confirm(confirmTxt))
  {
    $('#proID').html('<img height="100" src="assets/images/loading.gif"/>');    
    $('#isdel').val(del);
    var editor_data = tinyMCE.get('msg').getContent(); 
    $('#msg').val(editor_data);
    fData = $('#msgFrm').serialize();
     $.ajax({ // create an AJAX call...
          data: fData, // get the form data
          type: "POST", // GET or POST
          url: 'alert-pop-exp.php', // the file to call
          success: function(response) { // on success..
            //alert(response)
           $('#alertPop').html(response); // update the DIV
           repExpiry();
        }
    });
  }
}

function loadAll(){
   location.reload();
}
    
    </script>
    <script type="text/javascript" src="{{url('/')}}/public/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/public/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script type="text/javascript">
tinymce.init({
    selector: "#msg",
});
</script>
  <script type="text/javascript">
tinymce.init({
    selector: "#msga",
});
</script>