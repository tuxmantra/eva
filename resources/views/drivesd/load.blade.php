<input name="service_date" type="hidden" id="service_date" value="<?= $drive_dates ?>">
<?php    //print_r(json_encode($drive_latlon));die;
                           $myJSON = json_encode($drive_latlon);
                            // print_r($myJSON); die;

                     ?>
<div id="dvMap" style="position: initial; overflow: auto; height: 490px;"></div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{$gapikey}}&callback=initMap&region=IN">
    </script>
<script type="text/javascript">
    var markers = <?=$myJSON?>;
    // window.onload = function () {
    //     LoadMap();
    // } 
    function initMap() {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 5, //Not required.
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

         var iconBase =
            'https://eva.enginecal.com/public/css/';

        var icons = {
                          info: {
                            icon: iconBase + 'icons8-red-circle-48-removebg-preview.png'                           
                          }  
                    };
        if(markers[0].lat!='20.9745'){
        //Create LatLngBounds object.
        var latlngbounds = new google.maps.LatLngBounds();
 
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: icons['info'].icon,
                title: data.title
            });
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
 
            //Extend each marker's position in LatLngBounds object.
            latlngbounds.extend(marker.position);
        }
 
        //Get the boundaries of the Map.
        var bounds = new google.maps.LatLngBounds();
      
        //Center map and adjust Zoom based on the position of all markers.
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
    }
    }
   
</script>



