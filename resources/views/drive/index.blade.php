  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
   

  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }

    .tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    #datetimepicker12 .datepicker-inline {
    width: 335px !important;
    }table.table-condensed {
    width: 79%;
    }
      tr div {
      width: auto !important;
      z-index: auto !important;
    }
    .tab-pane tbody {
      display: block;
      max-height: 230px;
      
    }
    .ScrollStyle
    {
        max-height: 300px;
        overflow-y: scroll;
    }
    #map{
          background-color: aliceblue;
          margin-top: 30px !important;
    }
    .divTop{
      margin-top: 35px;
    }

.non-highlighted-cal-dates{
  background-color: gainsboro;
}
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">insert_drive_file</i>
                  </div>
                  <h4 class="card-title">Drive</h4>
                   <div class="row" id='vehicledetails'>
                      <!-- User and Vehicle Details to display from js -->
                  </div>
                  <br>
                </div>
                <input type="hidden" id="devid" name="devid" value="<?= $did ?>">
                <div class="card-body">
                     
                 <div class="col-md-12">
                  
                   <div class="row">
                      <div class="col-sm-3">
                        <div class="form-group">
                          <select class="selectpicker dtype"  data-style="select-with-transition" title="Select Parameter Type" data-size="7" name="ptype1" id="ptype1" tabindex="-98" onchange="dropdown_call()">
                            <?php  foreach ($ptype as $key => $value) { ?> 
                               <option value="{{$value->id}}"  <?php if($value->id==4){echo 'selected';} ?>>{{$value->title}}</option>
                               <? }  ?>
                              
                          </select>
                        </div>
                      </div>

                      <div class="col-sm-3">
                        <div class="form-group">
                          <select class="selectpicker dtype"  data-style="select-with-transition" title="Select Parameter Type" data-size="7" name="ptype2" id="ptype2" tabindex="-98" onchange="dropdown_call()">
                            <?php  foreach ($ptype as $key => $value) {
                              ?>
                               <option value="{{$value->id}}"  <?php if($value->id==5){echo 'selected';} ?>>{{$value->title}}</option>
                               <?
                            }  ?>
                              
                          </select>
                        </div>
                      </div>

                      <div class="col-sm-3">
                        <div class="form-group">
                          <select class="selectpicker dtype"  data-style="select-with-transition" title="Select Parameter Type" data-size="7" name="ptype3" id="ptype3" tabindex="-98" onchange="dropdown_call()">
                            <?php  foreach ($ptype as $key => $value) {
                              ?>
                               <option value="{{$value->id}}" <?php if($value->id==7){echo 'selected';} ?>>{{$value->title}}</option>
                               <?
                            }  ?>
                              
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <input type="hidden" name="snaptoraod" id="snaptoraod" value="1">
                        <button class="btn btn-primary btn-sm pull-right" id="mapbutton" onclick="snaptoraod()" >SnapToRoad</button>
                      </div>
                    </div>
                    </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="loader" id="loader" style="display: none;">Loading...</div>
                 <div class="col-md-12" id="load" style="display: none;" align="center">
                  <img src="public/assets/img/loader.gif" style="width: 20%;">
                 </div>
                      <div class="col-md-12" id="load_data">
                              
                          </div>
                    </div>
                     <div class="fixed-plugin">
                  <div class="dropdown show-dropdown">
                    <a href="#" data-toggle="dropdown" aria-expanded="false">
                     <span class="material-icons" style="font-size: 50px;     color: white;">
                    directions_car
                    </span>
                    </a>
                    <ul class="dropdown-menu" id="device_select" x-placement="bottom-start" style="position: absolute; top: 41px; left: 8px; will-change: top, left;">
                      <div id="device_select">
                       <div id="datetimepicker12" style="width: 100%">
                          
                          </div>
                      <div class="ScrollStyle tab-pane active" id="profile">
                                <input type="text" name="search" id="search" placeholder="Search Devices" class="form-control" style="background-color: #f9f5f5;">
                                    <table class="table" id="list_device">
                                     <tbody>
                                       <?php if($user_category == 1 || $user_category == 2 || $user_category == 3){ ?>
                                       <?php if(!empty($device_list)){ ?>
                                         <?php foreach($device_list as $key => $dl){ ?>
                                       <tr id="row_<?= $dl->device_id ?>" onclick="device_row('<?= $dl->device_id ?>');">

                                         <td><p style="font-size: 13px; margin-bottom: 0;"><a class="nav-link" href="javascript:void(0)"><?php echo $dl->device_name." (Device ID ".$dl->device_id.")
                                         "; ?>{{App\Helpers\Helper::getManufacturerById($dl->manu)}}</p></a></td>
                                       </tr>
                                       <?php } ?>
                                     <?php }else{ ?>
                                       <tr>
                                         <td>Empty</td>
                                       </tr>
                                       <?php } ?>
                                       <?php }else{ ?>
                                          <tr>

                                         <td>Sign contract for "What are conference organizers afraid of?"</td>

                                       </tr>

                                       <?php } ?>

                                     </tbody>
                                   </table>
                                  </div>
                                </div>
                    </ul>
                  </div>
                </div>
                <hr>
                
                    </div>
                  </div>
                   

                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>

    </div>
  </div>

  <!--   Core JS Files   -->
 @include('default.footer')
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">



$(document).ready(function() {
    
    $( "#search" ).keyup(function() {
        var did    = $('#devid').val();
        var search = $('#search').val();
        $.post('{{url('/')}}/getDeviceList', { search: search, did: did }).done(function (data) {
             
            $('#list_device').html(data);
        });
    });
    
    /* On Reload of page start  */
    var today               = new Date();
    var today_formatted     = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+('0'+today.getDate()).slice(-2);
    var user_busy_days      = [<?= $drive_dates ?>];

    $('#datetimepicker12').datepicker({

          inline: true,
          sideBySide: true,
          beforeShowDay: function (date) {
          calender_date = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+('0'+date.getDate()).slice(-2);
          var search_index = $.inArray(calender_date, user_busy_days);
          if (search_index > -1) {
                return {classes: 'non-highlighted-cal-dates', tooltip: 'Drive Took'};
          }else{
                return {classes: 'highlighted-cal-dates', tooltip: 'No Drive'};
          }
       }
    }).on('click', showTestDate);

    $('#datetimepicker12').datepicker('setDate', '<?=$today?>');

     $('#load_data').hide();
     $('#loader').show();
     var date = '<?=$today?>';
     var ptype1=$( "#ptype1 option:selected" ).val();
     var ptype2=$( "#ptype2 option:selected" ).val();
     var ptype3=$( "#ptype3 option:selected" ).val();
     getUservehicleDetails('<?= $did ?>');
    $.post('{{url('/')}}/getDriveLoad', { did: '<?= $did ?>', 'date': date,ptype1:ptype1, ptype2:ptype2, ptype3:ptype3,stroad:1 }).done(function (data) {
         $('#loader').hide();
          $('#load_data').show();
        $('#load_data').html(data);
        initMap();
    });
        /* On Reload of page end  */

});    

/* On clck of Device List start */
function device_row(did){
        $('#load_data').hide();
        $('#loader').show();
        $('#title').text('DeviceID : '+did);
        $('tr').css({ 'background-color' : '#fff'});
        $('#row_'+did).css({ 'background-color' : 'aliceblue'});
        $('#devid').val(did);
        var ptype1=$( "#ptype1 option:selected" ).val();
        var ptype2=$( "#ptype2 option:selected" ).val();
        var ptype3=$( "#ptype3 option:selected" ).val();
        $('#datetimepicker12').datepicker('setDate', '<?=$today?>');
        var date = $('#datetimepicker12').datepicker('getFormattedDate');
        var stroad=$( '#snaptoraod' ).val();
        // alert(parmtr.toString());
        // exit();
        getUservehicleDetails(did);
        $.post('{{url('/')}}/getDriveLoad', { did: did, 'date': date, ptype1:ptype1, ptype2:ptype2, ptype3:ptype3 ,stroad:stroad }).done(function (data) {
        $('#loader').hide();
        $('#load_data').show();
        $('#load_data').html(data);
        initMap();
             
        $('#datetimepicker12').datepicker('remove');
            var today           = $('#selected_date').val();
            // var today_formatted     = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+('0'+today.getDate()).slice(-2);
            var service_dates       = $('#service_date').val();
            var user_busy_days      = [service_dates];

        $('#datetimepicker12').datepicker({
                  inline: true,
                  sideBySide: true,
                  beforeShowDay: function (date) {
                  calender_date = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+('0'+date.getDate()).slice(-2);
                  var search_index = $.inArray(calender_date, user_busy_days);
                  var service_dates       = $('#service_date').val();
                  var n = service_dates.includes(""+calender_date);

                  if (n == true) {
                        return {classes: 'non-highlighted-cal-dates', tooltip: 'Drive Took'};
                  }else{
                        return {classes: 'highlighted-cal-dates', tooltip: 'No Drive'};
                  }

               }
            });
        $('#datetimepicker12').datepicker('setDate', today);
    });
}
/* On clck of Device List End */

// snaptoroad
function snaptoraod(){
        var val   = $('#snaptoraod').val();
        if(val == 1){
        $('#snaptoraod').val('0');
        $("#mapbutton").html("Raw GPS Graph");
      }
      if(val == 0){
        $('#snaptoraod').val('1');
        $("#mapbutton").html("SnapToRoad");
      }
        dropdown_call();
  }

// click in dropdown list call
function dropdown_call(){
   
          var did   = $('#devid').val();
        $('#load_data').hide();
        $('#loader').show();
         $('#title').text('DeviceID : '+did);
        var date = $('#datetimepicker12').datepicker('getFormattedDate');
        $('tr').css({ 'background-color' : '#fff'});
        $('#row_'+did).css({ 'background-color' : 'aliceblue'});
        $('#devid').val(did);
        var ptype1=$( "#ptype1 option:selected" ).val();
        var ptype2=$( "#ptype2 option:selected" ).val();
        var ptype3=$( "#ptype3 option:selected" ).val();
        var stroad=$( '#snaptoraod' ).val();
        $.post('{{url('/')}}/getDriveLoad', { did: did, 'date': date, ptype1:ptype1, ptype2:ptype2, ptype3:ptype3 ,stroad:stroad }).done(function (data) {
        $('#loader').hide();
        $('#load_data').show();
        $('#load_data').html(data);
        initMap();
             
       
    });
}
// end of dropdown call

/* On clck of Datepicker Start */
function showTestDate(){
    $('#load_data').hide();
    $('#loader').show();
    var date = $('#datetimepicker12').datepicker('getFormattedDate');
    var did   = $('#devid').val();
    var stroad=$( '#snaptoraod' ).val();
     // var parmtr=$( "#ptype option:selected" ).val();
    var ptype1=$( "#ptype1 option:selected" ).val();
    var ptype2=$( "#ptype2 option:selected" ).val();
    var ptype3=$( "#ptype3 option:selected" ).val();
    $.post('{{url('/')}}/getDriveLoad', { did: did, 'date': date, ptype1:ptype1, ptype2:ptype2, ptype3:ptype3 ,stroad:stroad }).done(function (data) {
      $('#loader').hide();
      $('#load_data').show();
      $('#load_data').html(data);
      initMap();
    });

}
/* On clck of Datepicker End */
$('#devid').val('<?= $did ?>');


</script>