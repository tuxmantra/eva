<input name="service_date" type="hidden" id="service_date" value="<?= $drive_dates ?>">
<?php    //print_r($drive_latlon);//die;
        $myJSON = json_encode($drive_latlon);
        // print_r($myJSON); die;
?>
<div id="dvMap" style="position: initial; overflow: auto; height: 490px;"></div>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{$gapikey}}&callback=initMap&region=IN">
    </script> 
<script type="text/javascript">
    var markers = <?=$myJSON?>;

    // window.onload = function () {
    //     LoadMap();
    // } 
    function initMap()
    {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 5, //Not required.
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

         var iconBase =
            'http://maps.google.com/mapfiles/kml/shapes/';

        var icons = {
          info: {
            icon: iconBase + 'placemark_circle.png'
           
          }
           
        };

        if(markers[0].lat!='20.9745'){
            var tripCoordinates = [];
            var latLng;
            //Create LatLngBounds object.
            var latlngbounds = new google.maps.LatLngBounds();
     
            for (var i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                tripCoordinates.push(myLatlng);
              if(i==0 || i==(markers.length-1)){
              if(i==0){var tittle='Start';
                      var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: tittle
                        });
                        (function (marker, data) {
                            google.maps.event.addListener(marker, "click", function (e) {
                                infoWindow.setContent("<div style = 'width:100px;min-height:20px'>Drive Start</div>");
                                infoWindow.open(map, marker);
                            });
                        })(marker, data);
             
                        //Extend each marker's position in LatLngBounds object.
                        latlngbounds.extend(marker.position);

               }else{var tittle='End';

                        var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: tittle
                        });
                        (function (marker, data) {
                            google.maps.event.addListener(marker, "click", function (e) {
                                infoWindow.setContent("<div style = 'width:100px;min-height:20px'>Drive END</div>");
                                infoWindow.open(map, marker);
                            });
                        })(marker, data);
             
                        //Extend each marker's position in LatLngBounds object.
                        latlngbounds.extend(marker.position);


             }
                
            }
            }
             // create a new polyline using the coordinates array, and add it to the map
          var tripPath = new google.maps.Polyline({
            path: tripCoordinates,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 4
          });
          tripPath.setMap(map);
          
          //Get the boundaries of the Map.
            var bounds = new google.maps.LatLngBounds();
            //Center map and adjust Zoom based on the position of all markers.
            map.setCenter(latlngbounds.getCenter());
            map.fitBounds(latlngbounds);
        }
    }
   
</script>
