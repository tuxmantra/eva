  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
    tfoot {
     display: table-header-group !important;
    }
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
  </style>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
                    <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     

                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
            <div class="col-md-12" align="right">
              <button title="Add Api Key" class="btn btn-primary btn-sm" onclick="addapikey()" >Add</button>
              
            </div>
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  <h4 class="card-title">Google Map API Key</h4>
                </div>
                <div class="card-body">
                  <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>
                  <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">API key</th>
                          <th class="disabled-sorting  table-heading-font">Actions</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">API key</th>
                          <th class="disabled-sorting  table-heading-font">Actions</th>
                        </tr>
                      </tfoot>
                      <tbody>
                       @if (!empty($gapikey))
                        <?php $id = 1; ?>
                        @foreach ($gapikey as $value)  
                        <tr>
                         
                          <td class="table-heading-font" style="background-color: #f5f5f5; width:8%">{{$id}}</td>
                          <td style="background-color: #ffffff; width:60%" class="table-heading-font"><input type="text" name="" id="{{$value->id}}" class="form-control" value="{{$value->api_key}}"></td>
                          <td class="table-heading-font" style="background-color: #f5f5f5; width:15%" >
                            <button  data-toggle="tooltip" title="Api Key" onclick="save_gkey({{$value->id}})" class="btn btn-sm btn-primary">Save</button>
                            <button  data-toggle="tooltip" title="Api Key" onclick="delete_gkey({{$value->id}})" class="btn btn-sm btn-primary">Delete</button>
                          </td>
                        </tr>
                        <?php $id++; ?>  
                        @endforeach
                        @else
                           <tr><td colspan="9" align="center">No Key Exist.</td></tr>
                        @endif
                       
                       
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')

       @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif

                      <script>
 
    $(document).ready(function() {
 
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables').DataTable();

      $('#datatables tfoot th').each( function () {
        var title = $(this).text();
        if(title == "S.No"){
           title = "Serial";
        }
        if(title == "Partner"){
            var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                .appendTo( $(this).empty() )
                .on( 'change', function () {

                    var val = $(this).val();

                    table.column( 3 ) //Only the first column
                        .search( val ? '^'+$(this).val()+'$' : val, true, false )
                        .draw();
                } );

                table.column( 3 ).data().unique().sort().each( function ( d, j ) {
                if(d != '-'){
                  select.append( '<option value="'+d+'">'+d+'</option>' );
                }else{
                  select.append( '<option value="'+d+'">Nil</option>' );
                }
            } );
        }else if(title == "Type"){
            var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                .appendTo( $(this).empty() )
                .on( 'change', function () {

                    var val = $(this).val();

                    table.column( 4 ) //Only the first column
                        .search( val ? '^'+$(this).val()+'$' : val, true, false )
                        .draw();
                });

                table.column( 4 ).data().unique().sort().each( function ( d, j ) {

                  select.append( '<option value="'+d+'">'+d+'</option>' );
                
                });
        }else{
           $(this).html( '<input class="'+title+'_tp" type="text" placeholder=" '+title+'" />' );
        }
      });

      table.columns([1,2,3,4,5]).every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
          });
      });

    });

function save_gkey(id){
      var key = $("#"+id).val();
      $.post('{{url('/')}}/update_gkey', { key: key, id: id,update:'1' }).done(function (data) {
                Swal.fire({
                            type: 'success',
                            title: 'Success',
                            text:   'Api Key Updated',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
         });
    }

function delete_gkey(id){
      var key = '';
      $.post('{{url('/')}}/update_gkey', { key: key, id: id,update:'2' }).done(function (data) {
                Swal.fire({
                            type: 'success',
                            title: 'Success',
                            text:   'Api Key Deleted',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                location.reload();
         });
    }
function creat_gkey(){
      var key = $("#new_key").val();
      var id='';
      $.post('{{url('/')}}/update_gkey', { key: key, id: id,update:'0' }).done(function (data) {
                Swal.fire({
                            type: 'success',
                            title: 'Success',
                            text:   'Api Key Created',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                location.reload();
         });
    }
function addapikey(){
      
     $('#datatables tbody').append($('<tr><td>{{$id}}</td><td style="background-color: #ffffff;" class="table-heading-font"><input type="text" name="" id="new_key" class="form-control" value=""></td><td><button  data-toggle="tooltip" title="Api Key" onclick="creat_gkey()" class="btn btn-sm btn-primary">Create</button></td></tr>'));
   }
  </script>