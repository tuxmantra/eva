  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
  
             <div class="col-md-12" align="right">
              <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
                
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Edit Core Data</h4>
                  </div>
                </div>

                <div class="card-body ">
                  <form method="post" action="{{url('/')}}/edit-core-data-save" autocomplete="off" class="form-horizontal" onsubmit="return editCoreData();">
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Title</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                         <input type="hidden" name="cid" value="{{App\Helpers\Helper::crypt(1,$core_data[0]->id)}}">
                         <input type="hidden" value="{{$form_token}}" name="form_token">
                          <input autocomplete="off" type="text" name="title" id="title" class="form-control" placeholder="Enter Title" value="{{$core_data[0]->title}}">
                        </div>
                      </div>
                    </div>
   
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Device1</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="device1" id="device1" placeholder="Enter Device1" value="{{$core_data[0]->device1}}">
                        </div>
                      </div>
                    </div>
                         <div class="row">
                      <label class="col-sm-2 col-form-label">Device2</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" name="device2" id="device2" class="form-control" placeholder="Enter Device2" value="{{$core_data[0]->device2}}">
                        </div>
                      </div>
                    </div>
                         <div class="row">
                      <label class="col-sm-2 col-form-label">Decimal</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="false" type="number" name="decimal" id="decimal" class="form-control" placeholder="Enter Decimal" value="{{$core_data[0]->decim}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Unit</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="unit" id="unit" placeholder="Enter Unit" value="{{$core_data[0]->unit}}">
                        </div>
                      </div>
                    </div>

                  <!--   <div class="row">
                      <label class="col-sm-2 col-form-label">Type</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <select class="selectpicker" data-style="select-with-transition" title="Select User Type" id="type" data-size="7" name="type" tabindex="-98">
                            <option disabled=""> Select TYpe</option>
                            <option <?php if($core_data[0]->type == 1){ ?> selected <?php } ?> value="1">Map</option>
                            <option <?php if($core_data[0]->type == 2){ ?> selected <?php } ?> value="2">Graph</option>
                          </select>
                          
                        </div>
                      </div>
                    </div> -->

                          <div class="row" style="margin-top: 25px;">
                      <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-10 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" 
                            value="1" <?php if($core_data[0]->status == 1){ ?> checked <?php } ?>> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0" <?php if($core_data[0]->status == 0){ ?> checked <?php } ?>> In Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>

            </div>
                      <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          </div>
        </div>
      </div>
       </form>
     
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
