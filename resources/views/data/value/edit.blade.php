  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
    
                <div class="col-md-12" align="right">
              <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Edit Value Data</h4>
                  </div>
                </div>
                <?php $value = explode(':', $value_data[0]->dtype); 
                            // print_r($value);die;
                            ?>
                <div class="card-body ">
                  <form method="post" action="{{url('/')}}/edit-value-data-save" autocomplete="off" class="form-horizontal" onsubmit="return editValuevalid_new();">
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Title</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                         <input type="hidden" name="cid" value="{{App\Helpers\Helper::crypt(1,$value_data[0]->id)}}">
                         <input type="hidden" value="{{$form_token}}" name="form_token">
                          <input autocomplete="off" type="text" name="title" id="title" class="form-control" placeholder="Enter Title" value="{{$value_data[0]->title}}">
                        </div>
                      </div>
                    </div>
   
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Device1</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="device1" id="device1" placeholder="Enter Device1" value="{{$value_data[0]->device1}}">
                        </div>
                      </div>
                    </div>
                         <div class="row">
                      <label class="col-sm-2 col-form-label">Device2</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" name="device2" id="device2" class="form-control" placeholder="Enter Device2" value="{{$value_data[0]->device2}}">
                        </div>
                      </div>
                    </div>
                         <div class="row">
                      <label class="col-sm-2 col-form-label">Decimal</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="false" type="number" name="decimal" id="decimal" class="form-control" placeholder="Enter Decimal" value="{{$value_data[0]->decim}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Unit</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="unit" id="unit" placeholder="Enter Unit" value="{{$value_data[0]->unit}}">
                        </div>
                      </div>
                    </div>
                      <div class="row">
                      <label class="col-sm-2 col-form-label">Column Name</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" disabled="" class="form-control" name="clmn" id="clmn" placeholder="Enter Column Name" value="{{$value_data[0]->device_data_column}}">
                        </div>
                      </div>
                    </div>
                      <div class="row">
                      <label class="col-sm-2 col-form-label">Type</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <select class="selectpicker valtype_create" data-style="select-with-transition" title="Select Type" data-size="7" name="val_type" tabindex="-98">
                            <option value="1" <?php if($value_data[0]->type == 1){ ?> selected="selected" <?php } ?>>Map</option>
                            <option value="2" <?php if($value_data[0]->type == 2){ ?> selected="selected" <?php } ?>>Graph</option>
                          </select>
                        </div>
                      </div>
                    </div>
                     <div class="row">
                      <label class="col-sm-2 col-form-label">Device Type</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <select class="selectpicker dtype_create" data-style="select-with-transition" title="Select Device Type" data-size="7" name="dtype[]" tabindex="-98" multiple>

                            <option value="1" <?php if(in_array("1", $value)){ ?> selected="selected" <?php } ?>>Bluetooth</option>
                            <option value="2" <?php if(in_array("2", $value)){ ?> selected="selected" <?php } ?>>GSM</option>
                             <option value="3" <?php if(in_array("3", $value)){ ?> selected="selected" <?php } ?>>LINUX</option>
                          </select>
                        </div>
                      </div>
                    </div>
                          <div class="row" style="margin-top: 25px;">
                      <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-10 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" 
                            value="1" <?php if($value_data[0]->status == 1){ ?> checked <?php } ?>> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0" <?php if($value_data[0]->status == 0){ ?> checked <?php } ?>> In Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>

            </div>
                    <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                    
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          </div>
        </div>
      </div>
       </form>

    </div>
  </div>
 
  <!--   Core JS Files   -->
 @include('default.footer')
 @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif

 <script type="text/javascript">
   function editValuevalid_new(){
  /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
  $('#serve_message').hide();
  $('#cli_message').hide();
  var title    = $('#title').val();
  var device1  = $('#device1').val();
  var device2  = $('#device2').val();
  var decimal  = $('#decimal').val();
  var unit     = $('#unit').val();
  var type     = $('#type').val();
  if(title == '' || device1 == '' || device2 == '' || decimal == '' || unit == '' || type == ''){
     Swal.fire({
            type: 'info',
            title: 'info',
            text: 'Fields required.',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
      return false;
  }else{
      return true;
  }
}

 </script>
