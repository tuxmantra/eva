  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <?php 
  if(!empty($_GET['vin_no']))
  {
   $vin_noreq         = $_GET['vin_no'];
   $vehiclereq        = $_GET['vehicle'];
   $testresult        = $_GET['result'];
  }
  else
  {
    $vin_noreq         = "";
    $vehiclereq        = "";
    $testresult        = "";
  }
   // echo $vehicle;die;
   ?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

  <style type="text/css">
    .content .filter-option-inner-inner {
      position: relative;
      left: 8px;
    }
    .table-heading-font{
        font-size: 13px !important;
    }
.card .card-body {
     padding: 0 ; 
    position: relative; 
    }
    .datepicker table tr td.active:hover, .datepicker table tr td.active:hover:hover, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active:focus, .datepicker table tr td.active:hover:focus, .datepicker table tr td.active.disabled:focus, .datepicker table tr td.active.disabled:hover:focus, .datepicker table tr td.active:active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.active, .datepicker table tr td.active:hover.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:hover.active, .open .dropdown-toggle.datepicker table tr td.active, .open .dropdown-toggle.datepicker table tr td.active:hover, .open .dropdown-toggle.datepicker table tr td.active.disabled, .open .dropdown-toggle.datepicker table tr td.active.disabled:hover {
    color: #ffffff;
    background-color: #34495e;
    border-color: #34495e;
}
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    #datetimepicker12 .datepicker-inline {
    width: 335px !important;
    }
    table.table-condensed {
    width: 78%;
   }
/*    tr div {
      width: auto !important;
      z-index: auto !important;
    }*/
/*    .tab-pane tbody {
      display: block;
      max-height: 230px;
      overflow-y: scroll;
    }*/
    .ScrollStyle
    {
        max-height: 300px;
        overflow-y: scroll;
    }
    #map{
          background-color: aliceblue;
          margin-top: 30px !important;
    }
    .divTop{
      margin-top: 35px;
    }

.non-highlighted-cal-dates{
  background-color: #adb3b2;
}
.scrollable-menu {
    height: auto;
    max-height: 200px;
    overflow-x: hidden;
}
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-md-12" align="center">
              
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">show_chart</i>
                  </div>
                  <h4 class="card-title">EVA</h4>
                  <div class="row" id='vehicledetails'>
                      <!-- User and Vehicle Details to display from js -->
                  </div>
                  <br>
                </div>
                <div class="card-body">
                  <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="col-md-12" id="load" style="display: none;" align="center">
                  <img src="public/assets/img/loader.gif" style="width: 20%;">
                 </div>
                  <input type="hidden" name="devid" id="devid" value="">
                <div class="card-body">
                   <div class="col-md-12" style="text-align: center;">
                    <div class="col-md-12" align="right">

                        <!--  <button class="btn btn-primary btn-sm pull-right" id='goLive' style='display: none' onclick="goLive()">Go Live</button>
                        <button class="btn btn-primary btn-sm pull-right" id='stopLive' style='display: none' onclick="stopLive()">Stop Live</button> -->
                        <button class="btn btn-primary btn-sm" onclick="showTestDate()">reload</button>
                        <button onclick="generateReportCsv()" class="btn btn-primary btn-sm" id="btnSubmit1"><i class="material-icons">cloud_download</i></button>

                         <button class="btn btn-primary" id="loadbutton" type="button" disabled style="display: none;">
                          <div class="spin"></div> <p style="margin-left: 16%;margin-bottom: -10%; margin-top: -6%;"><i class="material-icons">cloud_download</i></p></button>
                      </div>
                      <div class="row">
                      <div class="col-md-4">
                        
                         <select class="selectpicker vehicle" data-style="select-with-transition"  data-container="body" title="Select Vehicle" data-size="7" name="selectvehicle" id="selectvehicle" >
                          <option class="dropdown-item" value="" selected>All Vehicles</option>
                          <?php if($vehiclelist){ foreach ($vehiclelist as $key => $vahicle) if(!empty($vahicle->vehicle)){{ ?>
                            <option class="dropdown-item" <?php if(count($vehiclelist) == 1 || $vehiclereq == $vahicle->vehicle){echo "selected";} ?> value="<?=$vahicle->vehicle?>"><?=$vahicle->vehicle?></option>
                          <?php }}} ?>
                         </select>
                      </div>
                      <div class="col-md-4">

                         <select class="selectpicker vin_no" data-style="select-with-transition" data-live-search="true"  data-container="body" title="Select VIN No" data-size="7" name="vin_no" id="vin_no" >
                          <option class="dropdown-item" value="" selected>All VIN NO</option> vinlist
                          <?php if($vinlist){ foreach ($vinlist as $key => $vin) if(!empty($vin->vin_no)){{ ?>
                            <option class="dropdown-item" <?php if(count($vinlist) == 1 || $vin_noreq == $vin->vin_no){echo "selected";} ?> value="<?=$vin->vin_no?>"><?=$vin->vin_no?></option>
                          <?php }}} ?>
                         </select>
                      </div>
                      <div class="col-md-4">

                         <select class="selectpicker valueType1" data-style="select-with-transition"  data-container="body" title="Select Test Type" data-size="7" name="test_type" id="test_type" >
                          <option class="dropdown-item" value="" selected>All Test</option> testlist
                          <?php if($testlist)
                          {
                            foreach ($testlist as $key => $test) if(!empty($test->F43) && ($test->F43 == 3 || $test->F43 == 4))
                            {
                                if($test->F43 == 3)
                                { ?>
                                  <option class="dropdown-item"  value="<?=$test->F43?>">FC Test</option>
                                  <?php 
                                }
                                else 
                                {
                                  ?>
                                  <option class="dropdown-item"  value="<?=$test->F43?>">Other Test</option>
                                  <?php 
                                } 
                            }
                          }
                        
                          ?>
                          
                         </select>
                      </div>
                      
                    </div>
                  </div>
                  <div class="col-md-12" style="text-align: center;">
                    </br>
                  <div class="row" >
                
                        <div class="col-md-4">
                         
                          <select class="selectpicker valueType1" data-style="select-with-transition"  data-container="body" title="Select Red Line" data-size="7" name="valueType1" id="valueType1" >
                            <option class="dropdown-item" value="">Select Red Line</option>
                            @if (!empty($valuePartList))
                            @foreach ($valuePartList as $vpl)
                            <option <?php if ($vpl->id == '4'){ ?> selected <?php } ?>  value="{{$vpl->id}}">{{$vpl->title}}</option>
                            @endforeach
                            @endif
                          </select>
                        </div>
                      
                         <div class="col-md-4">
                         
                          <select class="selectpicker valueType1" data-style="select-with-transition" data-container="body" title="Select Blue Line" data-size="7" name="valueType2" id="valueType2" tabindex="-98">
                            <option value="">Select Blue Line</option>
                            @if (!empty($valuePartList))
                            @foreach ($valuePartList as $vpl)
                            <option  value="{{$vpl->id}}">{{$vpl->title}}</option>
                            @endforeach
                            @endif
                          </select>
                        </div>
                      
                        <div class="col-md-4">
                       
                          <select class="selectpicker valueType1" data-style="select-with-transition" data-container="body" title="Select Green Line" data-size="7" name="valueType3" id="valueType3" tabindex="-98">
                            <option value="">Select Green Line</option>
                            @if (!empty($valuePartList))
                            @foreach ($valuePartList as $vpl)
                            <option  value="{{$vpl->id}}">{{$vpl->title}}</option>
                            @endforeach
                            @endif
                          </select>
                        </div>
              </div>
            </div>
                  
                   <div class="loader" id="loader" style="display: none;">Loading...</div>
                  <div class="col-md-12" id="load_data">

                  

                <!-- Process Eva -->


                 
       
                  </div>
                  
                </div>

                    </div>
                     <div class="fixed-plugin">
                  <div class="dropdown show-dropdown">
                    <a href="#" data-toggle="dropdown" aria-expanded="false">
                     <span class="material-icons" style="font-size: 50px;     color: white;">
                    directions_car
                    </span>
                    </a>
                    <ul class="dropdown-menu" id="device_select" x-placement="bottom-start" style="position: absolute; top: 41px; left: 8px; will-change: top, left;">
                      <div id="device_select">
                       <div id="datetimepicker12" style="width: 100%">
                          
                          </div>
                      <div class="ScrollStyle tab-pane active" id="profile">
                                <input type="text" name="search" id="search" placeholder="Search Devices" class="form-control" style="background-color: #f9f5f5;">
                                    <table class="table" id="list_device">
                                     <tbody>
                                       <?php if($user_category == 1 || $user_category == 2 || $user_category == 3){ ?>
                                       <?php if(!empty($device_list)){ ?>
                                         <?php foreach($device_list as $key => $dl){ ?>
                                       <tr id="row_<?= $dl->device_id ?>" onclick="device_row('<?= $dl->device_id ?>');">

                                         <td><p style="font-size: 13px; margin-bottom: 0;"><a class="nav-link" href="javascript:void(0)"><?php echo $dl->device_name." (Device ID ".$dl->device_id.")
                                         "; ?></p></a></td>
                                       </tr>
                                       <?php } ?>
                                     <?php }else{ ?>
                                       <tr>
                                         <td>Empty</td>
                                       </tr>
                                       <?php } ?>
                                       <?php }else{ ?>
                                          <tr>

                                         <td>Sign contract for "What are conference organizers afraid of?"</td>

                                       </tr>

                                       <?php } ?>

                                     </tbody>
                                   </table>
                                  </div>
                                </div>
                    </ul>
                  </div>
                </div>
                <hr>
                
                    </div>

                  </div>
                  
                 <div class="col-md-12" id="load" style="display: none;" align="center">
                  <img src="public/assets/img/loader.gif" style="width: 20%;">
                 </div>
                  <input type="hidden" name="devid" id="devid" value="">

                
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>

    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')

<script type="text/javascript">
  var stoplive   = 0;
  var buttonlive = 0;      
  var requests   = [];  


function downloadURL(url)
{
    var hiddenIFrameID = 'hiddenDownloader',
    iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;
};

function generateReportCsv()
{
    $('#btnSubmit1').hide();
    $('#loadbutton').show();
    var date    = $('#datetimepicker12').datepicker('getFormattedDate');
    var did     = $('#devid').val();
    var dt      = date.split("/");
    var csvDate = dt[1]+""+dt[0]+""+dt[2];
    var vehicle = $('#selectvehicle').val(); 
    // var vehicle   = $('#selectvehicle').val(); 
    var vin_no    = $('#vin_no').val();
    var test_type = $('#test_type').val();
    if(date != '' && did != ''){
      $.post('{{url('/')}}/downloadCsvEva', { date: date, did: did,vehicle:vehicle,vin_no:vin_no , test_type:test_type }).done(function (data) {
          // if(data == '1'){
          downloadURL('http://evaeoldev.enginecal.com/datacsv/'+did+'/'+csvDate+'.csv');
          $('#btnSubmit1').show();
          $('#loadbutton').hide();
      });
    }
}

$(document).ready(function() {

    // Search in device list
    // 
    $( "#search" ).keyup(function() {
        var did    = $('#devid').val();
        var search = $('#search').val();
        $.post('{{url('/')}}/getDeviceListobd', { search: search, did: did, type:'2' }).done(function (data) {
            $('#list_device').html(data);
        });
    });

    // Load drive dates in datePicker
    var today               = new Date();
    var today_formatted     = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+('0'+today.getDate()).slice(-2);
    var user_busy_days      = [<?= $drive_dates ?>];

    $('#datetimepicker12').datepicker({
          inline: true,
          sideBySide: true,
          beforeShowDay: function (date) {
            calender_date = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+('0'+date.getDate()).slice(-2);
            var search_index = $.inArray(calender_date, user_busy_days);
            if (search_index > -1) {
                return {classes: 'non-highlighted-cal-dates', tooltip: 'Drive Took'};
            }else{
                return {classes: 'highlighted-cal-dates', tooltip: 'No Drive'};
            }
          }
    }).on('click', showTestDate);

    $('#datetimepicker12').datepicker('setDate', '<?=$today?>');

    // Load Graph By Page Load
    $('#load_data').hide();
    $('#loader').show();
    var red    = '4';
    var blue   = '';
    var yellow = '';
    var green  = '';
    // var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    // var today = new Date();
    // var dd = String(today.getDate()).padStart(2, '0');
    // var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    // var yyyy = today.getFullYear();

    // getUservehicleDetails('<?=$did ?>',today);
    var date = '<?=$today?>';
    getUservehicleDetails('<?=$did ?>',date,'<?=$testresult?>');
    var vehicle   = $('#selectvehicle').val(); 
    var vin_no    = $('#vin_no').val();
    // $('#testdate').html('<b>Test Date : </b> '+today)
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: '<?=$did ?>', 'date': date, 'state': '2', 'g1': '', 'g2': '4', 'g3': '', 'g4': '',vehicle:vehicle,vin_no:vin_no }).done(function (data) {
        $('#loader').hide();
        $('#load_data').show();
        $('#load_data').html(data);
        
    }));
});    

// Load Graph By Device
function device_row(did)
{
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
  $('#title').text('DeviceID : '+did);
    $('#load_data').hide();
    $('#loader').show();
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    $('tr').css({ 'background-color' : '#fff'});
    $('#row_'+did).css({ 'background-color' : 'aliceblue'});
    $('#devid').val(did);

    var red    = $("#valueType1 option:selected").val();
    if(red == ''){ red = '4'; }
    var blue   = $("#valueType2 option:selected").val();
    var yellow = $("#valueType3 option:selected").val();
    var green  = $("#valueType4 option:selected").val();
    $('#test_type').html('<option class="dropdown-item" value="" selected>All VIN NO</option>').selectpicker('refresh');
    $('#vin_no').html('<option class="dropdown-item" value="" selected>All VIN NO</option>').selectpicker('refresh');
    $.get('{{url('/')}}/getVehicle', { did: did, 'date': date}).done(function (data) {
        $('#selectvehicle').html(data);
        $('.selectpicker').selectpicker('refresh');
        // $('#selectvehicle').Refresh();
    }); 
    var vehicle = $('#selectvehicle').val();
    requests.push($.get('{{url('/')}}/getVinNo', { did: did, 'date': date, vehicle:vehicle}).done(function (data) {
        // $('#loader').hide();
        // $('#load_data').show();
        $('#vin_no').html(data).selectpicker('refresh');

    }));
    var vin_no = $("#vin_no option:selected").val();
    requests.push($.get('{{url('/')}}/getTestType', { did: did, 'date': date, vehicle:vehicle, vin_no : vin_no}).done(function (data) {
        // $('#loader').hide();
        // $('#load_data').show();
        $('#test_type').html(data).selectpicker('refresh');

    }));
    getUservehicleDetails(did,date,'<?=$testresult?>');
    requests.push($.post('{{url('/')}}/getEvaGraph', {  did: did, date: date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow, 'g4': green,'dd_date': '1' }).done(function (data) {
        $('#loader').hide();
        $('#load_data').show();
        $('#load_data').html(data);
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = mm + '/' + dd + '/' + yyyy;
        
        $('#datetimepicker12').datepicker('remove');
        var today           = $('#selected_date').val();
        // var today_formatted = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+('0'+today.getDate()).slice(-2);
        var service_dates   = $('#service_date').val();
        var user_busy_days  = [service_dates];

          $('#datetimepicker12').datepicker({
            inline: true,
            sideBySide: true,
            beforeShowDay: function (date) {
              calender_date = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+('0'+date.getDate()).slice(-2);
              var search_index = $.inArray(calender_date, user_busy_days);
              var service_dates       = $('#service_date').val();
              var n = service_dates.includes(""+calender_date);

              if (n == true) {
                return {classes: 'non-highlighted-cal-dates', tooltip: 'Drive Took'};
              }else{
                return {classes: 'highlighted-cal-dates', tooltip: 'No Drive'};
              }
            }
          });
          $('#datetimepicker12').datepicker('setDate', today);
    }));
}



// Load Graph By DatePicker
function showTestDate()
{
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
    $('#load_data').hide();
    $('#loader').show();
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    var did    = $('#devid').val();
    getUservehicleDetails(did,date,'<?=$testresult?>');
    var red    = $("#valueType1 option:selected").val();
    if(red == ''){ red = '4'; }
    var blue   = $("#valueType2 option:selected").val();
    var yellow = $("#valueType3 option:selected").val();
    $('#test_type').html('<option class="dropdown-item" value="" selected>All VIN NO</option>').selectpicker('refresh');
    $('#vin_no').html('<option class="dropdown-item" value="" selected>All VIN NO</option>').selectpicker('refresh');
    $.get('{{url('/')}}/getVehicle', { did: did, 'date': date}).done(function (data) {
        $('#selectvehicle').html(data);
        $('.selectpicker').selectpicker('refresh'); 
        // $('#selectvehicle').Refresh();
    }); 
    var vehicle = $('#selectvehicle').val();
    requests.push($.get('{{url('/')}}/getVinNo', { did: did, 'date': date, vehicle:vehicle}).done(function (data) {
        // $('#loader').hide();
        // $('#load_data').show();
        $('#vin_no').html(data).selectpicker('refresh');

    }));
    var vin_no = $("#vin_no option:selected").val();
    requests.push($.get('{{url('/')}}/getTestType', { did: did, 'date': date, vehicle:vehicle, vin_no : vin_no}).done(function (data) {
        // $('#loader').hide();
        // $('#load_data').show();
        $('#test_type').html(data).selectpicker('refresh');

    }));
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: did, 'date': date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow}).done(function (data) {
        $('#loader').hide();
        $('#load_data').show();
        $('#load_data').html(data);
    }));
    // Live chart Logic
    var today = new Date();
    var dd    = String(today.getDate()).padStart(2, '0');
    var mm    = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy  = today.getFullYear();
    today     = mm + '/' + dd + '/' + yyyy;
    
}

// Load By Red Line

$( ".valueType1" ).change(function() {
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
    $('#load_data').hide();
    $('#loader').show();
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    var did    = $('#devid').val();
    var vehicle   = $('#selectvehicle').val(); 
    var vin_no    = $('#vin_no').val();
    var test_type = $('#test_type').val(); 
    var red       = $("#valueType1 option:selected").val();
    var blue      = $("#valueType2 option:selected").val();
    var yellow    = $("#valueType3 option:selected").val();
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: did, 'date': date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow, vehicle:vehicle,vin_no:vin_no , test_type:test_type}).done(function (data) {
        $('#loader').hide();
        $('#load_data').show();
        $('#load_data').html(data);
    }));
});


$( ".vehicle" ).change(function() {
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
    $('#load_data').hide();
    $('#loader').show();
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    var did    = $('#devid').val();
    var vehicle = $('#selectvehicle').val(); 
    var red    = $("#valueType1 option:selected").val();
    var blue   = $("#valueType2 option:selected").val();
    var yellow = $("#valueType3 option:selected").val();
    $('#test_type').html('<option class="dropdown-item" value="" selected>All VIN NO</option>').selectpicker('refresh');
    $('#vin_no').html('<option class="dropdown-item" value="" selected>All VIN NO</option>').selectpicker('refresh');
    requests.push($.get('{{url('/')}}/getVinNo', { did: did, 'date': date, vehicle:vehicle}).done(function (data) {
        // $('#loader').hide();
        // $('#load_data').show();
        $('#vin_no').html(data).selectpicker('refresh');

    }));
    var vin_no = $("#vin_no option:selected").val();
    requests.push($.get('{{url('/')}}/getTestType', { did: did, 'date': date, vehicle:vehicle, vin_no : vin_no}).done(function (data) {
        // $('#loader').hide();
        // $('#load_data').show();
        $('#test_type').html(data).selectpicker('refresh');

    }));
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: did, 'date': date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow, vehicle:vehicle}).done(function (data) {
        $('#loader').hide();
        $('#load_data').show();
        $('#load_data').html(data);
    }));
});


$( ".vin_no" ).change(function() {
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
    $('#load_data').hide();
    $('#loader').show();
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    var did    = $('#devid').val();
    var vehicle = $('#selectvehicle').val(); 
    var red    = $("#valueType1 option:selected").val();
    var blue   = $("#valueType2 option:selected").val();
    var yellow = $("#valueType3 option:selected").val(); 
    var vin_no = $("#vin_no option:selected").val(); 
    requests.push($.get('{{url('/')}}/gettestresult', { did: did, 'date': date, vehicle:vehicle,vin_no : vin_no}).done(function (data) {
        
        $('#vehicledetails').html(data);

    }));
    requests.push($.get('{{url('/')}}/getTestType', { did: did, 'date': date, vehicle:vehicle, vin_no : vin_no}).done(function (data) {
        // $('#loader').hide();
        // $('#load_data').show();
        $('#test_type').html(data).selectpicker('refresh');

    }));
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: did, 'date': date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow, vehicle:vehicle,vin_no:vin_no}).done(function (data) {
        $('#loader').hide();
        $('#load_data').show();
        $('#load_data').html(data);
    }));
});


$('#devid').val('<?= $did ?>');

</script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> 