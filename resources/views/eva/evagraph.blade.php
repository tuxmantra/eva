<!-- Chartist JS -->
<input name = "service_date" type = "hidden" id = "service_date" value = "<?= $drive_dates ?>">
<script src = "{{url('/')}}/public/assets/js/moment.js"></script>
<script src = "{{url('/')}}/public/assets/js/highcharts.js"></script>
<script src = "{{url('/')}}/public/assets/js/exporting.js"></script>


<script type="text/javascript">
<?php
	$paramValBT = !empty($paramValB->title) ? $paramValB->title:""; 
	$paramValBU = !empty($paramValB->unit) ? $paramValB->unit:"";

	$paramValAT = !empty($paramValA->title) ? $paramValA->title:""; 
	$paramValAU = !empty($paramValA->unit) ? $paramValA->unit:"";
?>

$('#container').highcharts({
		chart: {
			zoomType: 'x'
		},
        title: {
				text: "<?= $paramValAT ?><?php if($paramValBT){?>\tVS\t<?= $paramValBT ?><?php } ?>",
				style: {
					color: '#34495e',
				}
	  	},
        xAxis: [{
			type: 'datetime', minRange: 100, 
			title: {
				text: 'Time',
				style: {
					color: '#34495e',
					fontWeight: 'bold'				
				}
			},
			min:<?=$min?>,
			max:<?=$max?>
        }],
        yAxis: [ { // Tertiary yAxis
            gridLineWidth: 1,
            title: {
                text: '<?= $paramValBT ?>',
                style: {
                    color: '#34495e',
					fontWeight: 'bold'	
                }
            },
			//min:0,
            labels: {
                format: '{value} <?= $paramValBU ?>',
                style: {
                    color: '#34495e'
                },
				align : 'right',
				x : 0,
				y : -2,
            },
            opposite: true
        },{ // Secondary yAxis
            gridLineWidth: 1,
            title: {
                text: '<?= $paramValAT ?>',
                style: {
                    color: '#eb262d',
					fontWeight: 'bold'	
                }
            },
			//min:0,
            labels: {
                format: '{value} <?= $paramValAU ?>',
                style: {
                    color: '#eb262d'
                },
				align : 'left',
				x : 0,
				y : -2
            }
        }],
		
        tooltip: {
			formatter : function() {
				var ms                  = this.x;
				var secs                = Math.floor(ms / 1000);
				var hours               = Math.floor(secs / (60 * 60));
				var divisor_for_minutes = secs % (60 * 60);
				var minutes             = Math.floor(divisor_for_minutes / 60);
				var divisor_for_seconds = divisor_for_minutes % 60;
				var seconds             = Math.ceil(divisor_for_seconds);
				var timeStr             = "";
				if (hours > 0) timeStr += hours + ":";
				if (hours > 0 || minutes > 0) timeStr += minutes + ":";
				timeStr += seconds + "." + (ms % 1000);
				return '[' + timeStr + '] ' + this.series.name + ': <strong>' + this.y + '</strong>';
			}
        },
		
        legend: {
			enabled: false
        },
        series: [ {
            name: '<?= $paramValAT ?>',
            type: 'spline',
			      color: '#eb262d',
            yAxis: 1,
            data: [<?= $datA ?>],
            /*marker: {
                enabled: false
            },*/
            tooltip: {
                formatter : function() {
					var ms = this.x;
					var secs = Math.floor(ms / 1000);
					var hours = Math.floor(secs / (60 * 60));
					var divisor_for_minutes = secs % (60 * 60);
					var minutes = Math.floor(divisor_for_minutes / 60);
					var divisor_for_seconds = divisor_for_minutes % 60;
					var seconds = Math.ceil(divisor_for_seconds);
					var timeStr = "";
					if (hours > 0) timeStr += hours + ":";
					if (hours > 0 || minutes > 0) timeStr += minutes + ":";
					timeStr += seconds + "." + (ms % 1000);
					return '[' + timeStr + '] ' + this.series.name + ': <strong>' + this.y + '</strong>';
				}
            }
        }, {
            name: '<?= $paramValBT ?>',
            type: 'spline',
		     	  color: '#34495e',
            data: [<?= $datB ?>],
            tooltip: {
            valueSuffix: ' <?= $paramValBU ?>'
            }
        }]
    });

</script>

<div id="container" class="col-md-12 col-sm-12 col-xs-12" style="height: 480px; margin: 0 auto"></div>