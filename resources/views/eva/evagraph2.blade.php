<input name="service_date" type="hidden" id="service_date" value="<?= $drive_dates ?>">
<input name="selected_date" type="hidden" id="selected_date" value="<?= $today ?>">

<script type="text/javascript">
<?php

$paramValAT = !empty($paramValA->title) ? $paramValA->title:""; 
$paramValAU = !empty($paramValA->unit) ? $paramValA->unit:"";

$paramValBT = !empty($paramValB->title) ? $paramValB->title:""; 
$paramValBU = !empty($paramValB->unit) ? $paramValB->unit:"";

$paramValCT = !empty($paramValC->title) ? $paramValC->title:""; 
$paramValCU = !empty($paramValC->unit) ? $paramValC->unit:"";

$str1       = !empty($paramValAT)? $paramValAT:"-";
$str2       = !empty($paramValBT)? $paramValBT:"-";
$str3       = !empty($paramValCT)? $paramValCT:"-";

$title =  "";
if($str2 !="-" && $str2 !=""){
    $title.= $str2;
}
if($str1 !="-" && $str1 !=""){
    $title.= "  vs  " .$str1 ;
}
if($str3 !="-" && $str3 !=""){
    $title.= "  vs  " .$str3 ;
}
// print_r($datA);   
?>
var H = Highcharts;
var extremesXmin = [],
  extremesXmax = [],
  zoomLevel = 0;

H.Chart.prototype.showResetZoom = function() {
  {
    var chart = this,
      lang = H.defaultOptions.lang,
      btnOptions = chart.options.chart.resetZoomButton,
      theme = btnOptions.theme,
      states = theme.states,
      alignTo = btnOptions.relativeTo === 'chart' ? null : 'plotBox';

    function zoomOut() {
      if (zoomLevel > 1) {
        chart.xAxis[0].setExtremes(extremesXmin[extremesXmin.length - 2], extremesXmax[extremesXmax.length - 2])
        extremesXmin.pop()
        extremesXmax.pop()
        zoomLevel--;
      } else {
        chart.zoomOut();
      }
    }

    H.fireEvent(this, 'beforeShowResetZoom', null, function() {
      chart.resetZoomButton = chart.renderer.button(

          lang.resetZoom,
          null,
          null,
          zoomOut,
          theme,
          states && states.hover
        )
        .attr({
          align: btnOptions.position.align,
          title: lang.resetZoomTitle
        })
        .addClass('highcharts-reset-zoom')
        .add()
        .align(btnOptions.position, false, alignTo);
    });

  }
}


$(function () {
    const timezone = new Date().getTimezoneOffset()
    Highcharts.setOptions({
        global: {
            timezoneOffset: timezone
        }
    });
    $('#container').highcharts({
        chart: {
            zoomType: 'x',
            events: {
          selection: function(e) {
            if (e.xAxis) {
              extremesXmin.push(e.xAxis[0].min)
              extremesXmax.push(e.xAxis[0].max)
            }
            zoomLevel++;
          }
        },
			panning: true,
            panKey: 'shift',
            resetZoomButton: {
            position: {
                // align: 'right', // by default
                // verticalAlign: 'top', // by default
                x: -30,
                y: -50
            }
        }
        },
        title: {
            text: '<?=$title?>',
            style: {
                color: '#34495e', 
                fontWeight: 'bold'
            }
        },
        xAxis: [{
        	minRange: 100,
            type: 'datetime',
            labels: {
                millisecond: '%H:%M:%S.%L',
                second: '%H:%M:%S',
                minute: '%H:%M',
                hour: '%H:%M',
                day: '%e. %b',
                week: '%e. %b',
                month: '%b \'%y',
                year: '%Y'
            },                                  
            title: {
                text: 'Time',
                style: {
                    color: '#34495e',
                    fontWeight: 'bold'              
                }
            },
        }],
         tooltip: {
            crosshairs: true,
            xDateFormat: '%e %b,%Y @ %H:%M:%S.%L',
            shared: true,       
        },

        yAxis: [ { // Tertiary yAxis
            gridLineWidth: 0.5,
            title: {
                text: '<?= $paramValBT ?>',
                style: {
                    color: '#eb262d',
                    fontWeight: 'bold'  
                }
            },
            //min:0,
            labels: {
                format: '{value} <?= $paramValBU ?>',
                style: {
                    color: '#eb262d'
                },
                align : 'right',
                x : 0,
                y : 1
            }
        },{ // Secondary yAxis
            gridLineWidth: 0.5,
            title: {
                text: '<?= $paramValAT ?>',
                style: {
                    color: '#34495e',
                    fontWeight: 'bold'  
                }
            },
            //min:0,
            labels: {
                format: '{value} <?= $paramValAU ?>',
                style: {
                    color: '#34495e'
                },
                align : 'right',
                x : 0, 
                y : 1
            },
            opposite: true

        },{ // C yAxis
            gridLineWidth: 0.5,
            title: {
                text: '<?= $paramValCT ?>',
                style: {
                    color: '#16a085',
                    fontWeight: 'bold'  
                }
            },
            //min:0,
            labels: {
                format: '{value} <?= $paramValCU ?>',
                style: {
                    color: '#16a085'
                },
                align : 'left',
                x : 0,
                y : 1
            },
            opposite: true
        }
        ],
        
        series: [ {
            name: '<?= $paramValBT ?>',
            tooltip: {
                valueSuffix: ' <?=$paramValBU?>',
            },
            type: 'spline',
            color: '#eb262d',
            yAxis: 0,
            data: [<?= $datB ?>],
            /*marker: {
                enabled: false
            },*/
        },{
            name: '<?= $paramValAT ?>',
            tooltip: {
                valueSuffix: ' <?=$paramValAU?>',
            },
            type: 'spline',
            color: '#34495e',
            yAxis: 1,
            data: [<?= $datA ?>],
            // marker: {
            //     enabled: false
            // },
        },{
            name: '<?= $paramValCT ?>',
            tooltip: {
                     valueSuffix: ' <?=$paramValCU?>',
            },
            type: 'spline',
            color: '#16a085',
            yAxis: 2,
            data: [<?= $datC ?>],
           
        }]
    });

// button handler
$('#button').click(function () {
       var chart = $('#container').highcharts();
       chart.series[0].update({
          color: '#34495e'
        });
    });
});

</script>
<!--  <button id="button">Update</button> -->
<div id="container" class="col-md-12 col-sm-12 col-xs-12" style="height: 480px; margin: 0 auto"></div>
