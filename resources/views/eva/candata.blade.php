  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <?php  ?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

  <style type="text/css">
    .content .filter-option-inner-inner {
      position: relative;
      left: 8px;
    }
    .table-heading-font{
        font-size: 13px !important;
    }
.card .card-body {
     padding: 0 ; 
    position: relative; 
    }
    .datepicker table tr td.active:hover, .datepicker table tr td.active:hover:hover, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active:focus, .datepicker table tr td.active:hover:focus, .datepicker table tr td.active.disabled:focus, .datepicker table tr td.active.disabled:hover:focus, .datepicker table tr td.active:active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.active, .datepicker table tr td.active:hover.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:hover.active, .open .dropdown-toggle.datepicker table tr td.active, .open .dropdown-toggle.datepicker table tr td.active:hover, .open .dropdown-toggle.datepicker table tr td.active.disabled, .open .dropdown-toggle.datepicker table tr td.active.disabled:hover {
    color: #ffffff;
    background-color: #34495e;
    border-color: #34495e;
}
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    #datetimepicker12 .datepicker-inline {
    width: 335px !important;
    }
    table.table-condensed {
    width: 78%;
   }
/*    tr div {
      width: auto !important;
      z-index: auto !important;
    }*/
/*    .tab-pane tbody {
      display: block;
      max-height: 230px;
      overflow-y: scroll;
    }*/
    .ScrollStyle
    {
        max-height: 300px;
        overflow-y: scroll;
    }
    #map{
          background-color: aliceblue;
          margin-top: 30px !important;
    }
    .divTop{
      margin-top: 35px;
    }

.non-highlighted-cal-dates{
  background-color: #adb3b2;
}
.scrollable-menu {
    height: auto;
    max-height: 200px;
    overflow-x: hidden;
}
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">show_chart</i>
                  </div>
                  <h4 class="card-title">CAN Eva</h4>
                  <div class="row" id='vehicledetails'>
                      <!-- User and Vehicle Details to display from js -->
                  </div>
                  <br>
                </div>
                <div class="card-body">
                  <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>

                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="col-md-12" id="load" style="display: none;" align="center">
                  <img src="public/assets/img/loader.gif" style="width: 20%;">
                 </div>
                  <input type="hidden" name="devid" id="devid" value="">
                <div class="card-body">
                  <div class="col-md-12" style="text-align: center;">
                  <div class="row">
                  <div class="col-md-4">

                     <select class="selectpicker valueType1" data-style="select-with-transition"  data-container="body" title="Select Vehicle" data-size="7" name="selectvehicle" id="selectvehicle" >
                      <option class="dropdown-item" value="" selected>All Vehicles</option>
                      <?php foreach ($vehiclelist as $key => $vahicle) if(!empty($vahicle->vehicle)){{ ?>
                        <option class="dropdown-item" <?php if(count($vehiclelist) == 1){echo "selected";} ?> value="<?=$vahicle->vehicle?>"><?=$vahicle->vehicle?></option>
                      <?php }} ?>
                     </select>
                  </div>

                  <div class="col-md-8" align="right">

                     <button class="btn btn-primary btn-sm pull-right" id='goLive' style='display: none' onclick="goLive()">Go Live</button>
                    <button class="btn btn-primary btn-sm pull-right" id='stopLive' style='display: none' onclick="stopLive()">Stop Live</button>
                    <button class="btn btn-primary btn-sm" onclick="showTestDate()">reload</button>
                    <button onclick="generateReportCsv()" class="btn btn-primary btn-sm" id="btnSubmit1"><i class="material-icons">cloud_download</i></button>

                     <button class="btn btn-primary" id="loadbutton" type="button" disabled style="display: none;">
                      <div class="spin"></div> <p style="margin-left: 16%;margin-bottom: -10%; margin-top: -6%;"><i class="material-icons">cloud_download</i></p></button>
                  </div>
                </div>
              </div>
                  <div class="col-md-12" style="text-align: center;" >
                    </br>
                  <div class="row" >
                     
                      <div class="col-md-4">
                          <select class="selectpicker valueType1" data-style="select-with-transition"  data-container="body" title="Select Red Line" data-size="7" name="valueType1" id="valueType1" >
                            <option class="dropdown-item" value="">Select Red Line</option>
                            @if (!empty($valuePartList))
                            @foreach ($valuePartList as $vpl)
                            <option <?php if($vpl->id == '4'){ ?> selected <?php } ?>  value="{{$vpl->id}}">{{$vpl->title}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <div class="col-md-4">
                          <select class="selectpicker valueType1" data-style="select-with-transition" data-container="body" title="Select Blue Line" data-size="7" name="valueType2" id="valueType2" tabindex="-98">
                            <option value="">Select Blue Line</option>
                            @if (!empty($valuePartList))
                            @foreach ($valuePartList as $vpl)
                            <option  value="{{$vpl->id}}">{{$vpl->title}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                      <div class="col-md-4">
                          <select class="selectpicker valueType1" data-style="select-with-transition" data-container="body" title="Select Green Line" data-size="7" name="valueType3" id="valueType3" tabindex="-98">
                            <option value="">Select Green Line</option>
                            @if (!empty($valuePartList))
                            @foreach ($valuePartList as $vpl)
                            <option  value="{{$vpl->id}}">{{$vpl->title}}</option>
                            @endforeach
                            @endif
                          </select>
                      </div>
                                            
                   </div>
                  </div>
                  <div class="loader" id="loader" style="display: none;">Loading...</div>
                  <div class="col-md-12" id="load_data">

                  

                <!-- Process Eva -->


                 
       
                  </div>
                  
                </div>

                    </div>
                    <div class="fixed-plugin">
                  <div class="dropdown show-dropdown">
                    <a href="#" data-toggle="dropdown" aria-expanded="false">
                     <span class="material-icons" style="font-size: 50px;     color: white;">
                    directions_car
                    </span>
                    </a>
                    <ul class="dropdown-menu" id="device_select" x-placement="bottom-start" style="position: absolute; top: 41px; left: 8px; will-change: top, left;">
                      <div id="device_select">
                       <div id="datetimepicker12" style="width: 100%">
                          
                          </div>
                          <?php if($user_category == 1 || $user_category == 2 || $user_category == 3){ ?>
                      <div class="ScrollStyle tab-pane active" id="profile">
                                <input type="text" name="search" id="search" placeholder="Search Devices" class="form-control" style="background-color: #f9f5f5;">
                                    <table class="table" id="list_device">
                                     <tbody>
                                       <?php if($user_category == 1 || $user_category == 2 || $user_category == 3){ ?>
                                       <?php if(!empty($device_list)){ ?>
                                         <?php foreach($device_list as $key => $dl){ ?>
                                       <tr id="row_<?= $dl->device_id ?>" onclick="device_row('<?= $dl->device_id ?>');">

                                         <td><p style="font-size: 13px; margin-bottom: 0;"><a class="nav-link" href="javascript:void(0)"><?php echo $dl->device_name." (Device ID ".$dl->device_id.")
                                         "; ?>{{App\Helpers\Helper::getManufacturerById($dl->manu)}}</p></a></td>
                                       </tr>
                                       <?php } ?>
                                     <?php }else{ ?>
                                       <tr>
                                         <td>Empty</td>
                                       </tr>
                                       <?php } ?>
                                       <?php }else{ ?>
                                          <tr>

                                         <td>Sign contract for "What are conference organizers afraid of?"</td>

                                       </tr>

                                       <?php } ?>

                                     </tbody>
                                   </table>
                                  </div>
                                <?php } ?>
                                </div>
                    </ul>
                  </div>
                </div>
                <hr>
                
                    </div>

                  </div>
                  
                 <div class="col-md-12" id="load" style="display: none;" align="center">
                  <img src="public/assets/img/loader.gif" style="width: 20%;">
                 </div>
                  <input type="hidden" name="devid" id="devid" value="">

                
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>

    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')

<script type="text/javascript">
  var stoplive=0;
  var buttonlive=0;    
  var requests=[];    

function downloadURL(url) {
    var hiddenIFrameID = 'hiddenDownloader',
        iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;
};

function generateReportCsv(){
      $('#btnSubmit1').hide();
      $('#loadbutton').show();
    var date    = $('#datetimepicker12').datepicker('getFormattedDate');
    var did     = $('#devid').val();
    var dt      = date.split("/");
    var csvDate = dt[1]+""+dt[0]+""+dt[2];
    var vehicle = $('#selectvehicle').val(); 
    if(date != '' && did != ''){
        $.post('{{url('/')}}/downloadCsvEva', { date: date, did: did,vehicle:vehicle }).done(function (data) {
            // if(data == '1'){

              downloadURL('https://eva.enginecal.com/datacsv/'+did+'/'+csvDate+'.csv');
              $('#btnSubmit1').show();
              $('#loadbutton').hide();

          
        });
    }
}

$(document).ready(function() {

    // Search in device list
    // 
    $( "#search" ).keyup(function() {
        var did    = $('#devid').val();
        var search = $('#search').val();
        $.post('{{url('/')}}/getDeviceListcan', { search: search, did: did, type:'3' }).done(function (data) {
            $('#list_device').html(data);
        });
    });

     // Load drive dates in datePicker
    var today               = new Date();
    var today_formatted     = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+('0'+today.getDate()).slice(-2);
    var user_busy_days      = [<?= $drive_dates ?>];

    $('#datetimepicker12').datepicker({
          inline: true,
          sideBySide: true,
          beforeShowDay: function (date) {
            calender_date = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+('0'+date.getDate()).slice(-2);
            var search_index = $.inArray(calender_date, user_busy_days);
            if (search_index > -1) {
                return {classes: 'non-highlighted-cal-dates', tooltip: 'Drive Took'};
            }else{
                return {classes: 'highlighted-cal-dates', tooltip: 'No Drive'};
            }

          }
    }).on('click', showTestDate);

    $('#datetimepicker12').datepicker('setDate', '<?=$today?>');

    // Load Graph By Page Load
     $('#load_data').hide();
   $('#loader').show();
    var red    = '4';
    var blue   = '';
    var yellow = '';
    var green  = '';
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
     var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = '<?=$today?>';
    getUservehicleDetails('<?=$did ?>');
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: '<?=$did ?>', 'date': today, 'state': '2', 'g1': '', 'g2': '4', 'g3': '', 'g4': '' }).done(function (data) {
       $('#loader').hide();
          $('#load_data').show();
              $('#load_data').html(data);

          document.getElementById("goLive").style.display = "block";
          document.getElementById("stopLive").style.display = "none";
        
    }));
    var did='{{$did}}';
    

     $.post('{{url('/')}}/getportdata', {  did:did }).done(function (data) {
      $('#valueType1').html('<option value = "" >Select Red Line</option>').selectpicker('refresh'); 
      $('#valueType2').html('<option value = "" >Select Blue Line</option>').selectpicker('refresh');
      $('#valueType3').html('<option value = "" >Select Green Line</option>').selectpicker('refresh');
      $('#valueType1').append(data).selectpicker('refresh'); 
      $('#valueType2').append(data).selectpicker('refresh');
      $('#valueType3').append(data).selectpicker('refresh');
      $('#valueType1 option[value="4"]').attr("selected", "selected");
      $('.selectpicker').selectpicker('refresh')
    });
     

});    

// Load Graph By Device
function device_row(did){
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
    $('#title').text('DeviceID : '+did);
   $('#load_data').hide();
   $('#loader').show();
   getUservehicleDetails(did);
   $.post('{{url('/')}}/getportdata', {  did: did }).done(function (data) {
      $('#valueType1').html(data).selectpicker('refresh');
      $('#valueType2').html(data).selectpicker('refresh');
      $('#valueType3').html(data).selectpicker('refresh');
      $('#valueType1 option[value="4"]').attr("selected", "selected");
      $('.selectpicker').selectpicker('refresh')
    });


    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    $('tr').css({ 'background-color' : '#fff'});
    $('#row_'+did).css({ 'background-color' : 'aliceblue'});
    $('#devid').val(did);


    var red    = $("#valueType1 option:selected").val();
    if(red == ''){ red = '4'; }
    var blue   = $("#valueType2 option:selected").val();
    var yellow = $("#valueType3 option:selected").val();
    var green  = $("#valueType4 option:selected").val();
    $.get('{{url('/')}}/getVehicle', { did: did, 'date': date}).done(function (data) {
        $('#selectvehicle').html(data);
        $('.selectpicker').selectpicker('refresh');
        // $('#selectvehicle').Refresh();
    });
    requests.push($.post('{{url('/')}}/getEvaGraph', {  did: did, date: date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow, 'g4': green,'dd_date': '1' }).done(function (data) {
       $('#loader').hide();
          $('#load_data').show();
          $('#load_data').html(data);
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            today = mm + '/' + dd + '/' + yyyy;
            if(date == today){
          document.getElementById("goLive").style.display = "block";
          document.getElementById("stopLive").style.display = "none";
        }
          $('#datetimepicker12').datepicker('remove');
          var today           = $('#selected_date').val();
          // var today_formatted     = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+('0'+today.getDate()).slice(-2);
          var service_dates       = $('#service_date').val();
          var user_busy_days      = [service_dates];


          $('#datetimepicker12').datepicker({
                    inline: true,
                    sideBySide: true,
                    beforeShowDay: function (date) {
                      calender_date = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+('0'+date.getDate()).slice(-2);
                      var search_index = $.inArray(calender_date, user_busy_days);
                      var service_dates       = $('#service_date').val();
                      var n = service_dates.includes(""+calender_date);

                      if (n == true) {
                          return {classes: 'non-highlighted-cal-dates', tooltip: 'Drive Took'};
                      }else{
                          return {classes: 'highlighted-cal-dates', tooltip: 'No Drive'};
                      }

                    }
              });
          $('#datetimepicker12').datepicker('setDate', today);

    }));
}

//stoplive
function stopLive(){
    clearInterval(showTestDate);
     stoplive=0;
    document.getElementById("stopLive").style.display = "none";
    document.getElementById("goLive").style.display = "block";

  }
  function goLive(){
    clearInterval(showTestDate);
    stoplive=1;
    buttonlive=1;
    document.getElementById("stopLive").style.display = "block";
    document.getElementById("goLive").style.display = "none";
    setTimeout(showTestDate, 5000);

  }

// Load Graph By DatePicker
// $('.datepicker-days').click


function showTestDate(){
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
   $('#load_data').hide();
   $('#loader').show();
   console.log($('#datetimepicker12').datepicker());
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    var did    = $('#devid').val();
    var red    = $("#valueType1 option:selected").val();
    if(red == ''){ red = '4'; }
    var blue   = $("#valueType2 option:selected").val();
    var yellow = $("#valueType3 option:selected").val();
    var green  = $("#valueType4 option:selected").val();
    $.get('{{url('/')}}/getVehicle', { did: did, 'date': date}).done(function (data) {
        $('#selectvehicle').html(data);
        $('.selectpicker').selectpicker('refresh');
        // $('#selectvehicle').Refresh();
    }); 
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: did, 'date': date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow, 'g4': green }).done(function (data) {
       $('#loader').hide();
          $('#load_data').show();
          $('#load_data').html(data);
    }));
    // Live chart Logic
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = mm + '/' + dd + '/' + yyyy;
    if(date == today && stoplive ==1){
      setTimeout(showTestDate, 5000);
     
    }else {
      //alert
          if(date != today){
          document.getElementById("goLive").style.display = "none";
          document.getElementById("stopLive").style.display = "none";
          }else{
                   if(buttonlive == 0){
               document.getElementById("goLive").style.display = "block";
            }
          }
    
      clearInterval(showTestDate);
    }
    //alert(today+date);
    // setTimeout(showTestDate, 15000);
}

// Load By Red Line

$( ".valueType1" ).change(function() {
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
   $('#load_data').hide();
   $('#loader').show();
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    var did    = $('#devid').val();
    var red    = $(this).children("option:selected").val();
    var vehicle = $('#selectvehicle').val(); 
    var red    = $("#valueType1 option:selected").val();
    var blue   = $("#valueType2 option:selected").val();
    var yellow = $("#valueType3 option:selected").val();
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: did, 'date': date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow, vehicle:vehicle }).done(function (data) {
       $('#loader').hide();
          $('#load_data').show();
          $('#load_data').html(data);
    }));
});

$( ".valueType2" ).change(function() {
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
   $('#load_data').hide();
   $('#loader').show();
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    var did    = $('#devid').val();
    var red    = $(".valueType1 option:selected").val();
    var blue   = $(this).children("option:selected").val();
    var yellow = $(".valueType3 option:selected").val();
    var green  = $(".valueType4 option:selected").val();
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: did, 'date': date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow, 'g4': yellow }).done(function (data) {
       $('#loader').hide();
          $('#load_data').show();
          $('#load_data').html(data);
    }));
});

$( ".valueType3" ).change(function() {
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
   $('#load_data').hide();
   $('#loader').show();
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    var did    = $('#devid').val();
    var red    = $(".valueType1 option:selected").val();
    var blue   = $(".valueType2 option:selected").val();
    var yellow = $(this).children("option:selected").val();
    var green  = $(".valueType4 option:selected").val();
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: did, 'date': date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow, 'g4': green }).done(function (data) {
       $('#loader').hide();
          $('#load_data').show();
          $('#load_data').html(data);
    }));
});

$( ".valueType4" ).change(function() {
  for(var i = 0; i < requests.length; i++)
  {
    requests[i].abort();
  }
   $('#load_data').hide();
   $('#loader').show();
    var date   = $('#datetimepicker12').datepicker('getFormattedDate');
    var did    = $('#devid').val();
    var red    = $(".valueType1 option:selected").val();
    var blue   = $(".valueType2 option:selected").val();
    var yellow = $(".valueType3 option:selected").val();
    var green  = $(this).children("option:selected").val();
    requests.push($.post('{{url('/')}}/getEvaGraph', { did: did, 'date': date, 'state': '2', 'g1': blue, 'g2': red, 'g3': yellow, 'g4': green }).done(function (data) {
       $('#loader').hide();
          $('#load_data').show();
          $('#load_data').html(data);
    }));
});


$('#devid').val('<?= $did ?>');

  </script>
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> 