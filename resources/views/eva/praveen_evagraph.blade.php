<!-- Chartist JS -->
<script src="{{url('/')}}/public/assets/js/jquery.canvasjs.min.js"></script>
<script src="{{url('/')}}/public/assets/js/canvasjs.min.js"></script>

<script type="text/javascript">
<?php
    $paramValBT = !empty($paramValB->title) ? $paramValB->title:"";
    $paramValBU = !empty($paramValB->unit) ? $paramValB->unit:"";
    
    $paramValAT = !empty($paramValA->title) ? $paramValA->title:"";
    $paramValAU = !empty($paramValA->unit) ? $paramValA->unit:"";
    
    $dataPointsA = array();
    foreach($datAA as $x => $x_value) {
        //        echo "Key=" . $x . ", Value=" . $x_value;
        //        echo "<br>";
        $new_str = str_replace("[","",$x_value);
        $new_str1 = str_replace("]","",$new_str);
        $str_arr = explode (",", $new_str1);
        $dataPointsA[$x] = array('x' => $str_arr[0],
                                   'y' => $str_arr[1]);
    }

    ?>

$(function () {
  var chart = new CanvasJS.Chart("chartContainer", {
                                 zoomEnabled: true,
                                 animationEnabled: true,
                                 title: {
                                         text: "<?= $paramValAT ?><?php if($paramValBT){?>\tVS\t<?= $paramValBT ?><?php } ?>",
                                         style: {
                                            color: '#34495e',
                                            }
                                        },
                                 toolTip: {
                                         shared: true,
                                         content: function (e) {
                                             var body;
                                             var head;
                                             head = "<span style = 'color:DodgerBlue; '><strong>" + (e.entries[0].dataPoint.x) + " sec</strong></span><br/>";
                                 
                                             body = "<span style= 'color:" + e.entries[0].dataSeries.color + "'> " + e.entries[0].dataSeries.name + "</span>: <strong>" + e.entries[0].dataPoint.y + "</strong>  m/s<br/> <span style= 'color:" + e.entries[1].dataSeries.color + "'> " + e.entries[1].dataSeries.name + "</span>: <strong>" + e.entries[1].dataPoint.y + "</strong>  m";
                                 
                                             return (head.concat(body));
                                         }
                                 },
                                 axisY: {
                                        title:'<?= $paramValAT ?>',
                                        titleFontColor: '#eb262d',
                                        titleFontWeight: "bold",
                                         includeZero: false,
                                         suffix: '<?= $paramValAU ?>',
                                         lineColor: '#eb262d'
                                 },
                                 axisY2: {
                                         title:'<?= $paramValBT ?>',
                                         titleFontColor: '#34495e',
                                         titleFontWeight: "bold",
                                         includeZero: false,
                                         suffix: '<?= $paramValBU ?>',
                                         lineColor: '#34495e'
                                 },
                                 axisX: {
                                         title: "Time",
                                         lineColor: "#34495e",
                                         labelFormatter: function(e){
                                         var ms = e.value;
                                         var secs = Math.floor(ms / 1000);
                                         var hours = Math.floor(secs / (60 * 60));
                                         var divisor_for_minutes = secs % (60 * 60);
                                         var minutes = Math.floor(divisor_for_minutes / 60);
                                         var divisor_for_seconds = divisor_for_minutes % 60;
                                         var seconds = Math.ceil(divisor_for_seconds);
                                         var timeStr = "";
                                         if (hours > 0) timeStr += hours + ":";
                                         if (hours > 0 || minutes > 0) timeStr += minutes + ":";
                                         timeStr += seconds + "." + (ms % 1000);
                                         return timeStr;
                                 },
//                                            labelFormatter: function (e) {
//                                            return CanvasJS.formatDate(e.value, “hh:mm TT”);
//                                          },
                                         //intervalType: "hour",
                                         //valueFormatString: "hh:mm TT",
                                         minRange:<?=$min?>,
                                         maxRange:<?=$max?>
                                 },
                                 data: [
                                        {
                                             type: "spline",
                                             name: '<?= $paramValAT ?>',
                                             dataPoints: <?php echo json_encode($dataPointsA, JSON_NUMERIC_CHECK); ?>
                                         },
                                         {
                                             type: "spline",
                                             axisYType: "secondary",
                                             name: '<?= $paramValBT ?>',
                                             dataPoints: <?php echo json_encode($dataPointsA, JSON_NUMERIC_CHECK); ?>
                                         }
                                 ]
                });
  
  chart.render();
  });
</script>

<div id="chartContainer" class="col-md-12 col-sm-12 col-xs-12" style="height: 480px; margin: 0 auto"></div>

