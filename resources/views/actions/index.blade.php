  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }

    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    #datetimepicker12 .datepicker-inline {
    width: 335px !important;
    }
    table.table-condensed {
    width: 100%;
   }
/*    tr div {
      width: auto !important;
      z-index: auto !important;
    }*/
/*    .tab-pane tbody {
      display: block;
      max-height: 230px;
      overflow-y: scroll;
    }*/
    .ScrollStyle
    {
        max-height: 300px;
        overflow-y: scroll;
    }
    #map{
          background-color: aliceblue;
          margin-top: 30px !important;
    }
    .divTop{
      margin-top: 35px;
    }

.non-highlighted-cal-dates{
  background-color: gainsboro;
}
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              
            </div>
         
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">undo</i>
                  </div>
                  <h4 class="card-title">Actions</h4>
                </div>
          
            
                
                <div class="card-body">
                  <div class="col-md-12">
             
              <div class="card">
                <!-- Start -->

                <div class="card-body" id="load_data">


                </div>

                <!-- END -->
              </div>
            </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
  
    $.post('{{url('/')}}/getActionLoad').done(function (data) {
              $('#load_data').html(data);
              $('#type').selectpicker('refresh');
              $('#select-from').selectpicker('refresh');

                  $('#datatables').DataTable({
                    "pagingType": "full_numbers",
                    "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
                  });

                    $('#datatables1').DataTable({
                      "pagingType": "full_numbers",
                      "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                      ],
                      responsive: true,
                      language: {
                        search: "_INPUT_",
                        searchPlaceholder: "Search records",
                      }
                    });
    });


});    

    function validateAssistance(){
      $('#cli_message').html('');
      var topic = $('#topics').val();
      var desc  = $('#desc').val();
      if(topic == ''){
          $('#cli_message').html('Fields Required.');
          return false;
      }else if(desc == ''){
          $('#cli_message').html('Fields Required.');
          return false;
      }else{
        $('#cli_message').html('in process...');
          return true;
      } 
    }
  </script>