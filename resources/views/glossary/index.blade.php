  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
    tfoot {
     display: table-header-group !important;
     background-color: blanchedalmond;
    }
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Status_tp{
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    .tox-statusbar__branding{
      display: none;
    }
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">
                    <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>

          <div class="col-md-12" align="right">
          </div>
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  <h4 class="card-title">Glossary</h4>
                </div>
                <div class="card-body">
                  <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>
                  <form name="form1" id="form1" action="{{url('/')}}/update-glossary" method="post">
                  <input type="hidden" name="form_token" id="form_token" value="{{$form_token}}">
                   @if (!empty($glossary_list))
                        @foreach ($glossary_list as $gloss1)
                  <textarea class="form-control mceEditor" style="height:40px;" rows="20" id="msg" name="msg" >{{$gloss1->glossary}}
                  </textarea>
                  @endforeach
                  @endif
                  @if (session('userdata')['ucategory'] != '4')
                  <button class="btn btn-primary" type="submit" name="save" id="save">Save</button>
                  @endif
                  </form>
           
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
  @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif
 <script>
    $(document).ready(function() {
       // var editor_data = tinyMCE.get('msg').getContent(); 
       // $('#msg').val(editor_data);
       $.ajax({
            type:'POST',
            url:  '{{url('/')}}/glossary-list',
           success: function(data) {
            var activeEditor = tinyMCE.get('msg');
            activeEditor.setContent(data);
        }
    });

    });
  </script>
     <script type="text/javascript" src="{{url('/')}}/public/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/public/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script type="text/javascript">
tinymce.init({
    selector: "#msg",
    plugins: "wordcount",

});

</script>
<script type="text/javascript">
 
</script>