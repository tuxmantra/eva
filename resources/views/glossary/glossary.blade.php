  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
    tfoot {
     display: table-header-group !important;
     background-color: blanchedalmond;
    }
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Status_tp{
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">
                    <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>

          <div class="col-md-12" align="right">
            <a href="{{url('/')}}/add-value" title="Add Data Value" class="btn btn-primary btn-sm">Add</a>
            <button class="btn btn-primary btn-sm" onclick="goBack()">Go Back</button>
          </div>
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  <h4 class="card-title">Value Part View</h4>
                </div>
                <div class="card-body">
                  <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>
                  <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                       
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Title</th>
                          <th class="table-heading-font">Device1</th>
                          <th class="table-heading-font">Device2</th>
                        
                          <th class="table-heading-font">Decimal</th>
                          <th class="table-heading-font">Unit</th>
                          <th class="table-heading-font">Type</th>
                          <th class="table-heading-font">Status</th>
                          <th class="disabled-sorting text-right table-heading-font">Actions</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                        
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Title</th>
                          <th class="table-heading-font">Device1</th>
                          <th class="table-heading-font">Device2</th>
                         
                          <th class="table-heading-font">Decimal</th>
                          <th class="table-heading-font">Unit</th>
                          <th class="table-heading-font">Type</th>
                          <th class="table-heading-font">Status</th>
                          <th class="disabled-sorting text-right">Actions</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @if (!empty($value_list))
                        <?php $id = 1; ?>
                        @foreach ($value_list as $value1)  
                        <tr>
                         
                          <td class="table-heading-font">{{$id}}</td>
                          <td style="background-color: antiquewhite;" class="table-heading-font">{{$value1->title}}</td>
                          <td style="background-color: beige;" class="table-heading-font">{{$value1->device1}}</td>
                          <td style="background-color: floralwhite;" class="table-heading-font">{{$value1->device2}}</td>
                        
                          <td class="table-heading-font">{{$value1->decim}}</td>
                          <td class="table-heading-font">{{$value1->unit}}</td>
                          <td class="table-heading-font">{{App\Helpers\Helper::getType($value1->type)}}</td>
                          <td style="background-color: cornsilk;" class="table-heading-font">{{App\Helpers\Helper::getStatus($value1->status)}}</td>
                           <td class="text-right">
                         <a href="<?= url('/')."/edit-value/"; ?>{{App\Helpers\Helper::crypt(1,$value1->id)}}" class="btn btn-link btn-info btn-just-icon like"><i class="material-icons">edit</i></a>
                          </td>
                         
                         
                        </tr>
                        <?php $id++; ?>  
                        @endforeach
                        @else
                           <tr><td colspan="9" align="center">No User Exist.</td></tr>
                        @endif
                       
                       
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
 <script>
    $(document).ready(function() {

      
      $('#datatables').DataTable({ 
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

       var table = $('#datatables').DataTable();

      $('#datatables tfoot th').each( function () {
        var title = $(this).text();
        if(title == "S.No"){
           title = "Serial";
        }
        if(title == "Type"){
            var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                .appendTo( $(this).empty() )
                .on( 'change', function () {

                    var val = $(this).val();

                    table.column( 6 ) //Only the first column
                        .search( val ? '^'+$(this).val()+'$' : val, true, false )
                        .draw();
                } );

                table.column( 6 ).data().unique().sort().each( function ( d, j ) {
                if(d != '-'){
                  select.append( '<option value="'+d+'">'+d+'</option>' );
                }else{
                  select.append( '<option value="'+d+'">Nil</option>' );
                }
            } );
        }else{
           $(this).html( '<input class="'+title+'_tp" type="text" placeholder=" '+title+'" />' );
        }
      });

      table.columns([1,2,3,4,5,6]).every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
          });
      });


     


    });
  </script>