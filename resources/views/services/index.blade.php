  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


  <style type="text/css">

     .dataTable > thead > tr > th[class*=sort]:after{
    display:none;
}
.dataTable > thead > tr > th[class*=sort]:before{
    display:none;
}
    .table-heading-font{
        font-size: 13px !important;
    }

    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    #datetimepicker12 .datepicker-inline {
    width: 335px !important;
    }
    table.table-condensed {
    width: 79%;
   }
   .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 293px;
}
/*    tr div {
      width: auto !important;
      z-index: auto !important;
    }*/
/*    .tab-pane tbody {
      display: block;
      max-height: 230px;
      overflow-y: scroll;
    }*/
    .ScrollStyle
    {
        max-height: 300px;
        overflow-y: scroll;
    }
    #map{
          background-color: aliceblue;
          margin-top: 30px !important;
    }
    .divTop{
      margin-top: 35px;
    }

.non-highlighted-cal-dates{
  background-color: gainsboro;
}
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-md-12" align="center">
             
             
            </div>
          
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">rv_hookup</i>
                  </div>
                  <h4 class="card-title">Services List</h4>
                  <div class="row" id='vehicledetails'>
                      <!-- User and Vehicle Details to display from js -->
                  </div>
                  <br>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="card-body" id="load_data">


                </div>

                    </div>
                     <div class="fixed-plugin">
                  <div class="dropdown show-dropdown">
                    <a href="#" data-toggle="dropdown" aria-expanded="false">
                     <span class="material-icons" style="font-size: 50px;     color: white;">
                    directions_car
                    </span>
                    </a>
                    <ul class="dropdown-menu" id="device_select" x-placement="bottom-start" style="position: absolute; top: 41px; left: 8px; will-change: top, left;">
                      <div id="device_select">
                       <!-- <div id="datetimepicker12" style="width: 100%">
                          
                          </div> -->
                      <div class="ScrollStyle tab-pane active" id="profile">
                                <input type="text" name="search" id="search" placeholder="Search Devices" class="form-control" style="background-color: #f9f5f5;">
                                    <table class="table" id="list_device">
                                     <tbody>
                                       <?php if($user_category == 1 || $user_category == 2 || $user_category == 3){ ?>
                                       <?php if(!empty($device_list)){ ?>
                                         <?php foreach($device_list as $key => $dl){ ?>
                                       <tr id="row_<?= $dl->device_id ?>" onclick="device_row('<?= $dl->device_id ?>');">

                                         <td><p style="font-size: 13px; margin-bottom: 0;"><a class="nav-link" href="javascript:void(0)"><?php echo $dl->device_name." (Device ID ".$dl->device_id.")
                                         "; ?>{{App\Helpers\Helper::getManufacturerById($dl->manu)}}</p></a></td>
                                       </tr>
                                       <?php } ?>
                                     <?php }else{ ?>
                                       <tr>
                                         <td>Empty</td>
                                       </tr>
                                       <?php } ?>
                                       <?php }else{ ?>
                                          <tr>

                                         <td>Sign contract for "What are conference organizers afraid of?"</td>

                                       </tr>

                                       <?php } ?>

                                     </tbody>
                                   </table>
                                  </div>
                                </div>
                    </ul>
                  </div>
                </div>
                <hr>
                    </div>
                  </div>

                  
                 <div class="col-md-12" id="load" style="display: none;" align="center">
                  <img src="public/assets/img/loader.gif" style="width: 20%;">
                 </div>
                <div class="card-body">
                  <div class="col-md-12">
             
              <div class="card">
                <!-- Start -->

                
                <!-- END -->
              </div>
            </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>

    </div>
  </div>
 
  <!--   Core JS Files   -->
 @include('default.footer')
  @if (Session::has('message'))
<script>
  Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '{{session('message')}}',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
</script>
 @endif
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

function addServicesValidate(){
  var service_date  = $('#sdate').val();
  var type          = $('#type').val();
  var details       = $('#details').val();
  var provider_name = $('#provider_name').val();
  var kms_read      = $('#kms_read').val();
  if(service_date == '' || type == '' || kms_read == ''){
  Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   'Fields required.',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })

        return false;
  }else{
     return true;
  }
}

    $(document).on('click','#btn-add', function() {

        $('#select-from option:selected').each( function() {

               /* $('#select-to').append("<option value='"+$(this).val()+"' selected='selected'>"+$(this).text()+"</option>");*/          /*$('#select-to').append("<div style='float:left;'><input style='padding: 0; width: 25px;' type='checkbox' value='"+$(this).val()+"' name='toval[]' checked='checked' />"+$(this).text()+"</div>");*/         
               $('#select-to').append("<div style='float:left;'><input style='padding: 0; width: 25px;' type='checkbox' value='"+$(this).val()+"' id='idv"+$(this).val()+"' name='toval[]' checked='checked' onchange='selSub("+$(this).val()+")'/>"+$(this).text()+" <input style='padding: 0; width: 113px;' placeholder='Enter Description' type='text'  name='idvName"+$(this).val()+"' id='idvName"+$(this).val()+"'  /></div>");
                  $(this).remove();   
                  var val = $(this).val();         
                  /* selSub() function for sub lists*/            

                    var chk = "idv"+val;
                    var v=document.getElementById(chk);
                    if(v.checked==false)
                    { 
                      var subVal = "#subVal"+val;
                      $(subVal).html('');
                    }
                    else
                    {

                      var subVal = "#subVal"+val;
                    }
        });
    });

$(document).ready(function() {
    
    $( "#search" ).keyup(function() {
        var did    = $('#devid').val();
        var search = $('#search').val();
        $.post('{{url('/')}}/getDeviceList', { search: search, did: did }).done(function (data) {
            $('#list_device').html(data);
        });
    });
/*  var today               = new Date();
    var today_formatted     = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+('0'+today.getDate()).slice(-2);
    var user_busy_days      = [<?= $service_dates ?>];

    $('#datetimepicker12').datepicker({
          inline: true,
          sideBySide: true,
          beforeShowDay: function (date) {
            calender_date = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+('0'+date.getDate()).slice(-2);
            var search_index = $.inArray(calender_date, user_busy_days);
            if (search_index > -1) {
                return {classes: 'non-highlighted-cal-dates', tooltip: 'Drive Took'};
            }else{
                return {classes: 'highlighted-cal-dates', tooltip: 'No Drive'};
            }

          }
    }).on('click', showTestDate);

    $('#datetimepicker12').datepicker('setDate', 'now');
    $( "#search" ).keyup(function() {
            var search = $('#search').val();
            $.post('{{url('/')}}/getDeviceList', { search: search }).done(function (data) {
                $('#list_device').html(data);
            });
    }); 
    var date = $('#datetimepicker12').datepicker('getFormattedDate');*/
    getUservehicleDetails(<?= $did ?>);
    $.post('{{url('/')}}/getServiceLoad', { did: '<?= $did ?>'/*, 'sdate': date*/ }).done(function (data) {
              $('#load_data').html(data);
              /*var service_dates = $('#service_date').val();*/

              $('#type').selectpicker('refresh');
              $('#select-from').selectpicker('refresh');

                  $('#datatables').DataTable({
                    "pagingType": "full_numbers",
                    "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
                  });

                    $('#datatables1').DataTable({
                      "pagingType": "full_numbers",
                      "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                      ],
                      responsive: true,
                      language: {
                        search: "_INPUT_",
                        searchPlaceholder: "Search records",
                      }
                    });
    });


});    

    function device_row(did){
       $('#title').text('DeviceID : '+did);
        // var date = $('#datetimepicker12').datepicker('getFormattedDate');
        $('tr').css({ 'background-color' : '#fff'});
        $('#row_'+did).css({ 'background-color' : 'aliceblue'});
        $('#devid').val(did);
        getUservehicleDetails(did);
        $.post('{{url('/')}}/getServiceLoad', { did: did }).done(function (data) {
              $('#load_data').html(data);

              $('#type').selectpicker('refresh');
              $('#select-from').selectpicker('refresh');

                  $('#datatables').DataTable({
                    "pagingType": "full_numbers",
                    "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
                  });

                    $('#datatables1').DataTable({
                      "pagingType": "full_numbers",
                      "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                      ],
                      responsive: true,
                      language: {
                        search: "_INPUT_",
                        searchPlaceholder: "Search records",
                      }
                    });
        });
    }

    function showTestDate(){
         var date = $('#datetimepicker12').datepicker('getFormattedDate');
         var did   = $('#devid').val();

         $.post('{{url('/')}}/getServiceLoad', { did: did, 'sdate': date }).done(function (data) {
              $('#load_data').html(data);
              var service_dates = $('#service_date').val();

              $('#type').selectpicker('refresh');
              $('#select-from').selectpicker('refresh');

                  $('#datatables').DataTable({
                    "pagingType": "full_numbers",
                    "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
                  });

                    $('#datatables1').DataTable({
                      "pagingType": "full_numbers",
                      "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                      ],
                      responsive: true,
                      language: {
                        search: "_INPUT_",
                        searchPlaceholder: "Search records",
                      }
                    });
        });

    }

    $('#devid').val('<?= $did ?>');
  </script>