<style type="text/css">
  .dark-theme {
  background-color: #181C30; }
/*   .dark-theme.fixed-sn .double-nav,
  .dark-theme.fixed-sn main,
  .dark-theme.fixed-sn footer {
    padding-left: 15rem; } */
  .dark-theme.fixed-sn main {
    margin-left: 1rem;
    margin-right: 1rem; }
  @media (max-width: 1289px) {
    .dark-theme.fixed-sn .double-nav,
    .dark-theme.fixed-sn main,
    .dark-theme.fixed-sn footer {
      padding-left: 0; } }
  @media (min-width: 1289px) {
    .dark-theme.fixed-sn .double-nav .button-collapse {
      display: none; } }
  @media (max-width: 1289px) {
    .dark-theme.fixed-sn .double-nav .button-collapse {
      display: block;
      position: relative;
      font-size: 1.4rem;
      margin-right: 10px;
      margin-left: 10px; } }
  .dark-theme header {
    border-top: 2px solid #1d8cf8; }
    .dark-theme header .navbar {
      box-shadow: none;
      background-color: #181C30; }
  .dark-theme .card {
    background-color: #27293d; }
    .dark-theme .card .card-header {
      background-color: #27293d; }
    .dark-theme .card.card-table .card-header {
      border-bottom: none; }
  .dark-theme .btn.btn-sm {
    padding: .4rem 1.5rem .25rem;
    font-size: .75rem;
    font-weight: 500; }
  .dark-theme i.icon {
    font-size: 1.1rem; }
    .dark-theme i.icon.icon-blue {
      color: #36A2EB; }
.cell-small {
  width: 1rem;
}
.dark-skin .side-nav .sidenav-bg:after, .dark-skin .side-nav .sidenav-bg.mask-strong:after {
    background: rgba(29, 140, 248, 0.8); }
.dark-skin .side-nav .collapsible li .collapsible-header.active {
        background-color: rgba(24, 28, 48, 0.8); }
      .dark-skin .side-nav .collapsible li .collapsible-header:hover {
        background-color: rgba(24, 28, 48, 0.8); }
.dark-skin .side-nav .collapsible li a:not(.collapsible-header):hover, .dark-skin .side-nav .collapsible li a:not(.collapsible-header).active, .dark-skin .side-nav .collapsible li a:not(.collapsible-header):active {
        color: #67d5ff !important; }

  
</style>
<body class="fixed-sn dark-skin dark-theme">

  <!-- Double navigation -->
  <header>

    <!-- Sidebar navigation -->
    <div id="slide-out" class="side-nav fixed wide sn-bg-4">
      <ul class="custom-scrollbar">
        <!-- Logo -->
        <li>
          <div class="logo-wrapper sn-ad-avatar-wrapper">
            <a href="#"><img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg"
                class="rounded-circle"><span>Anna Deynah</span></a>
          </div>
        </li>
        <!-- Logo -->
        <!-- Side navigation links -->
        <li>
          <ul class="collapsible collapsible-accordion">
            <li><a class="collapsible-header waves-effect arrow-r active"><i
                  class="sv-slim-icon fas fa-chevron-right"></i> Submit blog<i
                  class="fas fa-angle-down rotate-icon"></i></a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="#" class="waves-effect active">
                      <span class="sv-slim"> SL </span>
                      <span class="sv-normal">Submit listing</span></a>
                  </li>
                  <li><a href="#" class="waves-effect">
                      <span class="sv-slim"> RF </span>
                      <span class="sv-normal">Registration form</span></a>
                  </li>
                </ul>
              </div>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="sv-slim-icon far fa-hand-point-right"></i>
                Instruction<i class="fas fa-angle-down rotate-icon"></i></a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="#" class="waves-effect">
                      <span class="sv-slim"> FB </span>
                      <span class="sv-normal">For bloggers</span></a>
                  </li>
                  <li><a href="#" class="waves-effect">
                      <span class="sv-slim"> FA </span>
                      <span class="sv-normal">For authors</span></a>
                  </li>
                </ul>
              </div>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="sv-slim-icon fas fa-eye"></i> About<i
                  class="fas fa-angle-down rotate-icon"></i></a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="#" class="waves-effect">
                      <span class="sv-slim"> I </span>
                      <span class="sv-normal">Introduction</span></a>
                  </li>
                  <li><a href="#" class="waves-effect">
                      <span class="sv-slim"> MM </span>
                      <span class="sv-normal">Monthly meetings</span></a>
                  </li>
                </ul>
              </div>
            </li>
            <li><a class="collapsible-header waves-effect arrow-r"><i class="sv-slim-icon far fa-envelope"></i> Contact
                me<i class="fas fa-angle-down rotate-icon"></i></a>
              <div class="collapsible-body">
                <ul>
                  <li><a href="#" class="waves-effect">
                      <span class="sv-slim"> F </span>
                      <span class="sv-normal">FAQ</span></a>
                  </li>
                  <li><a href="#" class="waves-effect">
                      <span class="sv-slim"> W </span>
                      <span class="sv-normal">Write a message</span></a>
                  </li>
                </ul>
              </div>
            </li>
            <li><a id="toggle" class="waves-effect"><i class="sv-slim-icon fas fa-angle-double-left"></i>Minimize
                menu</a>
            </li>
          </ul>
        </li>
        <!-- Side navigation links -->
      </ul>
      <div class="sidenav-bg rgba-blue-strong"></div>
    </div>
    <!-- Sidebar navigation -->

    <!-- Navbar -->
    <nav class="navbar navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
      <!-- SideNav slide-out button -->
      <div class="float-left">
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i></a>
      </div>
      <!-- Breadcrumb -->
      <div class="breadcrumb-dn mr-auto text-white">
        <p>Material Design for Bootstrap</p>
      </div>
      <ul class="nav navbar-nav nav-flex-icons ml-auto text-white">
        <li class="nav-item">
          <a class="nav-link"><i class="fas fa-envelope"></i> <span
              class="clearfix d-none d-sm-inline-block">Contact</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link"><i class="fas fa-comments"></i> <span
              class="clearfix d-none d-sm-inline-block">Support</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link"><i class="fas fa-user"></i> <span
              class="clearfix d-none d-sm-inline-block">Account</span></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            Dropdown
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
      </ul>
    </nav>
    <!-- Navbar -->

  </header>
  <!-- Double navigation -->

  <!-- Main layout -->
  <main class="pt-3">

    <div class="container-fluid">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-12 mb-4">

          <div class="card">
            <div class="card-body pl-0 pr-2 pb-0">
              <div class="float-left white-text pl-4">
                <p class="font-weight-light mb-1 mt-n1 text-white-50 font-small">Total Shipments</p>
                <h2 class="font-weight-light">Performance</h2>
              </div>
              <div class="btn-group float-right pr-3" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-sm btn-primary" id="accountsData">Accounts</button>
                <button type="button" class="btn btn-sm btn-primary" id="purchasesData">Purchases</button>
                <button type="button" class="btn btn-sm btn-primary" id="sessionsData">Sessions</button>
              </div>
              <canvas id="lineChart" height="90"></canvas>
            </div>
          </div>

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-lg-4 col-md-12 mb-4">

          <div class="card">
            <div class="card-body px-2">
              <div class="float-left white-text pl-4">
                <p class="font-weight-light mb-1 mt-n1 text-white-50 font-small">Total Shipments</p>
                <p><i class="far fa-bell pr-2 align-text-top mt-n1 icon icon-blue"></i><span
                    class="font-weight-light h3">354 780</span></p>
              </div>
              <canvas id="horizontalBar" height="280"></canvas>
            </div>
          </div>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-4 col-md-6 mb-4">

          <div class="card">
            <div class="card-body px-2">
              <div class="float-left white-text pl-4">
                <p class="font-weight-light mb-1 mt-n1 text-white-50 font-small">Completed Tasks</p>
                <p><i class="far fa-paper-plane pr-2 align-text-top mt-n1 icon icon-blue"></i><span
                    class="font-weight-light h3">14,500K</span></p>
              </div>
              <canvas id="lineChartSecond" height="280"></canvas>
            </div>
          </div>

        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-lg-4 col-md-6 mb-4">

          <div class="card">
            <div class="card-body px-2">
              <div class="float-left white-text pl-4">
                <p class="font-weight-light mb-1 mt-n1 text-white-50 font-small">Daily Sales</p>
                <p><i class="fas fa-truck pr-2 align-text-top mt-n1 icon icon-blue"></i><span
                    class="font-weight-light h3">7,000€</span></p>
              </div>
              <canvas id="barChart" height="280"></canvas>
            </div>
          </div>

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-6 mb-4">

          <!-- Card -->
          <div class="card card-table">

            <div class="card-header d-flex justify-content-between">
              
              <div class="d-flex justify-content-start">
                <p class="my-2 text-white pr-3">Tasks(5)</p>
                <p class="my-2 text-white">Today</p>
              </div>
              <!--Dropdown primary-->
              <div class="dropdown mt-2">

                <!--Trigger-->
                <a class="dropdown-toggle text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></a>

                <!--Menu-->
                <div class="dropdown-menu dropdown-primary">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </div>

            </div>

            <!-- Card content -->
           
                         