<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="public/assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="public/assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    {{ $title }}
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{url('/')}}/public/assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{url('/')}}/public/assets/demo/demo.css" rel="stylesheet" />
  <style type="text/css">
   
   .sidebar-mini .sidebar:hover .myclass {
     display: none;
}


 
.loader {
 
  text-indent: -9999px;
    /* text-align: center; */
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
        margin: auto;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

  @keyframes spinner {
      0% {
      transform: translate3d(-50%, -50%, 0) rotate(0deg);
      }
      100% {
      transform: translate3d(-50%, -50%, 0) rotate(360deg);
      }
      }
      .spin::before {
     animation: 1.5s linear infinite spinner;
    animation-play-state: inherit;
    border: solid 2px #c1d2ce;
    border-bottom-color: #0e0e0e;
    border-radius: 64%;
    content: "";
    height: 21px;
    width: 21px;
    position: absolute;
    top: 49%;
    left: 18%;
    margin-right: 2%
    transform: translate3d(-50%, -50%, 0);
    will-change: transform;
      }

#device_select{
  overflow: hidden;
}
#profile span {
    position: absolute;
    /* margin: 3%; */
    padding: -2px;
    /* margin-right: 20%; */
    /* margin-right: 25px; */
    background-color: white;
    width: 85%;
    z-index: 1;
}
#list_device
{
  margin-top: 30px;
  }
  </style>

</head>

<body class="">