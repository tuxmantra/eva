  
  <div class="wrapper ">
    <div class="sidebar" data-color="rose" data-background-color="black" data-image="{{url('/')}}/public/assets/img/road.png">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        
        <a href="{{url('/')}}/public/logo/logo.png" class="simple-text logo-mini" >
          <div class="myclass" style="display: none">
            <img  src="{{url('/')}}/public/logo/logo.png" style="width: 100%;">
          </div>
        </a>
    
        <a href="{{url('/')}}" class="simple-text logo-normal">
       <img class="icon" src="{{url('/')}}/public/logo/logo.png" style="width: 70%;">
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
           <!--  <img src="{{url('/')}}/public/assets/img/faces/avatar.jpg" /> -->
          </div>
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample" class="username">
              <span>
                {{App\Helpers\Helper::getUserName()}}
                <b class="caret"></b>
              </span>
            </a>
            <div class="collapse" id="collapseExample">
              <ul class="nav">
               <!--  <li class="nav-item">
                  <a class="nav-link" href="profile">
                    <span class="sidebar-mini"> MP </span>
                    <span class="sidebar-normal"> My Profile </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">
                    <span class="sidebar-mini"> EP </span>
                    <span class="sidebar-normal"> Edit Profile </span>
                  </a>
                </li> -->
                <li class="nav-item">
                  <a class="nav-link" href="change-password">
                    <span class="sidebar-mini"> S </span>
                    <span class="sidebar-normal"> Change Password </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <ul class="nav">
          <?php if($menu->u_category <= 4){ ?>
            <li class="nav-item active ">
              <a class="nav-link" href="{{url('/')}}/dashboard">
                <i class="material-icons">dashboard</i>
                <p> Dashboard </p>
              </a>
            </li>
          <?php } ?>
        <?php if($menu->u_category == 1){ ?>
          <li class="nav-item " <?php if(!empty($conf_open)){ ?> style="background-color: transparent;" <?php } ?>>
            <a class="nav-link" data-toggle="collapse" style="<?php if(!empty($conf_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" href="#componentsExamples" <?php if(!empty($conf_open)){ ?> aria-expanded="true" <?php } ?>>
              <i class="material-icons">apps</i>
              <p> Configuration
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse <?php if(!empty($conf_open)){ ?> show <?php } ?>" id="componentsExamples">
              <ul class="nav">

                <li class="nav-item ">
                  <a style="<?php if(!empty($new_config)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/new-config">
                    <span class="sidebar-mini"> NC </span>
                    <span class="sidebar-normal"> New Configuration </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a style="<?php if(!empty($allot_config)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/allot-config">
                    <span class="sidebar-mini"> ACL </span>
                    <span class="sidebar-normal"> Allot Configuration </span>
                  </a>
                </li>
         
              </ul>
            </div>
          </li>
        <?php } ?>
        <?php if($menu->masters == 1){ ?>

          <li class="nav-item " <?php if(!empty($mast_open)){ ?> style="background-color: transparent;" <?php } ?>>
            <a style="<?php if(!empty($mast_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" data-toggle="collapse" href="#pagesExamples" <?php if(!empty($mast_open)){ ?> aria-expanded="true" <?php } ?>> 
              <i class="material-icons">perm_identity</i>
              <p> Masters
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse <?php if(!empty($mast_open)){ ?> show <?php } ?>" id="pagesExamples">
              <ul class="nav">
              <?php if($menu->users == 1){ ?>
                <li class="nav-item ">
                  <a style="<?php if(!empty($user_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/user-list">
                    <span class="sidebar-mini"> UL </span>
                    <span class="sidebar-normal">Users </span>
                  </a>
                </li>
              <?php } ?>
              <?php if($menu->device == 1){ ?>
                <li class="nav-item ">
                        <a style="<?php if(!empty($devi_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/device-list">
                          <span class="sidebar-mini"> ALC </span>
                          <span class="sidebar-normal">Devices</span>
                        </a>
                </li>
              <?php } ?>


              <?php if($menu->activation_code == 1){ ?>
                <li class="nav-item ">
                    <a style="<?php if(!empty($acti_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/activation-code">
                        <span class="sidebar-mini"> AC </span>
                        <span class="sidebar-normal">Activation Code<span>
                    </a>
                </li>
                <?php } ?>
                <?php if($menu->operator == 1){ ?>
                <li class="nav-item ">
                    <a style="<?php if(!empty($operator_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/operator">
                        <span class="sidebar-mini"> OP </span>
                        <span class="sidebar-normal">Operator<span>
                    </a>
                </li>
                <?php } ?>
                <?php if($menu->u_category == 1 || $menu->u_category == 2){ ?>
                 <li style="<?php if(!empty($devipen_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-item ">
                        <a class="nav-link" href="{{url('/')}}/device-pending">
                          <span class="sidebar-mini"> DP </span>
                          <span class="sidebar-normal">Device Pending</span>
                        </a>
                </li>
                <?php } ?>
               
              </ul>
            </div>
          </li>
         <?php } ?>
          <?php if($menu->settings == 1){ ?>
          <li class="nav-item " <?php if(!empty($sest_open)){ ?> style="background-color: transparent;" <?php } ?>>
            <a style="<?php if(!empty($sest_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" data-toggle="collapse" href="#componentsSettings" <?php if(!empty($sest_open)){ ?> aria-expanded="true" <?php } ?>>
              <i class="material-icons">settings</i>
              <p> Settings
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse <?php if(!empty($sest_open)){ ?> show <?php } ?>" id="componentsSettings">
              <ul class="nav">
                <?php if($menu->system == 1){ ?>
                <li class="nav-item ">
                  <a style="<?php if(!empty($syst_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/system-settings">
                    <span class="sidebar-mini"> SYS </span>
                    <span class="sidebar-normal"> System </span>
                  </a>
                </li>
                 <?php } ?>
                 <?php if($menu->vehicle == 1){ ?>
                <li class="nav-item ">
                  <a style="<?php if(!empty($vehs_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/vehicle-settings">
                    <span class="sidebar-mini"> VEH </span>
                    <span class="sidebar-normal"> Vehicle </span>
                  </a>
                </li>
                 <?php } ?>
                 <?php if($menu->vehspic == 1){ ?>
                <li class="nav-item ">
                  <a style="<?php if(!empty($spec_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/vehicle-specification">
                    <span class="sidebar-mini"> VSPC </span>
                    <span class="sidebar-normal"> Specification </span>
                  </a>
                </li>
               <?php } ?>
                 <?php if($menu->data == 1){ ?>
                <li class="nav-item ">
                  <a style="<?php if(!empty($ddt_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" data-toggle="collapse" href="#dataExample">
                    <p> Data
                      <b class="caret"></b>
                    </p>
                  </a>
                  <div class="collapse <?php if(!empty($ddt_open)){ ?> show <?php } ?>" id="dataExample">
                    <ul class="nav">
                      <?php if($menu->core_part == 1){ ?>
                      <li class="nav-item ">
                        <a style="<?php if(!empty($cr_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/data-core-list">
                          <span class="sidebar-mini">CP</span>
                          <span class="sidebar-normal">Core Part</span>
                        </a>
                      </li>
                      <?php } ?>
                      <?php if($menu->value_part == 1){ ?>
                       <li class="nav-item">
                        <a style="<?php if(!empty($vl_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/data-value-list">
                          <span class="sidebar-mini">VP</span>
                          <span class="sidebar-normal">Value Part</span>
                        </a>
                      </li>
                       <?php } ?>
                    </ul>
                  </div>
                </li> 
                <?php } ?>
                <?php if($menu->service_sett == 1){ ?>
                 <li class="nav-item ">
                  <a style="<?php if(!empty($ss_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" data-toggle="collapse" href="#serviceExample" <?php if(!empty($ss_open)){ ?> aria-expanded="true" <?php } ?>>
                    <p> Service Setting
                      <b class="caret"></b>
                    </p>
                  </a>
                  <div class="collapse <?php if(!empty($ss_open)){ ?> show <?php } ?>" id="serviceExample">
                    <ul class="nav">
                     <?php if($menu->descrp_sett == 1){ ?>
                      <li class="nav-item ">
                        <a style="<?php if(!empty($sst_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/desc-list">
                          <span class="sidebar-mini">DT</span>
                          <span class="sidebar-normal">Description Type</span>
                        </a>
                      </li>
                      <?php } ?>
                    </ul>
                  </div>
                </li>
                <?php } ?>
                <?php if($menu->alertmail == 1){ ?>
                <li class="nav-item ">
                    <a style="<?php if(!empty($alr_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/alert-mail-list">
                        <span class="sidebar-mini"> ALR </span>
                        <span class="sidebar-normal"> Alerts Mail</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($menu->dbase == 1){ ?>
                <li class="nav-item ">
                    <a style="<?php if(!empty($db_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/database-optimization">
                        <span class="sidebar-mini"> DB </span>
                        <span class="sidebar-normal">Database</span>
                    </a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </li>
          <?php } ?>

          <?php if($menu->summary == 1){ ?>
          <li class="nav-item ">
            <a style="<?php if(!empty($summary_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/summary">
              <i class="material-icons">menu_book</i>
              <p> Summary
              </p>
            </a>
          </li>
          <?php } ?>

          <?php if($menu->report == 1){ ?>
          <li class="nav-item">
            <a style="<?php if(!empty($repo_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/reports">
              <i class="material-icons">library_books</i>
              <p> Reports </p>
            </a>
          </li>
          <?php } ?>
          
          <?php if($menu->eva == 1){ ?>
          <li class="nav-item ">
            <a style="<?php if(!empty($eva_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/eva">
              <i class="material-icons">show_chart</i>
              <p> EVA
              </p>
            </a>
          </li>
          <?php } ?>
             
          <?php if($menu->digno == 1){ ?>
              <li class="nav-item">
                <a style="<?php if(!empty($digi)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/remote-diagnostics">
                  <i class="material-icons">network_check</i>
                  <p> Diagnostics </p>
                </a>
              </li>
          <?php } ?>

          

          <?php if($menu->alert == 1){ ?>
          <li class="nav-item">
           <a style="<?php if(!empty($allr_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/alerts">
             <i class="material-icons">notification_important</i>
             <p> Alerts </p>
           </a>
         </li>
          <?php } ?>
          
         
  
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
              </button>
            </div>
