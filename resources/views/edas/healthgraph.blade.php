<style type="text/css">
.highcharts-container{
    height: 400px !important;
   } 
</style>
<div id="container" style="height: 400px !important"></div>
<script>
Highcharts.chart('container', {
    legend: {
        enabled: false
    },
    title: {
            text: 'EDAS Score',
            style: {
                color: '#34495e', 
                fontWeight: 'bold'
                }
            },
    xAxis: {
        minPadding: 0.05,
        maxPadding: 0.05 
    },
    yAxis: [ { // Tertiary yAxis
            gridLineWidth: 0.5,
            title: {
                text: 'Score',
                style: {
                         fontWeight: 'bold'  
                       }
            }
        }
        ],
    series: [{
        name: 'Score',
        type: 'spline',
        data: [<?=$score?>]
    }]
});
</script>