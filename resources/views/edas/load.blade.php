<style type="text/css">
   .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
   padding: 12px 8px !important;
   vertical-align: middle;
   border-color: #ddd;
   }
   .rate-popover {
   color: #c4c4c4;
   }
   .oneStar {
   color: #3d381c;
   }
   .twoStars {
   color: #6d6126;
   }
   .threeStars {
   color: #c2aa36;
   }
   .fourStars {
   color: #e2c327;
   }
   .fiveStars {
   color: #f3cb06;
   }
   .highcharts-container{
    height: 275px !important;
   } 
</style>
<div class="col-md-12" id="load_data">
   <ul class="nav nav-pills nav-pills-warning" role="tablist">
      <li class="nav-item">
         <a class="nav-link active" onclick="reloadpage()" data-toggle="tab" href="#link1" role="tablist">
         EDAS Score
         </a>
      </li>
      <li class="nav-item">
         <a class="nav-link" onclick="getgraph()" id="graphb" data-toggle="tab" href="#link2" role="tablist">
         Graph 
         </a>
      </li>
      <?php  
         $userdata=Session::get('userdata'); 
         $utype=$userdata['utype']; 
         if($utype == 1)
         {
           ?>
      <li class="nav-item">
         <a class="nav-link" onclick="getalertcount()" id="graphb" data-toggle="tab" href="#link3" role="tablist">
         Alert Counts 
         </a>
      </li>
      <?php 
         } 
         ?>
   </ul>
   <div class="tab-content tab-space">
      <!----------------- Start EDAS Score --------------------> 
      <div class="tab-pane active" id="link1">
         <div class="container-fluid">
            <div class="continues">
               <div class="row classes">
                  <div class="col-md-12">
                     <div id="container-speed" class="chart-container"></div>
                     @if(!empty($scoreComment))
                     
                       <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                
                          <tr>
                            <th class="tdr tdrA tdl th-clr" style="width:20%;text-align: center;">
                              Alert Category
                            </td>
                            <th class="tdr tdrA tdl th-clr" style="width:20%">
                              Comments
                            </td>
                            <th class="tdr tdrB th-clr">
                              Explanation
                            </th>
                          </tr>
                          @foreach($scoreComment as $key => $value) 
                            @if(!empty($value))
                              @if(!empty($value['comment']))
                                <tr>
                                  <td class="tdr tdl " style="text-align: center;">
                                    {{$value['category']}}
                                  </td>
                                  <td class="tdr tdl ">
                                    {{$value['comment']}}
                                  </td>
                                  <td class="tdr">
                                    {{$value['exp']}}
                                  </td>
                                </tr>
                              @endif
                            @endif
                          @endforeach

                        </table>
                      @endif
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!----------------- End EDAS Score -------------------->
      <!----------------- Graph Start -------------------->
      <div class="tab-pane" id="link2">
         <div class="container-fluid">
            <div class="continues">
               <div class="row" >
                  <div class="col-md-3" >
                     <select  id="year" onchange="getgraph()" data-size="7" title="Select Year">
                        <option value="0">Pick Year</option>
                        <option <?php if(!empty($year)){ if($year == '2018'){ ?> selected <?php } } ?> value="2018">Year 2018</option>
                        <option <?php if(!empty($year)){ if($year == '2019'){ ?> selected <?php } } ?> value="2019">Year 2019</option>
                        <option <?php if(!empty($year)){ if($year == '2020'){ ?> selected <?php } } ?>   value="2020">Year 2020</option>
                        <option <?php if(!empty($year)){ if($year == '2021'){ ?> selected <?php } } ?> value="2021" selected>Year 2021</option>
                        <option <?php if(!empty($year)){ if($year == '2022'){ ?> selected <?php } } ?> value="2022">Year 2022</option>
                        <option <?php if(!empty($year)){ if($year == '2023'){ ?> selected <?php } } ?> value="2023">Year 2023</option>
                        <option <?php if(!empty($year)){ if($year == '2024'){ ?> selected <?php } } ?> value="2024">Year 2024</option>
                        <option <?php if(!empty($year)){ if($year == '2025'){ ?> selected <?php } } ?> value="2025">Year 2025</option>
                     </select>
                  </div>
                  <div class="col-md-3" >
                     <select  id="month" onchange="getgraph()" data-size="7" title="Select Month">
                        <option value="0" selected>Pick Month</option>
                        <option <?php if(!empty($month)){ if($month == '1'){ ?> selected <?php } } ?> value="1">January</option>
                        <option <?php if(!empty($month)){ if($month == '2'){ ?> selected <?php } } ?> value="2">February</option>
                        <option <?php if(!empty($month)){ if($month == '3'){ ?> selected <?php } } ?> value="3">March</option>
                        <option <?php if(!empty($month)){ if($month == '4'){ ?> selected <?php } } ?> value="4">April</option>
                        <option <?php if(!empty($month)){ if($month == '5'){ ?> selected <?php } } ?> value="5">May</option>
                        <option <?php if(!empty($month)){ if($month == '6'){ ?> selected <?php } } ?> value="6">June</option>
                        <option <?php if(!empty($month)){ if($month == '7'){ ?> selected <?php } } ?> value="7">July</option>
                        <option <?php if(!empty($month)){ if($month == '8'){ ?> selected <?php } } ?> value="8">August</option>
                        <option <?php if(!empty($month)){ if($month == '9'){ ?> selected <?php } } ?> value="9">September</option>
                        <option <?php if(!empty($month)){ if($month == '10'){ ?> selected <?php } } ?> value="10">October</option>
                        <option <?php if(!empty($month)){ if($month == '11'){ ?> selected <?php } } ?> value="11">November</option>
                        <option <?php if(!empty($month)){ if($month == '12'){ ?> selected <?php } } ?> value="12">December</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="card-body " id="graph"> 
            </div>
         </div>
      </div>
      <!----------------- Graph END -------------------->
      <!----------------- Counts Starts -------------------->
      <div class="tab-pane" id="link3">
        <div class="container-fluid">
           <div class="card-body " id="counts"> 
           </div>
        </div>
      </div>
      <!----------------- Counts Ends -------------------->
   </div>
</div>
<script type="text/javascript">
   $('select').selectpicker();
   function reloadpage()
   {
      $('#link1').hide();
      location.reload();
   }
   function getalertcount()
   {
      var did   ="{{$did}}";
      $.post('{{url('/')}}/getAlertCount', 
      { 
        did: did
      }).done(function (data) 
      {
          $('#counts').html(data);
      });
   }
   function getgraph()
   {
      /*  var date = $('#datetimepicker12').datepicker('getFormattedDate');*/
      var e1 = document.getElementById("year");
      var year = e1.options[e1.selectedIndex].value;
      // alert(year);
      var e2 = document.getElementById("month");
      var month = e2.options[e2.selectedIndex].value;
     
        
      var did   ="{{$did}}";
        
      $.post('{{url('/')}}/getEdasGraph', 
      { 
        did: did,year:year,month:month 
      }).done(function (data) 
      {
             $('#graph').html(data);
      });
    }
    
   
   var gaugeOptions = {
       chart: {
           type: 'solidgauge'
       },
   
       title: null,
   
       pane: {
           center: ['50%', '60%'],
           size: '120%',
           startAngle: -90,
           endAngle: 90,
           background: {
               backgroundColor:
                   Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
               innerRadius: '60%',
               outerRadius: '100%',
               shape: 'arc'
           }
       },
   
       exporting: {
           enabled: false
       },
   
       tooltip: {
           enabled: false
       },
   
       // the value axis
       yAxis: {
           stops: [
               [0.0, '#eb262d'],  // red
               [0.40, '#eb262d'], // red
               [0.41, '#fdff4f'], // orange
               [0.55, '#fdff4f'], // orange
               [0.56, '#ffff00'], //yellow
               [0.75, '#ffff00'], // yellow
               [0.76, '#00ff00'], //lightgreen
               [0.85, '#00ff00'], //lightgreen
               [0.86, '#37b518']  // green
   
           ],
           lineWidth: 0,
           tickWidth: 0,
           minorTickInterval: null,
           tickAmount: 1,
           title: {
               y: -70
           },
           labels: {
               y: 16
           }
       },
   
       plotOptions: {
           solidgauge: {
               dataLabels: {
                   y: 5,
                   borderWidth: 0,
                   useHTML: true
               }
           }
       }
   };
   
   // The speed gauge
   var chartSpeed = Highcharts.chart('container-speed', Highcharts.merge(gaugeOptions, {
       yAxis: {
           min: 0,
           max: 100,
           title: {
               text: 'Score',
               x:0,
               y:50,
               style: {
               fontSize : '20',
           }
           }
       },
   
       credits: {
           enabled: false
       },
   
       series: [{
           name: 'Score',
           data: [<?=$score?>],
           dataLabels: {
               format:
                   '<div style="text-align:center;margin-top: -55px !important;">' +
                   '<span style="font-size:65px">{y}</span>' +
                   '<span style="font-size:12px;opacity:0.4"> /100</span>' +
                   '</div>'
           },
           tooltip: {
               valueSuffix: ' '
           }
       }]
   
   }));
   
   
   
                  
   
</script>