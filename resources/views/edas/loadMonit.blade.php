<?php
//Bat Hel
$batHel = array();
if($data['bat_st'] == 1)
{
	$batHel['rate'] = "1";
	$batHel['con'] = "Bad";
	$batHel['msg'] = "\"Battery Critical\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
}
elseif($data['bat_st'] == 2)
{
	$batHel['rate'] = "2";
	$batHel['con'] = "Bad–Okay";
	$batHel['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
}
elseif($data['bat_st'] == 3)
{
	$batHel['rate'] = "3";
	$batHel['con'] = "Bad-Okay";
	$batHel['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
}
elseif($data['bat_st'] == 4)
{
	$batHel['rate'] = "4";
	$batHel['con'] = "Bad";
	$batHel['msg'] = "\"Battery Low\" Please drive to charge.";
}
elseif($data['bat_st'] == 5)
{
	$batHel['rate'] = "5";
	$batHel['con'] = "Good";
	$batHel['msg'] = "";
}
else
{
	$batHel['rate'] = "";
	$batHel['con'] = "";
	$batHel['msg'] = "PID Not Supported";
}

//Turbo
$turbo =array();
if($data['idlb_c'] == 1)
{
	if($data['pekb_c'] == 1)
	{
		$turbo['rate'] = "1";
		$turbo['con'] = "Bad";
		$turbo['msg'] = "Turbo boost pressure build-up critical. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
	}
	elseif($data['pekb_c'] == 2)
	{
		$turbo['rate'] = "2";
		$turbo['con'] = "Bad-Okay";
		$turbo['msg'] = "Turbo boost pressure build-up poor. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
	}
	elseif($data['pekb_c'] == 3)
	{
		$turbo['rate'] = "3";
		$turbo['con'] = "Okay";
		$turbo['msg'] = "Turbo boost pressure build-up below average. Clean/replace air filter element.";
	}
}
elseif($data['idlb_c'] == 2)
{
	if($data['pekb_c'] == 1)
	{
		$turbo['rate'] = "1";
		$turbo['con'] = "Bad";
		$turbo['msg'] = "Turbo boost pressure build-up critical. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
	}
	elseif($data['pekb_c'] == 2)
	{
		$turbo['rate'] = "3";
		$turbo['con'] = "Okay";
		$turbo['msg'] = "Turbo boost pressure build-up below average. Clean/replace air filter element.";
	}
	elseif($data['pekb_c'] == 3)
	{
		$turbo['rate'] = "4";
		$turbo['con'] = "Okay-Good";
		$turbo['msg'] = "Turbo boost pressure build-up average. Clean/replace air filter element. Eva will keep an eye on it during the next monitoring cycle.";
	}
}
elseif($data['idlb_c'] == 3)
{
	if($data['pekb_c'] == 1)
	{
		$turbo['rate'] = "2";
		$turbo['con'] = "Bad-Okay";
		$turbo['msg'] = "Turbo boost pressure build-up poor. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
	}
	elseif($data['pekb_c'] == 2)
	{
		$turbo['rate'] = "3";
		$turbo['con'] = "Okay";
		$turbo['msg'] = "Turbo boost pressure build-up below average. Clean/replace air filter element.";
	}
	elseif($data['pekb_c'] == 3)
	{
		$turbo['rate'] = "5";
		$turbo['con'] = "Good";
		$turbo['msg'] = "";
	}
}
$turbo['msg'] = (trim($data['idlb_m']) != trim($data['pekb_m']) ) ? trim($data['idlb_m']) ."<br/>". trim($data['pekb_m']) :  trim($data['pekb_m']);

//Air
$air =array();
if($data['idlbc_v'] == 1)
{
	if($data['peakb_v'] == 1)
	{
		$air['rate'] = "1";
		$air['con'] = "Bad";
		$air['msg'] = "Air system pressure build-up critical. Check/replace air filter element. Visit the Manufacturer Dealership whenever possible.";
	}
	elseif($data['peakb_v'] == 2)
	{
		$air['rate'] = "2";
		$air['con'] = "Bad-Okay";
		$air['msg'] = "Air system pressure build-up poor. Check/replace air filter element. Visit the Manufacturer Dealership whenever possible.";
	}
	elseif($data['peakb_v'] == 3)
	{
		$air['rate'] = "3";
		$air['con'] = "Okay";
		$air['msg'] = "Air system pressure build-up below average. Check/replace air filter element.";
	}
}
elseif($data['idlbc_v'] == 2)
{
	if($data['peakb_v'] == 1)
	{
		$air['rate'] = "1";
		$air['con'] = "Bad";
		$air['msg'] = "Air system pressure build-up critical. Check/replace air filter element. Visit the Manufacturer Dealership whenever possible.";
	}
	elseif($data['peakb_v'] == 2)
	{
		$air['rate'] = "3";
		$air['con'] = "Okay";
		$air['msg'] = "Air system pressure build-up below average. Check/replace air filter element.";
	}
	elseif($data['peakb_v'] == 3)
	{
		$air['rate'] = "4";
		$air['con'] = "Okay-Good";
		$air['msg'] = "Air system pressure build-up average. Check/replace air filter element. Eva will keep an eye on it during the next monitoring cycle.";
	}
}
elseif($data['idlbc_v'] == 3)
{
	if($data['peakb_v'] == 1)
	{
		$air['rate'] = "2";
		$air['con'] = "Bad-Okay";
		$air['msg'] = "Air system pressure build-up poor. Check/replace air filter element. Visit the Manufacturer Dealership whenever possible.";
	}
	elseif($data['peakb_v'] == 2)
	{
		$air['rate'] = "3";
		$air['con'] = "Okay";
		$air['msg'] = "Air system pressure build-up below average. Check/replace air filter element.";
	}
	elseif($data['peakb_v'] == 3)
	{
		$air['rate'] = "5";
		$air['con'] = "Good";
		$air['msg'] = "";
	}
}
$air['msg'] = (trim($data['idlbc_m']) != trim($data['peakb_m']) ) ? trim($data['idlbc_m']) ."<br/>". trim($data['peakb_m']) : trim($data['idlbc_m']);

//Fuel
$fuel =array();
if($data['idl_crp'] == 1)
{
	if($data['peak_crp'] == 1)
	{
		$fuel['rate'] = "1";
		$fuel['con'] = "Bad";
		$fuel['msg'] = "Fuel pressure critical. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
	}
	elseif($data['peak_crp'] == 2)
	{
		$fuel['rate'] = "2";
		$fuel['con'] = "Bad-Okay";
		$fuel['msg'] = "Fuel pressure low. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
	}
	elseif($data['peak_crp'] == 3)
	{
		$fuel['rate'] = "3";
		$fuel['con'] = "Okay";
		$fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
	}
}
elseif($data['idl_crp'] == 2)
{
	if($data['peak_crp'] == 1)
	{
		$fuel['rate'] = "1";
		$fuel['con'] = "Bad";
		$fuel['msg'] = "Fuel pressure critical. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
	}
	elseif($data['peak_crp'] == 2)
	{
		$fuel['rate'] = "3";
		$fuel['con'] = "Okay";
		$fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
	}
	elseif($data['peak_crp'] == 3)
	{
		$fuel['rate'] = "4";
		$fuel['con'] = "Okay-Good";
		$fuel['msg'] = "Fuel pressure build-up average. Eva will keep an eye on it during the next monitoring cycle.";
	}
}
elseif($data['idl_crp'] == 3)
{
	if($data['peak_crp'] == 1)
	{
		$fuel['rate'] = "2";
		$fuel['con'] = "Bad-Okay";
		$fuel['msg'] = "Fuel pressure low. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
	}
	elseif($data['peak_crp'] == 2)
	{
		$fuel['rate'] = "3";
		$fuel['con'] = "Okay";
		$fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
	}
	elseif($data['peak_crp'] == 3)
	{
		$fuel['rate'] = "5";
		$fuel['con'] = "Good";
		$fuel['msg'] = "";
	}
}
$fuel['msg'] = $data['crpm'];

//backPre
$backPre = array();
if($data['ergsmd_ms'] == 1)
{
	$backPre['rate'] = "1";
	$backPre['con'] = "Bad";
	$backPre['msg'] = "Exhaust back-pressure build-up very high. Try driving at high speeds continuously for about 3-5 mins on the freeway. Please exercise caution.";
}
elseif($data['ergsmd_ms'] == 2)
{
	$backPre['rate'] = "2";
	$backPre['con'] = "Bad–Okay";
	$backPre['msg'] = "Exhaust back-pressure build-up high. Try driving at high speeds continuously for about 2-4 mins on the freeway. Please exercise caution.";
}
elseif($data['ergsmd_ms'] == 3)
{
	$backPre['rate'] = "3";
	$backPre['con'] = "Okay";
	$backPre['msg'] = "Exhaust back-pressure build-up high. Try driving at high speeds continuously for about 2-3 mins on the freeway. Please exercise caution.";
}
elseif($data['ergsmd_ms'] == 4)
{
	$backPre['rate'] = "4";
	$backPre['con'] = "Okay-Good";
	$backPre['msg'] = "Exhaust back-pressure build-up marginally high. Eva will keep an eye on it during the next monitoring cycle.";
}
elseif($data['ergsmd_ms'] == 5)
{
	$backPre['rate'] = "5";
	$backPre['con'] = "Good";
	$backPre['msg'] = "";
}
else 
{
	$backPre['rate'] = "";
	$backPre['con'] = "";
	$backPre['msg'] = $data['ergsmd_ms'];
}


//combustionPressure
$copPres = array();
if($data['comp_pres'] == 1)
{
	$copPres['rate'] = "1";
	$copPres['con'] = "Bad";
}
elseif($data['comp_pres'] == 2)
{
	$copPres['rate'] = "2";
	$copPres['con'] = "Bad–Okay";
}
elseif($data['comp_pres'] == 3)
{
	$copPres['rate'] = "3";
	$copPres['con'] = "Okay";
}
elseif($data['comp_pres'] == 4)
{
	$copPres['rate'] = "4";
	$copPres['con'] = "Okay-Good";
}
elseif($data['comp_pres'] == 5)
{
	$copPres['rate'] = "5";
	$copPres['con'] = "Good";
}
else
{
	$copPres['msg'] = $data['comp_pres'];
}

//rpmOscillationAnomalyAnalysis
$rcOcil = array();
if($data['eng_block_ej'] == 1)
{
	$rcOcil['rate'] = "1";
	$rcOcil['con'] = "Bad";
}
elseif($data['eng_block_ej'] == 2)
{
	$rcOcil['rate'] = "2";
	$rcOcil['con'] = "Bad–Okay";
}
elseif($data['eng_block_ej'] == 3)
{
	$rcOcil['rate'] = "3";
	$rcOcil['con'] = "Okay";
}
elseif($data['eng_block_ej'] == 4)
{
	$rcOcil['rate'] = "4";
	$rcOcil['con'] = "Okay-Good";
}
elseif($data['eng_block_ej'] == 5)
{
	$rcOcil['rate'] = "5";
	$rcOcil['con'] = "Good";
}
//falseOdometerReading
$falOdMr = array();
$falOdMr['rate'] = "";
$falOdMr['con'] = "";
$falOdMr['msg'] = $data['falsemeter'];

//getVehSpeedSence
$vehSpeed = array();
$vehSpeed['rate'] = "";
$vehSpeed['con'] = "";
//$vehSpeed['msg'] = ($data['vehspeed'] <= 0) ? "Vehicle Speed Sensor Failure possibility" : "Vehicle Speed Okay";
if($data['vehspeed'] == 0)
{
	$vehSpeed['msg'] = "Vehicle Speed Sensor Failure possibility";
}
elseif($data['vehspeed'] == 1)
{
	$vehSpeed['msg'] = "Vehicle Speed Okay";
}
elseif($data['vehspeed'] == "") 
{
	$vehSpeed['msg'] = "";
}

// cool Temp
$coolTemp = array();
if($data['coolp'] == 1)
{
	$coolTemp['rate'] = "1";
	$coolTemp['con'] = "Bad";
	$coolTemp['msg'] = "Engine cooling poor. Check Coolant and Engine Oil Level. Visit the Manufacturer Dealership whenever possible.";
}
elseif($data['coolp'] == 2)
{
	$coolTemp['rate'] = "2";
	$coolTemp['con'] = "Bad–Okay";
	$coolTemp['msg'] = "Engine cooling inadequate. Check Coolant and Engine Oil Level. Top-up Coolant and Engine Oil if low.";
}
elseif($data['coolp'] == 3)
{
	$coolTemp['rate'] = "3";
	$coolTemp['con'] = "Okay";
	$coolTemp['msg'] = "Engine cooling below average. Check Coolant and Engine Oil Level. Top-up Coolant and Engine Oil if low.";
}
elseif($data['coolp'] == 4)
{
	$coolTemp['rate'] = "4";
	$coolTemp['con'] = "Okay-Good";
	$coolTemp['msg'] = "Engine cooling average. Eva will keep an eye on it during the next monitoring cycle.";
}
elseif($data['coolp'] == 5)
{
	$coolTemp['rate'] = "5";
	$coolTemp['con'] = "Good";
	$coolTemp['msg'] = "";
}
else
{
	$coolTemp['msg'] = $data['coolm'];
}


//combustion pressure message
if($copPres['rate'] == 5 || $copPres['rate'] == "")
{
	$copPres['msg'] = "";
}
elseif($copPres['rate'] <= 4)
{
	if((($turbo['rate']==5 || $air['rate']==5) && $fuel['rate'] ==5 && $rcOcil['rate']<=4) || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] <=0 )  || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] == 5)  || (($turbo['rate']==5 || $air['rate']==5)&& $rcOcil['rate']<=5 && $fuel['rate'] <=4 )  || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] <=4 ))
	{
		if($data['ocilv'] >= 1)
		{
			$copPres['msg'] = "It appears your engine's combustion pressures are not good. This is attributed to possibly the engine's compression pressure";
		}
		else 
		{
			$n = array();
			if($turbo['rate'] >= 1)
			{
				$n = array($turbo['rate'],$fuel['rate']);
			}
			else {
				$n = array($air['rate'],$fuel['rate']);
			}
			$nVal = min($n);
			
			if($nVal[0] == $nVal[1])
			{
				if($nVal <= 3)
				{	
					$copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your High Pressure Fuel System and/or Air System.";
				}
			}
			else
			{
				if($fuel['rate'] <= 0)
				{
					if($nVal[0] == 5)
					{	
						$copPres['msg'] = "It appears your engine's combustion pressures are not good. We could not check your High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
					}
					else 
					{
						$copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your Air System.";
					}
				}
				else 
				{
					if($nVal[0]>$nVal[1])
					{
						$copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your High Pressure Fuel System.";
					}
					else
					{
						$copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your Air System.";
					}
				}
			}
		}
	}
}
?>
 <div class="material-datatables">

  <table id="datatablesvehicle1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
  	 <thead>
	<tr>
		<th class="tdr tdrA tdl th-clr">
			Component
		</td>
		<th class="tdr tdrB th-clr">
			Ratings
		</th>
		<th class="tdr tdrC th-clr">
			Condition
		</th>
		<th class="tdr tdrD th-clr">
			Message
		</th>
	</tr>
	 <thead>
	 	 <tbody>
	<tr>
		<td class="tdr tdl ">
			Battery Health
		</td>
		<td class="tdr">
			<?=$batHel['rate']?>
		</td>
		<td class="tdr">
			<?=$batHel['con']?>
		</td>
		<td class="tdr">
			<?=$batHel['msg']?>
		</td>
	</tr>
	
	
	<tr>
		<td class="tdr tdl">
			Turbocharger Health
		</td>
		<td class="tdr">
			<?=$turbo['rate']?>
		</td>
		<td class="tdr">
			<?=$turbo['con']?>
		</td>
		<td class="tdr">
			<?=$turbo['msg']?>
		</td>
	</tr>
	
	<tr>
		<td class="tdr tdl">
			Air System
		</td>
		<td class="tdr">
			<?=$air['rate']?>
		</td>
		<td class="tdr">
			<?=$air['con']?>
		</td>
		<td class="tdr">
			<?=$air['msg']?>
		</td>
			
		</td>
	</tr>
	
	<tr>
		<td class="tdr tdl">
			Fuel System
		</td>
		<td class="tdr">
			<?=$fuel['rate']?>
		</td>
		<td class="tdr">
			<?=$fuel['con']?>
		</td>
		<td class="tdr">
			<?=$fuel['msg']?>
		</td>
	</tr>
	
	<tr>
		<td class="tdr tdl">
			Exhaust System
		</td>
		<td class="tdr">
			<?=$backPre['rate']?>
		</td>
		<td class="tdr">
			<?=$backPre['con']?>
		</td>
		<td class="tdr">
			<?=$backPre['msg']?>
		</td>
	</tr>
	
	<tr>
		<td class="tdr tdl">
			Combustion Pressure
		</td>
		<td class="tdr">
			<?=$copPres['rate']?>
		</td>
		<td class="tdr">
			<?=$copPres['con']?>
		</td>
		<td class="tdr">
			<?=$copPres['msg']?>
		</td>
	</tr>
	
	<tr>
		<td class="tdr tdl">
			Rpm Oscillation Anomaly Analysis 
		</td>
		<td class="tdr">
			<?=$rcOcil['rate']?>
		</td>
		<td class="tdr">
			<?=$rcOcil['con']?>
		</td>
		<td class="tdr">
			<?=$rcOcil['msg']?>
		</td>
	</tr>
	<tr>
		<td class="tdr tdl">
			False Odometer Reading Detection
		</td>
		<td class="tdr">
			<?=$falOdMr['rate']?>
		</td>
		<td class="tdr">
			<?=$falOdMr['con']?>
		</td>
		<td class="tdr">
			<?=$falOdMr['msg']?>
		</td>
	</tr>
	
	<tr>
		<td class="tdr tdl">
			Engine Cooling System
		</td>
		<td class="tdr">
			<?=$coolTemp['rate']?>
		</td>
		<td class="tdr">
			<?=$coolTemp['con']?>
		</td>
		<td class="tdr">
			<?=$coolTemp['msg']?>
		</td>
	</tr>
	
	
	
	<tr>
		<td class="tdr tdrb tdl">
		Vehicle Speed Sensor malfunction
		
		</td>
		<td class="tdr tdrb">
			
		</td>
		<td class="tdr tdrb">
			
		</td>
		
		<td class="tdr tdrb">
			<?=$vehSpeed['msg']?>
		</td>
		
	</tr>
	 <tbody>
</table>
</div>

<script>
   

    $(document).ready(function() {

 


      $('#datatablesvehicle1').DataTable({
        "pagingType": "full_numbers",
         "lengthMenu": [
                      [10, 25, 50, -1],
                      [10, 25, 50, "All"]
                    ],
                    responsive: true,
                    language: {
                      search: "_INPUT_",
                      searchPlaceholder: "Search records",
                    }
      });               
    });
</script>