  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-md-12" align="right">
              <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>

            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Edit Device</h4>
                  </div>
                </div>
                <div class="card-body">
                  <form method="post" action="{{url('/')}}/edit-device-save" class="form-horizontal" onsubmit="return createDevice()">
                  <input type="hidden" value="{{$form_token}}" name="form_token">
                  <input type="hidden" name="id" value="{{App\Helpers\Helper::crypt(1,$device_data[0]->id)}}">
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Device Id</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input type="text" class="form-control" value="{{$device_data[0]->device_id}}" name="device_id" id="device_id" placeholder="Enter Device Id">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Name</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input type="text" value="{{$device_data[0]->name}}" name="name" id="name" class="form-control" placeholder="Enter Name">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Description</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea class="form-control" cols="5" rows="5" name="desc" id="desc">{{$device_data[0]->descrp}}</textarea>
                        </div>
                      </div>
                    </div>
       
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Partner</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                         
                          <select class="selectpicker" data-style="select-with-transition" title="Select User Type" data-size="7" name="partner_id" id="partner_id" tabindex="-98">
                            <option disabled="">Select Partner</option>
                            @if (!empty($partner_list))
                            @foreach ($partner_list as $pl)
                            <option <?php if($device_data[0]->partner == $pl->uid){ ?> selected <?php } ?> value="{{$pl->uid}}">{{$pl->uname}}</option>
                            @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Type</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <select class="selectpicker" data-style="select-with-transition" title="Select User Type" data-size="7" name="type" id="type" tabindex="-98">
                            <option disabled="">Select Type</option>
                            <option <?php if($device_data[0]->devtype == '1'){ ?> selected <?php } ?> value="1">Bluetooth</option>
                            <option <?php if($device_data[0]->devtype == '2'){ ?> selected <?php } ?> value="2">GSM</option>
                          </select>
                        </div>
                      </div>
                    </div>
  
                    <div class="row">
                      <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-10 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="1" <?php if($device_data[0]->status == '1'){ ?> checked <?php } ?>> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0" <?php if($device_data[0]->status == '0'){ ?> checked <?php } ?>> In Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Device OTA Commands</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea class="form-control" name="dev_cmds" id="dev_cmds">{{$device_data[0]->dev_cmds}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Device Current Version</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea class="form-control" name="curr_ver" id="curr_ver">{{$device_data[0]->curr_ver}}</textarea>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>

            <div class="col-md-12" id="load" style="display: none;" align="center">
                <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>
            </div>
            
            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          </div>
        </div>
      </div>
    </form> 
  </div>
</div>
  
  <!--   Core JS Files   -->
 @include('default.footer')