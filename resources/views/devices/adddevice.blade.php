  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
    
            <div class="col-md-12" align="right">
              <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
            

            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Add Device</h4>
                  </div>
                </div>
                <div class="card-body ">
    <form method="post" action="save-device" class="form-horizontal" onsubmit="return createDevice_new();">
                  <input type="hidden" value="{{$form_token}}" name="form_token">
   
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Device Id</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input type="text" class="form-control" name="device_id" id="device_id" placeholder="Enter Device Id">
                        </div>
                      </div>
                    </div>
                         <div class="row">
                      <label class="col-sm-2 col-form-label">Name</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Description</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea class="form-control" cols="5" rows="5" name="desc" id="desc"></textarea>
                        </div>
                      </div>
                    </div>
       
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Partner</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                         
                          <select class="selectpicker" data-style="select-with-transition" title="Select User Type" data-size="7" name="partner_id" id="partner_id" tabindex="-98">
                            <option disabled="">Select Partner</option>
                            @if (!empty($partner_list))
                            @foreach ($partner_list as $pl)
                            <option value="{{$pl->uid}}">{{$pl->uname}}</option>
                            @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <label class="col-sm-2 col-form-label">Type</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <select class="selectpicker" data-style="select-with-transition" title="Select User Type" data-size="7" name="type" id="type" tabindex="-98">
                            <option disabled="">Select Type</option>
                            <option value="1">Bluetooth</option>
                            <option value="2">GSM</option>
                            <option value="3">LINUX</option>
                          </select>
                        </div>
                      </div>
                    </div>
  
                    <div class="row">
                      <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-10 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="1" checked> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0"> In Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Device OTA Commands</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea class="form-control" cols="5" rows="5" name="dev_cmds" id="dev_cmds"></textarea>
                        </div>
                      </div>
                    </div>
                
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right" style="float: left;">

              <button type="submit" class="btn btn-fill btn-rose">Save & Proceed</button>&nbsp;

            </div>
                    <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                   
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>

          </div>
        </div>
      </div>
       </form>
     
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
   @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif
                      <script>
                        function createDevice(){
                                /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
                                var flag = 0;
                                $('#serve_message').hide();
                                var device_id   = $('#device_id').val();
                                var name        = $('#name').val();
                                var desc        = $('#desc').val();
                                var partner_id  = $('#partner_id').val();
                                var type        = $('#type').val();
                                var status      = $('#status').val();
                                $('#cli_message').hide();
                                if(device_id == '' || name == '' || desc == '' || partner_id == '' || type == '' || status == ''){
                                               Swal.fire({
                                        type: 'info',
                                        title: 'info',
                                        text:   '<?= session('message') ?>',
                                         confirmButtonColor: '#eb262d',
                                        // footer: '<a href>Why do I have this issue?</a>'
                                      })
                                    return false;
                                }else{
                                  return true;
                                }
                        }
                      </script>