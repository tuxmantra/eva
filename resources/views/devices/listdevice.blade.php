  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
    tfoot {
     display: table-header-group !important;
    }
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
  </style>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
                    <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     

                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
            <div class="col-md-12" align="right">
              <a href="{{url('/')}}/add-device" title="Add Device" class="btn btn-primary btn-sm">Add</a>
              
            </div>
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  <h4 class="card-title">Device List</h4>
                </div>
                <div class="card-body">
                  <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>
                  <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Device Id</th>
                          <th class="table-heading-font">Name</th>
                        
                          <th class="table-heading-font">Partner</th>
                          <th class="table-heading-font">Type</th>
                          <th class="table-heading-font">Status</th>
                         
                          <th class="disabled-sorting text-right table-heading-font">Actions</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Device Id</th>
                          <th class="table-heading-font">Name</th>
                        
                          <th class="table-heading-font">Partner</th>
                          <th class="table-heading-font">Type</th>
                          <th class="table-heading-font">Status</th>
                         
                          <th class="disabled-sorting text-right table-heading-font">Actions</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @if (!empty($device_list))
                        <?php $id = 1; ?>
                        @foreach ($device_list as $device)  
                        <tr>
                         
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$id}}</td>
                          <td style="background-color: #ffffff;" class="table-heading-font">{{$device->device_id}}</td>
                          <td style="background-color: #f5f5f5;" class="table-heading-font">{{$device->name}}</td>
                          
                          <td style="background-color: #ffffff;" class="table-heading-font">{{App\Helpers\Helper::getPartnerName ($device->partner)}}</td>
                          <td style="background-color: #f5f5f5;" class="table-heading-font">{{App\Helpers\Helper::getDeviceType($device->devtype)}}</td>
                          <td style="background-color: #ffffff;" class="table-heading-font">{{App\Helpers\Helper::getStatus($device->status)}}</td>
                         
                          <td class="text-right" style="background-color: #f5f5f5;">
                             <a  href="<?= url('/')."/device_manager/"; ?>{{App\Helpers\Helper::crypt(1,$device->id)}}" class="btn btn-sm btn-primary">Device Manager</a>
                            
                            <a data-toggle="tooltip" title="Vehicle Info" href="<?= url('/')."/add-vehicle/"; ?>{{App\Helpers\Helper::crypt(1,$device->id)}}" class="btn btn-link btn-danger btn-just-icon edit"><i class="material-icons">local_car_wash</i></a>
                            <a  data-toggle="tooltip" title="Calibration Info" href="<?= url('/')."/vehicle-calibration/"; ?>{{App\Helpers\Helper::crypt(1,$device->id)}}" class="btn btn-link btn-info btn-just-icon remove"><i class="material-icons">view_column</i></a>
                          </td>
                        </tr>
                        <?php $id++; ?>  
                        @endforeach
                        @else
                           <tr><td colspan="9" align="center">No Device Exist.</td></tr>
                        @endif
                       
                       
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
      
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')

       @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif

                      <script>
 
    $(document).ready(function() {
 
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables').DataTable();

      $('#datatables tfoot th').each( function () {
        var title = $(this).text();
        if(title == "S.No"){
           title = "Serial";
        }
        if(title == "Partner"){
            var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                .appendTo( $(this).empty() )
                .on( 'change', function () {

                    var val = $(this).val();

                    table.column( 3 ) //Only the first column
                        .search( val ? '^'+$(this).val()+'$' : val, true, false )
                        .draw();
                } );

                table.column( 3 ).data().unique().sort().each( function ( d, j ) {
                if(d != '-'){
                  select.append( '<option value="'+d+'">'+d+'</option>' );
                }else{
                  select.append( '<option value="'+d+'">Nil</option>' );
                }
            } );
        }else if(title == "Type"){
            var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                .appendTo( $(this).empty() )
                .on( 'change', function () {

                    var val = $(this).val();

                    table.column( 4 ) //Only the first column
                        .search( val ? '^'+$(this).val()+'$' : val, true, false )
                        .draw();
                });

                table.column( 4 ).data().unique().sort().each( function ( d, j ) {

                  select.append( '<option value="'+d+'">'+d+'</option>' );
                
                });

        }else if(title == "Status"){
            var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                .appendTo( $(this).empty() )
                .on( 'change', function () {

                    var val = $(this).val();

                    table.column( 5 ) //Only the first column
                        .search( val ? '^'+$(this).val()+'$' : val, true, false )
                        .draw();
                });

                table.column( 5 ).data().unique().sort().each( function ( d, j ) {

                  select.append( '<option value="'+d+'">'+d+'</option>' );
                
                });
                
        }else{
           $(this).html( '<input class="'+title+'_tp" type="text" placeholder=" '+title+'" />' );
        }
      });

      table.columns([1,2,3,4,5]).every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
          });
      });

/*      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });*/
    });
  </script>