 
               <style type="text/css">
    .form-control{
      margin-top: 15px;
    }
    .bmd-form-group .form-control, .bmd-form-group label, .bmd-form-group input {
    line-height: 1.1;
}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    /* width: 220px; */

}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
     width: 100%; 
}
select.form-control:not([size]):not([multiple]) {
    height: calc(2.4375rem + 2px);
    height: 25px;
    margin: auto;
}
  </style>
  <style type="text/css">
    .dataTable>thead>tr>th, .dataTable>tbody>tr>th, .dataTable>tfoot>tr>th, .dataTable>thead>tr>td, .dataTable>tbody>tr>td, .dataTable>tfoot>tr>td {
     padding: 12px 8px !important; 
}
    .table-heading-font{
        font-size: 13px !important;
    }
    tfoot {
     display: table-header-group !important;
    }
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
  </style>
   <div class="row" >
    <div class="col-sm-6" >
    <h3>Device log - <?php echo $device_id; ?></h3>
  </div>
  <div class="col-md-6" align="right">
              <!-- <button class="btn btn-primary btn-sm" align="right" onclick="install();">Install New App</button> -->
            </div>
          </div>
  
          <div class="card-body">
                          <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                          </div>
                
                  <div class="material-datatables">
                     <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
              
                <thead>
                    <tr>
                      <th class="table-heading-font" style="width: 8%">Sl.No</th>
                      <th class="table-heading-font">log type</th>
                      <th class="table-heading-font">log description</th>
                      <th class="table-heading-font">Created by</th>
                      <th class="table-heading-font">Date</th>
                    </tr>
                  </thead>
                  <tfoot>
                     <tr>
                      <th class="table-heading-font" style="width: 8%">Sl.No</th>
                      <th class="table-heading-font">log type</th>
                      <th class="table-heading-font">log description</th>
                      <th class="table-heading-font">Created by</th>
                      <th class="table-heading-font">Date</th>
                    </tr>
                  </tfoot>
                  <tbody>

                  <?php 
                  $i=1;
                  foreach ($log_details as $key => $value) {
                    
                     ?>
                     <tr>
                       <td class="table-heading-font" style="background-color: #f5f5f5;"><?php echo $i;?></td>
                       <td style="background-color: #ffffff;" class="table-heading-font"><?php echo $value->log_type;?></td>
                       
                       <td class="table-heading-font" style="background-color: #f5f5f5;"><?php echo $value->log_desc;?></td>
                       <td style="background-color: #ffffff;" class="table-heading-font"><?php  echo $value->uname;?></td>
                       <td class="table-heading-font" style="background-color: #f5f5f5;"><?php  echo $value->ad_time;?></td>
                     </tr>

  
<?php 
$i++;
} ?>
</tbody>
</table>
</div>
</div>
<script>
    $(document).ready(function() {
 
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables').DataTable();

      $('#datatables tfoot th').each( function () {
        var title = $(this).text();
        if(title == "S.No"){
           title = "Serial";
        }
        if(title == "Partner"){
            var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                .appendTo( $(this).empty() )
                .on( 'change', function () {

                    var val = $(this).val();

                    table.column( 3 ) //Only the first column
                        .search( val ? '^'+$(this).val()+'$' : val, true, false )
                        .draw();
                } );

                table.column( 3 ).data().unique().sort().each( function ( d, j ) {
                if(d != '-'){
                  select.append( '<option value="'+d+'">'+d+'</option>' );
                }else{
                  select.append( '<option value="'+d+'">Nil</option>' );
                }
            } );
        }else if(title == "Type"){
            var select = $('<select class="deep"><option value="">'+title+'</option></select>')
                .appendTo( $(this).empty() )
                .on( 'change', function () {

                    var val = $(this).val();

                    table.column( 4 ) //Only the first column
                        .search( val ? '^'+$(this).val()+'$' : val, true, false )
                        .draw();
                });

                table.column( 4 ).data().unique().sort().each( function ( d, j ) {

                  select.append( '<option value="'+d+'">'+d+'</option>' );
                
                });
        }else{
           $(this).html( '<input class="'+title+'_tp" type="text" placeholder=" '+title+'" />' );
        }
      });

      table.columns([1,2,3,4,5]).every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
          });
      });

/*      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });*/
    });
  </script>
  </script>

