  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <style type="text/css">
     .dataTable > thead > tr > th[class*=sort]:after{
    display:none;
}
.dataTable > thead > tr > th[class*=sort]:before{
    display:none;
}

    .table-heading-font{
        font-size: 13px !important;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
  </style>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
                    <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
            <!--<div class="col-md-12" align="right">-->
              <!--<a href="{{url('/')}}/add-device" title="Add Device" class="btn btn-primary btn-sm">Add</a>-->
            <!--</div>-->
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assignment</i>
                  </div>
                  <h4 class="card-title">Device Pending List</h4>
                </div>
                <div class="card-body">
                  <div class="toolbar">
                    <!--        Here you can write extra buttons/actions for the toolbar              -->
                  </div>
                  <div class="material-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                      <thead>
                        <tr>
                          <th></th>
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Device Id</th>
                          <th class="table-heading-font">Total Files</th>
                         
                          <th class="table-heading-font none">Files</th>
                         
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th></th>
                          <th class="table-heading-font">S.No</th>
                          <th class="table-heading-font">Device Id</th>
                          <th class="table-heading-font">Total</th>
                      
                          <th class="table-heading-font none">Files</th>
                      
                        </tr>
                      </tfoot>
                      <tbody>
                        @if (!empty($device_pending))
                        <?php $id = 1; ?>
                        @foreach ($device_pending as $device)  
                        <?php
                        $fC  = explode(';',$device->file_list);
                        $fCC = count($fC);
                        ?>
                        <tr>
                          <td></td>
                          <td class="table-heading-font" style="background-color: #f5f5f5;">{{$id}}</td>
                          <td style="background-color: #ffffff;" class="table-heading-font">{{$device->device_id}}</td>
                          
                          <td style="background-color: #f5f5f5;" class="table-heading-font"><?= count($fC) ?></td>
                        
                           <td style="background-color: #ffffff;" class="table-heading-font none">
                            <form id="frm<?=$device->id?>">
                              <!-- <input type="hidden" name="devid" id="devid" value="<?=$device->id?>"> -->
                               <?php for($fi = 0; $fi<count($fC); $fi++){ ?>
                                <input type="checkbox" name="file[]" value="<?=$fC[$fi]?>" checked /> <?=$fC[$fi]?>
                               <?php } ?>
                              <input type="button" onclick="submitData('<?=$device->id?>')" class="btn btn-primary btn-sm" value="Resend" name="Resend"> 
                              <input type="button" onclick="submitData_delete('<?=$device->id?>')" class="btn btn-primary btn-sm" value="Delete" name="Delete"> 

                              </form>   
                           </td>
                        </tr>
                        <?php $id++; ?>  
                        @endforeach
                        @else
                           <tr><td colspan="9" align="center">No Device Exist.</td></tr>
                        @endif
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
      
    </div>
  </div>
   
  <!--   Core JS Files   -->
 @include('default.footer')
 <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

/*      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });*/
    });
  </script>
  <script>
  function callshowAct(div)
  {
    $(".clsfileTr").fadeOut();
    $(div).fadeIn(500);
  }

  function submitData_delete(id)
  {
    var str = $("#frm"+id);
    var val = [];
    $(':checkbox:checked').each(function(i){
       val[i] = $(this).val();
    });
    if(val ==""){
              Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Please select file name!',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
    } else {
      event.preventDefault();
       Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#eb262d',
                cancelButtonColor: '#eb262d',
                confirmButtonText: 'Yes, Delete it!'
            }).then((result) => {
              if(result.value){
                         $.ajax({
                      type:'POST',
                      url: '{{url('/')}}/api-device-pending_delete',
                      data: { 'did': id, 'files': val },
                      success: function (data) {
                        location.reload();
                      }
                    });
              }
            })
     }       
  }

  function submitData(id)
  {
    var str = $("#frm"+id);
    var val = [];
    $(':checkbox:checked').each(function(i){
       val[i] = $(this).val();
    });
    if(val ==""){
              Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Please select file name!',
            confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
    } else {
      event.preventDefault();
       Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#eb262d',
                cancelButtonColor: '#eb262d',
                confirmButtonText: 'Yes, Resend it!'
            }).then((result) => {
              if(result.value){
                         $.ajax({
                      type:'POST',
                      url: '{{url('/')}}/api-device-pending',
                      data: { 'did': id, 'files': val },
                      success: function (data) {
                        location.reload();
                      }
                    });
              }
            })
     }
  }

  function validateData(frmId)
  {
    frmId += " > input";
    //alert(frmId);
    notC = 0;
    chk = 0;
    $(frmId).each(function() {      
          if($(this).attr('type') == "checkbox") 
          {
             if($(this).is(":checked"))
             {
                //alert('ck');
                chk++;
             }
             else
             {
                //alert('nck');
                notC++;
             }
          }
      });file_list
      //alert(notC);
      if(notC >= 1)
      {
        if(confirm("Do you want to delete unchecked (" + notC + ") files ?"))
        {
          return true;
        }
        else
        {
          return false;
        }
      }
      return true;
  }
</script>