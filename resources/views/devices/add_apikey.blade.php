  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">

            <div class="col-md-12" align="right">
              <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>

            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">ADD API Key</h4>
                  </div>
                </div>
                <div class="card-body">
                  <form method="post" action="{{url('/')}}/save_apikey" class="form-horizontal">
                  <input type="hidden" value="{{$form_token}}" name="form_token">
                  
                    <div class="row">

                      
                      <div class="col-sm-4">
                        <div class="form-group ">
                          <label class="col-form-label">Api Key</label>
                          <br>
                          <input type="text" class="form-control" value="{{$api_token}}" name="apikey" id="apikey" placeholder="Enter API Key">
                        </div>
                      </div> 
                    <!-- </div>
                    <div class="row"> -->
                     
                      <div class="col-sm-4">
                        <div class="form-group">
                           <label class="col-form-label">Used by</label>
                           <br>
                          <input type="text" value="" name="name" id="name" class="form-control" placeholder="Enter Name">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      
                      <div class="col-sm-4">
                        <div class="form-group bmd-form-group">
                          <label class="col-form-label">Expiry Date</label>
                          <br>
                         
                         
                         <input type="text" value="" name="exdat" id="exdat" class="form-control datepicker" >
                        </div>
                      </div>
                   <!--  </div>
        
                  
  
                    <div class="row"> -->
                     
                      <div class="col-sm-4 checkbox-radios">
                  
                        <div class="form-check">
                           <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                           <br>
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="1"  checked > Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0" > In Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </div>
                    </div>
                    
                    

            <div class="col-md-12" id="load" style="display: none;" align="center">
                <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>
            </div>
            
            
          </div>
        </div>
      </div>
    </form> 
  </div>
</div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
 <script type="text/javascript">
    $(document).ready(function() {
    md.initFormExtendedDatetimepickers();
      if ($('.slider').length != 0) {
        md.initSliders();
      }
      });
 </script>
   @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif

                      <script>