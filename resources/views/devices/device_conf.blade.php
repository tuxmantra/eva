<style type="text/css">
   .form-control{
   margin-top: 15px;
   }
   .bmd-form-group .form-control, .bmd-form-group label, .bmd-form-group input {
   line-height: 1.1;
   }
   .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
   /* width: 220px; */
   }
   .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
   width: 100%; 
   }
   select.form-control:not([size]):not([multiple]) {
   height: calc(2.4375rem + 2px);
   height: 25px;
   margin: auto;
   }
   hr {
   margin-top: 1rem;
   margin-bottom: 1rem;
   border: 0;
   border-top: 3px solid rgba(0, 0, 0, 0.1);
   }
</style>
<style type="text/css">
   .dataTable>thead>tr>th, .dataTable>tbody>tr>th, .dataTable>tfoot>tr>th, .dataTable>thead>tr>td, .dataTable>tbody>tr>td, .dataTable>tfoot>tr>td {
   padding: 12px 8px !important; 
   }
   .table-heading-font{
   font-size: 13px !important;
   }
   tfoot {
   display: table-header-group !important;
   }
   tfoot input {
   width: 100%;
   }
   .Serial_tp {
   display: none;
   }
   .Actions_tp {
   display: none;
   }
   .deep{
   cursor: pointer;
   padding: 2%;
   border-radius: 2px;
   background: #fff;
   color: grey;
   position: relative;
   }
   .bmd-label-static {
   top: -16px !important;
   }
   .inputs{
  border: 0;
    width: 45px;
    height: 25px;
    text-align: center;
    border-bottom: 0px;
     background-color: #FFFF;
     background-image: linear-gradient(to top, #f3cacb 2px, rgba(156, 39, 176, 0) 1px), linear-gradient(to top, #d2d2d2 1px, rgba(210, 210, 210, 0) 1px);
}
</style>
<div class="row" >
   <div class="col-sm-6" >
      <h3>Update Status - <?php echo $device_id; ?></h3>
   </div>
   <div class="col-md-6" align="right">
      <!-- <button class="btn btn-primary btn-sm" align="right" onclick="install();">Install New App</button> -->
   </div>
</div>
<div class="card-body">
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_name">App Update</label>
            <input class="form-control" id="App_name" type="text" value="<?php if($app_Update[0]->num > 0){ echo 1;}else{echo 0;} ?>" readonly>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_name">App Update Count</label>
            <input class="form-control" id="App_name" type="text" value="{{$app_Update[0]->num}}" readonly>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_name">Firmware Update</label>
            <input class="form-control" id="App_name" type="text" value="<?php if($firm_Update[0]->num > 0){ echo 1;}else{echo 0;} ?> " readonly>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_name">Firmware Update count</label>
            <input class="form-control" id="App_name" type="text" value="{{$firm_Update[0]->num}} " readonly>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_name">Command Update</label>
            <input class="form-control" id="App_name" type="text" value="{{$device_data[0]->dev_cmd_status}}" readonly>
         </div>
      </div>
   </div>
   <?php $i='1'; 
      if(!empty($bt_dev)){
      
      foreach($bt_dev as $key => $value) {  ?>
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_name">Blutooth device {{$i}}</label>
            <input class="form-control" id="App_name" type="text" value="{{$value->update_status}}" readonly>
         </div>
      </div>
      <div class="col-md-6">
      </div>
   </div>
   <?php $i++; } }else{?>
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_name">Blutooth device 1</label>
            <input class="form-control" id="App_name" type="text" value="0" readonly>
         </div>
      </div>
      <div class="col-md-6">
      </div>
   </div>
   <?php } ?>
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_name">Config Update</label>
            <input class="form-control" id="App_name" type="text" value="{{$device_data[0]->dactval}}" readonly>
         </div>
      </div>
   </div>
   <hr>
   <div class="toolbar">
      <!--        Here you can write extra buttons/actions for the toolbar              -->
   </div>
   <?php  if(!empty($app_details)) { ?>
   <h4> App Update Status</h4>
   <?php foreach ($app_details as $key => $value) { ?>
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_name">App Name</label>
            <input class="form-control" id="App_name" type="text" value="{{$value->app_name}}" readonly>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_ver">App Ver</label>
            <input class="form-control" id="App_ver" type="text" value="{{$value->lat_ver}}" readonly>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_pth">App Path</label>
            <input class="form-control" id="App_pth" type="text" value="{{$value->lat_ver_path}}" readonly>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_file">App File</label>
            <input class="form-control" id="App_file" type="text" value="{{$value->lat_ver_filename}}" readonly>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_file">App Update Status</label>
            <input class="form-control" id="App_file" type="text" value="{{$value->upd_status}}" readonly>
         </div>
      </div>
   </div>
   <hr>
   <?php } }?>
   <?php 
      if(!empty($firm_details)){
      ?>
   <h4> Firmware Update Status</h4>
   <?php foreach ($firm_details as $key => $value) {
      ?>
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_name"> Name</label>
            <input class="form-control" id="App_name" type="text" value="{{$value->firm_name}}" readonly>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_ver"> Ver</label>
            <input class="form-control" id="App_ver" type="text" value="{{$value->lat_ver}}" readonly>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_pth"> Path</label>
            <input class="form-control" id="App_pth" type="text" value="{{$value->lat_ver_path}}" readonly>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_file"> File</label>
            <input class="form-control" id="App_file" type="text" value="{{$value->lat_ver_filename}}" readonly>
         </div>
      </div>
      <div class="col-md-4">
         <div class="form-group">
            <label for="App_file"> Update Status</label>
            <input class="form-control" id="App_file" type="text" value="{{$value->upd_status}}" readonly>
         </div>
      </div>
   </div>
   <hr>
   <?php } }  if($device_data[0]->dev_cmd_status=='1'){?>
   <div class="row">
      <div class="col-md-12">
         <h4> Commands</h4>
         <?php if( $device_data[0]->devtype==2)
            { ?>
         <div class="form-group ">
            <?php $cmd=json_decode($device_data[0]->dev_cmds,TRUE);
               // print_r($cmd); die;
               if(!empty($cmd)){
                 $keys = array_keys($cmd);
                 $values = array_values($cmd);
               
                 ?>
            <textarea rows="4" cols="50" class="form-control cmdvalue cmdkey" type="" name="" placeholder="Enter Commands" >{{$values[0]}}</textarea>
         </div>
         <?php }else{?>
         <textarea rows="4" cols="50" class="form-control cmdvalue cmdkey" type="" name="" placeholder="Enter Commands" ></textarea>
      </div>
      <?php   }
         }else{ ?>
      <table id="datatables_row" class="table table-striped  table-hover dataTable dtr-inline" cellspacing="0" width="100%" style="width: 100%;" role="grid" aria-describedby="datatables_info">
         <tbody>
            <tr>
               <th class="table-heading-font sorting_asc" style=" text-align: center; background-color: #eeeeee;">Key</th>
               <th  class="table-heading-font sorting_asc"style=" text-align: center; background-color: #eeeeee;">Value</th>
            </tr>
            <!--  </tbody>
               <tbody> -->
            <?php $cmd=json_decode($device_data[0]->dev_cmds,TRUE);
               // print_r($cmd); die;
               if(!empty($cmd)){
                 $keys = array_keys($cmd);
                 $values = array_values($cmd);
               
                for($i=0;$i<count($values);$i++) {?>
            <tr>
               <td style="background-color: #ffffff; text-align: center;width: 30%">
                  <div class="form-group "><input class="form-control cmdkey" style="text-align: center;" name="key_1" id="key_1" value="{{$keys[$i]}}" disabled></div>
               </td>
               <td style="background-color: #ffffff; text-align: center; width: 70%">
                  <div class="form-group"><input class="form-control cmdvalue" style="text-align: center;" name="value" id="value_1" value="{{$values[$i]}}" disabled /></div>
               </td>
            </tr>
            <?php } } else{
               ?>
            <tr>
               <td style="background-color: #ffffff; text-align: center;width: 30%">
                  <div class="form-group "><input class="form-control cmdkey" style="text-align: center;" name="key_1" id="key_1" value=""></div>
               </td>
               <td style="background-color: #ffffff; text-align: center; width: 70%">
                  <div class="form-group"><input class="form-control cmdvalue" style="text-align: center;" name="value" id="value_1" value="" /></div>
               </td>
            </tr>
            <?php }?>
         </tbody>
      </table>
      <?php }?>
      <!--  <textarea class="form-control" id="cmd" name="cmd" rows="4" cols="50">{{$device_data[0]->dev_cmds}}</textarea> -->
   </div>
   <?php } ?>
   <!-- bt_devdetails start -->

   <?php 
if(!empty($bt_dev)){
   if($bt_dev[0]->update_status){
    if(!empty($bt_details)){ ?>
   <div class="row classes">
      <div class="col-md-3" style="
         margin-left: 15px;
         " >
         <label class="form-lable" style="
            margin-bottom: 0px;
            margin-top: 27px;
            ">DEVICE MAKE : </label>
             <input style="width: 100%;" type="text" class="input form-control" id="value{{$i}}"  value="{{$bt_details[0]->bt_dev_make}}" disabled />        
      </div>
      <div class="col-md-3" >
         <label class="form-lable" style="
            margin-bottom: 0px;
            margin-top: 27px;
            ">DEVICE TYPE :</label>
            <input style="width: 100%;" type="text" class="input form-control" id="value{{$i}}"  value="{{$bt_details[0]->bt_dev_type}}" disabled />            
        
      </div>
      <div class="col-md-5" style="
         /* margin: 0px; */
         margin-top: 21px;
         margin-left: 0px;
         background-color: #f3ebec8c;
         ">
         <?php $pieces = explode(":", $bt_details[0]->mac_id); ?>
         <label class="form-lable">MAC ID : </label><br>
         <input type="text" class=" inputs" id="mac_id1" maxlength="2" value="{{$pieces[0]}}" disabled>: 
         <input type="text" class=" inputs" id="mac_id2" maxlength="2" value="{{$pieces[1]}}" disabled>:
         <input type="text" class=" inputs" id="mac_id3" maxlength="2" value="{{$pieces[2]}}" disabled>:
         <input type="text" class=" inputs" id="mac_id4" maxlength="2" value="{{$pieces[3]}}" disabled>:
         <input type="text" class=" inputs" id="mac_id5" maxlength="2" value="{{$pieces[4]}}" disabled>:
         <input type="text" class=" inputs" id="mac_id6" maxlength="2" value="{{$pieces[5]}}" disabled>
      </div>
      <?php if($bt_details[0]->bt_dev_make=="Numato"){ ?>
      <div class="col-md-12" style="margin-bottom: 30px; margin-top: 27px;" id="place">
         <div class="col-md-12">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
               <thread>
                  <tr>
                     <th class="table-heading-font" >Pin No</th>
                     <th class="table-heading-font" >Data Type</th>
                     <th class="table-heading-font">Operator</th>
                     <th class="table-heading-font">Value</th>
                     <th class="table-heading-font">Name</th>
                  </tr>
               </thread>
               <tr>
                  <?php 
                     if($bt_details[0]->bt_dev_type=="2 relay module"){ 
                         $count=2;
                     }else{
                        $count=8;
                     }
                     
                     for($i=($count-1);$i>=0;$i--){
                     $p='P'.$i.'type';
                     $val='P'.$i.'val';
                     $name='P'.$i.'name';
                     $op='P'.$i.'op';
                      ?>
                  <td style="background-color: #f5f5f5; width: 8%" >P{{$i}}</td>
                  <td style=" width:30%" > 
                    <?php if($bt_details[0]->{$p} == 'analog_input'){echo "Analog Input";}
                          if($bt_details[0]->{$p} == 'digital_input'){echo "Digital Input";}
                          if($bt_details[0]->{$p} == 'digital_output'){echo "Digital Output";}
                     ?>
                  </td>
                  <td style=" background-color: #f5b5b511; width:10%">
                    <input style="width: 100%;" type="text" class="input form-control" id="value{{$i}}"  value="{{$bt_details[0]->{$op} }}" disabled />
                     
                  </td>
                  <td style="width: 10%;"><input style="width: 100%;" type="text" class="input form-control" id="value{{$i}}"  value="{{$bt_details[0]->{$val} }}" disabled/></td>
                  <td style="background-color: #f5b5b511; width: 30%;"><input style="width: 100%;" type=text class="input form-control" id="name{{$i}}" value="{{$bt_details[0]->{$name} }}" disabled />
                  </td>
               </tr>
               <?php } ?>
            </table>
         </div>
      </div>
      <?php } ?>
     
   </div>
 <?php 
   }
 }
 } ?>
   <!-- bt_devdetails ends -->
</div>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<script>
  $('select').selectpicker();
   $(document).ready(function() {
   
     $('#datatables').DataTable({
       "pagingType": "full_numbers",
       "lengthMenu": [
         [10, 25, 50, -1],
         [10, 25, 50, "All"]
       ],
       responsive: true,
       language: {
         search: "_INPUT_",
         searchvalue: "Search records",
       }
     });
   
     var table = $('#datatables').DataTable();
   
     $('#datatables tfoot th').each( function () {
       var title = $(this).text();
       if(title == "S.No"){
          title = "Serial";
       }
       if(title == "Partner"){
           var select = $('<select class="deep"><option value="">'+title+'</option></select>')
               .appendTo( $(this).empty() )
               .on( 'change', function () {
   
                   var val = $(this).val();
   
                   table.column( 3 ) //Only the first column
                       .search( val ? '^'+$(this).val()+'$' : val, true, false )
                       .draw();
               } );
   
               table.column( 3 ).data().unique().sort().each( function ( d, j ) {
               if(d != '-'){
                 select.append( '<option value="'+d+'">'+d+'</option>' );
               }else{
                 select.append( '<option value="'+d+'">Nil</option>' );
               }
           } );
       }else if(title == "Type"){
           var select = $('<select class="deep"><option value="">'+title+'</option></select>')
               .appendTo( $(this).empty() )
               .on( 'change', function () {
   
                   var val = $(this).val();
   
                   table.column( 4 ) //Only the first column
                       .search( val ? '^'+$(this).val()+'$' : val, true, false )
                       .draw();
               });
   
               table.column( 4 ).data().unique().sort().each( function ( d, j ) {
   
                 select.append( '<option value="'+d+'">'+d+'</option>' );
               
               });
       }else{
          $(this).html( '<input class="'+title+'_tp" type="text" value=" '+title+'" />' );
       }
     });
   
     table.columns([1,2,3,4,5]).every( function () {
       var that = this;
   
       $( 'input', this.footer() ).on( 'keyup change', function () {
           if ( that.search() !== this.value ) {
               that
                   .search( this.value )
                   .draw();
           }
         });
     });
   
  
   });
</script>
