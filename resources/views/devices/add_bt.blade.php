 
               <style type="text/css">
    .form-control{
      margin-top: 15px;
    }
    .bmd-form-group .form-control, .bmd-form-group label, .bmd-form-group input {
    line-height: 1.1;
}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    /* width: 220px; */

}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
     width: 100%; 
}
  </style>
   <div class="row" >
    <div class="col-sm-6" >
   
  </div>
  <div class="col-md-6" align="right">
              <button class="btn btn-primary btn-sm" align="right" onclick="add_bt_dev();">Add Blutooth Device</button>
            </div>
          </div>
  
  
                <div class="row">
                  <?php foreach ($app_details as $key => $value) { 

                     ?>
  <div class="col-md-4 ">
    <div class="card card-pricing bg-primary"><div class="card-body ">
        <div class="card-icon">
            <i class="material-icons">business</i>
        </div>
        <h3 class="card-title">{{$value->bt_dev_make}} </h3>
        <p class="card-description">
            MAC ID {{$value->mac_id}}
        </p>
       
        <a href="#" onclick="install({{$value->id}});" class="btn btn-rose btn-round">Update</a>
        <a href="#pablo" onclick="remove({{$value->id}});" class="btn btn-rose btn-round">Remove</a>
   
        </div>
      }
    </div>
  </div>
<?php } ?>
</div>
<script>
  function install(appid){
    document.getElementById("load_data4").style.display = "none";
    $.get('{{url('/')}}/update_bt_device/{{App\Helpers\Helper::crypt(1,$id)}}', { appid: appid }).done(function (data) {
      //alert("ok");
          $('#load_data4').html(data);
          document.getElementById("load_data4").style.display = "block";
         
    });
  }

   function remove(appid){
   
    $.get('{{url('/')}}/remove_bt_device/{{App\Helpers\Helper::crypt(1,$id)}}', { appid: appid }).done(function (data) {
      //alert("ok");
             Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   'Device Removed',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
         
    });
  }


</script>
