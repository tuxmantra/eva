<style type="text/css">
   .form-control{
   margin-top: 15px;
   }
   .bmd-form-group .form-control, .bmd-form-group label, .bmd-form-group input {
   line-height: 1.1;
   }
   .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
   /* width: 220px; */
   }
   .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
   width: 100%; 
   }
   .card .card-header-primary .card-icon,
   .card .card-header-primary .card-text,
   .card .card-header-primary:not(.card-header-icon):not(.card-header-text),
   .card.bg-primary,
   .card.card-rotate.bg-primary .front,
   .card.card-rotate.bg-primary .back {
   background: linear-gradient(60deg, #ffffff, #ffffff);
   }
   .card {
   box-shadow: 0 1px 16px 0 rgba(0, 0, 0, 0.14);
   }
   table.dataTable {
   clear: both;
   margin-top: 6px !important;
   margin-bottom: 6px !important;
   max-width: none !important;
   border-collapse: separate !important;
   border-spacing: 0px;
   -webkit-border-vertical-spacing: 0px;
   -webkit-border-horizontal-spacing: 17px;
   }
</style>
<div class="row" >
   <div class="col-sm-12" style="margin-top: 20px;">
      <ul class="nav nav-pills nav-pills-warning" role="tablist">
         <?php $active='';
            $active1='active';
            $disable='';
            if( $device_data[0]->devtype==2)
            {
            $active='active';
            $active1='disabled disabledcolor';
            $disable='disabled';
            
            }
            
            ?>
         <li class="nav-item">
            <a class="nav-link {{$active1}} "  data-toggle="tab" href="#link22" role="tablist"{{$disable}}>
            Firmware
            </a>
         </li>
         <li class="nav-item">
            <a class="nav-link {{$active}}"  data-toggle="tab" href="#link11" role="tablist">
            Command
            </a>
         </li>
      </ul>
   </div>
   <div class="col-md-12">
      <div class="tab-content tab-space">
         <div class="tab-pane {{$active}}" id="link11">
            <div class="card-body" >
               <div class="row">
                  <div class="col-md-12">
                     <?php if( $device_data[0]->devtype==2)
                        { ?>
                     <div class="form-group ">
                        <?php 
                           if(!empty($device_data[0]->dev_cmds))
                           {
                             
                             ?>
                        <textarea rows="4" cols="50" class="form-control cmdvalue cmdkey" type="" name="" placeholder="Enter Commands" >{{$device_data[0]->dev_cmds}}</textarea>
                     </div>
                     <?php }else{?>
                     <textarea rows="4" cols="50" class="form-control cmdvalue cmdkey" type="" name="" placeholder="Enter Commands" ></textarea>
                  </div>
                  <?php   }
                     }else{ ?>
                  <table id="datatables_row" class="table table-striped  table-hover dataTable dtr-inline" cellspacing="0" width="100%" style="width: 100%;" role="grid" aria-describedby="datatables_info">
                     <tbody>
                        <tr>
                           <th class="table-heading-font sorting_asc" style=" text-align: center; background-color: #eeeeee;">Key</th>
                           <th  class="table-heading-font sorting_asc"style=" text-align: center; background-color: #eeeeee;">Value</th>
                        </tr>
                        <!--  </tbody>
                           <tbody> -->
                        <?php $cmd=json_decode($device_data[0]->dev_cmds,TRUE);
                           // print_r($cmd); die;
                           if(!empty($cmd)){
                             $keys = array_keys($cmd);
                             $values = array_values($cmd);
                           
                            for($i=0;$i<count($values);$i++) {?>
                        <tr>
                           <td style="background-color: #ffffff; text-align: center;width: 30%">
                              <div class="form-group "><input class="form-control cmdkey" style="text-align: center;" name="key_1" id="key_1" value="{{$keys[$i]}}"></div>
                           </td>
                           <td style="background-color: #ffffff; text-align: center; width: 70%">
                              <div class="form-group"><input class="form-control cmdvalue" style="text-align: center;" name="value" id="value_1" value="{{$values[$i]}}" /></div>
                           </td>
                        </tr>
                        <?php } } else{
                           ?>
                        <tr>
                           <td style="background-color: #ffffff; text-align: center;width: 30%">
                              <div class="form-group "><input class="form-control cmdkey" style="text-align: center;" name="key_1" id="key_1" value=""></div>
                           </td>
                           <td style="background-color: #ffffff; text-align: center; width: 70%">
                              <div class="form-group"><input class="form-control cmdvalue" style="text-align: center;" name="value" id="value_1" value="" /></div>
                           </td>
                        </tr>
                        <?php }?>
                     </tbody>
                  </table>
                  <button class="btn-rose btn-sm btn" onclick="add_row()">Add Row</button>
                  <?php }?>
                  <button class="btn-rose btn-sm btn" onclick="save_cmd({{$device_data[0]->device_id}})" class="btn btn-rose btn-round">Save Command</button> 
                  <!--  <textarea class="form-control" id="cmd" name="cmd" rows="4" cols="50">{{$device_data[0]->dev_cmds}}</textarea> -->
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane {{$active1}} box-shadow" id="link22">
         <div class="col-md-12"  >
            <div class="col-md-6" align="right">
               <!-- <button class="btn btn-primary btn-sm" align="right" onclick="install();">Install New App</button> -->
            </div>
            <div class="row">
               <?php foreach ($app_details as $key => $value) {
                  ?>
               <div class="col-md-4 ">
                  <div class="card card-pricing bg-primary">
                     <div style="color: #eb262d;text-align: right;">
                        <a data-toggle="tooltip" title="Delete App" href="{{url('/')}}/delete_firm_device?appid={{$value->id}}&did={{App\Helpers\Helper::crypt(1,$id)}}" class="btn btn-link btn-danger btn-just-icon edit">
                           <i class="material-icons">delete</i>
                           <div class="ripple-container"></div>
                        </a>
                     </div>
                     <div class="card-body ">
                        <div class="card-icon">
                           <i class="material-icons">business</i>
                        </div>
                        <h3 class="card-title"><?php echo $value->firm_name; ?>  </h3>
                        <p class="card-description">
                           Version <?php if($value->ins_status==1){ echo $value->curr_run_ver;}else{ echo $value->lat_ver; } ?>
                        </p>
                        <?php if($value->ins_status==1){ ?>
                        <a href="#"onclick="install({{$value->firm_id}});" class="btn btn-rose btn-round">Update</a>
                        <a onclick="uninstall_app({{$value->id}})" class="btn btn-rose btn-round">Un-Install</a>
                        <?php  }else{ ?>
                        <a href="#pablo" class="btn btn-rose btn-round">Firmware is Not Installed</a>
                        <?php } ?>
                     </div>
                  </div>
               </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   function add_row(){
   
     $('#datatables_row > tbody:last ').append('  <tr><td style="background-color: #ffffff; text-align: center;"> <div class="form-group "><input class="form-control cmdkey" style="text-align: center;" name="key_1" id="key_1"/ value=""></div></td><td style="background-color: #ffffff; text-align: center;"> <div class="form-group"><input class="form-control cmdvalue" style="text-align: center;" name="value" id="value_1" value="" /></div></td> </tr>');
   }
   
   
   
     function install(appid){
       document.getElementById("load_data3").style.display = "none";
       $.get('{{url('/')}}/install_firm_version/{{App\Helpers\Helper::crypt(1,$id)}}', { appid: appid }).done(function (data) {
         //alert("ok");
             $('#load_data3').html(data);
             document.getElementById("load_data3").style.display = "block";
            
       });
     }
   
   function uninstall_app(id){
   
       $.get('{{url('/')}}/uninstall_firm/{{App\Helpers\Helper::crypt(1,$id)}}', { id: id }).done(function (data) {
        location.reload();
            
       });
   
     }
   
   
</script>