@include('default.header')
@include('default.sidebar')
@include('default.submenu')
<?php  ?>
<style type="text/css">
  .nav-pills .nav-item .nav-link {
  line-height: 24px;
  text-transform: uppercase;
  font-size: 12px;
  font-weight: 500;
  min-width: 100px;
  text-align: center;
  color: #555;
  transition: all .3s;
  border-radius: 30px;
  padding: 10px 15px;
  background-color: azure;
  /* border-color: aquamarine; */
  }
  .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
  width: 100%; 
  }
</style>
<!-- End Navbar -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12" align="right">
        <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
      </div>
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header card-header-rose card-header-text">
            <div class="card-text">
              <h4 class="card-title">Update App Version</h4>
            </div>
          </div>
          <div class="card-body ">
            <form method="post" enctype="multipart/form-data" action="{{url('/')}}/app-ver-save?appid={{$appid}}"  id="save_app" name="save_app" class="form-horizontal" >
              <input type="hidden" value="{{$form_token}}" name="form_token">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group bmd-form-group">
                    <label class="col-form-label" style="font-size: 14px;">App Name</label>
                    <br>
                    <input type="text" name="vname" id="vname" class="form-control" placeholder="Enter App Name" value="{{$app_details[0]->app_name}}" readonly>
                  </div>
                </div>
                <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">MD5</label>
                              <br>
                                <input type="text"  class="form-control" placeholder="Enter MD5 Key" value="{{$app_details[0]->md5}}" readonly>
                            </div>
                          </div>
                         
                          <div class="col-md-4">
                             
                            <label class="col-form-label">Installed Status</label>
                            <br>
                            <div class=" row">
                              <div class="col-md-3">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="insstatus" value="1" <?php if($app_details[0]->ins_status==1){ echo "checked";} ?>> Active
                                  <span class="circle">
                                    <span class="check"></span>
                                  </span>
                                </label>
                              </div>
                            </div>
                             <div class="col-md-5">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="radio" name="insstatus" value="0" <?php if($app_details[0]->ins_status==0){ echo "checked";} ?>>Deactive
                                  <span class="circle">
                                    <span class="check"></span>
                                  </span>
                                </label>
                             
                            </div>
                          </div>
                          </div>
                        </div>
                    </div>
                     <div class="row">
                       <!-- <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Installed By</label>
                              <br>
                              <input type="text"  class="form-control" placeholder="Installed By" readonly>

                            </div>
                          </div> -->
                          <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                               
                                <label class="col-form-label" style="font-size: 14px;">Current running version</label>
                                <br>
                                <input type="text"  class="form-control" placeholder="Current version" value="{{$app_details[0]->curr_run_ver}}"readonly>
                              </div>
                              <!-- <label class="col-form-label" style="font-size: 14px;">Fuel Type</label>
                              <br>
                              
                              <select class="selectpicker fuel" data-style="select-with-transition" title="Select Fuel Type" data-size="7" name="fuel" id="fuel" tabindex="-98">
                                    <option disabled="">Fuel Type</option>
                                    @if (!empty($fuel))
                                    @foreach ($fuel as $fu)
                                    <option <?php if(!empty($vehicledata)){ if($vehicledata->fuel_tpe == $fu->fuel_type_id){ ?> selected <?php }} ?> value="{{$fu->fuel_type_id}}">{{$fu->fuel_name}}</option>
                                    @endforeach
                                    @endif
                              </select> -->
                        
                           
                          </div>
                          <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Latest version</label>
                              <br>
                              
                                <input type="text" name="version" class="form-control" placeholder="Latest version" value="{{$app_details[0]->lat_ver}}">
                        
                            </div>
                          </div>
                           <div class="col-md-4">
                            <div class=" bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Latest version file name</label>
                              <br>
                             
                              <input type="file" name="file" class="form-control" placeholder="Latest version file name" value="{{$app_details[0]->lat_ver_filename}}" >
                              <input type="hidden" name="file_p" class="form-control" placeholder="Latest version file name" value="{{$app_details[0]->lat_ver_filename}}" >
                               
                              <p>{{$app_details[0]->lat_ver_filename}}</p>
                              

                            </div>
                          </div>
                    </div>
                    <div class="row">
                       <div class="col-md-4">
                            <div class="form-group bmd-form-group">
                              <label class="col-form-label" style="font-size: 14px;">Latest version path</label>
                              <br>
                              
                               <input type="text" name="path"  class="form-control" placeholder="Latest version path" value="{{$app_details[0]->lat_ver_path}}">

                            </div>
                          </div>
                         
                         <div class="col-md-4">
                             
                      <label class="col-form-label">Update Status</label>
                      <br>
                      <div class=" row">
                        <div class="col-md-3">
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="upstatus" value="1" <?php if($app_details[0]->upd_status==0){ echo "checked";}?> > Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                      </div>
                       <div class="col-md-5">
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="upstatus" value="0" <?php if($app_details[0]->upd_status==1){ echo "checked";}?>>Deactive
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                       
                      </div>
                    </div>
                    </div>
                          </div>
                    </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group bmd-form-group">
                    <?php 
                        $devarr =  explode(':', $app_details[0]->devid);
                        // print_r($devarr);
                    ?>
                    <label class="col-form-label" style="font-size: 14px;"> Select Device ID</label>
                    <br>
                    <select class="selectpicker devid" data-live-search="true" data-style="select-with-transition" title="Select Device ID" data-size="7" name="devid[]" id="devid[]" tabindex="-98" multiple >
                      <option value="-1">Select All</option>
                      @foreach ($device as $dev)
                      <option <?php if(in_array($dev->device_id, $devarr)){ echo "selected";} ?> value="{{$dev->device_id}}">{{$dev->device_id}} </option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="col-md-12" align="right">
        <button type="submit" class="btn btn-fill btn-rose">Save</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@include('default.footer')
@if (Session::has('message'))
<script>
  Swal.fire({
     type: 'info',
     title: 'info',
     text:   '<?= session('message') ?>',
      confirmButtonColor: '#eb262d',
     // footer: '<a href>Why do I have this issue?</a>'
   })
</script>
@endif
<script type="text/javascript">
  $(document).ready(function() {
  
    $("select").on("click", function(){      
  if ($(this).find(":selected").text() == "Select All"){
  if ($(this).attr("data-select") == "false")
    $(this).attr("data-select", "true").find("option").prop("selected", true);
  else
    $(this).attr("data-select", "false").find("option").prop("selected", false);
  }
  });
  
  
    var IsNull= '@Session["msg"]'!= null; 
    var str='<?=session('msg') ?>';
    if(str !=''){
       // var msg= ''; 
      Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: '<?=session('msg') ?>',
           confirmButtonColor: '#eb262d',
          // footer: '<a href>Why do I have this issue?</a>'
        })
      <?php
    session()->forget('msg');
    // session(['msg' => '']);
    ?>
    }
  
  
    // initialise Datetimepicker and Sliders
    md.initFormExtendedDatetimepickers();
    if ($('.slider').length != 0) {
      md.initSliders();
    }
  
  <?php if(!empty($vehicledata)){ ?>
  
    loadvehicle();
    
  <?php } ?>
  });
  
  function loadvehicle(){
  
       var mfd = $("#manufact option:selected").val();
       <?php if(!empty($vehicledata)){ ?>
       <?php $vehicledatamodel = ($vehicledata->model != '')? $vehicledata->model:""; ?>
       <?php }else{ ?>
       <?php $vehicledatamodel = ""; ?>
       <?php } ?>
       $.post('{{url('/')}}/getModelList', { mfd: mfd }).done(function (data) {
              $('#model').html(data).selectpicker('refresh');
              $('#model').val('<?= $vehicledatamodel ?>').selectpicker('refresh');
              loadvehicle1();
        });
  }
  
  function loadvehicle1(){
       var mfd   = $("#manufact option:selected").val();
       var model = $("#model option:selected").val();
       <?php if(!empty($vehicledata)){ ?>
       <?php $vehicledatavarient = ($vehicledata->varient != '')? $vehicledata->varient:""; ?>
       <?php }else{ ?>
       <?php $vehicledatavarient = ""; ?>
       <?php } ?>
  
       $.post('{{url('/')}}/getVarientList', { 'mfd': mfd, 'model': model }).done(function (data) {
              $('#variant').html(data).selectpicker('refresh');
              $('#variant').val('<?= $vehicledatavarient ?>').selectpicker('refresh');
       });
  }
</script>