@include('default.header')
@include('default.sidebar')
@include('default.submenu')
<?php  ?>
<style type="text/css">
  .nav-pills .nav-item .nav-link {
  line-height: 24px;
  text-transform: uppercase;
  font-size: 12px;
  font-weight: 500;
  min-width: 100px;
  text-align: center;
  color: #555;
  transition: all .3s;
  border-radius: 30px;
  padding: 10px 15px;
  background-color: azure;
  /* border-color: aquamarine; */
  }
  .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
  width: 100%; 
  }
  .card .card-header-primary .card-icon,
  .card .card-header-primary .card-text,
  .card .card-header-primary:not(.card-header-icon):not(.card-header-text),
  .card.bg-primary,
  .card.card-rotate.bg-primary .front,
  .card.card-rotate.bg-primary .back {
  background: linear-gradient(60deg, #ffffff, #ffffff);
  }
  .card {
  box-shadow: 0 1px 16px 0 rgba(0, 0, 0, 0.14);
  }
</style>
<!-- End Navbar -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12" align="right">
        <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
      </div>
      <div class="col-md-12">
        <div class="card ">
          <div class="card-header card-header-rose card-header-text">
            <div class="card-text">
              <h4 class="card-title">Install New Firmware</h4>
            </div>
          </div>
          <div class="card-body ">
            <form method="post" enctype="multipart/form-data" action="{{url('/')}}/firmware-save"  class="form-horizontal" >
              <input type="hidden" value="{{$form_token}}" name="form_token">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group bmd-form-group">
                    <label class="col-form-label" style="font-size: 14px;">Firmware Name</label>
                    <br>
                    <input type="text" name="vname" id="vname" class="form-control" placeholder="Enter Firmware Name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group bmd-form-group">
                    <label class="col-form-label" style="font-size: 14px;">Version</label>
                    <br>
                    <input type="text" name="version"  class="form-control" placeholder="Enter Version">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class=" bmd-form-group">
                    <label class="col-form-label" style="font-size: 14px;">Select File</label>
                    <br>
                    <input type="file" accept="application/apk" name="file" id="file" class="form-control">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group bmd-form-group">
                    <label class="col-form-label" style="font-size: 14px;">File Path</label>
                    <br>
                    <select class="selectpicker path" data-live-search="true" data-style="select-with-transition" title="Select path" data-size="7" name="path" id="path" tabindex="-98">
                      <option value="http://172.105.41.167/install_apps">http://172.105.41.167/install_apps</option>
                    </select>
                  </div>
                  <!-- <label class="col-form-label" style="font-size: 14px;">Fuel Type</label>
                    <br>
                    
                    <select class="selectpicker fuel" data-style="select-with-transition" title="Select Fuel Type" data-size="7" name="fuel" id="fuel" tabindex="-98">
                          <option disabled="">Fuel Type</option>
                          @if (!empty($fuel))
                          @foreach ($fuel as $fu)
                          <option <?php if(!empty($vehicledata)){ if($vehicledata->fuel_tpe == $fu->fuel_type_id){ ?> selected <?php }} ?> value="{{$fu->fuel_type_id}}">{{$fu->fuel_name}}</option>
                          @endforeach
                          @endif
                    </select> -->
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group bmd-form-group">
                    <label class="col-form-label" style="font-size: 14px;"> Select Device ID</label>
                    <br>
                    <select class="selectpicker devid" data-live-search="true" data-style="select-with-transition" title="Select Device ID" data-size="7" name="devid[]" id="devid[]" tabindex="-98" multiple >
                      <option value="-1">Select All</option>
                      @foreach ($device as $dev)
                      <option value="{{$dev->device_id}}">{{$dev->device_id}} </option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="col-md-12" align="right">
        <button type="submit" class="btn btn-fill btn-rose">Save</button>
        </div>
        </form>
      </div>
      <div class="col-md-12">
        <div class="">
          <div class="card-body ">
            <div class="row" >
              <div class="col-sm-12" >
                <h3>Firmware Info</h3>
              </div>
              <div class="col-md-6" align="right">
                <!-- <button class="btn btn-primary btn-sm" align="right" onclick="install();">Install New App</button> -->
              </div>
            </div>
            <div class="row">
              <?php foreach ($app_details as $key => $value) {
                ?>
              <div class="col-md-4 ">
                <div class="card card-pricing bg-primary">
                  <div style="color: #eb262d;text-align: right;">
                    <a data-toggle="tooltip" title="Delete Firmware" href="delete_firm?appid={{$value->id}}" class="btn btn-link btn-danger btn-just-icon edit">
                      <i class="material-icons">delete</i>
                      <div class="ripple-container"></div>
                    </a>
                  </div>
                  <div class="card-body ">
                    <div class="card-icon">
                      <i class="material-icons">business</i>
                    </div>
                    <h3 class="card-title"><?php echo $value->firm_name; ?>  </h3>
                    <p class="card-description">
                      Version <?php echo $value->firm_ver; ?>
                    </p>
                    <?php if($value->status == 1){ ?>
                    <a href="update_verion_firm?appid={{$value->id}}" class="btn btn-rose btn-round">Update</a>
                    <a onclick="deactive_app({{$value->id}})" class="btn btn-rose btn-round">Deactivate</a>
                    <?php }else{ ?>
                    <a onclick="deactive_app1({{$value->id}})" class="btn btn-rose btn-round">Activate</a>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@include('default.footer')
@if (Session::has('message'))
<script>
  Swal.fire({
     type: 'info',
     title: 'info',
     text:   '<?= session('message') ?>',
      confirmButtonColor: '#eb262d',
     // footer: '<a href>Why do I have this issue?</a>'
   })
</script>
@endif
<script type="text/javascript">
  $(document).ready(function() {
    var IsNull= '@Session["msg"]'!= null; 
    var str='<?=session('msg') ?>';
    if(str !=''){
       // var msg= ''; 
      Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: '<?=session('msg') ?>',
           confirmButtonColor: '#eb262d',
          // footer: '<a href>Why do I have this issue?</a>'
        })
      <?php
    session()->forget('msg');
    // session(['msg' => '']);
    ?>
    }
  
  
    // initialise Datetimepicker and Sliders
    md.initFormExtendedDatetimepickers();
    if ($('.slider').length != 0) {
      md.initSliders();
    }
  
  <?php if(!empty($vehicledata)){ ?>
  
    loadvehicle();
    
  <?php } ?>
  });
  
  function deactive_app1(id){
  
    $.post('{{url('/')}}/deactive_firm1', { 'id': id }).done(function (data) {
               Swal.fire({
                          type: 'info',
                          title: 'info',
                          text:   'App Activated',
                           confirmButtonColor: '#eb262d',
                          // footer: '<a href>Why do I have this issue?</a>'
                        }).then(okay => {
                           if (okay) {
                            location.reload();
                          }
                        });
       });
  
  }
  
  function deactive_app(id){
  
    $.post('{{url('/')}}/deactive_firm', { 'id': id }).done(function (data) {
               Swal.fire({
                          type: 'info',
                          title: 'info',
                          text:   'App Deactivated',
                           confirmButtonColor: '#eb262d',
                          // footer: '<a href>Why do I have this issue?</a>'
                        }).then(okay => {
                           if (okay) {
                            location.reload();
                          }
                        });
       });
  
  }
  
  function loadvehicle(){
  
       var mfd = $("#manufact option:selected").val();
       <?php if(!empty($vehicledata)){ ?>
       <?php $vehicledatamodel = ($vehicledata->model != '')? $vehicledata->model:""; ?>
       <?php }else{ ?>
       <?php $vehicledatamodel = ""; ?>
       <?php } ?>
       $.post('{{url('/')}}/getModelList', { mfd: mfd }).done(function (data) {
              $('#model').html(data).selectpicker('refresh');
              $('#model').val('<?= $vehicledatamodel ?>').selectpicker('refresh');
              loadvehicle1();
        });
  }
  
  function loadvehicle1(){
       var mfd   = $("#manufact option:selected").val();
       var model = $("#model option:selected").val();
       <?php if(!empty($vehicledata)){ ?>
       <?php $vehicledatavarient = ($vehicledata->varient != '')? $vehicledata->varient:""; ?>
       <?php }else{ ?>
       <?php $vehicledatavarient = ""; ?>
       <?php } ?>
  
       $.post('{{url('/')}}/getVarientList', { 'mfd': mfd, 'model': model }).done(function (data) {
              $('#variant').html(data).selectpicker('refresh');
              $('#variant').val('<?= $vehicledatavarient ?>').selectpicker('refresh');
       });
  }
</script>