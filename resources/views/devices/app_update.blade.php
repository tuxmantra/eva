 
               <style type="text/css">
    .form-control{
      margin-top: 15px;
    }
    .bmd-form-group .form-control, .bmd-form-group label, .bmd-form-group input {
    line-height: 1.1;
}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    /* width: 220px; */

}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
     width: 100%; 
}
  </style>
   <div class="row" >
    <div class="col-sm-6" >
    <h3>App Info</h3>
  </div>
  <div class="col-md-6" align="right">
              <!-- <button class="btn btn-primary btn-sm" align="right" onclick="install();">Install New App</button> -->
            </div>
          </div>
  
  
                <div class="row">
                  <?php foreach ($app_details as $key => $value) {

                     ?>
  <div class="col-md-4 ">
    <div class="card card-pricing bg-primary">
       <div style="color: #eb262d;text-align: right;">
                            <a data-toggle="tooltip" title="Delete App" href="{{url('/')}}/delete_app_device?appid={{$value->id}}&did={{App\Helpers\Helper::crypt(1,$id)}}" class="btn btn-link btn-danger btn-just-icon edit"><i class="material-icons">delete</i><div class="ripple-container"></div></a>
       </div>
      <div class="card-body ">
        <div class="card-icon">
            <i class="material-icons">business</i>
        </div>
        <h3 class="card-title"><?php echo $value->app_name; ?>  </h3>
        <p class="card-description">
            Version <?php if($value->ins_status==1){ echo $value->curr_run_ver;}else{ echo $value->lat_ver; } ?>
        </p>
        <?php if($value->ins_status==1){ ?>
        <a href="#"onclick="install({{$value->app}})" class="btn btn-rose btn-round">Update</a>
        
        <a onclick="uninstall_app({{$value->id}})" class="btn btn-rose btn-round">Un-Install</a>
    <?php  }else{ ?>
      
        <a href="#pablo" class="btn btn-rose btn-round">App is Not Installed</a>
      <?php } ?>
        </div>
      }
    </div>
  </div>
<?php } ?>
</div>
<script>
  function install(appid){
    document.getElementById("load_data4").style.display = "none";
    $.get('{{url('/')}}/install_app_version/{{App\Helpers\Helper::crypt(1,$id)}}', { appid: appid }).done(function (data) {
      //alert("ok");
          $('#load_data4').html(data);
          document.getElementById("load_data4").style.display = "block";
         
    });
  }

  function uninstall_app(id){

    $.get('{{url('/')}}/uninstall_app/{{App\Helpers\Helper::crypt(1,$id)}}', { id: id }).done(function (data) {
     location.reload();
         
    });

  }


</script>
