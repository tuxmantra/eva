@include('default.header')
@include('default.sidebar')
@include('default.submenu')
<?php  ?>
<style type="text/css">
   .nav-pills .nav-item .nav-link {
   line-height: 24px;
   text-transform: uppercase;
   font-size: 12px;
   font-weight: 500;
   min-width: 100px;
   text-align: center;
   color: #555;
   transition: all .3s;
   border-radius: 30px;
   padding: 10px 15px;
   background-color: azure;
   /* border-color: aquamarine; */
   }
   .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
   width: 100%; 
   }
   .disabledcolor {
   background-color: #34495e85 !important;
   }
   .card .card-header-primary .card-icon,
   .card .card-header-primary .card-text,
   .card .card-header-primary:not(.card-header-icon):not(.card-header-text),
   .card.bg-primary,
   .card.card-rotate.bg-primary .front,
   .card.card-rotate.bg-primary .back {
   background: linear-gradient(60deg, #ffffff, #ffffff);
   }
   .card {
   box-shadow: 0 1px 16px 0 rgba(0, 0, 0, 0.14);
   }
</style>
<!-- End Navbar -->
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12" align="right">
            <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
         </div>
         <div class="col-md-12">
            <div class="card ">
               <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                     <h4 class="card-title">Device Manager</h4>
                  </div>
               </div>
               <div class="card-body">
                  <div class="row">
                     <div class="col-md-12" id="load_data">
                        <?php 
                           $disable1='';
                           if($device_data[0]->devtype == '1' ){
                             $disable1='disabled disabledcolor';
                           } 
                           $disable2='';
                           if($device_data[0]->devtype == '2' || $device_data[0]->devtype == '1'){
                             $disable2='disabled disabledcolor';
                           } 
                           
                           ?>
                        <ul class="nav nav-pills nav-pills-warning" role="tablist">
                           <li class="nav-item">
                              <a class="nav-link active" onclick="reload();"  data-toggle="tab" href="#link2" role="tablist">
                              Device Details
                              </a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link {{$disable1}}" onclick="firmware(); "data-toggle="tab" href="#link2" role="tablist">
                              FRM Update
                              </a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link {{$disable2}}" onclick="app_update();"  data-toggle="tab" href="#link2" role="tablist">
                              App Update
                              </a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link {{$disable2}}" onclick="add_bt();"  data-toggle="tab" href="#link2" role="tablist">
                              BT Dev
                              </a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link {{$disable2}}" onclick="device_conf()"  data-toggle="tab" href="#link2" role="tablist">
                              Device Config.
                              </a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" onclick="vehicle_info()" data-toggle="tab" href="#link2" role="tablist">
                              Vehicle info
                              </a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" onclick="cal_info()" data-toggle="tab" href="#link2" role="tablist">
                              Calibration info
                              </a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link " onclick="log_info()" data-toggle="tab" href="#link2" role="tablist">
                              Device Log
                              </a>
                           </li>
                        </ul>
                        <div class="tab-content tab-space">
                           <!----------------- Start Diagnostics --------------------> 
                           <div class="tab-pane active" id="link1">
                              <div class="container-fluid">
                                 <div class="continues">
                                    <div class="row classes">
                                       <form method="post" style="width: 100%;" action="{{url('/')}}/edit-device-save" class="form-horizontal" onsubmit="return createDevice()">
                                          <input type="hidden" value="{{$form_token}}" name="form_token">
                                          <input type="hidden" name="id" value="{{App\Helpers\Helper::crypt(1,$device_data[0]->id)}}">
                                          <div class="row">
                                             <label class="col-sm-2 col-form-label">Device Id</label>
                                             <div class="col-sm-4">
                                                <div class="form-group">
                                                   <input type="text" class="form-control" value="{{$device_data[0]->device_id}}" name="device_id" id="device_id" placeholder="Enter Device Id">
                                                </div>
                                             </div>
                                             <!-- </div>
                                                <div class="row"> -->
                                             <label class="col-sm-2 col-form-label">Name</label>
                                             <div class="col-sm-4">
                                                <div class="form-group">
                                                   <input type="text" value="{{$device_data[0]->name}}" name="name" id="name" class="form-control" placeholder="Enter Name">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <label class="col-sm-2 col-form-label">Description</label>
                                             <div class="col-sm-4">
                                                <div class="form-group">
                                                   <textarea class="form-control" cols="2" rows="2" name="desc" id="desc">{{$device_data[0]->descrp}}</textarea>
                                                </div>
                                             </div>
                                             <!--  </div>
                                                <div class="row"> -->
                                             <label class="col-sm-2 col-form-label">Partner</label>
                                             <div class="col-sm-4">
                                                <div class="form-group">
                                                   <select class="selectpicker" data-style="select-with-transition" title="Select User Type"  data-size="7" name="partner_id" id="partner_id" tabindex="-98">
                                                      <option disabled="">Select Partner</option>
                                                      @if (!empty($partner_list))
                                                      @foreach ($partner_list as $pl)
                                                      <option <?php if($device_data[0]->partner == $pl->uid){ ?> selected <?php } ?> value="{{$pl->uid}}">{{$pl->uname}}</option>
                                                      @endforeach
                                                      @endif
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <label class="col-sm-2 col-form-label">Type</label>
                                             <div class="col-sm-4">
                                                <div class="form-group">
                                                   <select class="selectpicker" data-style="select-with-transition" title="Select User Type" data-size="7" name="type" id="type" tabindex="-98">
                                                      <option disabled="">Select Type</option>
                                                      <option <?php if($device_data[0]->devtype == '1'){ ?> selected <?php } ?> value="1">Bluetooth</option>
                                                      <option <?php if($device_data[0]->devtype == '2'){ ?> selected <?php } ?> value="2">GSM</option>
                                                      <option <?php if($device_data[0]->devtype == '3'){ ?> selected <?php } ?> value="3">Linux</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <!--  </div>
                                                <div class="row"> -->
                                             <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                                             <div class="col-sm-4 checkbox-radios">
                                                <div class="form-check">
                                                   <label class="form-check-label">
                                                   <input class="form-check-input" type="radio" name="status" value="1" <?php if($device_data[0]->status == '1'){ ?> checked <?php } ?>> Active
                                                   <span class="circle">
                                                   <span class="check"></span>
                                                   </span>
                                                   </label>
                                                   <label class="form-check-label">
                                                   <input class="form-check-input" type="radio" name="status" value="0" <?php if($device_data[0]->status == '0'){ ?> checked <?php } ?>> In Active
                                                   <span class="circle">
                                                   <span class="check"></span>
                                                   </span>
                                                   </label>
                                                </div>
                                                <div class="form-check">
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row" style="display: none;">
                                             <label class="col-sm-2 col-form-label">Device OTA Commands</label>
                                             <div class="col-sm-4">
                                                <div class="form-group">
                                                   <textarea class="form-control" name="dev_cmds" id="dev_cmds">{{$device_data[0]->dev_cmds}}</textarea>
                                                </div>
                                             </div>
                                             <!-- </div>
                                                <div class="row"> -->
                                             <label class="col-sm-2 col-form-label">Device Current Version</label>
                                             <div class="col-sm-4">
                                                <div class="form-group">
                                                   <textarea class="form-control" name="curr_ver" id="curr_ver">{{$device_data[0]->curr_ver}}</textarea>
                                                </div>
                                             </div>
                                          </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12" id="load" style="display: none;" align="center">
                              <img src="public/assets/img/loader.gif" style="width: 15%;">
                              </div>
                              <div class="col-md-12" align="right">
                              <button type="submit" class="btn btn-fill btn-rose">Save</button>
                              </div>
                           </div>
                           <div class="tab-pane " id="link2">
                           <div class="col-md-12" id="load_data4" style="background-color: #fafafa">
                           </div>
                           </div>
                           <div class="tab-pane " id="link3">
                           <div class="col-md-12" id="load_data4" style="background-color: #fafafa">
                           </div>
                           </div>
                           <div class="tab-pane " id="link8">
                           <div class="col-md-12" id="load_data4" style="background-color: #fafafa">
                           </div>
                           </div>
                           <div class="tab-pane " id="link4">
                           <div class="col-md-12" id="load_data4" style="background-color: #fafafa">
                           </div>
                           </div>
                           <div class="tab-pane " id="link5">
                           <div class="col-md-12" id="load_data4">
                           </div>
                           </div>
                           <div class="tab-pane " id="link6">
                           <div class="col-md-12" id="load_data4">
                           </div>
                           </div>
                           <div class="tab-pane " id="link7">
                           <div class="col-md-12" id="load_data4">
                           </div>
                           </div>
                        </div>
                     </div>
                     </form> 
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!--   Core JS Files   -->
@include('default.footer')
<script type = "text/javascript">
  function vehicle_info() {
    document.getElementById("load_data4").style.display = "none";
    $.get('{{url('/')}}/add-vehicle_new/{{App\Helpers\Helper::crypt(1,$id)}}', {
      did: ''
    }).done(function (data) {
      //alert("ok");
      $('#load_data4').html(data);
      document.getElementById("load_data4").style.display = "block";

    });
  }

function log_info() {
  document.getElementById("load_data4").style.display = "none";
  $.get('{{url('/')}}/device_log/{{App\Helpers\Helper::crypt(1,$id)}}', {
    did: ''
  }).done(function (data) {
    //alert("ok");
    $('#load_data4').html(data);
    document.getElementById("load_data4").style.display = "block";

  });
}

function device_conf() {

  $.get('{{url('/')}}/device_conf/{{App\Helpers\Helper::crypt(1,$id)}}', {
    did: ''
  }).done(function (data) {
    //alert("ok");
    $('#load_data4').html(data);
    document.getElementById("load_data4").style.display = "block";

  });
}

function firmware() {
  document.getElementById("load_data4").style.display = "none";
  $.get('{{url('/')}}/firmware/{{App\Helpers\Helper::crypt(1,$id)}}', {
    did: ''
  }).done(function (data) {
    //alert("ok");
    $('#load_data4').html(data);
    document.getElementById("load_data4").style.display = "block";

  });
}

function app_update() {
  // document.getElementById("load_data4").style.display = "none";
  $.get('{{url('/')}}/app_update/{{App\Helpers\Helper::crypt(1,$id)}}', {
    did: ''
  }).done(function (data) {
    //alert("ok");
    $('#load_data4').html(data);
    // document.getElementById("load_data4").style.display = "block";

  });
}

function add_bt() {
  // document.getElementById("load_data4").style.display = "none";
  $.get('{{url('/')}}/add_bt/{{App\Helpers\Helper::crypt(1,$id)}}', {
    did: ''
  }).done(function (data) {
    //alert("ok");
    $('#load_data4').html(data);
    // document.getElementById("load_data4").style.display = "block";

  });
}


function update_dtype() {
  // document.getElementById("load_data4").style.display = "none";
  $('#dtype').html('').selectpicker('refresh');
  var dmake = $("#dmake").val();

  if (dmake == 'Numato') {
    $('#dtype').html('<option value="2">2 relay module</option> <option value="8">8 GPIO module</option><option value="8">8 relay module</option>').selectpicker('refresh');
  }
  if (dmake == 'Metaware') {
    $('#dtype').html('<option value="0">MetaMotionR</option> ').selectpicker('refresh');
    // $('#dtype').append(" "); MetaMotionR
  }
  // $('#dtype').append("<option>BMW</option>")
}

function add_bt_dev() {
  // document.getElementById("load_data4").style.display = "none";
  $.get('{{url('/')}}/add_bt_dev/{{App\Helpers\Helper::crypt(1,$id)}}', {
    did: ''
  }).done(function (data) {
    //alert("ok");
    $('#load_data4').html(data);
    // document.getElementById("load_data4").style.display = "block";

  });
}


function update_bt_data(id) {
  var dmake   = $("#dmake").val();
  var dtype   = $("#dtype").val();
  var dtype1  = $("#dtype option:selected").text();
  var mac_id1 = $("#mac_id1").val();
  var mac_id2 = $("#mac_id2").val();
  var mac_id3 = $("#mac_id3").val();
  var mac_id4 = $("#mac_id4").val();
  var mac_id5 = $("#mac_id5").val();
  var mac_id6 = $("#mac_id6").val();

  var i;
  var p     = [];
  var op    = [];
  var value = [];
  var name  = [];
  var unit  = [];
  if (dmake != '') {
    if (dtype != '') {
      for (i = (dtype - 1); i >= 0; i--) {
        p[i]     = $("#p" + i).val();
        op[i]    = $("#op" + i).val();
        value[i] = $("#value" + i).val();
        name[i]  = $("#name" + i).val();
        unit[i]  = $("#unit" + i).val();

        // alert(mac_id);
      }
      if (mac_id1 == '' || mac_id2 == '' || mac_id3 == '' || mac_id4 == '' || mac_id5 == '' || mac_id6 == '' || mac_id1.length != '2' || mac_id2.length != '2' || mac_id3.length != '2' || mac_id4.length != '2' || mac_id5.length != '2' || mac_id6.length != '2') {
        Swal.fire({
          type: 'info',
          title: 'info',
          text: 'Please Enter Valid MAC ID!',
          confirmButtonColor: '#eb262d',
          // footer: '<a href>Why do I have this issue?</a>'
        })

      } else {
        //  submit form
        var mac_id = mac_id1 + ':' + mac_id2 + ':' + mac_id3 + ':' + mac_id4 + ':' + mac_id5 + ':' + mac_id6;
        $.get('{{url('/')}}/update_bt_dev/{{App\Helpers\Helper::crypt(1,$id)}}', {
          btid: id,
          dmake: dmake,
          dtype: dtype1,
          mac_id: mac_id,
          p: p,
          op: op,
          value: value,
          name: name,
          unit: unit
        }).done(function (data) {

          Swal.fire({
            type: 'info',
            title: 'info',
            text: 'Device Saved',
            confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          }).then(okay => {
             if (okay) {
              location.reload();
            }
          });
        });
      }
    } else {
      Swal.fire({
        type: 'info',
        title: 'info',
        text: 'Please Select Device Type!',
        confirmButtonColor: '#eb262d',
        // footer: '<a href>Why do I have this issue?</a>'
      })
    }
  } else {
    Swal.fire({
      type: 'info',
      title: 'info',
      text: 'Please Select Device Make!',
      confirmButtonColor: '#eb262d',
      // footer: '<a href>Why do I have this issue?</a>'
    })
  }


}

function save_bt_data() {
  var dmake   = $("#dmake").val();
  var dtype   = $("#dtype").val();
  var dtype1  = $("#dtype option:selected").text();
  var mac_id1 = $("#mac_id1").val();
  var mac_id2 = $("#mac_id2").val();
  var mac_id3 = $("#mac_id3").val();
  var mac_id4 = $("#mac_id4").val();
  var mac_id5 = $("#mac_id5").val();
  var mac_id6 = $("#mac_id6").val();

  var i;
  var p     = [];
  var op    = [];
  var value = [];
  var name  = [];
  var unit  = [];
  if (dmake != '') {
    if (dtype != '') {
      for (i = (dtype - 1); i >= 0; i--) {
        p[i]     = $("#p" + i).val();
        op[i]    = $("#op" + i).val();
        value[i] = $("#value" + i).val();
        name[i]  = $("#name" + i).val();
        unit[i]  = $("#unit" + i).val();

      }
      if (mac_id1 == '' || mac_id2 == '' || mac_id3 == '' || mac_id4 == '' || mac_id5 == '' || mac_id6 == '' || mac_id1.length != '2' || mac_id2.length != '2' || mac_id3.length != '2' || mac_id4.length != '2' || mac_id5.length != '2' || mac_id6.length != '2') {
        Swal.fire({
          type: 'info',
          title: 'info',
          text: 'Please Enter Valid MAC ID!',
          confirmButtonColor: '#eb262d',
          // footer: '<a href>Why do I have this issue?</a>'
        })

      } else {


        //  submit form
        var mac_id = mac_id1 + ':' + mac_id2 + ':' + mac_id3 + ':' + mac_id4 + ':' + mac_id5 + ':' + mac_id6;

        $.get('{{url('/')}}/save_bt_dev/{{App\Helpers\Helper::crypt(1,$id)}}', {
          dmake: dmake,
          dtype: dtype1,
          mac_id: mac_id,
          p: p,
          op: op,
          value: value,
          name: name,
          unit: unit
        }).done(function (data) {

          Swal.fire({
            type: 'info',
            title: 'info',
            text: data,
            confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })

        });

      }
    } else {
      Swal.fire({
        type: 'info',
        title: 'info',
        text: 'Please Select Device Type!',
        confirmButtonColor: '#eb262d',
        // footer: '<a href>Why do I have this issue?</a>'
      })
    }
  } else {
    Swal.fire({
      type: 'info',
      title: 'info',
      text: 'Please Select Device Make!',
      confirmButtonColor: '#eb262d',
      // footer: '<a href>Why do I have this issue?</a>'
    })
  }


}


function reload() {
  document.getElementById("link1").style.display = "none";
  location.reload();
  document.getElementById("link1").style.display = "none";
}

function cal_info() {
  $.get('{{url('/')}}/vehicle-calibration_new/{{App\Helpers\Helper::crypt(1,$id)}}', {
    did: ''
  }).done(function (data) {
    //alert("ok");
    $('#load_data4').html(data);

  });
}

function save_cmd(devid) {

  // var cmd=document.getElementById("cmd").value;
  var key = [];
  var i = 0;
  var inputs = $(".cmdkey").each(function () {

    key[i] = $(this).val();
    // alert(key[i]);
    i++;
  });
  var value = [];
  var i = 0;
  var inputs1 = $(".cmdvalue").each(function () {

    value[i] = $(this).val();
    // alert(key[i]);
    i++;
  });
  // var cmd= getElementsByClassName("cmdkey");

  $.get('{{url('/')}}/save_cmd', {
    devid: devid,
    key: key,
    value: value
  }).done(function (data) {

    Swal.fire({
      type: 'info',
      title: 'info',
      text: 'Device Commands Updated',
      confirmButtonColor: '#eb262d',
      // footer: '<a href>Why do I have this issue?</a>'
    }).then(okay => {
             if (okay) {
              location.reload();
            }
          });

  });
}

</script> <script src = "https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js" > </script> 
<script type = "text/javascript" >
  $('select').selectpicker();


</script>
@if(Session::has('message'))
<script>
   Swal.fire({
      type: 'info',
      title: 'info',
      text:   '<?= session('message') ?>',
       confirmButtonColor: '#eb262d',
      // footer: '<a href>Why do I have this issue?</a>'
    })
</script>
@endif