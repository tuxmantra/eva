<style type="text/css">
   /*.bmd-form-group {
   position: relative;
   max-width: 38px;
   }*/
   .form-control {
   /* background: no-repeat center bottom, center calc(100% - 1px); */
   background-size: 0 100%, 100% 100%;
   border: 0;
   height: 19px;
   transition: background 0s ease-out;
   padding-left: 0;
   padding-right: 0;
   border-radius: 0;
   font-size: 14px;
   text-align: center;
   margin-top: 3px;
   /*background-color: #f3ebec8c;*/
   }
   .inputs{
   border: 0;
   width: 45px;
   height: 25px;
   text-align: center;
   border-bottom: 0px;
   background-color: #FFFF;
   background-image: linear-gradient(to top, #f3cacb 2px, rgba(156, 39, 176, 0) 1px), linear-gradient(to top, #d2d2d2 1px, rgba(210, 210, 210, 0) 1px);
   }
   .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
   width: 100%;
   margin-top: -10px;
   }
</style>
<div class="row classes">
   <div class="col-md-3" style="
      margin-left: 15px;
      " >
      <label class="form-lable" style="
         margin-bottom: 0px;
         margin-top: 27px;
         ">DEVICE MAKE : </label>
      <select class="selectpicker dmake" data-style="select-with-transition" onchange="update_dtype();" title="Device Make" data-size="7" name="dmake" id="dmake" tabindex="-98" >
         <option disabled="">Deviec Make</option>
         <option value="Numato" <?php if($app_details[0]->bt_dev_make =='Numato' ){echo 'selected';} ?> >Numato</option>
         <option value="Metaware" <?php if($app_details[0]->bt_dev_make =='Metaware' ){echo 'selected';} ?>>Metaware</option>
      </select>
   </div>
   <div class="col-md-3" >
      <label class="form-lable" style="
         margin-bottom: 0px;
         margin-top: 27px;
         ">DEVICE TYPE :</label>
      <select class="selectpicker rangecolumn" data-style="select-with-transition" title="Device Type" data-size="7" name="dtype" id="dtype" tabindex="-98" onchange="update_col();">
         <option disabled="">Select Columns</option>
         <?php if($app_details[0]->bt_dev_make=="Numato"){ ?>
         <option value="2" <?php if($app_details[0]->bt_dev_type == '2 relay module'){echo "selected";} ?>  >2 relay module</option>
         <option value="8" <?php if($app_details[0]->bt_dev_type == '8 GPIO module'){echo "selected";} ?> >8 GPIO module</option>
         <option value="8" <?php if($app_details[0]->bt_dev_type == '8 relay module'){echo "selected";} ?> >8 relay module</option>
         <?php }else{
            ?>
         <option value="" <?php if($app_details[0]->bt_dev_type == 'MetaMotionR'){echo "selected";} ?> >MetaMotionR</option>
         <?php } ?>
      </select>
   </div>
   <div class="col-md-5" style="
      /* margin: 0px; */
      margin-top: 21px;
      margin-left: 0px;
      background-color: #f3ebec8c;
      ">
      <?php $pieces = explode(":", $app_details[0]->mac_id); ?>
      <label class="form-lable">MAC ID : </label><br>
      <input type="text" class=" inputs" id="mac_id1" maxlength="2" value="{{$pieces[0]}}">: 
      <input type="text" class=" inputs" id="mac_id2" maxlength="2" value="{{$pieces[1]}}" >:
      <input type="text" class=" inputs" id="mac_id3" maxlength="2" value="{{$pieces[2]}}">:
      <input type="text" class=" inputs" id="mac_id4" maxlength="2" value="{{$pieces[3]}}">:
      <input type="text" class=" inputs" id="mac_id5" maxlength="2" value="{{$pieces[4]}}">:
      <input type="text" class=" inputs" id="mac_id6" maxlength="2" value="{{$pieces[5]}}">
   </div>
   <?php if($app_details[0]->bt_dev_make=="Numato"){ ?>
   <div class="col-md-12" style="margin-bottom: 30px; margin-top: 27px;" id="place">
      <div class="col-md-12">
         <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thread>
               <tr>
                  <th class="table-heading-font" >Pin No</th>
                  <th class="table-heading-font" >Data Type</th>
                  <th class="table-heading-font">Operator</th>
                  <th class="table-heading-font">Value</th>
                  <th class="table-heading-font">Name</th>
                  <th class="table-heading-font">Unit</th>
               </tr>
            </thread>
            <tr>
               <?php 
                  if($app_details[0]->bt_dev_type=="2 relay module"){ 
                      $count=2;
                  }else{
                     $count=8;
                  }
                  
                  for($i=($count-1);$i>=0;$i--){
                  $p='P'.$i.'type';
                  $val='P'.$i.'val';
                  $name='P'.$i.'name';
                  $op='P'.$i.'op';
                  $unit='P'.$i.'unit';
                  // echo $app_details[0]->{$op};
                   ?>
               <td style="background-color: #f5f5f5; width: 8%" >P{{$i}}</td>
               <td style=" width:30%" >
                  <select id="p{{$i}}"  class="custom-select" name="p{{$i}}"  >
                     <option value="analog_input" <?php if($app_details[0]->{$p} == 'analog_input'){echo "selected";} ?> >Analog Input</option>
                     <option value="digital_input" <?php if($app_details[0]->{$p} == 'digital_input'){echo "selected";} ?> >Digital Input</option>
                     <option value="digital_output" <?php if($app_details[0]->{$p} == 'digital_output'){echo "selected";} ?> >Digital Output</option>
                  </select>
               </td>
               <td style=" background-color: #f5b5b511; width:10%">
                  <select id="op{{$i}}"  class="custom-select" name="op{{$i}}"  >
                     <option value="+" <?php if($app_details[0]->{$op} == '+'){echo "selected";} ?> >+</option>
                     <option value="-" <?php if($app_details[0]->{$op} == '-'){echo "selected";} ?> >-</option>
                     <option value="/" <?php if($app_details[0]->{$op} == '/'){echo "selected";} ?> >/</option>
                     <option value="*" <?php if($app_details[0]->{$op} == '*'){echo "selected";} ?> >*</option>
                  </select>
               </td>
               <td style="width: 10%;"><input style="width: 100%;" type="text" class="input form-control" id="value{{$i}}"  value="{{$app_details[0]->{$val} }}" /></td>
               <td style="background-color: #f5b5b511; width: 30%;"><input style="width: 100%;" type=text class="input form-control" id="name{{$i}}" value="{{$app_details[0]->{$name} }}" />
               </td>
               <td style="background-color: #f5b5b511; width: 30%;"><input style="width: 100%;" type=text class="input form-control" id="unit{{$i}}" value="{{$app_details[0]->{$unit} }}" />
               </td>
            </tr>
            <?php } ?>
         </table>
      </div>
   </div>
   <?php } ?>
   <div class="col-md-12" style="margin-bottom: 30px;" >
      <button class="btn btn-rose btn-round pull-right" onclick="update_bt_data({{$app_details[0]->id}});" >save</button>
   </div>
</div>
<script>
   $(".inputs").keyup(function () {
       if (this.value.length == this.maxLength) {
         $(this).next('.inputs').focus();
       }
   });
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
   function update_col(){
   var count = $('.rangecolumn').children("option:selected").val(); 
   // alert(count);
   $('#place').html('');
   $('#place1').html('');
   $('#place2').html('');
   $('#place3').html('');
   $('#place4').html('');
   var string='<div class="col-md-12">   <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%"><thread>  <tr> <th class="table-heading-font" >Pin NO</th> <th class="table-heading-font" >Data Type</th><th class="table-heading-font">Operator</th><th class="table-heading-font">Value</th><th class="table-heading-font">Name</th><th class="table-heading-font">Unit</th>  </tr></thread>';
   
   for(var i=(count-1);i>=0;i--){
   
   
   string=string+'<tr> <td style="background-color: #f5f5f5; width: 8% " >P'+i+'</td><td style="width:30%" ><select id=p' + i +'  class="custom-select" name=p' + i +'  ><option value="analog_input">Analog Input</option><option value="digital_input">Digital Input</option><option value="digital_output">Digital Output</option></select></td><td style="background-color: #f5b5b511; width:10%" ><select id=op' + i +'  class="custom-select" name=op' + i +'  ><option value="+">+</option><option value="-">-</option><option value="/">/</option><option value="*">*</option></select></td><td style="width: 10%;" ><input style="width: 100%;" type=text class="input form-control" id=value' + i + '  value="" /></td><td style="background-color: #f5b5b511; width: 30%; "><input style="width: 100%;" type=text class="input form-control" id=name' + i + 
   ' value="" /></td><td style="background-color: #f5b5b511; width: 30%; "><input style="width: 100%;" type=text class="input form-control" id=unit' + i + 
   ' value="" /></td></tr>';
   
   
   
   
   
   // $('#place').append('<div class="row">  <div class="col-md-2">P'+i+'</div><div class="col-md-2"><input style="width: 100%;" type=text class="input form-control" id=p' + i+
   //    ' value="" /></div><div class="col-md-3"><select id=op' + i +'  class="custom-select" name=op' + i +'  ><option value="analog_input">Analog Input</option><option value="digital_input">Digital Input</option><option value="digital_output">Digital Output</option></select></div><div class="col-md-2"><input style="width: 100%;" type=text class="input form-control" id=value' + i + '  value="" /></div> <div class="col-md-2"><input style="width: 100%;" type=text class="input form-control" id=name' + i + 
   //    ' value="" /></div> </div>');
   }
   string=string+'</tbody</table></div>';
   $('#place').append(string);
   }
   $('.selectpicker').selectpicker();
   
   
   
</script>