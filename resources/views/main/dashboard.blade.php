  @include('default.header')
 
<?php

$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
?>

@include('main.menu')
 
<?php
}else{
?>
@include('default.sidebar')
  @include('default.submenu')
<?php
}

?>
 

<style type="text/css">
  .ct-chart .ct-label.ct-vertical.ct-start{margin: 12px auto;}
  .chartist-tooltip {
  position: absolute;
  display: inline-block;
  opacity: 0;
  min-width: 5em;
  padding: .5em;
  background: #F4C63D;
  color: #453D3F;
  font-family: Oxygen,Helvetica,Arial,sans-serif;
  font-weight: 700;
  text-align: center;
  pointer-events: none;
  z-index: 1;
  -webkit-transition: opacity .2s linear;
  -moz-transition: opacity .2s linear;
  -o-transition: opacity .2s linear;
  transition: opacity .2s linear; }
  .chartist-tooltip:before {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    width: 0;
    height: 0;
    margin-left: -15px;
    border: 15px solid transparent;
    border-top-color: #F4C63D; }
  .chartist-tooltip.tooltip-show {
    opacity: 1; }

.ct-area, .ct-line {
  pointer-events: none; 
}
.bootstrap-select{
  width: 152px !important;
}.card-header-rose .col-md-6 {
    width: 40%;
}
.card-header-info .col-md-6 {
    width: 40%;
}
.dropdown.bootstrap-select.month_select.dropup {
    padding-top: 10px;
}
.dropdown.bootstrap-select.year_select.dropup {
    padding-top: 10px;
}
/*# sourceMappingURL=chartist-plugin-tooltip.css.map */
</style>
      <!-- End Navbar -->
      <div class="content">
        <div class="content">
          <div class="container-fluid">
<!--             <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons"></i>
                    </div>
                    <h4 class="card-title">Global Sales by Top Locations</h4>
                  </div>
                  <div class="card-body ">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="table-responsive table-sales">
                          <table class="table">
                            <tbody>
                              <tr>
                                <td>
                                  <div class="flag">
                                    <img src="public/assets/img/flags/US.png" </div>
                                </td>
                                <td>USA</td>
                                <td class="text-right">
                                  2.920
                                </td>
                                <td class="text-right">
                                  53.23%
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="flag">
                                    <img src="public/assets/img/flags/DE.png" </div>
                                </td>
                                <td>Germany</td>
                                <td class="text-right">
                                  1.300
                                </td>
                                <td class="text-right">
                                  20.43%
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="flag">
                                    <img src="public/assets/img/flags/AU.png" </div>
                                </td>
                                <td>Australia</td>
                                <td class="text-right">
                                  760
                                </td>
                                <td class="text-right">
                                  10.35%
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="flag">
                                    <img src="public/assets/img/flags/GB.png" </div>
                                </td>
                                <td>United Kingdom</td>
                                <td class="text-right">
                                  690
                                </td>
                                <td class="text-right">
                                  7.87%
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="flag">
                                    <img src="public/assets/img/flags/RO.png" </div>
                                </td>
                                <td>Romania</td>
                                <td class="text-right">
                                  600
                                </td>
                                <td class="text-right">
                                  5.94%
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <div class="flag">
                                    <img src="public/assets/img/flags/BR.png" </div>
                                </td>
                                <td>Brasil</td>
                                <td class="text-right">
                                  550
                                </td>
                                <td class="text-right">
                                  4.34%
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          </div>
                          </div>
                          <div class="col-md-6 ml-auto mr-auto">
                            <div id="worldMap" style="height: 300px;"></div>
                          </div>
                          </div>
                          </div>
                          </div>
                          </div>
                        </div> -->
                        <!-- <button type="button" class="btn btn-round btn-default dropdown-toggle btn-link" data-toggle="dropdown">
7 days
</button> -->

                        <div class="row">
                          <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                              <div class="card-header card-header-warning card-header-icon">
                                <div class="card-icon">
                                  <img src="{{url('/')}}/public/icons/1.png" >
                                </div>
                                <p class="card-category"></p>
                                <h3 class="card-title">3</h3>
                              </div>
                              <div class="card-footer">
                                <div class="stats" style="font-weight: 500;">
                                  No. of Stations
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                              <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                  <img src="{{url('/')}}/public/icons/3.png" >
                                </div>
                                <p class="card-category"></p>
                                <h3 class="card-title">{{$total_test_done[0]->count}}</h3>
                              </div>
                              <div class="card-footer">
                                <div class="stats" style="font-weight: 500;">
                                   Total No. of Vehicles Tested
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-6 col-sm-6">
                          
                            <div class="card card-stats" onclick="alerts">
                              <div class="card-header card-header-rose card-header-icon">
                                <div class="card-icon">
                                  <img src="{{url('/')}}/public/icons/2.png" >
                                </div>
                                <p class="card-category"></p>
                                <h3 class="card-title">{{$passed[0]->count}}</h3>
                              </div>
                              <div class="card-footer">
                                <div class="stats" style="font-weight: 500;">
                                 No. of Vehicles Passed Test
                                </div>
                              </div>
                            </div>
                           
                          </div>
                          <div class="col-lg-3 col-md-6 col-sm-6">
                           <a href="/alerts#link1"> 
                            <div class="card card-stats" > 
                              <div class="card-header card-header-info card-header-icon">
                                <div class="card-icon">
                                  <img src="{{url('/')}}/public/icons/4.png" >
                                </div>
                                <p class="card-category"></p>
                                <h3 class="card-title">{{$failed[0]->count}}</h3>
                              </div>
                              <div class="card-footer">
                                <div class="stats" style="font-weight: 500;">
                                   No. of Vehicles Failed Test
                                </div>
                              </div>
                            </div>
                           </a> 
                          </div>
                        </div>

            <div class="row">
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header card-header-icon card-header-rose">
                    <div class="card-icon">
                      <i class="material-icons">insert_chart</i>
                    </div>
                    <div class="col-md-6" style="float: left;">
                    <h4 class="card-title">Monthly Counts
                     <!--  <small>- Bar Chart</small> -->
                    </h4>
                  </div>
                  <div class="col-md-6" style="float: right;margin: 0;text-align: right;padding: 0;">

                          <select class="selectpicker year_select" id="year_select" data-size="7" data-style="btn btn-primary btn-round" title="Single Select">
                            <option disabled selected>Pick Year</option>
                            <option <?php if(!empty($year)){ if($year == '2018'){ ?> selected <?php } } ?> value="2018">Year 2018</option>
                            <option <?php if(!empty($year)){ if($year == '2019'){ ?> selected <?php } } ?> value="2019">Year 2019</option>
                            <option <?php if(!empty($year)){ if($year == '2020'){ ?> selected <?php } } ?> value="2020">Year 2020</option>
                            <option <?php if(!empty($year)){ if($year == '2021'){ ?> selected <?php } } ?> value="2021">Year 2021</option>
                            <option <?php if(!empty($year)){ if($year == '2022'){ ?> selected <?php } } ?> value="2022">Year 2022</option>
                            <option <?php if(!empty($year)){ if($year == '2023'){ ?> selected <?php } } ?> value="2023">Year 2023</option>
                            <option <?php if(!empty($year)){ if($year == '2024'){ ?> selected <?php } } ?> value="2024">Year 2024</option>
                            <option <?php if(!empty($year)){ if($year == '2025'){ ?> selected <?php } } ?> value="2025">Year 2025</option>
                          </select>
                  </div>
                  </div>
                  <div class="card-body">
                  <canvas id="myChart_bar" width="400" height="200"></canvas>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="card">
                  <div class="card-header card-header-icon card-header-info">
                    <div class="card-icon">
                      <i class="material-icons">timeline</i>
                    </div>
                    <div class="col-md-6" style="float: left;">
                    <h4 class="card-title">Daily Counts
                      <small></small>
                    </h4>
                  </div>
                                    <div class="col-md-6" style="float: right; padding: 0;
    margin: 0;
    text-align: right;">
    
                          <select class="selectpicker month_select" id="month_select" data-size="7" data-style="btn btn-primary btn-round" title="Single Select">
                            <option disabled selected>Pick Month</option>
                            <option <?php if(!empty($month)){ if($month == '1'){ ?> selected <?php } } ?> value="1">January</option>
                            <option <?php if(!empty($month)){ if($month == '2'){ ?> selected <?php } } ?> value="2">February</option>
                            <option <?php if(!empty($month)){ if($month == '3'){ ?> selected <?php } } ?> value="3">March</option>
                            <option <?php if(!empty($month)){ if($month == '4'){ ?> selected <?php } } ?> value="4">April</option>
                            <option <?php if(!empty($month)){ if($month == '5'){ ?> selected <?php } } ?> value="5">May</option>
                            <option <?php if(!empty($month)){ if($month == '6'){ ?> selected <?php } } ?> value="6">June</option>
                            <option <?php if(!empty($month)){ if($month == '7'){ ?> selected <?php } } ?> value="7">July</option>
                            <option <?php if(!empty($month)){ if($month == '8'){ ?> selected <?php } } ?> value="8">August</option>
                            <option <?php if(!empty($month)){ if($month == '9'){ ?> selected <?php } } ?> value="9">September</option>
                            <option <?php if(!empty($month)){ if($month == '10'){ ?> selected <?php } } ?> value="10">October</option>
                            <option <?php if(!empty($month)){ if($month == '11'){ ?> selected <?php } } ?> value="11">November</option>
                            <option <?php if(!empty($month)){ if($month == '12'){ ?> selected <?php } } ?> value="12">December</option>
                          </select>
                  </div>

                  </div>
                  <div class="card-body">
                   <canvas id="myChart" width="400" height="200"></canvas>
                  </div>
                </div>
              </div>
            </div>

                      </div>
                    </div>
                  </div>

                
                </div>
              </div>


@include('default.footer')
<script src="{{url('/')}}/public/assets/js/plugins/Chart.js"></script>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [1,2,3,4,5,6,7,8,9,10,11],
        datasets: [{
            label: 'Test Passed',
            yLabel: 'Days',
            data: [30,10,5,60,30,40,10,4,40,15,13],
           
            borderColor: [
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)',
                'rgba(83,109,254,1)'

                // 'rgba(54, 162, 235, 1)',
                // 'rgba(255, 206, 86, 1)',
                // 'rgba(75, 192, 192, 1)',
                // 'rgba(153, 102, 255, 1)',
                // 'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 4
        },{
          label: 'Test Failed',
            yLabel: 'Days',
            data: [3,10,20,6,30,40,60,4,20,25,30],
            
            borderColor: [
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)',
                'rgba(235,38,45,1)'

                        
                // 'rgba(54, 162, 235, 1)',
                // 'rgba(255, 206, 86, 1)',
                // 'rgba(75, 192, 192, 1)',
                // 'rgba(153, 102, 255, 1)',
                // 'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 4
        },{
          label: 'Prognostics Test',
            yLabel: 'Days',
            data: [30,20,10,60,20,30,40,14,25,40,15],
            
            borderColor: [
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)',
                'rgba(64,196,255,1)'
                


                // 'rgba(54, 162, 235, 1)',
                // 'rgba(255, 206, 86, 1)',
                // 'rgba(75, 192, 192, 1)',
                // 'rgba(153, 102, 255, 1)',
                // 'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 4
        }]
    },
   options: {
      responsive: true,
      scales: {
        xAxes: [ {
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Days'
          },
          ticks: {
            major: {
              fontStyle: 'bold',
              fontColor: '#FF0000'
            }
          }
        } ],
        yAxes: [ {
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Counts'
          }
        } ]
      }
    }

});
</script>
<script>
var ctx = document.getElementById('myChart_bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        datasets: [{
            label: 'Fuel Test ',
            
            data: [220,300,400,100,240,310,30,110,330,120,100,440],
            backgroundColor: [
                '#536DFE',
                '#536DFE',
                  '#536DFE',
                   '#536DFE',
                    '#536DFE',
                     '#536DFE',
                      '#536DFE',
                       '#536DFE',
                        '#536DFE',
                        '#536DFE',
                          '#536DFE',
                           '#536DFE'
                
            ],
            borderColor: [
                '#536DFE',
                '#536DFE',
                  '#536DFE',
                   '#536DFE',
                    '#536DFE',
                     '#536DFE',
                      '#536DFE',
                       '#536DFE',
                        '#536DFE',
                        '#536DFE',
                          '#536DFE',
                           '#536DFE'
                
            ],
            borderWidth: 4
        },
        {
            label: 'Prognostics Test',
            
            data: [200,400,600,10,20,30,230,120,33,120,130,240],
            backgroundColor: [
                '#40C4FF',
                '#40C4FF',
                  '#40C4FF',
                   '#40C4FF',
                    '#40C4FF',
                     '#40C4FF',
                      '#40C4FF',
                       '#40C4FF',
                        '#40C4FF',
                         '#40C4FF',
                          '#40C4FF',
                           '#40C4FF'
                
            ],
            borderColor: [
                '#40C4FF',
                '#40C4FF',
                  '#40C4FF',
                   '#40C4FF',
                    '#40C4FF',
                     '#40C4FF',
                      '#40C4FF',
                       '#40C4FF',
                        '#40C4FF',
                         '#40C4FF',
                          '#40C4FF',
                           '#40C4FF'
                
            ],
            borderWidth: 4
        }]
    },
   options: {
      responsive: true,
      scales: {
        xAxes: [ {
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Month'
          },
          ticks: {
            major: {
              fontStyle: 'bold',
              fontColor: '#FF0000'
            }
          }
        } ],
        yAxes: [ {
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Counts'
          }
        } ]
      }
    }

});
</script>
<script src="{{url('/')}}/public/assets/js/plugins/chartist.min.js"></script>
<script type="text/javascript">
  /* chartist-plugin-tooltip 0.0.18
 * Copyright © 2017 Markus Padourek
 * Free to use under the WTFPL license.
 * http://www.wtfpl.net/
 */

!function(a,b){"function"==typeof define&&define.amd?define(["chartist"],function(c){return a.returnExportsGlobal=b(c)}):"object"==typeof exports?module.exports=b(require("chartist")):a["Chartist.plugins.tooltip"]=b(Chartist)}(this,function(a){return function(a,b,c){"use strict";function d(a){f(a,"tooltip-show")||(a.className=a.className+" tooltip-show")}function e(a){var b=new RegExp("tooltip-show\\s*","gi");a.className=a.className.replace(b,"").trim()}function f(a,b){return(" "+a.getAttribute("class")+" ").indexOf(" "+b+" ")>-1}function g(a,b){do{a=a.nextSibling}while(a&&!f(a,b));return a}function h(a){return a.innerText||a.textContent}var i={currency:void 0,currencyFormatCallback:void 0,tooltipOffset:{x:0,y:-20},anchorToPoint:!1,appendToBody:!1,class:void 0,pointClass:"ct-point"};c.plugins=c.plugins||{},c.plugins.tooltip=function(j){return j=c.extend({},i,j),function(i){function k(a,b,c){n.addEventListener(a,function(a){b&&!f(a.target,b)||c(a)})}function l(b){p=p||o.offsetHeight,q=q||o.offsetWidth;var c,d,e=-q/2+j.tooltipOffset.x,f=-p+j.tooltipOffset.y;if(j.appendToBody)o.style.top=b.pageY+f+"px",o.style.left=b.pageX+e+"px";else{var g=n.getBoundingClientRect(),h=b.pageX-g.left-a.pageXOffset,i=b.pageY-g.top-a.pageYOffset;!0===j.anchorToPoint&&b.target.x2&&b.target.y2&&(c=parseInt(b.target.x2.baseVal.value),d=parseInt(b.target.y2.baseVal.value)),o.style.top=(d||i)+f+"px",o.style.left=(c||h)+e+"px"}}var m=j.pointClass;i.constructor.name==c.Bar.prototype.constructor.name?m="ct-bar":i.constructor.name==c.Pie.prototype.constructor.name&&(m=i.options.donut?"ct-slice-donut":"ct-slice-pie");var n=i.container,o=n.querySelector(".chartist-tooltip");o||(o=b.createElement("div"),o.className=j.class?"chartist-tooltip "+j.class:"chartist-tooltip",j.appendToBody?b.body.appendChild(o):n.appendChild(o));var p=o.offsetHeight,q=o.offsetWidth;e(o),k("mouseover",m,function(a){var e=a.target,f="",k=i instanceof c.Pie?e:e.parentNode,m=k?e.parentNode.getAttribute("ct:meta")||e.parentNode.getAttribute("ct:series-name"):"",n=e.getAttribute("ct:meta")||m||"",r=!!n,s=e.getAttribute("ct:value");if(j.transformTooltipTextFnc&&"function"==typeof j.transformTooltipTextFnc&&(s=j.transformTooltipTextFnc(s)),j.tooltipFnc&&"function"==typeof j.tooltipFnc)f=j.tooltipFnc(n,s);else{if(j.metaIsHTML){var t=b.createElement("textarea");t.innerHTML=n,n=t.value}if(n='<span class="chartist-tooltip-meta">'+n+"</span>",r)f+=n+"<br>";else if(i instanceof c.Pie){var u=g(e,"ct-label");u&&(f+=h(u)+"<br>")}s&&(j.currency&&(s=void 0!=j.currencyFormatCallback?j.currencyFormatCallback(s,j):j.currency+s.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g,"$1,")),s='<span class="chartist-tooltip-value">'+s+"</span>",f+=s)}f&&(o.innerHTML=f,l(a),d(o),p=o.offsetHeight,q=o.offsetWidth)}),k("mouseout",m,function(){e(o)}),k("mousemove",null,function(a){!1===j.anchorToPoint&&l(a)})}}}(window,document,a),a.plugins.tooltip});
//# sourceMappingURL=chartist-plugin-tooltip.min.js.map
</script>
  <script>
    $(document).ready(function() {

      if(window.location.href == '{{url('/')}}/dashboard'){
          var d     = new Date();
          var year  = d.getFullYear();
          var month = d.getMonth();
          console.log(year);
          $('#year_select').selectpicker('refresh');
          $('.year_select').selectpicker('val', year);
          $('#month_select').selectpicker('refresh');
          $('.month_select').selectpicker('val', month+1);
      }
      
      // ON PAGE LOAD START
     
  // ON YEAR SELECT START

$( "#year_select" ).change(function() {
    var month = $(".month_select option:selected").val();
    var year  = $(this).children("option:selected").val();
    window.location.href = "{{url('/')}}/dashboard/"+year+"/"+month;
});

$( "#month_select" ).change(function() {
    var year  = $(".year_select option:selected").val();
    var month = $(this).children("option:selected").val();
    window.location.href = "{{url('/')}}/dashboard/"+year+"/"+month;
});


  // ON YEAR SELECT END

});
</script>