 <style type="text/css">
        #profile span{
            position: absolute;
            /* margin: 3%; */
            padding: -2px;
            /* margin-right: 20%; */
            /* margin-right: 25px; */
            
            background-color: white;
            width: 85%;
      }
       .table{
             margin-top: 25px;
      }
      </style>
<style type="text/css">
  body{
  	margin-top: 54px;
  }
a, a:active, a:hover {
  color: inherit;
  text-decoration: none;
}
ul, li {
  display: block;
  list-style: none;
}
ul {
  margin: 10px 5px 15px;
  padding: 5px;
}
li {
  margin: 2px 0;
  padding: 6px 8px;
}
p {
  line-height: 1.5;
  margin: 10px;
  padding: 5px 5px 15px;
  text-align: justify;
}
img {
  display: block;
  width: 100%;
}
h1, h2, h3, h4, h5, h6 {
  color: #555;
  padding: 5px 10px 10px;
}
header {
  background: #eeeeee;
  /*border-bottom: 1px solid #DF4C00;*/
  box-shadow: 1px 0 2px rgba(0,0,0,.5);
  color: #eb262d;
  display: flex;
  justify-content: space-between;
  padding: 10px 10px;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  vertical-laign: middle;
  z-index: 9;
  
  -webkit-transform: translate3d(0,0,0);
     -moz-transform: translate3d(0,0,0);
          transform: translate3d(0,0,0);
  -webkit-transition: all 500ms ease-in-out;
     -moz-transition: all 500ms ease-in-out;
          transition: all 500ms ease-in-out;
}
header [class*="menu-"] {
  cursor: pointer;
  font-size: 22px;
  height: initial;
  padding: 5px;
  width: initial;
}
header h1 {
  color: #FFF;
  font-size: 24px;
  font-weight: 100;
  padding: 0;
  text-shadow: 0 1px 2px rgba(0,0,0,.5);
}
.intro {
  margin: 57px auto 0px;
  padding: 0;
  -webkit-transform: translate3d(0,0,0);
     -moz-transform: translate3d(0,0,0);
          transform: translate3d(0,0,0);
  -webkit-transition: all 500ms ease-in-out;
     -moz-transition: all 500ms ease-in-out;
          transition: all 500ms ease-in-out;
}

section {
  margin: 20px auto 40px;
  -webkit-transform: translate3d(0,0,0);
     -moz-transform: translate3d(0,0,0);
          transform: translate3d(0,0,0);
  -webkit-transition: all 500ms ease-in-out;
     -moz-transition: all 500ms ease-in-out;
          transition: all 500ms ease-in-out;
}
[id*="menu-"] {
  background: #252525;
  bottom: 0;
  color: #858585;
  height: 100%;
  position: fixed;
  top: 0;
  width: 218px;
  margin-left: -14px;
  z-index: 99;
  margin-top: -6px;
}
[id*="menu-"] .box {
  height: 100%;
  margin: 0;
  padding: 0;
  overflow: auto;
}
[id*="menu-"] .box::-webkit-scrollbar {
  height: 4px;
  width: 4px;
}
[id*="menu-"] ul {
  margin: 0;
  padding: 0;
}
[id*="menu-"] li {
  border-bottom: 1px solid #1F1F1F;
  font-size: 12px;
  line-height: 28px;
  margin: 0;
  overflow: hidden;
  padding: 5px;
  position: relative;
  width: 100%;
}
[id*="menu-"] li:last-child {
  border: 0;
}
#menu-left {
  border-right: 1px solid #161616;
  left: -205px;
  -webkit-transform: translate3d(0,0,0);
     -moz-transform: translate3d(0,0,0);
          transform: translate3d(0,0,0);
  -webkit-transition: all 500ms ease-in-out;
     -moz-transition: all 500ms ease-in-out;
          transition: all 500ms ease-in-out;
}
#menu-left .profile {
  width: 100%;
    /* background: #222; */
    /* border-bottom: 5px solid #151515; */
    padding: 16px 24px;
}
#menu-left .profile  img {
      /* border: 1px solid #111; */
    border-radius: 5px;
    display: inline-block;
    height: 54px;
    vertical-align: inherit;
    width: 125px;
}
#menu-left .content {
  position: relative;
  height: initial;
  width: 100%;
}
#menu-left .content  h5 {
  background: #353535;
  color: #CCC;
  font-weight: 400;
}
#menu-left .content .fa {
  padding: 0 8px 0 4px;
}
#menu-left .content ul:last-child {
  color: #656565;
}
#menu-left .content ul:last-child .fa {
  color: #151515;
}
#menu-right li  img  {
  border: 1px solid #111;
  border-radius: 5px;
  display: inline-block;
  height: 32px;
  vertical-align: middle;
  width: 32px;
}
#menu-left .profile span, #menu-right li span {
  *font-weight: bold;
  padding-left: 8px;
}
#menu-right li span {
  font-size: 12px;
}
#menu-right li span:before {
  background: transparent;
  border: 1px solid #000;
  border-radius: 100%;
  box-shadow: inset 0 2px 5px #FFF;
  content: "";
  margin-top: -4px;
  padding: 6px;
  position: absolute;
  top: 50%;
  right: 10px;
}
#menu-right li span.active:before {
  background: #00CC0B;
}
#menu-right li span.so-active:before {
  background: #FF5B06;
}
#menu-right li span.no-active:before {
  background: transparent;
  border: 0;
  box-shadow: none;
}
#menu-right {
  border-left: 1px solid #161616;
  top: 57px;
  right: 0;
  width: 205px;
  -webkit-transform: translate3d(205px,0,0);
     -moz-transform: translate3d(205px,0,0);
          transform: translate3d(205px,0,0);
  -webkit-transition: all 500ms ease-in-out;
     -moz-transition: all 500ms ease-in-out;
          transition: all 500ms ease-in-out;
}
#menu-right.active {
  right: 0;
  -webkit-transform: translate3d(0,0,0);
     -moz-transform: translate3d(0,0,0);
          transform: translate3d(0,0,0);
  -webkit-transition: all 500ms ease-in-out;
     -moz-transition: all 500ms ease-in-out;
          transition: all 500ms ease-in-out;
}
section.active, header.active, .intro.active, #menu-left.active, footer.active {
  -webkit-transform: translate3d(205px,0,0);
     -moz-transform: translate3d(205px,0,0);
          transform: translate3d(205px,0,0);
  -webkit-transition: all 500ms ease-in-out;
     -moz-transition: all 500ms ease-in-out;
          transition: all 500ms ease-in-out;
}
#footer.active {
  -webkit-transform: translate3d(205px,0,0);
     -moz-transform: translate3d(205px,0,0);
          transform: translate3d(205px,0,0);
  -webkit-transition: all 500ms ease-in-out;
     -moz-transition: all 500ms ease-in-out;
          transition: all 500ms ease-in-out;
}
section, article {
  background: inherit;
  overflow: hidden;
  position: relative;
  text-align: center;
  width: initial;
}
article img {
  display: block;
  width: 100%
}
article:nth-child(2n) {
  -webkit-transform: rotate3d(0,1,0,180deg);
     -moz-transform: rotate3d(0,1,0,180deg);
          transform: rotate3d(0,1,0,180deg);
}
article:nth-child(2n) .box .feed {
  -webkit-transform: rotate3d(0,1,0,180deg);
     -moz-transform: rotate3d(0,1,0,180deg);
          transform: rotate3d(0,1,0,180deg);
}
article .box:before {
  background: #FFF;
  content: "";
  margin-top: -10px;
  position: absolute;
  width: 20px;
  height: 20px;
  top: 50%;
  left: -10px;
  -webkit-transform: rotate(45deg);
     -moz-transform: rotate(45deg);
          transform: rotate(45deg);
}
.box .feed {
  height: 100%;
  overflow: hidden;
  padding: 0 10px;
  position: relative;
  width: initial;
}
.box .feed h2 {
  color: #FF5B06;
  font-weight: 400;
  letter-spacing: 1px;
}
.box .feed p {
  color: #555;
  letter-spacing: 1px;
  line-height: 22px;
}
footer {
  background: inherit;
  position: relative;
  text-align: center;
  z-index: 2;
  -webkit-transform: translate3d(0,0,0);
     -moz-transform: translate3d(,0,0);
          transform: translate3d(0,0,0);
  -webkit-transition: all 500ms ease-in-out;
     -moz-transition: all 500ms ease-in-out;
          transition: all 500ms ease-in-out;
}
footer h2 {
  margin: 60px 10px 20px;
}
footer p {
  color: #656565;
  margin: 10px auto 50px;
  text-align: center;
  max-width: 500px;
}
footer .footer, .footer-02 {
  height: 100%;
  padding: 40px 10px 20px;
}
footer .footer {background: #111;}
footer .footer-02 {background: #000;}
footer .container {
  background: #050505;
  height: 100%;
  overflow: hidden;
  position: relative;
  width: 100%;
}


/*-------------------------------*/
/* CSS @media screen responsive
/*-------------------------------*/

@media only screen and (min-width: 240px) {
  article .box .feed p {font-size: 13px;}
}
@media only screen and (min-width: 320px) {
  article .box .feed p {font-size: 13px;}
}
@media only screen and (min-width: 480px) {  
  article .box .feed p {font-size: 14px;}
}
@media only screen and (min-width: 528px) {
  article .box, footer .box {
    display: inline-block;
    margin: 0 -2px;
    vertical-align: middle;
    width: 50%;
  }
  article .box .feed p {
    font-size: 11px;
    line-height: 16px;
  }
  footer .box {height: 100%;}
}
@media only screen and (min-width: 640px) {
  article .box {
    display: inline-block;
    margin: 0 -2px;
    vertical-align: middle;
    width: 50%;
  }
  article .box .feed p {font-size: 13px;}
  footer .box {height: 100%;}
}
@media only screen and (min-width: 768px) {
  article .box {
    display: inline-block;
    margin: 0 -2px;
    vertical-align: middle;
    width: 50%;
  }
  article .box .feed p {font-size: 15px;line-height: 24px;}
  footer .box {height: 100%;}
}
@media only screen and (min-width: 992px) {
  article .box {
    display: inline-block;
    margin: 0 -2px;
    vertical-align: middle;
    width: 50%;
  }
  article .box .feed p {font-size: 15px;line-height: 24px;}
  footer .box {height: 100%;}
}
@media only screen and (min-width: 1200px) {
  article .box {
    display: inline-block;
    margin: 0 -2px;
    vertical-align: middle;
    width: 50%;
  }
  article .box .feed p {font-size: 15px;line-height: 24px;}
  footer .box {height: 100%;}
}
@media only screen and (min-width: 767px) and (max-width: 991px) {
  article .box {
    display: inline-block;
    margin: 0 -2px;
    vertical-align: middle;
    width: 50%;
  }
  article .box .feed p {font-size: 15px;line-height: 24px;}
  footer .box {height: 100%;}
}
@media only screen and (max-width: 528px) {  
  article .box .feed {margin: 15px 0 25px;}
  article .box:before {
  content: "";
  margin-top: 0;
  margin-left: -10px;
  position: absolute;
  width: 20px;
  height: 20px;
  top: -10px;
  left: 50% !important;
  right: auto;
  -webkit-transform: rotate(45deg);
     -moz-transform: rotate(45deg);
          transform: rotate(45deg);
}
}
@media only screen and (max-width: 240px) {
  .intro {display: none}
  section {margin-top: 57px}
  article .box .feed p {font-size: 12px;}
}
a:hover, a:focus {
    color: #ffff;
    text-decoration: none;
}
</style>

<header>
  <span class="menu-left"><i class="fa fa-navicon"></i></span>
 <!--  <div><h1></h1></div> -->
  <span class="menu-right" style="
    /* float: left; */
    float: right;
"><i class="fa fa-list-ul"></i></span>
</header>

<div id="menu-left">
  <div class="box" style="background-image: url('http://172.105.58.115/public/assets/img/road.png');" data-image="http://172.105.58.115/public/assets/img/road.png">
    <div class="profile">
      <img src="http://172.105.58.115/public/logo/logo.png" alt="" /><span></span>
    </div>
    <div class="content">
    	 <ul class="nav">
          <?php if($menu->u_category <= 3){ ?>
            <li class="nav-item active ">
              <a class="nav-link" href="{{url('/')}}/dashboard">
                <i class="material-icons">dashboard</i>
                Dashboard 
              </a>
            </li>
          <?php } ?>
        <?php if($menu->u_category == 1){ ?>
          <li class="nav-item " <?php if(!empty($conf_open)){ ?> style="background-color: transparent;" <?php } ?>>
            <a class="nav-link" data-toggle="collapse" style="<?php if(!empty($conf_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" href="#componentsExamples" <?php if(!empty($conf_open)){ ?> aria-expanded="true" <?php } ?>>
              <i class="material-icons">apps</i>
               Configuration
                <b class="caret"></b>
              
            </a>
            <div class="collapse <?php if(!empty($conf_open)){ ?> show <?php } ?>" id="componentsExamples">
              <ul class="nav">

                <li class="nav-item ">
                  <a style="<?php if(!empty($new_config)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/new-config">
                    <span class="sidebar-mini"> NC </span>
                    <span class="sidebar-normal"> New Configuration </span>
                  </a>
                </li>
                <li class="nav-item ">
                  <a style="<?php if(!empty($allot_config)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/allot-config">
                    <span class="sidebar-mini"> ACL </span>
                    <span class="sidebar-normal"> Allot Configuration </span>
                  </a>
                </li>
         
              </ul>
            </div>
          </li>
        <?php } ?>
        <?php if($menu->masters == 1){ ?>

          <li class="nav-item " <?php if(!empty($mast_open)){ ?> style="background-color: transparent;" <?php } ?>>
            <a style="<?php if(!empty($mast_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" data-toggle="collapse" href="#pagesExamples" <?php if(!empty($mast_open)){ ?> aria-expanded="true" <?php } ?>> 
              <i class="material-icons">perm_identity</i>
               Masters
                <b class="caret"></b>
              
            </a>
            <div class="collapse <?php if(!empty($mast_open)){ ?> show <?php } ?>" id="pagesExamples">
              <ul class="nav">
              <?php if($menu->users == 1){ ?>
                <li class="nav-item ">
                  <a style="<?php if(!empty($user_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/user-list">
                    <span class="sidebar-mini"> UL </span>
                    <span class="sidebar-normal">Users </span>
                  </a>
                </li>
              <?php } ?>
              <?php if($menu->device == 1){ ?>
                <li class="nav-item ">
                        <a style="<?php if(!empty($devi_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/device-list">
                          <span class="sidebar-mini"> ALC </span>
                          <span class="sidebar-normal">Devices</span>
                        </a>
                </li>
              <?php } ?>
              <?php if($menu->activation_code == 1){ ?>
                <li class="nav-item ">
                    <a style="<?php if(!empty($acti_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/activation-code">
                        <span class="sidebar-mini"> AC </span>
                        <span class="sidebar-normal">Activation Code<span>
                    </a>
                </li>
                <?php } ?>
                <?php if($menu->u_category == 1 || $menu->u_category == 2){ ?>
                 <li style="<?php if(!empty($devipen_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-item ">
                        <a class="nav-link" href="{{url('/')}}/device-pending">
                          <span class="sidebar-mini"> DP </span>
                          <span class="sidebar-normal">Device Pending</span>
                        </a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </li>
         <?php } ?>
          <?php if($menu->settings == 1){ ?>
          <li class="nav-item " <?php if(!empty($sest_open)){ ?> style="background-color: transparent;" <?php } ?>>
            <a style="<?php if(!empty($sest_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" data-toggle="collapse" href="#componentsSettings" <?php if(!empty($sest_open)){ ?> aria-expanded="true" <?php } ?>>
              <i class="material-icons">settings</i>
              Settings
                <b class="caret"></b>
              
            </a>
            <div class="collapse <?php if(!empty($sest_open)){ ?> show <?php } ?>" id="componentsSettings">
              <ul class="nav">
                <?php if($menu->system == 1){ ?>
                <li class="nav-item ">
                  <a style="<?php if(!empty($syst_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/system-settings">
                    <span class="sidebar-mini"> SYS </span>
                    <span class="sidebar-normal"> System </span>
                  </a>
                </li>
                 <?php } ?>
                 <?php if($menu->vehicle == 1){ ?>
                <li class="nav-item ">
                  <a style="<?php if(!empty($vehs_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/vehicle-settings">
                    <span class="sidebar-mini"> VEH </span>
                    <span class="sidebar-normal"> Vehicle </span>
                  </a>
                </li>
                 <?php } ?>
                 <?php if($menu->vehspic == 1){ ?>
                <li class="nav-item ">
                  <a style="<?php if(!empty($spec_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/vehicle-specification">
                    <span class="sidebar-mini"> VSPC </span>
                    <span class="sidebar-normal"> Specification </span>
                  </a>
                </li>
               <?php } ?>
                 <?php if($menu->data == 1){ ?>
                <li class="nav-item ">
                  <a style="<?php if(!empty($ddt_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" data-toggle="collapse" href="#dataExample">
                    Data
                      <b class="caret"></b>
                    
                  </a>
                  <div class="collapse <?php if(!empty($ddt_open)){ ?> show <?php } ?>" id="dataExample">
                    <ul class="nav">
                      <?php if($menu->core_part == 1){ ?>
                      <li class="nav-item ">
                        <a style="<?php if(!empty($cr_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/data-core-list">
                          <span class="sidebar-mini">CP</span>
                          <span class="sidebar-normal">Core Part</span>
                        </a>
                      </li>
                      <?php } ?>
                      <?php if($menu->value_part == 1){ ?>
                       <li class="nav-item">
                        <a style="<?php if(!empty($vl_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/data-value-list">
                          <span class="sidebar-mini">VP</span>
                          <span class="sidebar-normal">Value Part</span>
                        </a>
                      </li>
                       <?php } ?>
                    </ul>
                  </div>
                </li> 
                <?php } ?>
                <?php if($menu->service_sett == 1){ ?>
                 <li class="nav-item ">
                  <a style="<?php if(!empty($ss_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" data-toggle="collapse" href="#serviceExample" <?php if(!empty($ss_open)){ ?> aria-expanded="true" <?php } ?>>
                     Service Setting
                      <b class="caret"></b>
                   
                  </a>
                  <div class="collapse <?php if(!empty($ss_open)){ ?> show <?php } ?>" id="serviceExample">
                    <ul class="nav">
                     <?php if($menu->descrp_sett == 1){ ?>
                      <li class="nav-item ">
                        <a style="<?php if(!empty($sst_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/desc-list">
                          <span class="sidebar-mini">DT</span>
                          <span class="sidebar-normal">Description Type</span>
                        </a>
                      </li>
                      <?php } ?>
                    </ul>
                  </div>
                </li>
                <?php } ?>
                <?php if($menu->alertmail == 1){ ?>
                <li class="nav-item ">
                    <a style="<?php if(!empty($alr_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/alert-mail-list">
                        <span class="sidebar-mini"> ALR </span>
                        <span class="sidebar-normal"> Alerts Mail</span>
                    </a>
                </li>
                <?php } ?>
                <?php if($menu->dbase == 1){ ?>
                <li class="nav-item ">
                    <a style="<?php if(!empty($db_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/database-optimization">
                        <span class="sidebar-mini"> DB </span>
                        <span class="sidebar-normal">Database</span>
                    </a>
                </li>
                <?php } ?>
              </ul>
            </div>
          </li>
          <?php } ?>

          <?php if($menu->map == 1){ ?>
          <li class="nav-item ">
            <a style="<?php if(!empty($map_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/summary">
              <i class="material-icons">menu_book</i>
               Summary
            </a>
          </li>
          <?php } ?>

          <?php if($menu->eva == 1){ ?>
          <li class="nav-item ">
            <a style="<?php if(!empty($eva_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/eva">
              <i class="material-icons">show_chart</i>
              EVA 
             
            </a>
          </li>
          <?php } ?>

          <?php if($menu->map == 1){ ?>
          <li class="nav-item ">
            <a style="<?php if(!empty($map_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/map">
              <i class="material-icons">map</i>
              Map &nbsp; &nbsp;&nbsp;
            </a>
          </li>
          <?php } ?>

          <?php if($menu->drive == 1){ ?>
          <li class="nav-item ">
            <a style="<?php if(!empty($drive)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/drive">
              <i class="material-icons">drive_eta</i>
              Drive
            </a>
          </li>
          <?php } ?>

          <?php if($menu->drivesd == 1){ ?>
          <li class="nav-item ">
            <a style="<?php if(!empty($drivesd)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/drivesd">
              <i class="material-icons">directions</i>
               Drive Plot
            </a>
          </li>
          <?php } ?>

          <?php if($menu->service == 1){ ?>
          <li class="nav-item ">
            <a style="<?php if(!empty($serv_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/services">
              <i class="material-icons">rv_hookup</i>
              Services
            </a>
          </li>
          <?php } ?> 

           <?php if($menu->statistics == 1){ ?>
          <li class="nav-item">
            <a style="<?php if(!empty($stati_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/statistics">
              <i class="material-icons">bar_chart</i>
              Statistics
            </a>
          </li>
          <?php } ?>

          <?php if($menu->report == 1){ ?>
          <li class="nav-item">
            <a style="<?php if(!empty($repo_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/reports">
              <i class="material-icons">library_books</i>
              Reports
            </a>
          </li>
          <?php } ?>

          <?php if($menu->glossary == 1){ ?>
          <li class="nav-item">
            <a style="<?php if(!empty($gllr_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/glossary-list">
              <i class="material-icons">border_color</i>
             Glossary
            </a>
          </li>
          <?php } ?>

          <?php if($menu->digno == 1){ ?>
              <li class="nav-item">
                <a style="<?php if(!empty($digi)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/remote-diagnostics">
                  <i class="material-icons">network_check</i>
                   Diagnostics
                </a>
              </li>
          <?php } ?>

          <?php if($menu->alert == 1){ ?>
          <li class="nav-item">
           <a style="<?php if(!empty($allr_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/alerts">
             <i class="material-icons">notification_important</i>
             Alerts 
           </a>
         </li>
          <?php } ?>
          
          <?php if($menu->action == 1){ ?>
          <li class="nav-item ">
            <a style="<?php if(!empty($actio_open)){ ?> background-color: rgba(200, 200, 200, 0.2); <?php } ?>" class="nav-link" href="{{url('/')}}/actions">
              <i class="material-icons">undo</i>
              Actions
            </a>
          </li>
          <?php } ?>
  
        </ul>


   
   </div>
  </div>
</div>
<div id="menu-right">
  <div class="box">
    <ul>
      <li><a href=""><img src="https://goo.gl/qqppkW" alt="" /><span class="active">Candice Swanepoel</span></a></li>
      <li><a href=""><img src="https://goo.gl/Dg2nY2" alt="" /><span class="active">Miranda Kerr</span></a></li>
      <li><a href=""><img src="https://goo.gl/2c2maA" alt="" /><span class="so-active">Alessandra Ambrosio</span></a></li>
      <li><a href=""><img src="https://goo.gl/axHPyj" alt="" /><span class="active">Doutzen Kroes</span></a></li>
      <li><a href=""><img src="https://goo.gl/PYJic3" alt="" /><span class="no-active">Elsa Hosk</span></a></li>
    </ul>
  </div>
</div>
<div class="wrapper ">

<script src="public/assets/js/core/jquery.min.js"></script>
  <script src="public/assets/js/core/popper.min.js"></script>
  <script src="public/assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="public/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="public/assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="public/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="public/assets/js/material-dashboard.js?v=2.1.0" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="public/assets/demo/demo.js"></script>
  <script src="public/assets/js/ebs.js"></script>
  <script src="public/assets/js/ebsformat.js"></script>
  <script src="public/assets/js/ebsmd.js"></script>
<script type="text/javascript">
  $( document ).ready(function() {
  (function(){
  $('.menu-left').click(function(){
    $('header').toggleClass('active')
    $('.intro').toggleClass('active')
    $('section').toggleClass('active')
    $('#menu-left').toggleClass('active')
    $('footer').toggleClass('active')
  })
  $('.menu-right').click(function(){$('#menu-right').toggleClass('active')})
})()
});
</script>