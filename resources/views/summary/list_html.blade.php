<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
<script type="text/javascript" src="tableExport.js"></script>
<script type="text/javascript" src="jquery.base64.js"></script>
<script type="text/javascript" src="html2canvas.js"></script>
<script type="text/javascript" src="jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="jspdf/jspdf.js"></script>
<script type="text/javascript" src="jspdf/libs/base64.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<div class="row" style="height:300px;overflow:scroll;">
                        <table id="employees" class="table table-striped">
  <tr>
    <td >DAILY DRIVE SUMMARY TABLE</h2></td>
  </tr>
  <tr>
    <?php $start = explode(" ",$start); ?>
    <?php $end   = explode(" ",$end); ?>
    <td colspan="27">Drive summary of Date : {{$start[0]}} to {{$end[0]}}</td>
  </tr>
  <thead>  
  <tr>
    <td></td>
    <td colspan="3" width="100"><h6>Vehicle identification</h6></td>
    <td colspan="8" width="100">General Usage</td>
    <td colspan="7" width="100"><h6>Engine usage</h6></td>
    <td colspan="3" width="100"><h6>Transmission</h6></td>
    <td colspan="5" width="100"><h6>Health prognosis</h6></td>
  </tr>
  <tr style="background-color: #31A6CA;">
    <th style="width: 50;"><b>Sr.No</b></th>
    <th width="100"><b>Model</b></th>
    <th width="100"><b>Reg No</b></th>
    <th><b>Location</b></th>
    <th><b>Odometer Reading</b></th>
    <th><b>Total Distance Covered</b></th>
    <th><b>Overall Health Score</b></th>
    <th><b>Driver Score</b></th>
    <th><b>Ride Analysis</b></th>
    <th><b>Traffic Condition</b></th>

    <th><b>Mileage</b></th>
    <th><b>Fuel Wastage</b></th>
    <th><b>No. of Engine Starts</b></th>
    <th><b>Starting Trouble</b></th>
    <th><b>Max Vehicle Speed</b></th>
    <th><b>Average Vehicle Speed</b></th>
    <th><b>Max Engine RPM</b></th>
    <th><b>Average Engine RPM</b></th>
    <th><b>Max Engine Oil Temperature</b></th>
    <th><b>No Of Gear Shifts / 100 Km</b></th>

    <th><b>Maximum Used Gear</b></th>
    <th><b>No Of Alerts Generated</b></th>
    <th><b>Battery Health</b></th>
    <th><b>Injector And Spark Plug</b></th>
    <th><b>Air Filter Health</b></th>
    <th><b>Oil Health</b></th>
    <th><b>Cooling System Health</b></th>

  </tr>
<thead>  
    @if (!empty($device_list))
         <?php $id = 1; ?>
         @foreach ($device_list as $key => $device)  
        <tr>
            <td>{{$id}}</td>
            <td>{{$device_model_list[$key]}}</td>
            <td>{{$device->reg_no}}</td>
            <td>-</td>
            <td>{{round($device->total_dist + $device->travel_purchase,2)}}</td>
            <td>{{$device_total_distance[$key]}}</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>{{$device_traffic_condition[$key]}}</td>

            <td>{{$device_milage[$key]}}</td>
            <td>{{$device_idle_time[$key]}}</td>
            <td>{{$device_engine_start[$key]}}</td>
            <td>{{$device_crank_3[$key]}}</td>
            <td>{{$device_max_speed[$key]}}</td>
            <td>{{$device_average_speed[$key]}}</td>
            <td>{{$device_max_rpm[$key]}}</td>
            <td>{{$device_avg_rpm[$key]}}</td>
            <td>{{$device_max_cool[$key]}}</td>
            <td>{{$device_gear_changes[$key]}}</td>

            <td>-</td>
            <td>-</td>
            <td>{{$device_bat_health[$key]}}</td>
            <td>{{$device_inj_health[$key]}}</td>
            <td>{{$device_air_health[$key]}}</td>
            <td>{{$device_oil_health[$key]}}</td>
            <td>{{$device_coolant_health[$key]}}</td>
         </tr>
        <?php $id++; ?>  
         @endforeach
         @else
         <tr>
            <td colspan="27" align="center">No User Exist.</td>
         </tr>
         @endif
</table>
</div>
<li><a href="#" onclick="$('#employees').tableExport({type:'json',escape:'false'});"> <img src="images/json.jpg" width="24px"> JSON</a></li>
                                <li><a href="#" onclick="$('#employees').tableExport({type:'json',escape:'false'});"><img src="images/json.jpg" width="24px">JSON (ignoreColumn)</a></li>
