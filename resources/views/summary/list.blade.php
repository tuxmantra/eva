<?php 
use App\Model\Fetch;
?>
<div class="toolbar">
  
</div>
<div class="material-datatables">
   <table id="datatables" class="table table-striped table-no-bordered table-hover"  cellspacing="0" width="100%" style="width:100%">
       @if (!empty($reports))
      <thead>
         <tr>
            <th></th>
            <th class="table-heading-font">S.No</th>
            <th class="table-heading-font">Date</th>
            <th class="table-heading-font">User Name</th>
            <th class="table-heading-font">Vehicle</th>
            <th class="table-heading-font">VIN No</th>
            <th class="table-heading-font">Operator</th>
            <th class="table-heading-font ">Mileage</th>
            <th class="table-heading-font ">Min Mileage</th>
            <th class="table-heading-font ">Max Mileage</th>
            <th class="table-heading-font">Result</th>
            <th class="table-heading-font ">Test Status</th>
            <th class="table-heading-font none">Temp</th>
            <th class="table-heading-font none">Pressure</th>
            <th class="table-heading-font none">Humidity</th>
            <th class="table-heading-font none">Torque</th>
            <th class="table-heading-font none">Ref Torque</th>
            <th class="table-heading-font none">Distance</th>
            <th class="table-heading-font none">Fuel Consumed</th>
            <th class="table-heading-font none">Max Battery Voltage</th>
            <th class="table-heading-font none">Min Battery Voltage</th>
            <th class="table-heading-font none">Coolant Start</th>
            <th class="table-heading-font none">Coolant End</th>
            <th class="table-heading-font none">IAT Start</th>
            <th class="table-heading-font none">IAT End</th>
         </tr>
      </thead>
            
      <tbody>
        
         <?php $id = 1; ?>
         @foreach ($reports as $key => $report)  
         <?php 
            $user = Fetch::getUserDeviceEmail($report->device_id);
            // print_r($user);die;
         ?>
         <tr>
            <td class="table-heading-font" style="background-color: #f5f5f5;"></td>
            <td style="background-color: #ffffff;" class="table-heading-font">{{$id}}</td>
            <td class="table-heading-font" style="background-color: #ffffff;;">{{$report->dt}}</td>
            <td style="background-color: #f5f5f5;;" class="table-heading-font">{{$user[0]->uname}}</td>
            <td style="background-color: #ffffff;" class="table-heading-font">{{$report->vehicle}}</td>
            <td class="table-heading-font" style="background-color: #f5f5f5;">{{$report->vin_no}}</td>
            <td class="table-heading-font" style="background-color: #ffffff;">{{$report->rider_name}}</td>
            <td class="text-right" style="background-color: #f5f5f5;">{{$report->milage}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->min_milage}}</td>
            <td class="text-right" style="background-color: #f5f5f5;">{{$report->max_milage}}</td>
            <td class="table-heading-font" style="background-color: #ffffff;"><?php if($report->test_result){ echo "Passed";}else{echo "Failed";}?></td>
            <td class="text-right" style="background-color: #f5f5f5;"><?php if($report->test_status){ echo "Completed";}else{ echo "Not Completed";} ?></td>
            <td class="table-heading-font" style="background-color: #f5f5f5;;">{{$report->temp}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->pressure}}</td>
            <td style="background-color: #f5f5f5;" class="table-heading-font">{{$report->humidity}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->torque}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->ref_torque}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->distance}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->fuel_cons}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->max_bt_vol}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->min_bt_vol}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->coolant_start}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->coolant_end}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->iat_start}}</td>
            <td class="text-right" style="background-color: #ffffff;">{{$report->iat_end}}</td>
         </tr>
         <?php $id++; ?>  
         @endforeach
         @else
         <tr>
            <td colspan="25" align="center">No Data Avilable</td>
         </tr>
         
      </tbody>
      @endif
   </table>
</div>

