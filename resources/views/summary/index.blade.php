  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


  <style type="text/css">
     
      @keyframes spinner {
      0% {
      transform: translate3d(-50%, -50%, 0) rotate(0deg);
      }
      100% {
      transform: translate3d(-50%, -50%, 0) rotate(360deg);
      }
      }
      .spin::before {
     animation: 1.5s linear infinite spinner;
    animation-play-state: inherit;
    border: solid 2px #c1d2ce;
    border-bottom-color: #0e0e0e;
    border-radius: 64%;
    content: "";
    height: 21px;
    width: 21px;
    position: absolute;
    top: 49%;
    left: 18%;
    margin-right: 2%
    transform: translate3d(-50%, -50%, 0);
    will-change: transform;
      }
  
table.dataTable thead .sorting:after,
table.dataTable thead .sorting:before,
table.dataTable thead .sorting_asc:after,
table.dataTable thead .sorting_asc:before,
table.dataTable thead .sorting_asc_disabled:after,
table.dataTable thead .sorting_asc_disabled:before,
table.dataTable thead .sorting_desc:after,
table.dataTable thead .sorting_desc:before,
table.dataTable thead .sorting_desc_disabled:after,
table.dataTable thead .sorting_desc_disabled:before {
bottom: .5em;
}


    .table-heading-font{
        font-size: 13px !important;
    }

    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    #datetimepicker12 .datepicker-inline {
    width: 335px !important;
    }
    table.table-condensed {
    width: 100%;
   }
/*    tr div {
      width: auto !important;
      z-index: auto !important;
    }*/
/*    .tab-pane tbody {
      display: block;
      max-height: 230px;
      overflow-y: scroll;
    }*/
    .ScrollStyle
    {
        max-height: 300px;
        overflow-y: scroll;
    }
    #map{
          background-color: aliceblue;
          margin-top: 30px !important;
    }
    .divTop{
      margin-top: 35px;
    }

.non-highlighted-cal-dates{
  background-color: gainsboro;
}
.dt-buttons
{
  margin-bottom: 25px !important;
  float: right;
}
.btn-group>.btn:not(:last-child):not(.dropdown-toggle), .btn-group>.btn-group:not(:last-child)>.btn 
{
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    margin-left: -1px;
    margin-right: 21px;
    background-color: #eb262d;
    color: #fff;
}
.buttons-excel
{
  background-color: #eb262d !important;
  color: #fff !important;
}
table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child, table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>th:first-child {
    position: relative;
    padding-left: 30px !important;
    cursor: pointer;
    padding: 20px !important;
}
.dataTables_filter
    {
      display: none;
    }
.table {
    margin-top: -65px;
}
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"> 
                    
              </span>
            </div>
         
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">menu_book</i>
                  </div>
                  <h4 class="card-title">Summary</h4>
                  <?php  ?>
                  <button class="btn btn-primary btn-sm" name="Select Date" onClick="window.location.reload();" value="" style="float: right;margin-top: -20px; margin-right: 43px;" >Reset</button>
                </div>

                <input type="hidden" name="hiddenStart1" id="hiddenStart1" value="">
                <input type="hidden" name="hiddenEnd2" id="hiddenEnd2" value="">
                <div class="card-body">
                  <div class="col-md-12">
                    <div class="card">
                      
                      <div class="col-md-12">
                      	<div class="row" style="margin-top: 20px">
                      		
	                       <div class="col-md-3">
	                       	<select class="selectpicker valueType1" data-style="select-with-transition"  data-container="body" title="Select Vehicle" data-size="7" name="vehicle" id="vehicle" >
	                          <option class="dropdown-item" value="" selected>All Vehicles</option>
	                          <?php if($vehiclelist){ foreach ($vehiclelist as $key => $vahicle) if(!empty($vahicle->vehicle)){{ ?>
	                            <option class="dropdown-item" <?php if(count($vehiclelist) == 1){echo "selected";} ?> value="<?=$vahicle->vehicle?>"><?=$vahicle->vehicle?></option>
	                          <?php }}} ?>
	                         </select>
	                       </div>
	                       <div class="col-md-3">
	                       	<select class="selectpicker valueType1" data-style="select-with-transition"  data-container="body" title="Select rider" data-size="7" name="rider" id="rider" >
	                          <option class="dropdown-item" value="" selected>All Users</option>
	                          <?php if($rider){ foreach ($rider as $key => $ridern) if(!empty($ridern->uname)){{ ?>
	                            <option class="dropdown-item" value="<?=$ridern->uid?>"><?=$ridern->uname?></option>
	                          <?php }}} ?>
	                         </select>
	                       </div>
	                       <div class="col-md-3">
	                       	<select class="selectpicker valueType1" data-style="select-with-transition"  data-container="body" title="Select result" data-size="7" name="result" id="result" >
	                          <option class="dropdown-item" value="" selected>All Result</option>
	                            <option class="dropdown-item" value="1">Passed</option>
	                            <option class="dropdown-item" value="2">Failed</option>
	                         </select>
	                       </div>
                         <div class="col-md-3">
                            <button class="btn btn-primary btn-sm" name="Select Date" id='btnDate' onclick="calldate()" value="" style="margin-top: 4px;float: right;margin-right: 9px;" >Select Date</button>
                            <input class="form-control" type="text" name="daterange" id='daterange' value="" style="margin-top: 4px; display: none" />
                         </div>
                       </div>
                      </div>
                       <div class="loader" id="loader" style="display: none;">Loading...</div>
                      <div class="card-body" id="load_data">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript">


$(document).ready(function() {

     $('#loader').show();
    $('input[name="dates"]').daterangepicker();
    $.post('{{url('/')}}/getSummaryLoad').done(function (data) {
       $('#loader').hide();
        $('#load_data').html(data);
        $('#datatables').DataTable({
          dom: 'Bfrtip',
        buttons: [
            
            {
                extend: 'excel',
                title: 'Summary-<?=date('Y-m-d')?>',
                text: 'Export to excel',
                className: 'btn btn-primary btn-sm'
            }
        ],
          "pagingType": "full_numbers",
          "columnDefs": [
              { 
               "searchable": false, 
               "targets": [0,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]
              }
          ],
          "lengthMenu": [
              [10, 25, 50, -1],
              [10, 25, 50, "All"]
          ],
          responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[0]+' '+data[1];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'ui table'
                } )
            }
        }
        });
        
    });
}); 

$(function() {
  $('input[name="daterange"]').daterangepicker({
    maxDate: new Date(),
    dateLimit: { days : 90 },
    opens: 'left',
  }, function(start, end, label) {
    $('#load_data').hide();
    $('#loader').show();
    $('#hiddenStart1').val(start.format('YYYY-MM-DD'));
     $('#hiddenEnd2').val(end.format('YYYY-MM-DD'));
    var file_name    = 'Summary from '+start.format('YYYY-MM-DD')+' to '+end.format('YYYY-MM-DD'); 
     var start        = start.format('YYYY-MM-DD 00:00:00');
     var end          = end.format('YYYY-MM-DD 23:59:59');

     var vehcile = $('#vehicle').val();
     var rider   = $('#rider').val();
     var result  = $('#result').val();

     var value_search = $('.dataTables_filter input').val();
     console.log(value_search);
      $('#btnSubmit').hide();
      $('#btnSubmit1').hide();
      $('#loadbutton').show();
     
     if(start != '' && end != ''){
          $.post('{{url('/')}}/getSummaryLoad', { 'start': start, 'end': end, 'vehcile': vehcile,'rider':rider,'result':result }).done(function (data) {
              $('#load_data').html(data);
              $('#loader').hide();
              $('#load_data').show();
              $('#btnSubmit').show();
              $('#loadbutton').hide();
              $('#datatables').DataTable({
                dom: 'Bfrtip',
                buttons: [
                   
                  {
                      extend: 'excel',
                      title: file_name,
                      text: 'Export to excel',
                      className: 'btn btn-primary btn-sm'
                  }
                ],
                "pagingType": "full_numbers",
                "columnDefs": [
                    { 
                     "searchable": false, 
                     "targets": [0,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
                    }
                ],
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[0]+' '+data[1];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'ui table'
                } )
            }
        }
              });
          });
         
          
     }
  });
}); 


$( ".valueType1" ).change(function() 
{
    
    var start        = $('#hiddenStart1').val();
    var end          = $('#hiddenEnd2').val();
    var file_name    = 'Summary from '+start+' to '+end; 
     
     var vehcile = $('#vehicle').val();
     var rider   = $('#rider').val();
     var result  = $('#result').val();
     // console.log(value_search);
      $('#btnSubmit').hide();
      $('#btnSubmit1').hide();
      $('#loadbutton').show();
     
     	$('#load_data').hide();
    	$('#loader').show();
          $.post('{{url('/')}}/getSummaryLoad', { 'start': start, 'end': end, 'vehcile': vehcile,'rider':rider,'result':result }).done(function (data) {
              $('#load_data').html(data);
              $('#loader').hide();
              $('#load_data').show();
              $('#btnSubmit').show();
              $('#loadbutton').hide();
              $('#datatables').DataTable({
                dom: 'Bfrtip',
                buttons: [
                   
                {
                    extend: 'excel',
                    title: file_name,
                    text: 'Export to excel',
                    className: 'btn btn-primary btn-sm'
                }
                ],
                "pagingType": "full_numbers",
                "columnDefs": [
                    { 
                     "searchable": false, 
                     "targets": [0,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
                    }
                ],
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[0]+' '+data[1];
                    }
                    } ),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                        tableClass: 'ui table'
                    } )
                }
              }
              });
          });
         
});  

function calldate()
{
 
  $('#btnDate').hide();
  $('#daterange').show();
  // alert('Hi');
}
</script>