  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px;"></span>
            </div>
                
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Change Password</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <form method="post" action="save-password" class="form-horizontal" onsubmit="return validPassword()">
                  <input type="hidden" value="{{$form_token}}" name="form_token">
   
                    <div class="row">
                      <label class="col-sm-2 col-form-label">New Password</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input type="password" class="form-control" name="password" id="password" placeholder="Enter New Password">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Confirm Password</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input type="password" name="confir" id="confir" class="form-control" placeholder="Confirm Password">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label label-checkbox">Show</label>
                      <div class="col-sm-10 checkbox-radios">
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" id="show" name="show" onclick="changeFrace(this)" type="checkbox" value=""> &nbsp;
                            <span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
              
                  
                      </div>
                    </div>
                
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>


            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Change</button>

            </div>
          </div>
        </div>
      </div>
       </form>
     
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')