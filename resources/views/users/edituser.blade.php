  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
    

            <div class="col-md-12" align="right">
              <button title="Go Back" class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
                
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Edit User</h4>
                  </div>
                </div>

                <div class="card-body ">
                  <form method="post" action="{{url('/')}}/edit-user-save" autocomplete="off" class="form-horizontal" >
                    <div class="row">
                      <label class="col-sm-2 col-form-label">User Type</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input type="hidden" value="{{$form_token}}" name="form_token">
                          <input type="hidden" value="" name="partner_device_status" id="partner_device_status">
                          <input type="hidden" name="uid" value="{{App\Helpers\Helper::crypt(1,$user_data[0]->uid)}}">
                          <select class="selectpicker usertype_usercreate" data-style="select-with-transition" title="Select User Type" data-size="7" name="user_type" tabindex="-98">
                            <option disabled=""> User Type Options</option>
                            @if (!empty($user_type))
                            @foreach ($user_type as $user)
                            <option <?php if($user_data[0]->utype == $user->u_type){ ?> selected <?php } ?> value="{{$user->u_type}}">{{$user->u_name}}</option>
                            @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                    </div>
   
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Name</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="name" id="name" placeholder="Enter Name" value="{{$user_data[0]->uname}}">
                        </div>
                      </div>
                    </div>
                         <div class="row">
                      <label class="col-sm-2 col-form-label">Mobile</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="number" name="mobile" id="mobile" class="form-control" placeholder="Enter Mobile" value="{{$user_data[0]->umob}}">
                        </div>
                      </div>
                    </div>
                         <div class="row">
                      <label class="col-sm-2 col-form-label">Email</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="false" type="text" name="email" id="email" class="form-control" placeholder="Enter Email" value="{{$user_data[0]->uemail}}">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <label class="col-sm-2 col-form-label">Password</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input autocomplete="off" type="Password" class="form-control" name="password" id="password" placeholder="Enter Password" value="{{$user_data[0]->upwd}}">
                        </div>
                      </div>
                    </div>
                     <div class="row">
                      <label class="col-sm-2 col-form-label">Address</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <textarea autocomplete="off" class="form-control" cols="5" rows="5" name="address" id="address">{{$user_data[0]->ulocation}}</textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row" style="margin-top: 25px;">
                      <label class="col-sm-2 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-10 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" 
                            value="1" <?php if($user_data[0]->ustatus == 1){ ?> checked <?php } ?>> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0" <?php if($user_data[0]->ustatus == 0){ ?> checked <?php } ?>> In Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>

                    <div class="row" style="display: none;" id="partner_list">
                      <label class="col-sm-2 col-form-label">Partner</label>
                      <div class="col-sm-10">
                        
                        <div class="form-group" id="partner_list_inner">
                 
                        </div>
                      </div>
                    </div>

                    <div class="row" style="display: none;" id="device_list">
                      <label class="col-sm-2 col-form-label">Device Id</label>
                      <div class="col-sm-10">
                        <div class="form-group" id="device_list_inner">
                         
                      
                        </div>
                      </div>
                      <br><br>
                    </div>
                  

                     
                
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>

            </div>
                    <div class="col-md-12" align="center">
              
            </div>
          </div>
        </div>
      </div>
       </form>
    
    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
 @if (Session::has('message'))
  <script type="text/javascript">
            Swal.fire({
            type: 'info',
            title: 'Info',
            text: '{!! session('message') !!}',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })

  </script>
  @endif
 <script type="text/javascript">
  $( document ).ready(function() {  
     var part   = '<?= $user_data[0]->partner ?>';
     var device = '<?= $user_data[0]->device ?>';
     var utype = $(".usertype_usercreate option:selected").val();
     $.post('{{url('/')}}/getUserPartner', { utype: utype }).done(function (data) {
         if(data != '0'){
            $('#partner_list').show();
            $('#partner_list_inner').html(data);
            $('#partner').selectpicker('refresh');

            $('.partner option').each(function() {
              if($(this).val() == part) {
                $(this).attr("selected", true);
              }
            });
            $('#partner').selectpicker('refresh');
         }else{
           $('#partner_list').hide();
           $('#partner_list_inner').html('');
           $('#device_list').hide();
           $('#device_list_inner').html('');
         }
    });
    

    $.post('{{url('/')}}/getCategoryPartner', { utype: utype }).done(function (data) {
        if(data == '1'){
              $('#partner_device_status').val('1');
        }else{
              $('#partner_device_status').val('');
        }
    });


     $.post(url+'getPartnerDevice', { partner: part }).done(function (data) {
         if(data != '0'){
            $('#device_list').show();
            $('#device_list_inner').html(data);
            $('#device').selectpicker('refresh');
            $('.device option').each(function() {
              if($(this).val() == device) {
                $(this).attr("selected", true);
              }
            });
            $('#device').selectpicker('refresh');
         }else{
            $('#device_list').hide();
            $('#device_list_inner').html('');
         }
    });

  });

 </script>