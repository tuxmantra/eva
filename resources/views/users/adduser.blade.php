  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <style type="text/css">
    .form-control{
      margin-top: 15px;
    }
    .bmd-form-group .form-control, .bmd-form-group label, .bmd-form-group input {
    line-height: 1.1;
}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    /* width: 220px; */

}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
     width: 100%; 
}
  </style>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
   
            <div class="col-md-12" align="right">
              <button title="Go Back" class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Add User</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <form method="post" action="create-user?device={{$device}}&partner={{$partner}}" autocomplete="off" class="form-horizontal" name="registration">
                   
                    <div class="row">
                      <label for="name" class="col-sm-1 col-form-label">User Type</label>
                      <div class="col-sm-5">
                        <div class="form-group">
                         
                          <input type="hidden" value="{{$form_token}}" name="form_token">
                          <input type="hidden" value="" name="partner_device_status" id="partner_device_status">
                         
                          <select class="selectpicker usertype_usercreate" data-style="select-with-transition" title="Select User Type" data-size="7" name="user_type" id="user_type" tabindex="-98">
                            <option disabled=""> User Type Options</option>
                            @if (!empty($user_type))
                            @foreach ($user_type as $user)
                            <option value="{{$user->u_type}}">{{$user->u_name}}</option>
                            @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                  
                    <label for="name" class="col-sm-1 col-form-label">Name</label>
                      <div class="col-sm-5">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
                        </div>
                      </div>
                    </div>
                   <div class="row">
                       <label for="mobile" class="col-sm-1 col-form-label" >Mobile</label>
                      <div class="col-sm-5">
                        <div class="form-group">
                          <input autocomplete="off" type="number" name="mobile" id="mobile" class="form-control" placeholder="Enter Mobile">
                        </div>
                      </div>
                   
                      <label for="email" class="col-sm-1 col-form-label">Email</label>
                      <div class="col-sm-5">
                        <div class="form-group">
                          <input autocomplete="false" type="text" name="email" value="" id="email" class="form-control" placeholder="Enter Email">
                        </div>
                      </div>
                    </div>
                         <div class="row">
                      <label for="address" class="col-sm-1 col-form-label">Address</label>
                      <div class="col-sm-11">
                        <div class="form-group">
                          <textarea autocomplete="off" class="form-control" cols="2" rows="2" name="address" id="address"></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="row" >
                        <label for="password" class="col-sm-1 col-form-label">Password</label>
                      <div class="col-sm-5">
                        <div class="form-group">
                          <input autocomplete="off" type="Password" class="form-control" name="password" id="password" placeholder="Enter Password">
                        </div>
                      </div>
                      <label class="col-sm-1 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-5 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="1" checked> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0"> In Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>

                    <div class="row" style="display: none;" id="partner_list">
                      <label class="col-sm-1 col-form-label">Partner</label>
                      <div class="col-sm-5">
                        <div class="form-group" id="partner_list_inner">
                 
                        </div>
                      </div>
                    <div class="col-sm-5" style="display: none;" id="device_list">
                      <div class="row">
                      <label class="col-sm-2 col-form-label">Device Id</label>
                      <div class="col-sm-10">
                        <div class="form-group" id="device_list_inner">
                         
                      
                        </div>
                      </div>
                    </div>
                      <br><br>
                    </div>
                  </div>
                  

                     
                
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>

            </div>
                     <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                     <!--  @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif -->
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
            </div>
          </div>
        </div>
      </div>

       </form>

    </div>
  </div>

 
 @include('default.footer')
 @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif
  <script>
 $(document).ready(function() {
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='registration']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      name: "required",
      mobile: "required",
      address: "required",
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
      password: {
        required: true,
        minlength: 5
      }
    },
    // Specify validation error messages
    messages: {
      name: "Please enter your Name",
      address: "Please enter your Address",
      mobile: "Please enter your Mobile Number",
      password: {
      required: "Please provide a password",
      minlength: "Your password must be at least 5 characters long"
      },
      email: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      event.preventDefault();
       form.submit();
       // event.preventDefault();
    }

  });
});
});

 </script>