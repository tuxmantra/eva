  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <style type="text/css">
    .form-control{
      margin-top: 15px;
    }
    .bmd-form-group .form-control, .bmd-form-group label, .bmd-form-group input {
    line-height: 1.1;
}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    /* width: 220px; */

}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
     width: 100%; 
}
  </style>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
   
            <div class="col-md-12" align="right">
              <button title="Go Back" class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Add Operator</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <form method="post" action="create-operator" autocomplete="off" class="form-horizontal" name="registration">
                   
                    <div class="row">
                      
                    <label for="name" class="col-sm-1 col-form-label">Name</label>
                      <div class="col-sm-5">
                        <div class="form-group">
                          <input autocomplete="off" type="text" class="form-control" name="name" id="name" placeholder="Enter Operator Name">
                        </div>
                      </div>
                    </div>
                   <div class="row">
                       <label for="id" class="col-sm-1 col-form-label" >Operator ID</label>
                      <div class="col-sm-5">
                        <div class="form-group">
                          <input autocomplete="off" type="text" name="id" id="id" class="form-control" placeholder="Enter Operator ID">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                       <label for="id" class="col-sm-1 col-form-label" >Select Plant</label>
                       <div class="col-sm-5">
                        <div class="form-group">
                          <select class="selectpicker usertype_usercreate" data-style="select-with-transition" title="Select Plant" data-size="7" name="plant" id="plant" tabindex="-98">
                              <option disabled="">Plant</option>
                              <option value="1">HOSUR</option>
                              <option value="2">MYSORE</option>
                              <option value="3">HIMACHAL PRADESH</option>
                              <option value="4">3W HOSUR</option>
                              <option value="5">MAHABHARATH</option>
                              <option value="6">JAWA BARAT</option>
                          </select>
                        </div>
                      </div>
                    </div>
                   
                      <div class="row">
                      <label class="col-sm-1 col-form-label label-checkbox">Status</label>
                      <div class="col-sm-5 checkbox-radios">
                  
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="1" checked> Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="status" value="0"> In Active
                            <span class="circle">
                              <span class="check"></span>
                            </span>
                          </label>
                        </div>

                      </div>
                    </div>
                  
                </div>
              </div>
            </div>
                  
                    
      

            <div class="col-md-12" align="right">
              <button type="submit" class="btn btn-fill btn-rose">Save</button>

            </div>
                    

       </form>

    </div>
  </div>

 
 @include('default.footer')
 @if (Session::has('message'))
                      <script>
                         Swal.fire({
                            type: 'info',
                            title: 'info',
                            text:   '<?= session('message') ?>',
                             confirmButtonColor: '#eb262d',
                            // footer: '<a href>Why do I have this issue?</a>'
                          })
                      </script>
                           
                      @endif
  <script>
 $(document).ready(function() {
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='registration']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      name: "required",
      mobile: "required",
      address: "required",
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
      password: {
        required: true,
        minlength: 5
      }
    },
    // Specify validation error messages
    messages: {
      name: "Please enter Operator Name",
      id: "Please enter Operator ID",
      mobile: "Please enter your Mobile Number",
      password: {
      required: "Please provide a password",
      minlength: "Your password must be at least 5 characters long"
      },
      email: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      event.preventDefault();
       form.submit();
       // event.preventDefault();
    }

  });
});
});

 </script>