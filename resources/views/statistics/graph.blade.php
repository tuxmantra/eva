

                 <div class="col-md-12">
                   <canvas id="myChart"   
                  width="400" height="180"></canvas>
  </div>

                  
                 <script src="{{url('/')}}/public/assets/js/plugins/Chart.js"></script>
<script>
   $('select').selectpicker();
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: <?=$lables?>,
        datasets: [{
            label: '<?=$mak1?>',
            yLabel: '<?=$yLabel?>',
            yAxisID: "y-axis-0",
            data: <?=$values?>,
            barPercentage: 0.5,
            barThickness: 20,
            maxBarThickness: 20,
            minBarLength: 2,
            backgroundColor: [
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)',
                'rgba(52, 73, 94,1)'
                
            ],
            borderColor: [
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff','#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff','#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff'
            ],
            borderWidth: 3
        },
        {
            label: '<?=$mak2?>',
            yLabel: '<?=$yLabel?>',
            yAxisID: "y-axis-1",
            data: <?=$values2?>,
            barPercentage: 0.5,
            barThickness: 20,
            maxBarThickness: 20,
            backgroundColor: [
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
               'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
               'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
               'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)',
                'rgba(235, 38, 45,1)'
                
            ],
             borderColor: [
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff','#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff','#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff',
                '#ffffff'
            ],
            borderWidth: 3
        }
        ]
    },
   options: {
      responsive: true,
      elements: {
        line: {
            tension: 0
        }
    },
       tooltips: {
       enabled: true, 
       mode: 'label'
     },          
      legend: {
        display: true,
        position: 'bottom',
      }  ,
      scales: {
        xAxes: [ {
          display: true,
          scaleLabel: {
            display: true,
            labelString: '<?=$yLabel?>'
          },
         
          ticks: {
            major: {
              fontStyle: 'bold',
              fontColor: '#FF0000'
            }
          }
        } ],
        yAxes: [{
        stacked: true,
        position: "left",
        type: 'linear',
         scaleLabel: {
            display: true,
            labelString: '<?=$mak1?>'
          },
          
        id: "y-axis-0",
      }, {
        stacked: true,
        type: 'linear',
        position: "right",
        id: "y-axis-1",
         scaleLabel: {
            display: true,
            labelString: '<?=$mak2?>'
          }
      }]
      }
    }

});
</script>
<script type="text/javascript">
  $('input[name="dates"]').daterangepicker();
</script>