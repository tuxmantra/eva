  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
   <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
      <!-- End Navbar -->
      <style type="text/css">
        .main-panel>.content {
    margin-top: 10px;
    padding: 30px 15px;
    min-height: calc(100vh - 123px);
}

.card-header-red{
    background-color: red;
}

p.card-title.move-bit {
    padding-left: 13%;
    line-height: 0;
}

.blank-btn {
    background: transparent;
    color: black;
    border: solid 1px black;
    border-radius: 50px;
    padding: 6px;
    font-size: 13px;
}

.blank-btn:hover {
    background-color: red;
}

.card [class*="card-header-"] .card-icon, .card [class*="card-header-"] .card-text {
    border-radius: 3px;
    background-color: #999999;
    padding: 24px;
    margin-top: -20px;
    margin-right: 15px;
    float: left;
}

.img-holder {
    padding: 14px;
    height: 7.5em;
}
.left-side {
    padding-right: 0;
}
.right-side {
    padding-left: 0;
}

.card.card-calendar {
    padding: 20px;
}



.continues {
    /* width: 99%; */
    width: 99%;
    height: 39em;
    /* overflow: scroll; */
    overflow-x: hidden;
}

.db-heading{
    background-color: #ddd;
    width: 100%;
    padding-left: 10px;
}

.db-heading i{
    float:right;
    padding: 5px;
}

.detailed-db{
    padding: 10px;
    border-bottom: #ddd 1px solid;
}

.db-update{
    overflow-y: scroll;
    overflow-x: hidden;
    height: 300px;
}

.row.nav-menu {
    padding: 0 21px;
}
@media (max-width: 768px){
  h2 {
    padding-top: 112px;
}
}
      </style>
<body onload="getgraph()">
    <div class="content">
        <div class="container-fluid">
          <div class="card-body">
            
                    <div class="card ">
                      <input type="hidden" name="devid" id="devid" value="<?= $did ?>">
                        <div class="card-header card-header-rose card-header-icon">
                            <div class="card-icon">
                            <i class="material-icons">bar_chart</i>
                            </div>
                            <h4 class="card-title"><b>Statistics</b></h4>
                            <div class="row" id='vehicledetails'>
                                <!-- User and Vehicle Details to display from js -->
                            </div>
                            <br>
                        </div>
                        <div class="card-body">
                          <div class="row">

                              <div class="fixed-plugin">
                  <div class="dropdown show-dropdown">
                    <a href="#" data-toggle="dropdown" aria-expanded="false">
                     <span class="material-icons" style="font-size: 50px;     color: white;">
                    directions_car
                    </span>
                    </a>
                    <ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; top: 41px; left: 8px; will-change: top, left;">
                      <?php if($user_category != 4){ ?>
                            <div class="">
                              <div id="datetimepicker12"></div>
                              <input type="text" placeholder="Search Devices" class="form-control" name="search" id="search">
                            <div class="db-update">
                              <div class="search-obj" id="list_device">
                                 <?php if($user_category == 1 || $user_category == 2 || $user_category == 3){ ?>
                                 <?php if(!empty($device_list)){ ?>  
                                 <?php foreach($device_list as $key => $dl){ ?>
                                <div id="row_<?= $dl->device_id ?>" <?php if($did == $dl->device_id){ ?> style="background-color: aliceblue;" <?php } ?> id="row_<?= $dl->device_id ?>" onclick="device_row('<?= $dl->device_id ?>');" class="detailed-db safe">
                                   <p style="font-size: 13px; margin-bottom: 0;"><a class="nav-link" href="javascript:void(0)"><?php echo $dl->device_name." (Device ID ".$dl->device_id.")
                                   "; ?>{{App\Helpers\Helper::getManufacturerById($dl->manu)}}</p></a>
                                </div>
                                 <?php } ?>
                                <?php }else{ ?>
                              <div class="detailed-db">
                              No Devices
                            </div>
                            <?php } ?>
                            <?php }else{ ?>
                            <div class="detailed-db">
                             No Devices
                            </div>
                            <?php } ?>  
                            </div>

                          </div>
                        <?php } ?> 
                    </ul>
                  </div>
                </div>


                            <div class="col-md-12" id="load_data">
                              <ul class="nav nav-pills nav-pills-warning" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" onclick="lastDrive()" data-toggle="tab" href="#link1" role="tablist">
                                      LAST DRIVE
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" onclick="fiveDrive()" data-toggle="tab" href="#link2" role="tablist">
                                    LAST 5 DRIVE
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" onclick="tenDrive()" data-toggle="tab" href="#link3" role="tablist">
                                    LAST 10 DRIVE
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" onclick="rangeDrive()" data-toggle="tab" href="#link4" role="tablist">
                                    DAYS
                                  </a>
                                </li>
                                <?php  $userdata=Session::get('userdata'); 
                                       $utype=$userdata['utype']; 
                                        if($utype == 1){
                                       ?>
                                 <li class="nav-item">
                                  <a class="nav-link" onclick="graphload()" data-toggle="tab" href="#link5" role="tablist">
                                    Graph
                                  </a>
                                </li>
                              <?php } ?>
                              </ul>
                              <div class="tab-content tab-space">
                                <div class="tab-pane active" id="link1">
                                  <div class="container-fluid">
                                    <div class="continues">
                                      <div class="row classes singleLoad">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="tab-pane" id="link2">
                                  <div class="container-fluid">
                                    <div class="continues">
                                      <div class="row classes fiveLoad">
                                      </div>
                                    </div>
                                  </div>  
                                </div>
                                <div class="tab-pane" id="link3">
                                  <div class="container-fluid">
                                    <div class="continues">
                                      <div class="row classes tenLoad">
                                      </div>
                                    </div>
                                  </div>  
                                </div>

                                <div class="tab-pane" id="link4">
                                <div class="container-fluid">
                                  <div class="continues">
                                    <div class="row classes rangeLoad">
                                    </div>
                                  </div>
                                </div>  
                              </div>

                               <div class="tab-pane" id="link5">
                                <div class="container-fluid">
                                    <div class="col-md-12">
                                      <div class="row">

                                        <div class="col-md-3" >
                                                   <select    onchange="getgraph1()"  title="Select Parameter Type" data-size="7" name="ptype2" id="ptype2" tabindex="-98" >
                                                                 
                                                                   <option value="1"    >Drive duration</option>
                                                                   <option value="2"  selected >Distance travelled </option>
                                                                   <option value="3"  >Fuel consumed</option>
                                                                   <option value="4"   >Mileage(km/L)</option>
                                                                   <option value="5"  >Fuel saved in Overrun (L)</option>
                                                                   <option value="6"  >Duration of Overrun (s)</option>
                                                                   <option value="7"   >Idle time</option>
                                                                   <option value="8"   >Maximum speed</option>
                                                                   <option value="9"  >Average speed</option>
                                                                   <!-- <option value="10"  >Peak Torque</option>
                                                                   <option value="11"   >Max Coolant Temperature</option>
                                                                    <option value="12"  >Max Voltage(V)</option> -->

                                                                 
                                                              </select>
                                      </div>
                                      
                                      <div class="col-md-3" >
                                                   <select    onchange="getgraph1()"  title="Select Parameter Type" data-size="7" name="ptype1" id="ptype1" tabindex="-98" >
                                                                 
                                                                   <option value="1"  selected >Drive duration</option>
                                                                   <option value="2"   >Distance travelled </option>
                                                                   <option value="3"   >Fuel consumed</option>
                                                                   <option value="4"  >Mileage(km/L)</option>
                                                                   <option value="5"   >Fuel saved in Overrun (L)</option>
                                                                   <option value="6"   >Duration of Overrun (s)</option>
                                                                   <option value="7"  >Idle time</option>
                                                                   <option value="8"  >Maximum speed</option>
                                                                   <option value="9"   >Average speed</option>
                                                                   <!-- <option value="10"   >Peak Torque</option>
                                                                   <option value="11"   >Max Coolant Temperature</option>
                                                                    <option value="12"   >Max Voltage(V)</option> -->
                                     
                                                                 
                                                              </select>
                                      </div>
                                      


                                      <div class="col-md-3" >
                                       <select  id="year" onchange="getgraph1()" data-size="7" title="Single Select">
                                                                <option value="0">Pick Year</option>
                                                                <option value="2018">Year 2018</option>
                                                                <option  value="2019">Year 2019</option>
                                                                <option  value="2020">Year 2020</option>
                                                                <option selected value="2021">Year 2021</option> 
                                                                <option  value="2022">Year 2022</option>
                                                                <option  value="2023">Year 2023</option>
                                                                <option  value="2024">Year 2024</option>
                                                                <option  value="2025">Year 2025</option>
                                                              </select>
                                                            </div>
                                                            <div class="col-md-3" >

                                                               <select  id="month" onchange="getgraph1()" data-size="7" title="Single Select">
                                                                <option value="0" selected>Pick Month</option>
                                                                <option  value="1">January</option>
                                                                <option  value="2">February</option>
                                                                <option  value="3">March</option>
                                                                <option  value="4">April</option>
                                                                <option  value="5">May</option>
                                                                <option  value="6">June</option>
                                                                <option  value="7">July</option>
                                                                <option  value="8">August</option>
                                                                <option  value="9">September</option>
                                                                <option  value="10">October</option>
                                                                <option  value="11">November</option>
                                                                <option  value="12">December</option>
                                                              </select>
                                                            </div>


                                                          </div>
                                                            
                                      
                                     <!--  <div class="col-md-6" style="float: left;">

                                        <?php if(!empty($start) && !empty($end)){ ?>
                                            <span style="font-size: 13px; font-weight: 500 !important;"><?php echo $start." to ".$end; ?></span>
                                        <?php } ?>
                                       
                                      </div>  -->              
                                       
                                    </div>
                                    <div class="row classes graphload">
                                    
                                  </div>
                                </div>  
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                                     

 </body>
  <!--   Core JS Files   -->
 @include('default.footer')

 <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
 <script type="text/javascript">
  $( document ).ready(function() {


        
    $( "#search" ).keyup(function() {
        var search = $('#search').val();
        $.post('{{url('/')}}/getDeviceList', { search: search }).done(function (data) {
            $('#list_device').html(data);
        });
    });

        $('#datetimepicker5').daterangepicker(
        {
          format: 'DD/MM/YYYY',
          maxDate: moment().add(30, 'days'),
          startDate: moment().add(-30, 'days'),
          endDate: moment().add(30, 'days')
        },          
        function(start, end) {
        //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        //actDriveStatiDataDate();
        }); 


        $('#devid').val('<?= $did ?>');
        $( "#search" ).keyup(function() {
                    var search = $('#search').val();
                    var did    = $('#devid').val();
                    $.post('{{url('/')}}/getDeviceList1', { search: search, 'did': did }).done(function (data) {
                        $('#list_device').html(data);
                    });
        });
        getUservehicleDetails('<?= $did ?>');
        $.post('{{url('/')}}/loadDataStat', { did: '<?= $did ?>', 'days': '1', 'status': '1' }).done(function (data) {
             $('.singleLoad').html(data);
        });
  });

  function device_row(did){
        $('#devid').val(did);
        $('.safe').css({ 'background-color' : '#fff'});
        $('#row_'+did).css({ 'background-color' : 'aliceblue'});
        $('#dph').html("Device Id: "+did);
        getUservehicleDetails(did);
        $.post('{{url('/')}}/loadDataStat', { did: did, 'days': '1', 'status': '2' }).done(function (data) {
              $('#load_data').html(data);
        });
  }

  function lastDrive(){
        var did = $('#devid').val();
        $('.safe').css({ 'background-color' : '#fff'});
        $('#row_'+did).css({ 'background-color' : 'aliceblue'});
        $('#dph').html("Device Id: "+did);

        $.post('{{url('/')}}/loadDataStat', { did: did, 'days': '1', 'status': '1' }).done(function (data) {
              $('.singleLoad').html(data);
        });
  }

  function fiveDrive(){
        var did = $('#devid').val();
        $('.safe').css({ 'background-color' : '#fff'});
        $('#row_'+did).css({ 'background-color' : 'aliceblue'});
        $('#dph').html("Device Id: "+did);

        $.post('{{url('/')}}/loadDataStat', { did: did, 'days': '5', 'status': '1' }).done(function (data) {
              $('.fiveLoad').html(data);
        });
  }

  function tenDrive(){
        var did = $('#devid').val();
        $('.safe').css({ 'background-color' : '#fff'});
        $('#row_'+did).css({ 'background-color' : 'aliceblue'});
        $('#dph').html("Device Id: "+did);

        $.post('{{url('/')}}/loadDataStat', { did: did, 'days': '10', 'status': '1' }).done(function (data) {
              $('.tenLoad').html(data);
        });
  }

  function rangeDrive(){
        var did = $('#devid').val();
        $('.safe').css({ 'background-color' : '#fff'});
        $('#row_'+did).css({ 'background-color' : 'aliceblue'});
        $('#dph').html("Device Id: "+did);

        var start = '<?= date('Y-m-d') ?>';

        $.post('{{url('/')}}/loadDataStatRangeDays', { did: did, start: start, end: start }).done(function (data) {
              $('.rangeLoad').html(data);
        });
  }

  function graphload(){
        var did = $('#devid').val();
        $('.safe').css({ 'background-color' : '#fff'});
        $('#row_'+did).css({ 'background-color' : 'aliceblue'});
        $('#dph').html("Device Id: "+did);
        var start = '<?= date('Y-m-d') ?>';
        // alert('Hi');
        $.post('{{url('/')}}/loadgraph', { did: did, year:'2021', ptype1:'2',ptype2:'1',month:'0' }).done(function (data) {
              $('.graphload').html(data);
        });
  }

  function rangeDriveLoad(){
      var did = $('#devid').val();
      $('.safe').css({ 'background-color' : '#fff'});
      $('#row_'+did).css({ 'background-color' : 'aliceblue'});
      $('#dph').html("Device Id: "+did);

      $.post('{{url('/')}}/loadDataStat', { did: did, 'days': '10', 'status': '1' }).done(function (data) {
            $('.rangeLoad').html(data);
      });
  }

  function getgraph1(){
                    /*  var date = $('#datetimepicker12').datepicker('getFormattedDate');*/
                    var e = document.getElementById("ptype2");
                    var ptype1 = e.options[e.selectedIndex].value;

                    var e4 = document.getElementById("ptype1");
                    var ptype2 = e4.options[e4.selectedIndex].value;
                    // alert(ptype); //ID002
                    var e1 = document.getElementById("year");
                    var year = e1.options[e1.selectedIndex].value;
                    // alert(year);
                    var e2 = document.getElementById("month");
                    var month = e2.options[e2.selectedIndex].value;
                   
                     
                     var did = $('#devid').val();
                      
                  $.post('{{url('/')}}/loadgraph', { did: did, ptype1:ptype1,ptype2:ptype2,year:year,month:month }).done(function (data) {
                           $('.graphload').html(data);
                  });
              }
 </script>