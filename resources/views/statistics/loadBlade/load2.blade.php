                             <ul class="nav nav-pills nav-pills-warning" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" onclick="lastDrive()" data-toggle="tab" href="#link1" role="tablist">
                                LAST DRIVE
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" onclick="fiveDrive()" data-toggle="tab" href="#link2" role="tablist">
                              LAST 5 DRIVE
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" onclick="tenDrive()" data-toggle="tab" href="#link3" role="tablist">
                              LAST 10 DRIVE
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" onclick="rangeDrive()" data-toggle="tab" href="#link4" role="tablist">
                              DAYS
                            </a>
                          </li>
                        </ul>
                        <div class="tab-content tab-space">
                          <div class="tab-pane active" id="link1">
                            <div class="container-fluid">
                              <div class="continues">
                                      <div class="row classes singleLoad">

                                                                           <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,19,22))){ ?>
                                         <?php if(!empty($durTr[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                      <img style="width: 20%;" src="{{url('/')}}/public/assets/statis/drive_duration.png">
                                              </div>
                                              
                                              <h2><?=gmdate('H:i:s',$durTr[0]->val)?></h2>
                                              <p>Drive duration (HH:MM:SS)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                      <img style="width: 20%;" src="{{url('/')}}/public/assets/statis/drive_duration.png">
                                              </div>
                                              
                                              <h2>0</h2>
                                              <p>Drive duration (HH:MM:SS)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>


                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,19,22))){ ?>
                                          <?php if(!empty($distTr[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">    
                                              <img style="width: 60%;" src="{{url('/')}}/public/assets/statis/distance_travel.png">
                                              </div>
                                                  <h2><?=round($distTr[0]->val,2) ?></h2>
                                                  <p>Distance travelled (km)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">    
                                              <img style="width: 60%;" src="{{url('/')}}/public/assets/statis/distance_travel.png">
                                              </div>
                                                  <h2>0</h2>
                                                  <p>Distance travelled (km)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <!--<?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,19,22))){ ?>
                                          <?php if(!empty($hltScore[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/engine_health.png">
                                                  </div>
                                                  <h2><?= $hltScore[0]->val ?></h2>
                                                  <p>Engine Health Score (%)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/engine_health.png">
                                                  </div>
                                                  <h2>0</h2>
                                                  <p>Engine Health Score (%)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,14,15,16))){ ?>
                                          <?php if(!empty($drvScore[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                      <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/13-Overrun.png">
                                                  </div>
                                                  
                                                  <h2><?= $drvScore[0]->val ?></h2>
                                                  <p>Drive Score (Max = 100)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                      <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/13-Overrun.png">
                                                  </div>
                                                  
                                                  <h2>0</h2>
                                                  <p>Drive Score (Max = 100)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>-->
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,19,22))){ ?>
                                          <?php if(!empty($fuelCon[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">    
                                              <img style="width: 29%;" src="{{url('/')}}/public/assets/statis/fuel_consume.png">
                                              </div>
                                              <h2><?= $fuelCon[0]->val ?></h2>
                                              <p>Fuel consumed (L)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 text-center">
                                                  <div class="img-holder">    
                                              <img style="width: 29%;" src="{{url('/')}}/public/assets/statis/fuel_consume.png">
                                              </div>
                                              <h2>0</h2>
                                              <p>Fuel consumed (L)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,19,22))){ ?>
                                          <?php if($mil != 0){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                              <div class="img-holder">
                                                      <img style="width: 44%;" src="{{url('/')}}/public/assets/statis/mileage.png">
                                              </div>
                                              <h2><?=round($mil,2) ?></h2>
                                              <p>Mileage(km/L)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                              <div class="img-holder">
                                                      <img style="width: 44%;" src="{{url('/')}}/public/assets/statis/mileage.png">
                                              </div>
                                              <h2>0</h2>
                                              <p>Mileage(km/L)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,19,22))){ ?>
                                          <?php if(!empty($flOvrRn[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                      <img style="width: 30%;" style="padding-top:30px;" src="{{url('/')}}/public/assets/statis/fuel_saved.png">
                                              </div>
                                              
                                              <h2><?=round($flOvrRn[0]->val,3) ?></h2>
                                              <p>Fuel saved in Overrun (L)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                      <img style="width: 30%;" style="padding-top:30px;" src="{{url('/')}}/public/assets/statis/fuel_saved.png">
                                              </div>
                                              
                                              <h2>0</h2>
                                              <p>Fuel saved in Overrun (L)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,19,22))){ ?>
                                          <?php if(!empty($durOvrRn[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">    
                                              <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/duration_ov.png">
                                              </div>
                          <h2><?= $durOvrRn[0]->val ?></h2>
                                                  <p>Duration of Overrun (s)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">    
                                              <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/duration_ov.png">
                                              </div>
                                                  <h2>0</h2>
                                                  <p>Duration of Overrun (s)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,19,22))){ ?>
                                          <?php if(!empty($idlT[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 20%;" src="{{url('/')}}/public/assets/statis/drive_duration.png">
                                                  </div>
                                                  <h2><?=gmdate('H:i:s',$idlT[0]->val)?></h2>
                                                  <p>Idle time (HH:MM:SS)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 20%;" src="{{url('/')}}/public/assets/statis/drive_duration.png">
                                                  </div>
                                                  <h2>0</h2>
                                                  <p>Idle time (HH:MM:SS)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,19,22))){ ?>
                                          <?php if(!empty($maxSpd[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                      <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/max_spd.png">
                                                  </div>
                                                  
                                                  <h2><?= round($maxSpd[0]->val) ?></h2>
                                                  <p>Maximum speed (km/hr)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                      <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/max_spd.png">
                                                  </div>
                                                  
                                                  <h2>0</h2>
                                                  <p>Maximum speed (km/hr)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,8,9,10,11,12,13,14,15,16,17,18,20,21,19,22))){ ?>
                                          <?php if($avgSpd != 0){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">    
                                              <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/avg_spd.png">
                                              </div>
                                              <h2><?= round($avgSpd) ?></h2>
                                              <p>Average speed (km/hr)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                      <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/avg_spd.png">
                                                  </div>
                                                  
                                                  <h2>0</h2>
                                                  <p>Average speed (km/hr)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,16,19,22))){ ?>
                                          <?php if(!empty($pkTor[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/max_tor.png">
                                                  </div>
                                                  <h2><?= $pkTor[0]->val ?></h2>
                                                  <p>Peak Torque (Nm)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/max_tor.png">
                                                  </div>
                                                  <h2>0</h2>
                                                  <p>Peak Torque (Nm)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>

                                          <!--<?php if(in_array($utype, array(1,2,3,4))){ ?>
                                          <?php if(!empty($pkMap[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/pk_map.png">
                                                  </div>
                                                  <h2><?= $pkMap[0]->val ?></h2>
                                                  <p>Peak MAP (kPa)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/pk_map.png">
                                                  </div>
                                                  <h2>0</h2>
                                                  <p>Peak MAP (kPa)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>-->
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13,16,19,22))){ ?>
                                          <?php if(!empty($maxCool[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/cool.png">
                                                  </div>
                                                  <h2><?= $maxCool[0]->val ?></h2>
                                                  <p>Max Coolant Temperature (°C)</p>
                                          </div>
                                          <?php }else{ ?>
                                           <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/cool.png">
                                                  </div>
                                                  <h2>0</h2>
                                                  <p>Max Coolant Temperature (°C)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <!--<?php if(in_array($utype, array(1,2,3,4))){ ?>
                                          <?php if(!empty($pkRaPre[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/mileage.png">
                                                  </div>
                                                  <h2><?= $pkRaPre[0]->val ?></h2>
                                                  <p>Peak Rail Pressure (kPa)</p>
                                          </div>
                                          <?php }else{ ?> 
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/mileage.png">
                                                  </div>
                                                  <h2>0</h2>
                                                  <p>Peak Rail Pressure (kPa)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>-->
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13))){ ?>
                                          <?php if(!empty($minVolt[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/engine_health.png">
                                                  </div>
                                                  <h2><?= $minVolt[0]->val ?></h2>
                                                  <p>Min Voltage (V)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/engine_health.png">
                                                  </div>
                                                  <h2>0</h2>
                                                  <p>Min Voltage (V)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                          
                                          <?php if(in_array($utype, array(1,2,3,4,5,6,7,8,9,10,11,12,13))){ ?>
                                          <?php if(!empty($maxVolt[0]->val)){ ?>
                                          <div class="col-md-4 col-sm-6 text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/max_vlt.png">
                                                  </div>
                                                  <h2><?= $maxVolt[0]->val ?></h2>
                                                  <p>Max Voltage(V)</p>
                                          </div>
                                          <?php }else{ ?>
                                          <div class="col-md-4 col-sm-6ssss text-center">
                                                  <div class="img-holder">
                                                          <img style="width: 30%;" src="{{url('/')}}/public/assets/statis/max_vlt.png">
                                                  </div>
                                                  <h2>0</h2>
                                                  <p>Max Voltage(V)</p>
                                          </div>
                                          <?php } ?>
                                          <?php } ?>
                                                                                                                
                                          
                                     
                                      </div>
                              </div>
                      </div>
                          </div>
                          <div class="tab-pane" id="link2">
                            <div class="container-fluid">
                              <div class="continues">
                                      <div class="row classes fiveLoad">
                                             
                                      </div>
                              </div>
                      </div>  
                          
                          </div>
                          <div class="tab-pane" id="link3">
                            
                            <div class="container-fluid">
                              <div class="continues">
                                      <div class="row classes tenLoad">
                 
                                      </div>
                              </div>
                      </div>  
                          </div>
                          <div class="tab-pane" id="link4">
                           
                            <div class="container-fluid">
                              <div class="continues">
                                      <div class="row classes rangeLoad">
                            
                                      </div>
                              </div>
                      </div>  
                          </div>
                        </div>