  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
  <style type="text/css">
    .table-heading-font{
        font-size: 13px !important;
    }
    tfoot {
     display: table-header-group !important;
     background-color: blanchedalmond;
    }
    tfoot input {
      width: 100%;
    }
    .Serial_tp {
      display: none;
    }
    .Actions_tp {
      display: none;
    }
    .deep{
      cursor: pointer;
      padding: 2%;
      border-radius: 2px;
      background: #fff;
      color: grey;
      position: relative;
    }
    tr div {
      width: auto !important;
      z-index: auto !important;
    }
    .tab-pane tbody {
      display: block;
      max-height: 230px;
      overflow-y: scroll;
    }
    #map{
          background-color: aliceblue;
          margin-top: 30px !important;
    }
  </style>
      <!-- End Navbar -->
       
      <div class="content">
        <div class="container-fluid">
          <div class="row">


         
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">map</i>
                  </div>
                  <h4 class="card-title">Map</h4>
                </div>
                <div class="card-body">
          
                <div class="card-body">
                  <div class="col-md-12">
                    <?php
                           $myJSON = json_encode($device_data);
                            // print_r($myJSON);

                     ?>
                    <div id="dvMap" style="position: initial; overflow: auto; height: 490px;"></div>
                  </div>
                </div>
                <!-- end content-->
              </div>
              <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
          </div>
          <!-- end row -->
        </div>
      </div>

    </div>
  </div>
  
  <!--   Core JS Files   -->
 @include('default.footer')
 <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABmH85QxA6UsfF96kz17AMV5xDOESzPIE&callback=initMap&region=IN">
    </script> 
<script type="text/javascript">
    var markers = <?=$myJSON?>;
    window.onload = function () {
        LoadMap();
    }
    function LoadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            // zoom: 8, //Not required.
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
 
        //Create LatLngBounds object.
        var latlngbounds = new google.maps.LatLngBounds();
 
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
 
            //Extend each marker's position in LatLngBounds object.
            latlngbounds.extend(marker.position);
        }
 
        //Get the boundaries of the Map.
        var bounds = new google.maps.LatLngBounds();
 
        //Center map and adjust Zoom based on the position of all markers.
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
    }
</script>