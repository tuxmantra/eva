  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="right">
              <button class="btn btn-primary btn-sm" onclick="goBack()">Back</button>
            </div>
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">User Type</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <form method="post" action="" class="form-horizontal">
                    <div class="row">
                      <label class="col-sm-2 col-form-label">User Type</label>
                      <div class="col-sm-10">
                        <div class="form-group">

                          <select class="selectpicker usertype_config" data-style="select-with-transition" title="Select User Type" data-size="7" name="user_type" tabindex="-98">
                            <option disabled=""> User Type Options</option>
                            @if (!empty($user_type))
                            @foreach ($user_type as $user)
                            <option value="{{$user->u_type}}">{{$user->u_name}}</option>
                            @endforeach
                            @endif
                          </select>
                        </div>
                      </div>
                    </div>

                      </div>
                    </div>
                
                </div>
              </div>
            </div>
            <div class="col-md-12" id="load" style="display: none;" align="center">
            <img src="public/assets/img/loader.gif" style="width: 15%;">
            </div>
           
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Configuration</h4>
                  </div>
                </div>
                <div class="card-body ">
                  
                    <div class="row" style="margin-top: 30px;" id="configDiv">

                    </div>
             
    
                 
                </div>
              </div>
            </div>
            <div class="col-md-12" align="right">
              <button id="save_config" style="display: none;" type="button" onclick="doSomething();" class="btn btn-fill btn-rose">Save</button>

            </div>
             <div class="col-md-12" id="success" align="center" style="display: none;">
                <span style="color: red; font-size: 13px; font-weight: 500;">Configuration saved...</span>
            </div>
          </div>
        </div>
      </div>
       </form>
     
    </div>
  </div>
 
  <!--   Core JS Files   -->
 @include('default.footer')
 <script>
  function doSomething(){
  $('#success').hide();
  var val = [];
    $(':checkbox:checked').each(function(i){
        val[i] = $(this).val();
    });
    var cal = [];
    $("input:checkbox:not(:checked)").each(function (i) {
            cal[i] = $(this).val();
    });

    var utype = $(".usertype_config option:selected").val();
    $.ajax({
      type:'POST',
      url: url+'update-config-usertype',
      data: { 'check': val, 'uncheck': cal ,'utype': utype },
      success: function(data){
        /*$('html, body').animate({ scrollTop: 0 }, 'slow');*/
         Swal.fire({
            type: 'success',
            title: 'Successfull',
            text: 'Configuration saved',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
      }
    });
}
 </script>
