  
  @include('default.header')
  @include('default.sidebar')
  @include('default.submenu')

  <style>
    select#user_type_select {
    background: transparent;
    border: none;
    border-bottom: 1px solid #ccc;
    width: 30%;
    color: #3c4858;
    }
  </style>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12" align="center">
              <span id="serve_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;">
                     
                      @if (Session::has('message'))
                            {!! session('message') !!}
                      @endif
                   
              </span>
              <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500; display: none;">Please select User Type.</span>
            </div>

             <div class="col-md-12" id="newconfig" style="display: none;" align="center">
                <span style="color: red; font-size: 13px; font-weight: 500;">New Config added...</span>
            </div>
         
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">New Config</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <form method="post" action="" class="form-horizontal">
                    <div class="row">
                      <label class="col-sm-2 col-form-label">User Type</label>
                      <div class="col-sm-3">
                         <!--  <input type="text" value="" id="hiddenconfig">
                          <input type="text" value="" id="hiddenlabel"> -->
                          <select class="selectpicker createconfig" data-style="select-with-transition" title="Select User Type" data-size="7" id="new_config" name="new_config" tabindex="-98" required="">
                            <option value="">Select User Type</option>

                            <optgroup label="Admin">
                  
                                    <option value="">Admin</option>
                    
                            </optgroup>
                            <optgroup label="Partner">
                   
                                    <option value="">Bluetooth</option>
                                    <option value="">Standalone</option>
                                    <option value="">Fleet</option>
                  
                            </optgroup>
                            <optgroup label="Driver">
               
                                    <option value="">Bluetooth</option>
                                    <option value="">Standalone</option>
                                    <option value="">Fleet</option>
                   
                            </optgroup>
                         
                          </select>
                         </div>
                            <div class="col-sm-2" >
                             <input type="text" class="form-control" value="" id="hiddenlabel" readonly style="display: none">
                           </div>
                           <div class="col-sm-3" > 
                             <input type="text" class="form-control" value="" id="hiddenconfig" style="display: none">
                           </div>
                           <div class="col-sm-2" >
                              &nbsp;&nbsp;&nbsp;&nbsp;<button type="button" onclick="createconfig_new()" class="btn btn-fill btn-rose">Create</button>
                           </div>
                          
                        </div>
                      
                    </form>
                      <div class="col-sm-8">
                        <div class="form-group">

                        </div>
                      </div>
                    </div>
     
                    <div class="row" id="output" style="display: none;">
                      <label class="col-sm-2 col-form-label">Created</label>
                      <div class="col-sm-10">
                        <div class="form-group">
                          <input type="text" name="create" id="create" class="form-control" value="Driver 4" disabled>
                        </div>
                      </div>
                    </div>
                
                </div>
              </div>
            </div>

<!--             <div class="col-md-12" id="success" align="center" style="display: none;">
                <span style="color: red; font-size: 13px;">Configuration saved...</span>
            </div> -->

            <div class="col-md-12" align="right">
             

            </div>
          </div>
        </div>
      </div>

      
    </div>
  </div>
  
  <!--   Core JS Files   -->
@include('default.footer')
<script>
  function createconfig_new(){
  $('#cli_message').hide();
  $('#newconfig').hide();
  var val = $('#hiddenconfig').val();
  var lab = $('#hiddenlabel').val();
  if(val != ''){
        $.ajax({
          type:'POST',
          url: url+'create-config',
          data: { 'val': val, 'lab': lab },
          success: function(data){
            
              Swal.fire({
                  type: 'success',
                  title: 'Successfull',
                  text: 'New configuration for user Created',
                   confirmButtonColor: '#eb262d',
                  // footer: '<a href>Why do I have this issue?</a>'
                })
              $('#create').val(data);
          }
        });
  }else{
      Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Please select User Type.',
             confirmButtonColor: '#eb262d',
            // footer: '<a href>Why do I have this issue?</a>'
          })
  }

}
</script>