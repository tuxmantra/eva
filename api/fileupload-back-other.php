<?php
include 'fleet-algo.php';
// include 'common.php';
// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

// $logFilePath = getcwd();
// $req_dump = print_r($argv[1], TRUE);
// $filePath = $logFilePath. "/"."request1.txt";
// //print_r($filePath);die;
// $fp = fopen($filePath, 'a');
// fwrite($fp, "\n---------------- fileupload-back-app - Request Begin --------------------\n");
// $currTimeStamp = date('Y-m-d H:i:s',time());
// fwrite($fp, $currTimeStamp);
// fwrite($fp, "\n");
// fwrite($fp, $req_dump);
// fwrite($fp, "----------------- fileupload-back-app - Request End --------------------\n");
// fclose($fp);

$arrayData = array();
$colHeader = array();
function csvToMysql($tmpName,$conn)
{
    $fileCsv = $tmpName;
	$fileObj = fopen($fileCsv, 'r');
	$i       = 0;
	$iLoop   = 0;
	$iLoopData  = array();
    $postValKey = array();
    $driveStat  = array();
	$mainID = 0;
	$devId  = "";

    // get device data columns from device data table
    $sql             = "SHOW columns FROM device_data";
    $device_data_col = $conn->query($sql);

    if ($device_data_col->num_rows > 0) {
	    $valLast = $postVal = $oldData = array();
	    while($row = $device_data_col->fetch_assoc()) {
	       $valLast[]   = $row['Field'];
	       $postVal     = $valLast;
	       $colHeader[] = $row['Field'];
	    }
    }

    $currentDTCs   = "";
	$previousDTCs  = "";
	$currRowNumber = 0;

    while($data = fgetcsv($fileObj, 1000, ","))
	{
		// print_r($data);
		$numColumns = count($data);
		
		// First Two Rows of the CSV file Contains the Device ID and Meta Data
		if($currRowNumber == 1)
		{ 
			// Create Array for copying the data from csv file
			$postValRep = array('');
			
			// Copy Device ID which is present at Column 0
			$devId = $data[0];
			
			// Oter Metadata if present in Row number 1 must be processed here
		}
		elseif($currRowNumber == 3)
		{
			// Row number 3 contains the data Header row. Copy all headers in the "KEY" Array
			// PLEASE NOTE the first header must always be "time_v" !!!!!!!!!!!!!!!
			if(trim($data[0]) == "time_v")
			{
				for($i = 0;$i < count($data);$i++)
				{
					// Copy headers into KEY array
					$postValKey[$i] = trim($data[$i]);
					//print_r($postValKey);					
				}
			}
			else
			{
				// If header is not present the file format is invalid and hence return false.
				return false;
			}
		}
		elseif($currRowNumber > 3)
		{
			$postVal['device_id'] = $devId;
			$postVal['time_s']    = $data[0];
			$tsValTen             = substr($postVal['time_s'],0,10);
			$postVal['time_v']    = date('Y-m-d H:i:s',$tsValTen);
			$postVal['ad_dt']     = date('Y-m-d H:i:s');
			$postVal['ad_dtin']   = date('Y-m-d',substr($postVal['time_s'],0,10));
			$dateElements = explode('-', $postVal['time_v']);
			$date         = $dateElements[1];
			if($date == '03'){
				$table = 'dd_mar';
			}
			if($date == '04'){
				$table = 'dd_apr';
			}
			if($date == '05'){
				$table = 'dd_may';
			}
			if($date == '06'){
				$table = 'dd_june';
			}
			if($date == '07'){
				$table = 'dd_july';
			}
			if($date == '08'){
				$table = 'dd_aug';
			}
			if($date == '09'){
				$table = 'dd_sep';
			}
			if($date == '10'){
				$table = 'dd_oct';
			}
			if($date == '11'){
				$table = 'dd_nov';
			}
			if($date == '12'){
				$table = 'dd_dec';
			}
			if($date == '01'){
				$table = 'dd_jan';
			}
			if($date == '02'){
				$table = 'dd_feb';
			}

			$sqldriveid    = "select F43,Fsprint from $table where device_id = '$devId' ORDER by id DESC LIMIT 1";
			$resultdriveid = $conn->query($sqldriveid);
			$rowdriveid    = $resultdriveid->fetch_assoc();
			if($rowdriveid['F43'] == 6 || empty($rowdriveid['F43']))
			{
				$postVal['Fsprint']   = $driveID = strtotime(date('Y-m-d H:i:s'));
			}
			else
			{
				// echo "I m here";
				$postVal['Fsprint']   = $driveID = $rowdriveid['Fsprint'];
			}			
			// echo $postVal['Fsprint'];die;
			$postVal['F43']       = $data[1];						// Engine State
            $postVal['F4']        = $data[2];						// RPM
            $postVal['F19']       = $data[3];						// Engine Load
            $postVal['F6']        = $data[4];						// MAF
            $postVal['F3']        = $data[5];						// Map
            $postVal['F38']       = $data[6];						// Commanded EGR
            $postVal['F7']        = $data[7];						// Throttle
            $postVal['F42']       = $data[8];						// Accelerator Pedal Position
			$postVal['F5']        = $data[9];						// Vehicle Speed
            $postVal['F34']       = $data[10];						// BattV
            $postVal['F37']       = $data[11];						// Fuel Common Rail Pressure
            $postVal['F18']       = $data[12];						// IAT
            $postVal['F1']        = $data[13];						// Coolant
            $postVal['F41']       = $data[14];						// Baro
            $postVal['F87']       = $data[15];						// Torque Nm
            $postVal['F66']       = $data[16];						// Dist Since Error Cleared
            $postVal['F88']       = $data[18];						// Power BHP
            $postVal['F90']       = $data[19];						// Fuel Flow L/hr
            $postVal['F36']       = $data[20];						// Fuel System Status
            $postVal['F15']       = $data[22];						// Engine ON Time
           	$postVal['F48']       = $data[23]; 						// Drive Distance
           	$postVal['F49']       = $data[24]; 						// Drive Duration
			$postVal['lat']       = $data[25];						// lat
			$postVal['lon']       = $data[26];						// long
			$postVal['alt']       = $data[27];						// alt
           	$postVal['F47']       = $data[28]; 						// Calibration Stage
           	$postVal['F11']       = $data[29]; 						// DTC
            $postVal['F52']       = $data[30];						// Average Fuel Flow
            $postVal['F53']       = $data[31];						// Average Speed
            $postVal['F58']       = $data[32]; 						// Mileage
            $postVal['F59']       = $data[33]; 						// Max Torque
            $postVal['F60']       = $data[34]; 						// Max Speed
            $postVal['F61']       = $data[35];						// Overrun Duration
            $postVal['F62']       = $data[36];						// Fuel Saved in Overrun
            $postVal['F63']       = $data[37];						// Overrun Distance
            $postVal['F64']       = $data[38];						// Fuel Consumed
            $postVal['F65']       = $data[39];						// Idle Time
            $postVal['F69']       = $data[41];						// Calculated Baro
            $postVal['F68']       = $data[42];						// Active Alerts
			$postVal['F71']       = $data[43]; 						// Acceleration
            // $postVal['F96']       = $data[44];						// V/n Ratio
            // $postVal['F81']       = $data[45];						// Current Selected Gear
            // $postVal['F85']       = $data[47];						// Clutch Status
            // $postVal['F86']       = $data[46];						// Brake Status

            //Compensated torque calculation
            // print_r($postVal['F87']);die;
            if($postVal['F18']!='' && $postVal['F87'] !='')
            {
	            $divFact    = ((273 + $postVal['F18'])/298);
	            $torqueFact = pow($divFact,0.6);
	            $compTorque = ($postVal['F87'] * $torqueFact);
	            $postVal['F89'] = round($compTorque,2);
            }                			// Compensated Torque

			$oldSt = trim($data[1]);
			
			// Reading all data from the current row is fininshed.
			// Next step is to buffer/save all individual rows data and minimise write to database.
			// $iLoopData is the variable that holds araay of rows and $iloop is the row index/row number

			$oldData = $postVal;
			
			$iLoopData[$iLoop] = getSqlQueryTest('device_data',$postVal,$conn);
			// Increment $iLoop
			$iLoop++;
			if($data[1] == 6)
			{
				$driveStat = $data;
			}
		}
		//echo $currRowNumber;
        // End of Processing the Current Row. Incremnet Row number count
		$currRowNumber++;

	} // while end
	
   /*******************************While Loop End update database now**********************************/
	//print_r($iLoopData);
	
	//Add data in device_data table
    // print_r($iLoopData); die;
    if(count($iLoopData) >= 1)
    {   
    	for($i = 0;$i<count($iLoopData);$i++)
        	{
        		$data11       = explode(',', $iLoopData[$i]);
        	    $date         = $data11[2];
        	    $dateElements = explode('-', $date);
				$date         = $dateElements[1];
        	    // echo date('m', strtotime($date));
        	    // echo $date; 	
        	    if($date == '03'){
					$table = 'dd_mar';
				}
				if($date == '04'){
					$table = 'dd_apr';
				}
				if($date == '05'){
					$table = 'dd_may';
				}
				if($date == '06'){
					$table = 'dd_june';
				}
				if($date == '07'){
					$table = 'dd_july';
				}
				if($date == '08'){
					$table = 'dd_aug';
				}
				if($date == '09'){
					$table = 'dd_sep';
				}
				if($date == '10'){
					$table = 'dd_oct';
				}
				if($date == '11'){
					$table = 'dd_nov';
				}
				if($date == '12'){
					$table = 'dd_dec';
				}
				if($date == '01'){
					$table = 'dd_jan';
				}
				if($date == '02'){
					$table = 'dd_feb';
				}
        	} 
    	$queryM  = getSqlTableHeader($table,$conn);
        $queryM .= implode(',',$iLoopData);
        $iLoopData = array();
        // echo "$queryM";die;
        // Call function insert into DB Table

        if($conn->query($queryM))
        {   
        	if($driveStat[1] == 6)
        	{
        		
        		$postValDrive['device_id'] = $devId;
				$postValDrive['dt']        = $dt = date('Y-m-d',substr($driveStat[0],0,10));
				// $driveID                   = strtotime(date('Y-m-d H:i:s'));
				$postValDrive['drive']     = $driveID;
				$postValDrive['dur'] 	   = $durTr = $driveStat[24];
				$postValDrive['dist']      = $dist  = round($driveStat[23],2);
				$postValDrive['max_tor']   = round($driveStat[33],2);
				$postValDrive['max_sp']    = round($driveStat[34]);
				$postValDrive['dur_ovr']   = round($driveStat[35]);
				$postValDrive['ful_ovr']   = round($driveStat[36],3);
				$postValDrive['ful_c']     = round($driveStat[38],3);
				$postValDrive['avg_ml']    = round($driveStat[32],2);
				$postValDrive['idt']       = round($driveStat[39]);
				$postValDrive['avg_speed'] = round($driveStat[31],2);
				$postValDrive['dist_ovr']  = round($driveStat[37],2);
				$postValDrive['ad_dt']     = date('Y-m-d H:i:s');
				$postValDrive['dfrm']      = date('Y-m-d H:i:s');
				$postValDrive['dto']       = date('Y-m-d H:i:s');
				$postValDrive['servduedayupDt'] = date('Y-m-d'); 
				
				$oilVal = calcOilEngLife($driveID,$devId,$dt,$durTr,$conn);	
				$oilUsedLife                    = $oilVal[0];
				$pOfOilLf                       = $oilVal[1];
				$postValDrive['oillife']        = $oilUsedLife;
				$postValDrive['oillifep']       = $pOfOilLf;
				$postValDrive['serviceduekm']   = $oilVal[2];
				$postValDrive['serviceduedays'] = $oilVal[3];
				// OilLife Distance
				$sqla    = "select OilLifeDist from device_drive where device_id = '$devId' ORDER by id DESC LIMIT 1";
				$resulta = $conn->query($sqla);
				$rowa    = $resulta->fetch_assoc();
				$postValDrive['OilLifeDist'] = $rowa['OilLifeDist'] + $dist;

				// get vehicle details
				$sqlveh    = "SELECT *,v.model as vmodel FROM vehicles v,device d,vehinfo i where d.id=v.device and device_id='$devId' and v.manufact=i.mfd";
				$resultveh = $conn->query($sqlveh);
				$rowveh    = $resultveh->fetch_assoc();

				$sqlvehsett    = "SELECT * FROM `vehicle_setting`";
				$resultvehsett = $conn->query($sqlvehsett);
				$rowvehsett    = $resultvehsett->fetch_assoc();
				$manufarr      = explode(';',$rowvehsett['manufacturer']);
				// $varientarr    = explode(';',$rowveh['var']);
				$modelarr      = explode(';',$rowveh['model']); 
				$engine_caparr = explode(';',$rowvehsett['engine_capacity']); 
				// print_r($rowveh);die;
				if($rowveh['fuel_tpe'] == 1)
				{
					$ftype="Petrol";
				}
				else
				{
					$ftype="Diesel";
				}
				// print_r($rowveh['vmodel']);
				// echo $manufarr[$rowveh['manufact']]." ".$modelarr[$rowveh['vmodel']];die;
				$mfd = explode("_", $manufarr[$rowveh['manufact']]);
				$vehicle       = $mfd[0]." ".$modelarr[$rowveh['vmodel']]." ".$ftype." ".$engine_caparr[$rowveh['eng_cap']];
				// echo $vehicle;
				$postValDrive['vehicle'] = $vehicle;
				$updateStats   = insertDbSingle('device_drive',$postValDrive,$conn);
				if($updateStats)
				{
					//Add DTC in device table
					$dataDTC = $driveStat[29];
					$dataDTC = explode(":", $dataDTC);					
					$dataDTC = implode(',',$dataDTC);

					if(!empty($dataDTC))
					{
						setDTC($devId,$dataDTC,$conn);
						generateAlertMailAdvMon($devId,$driveID,1,1,$conn);
					}
					else
					{
						resetDTC($devId,$conn);
					}

					//Add alerts in alert table
					//Base Air System Alert
					if(($driveStat[42] & 0x4000) > 0)
					{
						generateAlertMailAdvMon($devId,$driveID,2,3,$conn);
					}
					// //Base Coolant System Alert
					// if(($driveStat[42] & 0x0000) > 0)
					// {
					// 	generateAlertMailAdvMon($devId,$driveID,2,37,$conn);
					// }
					//Base Battery System Alert
					if(($driveStat[42] & 0x2000) > 0)
					{
						generateAlertMailAdvMon($devId,$driveID,2,7,$conn);
					}
					return $devId;
				}
				else
				{
					return FALSE;
				}
        	}
        }
        else
        {
            return FALSE;
        }
    }
    else
    {
        return $devId;
    }
	// calcStatMonData($devId,$iLoopData,$iLoop,$driveID,$conn,$colHeader);
	return $devId;
}

function resetdeviceFile($deviceid,$fname,$conn)
{
	$devFile = getdeviceFile($deviceid,$conn);
    $devFile = explode(';', $devFile);

	if(in_array($fname, $devFile))
	{
		$key = array_search($fname, $devFile);
		unset($devFile[$key]);
	}
	$flist  = implode(';',$devFile);
	$sql    = "update device set file_list='$flist' where device_id='$deviceid'";
	$result = $conn->query($sql);
	return $result;
}

function getdeviceFile($deviceid,$conn)
{
	$file   = '';
	$sql    = "select file_list from device where device_id='$deviceid'";
	$device = $conn->query($sql);
	while($row = $device->fetch_assoc()){
		$file  = $row['file_list'];
	}
	return $file;
}

function calcOilEngLife($reqFor,$deviceID,$dt,$durTr,$conn)
{
	if(date('m')=='03'){
		$table = 'dd_mar';
	}
	elseif(date('m')=='04'){
		$table = 'dd_apr';
	}
	elseif(date('m')=='05'){
		$table = 'dd_may';
	}
	elseif(date('m')=='06'){
		$table = 'dd_june';
	}
	elseif(date('m')=='07'){
		$table = 'dd_july';
	}
	elseif(date('m')=='08'){
		$table = 'dd_aug';
	}
	elseif(date('m')=='09'){
		$table = 'dd_sep';
	}
	elseif(date('m')=='10'){
		$table = 'dd_oct';
	}
	elseif(date('m')=='11'){
		$table = 'dd_nov';
	}
	elseif(date('m')=='12'){
		$table = 'dd_dec';
	}
	elseif(date('m')=='01'){
		$table = 'dd_jan';
	}
	elseif(date('m')=='02'){
		$table = 'dd_feb';
	}

	//============== Calc for Eng Oil Life ==========
	$sql = "select min(F41*1) as minT,max(F41*1) as maxT,max(F19*1) as maxL from $table where device_id='$deviceID' and Fsprint='$reqFor' and  (F41*1) <> 0 order by time_s asc";
	// echo $sql;
	$result    = $conn->query($sql);
	$row       = $result->fetch_assoc();
	$obdCTInit = $row['minT'];
	$obdCTMax  = $row['maxT'];
	$obdLDMax  = $row['maxL'];
	 
	// check DPF
	$sqla    = "select F68 from $table where device_id='$deviceID' and Fsprint='$reqFor' ORDER by id DESC LIMIT 0,1";
	$resulta = $conn->query($sqla);
	$rowa    = $resulta->fetch_assoc();
	$lastActAlertVal = $rowa['F68'];
	$res = $lastActAlertVal & 0x000F0000;
	if ($res == 131072) 
	{
	    //DPF Ongoing
	    // DPF Correction factor
	    $dpfCorFact = 3; //This factor is weighted equally as AVG coolant factor. 
	}
	else
	{
	    //DPF Complete
	    $dpfCorFact = 1;
	}
	
	$sql1    = "select sum(dist*1) from device_drive where device_id ='$deviceID' and (dist*1) <> 0 and (avg_ml*1) <> 0";
	$result1 = $conn->query($sql1);
	$row1    = $result1->fetch_assoc();
	$numDist = $row1['sum(dist*1)'];
	$numDist = round($numDist,2);
	
	$sqlv    = "SELECT * FROM vehicles a,device b where a.device=b.id and b.device_id='$deviceID'";
	// echo $sql;
	$resultv = $conn->query($sqlv);
	$valP    = $resultv->fetch_assoc();
	// print_r($valP);die;
	//$factVal = ($valP[0]['co2fact'] >= 1) ? $valP[0]['co2fact'] : 2320;
	$lastOilChngDt = ($valP['oil_dt'] != "0000-00-00" && $valP['oil_dt'] != "") ? $valP['oil_dt'] : "2017-01-01"; // set and get frm veh info
	$lastOilChngKm = ($valP['oil_dist'] >= 1) ? $valP['oil_dist'] : 1000; // set and get frm veh info
	$distTrSPur    = ($valP['travel_purchase'] >= 1) ? $valP['travel_purchase'] : 100; // get from veh info
	$distSinOnB    = ($numDist >= 1) ? ($distTrSPur + $numDist) : $distTrSPur; // get from chart distance since on board
	
	$oilLifeDaysC = ($valP['oill_day'] >= 1) ? $valP['oill_day'] : 365; // set and get frm veh cal info
	$oilLifeKmsC  = ($valP['oill_dist'] >= 1) ? $valP['oill_dist'] : 10000; // set and get frm veh cal info
	$dustEnvC     = ($valP['dust_env'] >= 1) ? $valP['dust_env'] : 0; // set and get frm veh cal info
	
	//calc days since oil chng=====================================
	$daySOChng = round((strtotime(date('Y-m-d')) - strtotime($lastOilChngDt)) / (60*60*24));
	
	//calc km since oil chng=====================================
	$factdist = floor($distSinOnB/$oilLifeKmsC);
	$factoilchng = floor($lastOilChngKm/$oilLifeKmsC);
	if($factdist == $factoilchng)
	{
		$kmSOChng = ($distSinOnB - ($oilLifeKmsC * $factdist)) - ($lastOilChngKm  - ($oilLifeKmsC * $factoilchng));
	}
	else
	{
		$kmSOChng = $distSinOnB - $lastOilChngKm;
	}
	
	//Equivalent drive is to be converted in km and added. 5,25,600
	$durTr_eq_dist = (10000 * $durTr)/(525600*60);
	$kmSOChng      = $kmSOChng + $durTr_eq_dist;
	//Corresponding days for distance travelled=====================================
	$daysDistTrv = ($kmSOChng*$oilLifeDaysC)/$oilLifeKmsC;
	$daysDistTrv = floor($daysDistTrv);
	
	//Engine oil operating temperature (T)=====================================
	$engOilOpTemp = 0;			
	if($obdCTMax < 100)
	{
		$engOilOpTemp = $obdCTMax + 5;
	}
	else 
	{
		$engOilOpTemp = $obdCTMax + 10;
	}
	
	//Time after cranking (t)=====================================
	$timeAfCrnk = 40 - ($obdCTInit * 0.25);
	
	//Temperature Correction Factor (Ct)=====================================
	$tempCorrFact = 0;
	if($engOilOpTemp < 0)
	{
		$tempCorrFact = 8;
	}
	elseif($engOilOpTemp < 80) 
	{
		$tempCorrFact = 1 + (7 * ((80 - $engOilOpTemp)/80));
		$tempCorrFact = round($tempCorrFact,2);
	}
	elseif($engOilOpTemp < 130) 
	{
		$tempCorrFact = 1;
	}
	else
	{
		$expNt = ($engOilOpTemp - 130)/(80);
		$tempCorrFact = pow(10,$expNt);
		$tempCorrFact = round($tempCorrFact,2);
	}
	
	//Engine Load Correction Factor (Cl)=====================================
	$engLoadCorFact = 0;
	if($obdLDMax < 70)
	{
		$engLoadCorFact = 1;
	}
	else
	{
		$expNt = ($obdLDMax - 70)/(30);
		$engLoadCorFact = round(pow(2,$expNt),2);
	}
	
	//Engine RPM Correction Factor (Crpm)=====================================
	$engRPMCorFact = 1; //fixed value to reduce complexity. Values not affected much. Factor is very small.
	
	//Ccck0=====================================
	$cck = 0;
	if($obdCTMax <= 0)
	{
		$cck = 10;
	}
	elseif($obdCTMax < 80)
	{
		$cck = 10 - ($obdCTMax/16);
	}
	else
	{
		$cck = 5;
	}
	
	//T0=====================================
	$t0 = 0;
	if($obdCTMax <= 0)
	{
		$t0 = 40;
	}
	elseif($obdCTMax < 80)
	{
		$t0 = 40 - ($obdCTMax/4);
	}
	else
	{
		$t0 = 20;
	}
	
	//Engine Cranking Correction Factor=====================================
	$engCrCorFact = 0;
	if($timeAfCrnk < $t0)
	{
		$engCrCorFact = 1 + (($cck - 1) * (($t0 - $timeAfCrnk)/$t0));
		$engCrCorFact = round($engCrCorFact,2);
	}
	else
	{
		$engCrCorFact = 1;
	}
	
	//Oil Age Factor=====================================
	$oilAgeFact = 0;
	if($daySOChng <= 365)
	{
		$oilAgeFact = 1;
	}
	else
	{
		$expN = (365 - $daySOChng)/365;				
		$oilAgeFact = round(exp($expN),2);
	}
	//Dust Correction factor=====================================
	$dustCorFact = 0;
	if($dustEnvC == 1)
	{
		$dustCorFact = 1.5;
	}
	else
	{
		$dustCorFact = 1;
	}
	
	//Cumulated Oil Life Initialisation=====================================
	//get from Database of last driveData
	$sqldv    = "SELECT oillife FROM device_drive where device_id='$deviceID' ORDER by id DESC LIMIT 0,1";
	$resultdv = $conn->query($sqldv);
	$valdP    = $resultdv->fetch_assoc();
	$cumOilLifeLastDrive = $valdP['oillife'];
	$cumOilLife = ($cumOilLifeLastDrive > 0 && $cumOilLifeLastDrive <= 365) ? $cumOilLifeLastDrive : $daySOChng;
	
	//Oil used life=====================================
	// (($durTr/60) this was removed becuz Equivalent time duration of drive is getting added to Km driven since last service now.
	$oilUsedLife = 0;
	if($cumOilLife <= $daysDistTrv)
	{
		$oilUsedLife = $daysDistTrv + ($dpfCorFact * $tempCorrFact * $engLoadCorFact * $engRPMCorFact * $engCrCorFact * $dustCorFact);
	}
	else
	{
		$oilUsedLife = $cumOilLife + ($dpfCorFact * $tempCorrFact * $engLoadCorFact * $engRPMCorFact * $engCrCorFact * $dustCorFact);
	}
	$oilUsedLife = floor($oilUsedLife);
	//Cumulated Oil Life Calculation=====================================
	$cumOilLife = $oilUsedLife;
	
	//% of Oil life remaining=====================================
	$pOfOilLf = 0;
	// if($cumOilLife >= $oilLifeDaysC || $daysDistTrv >= $oilLifeDaysC)
	// {
	// 	$pOfOilLf = 0;
	// }
	// else
	// {
	// 	$pOfOilLf = 100 * (1 - ($oilUsedLife/($oilLifeDaysC * $oilAgeFact)));
	// }
	// $pOfOilLf = round(($pOfOilLf),0);

	// print_r($row);
	// echo "<br>";
	// echo "Oil life".$oilLifeDaysC."<br>";
	// echo "last oil change".$lastOilChngDt."<br>";
	// echo "last oil changeKm".$lastOilChngKm."<br>";
	// echo "Dist tra purcher".$distTrSPur."<br>";
	// echo "Dist Since Onboard".$distSinOnB."<br>";
	// echo "Oil Dist".$oilLifeKmsC."<br>";
	// echo "Days Since Oil change".$daySOChng."<br>";
	// echo "Dust Factor".$dustEnvC."<br>";
	// echo "Km run Since Oil change".$kmSOChng."<br>"; 
	// echo "Dist Equivalent to Duration".$daysDistTrv."<br>";
	// echo "Engine Oil Temp".$engOilOpTemp."<br>";
	// echo "Cummilative oil life ".$cumOilLife."<br>";
	// echo "oil used life ".$oilUsedLife."<br>";
	// echo "dpfCorFact ".$dpfCorFact."<br>";
	// echo "tempCorrFact ".$tempCorrFact."<br>";
	// echo "engLoadCorFact ".$engLoadCorFact."<br>";
	// echo "engRPMCorFact ".$engRPMCorFact."<br>";
	// echo "engCrCorFact ".$engCrCorFact."<br>";
	// echo "dustCorFact ".$dustCorFact."<br>";
	
	// last service date and respective service date Odometer reading , sub(currentOD reading-)
	$sqldo    = "SELECT kms_read,sdate FROM `service` where devid ='$deviceID' ORDER BY `id`  DESC LIMIT 1";
	$resultdo = $conn->query($sqldo);
	$valdo    = $resultdo->fetch_assoc();
	$lastServiceOdometerReading = $valdo['kms_read'];
	$lastServicedate = $valdo['sdate'];

	//Odometer reading when vehicle registered with EVA
	$sqllo    = "SELECT travel_purchase FROM vehicles a,device b where a.device=b.id and b.device_id='$deviceID'";
	$resultlo = $conn->query($sqllo);
	$vallo    = $resultlo->fetch_assoc();
	$lastOdometerregistered = $vallo['travel_purchase'];

	// Km run after registed 
	$sqlloc    = "SELECT sum(dist) FROM `device_drive` where device_id='$deviceID'";
	$resultloc = $conn->query($sqlloc);
	$valloc    = $resultloc->fetch_assoc();
	$KMrunafterregi = $valloc['sum(dist)'];

	$serviceKmdue   = (10000 - abs($lastServiceOdometerReading - ($lastOdometerregistered + $KMrunafterregi)) - $durTr_eq_dist);
	$servicedaysdue = 365*$serviceKmdue/10000; 
	$Factordays     = $dpfCorFact * $tempCorrFact * $engLoadCorFact * $engRPMCorFact * $engCrCorFact * $dustCorFact;
	$servicedaysdue = $servicedaysdue-$Factordays;
	// $serviceKmdue   = 10000*$servicedaysdue/365;
	$daysDiff       = date_diff($lastServicedate , date('Y-m-d'));
	$servicedaysdueactuval = 365 - $daysDiff ;
	if($servicedaysdueactuval < $servicedaysdue)
	{
		$servicedaysdue = $servicedaysdueactuval;
	}
	else
	{
		$servicedaysdue = $servicedaysdue;
	}

	$servicedaysdue =  round($servicedaysdue);
	// $serviceKmdue   =  10000 * $servicedaysdue / 365;
	// echo "Kms Due".$serviceKmdue; 
	// echo "Kms Due".$servicedaysdue; 

	if($servicedaysdue < 0)
	{
		$servicedaysdue = 0;
		$serviceKmdue   = 0;
	}
	$pOfOilLf = round($servicedaysdue * 100 / 365);
	

	if($cumOilLife <= $daysDistTrv)
	{
		$oilUsedLife = $daysDistTrv + ($dpfCorFact * $tempCorrFact * $engLoadCorFact * $engRPMCorFact * $engCrCorFact * $dustCorFact);
	}
	//Point 20 need to be set in text
	return array($oilUsedLife,$pOfOilLf,$serviceKmdue,$servicedaysdue);

	//============== End ==========
}

function totSpeedLast($sp,$fld,$did,$type="",$aD="desc")
{
	$data = driveSprint($sp);
	$dVal =  $did;
	$datAct = new datAct('device1');
	if($type != 1)
	{
		$spd = $datAct->valuefield($fld);
		$spd = $spd[0];
	}
	else
	{
		$spd = $fld;
	}
	//(60*5)
	$speedT = 0;
	foreach($data as $d)
	{
		$sprint = $d['Fsprint'];
		
		$sql = "select $spd from device_data where device_id='$dVal' and Fsprint='$sprint' and  ($spd*1) <> 0 order by time_s $aD limit 0,1";
		$speed =  select($sql);
		$speed = $speed[0][0];
		//echo "$speed / $tot";
		//$avg = $speed / $tot;
		$speedT += $speed;		
	}	
	$speedT = $speedT;
	//unset($last);
	unset($data);
	unset($speed);
	return $speedT; 
}

function maxSpeed($sp,$fld,$type="",$did)
{
	$data = driveSprint($sp);
	$dVal =  $did;
	$datAct = new datAct('device1');
	if($type != 1)
	{
		$spd = $datAct->valuefield($fld);
		$spd = $spd[0];
	}
	else
	{
		$spd = $fld;
	}
	//(60*5)
	$speedT = 0;
	$spVal = array();
	foreach($data as $d)
	{
		$sprint = $d['Fsprint'];
		$sql = "select max($spd*1) from device_data where device_id='$dVal' and Fsprint='$sprint' and  ($spd*1) <> 0 order by time_s asc";
		$speed =  select($sql);
		$spVal[$speedT] = $speed[0][0];
		$speedT++;			
	}
	unset($last);
	unset($data);
	unset($speed);
	unset($spd);
	$max = (count($spVal) >= 1) ? max($spVal) : 0;
	//echo "$max |";
	return $max;
}
/*********************************************************************************************/
$arrayData = array();
$tmpName   = $argv[1];
if($_POST['csvfile'])
{
	$tmpName = $_POST['csvfile'];
}
$fileName  = $tmpName;
$tmpName   = "/var/www/html/eva.enginecal.com/incoming-csv/$tmpName";
$device_id = csvToMysql($tmpName,$conn);

if(!empty($device_id))
{
	if(resetdeviceFile($device_id,$fileName,$conn))
	{
		$file = getdeviceFile($device_id,$conn);
		if(!empty($file))
		{
			$file     = explode(';',$file);
			$pathInfo = getcwd();
			$fileName = $file[0];

			$cmd = "/usr/bin/timeout 60m /usr/bin/php $pathInfo/fileupload-back-app.php $fileName";
			$sql = "update device set act = '$cmd' where device_id = '$device_id'";
			$conn->query($sql);
		}
		else
		{
			$sql = "update device set act = 'No Pending Files' where device_id = '$device_id'";
			$conn->query($sql);
		}
	}
}

unset($valLast);
unset($postVal);
?>