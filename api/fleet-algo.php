<?php
include 'common.php';

$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

function calcStatMonData($devId,$iLoopData,$iLoopSize,$driveID,$conn,$colHeader)
{
	/********************************************************************************************
	 * INITILASE all required variables
	 * *****************************************************************************************/
	$calval = array();
	$count = 0;
	$countstats = 0;
	$distT = 0;
	$totalDist = 0;
	$oldtimestamp = 0;
	$totaldur = 0;
	$rowcount = 0;
	$maxVehSpd = 0;
	$maxTorque = 0;
	$firsttimestamp = 0;
	$idletime = 0;
	$ovrruntime = 0;
	$ovrrundist = 0;
	$fFlowSum = 0;
	$fFlowAvg = 0;
	$vehspdSum = 0;
	$vehSpdAvg = 0;
	$maxCRP = 0;
	$maxCRPcsv = 0;
	$crpEload = 0;
	$crpRpm = 0;
	$idleCRP = 0;
	$startNewSegment = 1; 		// 1 for new segment has to start and 0 for the ongoing segment.
	$MAPPeak0 = 0;
	$MAPPeak1 = 0;
	$maxMAP = 0;
	$maxMAPcon = 0;
	$maxMAPcsv = 0;
	$maxMAPEload = 0;
	$maxairMAP = 0;
	$idleMAPFlag = 0;
	$idleMAP = 0;
	$baroIdleMAP = 0;
	$baroMaxMAP = 0;
	$maxMAF = 0;
	//$minBatVolt = 0;
	$maxBatVolt = 0;
	$ignOnMinBatVolt = 0;
	$ignOnMaxBatVolt = 0;
	$batcool = 0;
	$maxCool = 0;
	$minCool = 0;
	$prevCool = 0;
	$countcool = 0;
	$countcoollow = 0;
	$countcool = 0;
	$calcMonBuf = array();
	$calcMonBuf[0]=0;
	 $calcMonBuf[1]=0;
	$deltaRPM = 0;
	$deltaRPMcurr = 0;
	$currRpmOscScore = 0;
	$curCO2 = 0;
	$totCO2 = 0;
	$batState = 0;
	$altState = 0;
	$regState = 0;
	$batOverallState = 0;
	$IdleBoostSCORE = 0;
	$PeakBoostSCORE = 0;
	$IdleAirBoostSCORE = 0;
	$PeakairBoostSCORE = 0;
	$IdleCRPSCORE = 0;
	$PeakCRPSCORE = 0;
	$CombPresState = 0;
	$coolHelScore = 0;
	$pi =  3.14159;
	$mm = 28.97;
	$r = 8.314;
	$gasconst = 287.053;		//Universal Gas constant
	$gacc = 9.80665;			//Gravitational acceleration
	$stdBaro = 101325;			//Standard barometric pressure @sea level
	$rpmIndex = 0;
	$fuelFlowL = 0;
	$latMaxTor = 0;
	$lonMaxTor = 0;
	$altMaxTor = 0;
	$atmpress =0;
	$eloadMaxTor =0;
	$rpmMaxTor =0;
	$AvgEloadFact=0;
	$TotalFact=0;
	$TorquePer=0;
	$NewMaxTorque=0;
	/********************************************************************************************
	 * STEP1: Get all calibration data based on device ID
	 * *****************************************************************************************/
	//print_r($devId);die;
	$caldata = getCaliPramVal($devId,$conn);
	$calval  = $caldata['calval'];
	$fuel    = $caldata['fuel'];

	//print_r($calval);
	if($calval != "")
	{

		$vType = $fuel;
		$baseN = $calval['Eng_eff_C']; 
		$ed = $calval['Eng_Disp_C'];
		$CurrCityPress = $calval['Baro_Press_City_C'];
		$MAP_Valid_C = $calval['MAP_Valid_C'];
		$MAF_Valid_C = $calval['MAF_Valid_C'];
		$Turbo_Avail_C = $calval['TC_Status_C'];
		$Injection_Type_C = $calval['Injection_Type_C'];
		$MAP_Type_C = $calval['MAP_Type_C'];
		$intercoolerPresent = $calval['IC_Status_C'];
		$VVT_Status_C = $calval['VVT_Status_C'];
		$Veh_Type_C = $calval['Veh_Type_C'];
		$Cool_Lmt_C = $calval['Cool_Lmt_C'];
		$RPM_Idle_Warm_C = $calval['RPM_Idle_Warm_C'];
		$RPM_Idle_Ld1_C = $calval['RPM_Idle_Ld1_C'];
		$RPM_Idle_Ld2_C = $calval['RPM_Idle_Ld2_C'];
		$Eload_Max_C = $calval['Eload_Max_C'];
		$RPM_Limit1 = $calval['Eff_RPM_Limit1_C'];
		$RPM_Limit2 = $calval['Eff_RPM_Limit2_C'];
		$RPM_Limit3 = $calval['Eff_RPM_Limit3_C'];
		$RPM_Limit4 = $calval['Eff_RPM_Limit4_C'];
		$RPM_Limit5 = $calval['Eff_RPM_Limit5_C'];
		$VolEff_Eload_1_10_RPMLimit1_C = $calval['VolEff_Eload_1_10_RPMLimit1_C'];
		$VolEff_Eload_1_10_RPMLimit2_C = $calval['VolEff_Eload_1_10_RPMLimit2_C'];
		$VolEff_Eload_1_10_RPMLimit3_C = $calval['VolEff_Eload_1_10_RPMLimit3_C'];
		$VolEff_Eload_1_10_RPMLimit4_C = $calval['VolEff_Eload_1_10_RPMLimit4_C'];
		$VolEff_Eload_1_10_RPMLimit5_C = $calval['VolEff_Eload_1_10_RPMLimit5_C'];
		$VolEff_Eload_11_20_RPMLimit1_C = $calval['VolEff_Eload_11_20_RPMLimit1_C'];
		$VolEff_Eload_11_20_RPMLimit2_C = $calval['VolEff_Eload_11_20_RPMLimit2_C'];
		$VolEff_Eload_11_20_RPMLimit3_C = $calval['VolEff_Eload_11_20_RPMLimit3_C'];
		$VolEff_Eload_11_20_RPMLimit4_C = $calval['VolEff_Eload_11_20_RPMLimit4_C'];
		$VolEff_Eload_11_20_RPMLimit5_C = $calval['VolEff_Eload_11_20_RPMLimit5_C'];
		$VolEff_Eload_21_30_RPMLimit1_C = $calval['VolEff_Eload_21_30_RPMLimit1_C'];
		$VolEff_Eload_21_30_RPMLimit2_C = $calval['VolEff_Eload_21_30_RPMLimit2_C'];
		$VolEff_Eload_21_30_RPMLimit3_C = $calval['VolEff_Eload_21_30_RPMLimit3_C'];
		$VolEff_Eload_21_30_RPMLimit4_C = $calval['VolEff_Eload_21_30_RPMLimit4_C'];
		$VolEff_Eload_21_30_RPMLimit5_C = $calval['VolEff_Eload_21_30_RPMLimit5_C'];
		$VolEff_Eload_31_40_RPMLimit1_C = $calval['VolEff_Eload_31_40_RPMLimit1_C'];
		$VolEff_Eload_31_40_RPMLimit2_C = $calval['VolEff_Eload_31_40_RPMLimit2_C'];
		$VolEff_Eload_31_40_RPMLimit3_C = $calval['VolEff_Eload_31_40_RPMLimit3_C'];
		$VolEff_Eload_31_40_RPMLimit4_C = $calval['VolEff_Eload_31_40_RPMLimit4_C'];
		$VolEff_Eload_31_40_RPMLimit5_C = $calval['VolEff_Eload_31_40_RPMLimit5_C'];
		$VolEff_Eload_41_50_RPMLimit1_C = $calval['VolEff_Eload_41_50_RPMLimit1_C'];
		$VolEff_Eload_41_50_RPMLimit2_C = $calval['VolEff_Eload_41_50_RPMLimit2_C'];
		$VolEff_Eload_41_50_RPMLimit3_C = $calval['VolEff_Eload_41_50_RPMLimit3_C'];
		$VolEff_Eload_41_50_RPMLimit4_C = $calval['VolEff_Eload_41_50_RPMLimit4_C'];
		$VolEff_Eload_41_50_RPMLimit5_C = $calval['VolEff_Eload_41_50_RPMLimit5_C'];
		$VolEff_Eload_51_60_RPMLimit1_C = $calval['VolEff_Eload_51_60_RPMLimit1_C'];
		$VolEff_Eload_51_60_RPMLimit2_C = $calval['VolEff_Eload_51_60_RPMLimit2_C'];
		$VolEff_Eload_51_60_RPMLimit3_C = $calval['VolEff_Eload_51_60_RPMLimit3_C'];
		$VolEff_Eload_51_60_RPMLimit4_C = $calval['VolEff_Eload_51_60_RPMLimit4_C'];
		$VolEff_Eload_51_60_RPMLimit5_C = $calval['VolEff_Eload_51_60_RPMLimit5_C'];
		$VolEff_Eload_61_80_RPMLimit1_C = $calval['VolEff_Eload_61_80_RPMLimit1_C'];
		$VolEff_Eload_61_80_RPMLimit2_C = $calval['VolEff_Eload_61_80_RPMLimit2_C'];
		$VolEff_Eload_61_80_RPMLimit3_C = $calval['VolEff_Eload_61_80_RPMLimit3_C'];
		$VolEff_Eload_61_80_RPMLimit4_C = $calval['VolEff_Eload_61_80_RPMLimit4_C'];
		$VolEff_Eload_61_80_RPMLimit5_C = $calval['VolEff_Eload_61_80_RPMLimit5_C'];
		$VolEff_Eload_81_90_RPMLimit1_C = $calval['VolEff_Eload_81_90_RPMLimit1_C'];
		$VolEff_Eload_81_90_RPMLimit2_C = $calval['VolEff_Eload_81_90_RPMLimit2_C'];
		$VolEff_Eload_81_90_RPMLimit3_C = $calval['VolEff_Eload_81_90_RPMLimit3_C'];
		$VolEff_Eload_81_90_RPMLimit4_C = $calval['VolEff_Eload_81_90_RPMLimit4_C'];
		$VolEff_Eload_81_90_RPMLimit5_C = $calval['VolEff_Eload_81_90_RPMLimit5_C'];
		$VolEff_Eload_91_94_RPMLimit1_C = $calval['VolEff_Eload_91_94_RPMLimit1_C'];
		$VolEff_Eload_91_94_RPMLimit2_C = $calval['VolEff_Eload_91_94_RPMLimit2_C'];
		$VolEff_Eload_91_94_RPMLimit3_C = $calval['VolEff_Eload_91_94_RPMLimit3_C'];
		$VolEff_Eload_91_94_RPMLimit4_C = $calval['VolEff_Eload_91_94_RPMLimit4_C'];
		$VolEff_Eload_91_94_RPMLimit5_C = $calval['VolEff_Eload_91_94_RPMLimit5_C'];
		$VolEff_Eload_95_98_RPMLimit1_C = $calval['VolEff_Eload_95_98_RPMLimit1_C'];
		$VolEff_Eload_95_98_RPMLimit2_C = $calval['VolEff_Eload_95_98_RPMLimit2_C'];
		$VolEff_Eload_95_98_RPMLimit3_C = $calval['VolEff_Eload_95_98_RPMLimit3_C'];
		$VolEff_Eload_95_98_RPMLimit4_C = $calval['VolEff_Eload_95_98_RPMLimit4_C'];
		$VolEff_Eload_95_98_RPMLimit5_C = $calval['VolEff_Eload_95_98_RPMLimit5_C'];
		$VolEff_Eload_99_100_RPMLimit1_C = $calval['VolEff_Eload_99_100_RPMLimit1_C'];
		$VolEff_Eload_99_100_RPMLimit2_C = $calval['VolEff_Eload_99_100_RPMLimit2_C'];
		$VolEff_Eload_99_100_RPMLimit3_C = $calval['VolEff_Eload_99_100_RPMLimit3_C'];
		$VolEff_Eload_99_100_RPMLimit4_C = $calval['VolEff_Eload_99_100_RPMLimit4_C'];
		$VolEff_Eload_99_100_RPMLimit5_C = $calval['VolEff_Eload_99_100_RPMLimit5_C'];
		$Eqratio_Eload_1_10_RPMLimit1_C = $calval['Eqratio_Eload_1_10_RPMLimit1_C'];
		$Eqratio_Eload_1_10_RPMLimit2_C = $calval['Eqratio_Eload_1_10_RPMLimit2_C'];
		$Eqratio_Eload_1_10_RPMLimit3_C = $calval['Eqratio_Eload_1_10_RPMLimit3_C'];
		$Eqratio_Eload_1_10_RPMLimit4_C = $calval['Eqratio_Eload_1_10_RPMLimit4_C'];
		$Eqratio_Eload_1_10_RPMLimit5_C = $calval['Eqratio_Eload_1_10_RPMLimit5_C'];
		$Eqratio_Eload_11_20_RPMLimit1_C = $calval['Eqratio_Eload_11_20_RPMLimit1_C'];
		$Eqratio_Eload_11_20_RPMLimit2_C = $calval['Eqratio_Eload_11_20_RPMLimit2_C'];
		$Eqratio_Eload_11_20_RPMLimit3_C = $calval['Eqratio_Eload_11_20_RPMLimit3_C'];
		$Eqratio_Eload_11_20_RPMLimit4_C = $calval['Eqratio_Eload_11_20_RPMLimit4_C'];
		$Eqratio_Eload_11_20_RPMLimit5_C = $calval['Eqratio_Eload_11_20_RPMLimit5_C'];
		$Eqratio_Eload_21_30_RPMLimit1_C = $calval['Eqratio_Eload_21_30_RPMLimit1_C'];
		$Eqratio_Eload_21_30_RPMLimit2_C = $calval['Eqratio_Eload_21_30_RPMLimit2_C'];
		$Eqratio_Eload_21_30_RPMLimit3_C = $calval['Eqratio_Eload_21_30_RPMLimit3_C'];
		$Eqratio_Eload_21_30_RPMLimit4_C = $calval['Eqratio_Eload_21_30_RPMLimit4_C'];
		$Eqratio_Eload_21_30_RPMLimit5_C = $calval['Eqratio_Eload_21_30_RPMLimit5_C'];
		$Eqratio_Eload_31_40_RPMLimit1_C = $calval['Eqratio_Eload_31_40_RPMLimit1_C'];
		$Eqratio_Eload_31_40_RPMLimit2_C = $calval['Eqratio_Eload_31_40_RPMLimit2_C'];
		$Eqratio_Eload_31_40_RPMLimit3_C = $calval['Eqratio_Eload_31_40_RPMLimit3_C'];
		$Eqratio_Eload_31_40_RPMLimit4_C = $calval['Eqratio_Eload_31_40_RPMLimit4_C'];
		$Eqratio_Eload_31_40_RPMLimit5_C = $calval['Eqratio_Eload_31_40_RPMLimit5_C'];
		$Eqratio_Eload_41_50_RPMLimit1_C = $calval['Eqratio_Eload_41_50_RPMLimit1_C'];
		$Eqratio_Eload_41_50_RPMLimit2_C = $calval['Eqratio_Eload_41_50_RPMLimit2_C'];
		$Eqratio_Eload_41_50_RPMLimit3_C = $calval['Eqratio_Eload_41_50_RPMLimit3_C'];
		$Eqratio_Eload_41_50_RPMLimit4_C = $calval['Eqratio_Eload_41_50_RPMLimit4_C'];
		$Eqratio_Eload_41_50_RPMLimit5_C = $calval['Eqratio_Eload_41_50_RPMLimit5_C'];
		$Eqratio_Eload_51_60_RPMLimit1_C = $calval['Eqratio_Eload_51_60_RPMLimit1_C'];
		$Eqratio_Eload_51_60_RPMLimit2_C = $calval['Eqratio_Eload_51_60_RPMLimit2_C'];
		$Eqratio_Eload_51_60_RPMLimit3_C = $calval['Eqratio_Eload_51_60_RPMLimit3_C'];
		$Eqratio_Eload_51_60_RPMLimit4_C = $calval['Eqratio_Eload_51_60_RPMLimit4_C'];
		$Eqratio_Eload_51_60_RPMLimit5_C = $calval['Eqratio_Eload_51_60_RPMLimit5_C'];
		$Eqratio_Eload_61_80_RPMLimit1_C = $calval['Eqratio_Eload_61_80_RPMLimit1_C'];
		$Eqratio_Eload_61_80_RPMLimit2_C = $calval['Eqratio_Eload_61_80_RPMLimit2_C'];
		$Eqratio_Eload_61_80_RPMLimit3_C = $calval['Eqratio_Eload_61_80_RPMLimit3_C'];
		$Eqratio_Eload_61_80_RPMLimit4_C = $calval['Eqratio_Eload_61_80_RPMLimit4_C'];
		$Eqratio_Eload_61_80_RPMLimit5_C = $calval['Eqratio_Eload_61_80_RPMLimit5_C'];
		$Eqratio_Eload_81_90_RPMLimit1_C = $calval['Eqratio_Eload_81_90_RPMLimit1_C'];
		$Eqratio_Eload_81_90_RPMLimit2_C = $calval['Eqratio_Eload_81_90_RPMLimit2_C'];
		$Eqratio_Eload_81_90_RPMLimit3_C = $calval['Eqratio_Eload_81_90_RPMLimit3_C'];
		$Eqratio_Eload_81_90_RPMLimit4_C = $calval['Eqratio_Eload_81_90_RPMLimit4_C'];
		$Eqratio_Eload_81_90_RPMLimit5_C = $calval['Eqratio_Eload_81_90_RPMLimit5_C'];
		$Eqratio_Eload_91_94_RPMLimit1_C = $calval['Eqratio_Eload_91_94_RPMLimit1_C'];
		$Eqratio_Eload_91_94_RPMLimit2_C = $calval['Eqratio_Eload_91_94_RPMLimit2_C'];
		$Eqratio_Eload_91_94_RPMLimit3_C = $calval['Eqratio_Eload_91_94_RPMLimit3_C'];
		$Eqratio_Eload_91_94_RPMLimit4_C = $calval['Eqratio_Eload_91_94_RPMLimit4_C'];
		$Eqratio_Eload_91_94_RPMLimit5_C = $calval['Eqratio_Eload_91_94_RPMLimit5_C'];
		$Eqratio_Eload_95_98_RPMLimit1_C = $calval['Eqratio_Eload_95_98_RPMLimit1_C'];
		$Eqratio_Eload_95_98_RPMLimit2_C = $calval['Eqratio_Eload_95_98_RPMLimit2_C'];
		$Eqratio_Eload_95_98_RPMLimit3_C = $calval['Eqratio_Eload_95_98_RPMLimit3_C'];
		$Eqratio_Eload_95_98_RPMLimit4_C = $calval['Eqratio_Eload_95_98_RPMLimit4_C'];
		$Eqratio_Eload_95_98_RPMLimit5_C = $calval['Eqratio_Eload_95_98_RPMLimit5_C'];
		$Eqratio_Eload_99_100_RPMLimit1_C = $calval['Eqratio_Eload_99_100_RPMLimit1_C'];
		$Eqratio_Eload_99_100_RPMLimit2_C = $calval['Eqratio_Eload_99_100_RPMLimit2_C'];
		$Eqratio_Eload_99_100_RPMLimit3_C = $calval['Eqratio_Eload_99_100_RPMLimit3_C'];
		$Eqratio_Eload_99_100_RPMLimit4_C = $calval['Eqratio_Eload_99_100_RPMLimit4_C'];
		$Eqratio_Eload_99_100_RPMLimit5_C = $calval['Eqratio_Eload_99_100_RPMLimit5_C'];
		$Ovr_Run_EqRatio_C = $calval['Ovr_Run_EqRatio_C'];
		$Ovr_Run_VolEff_C = $calval['Ovr_Run_VolEff_C'];
		$Ovr_Run_FuelFact_C = $calval['Ovr_Run_FuelFact_C'];
		$Ovr_Run_FuelFact_Eload_C = $calval['Ovr_Run_FuelFact_Eload_C'];
		$constfact_Eload_0_20 = $calval['Fact_Fuel_Run_ELoad_0_20_C'];
		$constfact_Eload_20_40 = $calval['Fact_Fuel_Run_ELoad_20_40_C'];
		$constfact_Eload_40_60 = $calval['Fact_Fuel_Run_ELoad_40_60_C'];
		$constfact_Eload_60_80 = $calval['Fact_Fuel_Run_ELoad_60_80_C'];
		$constfact_Eload_80_100 = $calval['Fact_Fuel_Run_ELoad_80_100_C'];
		$constfactidle_Eload1 = $calval['Fact_Fuel_Idle_ELoad1_C'];
		$constfactidle_Eload2 = $calval['Fact_Fuel_Idle_ELoad2_C'];
		$co2_Cnv_Fac_C = $calval['Co2_Cnv_Fac_C'];
		//**************************** Battery Algo labels *************************************//
		$Bat_Coolant_Lmt1_C = $calval['Bat_Cool_Lmt1_C'];
		$Bat_Coolant_Lmt2_C = $calval['Bat_Cool_Lmt2_C'];
		//battery condition limts when coolant is below 15 deg
		$Bat_UnderCoolLmt1_Lmt1_C = $calval['Bat_UnderCoolLmt1_Lmt1_C'];
		$Bat_UnderCoolLmt1_Lmt2_C = $calval['Bat_UnderCoolLmt1_Lmt2_C'];
		$Bat_UnderCoolLmt1_Lmt3_C = $calval['Bat_UnderCoolLmt1_Lmt3_C'];
		$Bat_UnderCoolLmt1_Lmt4_C = $calval['Bat_UnderCoolLmt1_Lmt4_C'];
		//battery condition limts when coolant is below 35 deg
		$Bat_OverCoolLmt1_Lmt1_C = $calval['Bat_OverCoolLmt1_Lmt1_C'];
		$Bat_OverCoolLmt1_Lmt2_C = $calval['Bat_OverCoolLmt1_Lmt2_C'];
		$Bat_OverCoolLmt1_Lmt3_C = $calval['Bat_OverCoolLmt1_Lmt3_C'];
		$Bat_OverCoolLmt1_Lmt4_C = $calval['Bat_OverCoolLmt1_Lmt4_C'];
		//battery condition limts when coolant is above 35 deg
		$Bat_OverCoolLmt2_Lmt1_C = $calval['Bat_OverCoolLmt2_Lmt1_C'];
		$Bat_OverCoolLmt2_Lmt2_C = $calval['Bat_OverCoolLmt2_Lmt2_C'];
		$Bat_OverCoolLmt2_Lmt3_C = $calval['Bat_OverCoolLmt2_Lmt3_C'];
		$Bat_OverCoolLmt2_Lmt4_C = $calval['Bat_OverCoolLmt2_Lmt4_C'];
		//*************************** Torque Algo labels *************************************//
		$MaxTorque = $calval['Max_Torque_C'];
		$Trq_Lmt1_C = $calval['Trq_Lmt1_C'];
		$Trq_Lmt2_C = $calval['Trq_Lmt2_C'];
		$Trq_Lmt3_C = $calval['Trq_Lmt3_C'];
		$Trq_Lmt4_C = $calval['Trq_Lmt4_C'];
		$Trq_RPM_PkLmt_C = $calval['Trq_RPM_PkLmt_C'];
		$Trq_EloadOffset_PkLmt_C = $calval['Trq_EloadOffset_PkLmt_C'];
		$Trq_Cool_PkLmt_C = $calval['Trq_Cool_PkLmt_C'];
		//$RPM_Limit2 = $calval['Eff_RPM_Limit2_C'];
		//$RPM_Limit3 = $calval['Eff_RPM_Limit3_C'];
		//$RPM_Limit4 = $calval['Eff_RPM_Limit4_C'];
		$MaxMAP_C = $calval['MAP_Abs_PkLmt3_C'];
		//************************* Coolant Algo labels *************************************//
		$Base_OvrHt_Cool_Lmt_C = $calval['Base_OvrHt_Cool_Lmt_C'];
		$Cool_Temp_Lmt4_C = $calval['Cool_Temp_Lmt4_C'];
		$Cool_Temp_Lmt1percent_C = $calval['Cool_Temp_Lmt1_C'];
		$Cool_Temp_Lmt2percent_C = $calval['Cool_Temp_Lmt2_C'];
		$Cool_Temp_Lmt3percent_C = $calval['Cool_Temp_Lmt3_C'];
		//************************ Fuel Rail Pressure labels *******************************//
		$CRP_Type_C = $calval['CRP_Type_C'];
		$Base_CRP_Lmt_C = $calval['Base_CRP_Lmt_C'];
		$CRP_MinLmtScore3_C = $calval['CRP_MinLmt3_C'];
		$CRP_MinLmtScore1_C = $calval['CRP_MinLmt1_C'];
		$CRP_MaxLmtScore3_C = $calval['CRP_MaxLmt3_C'];
		$CRP_MaxLmtScore1_C = $calval['CRP_MaxLmt1_C'];
		$CRP_RPM_PkLmt_C = $calval['CRP_RPM_PkLmt_C'];
		$CRP_RPMOffset_PkLmt_C = $calval['CRP_RPMOffset_PkLmt_C'];
		$CRP_ELoadOffset_PkLmt_C = $calval['CRP_ELoadOffset_PkLmt_C'];
		//******************* Turbocharger Manifold Air Pressure labels *******************************//
		$MAP_Dif_IdleLmt3_C = $calval['MAP_Dif_IdleLmt3_C'];
		$MAP_Dif_IdleLmt1_C = $calval['MAP_Dif_IdleLmt1_C'];
		$MAP_Abs_PkLmt_C = $calval['MAP_Abs_PkLmt_C'];
		$MAP_Dif_PkLmt_C = $calval['MAP_Dif_PkLmt_C'];
		$MAP_Abs_PkLmt3old_C = $calval['MAP_Abs_PkLmt3_C'];
		$MAP_Abs_PkLmt1_C = $calval['MAP_Abs_PkLmt1_C'];
		$MAP_Dif_PkLmt3old_C = $calval['MAP_Dif_PkLmt3_C'];
		$MAP_Dif_PkLmt1_C = $calval['MAP_Dif_PkLmt1_C'];
		$MAP_RPM_PkLmt_C = $RPM_Limit2;			//This is the limit above which peak MAP will be got
		$MAP_RPMOffset_PkLmt_C = $calval['MAP_RPMOffset_PkLmt_C'];
		$MAP_EloadOffset_PkLmt_C = $calval['MAP_EloadOffset_PkLmt_C'];
		//****************** Air System Manifold Air Pressure labels **************************//
		$MAP_Air_IdleLmt_C = $calval['MAP_Air_IdleLmt_C'];
		$MAP_Air_IdleLmt3_C = $calval['MAP_Air_IdleLmt3_C'];
		$MAP_Air_IdleLmt1_C = $calval['MAP_Air_IdleLmt1_C'];
		$MAP_Air_PkLmt_C = $calval['MAP_Air_PkLmt_C'];
		$MAP_Air_PkLmt3_C = $calval['MAP_Air_PkLmt3_C'];
		$MAP_Air_PkLmt1_C = $calval['MAP_Air_PkLmt1_C'];
		$MAP_Air_EloadOffset_PkLmt_C = $calval['MAP_Air_EloadOffset_PkLmt_C'];
		$MAP_Air_RPM_LwrLmt_C = $calval['MAP_Air_RPM_LwrLmt_C'];
		$MAP_Air_RPM_PkLmt_C = $calval['MAP_Air_RPM_PkLmt_C'];
		$Max_Air_Trq_RPM_C = $calval['Max_Air_Trq_RPM_C'];
		$Multi_Cyl_C = $calval['Multi_Cyl_C'];
		//**************** RPM Oscillation Anomaly Analysis labels **********************//
		$Osc_RPM_Lmt1_C = $calval['Osc_RPM_Lmt1_C'];
		$Osc_RPM_Lmt2_C = $calval['Osc_RPM_Lmt2_C'];
		$Osc_RPM_Lmt3_C = $calval['Osc_RPM_Lmt3_C'];
		$Osc_RPM_Lmt4_C = $calval['Osc_RPM_Lmt4_C'];
		$Osc_EloadOffset_Lmt_C = $calval['Osc_EloadOffset_Lmt_C'];
		$Eload_Idle_Warm_C = $calval['ELoad_Idle_Warm_C'];
		$Eload_Idle_Ld1_C = $calval['ELoad_Idle_Ld1_C'];
		$Eload_Idle_Ld2_C = $calval['ELoad_Idle_Ld2_C'];
	}
	$MAPrpmValA = $RPM_Idle_Warm_C - $MAP_RPMOffset_PkLmt_C;
	$MAPrpmValB = $RPM_Idle_Warm_C + $MAP_RPMOffset_PkLmt_C;
	$MAPrpmValA1 = $RPM_Idle_Ld1_C - $MAP_RPMOffset_PkLmt_C;
	$MAPrpmValB1 = $RPM_Idle_Ld1_C + $MAP_RPMOffset_PkLmt_C;
	$MAPrpmValA2 = $RPM_Idle_Ld2_C - $MAP_RPMOffset_PkLmt_C;
	$MAPrpmValB2 = $RPM_Idle_Ld2_C + $MAP_RPMOffset_PkLmt_C;
	$MAPPeakrpmValA = $Max_Air_Trq_RPM_C - $MAP_Air_RPM_LwrLmt_C;
	$MAPPeakrpmValB = $Max_Air_Trq_RPM_C + $MAP_Air_RPM_PkLmt_C;
	$rpmValA = $RPM_Idle_Warm_C - $CRP_RPMOffset_PkLmt_C;
	$rpmValB = $RPM_Idle_Warm_C + $CRP_RPMOffset_PkLmt_C;
	$rpmValA1 = $RPM_Idle_Ld1_C - $CRP_RPMOffset_PkLmt_C;
	$rpmValB1 = $RPM_Idle_Ld1_C + $CRP_RPMOffset_PkLmt_C;
	$rpmValA2 = $RPM_Idle_Ld2_C - $CRP_RPMOffset_PkLmt_C;
	$rpmValB2 = $RPM_Idle_Ld2_C + $CRP_RPMOffset_PkLmt_C;

	/********************************************************************************************
	 * STEP2: Compute Modelled PIDs
	 * ******************************************************************************************/
	//print_r($iLoopData);
	foreach($iLoopData as & $currRowData)
	{
		//print_r($currRowData);
		//Get value for various PIDs in current row
		$obdCoolantTemperature = $currRowData['F1'];
		$map = $currRowData['F3'];
		$obdManifoldPressure = $map;
		$rpm = $currRowData['F4'];
		$obdRpm = $rpm;
		$vehspd = $currRowData['F5'];
		$maf = $currRowData['F6'];
		$throtPos = $currRowData['F7'];
		$distWithMIL = $currRowData['F8'];
		$engineState = $currRowData['F16']; 
		$obdIntakeTemperature = $currRowData['F18'];
		$inTakeTemp = $obdIntakeTemperature + 273;
		$eLoad = $currRowData['F19'];
		$obdEngineLoad = $eLoad;
		$batVolt = $currRowData['F34'];
		$actEquiRatio = $currRowData['F35'];
		$ratio = $actEquiRatio;
		$flSys = $currRowData['F36'];
		$crp = $currRowData['F37'];
		$obdCommandedEGR = $currRowData['F38'];
		$baro_value = $currRowData['F41'];
		$accPosD = $currRowData['F42'];
		$currtimestamp = $currRowData['time_s'];
		$fuelFlow = $currRowData['Ffuelflow_l'];
		
		//Get current engine state
		$calcEngState = calcEngStateNew($devId,$engineState,$rpm,$vehspd,$vType,$flSys,$eLoad);

		$currRowData['F43'] = $calcEngState;
		// echo "\n Chk pt 0. im here calcEngState = $calcEngState \n";
		
		//Get Drive sprint id
		$Fsprint = $driveID;
		$currRowData['Fsprint'] = $Fsprint;
		// echo"\n Chk pt 1. im here $Fsprint \n";
		//$postVal = $this -> devFlowInfoAPI($devId,$currRowData,'',$fspLast,$calval,$count);	
		$count++;
		if($rpm > 0)
		{
			$countstats++;
		}
		else
		{
			//Do nothing
		}
		/********************************************************************************************
		 * STEP 2.1 : Compute Distance Travelled
		 * ******************************************************************************************/
		//Distance Travelled (km) = Previous Distance (km) + (Velocity (km/hr) * (Current Time Stamp (s) – Previous Time Stamp (s)) /3600
		if($oldtimestamp == 0)
		{
			$oldtimestamp = $currtimestamp;
			$firsttimestamp = $oldtimestamp;
		}
		$timedif = ($currtimestamp - $oldtimestamp);
		if($timedif < 0)
		{
			$timedif = 0;
		}
		else
		{
			//do nothing
		}
		$timedif = $timedif / 1000;		//divided by 1000 to get time in seconds
		//Get Distance Travelled
		if (is_numeric($vehspd ) && is_numeric($timedif) && is_numeric($currRowData['Fdist'])){
		$currRowData['Fdist'] += (($vehspd * $timedif) / 3600);
	}else
	{
		// do nothing
	}
		
		$oldtimestamp = $currtimestamp;
		//Get Duration of Drive in seconds
		$currRowData['F49'] = (($currtimestamp - $firsttimestamp) / 1000);
		$totaldur = $currRowData['F49'];
		//Get Duration in Idle
		if($calcEngState == 2)
		{
			$idletime += ($timedif);
			$currRowData['F65'] = $idletime;
		}
		//Get Duration and Distance Travelled in Overrun 
		elseif($calcEngState == 4)
		{
			//Time spent in overrun
			$ovrruntime += ($timedif);
			$currRowData['F61'] = $ovrruntime;
			
			//Distance covered in overrun
			$ovrrundist += (($vehspd * $timedif) / 3600);
			$currRowData['F63'] = $ovrrundist;
		}
		//echo "ovrrundist = $ovrrundist | ovrruntime = $ovrruntime | idletime = $idletime\n";
		
		/********************************************************************************************
		 * STEP 2.2 : Fuel FLow, Power and Torque Calculations 
		 * ******************************************************************************************/
		$pi =  3.14159;
		$mm = 28.97;
		$r = 8.314;		
		if($baro_value == 0 || $baro_value == "")
		{
			//Calculate baro pressure if not available
			$altbaro = $currRowData['alt'];
			if(($altbaro == 0) || ($altbaro == ""))
			{
				$altbaro = 810;
			}
			else
			{
				//Do nothing
			}
			$exponent = ($altbaro * $gacc) / ($gasconst * $inTakeTemp);
			$calBaro = $stdBaro * exp(-$exponent);
			$calBaroP = floor($calBaro / 1000);
			$calBaroP = ($calBaroP - 1);
			$currRowData['F41'] = $calBaroP;
			$obdBarometricPressure = $calBaroP;
			//echo "altbaro = $altbaro | exponent = $exponent | calBaroP = $calBaroP | obdBarometricPressure = $obdBarometricPressure \n";
		}
		else
		{
			$obdBarometricPressure = $baro_value;
		}
		
		//********************----------------New MAF to MAP Formula Start--------------------********************//
		if($MAP_Valid_C == 1)
		{
			if($obdManifoldPressure <= 0 || $obdManifoldPressure == "")
			{
				if($maf > 0 && $MAF_Valid_C == 1)
				{
					//*****************==================Volumetric Efficiency calculation start============================*****************//
					//Diesel Engine Volumetric Efficiency
					if($vType == 2)
					{
						if($MAP_Type_C == "Differential")
						{
						 	if($eLoad <= 55)
							{
								$fact11 = pow($eLoad, 4);
								$fact21 = pow($eLoad, 3);
								$fact31 = pow($eLoad, 2);
								$fact41 = $eLoad;
								
								$fact1 = (0.000004 * $fact11);
								$fact2 = (0.0021 * $fact21);
								$fact3 = (0.1497 * $fact31);
								$fact4 = (1.9792 * $fact41);
								
								$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
								//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
							}
							elseif($eLoad <= 85)
							{
								$ve = 98 + (0.08 * $eLoad);
							}
							elseif($eLoad <= 98)
							{
								$ve = 99 + (0.05 * $eLoad);
							}
							else
							{
								$ve = 99 + (0.04 * $eLoad);
							}
						}
						else
						{
							//This condition is added so that the calculation is proper even when IAT is not read/supported
							if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
							{
								$obdIntakeTemperature = $obdCoolantTemperature;
							}
							else
							{
								//Do nothing
							}
							$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
							$barofactor = (101.3 / $obdBarometricPressure);
			
							if($calcEngState == 2)
							{
								if($obdIntakeTemperature < 20)
								{
									$ve1 = (34.4803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
								else
								{
									$ve1 = (34.6803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
							}
							else
							{
								if($vehspd > 20)
								{
									$ve = (-0.55 * $factor * $factor) + (11.35 * $factor) + 44.398;
									$ve = round($ve,0);
								}
								else
								{
									$ve = (-0.65 * $factor * $factor) + (11.35 * $factor) + 38.398;
									$ve = round($ve,0);
								}
							}
						}
					}
					
					//Petrol Engine Volumetric Efficiency
					else
					{
						if($Turbo_Avail_C == "No")
						{
							if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
							{
								$obdIntakeTemperature = $obdCoolantTemperature;
							}
							else
							{
								//Do nothing
							}
							$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
							$barofactor = (101.3 / $obdBarometricPressure);
				
							if($calcEngState == 2)
							{
								if($obdIntakeTemperature < 20)
								{
									$ve1 = (91.9803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
								else
								{
									$ve1 = (92.1803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
							}
							else
							{
								if($vehspd > 20)
								{
									$ve = (68.03 * $factor * $factor) + (-325.78 * $factor) + 475.32;
									$ve = round($ve,0);
								}
								else
								{
									$ve = (68.03 * $factor * $factor) + (-325.78 * $factor) + 375.32;
									$ve = round($ve,0);
								}
							}
						}
						else
						{
							if($calcEngState != 4)
							{
								if($eLoad >= 1 &&  $eLoad <= 10)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_1_10_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 11 &&  $eLoad <= 20)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_11_20_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 21 &&  $eLoad <= 30)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_21_30_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 31 &&  $eLoad <= 40)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_31_40_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 41 &&  $eLoad <= 50)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_41_50_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 51 &&  $eLoad <= 60)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_51_60_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 61 &&  $eLoad <= 80)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_61_80_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 81 &&  $eLoad <= 90)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_81_90_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 91 &&  $eLoad <= 94)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_91_94_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 95 && $eLoad <= 98)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_95_98_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 99 &&  $eLoad <= 100)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
								}
								else
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
								}
							}
							else
							{
								$ve = $Ovr_Run_VolEff_C;
							}
						}
					}
					//********************===================Volumetric Efficiency Calculations End===================******************//
					
				 	$CurHmdty = 50;
					$CurHumiper = $CurHmdty/100;
					
					$num = (7.5 * $obdIntakeTemperature);
					$denom = ($obdIntakeTemperature + 237.3);
					$divFact = $num/$denom;
					$fact = pow(10, $divFact);
					$pv = $CurHumiper * ((6.1078 * $fact)/10);
	            	
		 			//	Density Calculation
		            $density = $maf * (100 * 120) / ($rpm * $ve * $ed * 1000);
		 			
					$MAPfact = ($density * (8.314 * ($obdIntakeTemperature + 273))) - ($pv * 0.018016);
				 	$pd = $MAPfact / 0.028964;
				 	$map = $pd + $pv;
					
					//echo "CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | density = $density | MAPfact = $MAPfact | pd = $pd | map = $map |";
					$map = round($map,0);
					
					$PressDiff = $obdBarometricPressure - 91;
					$currMaxMAP = $MAP_Abs_PkLmt3old_C + $PressDiff;
					if(($Turbo_Avail_C == "Yes") && ($calcEngState == 3) && ($map > $currMaxMAP))
					{
						$map = $currMaxMAP;
					}
					elseif(($vType == 2) && ($calcEngState == 3) && ($map < $obdBarometricPressure))
					{
						if($obdCoolantTemperature < 55)
						{
							$map = $obdBarometricPressure;
						}
						elseif($obdCoolantTemperature < 72)
						{
							$map = $obdBarometricPressure + 1;
						}
						else
						{
							$map = $obdBarometricPressure + 2;
						}
					}
					elseif($calcEngState == 4)
					{
						$map = "";
					}
					elseif($rpm <= 500)
					{
						$map = $obdBarometricPressure;
					}
					elseif(($map < 0) || (($map > $obdBarometricPressure) && ($vType == 1) && ($Turbo_Avail_C == "No")))
					{
						$map = $obdBarometricPressure;
					}
					else
					{
						//Do nothing;
					}
					$currRowData['F3'] = $map;
				}
				else
				{
					//do nothing;	
				}
			}
			else
			{
				//do nothing;	
			}
		}
		else
		{
			if($maf > 0 && $MAF_Valid_C == 1)
			{
				//*****************==================Volumetric Efficiency calculation start============================*****************//
				//Diesel Engine Volumetric Efficiency
				if($vType == 2)
				{
					if($MAP_Type_C == "Differential")
					{
					 	if($eLoad <= 55)
						{
							$fact11 = pow($eLoad, 4);
							$fact21 = pow($eLoad, 3);
							$fact31 = pow($eLoad, 2);
							$fact41 = $eLoad;
							
							$fact1 = (0.000004 * $fact11);
							$fact2 = (0.0021 * $fact21);
							$fact3 = (0.1497 * $fact31);
							$fact4 = (1.9792 * $fact41);
							
							$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
							//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
						}
						elseif($eLoad <= 85)
						{
							$ve = 98 + (0.08 * $eLoad);
						}
						elseif($eLoad <= 98)
						{
							$ve = 99 + (0.05 * $eLoad);
						}
						else
						{
							$ve = 99 + (0.04 * $eLoad);
						}
					}
					else
					{
						//This condition is added so that the calculation is proper even when IAT is not read/supported
						if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
						{
							$obdIntakeTemperature = $obdCoolantTemperature;
						}
						else
						{
							//Do nothing
						}
						$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
						$barofactor = (101.3 / $obdBarometricPressure);
		
						if($calcEngState == 2)
						{
							if($obdIntakeTemperature < 20)
							{
								$ve1 = (34.4803 * $factor) - 0.2799;
								$ve = $ve1 * $barofactor;
								$ve = round($ve,0);
							}
							else
							{
								$ve1 = (34.6803 * $factor) - 0.2799;
								$ve = $ve1 * $barofactor;
								$ve = round($ve,0);
							}
						}
						elseif($calcEngState == 4)
						{
							$power = pow(10, -11);
							$ve = (-3 * $power * $factor * $factor) + (11.35 * $factor) + 27.398;
							$ve = round($ve,0);
						}
						else
						{
							if($vehspd > 20)
							{
								// $power = pow(10, -11);
								$ve = (-0.55 * $factor * $factor) + (11.35 * $factor) + 44.398;
								$ve = round($ve,0);
							}
							else
							{
								// $power = pow(10, -11);
								$ve = (-0.65 * $factor * $factor) + (11.35 * $factor) + 38.398;
								$ve = round($ve,0);
							}
						}
					}
				}
				//Petrol Engine Volumetric Efficiency
				else
				{
					if($Turbo_Avail_C == "No")
					{
						if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
						{
							$obdIntakeTemperature = $obdCoolantTemperature;
						}
						else
						{
							//Do nothing
						}
						$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
						$barofactor = (101.3 / $obdBarometricPressure);
			
						if($calcEngState == 2)
						{
							if($obdIntakeTemperature < 20)
							{
								$ve1 = (91.9803 * $factor) - 0.2799;
								$ve = $ve1 * $barofactor;
								$ve = round($ve,0);
							}
							else
							{
								$ve1 = (92.1803 * $factor) - 0.2799;
								$ve = $ve1 * $barofactor;
								$ve = round($ve,0);
							}
						}
						else
						{
							if($vehspd > 20)
							{
								// $ve = (70.03 * $factor * $factor) + (-328.78 * $factor) + 475.32;
								$ve = (68.03 * $factor * $factor) + (-325.78 * $factor) + 475.32;
								$ve = round($ve,0);
							}
							else
							{
								// $ve = (70.03 * $factor * $factor) + (-328.78 * $factor) + 375.32;
								$ve = (68.03 * $factor * $factor) + (-325.78 * $factor) + 375.32;
								$ve = round($ve,0);
							}
						}
					}
					else
					{
						if($calcEngState != 4)
						{
							if($eLoad >= 1 &&  $eLoad <= 10)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_1_10_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_1_10_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_1_10_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_1_10_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_1_10_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_1_10_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 11 &&  $eLoad <= 20)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_11_20_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_11_20_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_11_20_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_11_20_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_11_20_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_11_20_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 21 &&  $eLoad <= 30)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_21_30_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_21_30_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_21_30_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_21_30_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_21_30_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_21_30_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 31 &&  $eLoad <= 40)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_31_40_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_31_40_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_31_40_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_31_40_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_31_40_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_31_40_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 41 &&  $eLoad <= 50)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_41_50_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_41_50_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_41_50_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_41_50_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_41_50_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_41_50_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 51 &&  $eLoad <= 60)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_51_60_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_51_60_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_51_60_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_51_60_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_51_60_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_51_60_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 61 &&  $eLoad <= 80)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_61_80_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_61_80_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_61_80_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_61_80_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_61_80_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_61_80_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 81 &&  $eLoad <= 90)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_81_90_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_81_90_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_81_90_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_81_90_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_81_90_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_81_90_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 91 &&  $eLoad <= 94)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_91_94_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_91_94_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_91_94_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_91_94_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_91_94_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_91_94_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 95 && $eLoad <= 98)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_95_98_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_95_98_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_95_98_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_95_98_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_95_98_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_95_98_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 99 &&  $eLoad <= 100)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_99_100_RPMLimit5_C;
								}
							}
							else
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_99_100_RPMLimit5_C;
								}
							}
						}
						else
						{
							$ve = $Ovr_Run_VolEff_C;
						}
					}	
				}
				//********************===================Volumetric Efficiency Calculations End===================******************//
				
			 	$CurHmdty = 50;
				$CurHumiper = $CurHmdty/100;
				
				$num = (7.5 * $obdIntakeTemperature);
				$denom = ($obdIntakeTemperature + 237.3);
				$divFact = $num/$denom;
				$fact = pow(10, $divFact);
				$pv = $CurHumiper * ((6.1078 * $fact)/10);
            	
	 			//	Density Calculation
	            $density = $maf * (100 * 120)/ ($rpm * $ve * $ed * 1000);
	 			
				$MAPfact = ($density * (8.314 * ($obdIntakeTemperature + 273))) - ($pv * 0.018016);
			 	$pd = $MAPfact / 0.028964;
			 	$map = $pd + $pv;
				
				//echo "CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | density = $density | MAPfact = $MAPfact | pd = $pd | map = $map |";
				$map = round($map,0);
				
				$PressDiff = $obdBarometricPressure - 91;
				$currMaxMAP = $MAP_Abs_PkLmt3old_C + $PressDiff;
				if(($Turbo_Avail_C == "Yes") && ($calcEngState == 3) && ($map > $currMaxMAP))
				{
					$map = $currMaxMAP;
				}
				elseif(($vType == 2) && ($calcEngState == 3) && ($map < $obdBarometricPressure))
				{
					if($obdCoolantTemperature < 55)
					{
						$map = $obdBarometricPressure;
					}
					elseif($obdCoolantTemperature < 72)
					{
						$map = $obdBarometricPressure + 1;
					}
					else
					{
						$map = $obdBarometricPressure + 2;
					}
				}
				elseif($calcEngState == 4)
				{
					$map = "";
				}
				elseif($rpm <= 500)
				{
					$map = $obdBarometricPressure;
				}
				elseif(($map < 0) || (($map > $obdBarometricPressure) && ($vType == 1) && ($Turbo_Avail_C == "No")))
				{
					$map = $obdBarometricPressure;
				}
				else
				{
					//Do nothing;
				}
				$currRowData['F3'] = $map;
			}
			else
			{
				//do nothing;	
			}
		}	
		//********************----------------New MAF to MAP Formula End--------------------*********************//
		
		//********************----------------New MAP to MAF Formula Start--------------------*******************//
		if($Veh_Type_C != 2)	//Veh_Type_C = 2 :- is for trucks. MAF is not supported but fuel flow is directly got. So don't calculate.
		{
			if($MAF_Valid_C == 1)
			{
				if($maf <= 0 || $maf == "")
				{
					if($obdManifoldPressure > 0 && $MAP_Valid_C == 1)
					{
						//*****************==================Volumetric Efficiency calculation start============================*****************//
						//Diesel Engine Volumetric Efficiency
						if($vType == 2)
						{
							if($MAP_Type_C == "Differential")
							{
							 	if($eLoad <= 55)
								{
									$fact11 = pow($eLoad, 4);
									$fact21 = pow($eLoad, 3);
									$fact31 = pow($eLoad, 2);
									$fact41 = $eLoad;
									
									$fact1 = (0.000004 * $fact11);
									$fact2 = (0.0021 * $fact21);
									$fact3 = (0.1497 * $fact31);
									$fact4 = (1.9792 * $fact41);
									
									$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
									//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
								}
								elseif($eLoad <= 85)
								{
									$ve = 98 + (0.08 * $eLoad);
								}
								elseif($eLoad <= 98)
								{
									$ve = 99 + (0.05 * $eLoad);
								}
								else
								{
									$ve = 99 + (0.04 * $eLoad);
								}
							}
							else
							{
								if($eLoad <= 35)
								{
									$ve = 92;
									if($obdIntakeTemperature > 50)
									{
										$ve = $ve + 5;
									}
									else
									{
										//do nothing;
									}
								}
								elseif($eLoad <= 55)
								{
									$fact11 = pow($eLoad, 4);
									$fact21 = pow($eLoad, 3);
									$fact31 = pow($eLoad, 2);
									$fact41 = $eLoad;
									
									$fact1 = (0.0000002 * $fact11);
									$fact2 = (0.00007 * $fact21);
									$fact3 = (0.0082 * $fact31);
									$fact4 = (0.3661 * $fact41);
									
									$ve = -$fact1 + $fact2 - $fact3 + $fact4 + 86.679;
									//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
								}
								elseif($eLoad <= 85)
								{
									$ve = 92 + (0.06 * $eLoad);
								}
								elseif($eLoad <= 98)
								{
									$ve = 94 + (0.05 * $eLoad);
								}
								else
								{
									$ve = 94 + (0.025 * $eLoad);
								}
								
								//Logic added for cars with no intercooler. VE reduces if intercooler not present.	Added on 25/05/2017.
								if($intercoolerPresent == "0")
								{
									if($eLoad <= 25)
									{
										if($vehspd < 10)
										{
											$ve = $ve - 36.5;
										}
										else
										{
											$ve = $ve;
										}
									}
									elseif($eLoad <= 35)
									{
										$ve = $ve - 28;
									}
									elseif($eLoad <= 55)
									{
										$ve = $ve - 20;
									}
									elseif($eLoad <= 85)
									{
										$ve = $ve - 15;
									}
									elseif($eLoad <= 98)
									{
										$ve = $ve - 9;
									}
									else
									{
										$ve = $ve - 2;
									}
								}
								else
								{
									//do nothing
								}
							}
						}

						//Petrol Engine Volumetric Efficiency
						else
						{
							if($Veh_Type_C == 0)
							{
								if($calcEngState != 4)
								{
									if($eLoad >= 1 &&  $eLoad <= 10)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_1_10_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 11 &&  $eLoad <= 20)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_11_20_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 21 &&  $eLoad <= 30)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_21_30_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 31 &&  $eLoad <= 40)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_31_40_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 41 &&  $eLoad <= 50)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_41_50_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 51 &&  $eLoad <= 60)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_51_60_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 61 &&  $eLoad <= 80)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_61_80_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 81 &&  $eLoad <= 90)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_81_90_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 91 &&  $eLoad <= 94)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_91_94_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 95 && $eLoad <= 98)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_95_98_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 99 &&  $eLoad <= 100)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
									}
									else
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
									}
								}
								else
								{
									$ve = $Ovr_Run_VolEff_C;
								}
							}
							elseif($Veh_Type_C == 1)
							{
								if($calcEngState != 4)
								{
									if($eLoad >= 1 &&  $eLoad <= 10)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_1_10_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 11 &&  $eLoad <= 20)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_11_20_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 21 &&  $eLoad <= 30)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_21_30_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 31 &&  $eLoad <= 40)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_31_40_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 41 &&  $eLoad <= 50)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_41_50_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 51 &&  $eLoad <= 60)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_51_60_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 61 &&  $eLoad <= 80)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_61_80_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 81 &&  $eLoad <= 90)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_81_90_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 91 &&  $eLoad <= 94)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_91_94_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 95 && $eLoad <= 98)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_95_98_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 99 &&  $eLoad <= 100)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
									}
									else
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
									}
								}
								else
								{
									$ve = $Ovr_Run_VolEff_C;
								}
							}
						}
						//********************===================Volumetric Efficiency Calculations End===================******************//
								
						//MAP to MAF old formula
						//$maf = ($inTakeTemp > 0 && $r > 0) ? ($rpm * $map * $ve * $ed * $mm) / ($inTakeTemp * 120 * 100 * $r) : 0;
						
						$CurHmdty = 50;
						$CurHumiper = $CurHmdty / 100;
						if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
						{
							$obdIntakeTemperature = $obdCoolantTemperature;
						}
						else
						{
							//Do nothing
						}
						
						$num = (7.5 * $obdIntakeTemperature);
						$denom = ($obdIntakeTemperature + 237.3);
						$divFact = $num / $denom;
						$fact = pow(10, $divFact);
						$pv = $CurHumiper * ((6.1078 * $fact)/10);
		            	$pd = $map - $pv;
						
						//	Formula for density
			            $density = (($pd * 0.028964) + ($pv * 0.018016)) / (8.314 * ($obdIntakeTemperature + 273));
						
						//	MAF Calculation
			            $maf = ($rpm * $ve * $ed * $density * 1000) / (100 * 120);
						$maf = round($maf,3);
						//echo "ve = $ve | fact1 = $fact1 | CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | pd = $pd | density = $density | maf = $maf | \n";
						
						$currRowData['F6'] = $maf;
					}
					else
					{
						//do nothing;
					}
				}
				else
				{
					//do nothing;
				}
			}
			else
			{
				if($obdManifoldPressure > 0 && $MAP_Valid_C == 1)
				{
					//*****************==================Volumetric Efficiency calculation start============================*****************//
					//Diesel Engine Volumetric Efficiency
					if($vType == 2)
					{
						if($MAP_Type_C == "Differential")
						{
						 	if($eLoad <= 55)
							{
								$fact11 = pow($eLoad, 4);
								$fact21 = pow($eLoad, 3);
								$fact31 = pow($eLoad, 2);
								$fact41 = $eLoad;
								
								$fact1 = (0.000004 * $fact11);
								$fact2 = (0.0021 * $fact21);
								$fact3 = (0.1497 * $fact31);
								$fact4 = (1.9792 * $fact41);
								
								$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
								//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
							}
							elseif($eLoad <= 85)
							{
								$ve = 98 + (0.08 * $eLoad);
							}
							elseif($eLoad <= 98)
							{
								$ve = 99 + (0.05 * $eLoad);
							}
							else
							{
								$ve = 99 + (0.04 * $eLoad);
							}
						}
						else
						{
							if($eLoad <= 35)
							{
								$ve = 92;
								if($obdIntakeTemperature > 50)
								{
									$ve = $ve + 5;
								}
								else
								{
									//do nothing;
								}
							}
							elseif($eLoad <= 55)
							{
								$fact11 = pow($eLoad, 4);
								$fact21 = pow($eLoad, 3);
								$fact31 = pow($eLoad, 2);
								$fact41 = $eLoad;
								
								$fact1 = (0.0000002 * $fact11);
								$fact2 = (0.00007 * $fact21);
								$fact3 = (0.0082 * $fact31);
								$fact4 = (0.3661 * $fact41);
								
								$ve = -$fact1 + $fact2 - $fact3 + $fact4 + 86.679;
								//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
							}
							elseif($eLoad <= 85)
							{
								$ve = 92 + (0.06 * $eLoad);
							}
							elseif($eLoad <= 98)
							{
								$ve = 94 + (0.05 * $eLoad);
							}
							else
							{
								$ve = 94 + (0.025 * $eLoad);
							}
							
							//Logic added for cars with no intercooler. VE reduces if intercooler not present.	Added on 25/05/2017.
							if($intercoolerPresent == "0")
							{
								if($eLoad <= 25)
								{
									if($vehspd < 10)
									{
										$ve = $ve - 36.5;
									}
									else
									{
										$ve = $ve;
									}
								}
								elseif($eLoad <= 35)
								{
									$ve = $ve - 28;
								}
								elseif($eLoad <= 55)
								{
									$ve = $ve - 20;
								}
								elseif($eLoad <= 85)
								{
									$ve = $ve - 15;
								}
								elseif($eLoad <= 98)
								{
									$ve = $ve - 9;
								}
								else
								{
									$ve = $ve - 2;
								}
							}
							else
							{
								//do nothing
							}
						}
					}
			
					//Petrol Engine Volumetric Efficiency
					else
					{
						if($Veh_Type_C == 0)
						{
							if($calcEngState != 4)
							{
								if($eLoad >= 1 &&  $eLoad <= 10)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_1_10_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 11 &&  $eLoad <= 20)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_11_20_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 21 &&  $eLoad <= 30)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_21_30_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 31 &&  $eLoad <= 40)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_31_40_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 41 &&  $eLoad <= 50)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_41_50_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 51 &&  $eLoad <= 60)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_51_60_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 61 &&  $eLoad <= 80)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_61_80_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 81 &&  $eLoad <= 90)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_81_90_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 91 &&  $eLoad <= 94)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_91_94_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 95 && $eLoad <= 98)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_95_98_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 99 &&  $eLoad <= 100)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
								}
								else
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
								}
							}
							else
							{
								$ve = $Ovr_Run_VolEff_C;
							}
						}
						elseif($Veh_Type_C == 1)
						{
							if($calcEngState != 4)
							{
								if($eLoad >= 1 &&  $eLoad <= 10)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_1_10_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 11 &&  $eLoad <= 20)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_11_20_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 21 &&  $eLoad <= 30)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_21_30_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 31 &&  $eLoad <= 40)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_31_40_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 41 &&  $eLoad <= 50)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_41_50_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 51 &&  $eLoad <= 60)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_51_60_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 61 &&  $eLoad <= 80)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_61_80_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 81 &&  $eLoad <= 90)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_81_90_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 91 &&  $eLoad <= 94)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_91_94_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 95 && $eLoad <= 98)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_95_98_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 99 &&  $eLoad <= 100)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
								}
								else
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
								}
							}
							else
							{
								$ve = $Ovr_Run_VolEff_C;
							}
						}
					}
					//********************===================Volumetric Efficiency Calculations End===================******************//
					
					//echo "obdIntakeTemperature = $obdIntakeTemperature | map = $map | rpm = $rpm | ed = $ed\n";
					//MAP to MAF old formula	
					//$maf = ($inTakeTemp > 0 && $r > 0) ? ($rpm * $map * $ve * $ed * $mm) / ($inTakeTemp * 120 * 100 * $r) : 0;
					$CurHmdty = 50;
					$CurHumiper = $CurHmdty/100;
					if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
					{
						$obdIntakeTemperature = $obdCoolantTemperature;
					}
					else
					{
						//Do nothing
					}
					
					$num = (7.5 * $obdIntakeTemperature);
					$denom = ($obdIntakeTemperature + 237.3);
					$divFact = $num/$denom;
					$fact = pow(10, $divFact);
					$pv = $CurHumiper * ((6.1078 * $fact)/10);
	            	$pd = $map - $pv;
					
					//	Formula for density
		            $density = (($pd * 0.028964) + ($pv * 0.018016)) / (8.314 * ($obdIntakeTemperature + 273));
					
					//	MAF Calculation
		            $maf = ($rpm * $ve * $ed * $density * 1000) / (100 * 120);
					$maf = round($maf,3);
					//echo "ve = $ve | fact1 = $fact1 | CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | pd = $pd | density = $density | maf = $maf | \n";
					
					$currRowData['F6'] = $maf;
					//echo "\n ve = $ve | maf = $maf \n";
				}
				else
				{
					//do nothing;
				}
			}	
		}
		else
		{
			//MAF is not supported but fuel flow is directly got. So don't calculate.
		}
		//********************----------------New MAP to MAF Formula End--------------------********************//
		
		/************=========================Efficiency Calculation Start================================*********************/
		$obdManifoldPressure = $map; //so that $obdManifoldPressure has the new calculated value of MAP.
		$n = ($baseN / 100);
		//Changing Efficiencies for Diesel Engines
		if($vType == 2)
		{
			if($obdEngineLoad <= 15)
			{
				if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
				{
					$n = $n - 0.07;
				}
				elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
				{
					$n = $n - 0.06; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
				}
				elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
				{
					$n = $n - 0.03; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
				}
				elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
				{
					$n = $n - 0.04;
				}
				elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
				{
					$n = $n - 0.06;
				}
				else
				{
					$n = $n - 0.07;
				}
			}
			elseif($obdEngineLoad <= 35)
			{
				if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
				{
					$n = $n - 0.06;
				}
				elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
				{
					$n = $n - 0.05; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
				}
				elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
				{
					$n = $n - 0.02; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
				}
				elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
				{
					$n = $n - 0.03;
				}
				elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
				{
					$n = $n - 0.05;
				}
				else
				{
					$n = $n - 0.06;
				}
			}
			elseif($obdEngineLoad <= 55)
			{
				if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
				{
					$n = $n - 0.05;
				}
				elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
				{
					$n = $n - 0.04; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
				}
				elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
				{
					$n = $n - 0.01; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
				}
				elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
				{
					$n = $n - 0.02;
				}
				elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
				{
					$n = $n - 0.04;
				}
				else
				{
					$n = $n - 0.05;
				}
			}
			elseif($obdEngineLoad <= 75)
			{
				if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
				{
					$n = $n - 0.04;
				}
				elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
				{
					$n = $n - 0.03; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
				}
				elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
				{
					$n = $n; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
				}
				elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
				{
					$n = $n - 0.02;
				}
				elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
				{
					$n = $n - 0.03;
				}
				else
				{
					$n = $n - 0.05;
				}
			}
			else
			{
				if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
				{
					$n = $n - 0.03;
				}
				elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
				{
					$n = $n - 0.02; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
				}
				elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
				{
					$n = $n; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
				}
				elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
				{
					$n = $n - 0.02;
				}
				elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
				{
					$n = $n - 0.03;
				}
				else
				{
					$n = $n - 0.05;
				}
				//do nothing
			}

			// $n is calclated. Now find efficiency changes due to xurrent MAP value.
			if ($obdManifoldPressure <= $obdBarometricPressure)
			{
				$n = $n;
			} 
			else
			{
				if($MAP_Type_C == "Absolute")
				{
					if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 140)
					{
						$n = $n + 0.01; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
					}
					else 
					{				
						if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
						{
							if(140 < $obdManifoldPressure && $obdManifoldPressure <= 175)
							{
								$n = $n + 0.03;
							}
							elseif(175 < $obdManifoldPressure && $obdManifoldPressure <= 200)
							{
								$n = $n + 0.04;
							}
							elseif(200 < $obdManifoldPressure && $obdManifoldPressure <= 210)
							{
								$n = $n + 0.05;
							}
							elseif(210 < $obdManifoldPressure && $obdManifoldPressure <= 225)
							{
								$n = $n + 0.06;
							}
							elseif(225 < $obdManifoldPressure && $obdManifoldPressure <= 250)
							{
								$n = $n + 0.07;
							}
							elseif($obdManifoldPressure > 250)
							{
								$n = $n + 0.08;
							}
						}
						else 
						{
							if(140 < $obdManifoldPressure && $obdManifoldPressure <= 175)
							{
								$n = $n + 0.02;
							}
							elseif(175 < $obdManifoldPressure && $obdManifoldPressure <= 200)
							{
								$n = $n + 0.03;
							}
							elseif(200 < $obdManifoldPressure && $obdManifoldPressure <= 210)
							{
								$n = $n + 0.04;
							}
							elseif(210 < $obdManifoldPressure && $obdManifoldPressure <= 225)
							{
								$n = $n + 0.05;
							}
							elseif(225 < $obdManifoldPressure && $obdManifoldPressure <= 250)
							{
								$n = $n + 0.06;
							}
							elseif($obdManifoldPressure > 250)
							{
								$n = $n + 0.07;
							}
						}
					}
				}
				else
				{
					if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 100)
					{
						$n = $n + 0.01; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
					}
					else
					{
						if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
						{
							if(100 < $obdManifoldPressure && $obdManifoldPressure <= 110)
							{
								$n = $n + 0.03;
							}
							elseif(110 < $obdManifoldPressure && $obdManifoldPressure <= 120)
							{
								$n = $n + 0.04;
							}
							elseif(120 < $obdManifoldPressure && $obdManifoldPressure <= 130)
							{
								$n = $n + 0.05;
							}
							elseif(130 < $obdManifoldPressure && $obdManifoldPressure <= 142)
							{
								$n = $n + 0.06;
							}
							elseif(142 < $obdManifoldPressure && $obdManifoldPressure <= 152)
							{
								$n = $n + 0.07;
							}
							elseif($obdManifoldPressure > 152)
							{
								$n = $n + 0.08;
							}
						}
						else 
						{
							if(100 < $obdManifoldPressure && $obdManifoldPressure <= 110)
							{
								$n = $n + 0.02;
							}
							elseif(110 < $obdManifoldPressure && $obdManifoldPressure <= 120)
							{
								$n = $n + 0.03;
							}
							elseif(120 < $obdManifoldPressure && $obdManifoldPressure <= 130)
							{
								$n = $n + 0.04;
							}
							elseif(130 < $obdManifoldPressure && $obdManifoldPressure <= 142)
							{
								$n = $n + 0.05;
							}
							elseif(142 < $obdManifoldPressure && $obdManifoldPressure <= 152)
							{
								$n = $n + 0.06;
							}
							elseif($obdManifoldPressure > 152)
							{
								$n = $n + 0.07;
							}
						}
					}
				}
			}
		}

		//Changing Efficiencies for Petrol Engines	
		elseif($vType == 1)
		{
			if($Turbo_Avail_C == "No")
			{
				if($Injection_Type_C == "Direct") //efficiency of direct injection non turbocharged
				{
					$n = $n + 0.01;
				}
				elseif($Injection_Type_C == "Indirect") //efficiency of indirect injection non turbocharged
				{
					$n = $n;
				}
				
				//For cars
				if($Veh_Type_C == 0)
				{
					//Efficiency change with ENGINE RPM CONDITION and ENGINE LOAD CONDITION
					if($obdEngineLoad < 20)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.06;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.05;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n - 0.03;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n - 0.01;
						}
						else
						{
							$n = $n - 0.02;
						}
					}
					elseif($obdEngineLoad < 45)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.05;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.04;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n;
						}
						else
						{
							$n = $n - 0.01;
						}
					}
					elseif($obdEngineLoad < 65)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.04;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.03;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n - 0.01;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n + 0.02;
						}
						else
						{
							$n = $n;
						}
					}
					elseif($obdEngineLoad < 85)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.03;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n + 0.03;
						}
						else
						{
							$n = $n + 0.01;
						}
					}
					else
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.01;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n + 0.01;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							if($VVT_Status_C == 0)
							{
								$n = $n + 0.04;
							}
							else
							{
								$n = $n + 0.06;
							}
						}
						else
						{
							$n = $n + 0.02;
						}
					}
				}
				elseif($Veh_Type_C == 1)		//----------------------for Bikes
				{
					if($obdEngineLoad < 20)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.06;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
						{
							$n = $n - 0.05;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
						{
							$n = $n + 0.01;
						}
						else
						{
							$n = $n;
						}
					}
					elseif($obdEngineLoad < 45)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.05;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
						{
							$n = $n - 0.04;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
						{
							$n = $n - 0.01;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
						{
							$n = $n + 0.03;
						}
						else
						{
							$n = $n + 0.01;
						}
					}
					elseif($obdEngineLoad < 65)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.03;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
						{
							$n = $n;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
						{
							$n = $n + 0.05;
						}
						else
						{
							$n = $n + 0.03;
						}
					}
					elseif($obdEngineLoad < 85)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.01;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
						{
							$n = $n;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
						{
							$n = $n + 0.02;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
						{
							$n = $n + 0.07;
						}
						else
						{
							$n = $n + 0.05;
						}
					}
					else
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 1400
						{
							$n = $n + 0.02;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 2000
						{
							$n = $n + 0.03;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 3000
						{
							$n = $n + 0.05;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 4500
						{
							$n = $n + 0.08;
						}
						else
						{
							$n = $n + 0.06;
						}
					}
				}
			}
			elseif($Turbo_Avail_C == "Yes")
			{
				if($Injection_Type_C == "Direct") //efficiency of direc injection turbocharged
				{
					$n = $n + 0.01;
				}
				elseif($Injection_Type_C == "Indirect") //efficiency of indirec injection turbocharged
				{
					$n = $n;
				}
				
				//Efficiency change with ENGINE RPM CONDITION and ENGINE LOAD CONDITION
				if($obdEngineLoad < 20)
				{
					if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
					{
						$n = $n - 0.06;
					}
					elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
					{
						$n = $n - 0.05;
					}
					elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
					{
						$n = $n - 0.03;
					}
					elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
					{
						$n = $n - 0.01;
					}
					else
					{
						$n = $n - 0.02;
					}
				}
				elseif($obdEngineLoad < 45)
				{
					if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
					{
						$n = $n - 0.05;
					}
					elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
					{
						$n = $n - 0.04;
					}
					elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
					{
						$n = $n - 0.02;
					}
					elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
					{
						$n = $n;
					}
					else
					{
						$n = $n - 0.01;
					}
				}
				elseif($obdEngineLoad < 65)
				{
					if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
					{
						$n = $n - 0.04;
					}
					elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
					{
						$n = $n - 0.03;
					}
					elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
					{
						$n = $n - 0.01;
					}
					elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
					{
						$n = $n + 0.02;
					}
					else
					{
						$n = $n;
					}
				}
				elseif($obdEngineLoad < 85)
				{
					if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
					{
						$n = $n - 0.03;
					}
					elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
					{
						$n = $n - 0.02;
					}
					elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
					{
						$n = $n;
					}
					elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
					{
						$n = $n + 0.03;
					}
					else
					{
						$n = $n + 0.01;
					}
				}
				else
				{
					if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 1400
					{
						$n = $n - 0.02;
					}
					elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 2000
					{
						$n = $n - 0.01;
					}
					elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 3000
					{
						$n = $n + 0.01;
					}
					elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 4500
					{
						$n = $n + 0.04;
					}
					else
					{
						$n = $n + 0.02;
					}
				}
				
				// Efficiency change with Boost / MAP
				if ($obdManifoldPressure <= $obdBarometricPressure)
				{
					$n = $n;
				} 
				else
				{
					if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 125)
					{
						$n = $n + 0.005; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
					}
					else 
					{				
						if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
						{
							if(125 < $obdManifoldPressure && $obdManifoldPressure <= 150)
							{
								$n = $n + 0.015;
							}
							elseif(150 < $obdManifoldPressure && $obdManifoldPressure <= 180)
							{
								$n = $n + 0.025;
							}
							elseif($obdManifoldPressure > 180)
							{
								$n = $n + 0.03;
							}
						}
						else 
						{
							if(125 < $obdManifoldPressure && $obdManifoldPressure <= 150)
							{
								$n = $n + 0.01;
							}
							elseif(150 < $obdManifoldPressure && $obdManifoldPressure <= 180)
							{
								$n = $n + 0.02;
							}
							elseif ($obdManifoldPressure > 180)
							{
								$n = $n + 0.025;
							}
						}
					}
				}
			}
		}
		//CHECKS FOR COOLANT TEMPERATURE CONDITION FOR BOTH PETROL AND DIESEL
		if($obdCoolantTemperature < 65)
		{
			$n = $n - 0.01;
		}
		else 
		{
			$n = $n;
		}
		/************=========================Efficiency Calculation End================================*********************/
	
		$qH = 45;
		$pFuel = 0.85;
		if($vType == 1)
		{
			$qH = 43.5;
			$pFuel = 0.737;
		}
		//===================================PETROL ENGINE TORQUE CALCULATIONS START================================================//
		if($vType == 1)
		{
			//-------------- Equivalence Ratio Calculation ----------------------//
			//$ratio = 0;
			if($ratio <= 0 || $ratio == "")	
			{
				$ratio = 1;
				if($calcEngState != 4)
				{
					if($eLoad >= 1 &&  $eLoad <= 10)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_1_10_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_1_10_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_1_10_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_1_10_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_1_10_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_1_10_RPMLimit5_C;
						}
					}
					elseif($eLoad >= 11 &&  $eLoad <= 20)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_11_20_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_11_20_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_11_20_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_11_20_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_11_20_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_11_20_RPMLimit5_C;
						}
					}
					elseif($eLoad >= 21 &&  $eLoad <= 30)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_21_30_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_21_30_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_21_30_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_21_30_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_21_30_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_21_30_RPMLimit5_C;
						}
					}
					elseif($eLoad >= 31 &&  $eLoad <= 40)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_31_40_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_31_40_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_31_40_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_31_40_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_31_40_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_31_40_RPMLimit5_C;
						}
					}
					elseif($eLoad >= 41 &&  $eLoad <= 50)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_41_50_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_41_50_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_41_50_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_41_50_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_41_50_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_41_50_RPMLimit5_C;
						}
					}
					elseif($eLoad >= 51 &&  $eLoad <= 60)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_51_60_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_51_60_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_51_60_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_51_60_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_51_60_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_51_60_RPMLimit5_C;
						}
					}
					elseif($eLoad >= 61 &&  $eLoad <= 80)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_61_80_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_61_80_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_61_80_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_61_80_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_61_80_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_61_80_RPMLimit5_C;
						}
					}
					elseif($eLoad >= 81 &&  $eLoad <= 90)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_81_90_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_81_90_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_81_90_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_81_90_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_81_90_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_81_90_RPMLimit5_C;
						}
					}
					elseif($eLoad >= 91 &&  $eLoad <= 94)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_91_94_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_91_94_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_91_94_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_91_94_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_91_94_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_91_94_RPMLimit5_C;
						}
					}
					elseif($eLoad >= 95 && $eLoad <= 98)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_95_98_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_95_98_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_95_98_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_95_98_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_95_98_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_95_98_RPMLimit5_C;
						}
					}
					elseif($eLoad >= 99 &&  $eLoad <= 100)
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit5_C;
						}
					}
					else
					{
						if($obdRpm < $RPM_Limit1)
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit1_C;
						}
						elseif($obdRpm <= $RPM_Limit2)
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit2_C;
						}
						elseif($obdRpm <= $RPM_Limit3)
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit3_C;
						}
						elseif($obdRpm <= $RPM_Limit4)
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit4_C;
						}
						elseif($obdRpm <= $RPM_Limit5)
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit5_C;
						}
						else
						{
							$ratio = $Eqratio_Eload_99_100_RPMLimit5_C;
						}
					}
				}
				else
				{
					$ratio = $Ovr_Run_EqRatio_C;
				}
				$currRowData['F35'] = $ratio;
			}

			if($calcEngState != 0 || $calcEngState != 7)
			{
				//$sql = "update device set act='I am here $maf | $ratio' where device_id='$deviceID'";if(mysql_query($sql)){exec($cmd . " > /dev/null &");}
				//𝐹𝑢𝑒𝑙 𝐹𝑙𝑜𝑤 (𝑔/𝑠)=𝑀𝐴𝐹  / 𝐸𝑞𝑢𝑖𝑣𝑎𝑙𝑒𝑛𝑐𝑒 𝑅𝑎𝑡𝑖𝑜×14.7			
				$fuelFlowG = ($ratio == 0) ? 0 : ($maf / ($ratio * 14.7));
				$fuelFlowL = $fuelFlowG / ($pFuel * 5 / 18);
				
				if($rpm >= 450) //($eLoad > 0)
				{
					//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100
					$powerK = $fuelFlowG * $qH * $n;
					
					//Power (HP) = Power (kW) * 1.341
					$powerG = $powerK * 1.341;
					
					//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
					$tor = ($rpm > 0) ? ($powerK * 1000 * 60) / (2 * $pi * $rpm) : 0;
					
					// Temperature compensation algo for torque
					$fact = 1;
					$num = (273 + $obdIntakeTemperature);
					$denom = 298;
					$divFact = $num/$denom;
					$fact = pow($divFact,0.6);
					$torcomp = ($tor * $fact);
					$currRowData['Ftorcomp'] = round($torcomp,4);
				}
				else
				{
					$powerK = 0;
					$powerG = 0;
					$tor = 0;
				}
			}
			else
			{
				$fuelFlowG = 0;
				$fuelFlowL = 0;
				$powerK = 0;
				$powerG = 0;
				$tor = 0;
			}
		}
		//====================================PETROL ENGINE TORQUE CALCULATIONS END=================================================//
		//===================================DIESEL ENGINES TORQUE CALCULATIONS START===============================================//
		else
		{
			if($Veh_Type_C != 2)	//Veh_Type_C = 2 :- is for trucks. MAF is not supported but fuel flow is directly got. So don't calculate.
			{
				if($calcEngState != 0 || $calcEngState != 7)
				{
					if($calcEngState == 3)
					{
						//Fuel Flow (L/hr) = 0.0022*(MAF*Engine Load) + 1.0305	
						if($eLoad < 20)
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_0_20;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
						}
						elseif($eLoad < 40)
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_20_40;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
						}
						elseif($eLoad < 60)
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_40_60;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
						}
						elseif($eLoad < 80)
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_60_80;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
						}
						else
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_80_100;	//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
						}
					}
					elseif($calcEngState == 2)
					{
						//Fuel Flow (L/hr) = 0.0022*(MAF*Engine Load) + 1.0305
						if($eLoad < ($Eload_Idle_Warm_C + 5))
						{
							if (is_numeric($maf) && is_numeric($eLoad) && is_numeric($constfactidle_Eload1))
							{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfactidle_Eload1;		//changed on 29.08.2016		constant = 0.30
						}
						}
						else
						{ 
							if (is_numeric($maf) && is_numeric($eLoad) && is_numeric($constfactidle_Eload2))
							{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfactidle_Eload2;
						    }else{
						    	//do nothing
						    }
						}
					}
					elseif($calcEngState == 4)
					{
						$fuelFlowL = (0.0021 * $maf * $Ovr_Run_FuelFact_Eload_C) + $Ovr_Run_FuelFact_C;
					}
					else
					{
						$fuelFlowL = 0;
					}
				}
				else 
				{
					$fuelFlowL = 0;
				}
			}
			else
			{
				//echo "fuelFlow = $Veh_Type_C :- $fuelFlow \n";
				$fuelFlowL = $fuelFlow;
			}
			
			//Fuel Flow (g/s) = Fuel Flow (L/hr) * ρfuel * 5/18
			$fuelFlowG = $fuelFlowL * $pFuel * 5 / 18;
			
			//Power and Torque calculations.
			if($rpm >= 450) //($eLoad > 0)
			{
				//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100
				$powerK = $fuelFlowG * $qH * $n;
				
				//Power (HP) = Power (kW) * 1.341
				$powerG = $powerK * 1.341;
				
				//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
				$tor = ($rpm > 0) ? ($powerK * 1000 * 60) / (2 * $pi * $rpm) : 0;
				
				// Temperature compensation algo for torque
				$fact = 1;
				$num = (273 + $obdIntakeTemperature);
				$denom = 298;
				$divFact = $num/$denom;
				$fact = pow($divFact,0.6);
				$torcomp = ($tor * $fact);
				$currRowData['Ftorcomp'] = round($torcomp,4);
				//echo "constfact_Eload_20_40 = $constfact_Eload_20_40 | fuelFlowL = $fuelFlowL | n = $n | powerK = $powerK | powerG = $powerG | tor = $tor \n";
			}
			else 
			{
				//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100	
				$powerK = 0;
				
				//Power (HP) = Power (kW) * 1.341
				$powerG = 0;
				
				//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
				$tor = 0;
			}
		}		
		//====================================DIESEL ENGINES TORQUE CALCULATIONS END===============================================//	
		//Instantaneous Mileage = (km/L) = Vehicle Velocity (km/hr) / Fuel flow (L/hr)		
		$intmil = ($fuelFlowL > 0) ? ($currRowData['F5'] / $fuelFlowL) : 0;
		
		//Average Speed and average fuel flow calculations
		$fFlowSum += $fuelFlowL;
		$vehspdSum += $vehspd;
		if($countstats != 0)
		{
			$vehSpdAvg = ($vehspdSum / $countstats);
			$fFlowAvg = ($fFlowSum / $countstats);
		}
		//Mileage
		$mil = ($fFlowAvg > 0) ? ($vehSpdAvg / $fFlowAvg) : 0;
		$currRowData['F58'] = $mil;
		
		//Fuel Consumed (L) = Distance Travelled (km) / Mileage (km/L)
		$distT = $currRowData['Fdist'];//$this -> callSum($devId,'Fdist',$row['Fsprint']);
		if (is_numeric($totalDist) && is_numeric($distT)){
		   $totalDist += $distT;
	      }
		$currRowData['Fdist'] = $totalDist;
		$Ffcon = ($mil > 0) ? ($totalDist / $mil) : 0;
		//echo "$eSpeA | $fFlowAvg = $mil";
		
		//Average Speed (km/hr) = Sum of all speeds of the Vehicle for one drive (km/hr) / No. of instances
		$Favspeed = $vehSpdAvg;
		$currRowData['F53'] = $Favspeed;
		
		//echo "fuelFlowL = $fuelFlowL | fuelFlowG = $fuelFlowG | powerK = $powerK | powerG = $powerG | tor = $tor | inm =  $intmil mil = $mil | dist = $distT fc= $Ffcon | $eSpeA  | $fFlowAvg";
		$currRowData['Ffuelflow_l'] = round($fuelFlowL,4);
		$currRowData['Fpower_hp'] = round($powerG,4);
		$currRowData['Ftorque'] = round($tor,4);	
		$currRowData['Fintmil'] = round($intmil,4);	
		$currRowData['Fmil'] = round($mil,4);
		$currRowData['Ffcon'] = round($Ffcon,4);
		$currRowData['Favspeed'] = round($Favspeed,4);
		$currRowData['ad_dtin'] = date('Y-m-d');
		//echo "\n";
		//print_r($currRowData);
			
		//Calculate max speed
		if($vehspd > $maxVehSpd)
		{
			$maxVehSpd = $vehspd;
			$currRowData['F60'] = $maxVehSpd;
		}
		
		//===================== Combustion Pressure System Algo Value Start ==========================//
		//Calculate max Torque
		if($tor > $maxTorque)
		{
			$maxTorque = $tor;
			$rpmMaxTor = $rpm;
			$mapMaxTor = $map;
			$eloadMaxTor = $eLoad;
			$mafMaxTor = $maf;
			$iatMaxTor = $obdIntakeTemperature;
			$atmpress = $baro_value;
			$latMaxTor = $currRowData['lat'];
			$lonMaxTor = $currRowData['lon'];
			$altMaxTor = $currRowData['alt'];
		}
		//===================== Combustion Pressure System Algo Value End ==========================//
		
		//===================== Fuel Common Rail Pressure System Algo Value Start ==========================//
		//Calculate max rail pressure
		if(($calcEngState == 3) && ($eLoad > ($Eload_Max_C - $CRP_ELoadOffset_PkLmt_C)) && ($rpm > $CRP_RPM_PkLmt_C))
		{
			if($crp > $maxCRP)
			{
				$maxCRP = $crp;
			}
		}
		//Get Max rail pressure in incoming csv
		elseif(($calcEngState == 3) || ($calcEngState == 4))
		{
			if($crp > $maxCRPcsv)
			{
				$maxCRPcsv = $crp;
				$crpEload = $eLoad;
				$crpRpm = $rpm;
			}
		}
		
		//Take maximum value of rail pressure if test conditions are satisfied.
		if(($maxCRP != 0) && ($maxCRP < $maxCRPcsv))
		{
			//If macCRP is less than maxCRPcsv then we need to use the CRP table and get the relevant value at that RPM and Engine Load. 
			//Therefore put maxcrp to 0 so that maxCRPcsv code can run. 
			$maxCRP = 0;
		}
		//echo "maxCRP = $maxCRP | maxCRPcsv = $maxCRPcsv \n";
		
		//Calculate idle rail pressure
		if(($calcEngState == 2) && ($rpm > $rpmValA) && ($rpm < $rpmValB) && ($obdCoolantTemperature > $Cool_Lmt_C))
		{
			if($idleCRP == 0)
			{
				$idleCRP = $crp;
			}
			if($crp > $idleCRP)
			{
				$idleCRP = $crp;
			}
		}
		elseif(($calcEngState == 2) && ($rpm > $rpmValA1) && ($rpm < $rpmValB1) && ($obdCoolantTemperature > $Cool_Lmt_C))
		{
			if($idleCRP == 0)
			{
				$idleCRP = $crp;
			}
			if($crp > $idleCRP)
			{
				$idleCRP = $crp;
			}
		}
		elseif(($calcEngState == 2) && ($rpm > $rpmValA2) && ($rpm < $rpmValB2) && ($obdCoolantTemperature > $Cool_Lmt_C))
		{
			if($idleCRP == 0)
			{
				$idleCRP = $crp;
			}
			if($crp > $idleCRP)
			{
				$idleCRP = $crp;
			}
		}
		//===================== Fuel Common Rail Pressure System Algo Value End ==========================//
		
		//===================== Turbo/Air System Algo Value Start ==========================//
		//Calculate max MAP
		if($Turbo_Avail_C == "Yes")
		{
			if(($calcEngState == 3) && ($eLoad > ($Eload_Max_C - $MAP_EloadOffset_PkLmt_C)) && ($rpm > $MAP_RPM_PkLmt_C))
			{
				if($maxMAPcon == 0)
				{
					$maxMAPcon = $obdManifoldPressure;
					$baroMaxMAP = $obdBarometricPressure;
				}
				if($obdManifoldPressure > $maxMAP)
				{
					$maxMAPcon = $obdManifoldPressure;
					$baroMaxMAP = $obdBarometricPressure;
				}
			}
			
			//Get Max MAP in incoming csv
			if(($calcEngState == 3) || ($calcEngState == 4))
			{
				if($obdManifoldPressure > $maxMAPcsv)
				{
					$maxMAPcsv = $obdManifoldPressure;
					$maxMAPEload = $eLoad;
				}
			}
		}
		else
		{
			if(($rpm > $MAPPeakrpmValA) && ($rpm < $MAPPeakrpmValB) && ($eLoad > ($Eload_Max_C - $MAP_Air_EloadOffset_PkLmt_C)) && ($obdCoolantTemperature > $Cool_Lmt_C))
			{
				//echo "rpm = $rpm | eLoad = $eLoad | obdCoolantTemperature = $obdCoolantTemperature \n";
				if($startNewSegment == 1)
				{
					$startNewSegment = 0;					//So that next point can be considered as P1
					if($obdManifoldPressure > $MAPPeak0)
					{
						//$MAPPeak0 = $obdManifoldPressure;
						$prevMAP = $obdManifoldPressure;
					}
					else
					{
						//Do nothing;
					}
				}
				else
				{
					if ($obdManifoldPressure > $MAPPeak1)
					{
						$MAPPeak1 = $obdManifoldPressure;
						$MAPPeak0 = $prevMAP;
					}
					else
					{
						//Do nothing
					}
				}
			}
			else
			{
				$startNewSegment = 1;
			}
		}
		//echo "Row No: $count | MAP = $obdManifoldPressure | MapPeak0 = $MAPPeak0 | MapPeak1 = $MAPPeak1 | startNewSegment = $startNewSegment\n";
		//Calculate idle manifold air pressure (MAP)
		if($Turbo_Avail_C == "Yes")
		{
			if($idleMAPFlag == 0)		//Idle MAP value not got. Search for idle MAP value
			{
				if(($calcEngState == 2) && ($rpm > $MAPrpmValA) && ($rpm < $MAPrpmValB) && ($obdCoolantTemperature > $Cool_Lmt_C))
				{
					//If idle MAP is 0 initialise with first MAP value
					if($idleMAP == 0)
					{
						$idleMAP = $obdManifoldPressure;
						$baroIdleMAP = $obdBarometricPressure;
					}
					//This is written to pick max MAP value at idle
					if($obdManifoldPressure > $idleMAP)
					{
						$idleMAP = $obdManifoldPressure;
						$baroIdleMAP = $obdBarometricPressure;
						if(($idleMAP >= $baroIdleMAP) && ($idleMAP <= $baroIdleMAP + 5))
						{
							$idleMAPFlag = 1;
						}
					}
				}
				elseif(($calcEngState == 2) && ($rpm > $MAPrpmValA1) && ($rpm < $MAPrpmValB1) && ($obdCoolantTemperature > $Cool_Lmt_C))
				{
					if($idleMAP == 0)
					{
						$idleMAP = $obdManifoldPressure;
						$baroIdleMAP = $obdBarometricPressure;
					}
					//This is written to pick max MAP value at idle
					if($obdManifoldPressure > $idleMAP)
					{
						$idleMAP = $obdManifoldPressure;
						$baroIdleMAP = $obdBarometricPressure;
						if(($idleMAP >= $baroIdleMAP) && ($idleMAP <= $baroIdleMAP + 5))
						{
							$idleMAPFlag = 1;
						}
					}
				}
				elseif(($calcEngState == 2) && ($rpm > $MAPrpmValA2) && ($rpm < $MAPrpmValB2) && ($obdCoolantTemperature > $Cool_Lmt_C))
				{
					if($idleMAP == 0)
					{
						$idleMAP = $obdManifoldPressure;
						$baroIdleMAP = $obdBarometricPressure;
					}
					//This is written to pick max MAP value at idle
					if($obdManifoldPressure > $idleMAP)
					{
						$idleMAP = $obdManifoldPressure;
						$baroIdleMAP = $obdBarometricPressure;
						if(($idleMAP >= $baroIdleMAP) && ($idleMAP <= $baroIdleMAP + 5))
						{
							$idleMAPFlag = 1;
						}
					}
				}
			}
			else		//Idle MAP value recieved. Do not search for it again.
			{
				//Do nothing
			}
		}
		else
		{
			//Do nothing. Idle MAP not required for naturally aspirated engines.
		}
		//echo "maxMAP = $maxMAP | baroMaxMAP = $baroMaxMAP | idleMAP = $idleMAP | baroIdleMAP = $baroIdleMAP \n";
		//echo "($eLoad > ($Eload_Max_C - $MAP_EloadOffset_PkLmt_C)) && ($rpm > $MAP_RPM_PkLmt_C)";
		
		//Calculate max MAF
		if($maf > $maxMAF)
		{
			$maxMAF = $maf;
		}
		//===================== Turbo/Air System Algo Value End ==========================//
		
		//===================== Battery System Algo Value Start ==========================//
		//Calculate/get maximum and minimum battery voltage at ignition on
		if(($calcEngState == 0) && ($obdCoolantTemperature > 70))
		{
			//Minimum battery voltage
			if(($batVolt < $ignOnMinBatVolt) || ($ignOnMinBatVolt == 0))
			{
				$ignOnMinBatVolt = $batVolt;
			}
			//Maximum battery voltage
			if($batVolt > $ignOnMaxBatVolt)
			{
				$ignOnMaxBatVolt = $batVolt;
			}
		}
		/*//Calculate/get min battery voltage
		if($batVolt < $minBatVolt)
		{
			$minBatVolt = $batVolt;
			$batcool = $obdCoolantTemperature;
		}*/
		//Calculate/get max battery voltage during idle/running/overrun
		if(($batVolt > $maxBatVolt) && (($calcEngState == 2) || ($calcEngState == 3) || ($calcEngState == 4)))
		{
			$maxBatVolt = $batVolt;
		}
		//echo "ignOnBatVolt = $ignOnBatVolt | minBatVolt = $minBatVolt | maxBatVolt = $maxBatVolt \n";
		//===================== Battery System Algo Value End ==========================//
		
		//===================== Coolant System Algo Value Start ==========================//
		//Calculate max Coolant Temperature
		if($calcEngState == 3)
		{
			if(($obdCoolantTemperature > $maxCool) && ($vehspd > 10))
			{
				$maxCool = $obdCoolantTemperature;
				$minCool = $maxCool;
				$countcool = $currtimestamp;				//Get timestamp of highest coolant value
				$countcoollow = $currtimestamp + 600000;	//Limit till next 10 mins
			}
			if(($currtimestamp > $countcool) && ($currtimestamp <= $countcoollow))
			{
				if($obdCoolantTemperature < $minCool)
				{
					$minCool = $obdCoolantTemperature;
				}
				// echo "2-- maxCool = $maxCool | minCool = $minCool | count = $currtimestamp | countcool = $countcool | countcoollow = $countcoollow \n";
			}
			//echo "maxCool = $maxCool | minCool = $minCool \n";
		}
		
		/*//If max coolant temperature is above coolant limit 4 then find the minimum temperature the coolant falls to after reaching peaks.
		if($maxCool > $Cool_Temp_Lmt4_C)
		{
			if($prevCool == 0)
			{
				$prevCool = $obdCoolantTemperature;
			}
			if($obdCoolantTemperature < $prevCool)
			{
				//If later found that due to thermostat kicking in, reduces the temp by 1 and disturbs the mincool scenerio,
				//we'll introduce 'no. of points' check in the code by putting counter
				//Initialise minCool with current coolant value
				if($minCool == 0)
				{
					$minCool = $obdCoolantTemperature;
				}
				//Get minimum coolant drop
				if($obdCoolantTemperature < $minCool)
				{
					$minCool = $obdCoolantTemperature;
				}
				else
				{
					//Do nothing
				}
			}
			else
			{
				//Do nothing
			}
			$prevCool = $obdCoolantTemperature;
		}
		else
		{
			$minCool = $maxCool;
		}*/
		//===================== Coolant System Algo Value End ==========================//
		
		//===================== RPM OSCILLATION ANALYSIS Algo Value Start ==========================//
		//Check if Engine State is IDLE and get IDLE RPM
		if($calcEngState == 2 )
		{
			/*calcMonBuf[0] -> Holds the Buffer Status
			 * 0 = Buffer is Empty
			 * 1 = Buffer Contains RPM values at Warm Idle
			 * 2 = Buffer Contains RPM values at ELOAD 1
			 * 3 = Buffer Contains RPM values at ELOAD 2  */
			/* calcMonBuf[1] -> Holds count on values in the buffer
			 * 0 = Counter not started and hence Buffer invalid
			 * 1 to 18 =  Number of RPM Values stored in Buffer */
			
			//Get Engine RPM at Warm IDLE
			if((($calcMonBuf[0] == 0) || ($calcMonBuf[0] == 1)) && ($calcMonBuf[1] < 10))
			{
				if(($obdEngineLoad >= ($Eload_Idle_Warm_C - $Osc_EloadOffset_Lmt_C)) &&
						($obdEngineLoad <= ($Eload_Idle_Warm_C + $Osc_EloadOffset_Lmt_C)) &&
						($obdCoolantTemperature > $Cool_Lmt_C))
				{
					$calcMonBuf[0] = 1 ; // Buffer Contains Warm Idle Values
					$rpmIndex =  $calcMonBuf[1] + 2; // +2 is to offset for index
					$calcMonBuf[$rpmIndex] = $obdRpm; //Update curr RPM Val in Buffer
					$calcMonBuf[1] = $calcMonBuf[1] + 1; // Increment the buffer counter
				}
				else
				{
					// RESET Status and Counter
					$calcMonBuf[0] = 0;
					$calcMonBuf[1] = 0;
					
				}
			}
			else
			{
				//Warm Idle Conditions Not MET
			}
	
			//Get Engine RPM at Warm IDLE under ELOAD1
			if((($calcMonBuf[0] == 0) || ($calcMonBuf[0] == 2)) && ($calcMonBuf[1] < 10))
			{
				//Get Engine RPM at Warm IDLE under ELOAD1
				if(($obdEngineLoad >= ($Eload_Idle_Ld1_C - $Osc_EloadOffset_Lmt_C)) &&
						($obdEngineLoad <= ($Eload_Idle_Ld1_C + $Osc_EloadOffset_Lmt_C)) &&
						($obdCoolantTemperature > $Cool_Lmt_C))
				{
					$calcMonBuf[0] = 2 ; // Buffer Contains Idle Values at ELoad 1
					$rpmIndex =  $calcMonBuf[1] + 2; // +2 is to offset for index
					$calcMonBuf[$rpmIndex] = $obdRpm; //Update curr RPM Val in Buffer
					$calcMonBuf[1] = $calcMonBuf[1] + 1; // Increment the buffer counter
				}
				else
				{
					// RESET Status and Counter
					$calcMonBuf[0] = 0;
					$calcMonBuf[1] = 0;
				}
			}
			else
			{
				//Warm Idle under Load 1 Conditions Not MET
			}
			
			//Get Engine RPM at Warm IDLE under ELOAD2
			if((($calcMonBuf[0] == 0) || ($calcMonBuf[0] == 3)) && ($calcMonBuf[1] < 10))
			{
				//Get Engine RPM at Warm IDLE under ELOAD2
				if(($obdEngineLoad >= ($Eload_Idle_Ld2_C - $Osc_EloadOffset_Lmt_C)) &&
						($obdEngineLoad <= ($Eload_Idle_Ld2_C + $Osc_EloadOffset_Lmt_C)) &&
						($obdCoolantTemperature > $Cool_Lmt_C))
				{
					$calcMonBuf[0] = 3 ; // Buffer Contains Idle Values at ELoad 2
					$rpmIndex =  $calcMonBuf[1] + 2; // +2 is to offset for index
					$calcMonBuf[$rpmIndex] = $obdRpm; //Update curr RPM Val in Buffer
					$calcMonBuf[1] = $calcMonBuf[1] + 1; // Increment the buffer counter
				}
				else
				{
					// RESET Status and Counter
					$calcMonBuf[0] = 0;
					$calcMonBuf[1] = 0;
				}
			}
			else
			{
				//Warm Idle under Load 1 Conditions Not MET
			}
			
			// Once Buffer Reaches a Value of 16 Run the Algorithm.
			if(($calcMonBuf[0] > 0) && ($calcMonBuf[1] >= 10))
			{
				//print_r($calcMonBuf);
				//TODO: Buffer is full so run algorithm and get results
				// Ignore first 4 values and Last 2 values
				// GET Minimum and Maximum RPM Value from  The buffer.
				$MinobdRpm = $calcMonBuf[3];
				$MaxobdRpm = $calcMonBuf[3];
				for($rpmIndex = 4;$rpmIndex < 9; $rpmIndex++)
				{
					if($calcMonBuf[($rpmIndex)] <= $MinobdRpm)
					{
						$MinobdRpm = $calcMonBuf[$rpmIndex];
					}
					if($calcMonBuf[($rpmIndex)] > $MaxobdRpm)
					{
						$MaxobdRpm = $calcMonBuf[$rpmIndex];
					}
				}
				//echo "\n MinobdRpm = $MinobdRpm | MaxobdRpm = $MaxobdRpm \n";
				if(($MinobdRpm != 0) || ($MaxobdRpm != 0))
				{
					$deltaRPMcurr = $MaxobdRpm - $MinobdRpm;
				}
				//Update RPM Oscillation score only if the current value is higher than the previous one
				if($deltaRPMcurr < $deltaRPM)
				{
					$deltaRPM = $deltaRPMcurr;
				}
			}
		}
		else
		{
			//Other Engine States Do nothing
			// RESET BUFFER Status and Counter
			$calcMonBuf[0] = 0;
			$calcMonBuf[1] = 0;
		}	
		    // addData('device_data',$currRowData,$conn);
			// unset($currRowData);	
	}
	/********************************************************************************************************************
	 * ************************************** END OF FOR LOOP TO PROCESS EACH ROW ***************************************
	 * *****************************************************************************************************************/
	// has to go 
	// print_r($currRowData);
    addData('device_data',$currRowData,$conn);
	unset($currRowData);
	

		
	$curCO2 = ($mil > 0) ? ($co2_Cnv_Fac_C / $mil) : 0;
	
	$dt = date('Y-m-d');
	// After all calculations update database
	$computedSatatistics = array();
	$computedSatatistics['device_id'] = $devId;
	$computedSatatistics['dt'] = $dt;
	$computedSatatistics['drive'] = $Fsprint;
	$computedSatatistics['dur'] = $totaldur;
    
    $sqlDev = "select total_dist from device where device_id ='$devId'";
	$dat = $conn->query($sqlDev);
    if ($dat->num_rows > 0) {

        while($row = $dat->fetch_assoc()) {
           $distance_dev = $row['total_dist'];
           $tot_distAdd = $distance_dev + round($totalDist,2);
           $sql = "update device set total_dist = '$tot_distAdd' where device_id = '$devId'";
           $conn->query($sql);
        }
    }
    
	$computedSatatistics['dist'] = round($totalDist,2);
	$computedSatatistics['max_tor'] = round($maxTorque,2);
	$computedSatatistics['max_sp'] = $maxVehSpd;
	$computedSatatistics['dur_ovr'] = $ovrruntime;
	$computedSatatistics['ful_c'] = round($Ffcon,2);
	$computedSatatistics['avg_ml'] = round($mil,4);
	$computedSatatistics['idt'] = $idletime;
	$computedSatatistics['avg_speed'] = round($Favspeed,2);
	$computedSatatistics['dist_ovr'] = round($ovrrundist,2);
	$computedSatatistics['max_railp'] = $maxCRP;
	$computedSatatistics['max_map'] = $maxMAP;
	$computedSatatistics['max_maf'] = $maxMAF;
	$computedSatatistics['min_batv'] = $ignOnMinBatVolt;
	$computedSatatistics['ad_dt'] = date('Y-m-d H:i:s');
	$computedSatatistics['cur_co2'] = round($curCO2,2);
	//$computedSatatistics['tot_co2'] = $totCO2;		//Commented as no query is being made to get previous value
	$computedSatatistics['nodrive'] = $count;
	$computedSatatistics['dfrm'] = date('Y-m-d H:i:s',substr($firsttimestamp,0,10));//$currtimestamp - $firsttimestamp
	$computedSatatistics['dto'] = date('Y-m-d H:i:s',substr($currtimestamp,0,10));
	//$fileN = "$floder/". date('dmY',strtotime($fRow['time_v'])). ".csv";


	addData('device_drive',$computedSatatistics,$conn);
	CreateStatMonDataCSV($iLoopData,$devId,$Fsprint,$colHeader);
	// Save data to data drive table
	/*$saveData = new saveForm('device_drive',$computedSatatistics);
	

	
	// Copy all data to csv to view in GOT
	
	*/
	//============================================= Monitoring Results ===============================================//
	//Get previous monitoring results for the device
	$sqlvalidApo = "select * from device_monitor where device_id ='$devId' order by id desc limit 0,1";
	$dat = $conn->query($sqlvalidApo);
    if ($dat->num_rows > 0) {

        while($row = $dat->fetch_assoc()) {
           $data = $row;
        }

    }
	
	//Get Pressure,Temperature and Humidity from the lat, long and alt
	if($latMaxTor != "" || $lonMaxTor != "" || $altMaxTor != "")
	{
		if($latMaxTor != "null" || $lonMaxTor != "null" || $altMaxTor != "null")
		{
			$url = "http://api.openweathermap.org/data/2.5/weather?lat=$latMaxTor&lon=$lonMaxTor&units=metric&cnt=7&lang=en&appid=8f91e1e5881797629a26f45ab3298ac3";
			$json = file_get_contents($url);
			$data = json_decode($json,true);
			//Get current Temperature in Celsius
			$CurTemp = $data['main']['temp'];
			//echo $data['main']['temp'];
			//Get current Humidity in Percentage
			$CurHumi = $data['main']['humidity'];
			//echo $data['main']['humidity'];
			//Get current Absolute/corrected Pressure in hPa
			//$CurAbsPress = $data['main']['pressure'];
			//echo $data['main']['pressure'];
			//Get current Ground Level Pressure in hPa
			//$CurGrndPress = $data['main']['grnd_level'];
			//echo "CurGrndPress = $CurGrndPress \n";
			
			$CurrBaroPress = $obdBarometricPressure;
		}
		else
		{
			$CurTemp = 20;
			$CurHumi = 91;
			if($atmpress != "")
			{
				$CurrBaroPress = $atmpress;
			}
			else
			{
				$CurrBaroPress = $CurrCityPress;
			}
		}
	}
	else
	{
		$CurTemp = 20;
		$CurHumi = 91;
		if($atmpress != "")
		{
			$CurrBaroPress = $atmpress;
		}
		else
		{
			$CurrBaroPress = $CurrCityPress;
		}
	}
	
	//Get drivability
	$Drivability = "Intermediate";
	if($CurTemp != "" && $CurHumi != "")
	{
		if($CurTemp < 20)
		{
			if($CurHumi > 95)
			{
				$Drivability = "Bad";
			}
			elseif($CurHumi >= 90)
			{
				$Drivability = "Intermediate";
			}
			else
			{
				$Drivability = "Good";
			}
		}
		elseif($CurTemp <= 22.8)
		{
			if($CurHumi > 95)
			{
				$Drivability = "Bad";
			}
			elseif($CurHumi >= 90)
			{
				$Drivability = "Intermediate";
			}
			else
			{
				$Drivability = "Good";
			}
		}
		elseif($CurTemp < 30)
		{
			if($CurHumi > 85)
			{
				$Drivability = "Bad";
			}
			elseif($CurHumi > 60)
			{
				$Drivability = "Intermediate";
			}
			else
			{
				$Drivability = "Good";
			}
		}
		else
		{
			if($CurHumi > 60)
			{
				$Drivability = "Bad";
			}
			elseif($CurHumi > 45)
			{
				$Drivability = "Intermediate";
			}
			else
			{
				$Drivability = "Good";
			}
		}
	}
	else
	{
		$Drivability = "Intermediate";
	}
	
	//===================== Battery Algo Start ========================================//
	//Initalise $batState to previous rating
	$batState = $data['bat_st'];
	if(($ignOnMaxBatVolt != 0) && ($ignOnMaxBatVolt != ""))
	{
		if($ignOnMaxBatVolt > 12.2)
		{
			$batState = 5;
		}
		elseif($ignOnMaxBatVolt > 11.9)
		{
			$batState = 4;
		}
		elseif($ignOnMaxBatVolt > 11.6)
		{
			$batState = 3;
		}
		elseif($ignOnMaxBatVolt > 10.9)
		{
			$batState = 2;
		}
		elseif($ignOnMaxBatVolt < 11)
		{
			$batState = 1;
		}	
	}
	
	//Code for alternator and regulator scoring
	if($maxBatVolt < 12.7)
	{
		$regState = 2;
		if($maxBatVolt < ($ignOnMinBatVolt + 0.4))
		{
			$altState = 1;
			//$batState = $batState + 10;			//This score is to get the alternator comment
		}
		else
		{
			$altState = 2;
			//$batState = ($batState - 1) + 30;	//This score is to get the battery comment and reduce the battery score by 1.
		}
	}
	elseif($batVolt > 15.4)
	{
		$altState = 2;
		$regState = 1;
		//$batState = $batState + 20;	//This score is to get the voltage regulator comment
	}
	else
	{
		$altState = 2;
		$regState = 2;
	}

	//Final Battery Health Score
	if($batState == 1 || $batState == 2)
	{
		if($altState == 1)
		{
			if($regState == 1)
			{
				//$batState = $batState + 10; 		//altState and regState both will never have a score 1 together.
				$batOverallState = 11;
			}
			elseif($regState == 2)
			{
				$batOverallState = 11;
			}
		}
		elseif($altState == 2)
		{
			if($regState == 1)
			{
				//$batState = $batState + 20;
				$batOverallState = 22;
			}
			elseif($regState == 2)
			{
				if($maxBatVolt < 12.7)
				{
					//$batState = $batState + 30;
					$batOverallState = 32;
				}
			}
		}
	}
	elseif($batState == 3 || $batState == 4 || $batState == 5)
	{
		if($altState == 1)
		{
			if($regState == 1)
			{
				//$batState = $batState + 10; 		//altState and regState both will never have a score 1 together.
				$batOverallState = 12;
			}
			elseif($regState == 2)
			{
				$batOverallState = 12;
			}
		}
		elseif($altState == 2)
		{
			if($regState == 1)
			{
				//$batState = $batState + 20;
				$batOverallState = 23;
			}
			elseif($regState == 2)
			{
				if($maxBatVolt < 12.7)
				{
					//$batState = $batState + 30;
					$batOverallState = $batState + 30;
				}
			}
		}
	}
	//===================== Battery Algo End =========================================//
	
	//===================== Coolant Algo Start =======================================//
	$Diff = $Base_OvrHt_Cool_Lmt_C - $Cool_Temp_Lmt4_C;
	$Cool_Temp_Lmt1fact = ($Diff * $Cool_Temp_Lmt1percent_C) / 100;
	$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt1fact;
	$Cool_Temp_Lmt2fact = ($Diff * $Cool_Temp_Lmt2percent_C) / 100;
	$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt2fact;
	$Cool_Temp_Lmt3fact = ($Diff * $Cool_Temp_Lmt3percent_C) / 100;
	$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt3fact;
	//Initalise $coolHelScore to previous rating
	$coolHelScore = $data['coolp'];
	
	if($CurTemp == "" || $CurTemp == 0)
	{
		$CurTemp = 25;
	}
	else
	{
		$CurTemp = $CurTemp;
	}
	
	if($CurTemp < 30)
	{
		$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C;
		$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C;
		$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C;
		$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C;
	}
	elseif($CurTemp <= 40)
	{
		$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C + 2;
		$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C + 2;
		$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C + 2;
		$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C + 2;
	}
	else
	{
		$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C + 4;
		$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C + 4;
		$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C + 4;
		$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C + 4;
	}
	
	if($minCool > $Cool_Temp_Lmt1_C)
	{
		$coolHelScore = 1;
	}
	elseif($minCool >= $Cool_Temp_Lmt2_C)
	{
		$coolHelScore = 2;
	}
	elseif($minCool >= $Cool_Temp_Lmt3_C)
	{
		$coolHelScore = 3;
	}
	elseif($minCool > $Cool_Temp_Lmt4_C)
	{
		$coolHelScore = 4;
		//Get Coolant System score with Maximum coolant temp. If max coolant temp is above limit 2 then reduce score by 1
		if($maxCool >= $Cool_Temp_Lmt2_C)
		{
			$coolHelScore = $coolHelScore - 1;
		}
		else
		{
			//Do nothing;
		}
	}
	else
	{
		$coolHelScore = 5;
		//Get Coolant System score with Maximum coolant temp. If max coolant temp is above limit 2 then reduce score by 1
		if($maxCool >= $Cool_Temp_Lmt2_C)
		{
			$coolHelScore = $coolHelScore - 1;
		}
		else
		{
			//Do nothing;
		}
	}
	//===================== Coolant Algo End =======================================//
	
	//===================== Combustion Pressure Algo Start =======================================//
	//Initalise $CombPresState to previous rating
	$CombPresState = $data['comp_pres'];
	$LoadFact1 = 0.95;	//this is a 5% factor for manufacturing variations.
	$LoadFact2 = 0.95;	//this is a 5% factor for accessory loads.
	
	/******************************************************************************************************* /
	 * The compensation for drivability is being removed as the temperature and pressure are already being *
	 * compensated through pressure factor and compensated torque. So we were over compensating with this *
	 * factor. therefore it is now removed. Also the change in atm conditions can be seen in engine load *
	 * values in petrol NA engines.
	/********************************************************************************************************/
	/*if($Drivability == "Good")
	{
		$LoadFact3 = 1;
	}
	elseif($Drivability == "Intermediate")
	{
		$LoadFact3 = 0.98;
	}
	elseif($Drivability == "Bad")
	{
		$LoadFact3 = 0.96;
	}*/
	$LoadFact3 = 1;
	//Pressure correction factor
	//turbocharged petrols do not require the pressure correction factor as seen in the TSI in bangalore and mumbai (max MAP doesn't change)
	if($vType == 1 && $Turbo_Avail_C == "Yes")
	{
		$PresFact = 1; //no correction required
	}
	else
	{
		$PresFact = $obdBarometricPressure / 100;
	}
	
	//Engine Load Factor
	if (is_numeric($eloadMaxTor) && is_numeric($Eload_Max_C)){
	$AvgEloadFact = ($eloadMaxTor / $Eload_Max_C); // Engine Load correction factor
	}
	//MAP Factor
	//$MAPFact = ($mapMaxTor / $MaxMAP_C);
	$MAPFact = 1;
	
	//RPM Factor
	$Torquemax3RPM = $rpmMaxTor;
	$RPMDiffPer = 0;
	if($vType == 1)
	{
		if(($Torquemax3RPM > $RPM_Limit3) && ($Torquemax3RPM < $RPM_Limit4))
		{
			//do nothing;
		}
		else
		{
			if($Torquemax3RPM > $RPM_Limit4)
			{
				$RPMDiff = $Torquemax3RPM - $RPM_Limit4;
				$RPMDiffPer = ($RPMDiff * 100) / $RPM_Limit4;
				$RPMDiffPer = round($RPMDiffPer,0);
			}
			else
			{
				//If the max torque is recieved before RPM limit 3.
				$RPMDiff = $RPM_Limit3 - $Torquemax3RPM;
				$RPMDiffPer = ($RPMDiff * 100) / $RPM_Limit3;
				$RPMDiffPer = round($RPMDiffPer,0);
			}
		}
	}
	else
	{
		if(($Torquemax3RPM > $RPM_Limit2) && ($Torquemax3RPM < $RPM_Limit3))
		{
			//do nothing;
		}
		else
		{
			if($Torquemax3RPM > $RPM_Limit3)
			{
				$RPMDiff = $Torquemax3RPM - $RPM_Limit3;
				$RPMDiffPer = ($RPMDiff * 100) / $RPM_Limit3;
				$RPMDiffPer = round($RPMDiffPer,0);
			}
			else
			{
				//If the max torque is recieved before RPM limit 2.
				//do nothing;
			}
		}
	}
	if($RPMDiffPer == 0)
	{
		$RPMFactor = 1;
	}
	elseif($RPMDiffPer <= 3)
	{
		$RPMFactor = 0.97;
	}
	elseif($RPMDiffPer <= 5)
	{
		$RPMFactor = 0.964;
	}
	elseif($RPMDiffPer <= 15)
	{
		$RPMFactor = 0.94;
	}
	elseif($RPMDiffPer <= 27)
	{
		$RPMFactor = 0.91;
	}
	elseif($RPMDiffPer <= 34)
	{
		$RPMFactor = 0.875;
	}
	elseif($RPMDiffPer <= 43.5)
	{
		$RPMFactor = 0.81;
	}
	elseif($RPMDiffPer <= 50)
	{
		$RPMFactor = 0.76;
	}
	elseif($RPMDiffPer <= 60)
	{
		$RPMFactor = 0.72;
	}
	elseif($RPMDiffPer <= 80)
	{
		$RPMFactor = 0.55;
	}
	else
	{
		$RPMFactor = 0.50;
	}
	if (is_numeric($LoadFact1) && is_numeric($LoadFact2) && is_numeric($LoadFact3) && is_numeric($PresFact) && is_numeric($AvgEloadFact) && is_numeric($MAPFact) && is_numeric($RPMFactor)){
	$TotalFact = $LoadFact1 * $LoadFact2 * $LoadFact3 * $PresFact * $AvgEloadFact * $MAPFact * $RPMFactor;

	
	$NewMaxTorque = $MaxTorque * $TotalFact;
	$TorquePer = ($NewMaxTorque != 0 ) ? ($maxTorque / $NewMaxTorque) * 100 : 0;
	//echo "$TotalFact = $LoadFact1 * $LoadFact2 * $LoadFact3 * $PresFact * $AvgEloadFact * $RPMFactor \n";
	//echo "$NewMaxTorque = $MaxTorque * $TotalFact | TorquePer : $TorquePer | MaxTorque : $maxTorque \n";
	}
	if($TorquePer < $Trq_Lmt1_C)
	{
		if($TorquePer == 0)
		{
			//do nothing;
		}
		else
		{
			$CombPresState = 1;
		}
	}
	elseif($TorquePer <= $Trq_Lmt2_C)
	{
		$CombPresState = 2;
	}
	elseif($TorquePer <= $Trq_Lmt3_C)
	{
		$CombPresState = 3;
	}
	elseif($TorquePer <= $Trq_Lmt4_C)
	{
		$CombPresState = 4;
	}
	elseif($TorquePer > $Trq_Lmt4_C)
	{
		$CombPresState = 5;
	}
	//===================== Combustion Pressure Algo End =======================================//
	
	//===================== Fuel Rail Pressure Algo Start =======================================//
	$CRP_MinLmt3_C = $Base_CRP_Lmt_C;
	$CRP_MinLmt1_C = $Base_CRP_Lmt_C - $CRP_MinLmtScore1_C;
	$CRP_MaxLmt3_C = $CRP_Type_C - $CRP_MaxLmtScore3_C;
	$CRP_MaxLmt1_C = $CRP_MaxLmt3_C - $CRP_MaxLmtScore1_C;
	//Initalise $IdleCRPSCORE and $PeakCRPSCORE to previous rating
	//$IdleCRPSCORE = $data['idl_crp'];
	//$PeakCRPSCORE = $data['peak_crp'];
	
	//Fuel Idle Score not judged
	/*if($idleCRP != 0)
	{
		if($idleCRP > $CRP_MinLmt3_C)
		{
			$IdleCRPSCORE = 3;
		}
		elseif($idleCRP < $CRP_MinLmt1_C)
		{
			$IdleCRPSCORE = 1;
		}
		else
		{
			$IdleCRPSCORE = 2;
		}
	}
	else
	{
		$IdleCRPSCORE = 0;
	}*/
	
	//Fuel Peak Score
	if(($maxCRP != 0) || ($maxCRP != ""))
	{
		//Idle Score is not judged. Given 3/3 directly
		$IdleCRPSCORE = 3;
		if($maxCRP > $CRP_MaxLmt3_C)
		{
			$PeakCRPSCORE = 3;
		}
		elseif($maxCRP < $CRP_MaxLmt1_C)
		{
			$PeakCRPSCORE = 1;
		}
		else
		{
			$PeakCRPSCORE = 2;
		}
	}
	else
	{
		if(($maxCRPcsv != 0) || ($maxCRPcsv != ""))
		{
			//Idle Score is not judged. Given 3/3 directly
			$IdleCRPSCORE = 3;
			
			$maxCRP = $maxCRPcsv / 100;
			//Get the CRP type for the vehicle to get the csv file name
			$crp = ($CRP_Type_C / 100);
			$crptype = $crp."bar";
			$folder = "../datacsv/". "RailP" . "_". $crptype .".csv";
			//Open the csv file for reading
			$handle = fopen("$folder", "r");
			
			//Get the row number of the Engine Load closest to the Eload at which max CRP is got ($crpEload)
			$i = floor($crpEload / 5) + 1;
			
			if($crpRpm >= 1000)
			{
				//Get the column numbers of the RPM between which the rpm at which max CRP is got ($crpRpm)
				$j1 = floor($crpRpm / 200) - 4;		//-4 is added as the table starts from 1000 and not from 0
				$j2 = $j1 + 1;
				$rpmlimit1 = ($j1 + 4) * 200;
				$rpmlimit2 = ($j2 + 4) * 200;
				// echo "\n i = $i | j1 = $j1 | j2 = $j2 | rpm = $rpm | rpm1 = $rpmlimit1 | rpm2 = $rpmlimit2 \n";
				if($handle)
				{
					while(($row = fgetcsv($handle, 4096, ",")) !== false)
				    {
				    	$csv[] = $row;
				    }
					$crpRpmLimit1 = $csv[$i][$j1];
					$crpRpmLimit2 = $csv[$i][$j2];
					$crp = $crpRpmLimit1 + ((($crpRpmLimit2 - $crpRpmLimit1)/($rpmlimit2 - $rpmlimit1)) * ($crpRpm - $rpmlimit1));
				}
			}
			//RPM of getting peak CRP is very low, lower than 1000rpm 
			else
			{
				//do nothing
			}
			
			//Rate the CRP system based on the CRP value got from the table
			if($maxCRP <= $crp - 60)
			{
				$PeakCRPSCORE = 1;
			}
			elseif($maxCRP <= $crp - 35)
			{
				$PeakCRPSCORE = 2;
			}
			elseif($maxCRP <= $crp - 20)
			{
				$IdleCRPSCORE = 2;
				$PeakCRPSCORE = 3;
			}
			else
			{
				$PeakCRPSCORE = 3;
			}
		}
	}
	//echo "Fuel Sys Hel : CRPIdle: $idleCRP | CRPPeak: $maxCRP | IDLECRPSCORE = $IdleCRPSCORE | PeakCRPSCORE = $PeakCRPSCORE \n";
	//===================== Fuel Rail Pressure Algo End =================================//
	
	//===================== Turbocharged System Algo Start ==============================//
	if($Turbo_Avail_C == "Yes")
	{
		if(($baroIdleMAP == 0) || ($baroIdleMAP == ""))
		{
			$BaroPress = $obdBarometricPressure;
		}
		else
		{
			$BaroPress = $baroIdleMAP;
		}
		
		$PressDiff = $baroMaxMAP - $CurrCityPress;
		if($PressDiff >= 2)
		{
			$PressDiff = $PressDiff - 2;
		}
		else
		{
			//do nothing;
		}
		
		if($idleMAP != 0)
		{
			if($vType == 2)
			{
				if($MAP_Type_C == "Absolute")
				{
					//echo "Turbo idle Score: idleMAP = $idleMAP | BaroPress = $BaroPress \n";
					//Step 3. Check Idle Boost State for Absolute Pressure Sensor
					if($idleMAP >= $BaroPress)
					{
						$IdleBoostSCORE = 3;
					}
					elseif($idleMAP == ($BaroPress - 1))
					{
						$IdleBoostSCORE = 2;
					}
					else//if($MAPIdle < $BaroPress)
					{
						$IdleBoostSCORE = 1;
					}
				}
				else
				{
					//Step 4. Check Idle Boost State for Differential Pressure Sensor
					if($idleMAP >= ($BaroPress - $MAP_Dif_IdleLmt3_C))
					{
						$IdleBoostSCORE = 3;
					}
					elseif($idleMAP < ($BaroPress - $MAP_Dif_IdleLmt1_C))
					{
						$IdleBoostSCORE = 1;
					}
					else
					{
						$IdleBoostSCORE = 2;
					}
				}
			}
			else
			{
				$IdleBoostSCORE = 3;
			}
		}
		else
		{
			$IdleBoostSCORE = 0;
		}
		
		//Peak Score
		if($maxMAPcon != 0)
		{
			//Max MAP got in defined test conditions
			if($maxMAPcon > $maxMAPcsv)
			{
				$maxMAP = $maxMAPcon;
				if($MAP_Type_C == "Absolute")
				{
					if($vType == 2)
					{
						$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C + $PressDiff;
					}
					else
					{
						$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C;
					}
					
					//echo "Turbo Score: maxMAP = $maxMAP | BaroPress = $BaroPress | MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3_C | MAP_Abs_PkLmt1_C = $MAP_Abs_PkLmt1_C \n";
					if($maxMAP >= $MAP_Abs_PkLmt3_C)
					{
						if($maxMAP >= ($MAP_Abs_PkLmt3_C + $MAP_Abs_PkLmt_C))
						{
							$PeakBoostSCORE = 3;
							$msgPeak = "Your MAP values are a little higher than usual. We will keep you posted";
						}
						else
						{
							$PeakBoostSCORE = 3;
						}
					}
					elseif($maxMAP <= ($MAP_Abs_PkLmt3_C - $MAP_Abs_PkLmt1_C))
					{
						$PeakBoostSCORE = 1;
					}
					else
					{
						if($IdleBoostSCORE == 3)
						{
							$diff = $MAP_Abs_PkLmt3_C - $maxMAP;
							if($diff <= 2)
							{
								$IdleBoostSCORE = 2;
								$PeakBoostSCORE = 3;
							}
							else
							{
								$PeakBoostSCORE = 2;
							}
						}
						else
						{
							$PeakBoostSCORE = 2;
						}
					}
					
					if(($PeakBoostSCORE == 1) || ($PeakBoostSCORE == 2))
					{
						if($CombPresState == 4)
						{
							$PeakBoostSCORE = 2;
						}
						elseif($CombPresState == 5)
						{
							$PeakBoostSCORE = 3;
						}
					}
				}
				else
				{
					if($vType == 2)
					{
						$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C + $PressDiff;
					}
					else
					{
						$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C;
					}
					
					if($maxMAP >= $MAP_Dif_PkLmt3_C)
					{
						if($maxMAP >= ($MAP_Dif_PkLmt3_C + $MAP_Dif_PkLmt_C))
						{
							$PeakBoostSCORE = 3;
							$msgPeak = "Your MAP values are a little higher than usual. We will keep you posted";
						}
						else
						{
							$PeakBoostSCORE = 3;
						}
					}
					elseif($maxMAP <= ($MAP_Dif_PkLmt3_C - $MAP_Dif_PkLmt1_C))
					{
						$PeakBoostSCORE = 1;
					}
					else
					{
						if($IdleBoostSCORE == 3)
						{
							$diff = $MAP_Dif_PkLmt3_C - $maxMAP;
							if($diff <= 2)
							{
								$IdleBoostSCORE = 2;
								$PeakBoostSCORE = 3;
							}
							else
							{
								$PeakBoostSCORE = 2;
							}
						}
						else
						{
							$PeakBoostSCORE = 2;
						}
					}
					if(($PeakBoostSCORE == 1) || ($PeakBoostSCORE == 2))
					{
						if($CombPresState == 4)
						{
							$PeakBoostSCORE = 2;
						}
						elseif($CombPresState == 5)
						{
							$PeakBoostSCORE = 3;
						}
					}
				}
			}
			//Max MAP got at lower Eloads
			else
			{
				$maxMAP = $maxMAPcsv;
				if($MAP_Type_C == "Absolute")
				{
					if($vType == 2)
					{
						$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C + $PressDiff;
					}
					else
					{
						$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C;
					}
					
					//echo "Turbo Score: maxMAP = $maxMAP | BaroPress = $BaroPress | MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3_C | MAP_Abs_PkLmt1_C = $MAP_Abs_PkLmt1_C \n";
					if($maxMAPEload <= 70)
					{
						if($maxMAP >= $MAP_Abs_PkLmt3_C - 10)
						{
							$PeakBoostSCORE = 3;
						}
						else
						{
							$PeakBoostSCORE = 0;
						}
					}
					elseif($maxMAPEload <= 85)
					{
						if($maxMAP >= $MAP_Abs_PkLmt3_C - 5)
						{
							$PeakBoostSCORE = 3;
						}
						else
						{
							$PeakBoostSCORE = 0;
						}
					}
				}
				else
				{
					if($vType == 2)
					{
						$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C + $PressDiff;
					}
					else
					{
						$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C;
					}
					
					if($maxMAPEload <= 70)
					{
						if($maxMAP >= $MAP_Abs_PkLmt3_C - 10)
						{
							$PeakBoostSCORE = 3;
						}
						else
						{
							$PeakBoostSCORE = 0;
						}
					}
					elseif($maxMAPEload <= 85)
					{
						if($maxMAP >= $MAP_Abs_PkLmt3_C - 5)
						{
							$PeakBoostSCORE = 3;
						}
						else
						{
							$PeakBoostSCORE = 0;
						}
					}
				}
			}
		}
		//Test Conditions not met
		else
		{
			$PeakBoostSCORE = 0;
		}
		$IdleAirBoostSCORE = 0;
		$PeakAirBoostSCORE = 0;
		//echo "Turbo Sys Hel : MAPIdle: $idleMAP | MAPPeak: $maxMAP | IDLEboostSCORE = $IDLEboostSCORE | PeakBoostSCORE = $PeakBoostSCORE \n";
	}
	//===================== Turbocharged System Algo End ==================================//
	//===================== Air System Algo Start =========================================//
	else
	{
		if(($obdBarometricPressure == 0) || ($obdBarometricPressure == ""))
		{
			$BaroPress = $baroIdleMAP;
		}
		else
		{
			$BaroPress = $obdBarometricPressure;
		}
		
		//$maxairMAP = $maxMAP;
		if($MAP_Valid_C == 1)
		{
			/*$IdleAirBoostSCORE = 3; 	//Air system not judged for NA petrols at idle
			if($maxairMAP != 0)
			{
				if($maxairMAP >= ($BaroPress - $MAP_Air_PkLmt3_C))
				{
					if($maxairMAP >= ($BaroPress + $MAP_Air_PkLmt_C))
					{
						$PeakAirBoostSCORE = 3; 
						$msgPeakAir = "Your MAP values are a little higher than usual. We will keep you posted";
					}
					elseif($maxairMAP == ($BaroPress - $MAP_Air_PkLmt3_C)) 
					{
						$PeakAirBoostSCORE = 3;
						$IdleAirBoostSCORE = 2;
					}
					else
					{
						$PeakAirBoostSCORE = 3;
					}
				}
				elseif($maxairMAP < ($BaroPress - $MAP_Air_PkLmt1_C))
				{
					$PeakAirBoostSCORE = 1;
				}
				elseif($maxairMAP < ($BaroPress - $MAP_Air_PkLmt3_C))
				{
					$PeakAirBoostSCORE = 2;
				}
			}
			else
			{
				$PeakAirBoostSCORE = 0;
				$msgPeakAir = "Conditions not met.";
			}*/
			
			//*************************** Idle Air MAP Score *********************************//
			//echo "MAPPeak0 = $MAPPeak0 | MAPPeak1 = $MAPPeak1 | MAP_Air_IdleLmt3_C = $MAP_Air_IdleLmt3_C | obdBarometricPressure = $obdBarometricPressure \n";
			if($MAPPeak0 != 0)
			{
				if($MAPPeak0 >= ($obdBarometricPressure - $MAP_Air_IdleLmt3_C))
				{
					$IdleAirBoostSCORE = 3;
				}
				elseif($MAPPeak0 < ($obdBarometricPressure - $MAP_Air_IdleLmt1_C))
				{
					$IdleAirBoostSCORE = 1;
				}
				else
				{
					$IdleAirBoostSCORE = 2;
				}
			}
			else
			{
				$IdleAirBoostSCORE = 0;
			}
			//*************************** Peak Air MAP Score *********************************//
			if($MAPPeak1 != 0)
			{
				if($MAPPeak1 >= ($obdBarometricPressure - $MAP_Air_PkLmt3_C))
				{
					$PeakAirBoostSCORE = 3;
					if(($IdleAirBoostSCORE < 3) && ($IdleAirBoostSCORE != 0))
					{
						$IdleAirBoostSCORE = $IdleAirBoostSCORE + 1;
					}
					else
					{
						//Do nothing
					}
				}
				elseif($MAPPeak1 < ($obdBarometricPressure - $MAP_Air_PkLmt1_C))
				{
					$PeakAirBoostSCORE = 1;
				}
				else//if($MAPPeak1 < ($obdBarometricPressure - $MAP_Air_PkLmt3_C))
				{
					$PeakAirBoostSCORE = 2;
				}
			}
			else
			{
				$PeakAirBoostSCORE = 0;
			}
		}
		else
		{
			$IdleAirBoostSCORE = 0;
			$PeakAirBoostSCORE = 0;
			$msgPeakAir = "MAP not supported";
		}
		
		$IdleBoostSCORE = 0;
		$PeakBoostSCORE = 0;
		//echo "Air Sys Hel : MAPIdle: $idleMAP | MAPPeak: $maxMAP | IdleAirBoostSCORE = $IdleAirBoostSCORE | PeakairBoostSCORE = $PeakAirBoostSCORE \n";
	}
	//===================== Air System Algo End ==========================================//
	
	//===================== RPM OSCILLATION ANALYSIS Algo Start ==========================//
	//echo "\n deltaRPM = $deltaRPM \n";
	if($deltaRPM >= $Osc_RPM_Lmt1_C)
	{
		$currRpmOscScore = 1;
	}
	elseif($deltaRPM > $Osc_RPM_Lmt2_C)
	{
		$currRpmOscScore = 2;
	}
	elseif($deltaRPM > $Osc_RPM_Lmt3_C)
	{
		$currRpmOscScore = 3;
	}
	elseif($deltaRPM > $Osc_RPM_Lmt4_C)
	{
		$currRpmOscScore = 4;
	}
	elseif(($deltaRPM <= $Osc_RPM_Lmt4_C) && ($deltaRPM != 0))
	{
		$currRpmOscScore = 5;
	}
	else
	{
		//do nothing
	}
	
	//If score is 4 we give score 5 because of accuracy issue with the incoming data
	if(($currRpmOscScore == 4) && ($CombPresState == 5))
	{
		$currRpmOscScore = 5;
	}
	//If RPM Osc score is 2 and Comb score is greater than 3 we ignore RPM Osc score and check RPM Osc in next drive
	elseif(($currRpmOscScore == 2) && ($CombPresState > 3))
	{
		$currRpmOscScore = "";
	}
	//If RPM Osc score is 1 and Comb score is greater than 2 we ignore RPM Osc score and check RPM Osc in next drive
	elseif(($currRpmOscScore == 1) && ($CombPresState > 2))
	{
		$currRpmOscScore = "";
	}
	//====================== RPM OSCILLATION ANALYSIS Algo End ===========================//
	
	//If turbo score is good then the torque cannot be low. This is due to data missing (slow raster)
	if($CombPresState <= 3)
	{
		if($IdleBoostSCORE == 2 && $PeakBoostSCORE == 3)
		{
			$CombPresState = 3;
		}
		elseif($IdleBoostSCORE == 3 && $PeakBoostSCORE == 3)
		{
			$CombPresState = 4;
		}
	}
	
	// After all calculations update database
	$computedMonRes = array();
	$computedMonRes['device_id'] = $devId;
	$computedMonRes['dt'] = date('Y-m-d');
	$computedMonRes['drive'] = $Fsprint;
	$computedMonRes['dur'] = $totaldur;
	$computedMonRes['press'] = $CurrBaroPress;
	$computedMonRes['temp'] = $CurTemp;
	$computedMonRes['hmdty'] = $CurHumi;
	$computedMonRes['drvblty'] = $Drivability;
	$computedMonRes['ad_dt'] = date('Y-m-d H:i:s');
	$computedMonRes['bat_st'] = $batState;
	$computedMonRes['bat_c'] = $baroIdleMAP;
	$computedMonRes['idlb_c'] = $IdleBoostSCORE;
	$computedMonRes['idlb_m'] = $idleMAP;
	$computedMonRes['pekb_c'] = $PeakBoostSCORE;
	$computedMonRes['pekb_m'] = $maxMAP;
	$computedMonRes['idlbc_v'] = $IdleAirBoostSCORE;
	$computedMonRes['peakb_v'] = $PeakAirBoostSCORE;
	$computedMonRes['peakb_m'] = $maxairMAP;
	$computedMonRes['idl_crp'] = $IdleCRPSCORE;
	$computedMonRes['peak_crp'] = $PeakCRPSCORE;
	$computedMonRes['crpm'] = $idleCRP;
	$computedMonRes['tune_msg'] = $maxCRP;
	$computedMonRes['comp_pres'] = $CombPresState;
	$computedMonRes['eng_block_ej'] = $currRpmOscScore;
	$computedMonRes['erg_cmd_wrm'] = $deltaRPM;
	$computedMonRes['coolp'] = $coolHelScore;
	$computedMonRes['coolm'] = $minCool;
	$computedMonRes['max_drive_torque'] = $maxTorque;
	$computedMonRes['reference_torque'] = $NewMaxTorque;
	
	// has to go

	$saveId = addData('device_monitor',$computedMonRes,$conn);
	if($saveId)
	{
		setReportData($devId,$dt,$saveId,2,$conn);
		UpdateMonitorRatings($devId,$conn);
	}
	unset($computedSatatistics);
	unset($iLoopData);
}


// function getCaliPramVal($deviceID,$conn)
// {
// 	//return 1;
// 	// $row     = array();
// 	$sqlSett = "select * from vehicle_setting";
// 	$result  = $conn->query($sqlSett);
// 	if ($result->num_rows > 0) {
//         while($row  = $result->fetch_assoc()) {
//            $vehSett = $row;
//         }
//     }

// 	$sql    = "select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='$deviceID'";
// 	$result = $conn->query($sql);
//     if ($result->num_rows > 0) {
//         while($row  = $result->fetch_assoc()) {
//            $vehicle = $row;
//         }
//     }
// 	$f1 = "calval$pDVar";
// 	$vsetVal = json_decode($vehSett[$f1],true);
// 	$calval = json_decode($vehicle['calval'],true);
// 	// echo "funct = $calval";	 
// 	return array('calval' => $calval, 'fuel' => $vehicle['fuel_tpe']);
// }

function calcEngStateNew($devId,$engineState,$engRpm = "",$obdSpeed = "",$type = "",$flSys = "",$engLoadVal = "")
{
	$updEngState = 0;
	if($engineState == 0)
	{
		if($engRpm == 0)
		{
			if(($obdSpeed < 2))
			{
				$updEngState = 0;
			}
			else
			{
				$updEngState = 7;
			}
		}
		elseif($engRpm > 0 && $engRpm < 450)
		{
			if($obdSpeed < 2)
			{
				$updEngState = 1;
			}
			else
			{
				$updEngState = 8;
			}
		}
		elseif($engRpm >= 450)
		{
			if($obdSpeed < 2)
			{
				$updEngState = 2;
			}
			else
			{
				if($type == 1) //Petrol cars
				{
					if(($engRpm > 1250) && ($flSys == 4) && ($engLoadVal <= 50))
					{
						$updEngState = 4;
					}
					else
					{
						$updEngState = 3;
					}
				}
				elseif($type == 2)	//Diesel Cars
				{
					if(($engRpm > 1050) && ($engLoadVal == 0))
					{
						$updEngState = 4;
					}
					else
					{
						$updEngState = 3;
					}
				}
			}
		}
	}
	elseif($engineState == 6)
	{
		$updEngState = 6;
	}
	else
	{
		//Do nothing
	}
	return $updEngState;
}

function addData($table,$postVal,$conn)
{ 
	$tablefields = getfields($table,$conn);
	unset($tablefields[0]);

	$tablefields = array_values($tablefields);
	$fields = implode(',',$tablefields);
	$value = "";
	$lastVal = count($tablefields)-1;
	$val = $tablefields;
	for($i=0; $i<count($tablefields); $i++)
	{
		if(isset($postVal[$val[$i]]))
		{
			$value.= "'".$postVal[$val[$i]]."'";
		}
		elseif(!isset($postVal[$val[$i]]))
		{
			$value.= "''";
		}
		if($i < $lastVal)
		{
			$value.= ",";
		} 
	}

	$query = "insert into " . $table . "($fields) values($value)";
	// echo $query;
    $conn->query($query);
    return $conn->insert_id;
}

// function getfields($table,$conn){
//     $sql             = "SHOW columns FROM $table";
//     $device_data_col = $conn->query($sql);
//     if ($device_data_col->num_rows > 0) {
// 	    $valLast     = $postVal = $oldData = array();
// 	    while($row   = $device_data_col->fetch_assoc()) {
// 	       $vestFields[] = $row['Field'];
// 	    }
//     }
//     return $vestFields;
// }

function setReportData($deviceID,$dt,$id,$type,$conn)
{ 
	$dt = date('Y-m-d H:i:s');
	$dtTxt = date('dMY H:i:s',strtotime($dt));
	$typeTxt = ($type == 1) ? "Calibration $dtTxt" : "Monitor $dtTxt" ;
	$postVal = array();
	$postVal['devid'] = $deviceID;
	$dev = getdevice($deviceID,$conn);
	$postVal['did'] = $dev['id'];
	$postVal['type'] = $typeTxt;
	$postVal['status'] = 2;
	$postVal['ad_dt'] = $dt;
	$postVal['ad_by'] = 1;
	$postVal['idmc'] = $id;
	$postVal['mctype'] = $type;
	addData('report',$postVal,$conn);
}

// function getdevice($deviceid,$conn)
// {
// 	$sql    = "select * from device where device_id='$deviceid'";
// 	$result = $conn->query($sql);
//     if ($result->num_rows > 0) {
//         while($row = $result->fetch_assoc()) {
//            $device = $row;
//         }
//     }
//     return $device;
// }

function CreateStatMonDataCSV($iLoopData,$devId,$Fsprint,$colHeader)
{
	$conn = getDbHandle();

	unset($colHeader[0]);
	$csvheaderArr  = array_values($colHeader);
	$csvheaderName = getValuePartList($conn);
	
	foreach ($csvheaderArr as $key => $value) {
		if (substr($value, 0, 1) == 'F')
		{
			$i = substr($value, 1);
			if(is_numeric($i) && $i != 27 && $i != 28 && $i != 29 && $i != 30 && $i != 31 && $i != 32 && $i != 33 && $i != 50 && $i != 51 && $i != 52 && $i != 54 && $i != 55 && $i != 56)
			{
				$csvheader[$key] = $csvheaderName[$i];
			}
			else
			{
				$csvheader[$key] = $csvheaderArr[$key];
			}
		}
		else
		{
			$csvheader[$key] = $csvheaderArr[$key];
		}
	}
	//print_r($iLoopData);
	//clearstatcache();
	$folder = "/var/www/html/eva.enginecal.com/datacsv/".$devId;
	if(!file_exists($folder))
	{
		mkdir($folder,0777,true);
	}
	// chmod($folder, 0777);
	//echo "\n floder = $floder \n";
	$folder1 = "$folder/".date('dmY',time());
	if(!file_exists($folder1))
	{
		mkdir($folder1,0777,true);
	}
	// chmod($folder1, 0777);
	$fileN = "$folder1/". date('dmY',time()) ."_". $devId ."_". $Fsprint . ".csv";

	// $fh = fopen("$fileN",'w');

	# write out the headers
    // fputcsv($fh, $csvheader);
    
	//echo "\n fh = $fh \n";
    # write out the data
    // foreach($iLoopData as $row)
    // {      	
    // 	array_shift($row);
    // 	// print_r($row);die;
    // 	fputcsv($fh, $row);	
    // }
    // fclose($fh);
    // chmod($fileN, 0777);
	unset($iLoopData);
}

// function getdeviceFile($deviceid,$conn)
// {
// 	  $sql    = "select file_list from device where device_id='$deviceid'";
// 	  $device = $conn->query($sql);
// 	   while($row   = $device->fetch_assoc()){
// 	          $file = $row['file_list'];
// 	   }
// 	   return $file;
// }

function setdeviceFile($deviceid,$fname,$conn)
{
	$devFile = getdeviceFile($deviceid,$conn);
 	$exist   = 0;
	if(in_array($fname, $devFile))
	{
	   $exist = 1;
	}
	if($exist == 0)
	{
		$cnt = count($devFile);
		$cnt = ($cnt >= 1 && trim($devFile[0]) != "") ? $cnt : 0;
		$devFile[$cnt] = $fname;
	}
	 
	$flist = implode(';',$devFile);
	$sql   = "update device set file_list = '$flist' where device_id = '$deviceid'";
	if($conn->query($sql))
	{
		return true;
	}
	else
	{
		return false;
	}		
}
