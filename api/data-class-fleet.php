<?php
class datAct extends myclass 
{
	var $db = "";
	var $dev = "";
	function __construct($id = NULL)
	{
		$db = parent::__construct();
		$this -> dev = $id;
	}
	
	function randKey($length)
	{
		$chars = "0123456789abcdefghijklmnopqrstuvwxyz0123456789";
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ )
		{
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}	
		$sql = "select * from api_key where api_key = '$str'";
		if(mysql_num_rows(mysql_query($sql)) >= 1)
		{
			$this -> rand_string($length);
		}
		return $str;
	}
	
	function randKeyVerify($str)
	{
		$sql = "select * from api_key where api_key = '$str'";
		if(mysql_num_rows(mysql_query($sql)) >= 1)
		{
			return TRUE;
		}
		return FALSE;
	}
	
	function bearerKeyVerify($str)
	{
		//Currently verification does not consider expiry time. TOBE added later.
		$sql = "select * from api_key where bearer_auth = '$str'";
		if(mysql_num_rows(mysql_query($sql)) >= 1)
		{
			return TRUE;
		}
		return FALSE;
	}
	
	function isLat($fname)
	{
		$fNameVal = $this -> dev;
		$sql = "select 	va_id,va_main from valuesubpart where $fNameVal = '$fname' and va_lt_or_lng = '1'";
		return mysql_num_rows(mysql_query($sql));
	}
	function isLon($fname)
	{
		$fNameVal = $this -> dev;
		$sql = "select 	va_id,va_main from valuesubpart where $fNameVal = '$fname' and va_lt_or_lng = '2'";
		return mysql_num_rows(mysql_query($sql));
	}
	function isAlt($fname)
	{
		$fNameVal = $this -> dev;
		$sql = "select 	va_id,va_main from valuesubpart where $fNameVal = '$fname' and va_lt_or_lng = '3'";
		return mysql_num_rows(mysql_query($sql));
	}
	
	function corefield($fname)
	{
		$fNameVal = $this->dev;
		$sql = "select id,decim from corepart where $fNameVal = '$fname'";
		$fValDat = $this->select($sql);
		$fValDat = $fValDat[0];
		$fieldName = "C";
		$fieldName .= ($fValDat['id'] >= 1) ? $fValDat['id'] : 0;
		//return $fieldName;
		return array($fieldName,$fValDat['decim']);
	}
	
	function valuefield($fname)
	{
		$fNameVal = $this->dev;
		$sql = "select device_data_column from valuepart where $fNameVal = '$fname'";
		$fValDat = $this -> select($sql);
		if(empty($fValDat))
		{
     		// list is empty.
     		return array("","");
		}
		else
		{
			$fValDat = $fValDat[0];
			$fieldName = "F";
			$fieldName .= ($fValDat['id'] >= 1) ? $fValDat['id'] : 0;
			return array($fieldName,$fValDat['decim']);
		}
	}

	function valueSubfield($fname)
	{
		$fNameVal = $this->dev;
		$sql = "select va_id,va_main,va_decim from valuesubpart where va_$fNameVal = '$fname'";
		$fValDat = $this->select($sql);
		$fValDat = $fValDat[0];
		$fieldName = "F".$fValDat['va_main']."_";
		$fieldName .= ($fValDat['va_id'] >= 1) ? $fValDat['va_id'] : 0;
		//echo $fValDat['va_main'];
		return array($fieldName,$fValDat['va_decim']);
	}
	
	function devTotCount()
	{
		$sql = "select * from device where status = 1";
		$sql .= ($_SESSION['devListMainHome'] != "") ? " and device_id in(".$_SESSION['devListMainHome'].")" : "";
		return mysql_num_rows(mysql_query($sql));
	}
	
	function devLiveCount()
	{
		$dt = strtotime(date('Y-m-d H:i:s')) - (30*60);
		$lst = date('Y-m-d H:i:s',$dt);
		$sql = "select distinct d.id from device as d, device_data as dd where d.status = 1 and d.device_id = dd.device_id and time_v >= '$lst'";
		$sql .= ($_SESSION['devListMainHome'] != "") ? " and d.device_id in(".$_SESSION['devListMainHome'].")" : "";
		$res = $this -> select($sql);
		$cont = count($res);
		$redL = array();
		$i = 0;
		foreach($res as $r)
		{
			$redL[$i] = $r['id'];
			$i++;
		}
		unset($res);
		return array($cont,implode(',',$redL));
	}
	
	function devIncativeCount()
	{
		$dt = strtotime(date('Y-m-d H:i:s')) - (30*60);
		$dtF = strtotime(date('Y-m-d H:i:s')) - (60*60*24*30);
		$lst = date('Y-m-d H:i:s',$dt);
		$lstF = date('Y-m-d H:i:s',$dtF);
		$sql = "select distinct d.id from device as d, device_data as dd where d.status = 1 and d.device_id = dd.device_id and time_v >= '$lstF' and  time_v <= '$lst'";
		$sql .= ($_SESSION['devListMainHome'] != "") ? " and d.device_id in(".$_SESSION['devListMainHome'].")" : "";
		$res = $this -> select($sql);
		$cont = count($res);
		$redL = array();
		$i=0;
		foreach($res as $r)
		{
			$redL[$i] = $r['id'];
			$i++;
		}
		unset($res);
		
		$tCnt = $this -> devTotCount();
		$cont = ($tCnt - $cont);
		
		return array($cont,implode(',',$redL));
	}
}

class devAct extends myclass
{
	var $db = "";
	var $dev = "";
	function __construct($id = NULL)
	{
		$db = parent::__construct();
		$this -> dev = $id;
	}
	
	function devCurData($deviceList,$lat,$lon)
	{
		$devInfoTxt = "0";
		for($iD=0;$iD<count($deviceList);$iD++)
		{
			$sql = "select  latitude as lat, longitude as lon from device where device_id = '".$deviceList[$iD]."' and latitude <> '' and longitude <> '' order by id desc limit 0,1";
			$dat = $this -> select($sql);
			
			//$sql = "select lat,lon from device_data where device_id = '".$deviceList[$iD]."' and lat <> '' and lon <> '' order by time_s desc limit 0,1";
			//$dat = $this->select($sql);
			foreach($dat as $val)
			{
				$lat .= "," . $val['lat'];
				$lon .= "," . $val['lon'];
				$devInfo = $this -> select("select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='".$deviceList[$iD]."'");
				$driver = $this -> getdriver($deviceList[$iD]);
				$vehicle = $this -> getvehicle($deviceList[$iD]);
				$veh_sett = $vehicle[1];
				$veh_det = $vehicle[0];
				$vehManu = explode(';',$veh_sett['manufacturer']);
				
				$devInfoTxt .= "," . $driver['uname']."(DeviceID ".$deviceList[$iD].")<br/>".$vehManu[$veh_det['manufact']]."@S".$deviceList[$iD];	
			}
		}
		return array($lat,$lon,$devInfoTxt);
	}
	
	function getdriver($deviceid)
	{
		 $sql = "select u.* from users as u, device as d where d.id=u.device and d.device_id='$deviceid'";
		 $driver = mysql_fetch_array(mysql_query($sql));
		 return $driver;
	}
	
	function getPartner($deviceid)
	{
		 $sql = "select u.* from users as u, device as d where d.partner=u.uid and d.device_id='$deviceid'";
		 $driver = mysql_fetch_array(mysql_query($sql));
		 return $driver;
	}

	function getdevice($deviceid)
	{
		 $sql = "select * from device where device_id='$deviceid'";
		 $device = mysql_fetch_array(mysql_query($sql));
		 return $device;
	}
	
	function getdeviceActStatus($deviceid)
	{
		 $sql = "select d.status,a.expdt from device as d, activation_code as a where d.device_id = a.mac_id and d.device_id='$deviceid'";
		 $device = mysql_fetch_array(mysql_query($sql));
		 return $device;
	}
	
	function getdeviceFile($deviceid)
	{
		 $sql = "select file_list from device where device_id='$deviceid'";
		 $device = mysql_fetch_array(mysql_query($sql));
		 return explode(';',$device['file_list']);
	}

	function setdeviceFile($deviceid,$fname)
	{
		 $devFile = $this -> getdeviceFile($deviceid);
		 $exist = 0;
		 if(in_array($fname, $devFile))
		 {
		    $exist = 1;
		 }
		 if($exist == 0)
		 {
			 $cnt = count($devFile);
			 $cnt = ($cnt>=1 && trim($devFile[0]) != "") ? $cnt : 0;
			 $devFile[$cnt] = $fname;
		 }
		 
		 $flist = implode(';',$devFile);
		 $sql = "update device set file_list='$flist' where device_id='$deviceid'";
		 if(mysql_query($sql))
		 {
		 	return true;
		 }
		 else
		 {
			 return false;
		 }		
	}
	
	function resetdeviceFile($deviceid,$fname)
	{
		$devFile = $this -> getdeviceFile($deviceid);
		//$cnt = count($devFile);
		//$cnt = ($cnt>=1) ? $cnt : 0;
		//$devFile[$cnt] = $fname;
		if(in_array($fname, $devFile))
		{
			$key = array_search($fname, $devFile);
			unset($devFile[$key]);
		}
		$flist = implode(';',$devFile);
		$sql = "update device set file_list='$flist' where device_id='$deviceid'";
		if(mysql_query($sql))
		{
			return true;
		}
		else
		{
			return false;
		}	
	}

	function getdeviceKey($deviceid)
	{
		 $sql = "select * from device where device_id='$deviceid'";
		 $device = mysql_fetch_array(mysql_query($sql));
		 //return $device;
		 //echo $device['ad_dt'];
		 $tm = strtotime($device['ad_dt']);
		 $tm *= $device['id'];
		 $tm = "$tm";
		 $len = strlen($tm);
		 $len -= 4;
		 $key = substr($tm, $len);
		 return $key;
	}
	
	function getbluetoothKey($deviceid)
	{
		 //$sql = "select * from device where device_id='$deviceid'";
		 //$device = mysql_fetch_array(mysql_query($sql));
		 //return $device;
		 //echo $device['ad_dt'];
		 $tm = strtotime($device['ad_dt']);
		 $tm *= $deviceid;
		 $tm = "$tm";
		 $len = strlen($tm);
		 $len -= 4;
		 $key = substr($tm, $len);
		 return $key;
	}
	
	function getvehicle($deviceid)
	{
		 $sqlSett = "select * from vehicle_setting";
		 $vehSett = mysql_fetch_array(mysql_query($sqlSett));
		 $sql = "select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='$deviceid'";
		 $vehicle = mysql_fetch_array(mysql_query($sql));
		 $pDVar = ($vehicle['fuel_tpe'] == 1) ? "p" : "d";
		 $f1 = "co2fact$pDVar";
		 $vehicle['co2fact'] = $this -> getCaliPramVal('Co2_Cnv_Fac_C',$deviceid);//($vehicle['fuel_tpe'] >= 1 && trim($vehicle['co2fact']) <= 0) ? $vehSett[$f1] : $vehicle['co2fact'];
		 $vehicle['oill_day'] = $this -> getCaliPramVal('Oil_Life_Days_C',$deviceid);//($vehicle['fuel_tpe'] >= 1 && trim($vehicle['oill_day'])<=0) ? $vehSett[$f2] : $vehicle['oill_day'];
		 $vehicle['oill_dist'] = $this -> getCaliPramVal('Oil_Life_Kms_C',$deviceid);//($vehicle['fuel_tpe'] >= 1 && trim($vehicle['oill_dist'])<=0) ? $vehSett[$f3] : $vehicle['oill_dist'];
		 $vehicle['dust_env'] = $this -> getCaliPramVal('DustEnv_Status_C',$deviceid);//($vehicle['fuel_tpe'] >= 1 && trim($vehicle['dust_env'])<=0) ? $vehSett[$f4] : $vehicle['dust_env'];
		 
		 $vehicle['nfact'] = $this -> getCaliPramVal('Eng_eff_C',$deviceid);
		 $vehicle['ve'] = $this -> getCaliPramVal('Vol_eff_C',$deviceid);
		 $vehicle['ed'] = $this -> getCaliPramVal('Eng_Disp_C',$deviceid);
		 
		 $vehicle['int_cool'] = $this -> getCaliPramVal('IC_Status_C',$deviceid);
		 $vehicle['egr_cool'] = $this -> getCaliPramVal('EgrC_Status_C',$deviceid);
		 $vehicle['egr_cmd'] = $this -> getCaliPramVal('Egr_Status_C',$deviceid);
		 
		 $vehicle['lmt1'] = $this -> getCaliPramVal('Eff_RPM_Limit1_C',$deviceid);
		 $vehicle['lmt2'] = $this -> getCaliPramVal('Eff_RPM_Limit2_C',$deviceid);
		 $vehicle['lmt3'] = $this -> getCaliPramVal('Eff_RPM_Limit3_C',$deviceid);
		 // ve ed int_cool egr_cool egr_cmd lmt1 lmt2 lmt3
		 return array($vehicle,$vehSett);
	}

	function devDriveData($deviceList,$lat,$lon,$dt='')
	{
		$devInfoTxt = "0";
		for($iD=0;$iD<count($deviceList);$iD++)
		{
			$drF = $dt . " 00:00:00";
			$drT = $dt . " 23:59:59";
			$sql = "select lat,lon,time_v from device_data where device_id = '".$deviceList[$iD]."' and lat <> '' and lon <> '' and (time_v >= '$drF' and time_v <= '$drT') order by time_s asc";
			$dat = $this->select($sql);
			foreach($dat as $val)
			{
				$timeSt = strtotime($val['time_v']);
				$lat .= "," . $val['lat'];
				$lon .= "," . $val['lon'];
				/*$devInfo = $this->select("select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='".$deviceList[$iD]."'");
				$devInfoTxt .= "," . $devInfo[0]['model'];*/
				$devInfo = $this->select("select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='".$deviceList[$iD]."'");
				$driver = $this->getdriver($deviceList[$iD]);
				$vehicle = $this->getvehicle($deviceList[$iD]);
				$veh_sett = $vehicle[1];
				$veh_det = $vehicle[0];
				$vehManu = explode(';',$veh_sett['manufacturer']);
				
				$devInfoTxt .= "," . $driver['uname']."(DeviceID ".$deviceList[$iD].") <br/>".$vehManu[$veh_det['manufact']]. " @ " . date('H:i:s',$timeSt)."@S".$deviceList[$iD] . " @ " . date('H:i:s',$timeSt);		
			}
		}
		return array($lat,$lon,$devInfoTxt);
	}
	
	function devDriveDataApi($deviceID,$dt='')
	{
		$devInfoTxt = "0";
		$drF = date('Y-m-d',strtotime($dt)) . " 00:00:00";
		$drT = date('Y-m-d',strtotime($dt)) . " 23:59:59";
		$sql = "select * from event where device_id = '".$deviceID."' and lat <> '' and lon <> '' and (ad_dt >= '$drF' and ad_dt <= '$drT') order by time_s asc";
		$dat = $this->select($sql);
		$array = array();
		$aI = 0;
		foreach($dat as $val)
		{
			$array[$aI]	= array('ts' => $val['time_s'], 'lat' => $val['lat'], 'long' => $val['lon'] , 'alt' => $val['alt'] );
			$aI++;
		}
		return $array;
	}
	
	function devDriveNumDataApi($deviceID,$drivenum)
	{
		$devInfoTxt = "0";
		$sql = "select * from event where device_id = '".$deviceID."' and lat <> '' and lon <> '' and nodrive = '$drivenum' order by time_s asc";
		$dat = $this -> select($sql);
		$array = array();
		$aI = 0;
		foreach($dat as $val)
		{
			$array[$aI]	= array('ts' => $val['time_s'], 'lat' => $val['lat'], 'lang' => $val['lon'] , 'driveid' => $val['nodrive'] );
			$aI++;
		}
		return $array;
	}
	
	function devDriveRangeDataApi($deviceID,$driverangefrm,$driverangeto)
	{
		$devInfoTxt = "0";
		$sql = "select * from event where device_id = '".$deviceID."' and lat <> '' and lon <> '' and nodrive >= '$driverangefrm' and nodrive <= '$driverangeto' order by nodrive desc,time_s asc";
		$dat = $this -> select($sql);
		$array = array();
		$aI = 0;
		foreach($dat as $val)
		{
			$array[$aI]	= array('ts' => $val['time_s'], 'lat' => $val['lat'], 'lang' => $val['lon'] , 'driveid' => $val['nodrive'] );
			$aI++;
		}
		return $array;
	}
	
	function devDriveDataNew($deviceList,$lat,$lon,$dt='',$limit=0)
	{
		$devInfoTxt = "0";
		$lat = 0;
		$lon = 0;
		for($iD=0;$iD<count($deviceList);$iD++)
		{
			$drF = $dt . " 00:00:00";
			$drT = $dt . " 23:59:59";
			//$sql = "select distinct lat,lon,time_v from device_data where device_id = '".$deviceList[$iD]."' and lat <> '' and lon <> '' and (time_v >= '$drF' and time_v <= '$drT') order by time_s asc limit $limit,1000";
			$sql = "select distinct t2.lat,t2.lon,t2.time_v FROM ( select MIN(`time_v`) AS dt FROM device_data where device_id = '".$deviceList[$iD]."' and lat <> '' and lon <> '' and (lat*1) > '0' and (lon*1) > '0' and (time_v >= '$drF' and time_v <= '$drT') GROUP BY DATE_FORMAT(`time_v`,'%Y-%m-%d %H:%i') ) t1 JOIN device_data t2 ON t1.dt = t2.`time_v` where device_id = '".$deviceList[$iD]."' and lat <> '' and lon <> '' order by t2.time_s asc limit $limit,1000";
			$dat = $this->select($sql);
			foreach($dat as $val)
			{
				$timeSt = strtotime($val['time_v']);
				$lat .= "," . $val['lat'];
				$lon .= "," . $val['lon'];
				/*$devInfo = $this->select("select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='".$deviceList[$iD]."'");
				$devInfoTxt .= "," . $devInfo[0]['model'];*/
				$devInfo = $this->select("select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='".$deviceList[$iD]."'");
				$driver = $this->getdriver($deviceList[$iD]);
				$vehicle = $this->getvehicle($deviceList[$iD]);
				$veh_sett = $vehicle[1];
				$veh_det = $vehicle[0];
				$vehManu = explode(';',$veh_sett['manufacturer']);
				
				$devInfoTxt .= "," . $driver['uname']."(DeviceID ".$deviceList[$iD].") <br/>".$vehManu[$veh_det['manufact']]. " @ " . date('H:i:s',$timeSt)."@S".$deviceList[$iD] . " @ " . date('H:i:s',$timeSt);		
			}
		}
		return array($lat,$lon,$devInfoTxt,$sql);
	}

	function devDriveDataNewSD($deviceList,$lat,$lon,$dt='',$limit=0)
	{
		$devInfoTxt = "0";
		$lat = 0;
		$lon = 0;
		for($iD=0;$iD<count($deviceList);$iD++)
		{
			$drF = $dt . " 00:00:00";
			$drT = $dt . " 23:59:59";
			//$sql = "select distinct lat,lon,ad_dt from event where device_id = '".$deviceList[$iD]."' and lat <> '' and lon <> '' and (ad_dt >= '$drF' and ad_dt <= '$drT') order by time_s asc limit $limit,1000";
			//echo $sql = "select distinct t2.lat,t2.lon,t2.ad_dt FROM ( select MIN(`ad_dt`) AS dt FROM event where device_id = '".$deviceList[$iD]."' and lat <> '' and lon <> '' and (lat*1) > '0' and (lon*1) > '0' and (ad_dt >= '$drF' and ad_dt <= '$drT') GROUP BY DATE_FORMAT(`ad_dt`,'%Y-%m-%d %H:%i') ) t1 JOIN event t2 ON t1.dt = t2.`ad_dt` where device_id = '".$deviceList[$iD]."' and lat <> '' and lon <> '' order by t2.time_s asc limit $limit,1000";
			$sql = "select * FROM event where device_id = '".$deviceList[$iD]."' and (ad_dt >= '$drF' and ad_dt <= '$drT') and lat <> '' and lon <> '' and (lat*1) > '0' and (lon*1) > '0' order by time_s asc limit $limit,1000";
			$dat = $this->select($sql);
			foreach($dat as $val)
			{
				$timeSt = strtotime($val['ad_dt']);
				$lat .= "," . $val['lat'];
				$lon .= "," . $val['lon'];
				/*$devInfo = $this->select("select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='".$deviceList[$iD]."'");
				$devInfoTxt .= "," . $devInfo[0]['model'];*/
				$devInfo = $this->select("select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='".$deviceList[$iD]."'");
				$driver = $this->getdriver($deviceList[$iD]);
				$vehicle = $this->getvehicle($deviceList[$iD]);
				$veh_sett = $vehicle[1];
				$veh_det = $vehicle[0];
				$vehManu = explode(';',$veh_sett['manufacturer']);
				
				$devInfoTxt .= "," . $driver['uname']."(DeviceID ".$deviceList[$iD].") <br/>".$vehManu[$veh_det['manufact']]. " @ " . date('H:i:s',$timeSt)."@S".$deviceList[$iD] . " @ " . date('H:i:s',$timeSt);		
			}
		}
		return array($lat,$lon,$devInfoTxt,$sql);
	}
	
	function deviceDetailsDlist($srchVal='', $devL = '',$limit=0)
	{ 		
		$srchVal = ($devL != "") ? $srchVal : $srchVal;
		
		$sqlSett = "select * from vehicle_setting";
		$vehS = $this->select($sqlSett);
		$vehManu = explode(';',$vehS[0]['manufacturer']);
		
		$sqlS .= ($_SESSION['uType'] == 1 || $_SESSION['uType'] == 2)?" and u.partner != ''":"";
		$sqlS .= ($_SESSION['uType'] == 3 || $_SESSION['uType'] == 4)?" and u.partner = '".$_SESSION['uid']."'":"";
		$sqlS .= ($_SESSION['uType'] >= 5)?" and u.uid = '".$_SESSION['uid']."'":"";
		
		$devLL = explode(';',$devL);
		$devL = $devLL[0];
		$devSql = ($devLL[1] == 1) ? " not " : "";
		$sqlS .= ($devL != "") ? " and d.id $devSql in ($devL)" : "";
		
		$sqlS .= ($srchVal != '')?" and (u.uname LIKE '$srchVal%' or d.device_id LIKE '$srchVal%')":"";
		$sql = "select u.uname as dname,d.device_id as devid, v.manufact as manu from users as u, device as d, vehicles as v where u.ustatus='1' and d.status='1' and d.id=u.device and v.device=u.device $sqlS order by u.uid desc limit $limit, 10";
		$dat = $this->select($sql);
		foreach($dat as $val)
		{
			$drvn .= ($drvn == '')?$val['dname']:"," . $val['dname'];
			$devid .= ($devid == '')?$val['devid']:','.$val['devid'];
		
			$vehMan = $vehManu[$val['manu']];
			$vehd .= ($vehd == '')?$vehMan:','.$vehMan;
		}
		return array($drvn,$devid,$vehd,$sql); 
	}
	
	function deviceDetails($srchVal='', $devL = '')
	{ 		
		$srchVal = ($devL != "") ? "" : $srchVal;
		
		$sqlSett = "select * from vehicle_setting";
		$vehS = $this->select($sqlSett);
		$vehManu = explode(';',$vehS[0]['manufacturer']);
		
		$sqlS .= ($_SESSION['uType'] == 1 || $_SESSION['uType'] == 2) ? " and u.partner != ''":"";
		$sqlS .= ($_SESSION['uType'] == 3 || $_SESSION['uType'] == 4) ? " and u.partner = '".$_SESSION['uid']."'":"";
		$sqlS .= ($_SESSION['uType'] >= 5) ? " and u.uid = '".$_SESSION['uid']."'":"";
		//$sqlS .= ($_SESSION['uType'] == 6 || $_SESSION['uType'] == 7) ? " and u.uid = '".$_SESSION['uid']."'":"";
		
		$devLL = explode(';',$devL);
		$devL = $devLL[0];
		$devSql = ($devLL[1] == 1) ? " not " : "";
		$sqlS .= ($devL != "") ? " and d.id $devSql in ($devL)" : "";
		
		$sqlS .= ($srchVal != '')?" and (u.uname LIKE '$srchVal%' or d.device_id LIKE '$srchVal%')":"";
		$sql = "select u.uname as dname,d.device_id as devid, v.manufact as manu from users as u, device as d, vehicles as v where u.ustatus='1' and d.status='1' and d.id=u.device and v.device=u.device $sqlS order by u.uid desc limit 10";
		$dat = $this->select($sql);
		foreach($dat as $val)
		{
			$drvn .= ($drvn == '')?$val['dname']:"," . $val['dname'];
			$devid .= ($devid == '')?$val['devid']:','.$val['devid'];
			
			$vehMan = $vehManu[$val['manu']];
			$vehd .= ($vehd == '')?$vehMan:','.$vehMan;
		}
		return array($drvn,$devid,$vehd);
	}
	
	function calcEngState($engStat,$engStatOrg,$curVal,$devId,$engLoadRpm = "",$powerState="",$obdSpeed="",$type="",$flSys="",$engLoadVal="")
	{
		//echo "$eSpe,$eRpm,$tm,$devId <br/>";
		//echo "<br/> $tmF | $tmT <br/>";
		//$powerState = strtotime($powerState);
		$datAct = new datAct('device1');
		$fPst = $datAct->valuefield('corePowerState');
		$corePState = $fPst[0];
		$ret = $curVal;
		
		$sqlSprint = "select $engStatOrg, $engStat, time_v, $corePState from device_data where device_id='$devId'  order by time_s desc limit 0,1";
		$fValDat = $this->select($sqlSprint);
		$dat = $fValDat[0];
		//$delay = $timeNow - strtotime($dat['time_v']);
		//if(($curVal == 0) && ($powerState == 2) && trim($dat[$corePState]) != 2)
		if(($powerState == 2) && trim($dat[$corePState]) != 2)
		{
			$ret = 6;
		}
		elseif($curVal == 0)
		{
			//added to get Engine state rolling and Engine state push start
			if($obdSpeed != 0 && $engLoadRpm == 0)
			{
				$ret = 7;
			}
			elseif($obdSpeed != 0 && $engLoadRpm > 0 && $engLoadRpm <= 450)
			{
				$ret = 8;
			}
			else
			{
				$ret = 0;
			}
			/*if(($dat[$engStatOrg] == 2 || $dat[$engStatOrg] == 1) && $engLoadRpm < 450)
			{
				$ret = 6;
			}
			else
			{
				$ret = $dat[$engStat];
			}*/
			//$ret = $dat[$engStat];
		}
		elseif($curVal == 2)
		{
			if($obdSpeed == 0)
			{
				$ret = 2;
			}
			else
			{
				$ret = 3;
				if($type == 1 && $engLoadRpm > 1250 && $flSys == 4  && $engLoadVal <= 50)
				{
					$ret = 4;
				}
				elseif($type == 2 && $engLoadRpm > 1050 && $engLoadVal == 0)
				{
					$ret = 4;
				}
			}
		}
		elseif($curVal == 1)
		{
			if($obdSpeed == 0)
			{
				$ret = 2;
			}
			else
			{
				$ret = 3;
			}
		}		
		return $ret;
	}
	
	function calcEngStateNew($devId,$engineState,$engRpm = "",$obdSpeed = "",$type = "",$flSys = "",$engLoadVal = "")
	{
		$updEngState = 0;
		if($engineState == 0)
		{
			if($engRpm == 0)
			{
				if(($obdSpeed < 2))
				{
					$updEngState = 0;
				}
				else
				{
					$updEngState = 7;
				}
			}
			elseif($engRpm > 0 && $engRpm < 450)
			{
				if($obdSpeed < 2)
				{
					$updEngState = 1;
				}
				else
				{
					$updEngState = 8;
				}
			}
			elseif($engRpm >= 450)
			{
				if($obdSpeed < 2)
				{
					$updEngState = 2;
				}
				else
				{
					if($type == 1) //Petrol cars
					{
						if(($engRpm > 1250) && ($flSys == 4) && ($engLoadVal <= 50))
						{
							$updEngState = 4;
						}
						else
						{
							$updEngState = 3;
						}
					}
					elseif($type == 2)	//Diesel Cars
					{
						if(($engRpm > 1050) && ($engLoadVal == 0))
						{
							$updEngState = 4;
						}
						else
						{
							$updEngState = 3;
						}
					}
				}
			}
		}
		elseif($engineState == 6)
		{
			$updEngState = 6;
		}
		else
		{
			//Do nothing
		}
		return $updEngState;
	}
	
	function sprintDelayMain($dat,$fspLast,$engStat)
	{
		//print_r($fspLast);
		$datAct = new datAct('device1');
		$fld = $datAct->valuefield('obdEngineState');
		//echo $engStat = $fld[0];	
		$err = true;
		if(trim($fspLast[$engStat]) != "" && $fspLast[$engStat] < 6)
		{
			$err = false;
			//echo "aaaaaaaaaaaaaaaaa";
		}
		return $err;
	}
	
	function sprintDelay($eRpm,$engStat,$tm,$devId)
	{
		//echo "$eSpe,$eRpm,$tm,$devId <br/>";
		//echo "<br/> $tmF | $tmT <br/>";
		$sqlSprint = "select $eRpm, $engStat from device_data where device_id='$devId'  order by time_s desc limit 0,1";
		$fValDat = $this -> select($sqlSprint);
		$err = true;
		foreach($fValDat as $dat)
		{
			$sqlSprintLast = "select $eRpm, $engStat from device_data where device_id='$devId'  order by time_s desc limit 1,1";
			$fspLast = $this -> select($sqlSprintLast);
			
			if(trim($dat[$engStat]) != "" && ($dat[$engStat] < 6 || $dat[$engStat] > 6))
			{
				$err = false;
				break;
			}
			elseif($fspLast[0][$engStat] == 6)
			{
				$err = false;
				break;
			}			
			elseif(trim($dat[$engStat]) == "")
			{
				$time = strtotime($tm);
				$time4Min = $time - (60*5);
				
				$tmF = date('Y-m-d H:i:s',$time4Min);
				$tmT = date('Y-m-d H:i:s',$time);
				
				//echo "<br/> $tmF | $tmT <br/>";
				$sqlSprint = "select $eRpm from device_data where device_id='$devId' and time_v >= '$tmF' and  time_v <= '$tmT'  order by time_v asc";
				$fValDat = $this->select($sqlSprint);
				$err = true;
				foreach($fValDat as $dat)
				{
					if($dat[$eRpm] > 500)
					{
						$err = false;
						break;
					}
				}									
				//$err = false;
				//break;
			}
		}
		unset($fValDat);
		return $err;
	}
	
	/*function sprintDelay($eSpe,$engStat,$tm,$devId)
	{
		//echo "$eSpe,$eRpm,$tm,$devId <br/>";
		$time = strtotime($tm);
		$time4Min = $time - (60*5);
		
		$tmF = date('Y-m-d H:i:s',$time4Min);
		$tmT = date('Y-m-d H:i:s',$time);
		
		//echo "<br/> $tmF | $tmT <br/>";
		$sqlSprint = "select $eSpe, $eRpm from device_data where device_id='$devId' and time_v >= '$tmF' and  time_v <= '$tmT'  order by time_v asc";
		$fValDat = $this->select($sqlSprint);
		$err = true;
		foreach($fValDat as $dat)
		{
			if($dat[$eRpm] > 500)
			{
				$err = false;
				break;
			}
		}
		unset($fValDat);
		return $err;
	}*/

	function callArgage($devId,$f,$tms)
	{
		//$f = 'f4';
		$sqlSprint = "select avg($f) from device_data where device_id='$devId' and Fsprint = '$tms'  order by time_v asc";
		$fValDat = $this->select($sqlSprint);
		/*$err = true;
		$datVal = 0;
		$avr = 0;
		if(count($fValDat) >= 1)
		{
			foreach($fValDat as $dat)
			{
				$datVal += ($dat[$f] > 0) ? $dat[$f] : 0;
			}
			$avr = $datVal / count($fValDat);
		}
		unset($fValDat);*/
		//echo "$datVal | $avr <br/>" ;
		$avr = $fValDat[0][0];
		unset($fValDat);
		return $avr;
	}
	
	function callSum($devId,$f,$tms)
	{
		//$f = 'f4';
		$sqlSprint = "select $f from device_data where device_id='$devId' and Fsprint = '$tms'  order by time_v asc";
		$fValDat = $this->select($sqlSprint);
		$err = true;
		$datVal = 0;
		$avr = 0;
		if(count($fValDat) >= 1)
		{
			foreach($fValDat as $dat)
			{
				$datVal += ($dat[$f] > 0) ? $dat[$f] : 0;
			}
			//$avr = $datVal / count($fValDat);
		}
		unset($fValDat);
		//echo "$datVal | $avr <br/>" ;
		return $datVal;
	}
	
	function clearTimeData($row,$devId)
	{
		$datAct = new datAct('device1');
		$f3 = $datAct->valuefield('obdRpm');
		$f7 = $datAct->valuefield('obdSpeed');
		$f9 = $datAct->valuefield('obdCalcEngineState');
		$fPst = $datAct->valuefield('corePowerState');
		$corePState = $fPst[0];
		$row[$corePState] = 0;
		$eSpe = $f7[0];
		$eRpm = $f3[0];
		//print_r($row);
		$engStat = $f9[0];
		if($this -> sprintDelay($eRpm,$engStat,$row['time_v'],$devId))
		{
			/*$row['Fsprint'] = $time;
			$row['Fdist'] = 0;*/
			$fieldsTbale = $this->getfields('device_data');
			for($i=9;$i<count($fieldsTbale);$i++)
			{
				//echo $fieldsTbale[$i];
				if($fieldsTbale[$i] != "Fsprint")
				{
					$row[$fieldsTbale[$i]] = "";
				}
			}
		}
		return $row;
	}

/*	function nCalc($deviceid,$row,$n)
	{
			$datAct = new datAct('device1');
			$fZ = $datAct->valuefield('obdCommandedEGR');
			$obdCommandedEGR = $row[$fZ[0]];
			$fZ = $datAct->valuefield('obdEngineLoad');
			$obdEngineLoad = $row[$fZ[0]];
			$fZ = $datAct->valuefield('obdManifoldPressure');
			$obdManifoldPressure = $row[$fZ[0]];
			$fZ = $datAct->valuefield('obdRpm');
			$obdRpm = $row[$fZ[0]];
			$fZ = $datAct->valuefield('obdBarometricPressure');
			$obdBarometricPressure = $row[$fZ[0]];
			$fZ = $datAct->valuefield('obdCoolantTemperature');
			$obdCoolantTemperature = $row[$fZ[0]];
			$fZ = $datAct->valuefield('obdIntakeTemperature');
			$obdIntakeTemperature = $row[$fZ[0]];
			$fZ = $datAct->valuefield('obdAirFlowRate');
			$obdAirFlow = $row[$fZ[0]];
			
			if($obdBarometricPressure == 0 || $obdBarometricPressure == "")
			{
				$obdBarometricPressure = $this -> cityPres($deviceid);
			}
			
			/********************----------------New MAF to MAP Formula Start--------------------******************
				if($obdManifoldPressure <= 0 || $obdManifoldPressure == "")
				{
					$vehi = $this->getvehicle($deviceid);
				 	$vType = $vehi[0]['fuel_tpe'];
					$ve = $vehi[0]['ve'];
					$ed = $vehi[0]['ed']; 	
				 
				 	//Volumetric Efficiency calculations
				 	$Turbo_Avail_C = $this -> getCaliPramVal('TC_Status_C',$deviceid);
			
					if($vType == 2 || ($vType == 1 && $Turbo_Avail_C == "Yes"))
					{
					 	if($obdEngineLoad <= 55)
						{
							$fact1 = (0.000004 * Math.pow($obdEngineLoad, 4));
							$fact2 = (0.0021 * Math.pow($obdEngineLoad, 3));
							$fact3 = (0.1497 * Math.pow($obdEngineLoad, 2));
							$fact4 = ((1.9792 * $obdEngineLoad));
							
							$ve = $fact1 - $fact2 + $fact3 - $fact4 + 82.46;
						}
						elseif($obdEngineLoad <= 95)
						{
							$ve = 103 + (0.1 * $obdEngineLoad);
						}
						else
						{
							$ve = 101 + (0.1 * $obdEngineLoad);
						}
					}
					else
					{
						if($obdEngineLoad >= 1 &&  $obdEngineLoad <= 40)
						{
							$ve = 96;
						}
						elseif($obdEngineLoad >= 41 &&  $obdEngineLoad <= 80)
						{
							$ve = 96;
						}
						elseif($obdEngineLoad >= 81 &&  $obdEngineLoad <= 89)
						{
							$ve = 96;
						}
						elseif($obdEngineLoad >= 90 &&  $obdEngineLoad <= 98)
						{
							$ve = 97;
						}
						elseif($obdEngineLoad >= 99 &&  $obdEngineLoad <= 100)
						{
							$ve = 99;
						}	
					}
					
				 	//$weatherArray = $this -> getCurrWeather($reqFor,$deviceID,$dt,$row,$durTr);
					//$CurHumi = $weatherArray[1];
					$CurHmdty = ($CurHumi != "") ? $CurHumi : 50;
					$CurHumiper = $CurHumi/100;
					
					$num = (7.5 * $obdIntakeTemperature);
					$denom = ($obdIntakeTemperature + 237.3);
					$divFact = $num/$denom;
					$fact = pow(10, $divFact);
					$pv = $CurHumiper * ((6.1078 * $fact)/10);
	            	//	Density Calculation
		            $density = $obdAirFlow * (100 * 120)/ ($obdRpm * $ve * $ed * 1000);
					$MAPfact = ($density * (8.314 * ($obdIntakeTemperature + 273))) - ($pv * 0.018016);
				 	$pd = $MAPfact / 0.028964;
				 	$map = $pd + $pv;
				 }

			//********************----------------New MAF to MAP Formula End--------------------*******************
			//$obdManifoldPressure = $map;

			//$fld = $obdManifoldPressure;
			$datAct = new datAct('device1');
			$calStg = $datAct->valuefield('obdTestState');
			$calStg = $calStg[0];
			
			$fld = $datAct->valuefield('obdManifoldPressure');	 	
			$MAPStatic = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceid);
			
			// Check the type of Pressure Sensor
			$vlChk = ($obdBarometricPressure - $MAPStatic);
			if($MAP_Type_MinLmt_C < $vlChk &&  $vlChk < $MAP_Type_MaxLmt_C)
			{			 
				$MAP_Type_C = "Absolute";	//DIRECT INPUT INTO CARNET (CALIBRATABLE)//
			}
			else 
			{
				$MAP_Type_C = "Differential";  //DIRECT INPUT INTO CARNET (CALIBRATABLE)//
			}
			
			//Set the type of MAP sensor 
			$this -> setCaliPramVal('MAP_Type_C',$deviceid,$MAP_Type_C);
			
			$veh = $this -> getvehicle($deviceid);
			$baseN = $veh[0]['nfact'] / 100;
			$n = $baseN;
			$intercoolerPresent = $veh[0]['int_cool'];
			$EGRCoolerPresent = $veh[0]['egr_cool'];
			$EGRCommandPresent = $veh[0]['egr_cmd']; // added new
			$RPM_Limit1 = $veh[0]['lmt1'];
			$RPM_Limit2 = $veh[0]['lmt2'];
			$RPM_Limit3 = $veh[0]['lmt3'];
			$vType = $veh[0]['fuel_tpe'];
			$MAP_Type_C = $this -> getCaliPramVal('MAP_Type_C',$deviceid);
			$Turbo_Avail_C = $this -> getCaliPramVal('TC_Status_C',$deviceid);
			
		//Changing Efficiencies for Diesel Engines
			if(($vType == 2) || (($vType == 1) && ($Turbo_Avail_C == "Yes")))
			{
				if($EGRCommandPresent == 1) // changed
				{
	                if($obdCommandedEGR == 0)
	                {
	     				$n = $n;
	                }					
					elseif($obdCommandedEGR <= 30)
					{
						$n = $n - 0.02;
						if ($EGRCoolerPresent == 1) // Read EGR Cooler present = YES/NO from vehicle info form.
						{
							$n = $n - 0.01;
						}		
					}
					elseif($obdCommandedEGR <= 50)
					{
						$n = $n - 0.03; // As per the example Values, The code will hit here. soƞcalc = ƞbase - 2%, hence ƞcalc = 25%
						if ($EGRCoolerPresent == 1)
						{
							$n = $n - 0.02;
						}
					}
					else 
					{
						$n = $n - 0.04;
						if($EGRCoolerPresent == 1)
						{
							$n = $n - 0.03;
						}
					}
				}
				else
				{
					if($obdEngineLoad <= 15)
					{
						$n = $n - 0.03;
					}
					elseif($obdEngineLoad <= 35)
					{
						$n = $n - 0.02;
					}
					elseif($obdEngineLoad <= 55)
					{
						$n = $n - 0.01;
					}
					else
					{
						$n = $n;
						//do nothing
					}
				}
				// $n is calclated
				if ($obdManifoldPressure <= $obdBarometricPressure)
				{
					$n = $n;
				} 
				else
				{
					if(MAP_Type_C == "Absolute")
					{
						if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 125)
						{
							$n = $n + 0.01; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
						}
						else 
						{				
							if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
							{
								if(125 < $obdManifoldPressure && $obdManifoldPressure <= 175)
								{
									$n = $n + 0.03;
								}
								elseif(175 < $obdManifoldPressure && $obdManifoldPressure <= 220)
								{
									$n = $n + 0.04;
								}
								elseif($obdManifoldPressure > 220)
								{
									$n = $n + 0.05;
								}
							}
							else 
							{
								if(125 < $obdManifoldPressure && $obdManifoldPressure <= 175)
								{
									$n = $n + 0.02;
								}
								elseif(175 < $obdManifoldPressure && $obdManifoldPressure <= 220)
								{
									$n = $n + 0.03;
								}
								elseif ($obdManifoldPressure > 220)
								{
									$n = $n + 0.04;
								}
							}
						}
					}
					else
					{
						if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 100)
						{
							$n = $n + 0.01; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
						}
						else
						{
							if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
							{
								if(100 < $obdManifoldPressure && $obdManifoldPressure <= 120)
								{
									$n = $n + 0.03;
								}
								elseif(120 < $obdManifoldPressure && $obdManifoldPressure <= 140)
								{
									$n = $n + 0.04;
								}
								elseif($obdManifoldPressure > 140)
								{
									$n = $n + 0.05;
								}
							}
							else 
							{
								if(100 < $obdManifoldPressure && $obdManifoldPressure <= 120)
								{
									$n = $n + 0.02;
								}
								elseif(120 < $obdManifoldPressure && $obdManifoldPressure <= 140)
								{
									$n = $n + 0.03;
								}
								elseif ($obdManifoldPressure > 140)
								{
									$n = $n + 0.04;
								}
							}
						}
					}
				}
				if($obdRpm > $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1200
				{
					if($obdRpm < $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.01; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm > $RPM_Limit2)
					{
						if($obdRpm < $RPM_Limit3) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
						{
							$n = $n - 0.02;
						}
						else
						{
							$n = $n - 0.03;
						}
					}
				}
			}

			//Changing Efficiencies for Petrol Engines
			
			elseif($vType == 1 && $Turbo_Avail_C == "No")
			{
				//CHECKS FOR ENGINE RPM CONDITION
				if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
				{
					$n = $n;
				}
				elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
				{
					$n = $n + 0.01;
				}
				elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
				{
					$n = $n + 0.02;
				}
				elseif($obdRpm < 5500)
				{
					$n = $n + 0.01;
				}
				else
				{
					$n = $n;
				}
				
				//CHECKS FOR ENGINE LOAD CONDITION
				
				if($obdEngineLoad < 20)
				{
					$n = $n - 0.02;
				}
				elseif($obdEngineLoad < 45)
				{
					$n = $n - 0.01;
				}
				elseif($obdEngineLoad < 65)
				{
					$n = $n;
				}
				elseif($obdEngineLoad < 85)
				{
					$n = $n + 0.01;
				}
				else
				{
					$n = $n + 0.02;
				}
			}
			
			//CHECKS FOR COOLANT TEMPERATURE CONDITION FOR BOTH PETROL AND DIESEL
			if($obdCoolantTemperature < 65)
			{
				$n = $n - 0.01;
			}
			else 
			{
				$n = $n;
			}
			
			return $n;
	}
*/

	function updateDistMil($devId)
	{
		$dt = date('Y-m-d 00:00:00');
		$sql = "select * from device_data where device_id ='$devId' and time_v >= '$dt' limit 0,1";
		//time_v
		$dtExist = mysql_num_rows(mysql_query($sql));
		$this -> dev = $devId;
		$distTr = 0;
		$totMil = 0;
		$dtN = date('Y-m-d');
		//$sqlN = "select * from device_dataday where dt = '$dtN' limit 0,1";
		$sqlN = "select * from device_dataday where dt = '$dtN' and device_id ='$devId' limit 0,1";
		
		$data = $this -> select($sqlN);
		$postVal = $data[0];
		if($dtExist >= 1)
		{
			$distTr = $this -> totSpeedLastAvg(1,'Fdist',1);
			$totMil = $this -> totSpeedLastAvg(1,'Fmil',1);
		}		
		
		if($postVal['id'] == "" || $postVal['id'] <= 0)
		{
			$postVal['dist'] = $distTr;
			$postVal['mil'] = $totMil;
			$postVal['dt'] = $dtN;
			$postVal['device_id'] = $devId;
			$saveData = new saveForm('device_dataday',$postVal);
			$saveData->addData();
		}
		else
		{
			$postVal['dist'] += $distTr;
			if(($totMil > 0))
			{
				$postVal['mil'] .= ",".$totMil;
			}
			
			$mil = explode(',',$postVal['mil']);
			$mCount = count($mil)-1;
			$avg = ($mCount >= 1) ? (array_sum($mil)/$mCount) : $totMil;
			
			$postVal['milavg'] = $avg;
			
			$saveData = new saveForm('device_dataday',$postVal);
			$saveData->updateData('id','id');
		}
		unset($data);
		unset($postVal);
	}
		
	function devFlowInfo($devId,$row,$val="",$typVal="",$fspLast="")
	{
		//print_r($row);
		//echo "<br/>=================================<br/>";
		$datAct = new datAct('device1');
		$flist = array('obdEngineLoad','obdAirFlowRate');
		$dev = $this->getdevice($devId);
		$vehi = $this->getvehicle($devId);
		$f1 = $datAct->valuefield('obdEngineLoad');
		$f2 = $datAct->valuefield('obdAirFlowRate');
		$f3 = $datAct->valuefield('obdRpm');
		$f4 = $datAct->valuefield('obdManifoldPressure');
		$f5 = $datAct->valuefield('obdIntakeTemperature');
		$f6 = $datAct->valuefield('obdActualEquivalenceRatio');
		$f7 = $datAct->valuefield('obdSpeed');
		$f8 = $datAct->valuefield('obdCalcBarometricPressure');
		$f9 = $datAct->valuefield('obdCalcEngineState'); 
		$f10 = $datAct->valuefield('obdEngineState');
		$f11 = $datAct->valuefield('obdCommandedEGR');
		$f12 = $datAct->valuefield('obdCoolantTemperature');
		
		$fSys = $datAct->valuefield('obdFuelSystemStatus');
		$fPst = $datAct->valuefield('corePowerState');
		
		$distF = $datAct->valuefield('obdDistanceTravelled');
		$distF = $distF[0];
		
		$eSpe = $f7[0];
		$eRpm = $f3[0];
		$time = strtotime($row['time_v']);
		$engStat = $f9[0];
		//$row[$engStat] = $row[$f10[0]];
		if($val == 1)
		{
			$engLoadRpm = $row[$eRpm];
			$powerState = $row[$fPst[0]];
			$obdSpeed = $row[$eSpe];
			$engLoadVal = $row[$f1[0]];
			$flSys = $row[$fSys[0]];
			$row[$engStat] = $this -> calcEngState($engStat,$f10[0],$row[$f10[0]],$devId,$engLoadRpm,$powerState,$obdSpeed,$vehi[0]['fuel_tpe'],$flSys,$engLoadVal);
			$row[$eRpm] = ($row[$engStat] == 6) ? 0 : $row[$eRpm];
		}
		else
		{
			$row[$engStat] = $row[$f10[0]];
			//$row[$engStat] = $this -> calcEngState($engStat,$f10[0],$row[$f10[0]],$devId);
		}
		
		$row['Fsprint'] = ($row['Fsprint'] <= 0) ? $time : $row['Fsprint'];
		// Distance calculation
		if(($typVal <= 0) && ($this -> sprintDelay($eRpm,$engStat,$row['time_v'],$devId))) // && $typVal == 1
		{
			$this -> updateDistMil($devId);
			$dtDrive = date('Y-m-d',strtotime($row['time_v']));
			//$this -> callDriveUpdate($row['Fsprint'],$devId,$dtDrive,$row); commented on 13 Jul
			
			$row['Fsprint'] = $time;
			$row['Fdist'] = 0;
			//13/03/2018: Take values diretly from app. Do not overwrite.
			// $row['Fdist'] = $row[$distF];
		}
		elseif(($typVal >= 1) && ($this -> sprintDelayMain($row,$fspLast,$engStat)))
		{
			$this -> updateDistMil($devId);
			$dtDrive = date('Y-m-d',strtotime($row['time_v']));
			//$this -> callDriveUpdate($row['Fsprint'],$devId,$dtDrive,$row); commented on 13 Jul
			
			$row['Fsprint'] = $time;
			$row['Fdist'] = 0;
			//13/03/2018: Take values diretly from app. Do not overwrite.
			// $row['Fdist'] = $row[$distF];
		}
		else
		{
			$timedif = 10;		//Value hardcoded as the raster for this device is 10 sec. TODO: to be removed.
			//$timedif = $row['time_s'] - $row['time_sold'];
			//Distance Travelled (km) = Previous Distance (km) + (Velocity (km/hr) * (Current Time Stamp (s) – Previous Time Stamp (s)) /3600
			//$timedif /= 10000;
			$row['Fdist'] += (($row[$eSpe] * $timedif) / 3600);
			// if(($val == 1))
			// {
				// $row['Fdist'] += (($row[$eSpe] * $timedif) / 3600000);
			// }
			// else
			// {
				// $row['Fdist'] = $row[$distF];
			// }
			$dtDrive = date('Y-m-d',strtotime($row['time_v']));
			
			//$this -> callDriveUpdate($row['Fsprint'],$devId,$dtDrive);
			//echo " (($row[$eSpe] * $timedif) / 3600) <br/>";
			//echo $row['Fdist'];
		}
		// Distance calculation End
		//print_r($vehi);
		
		$vType = $vehi[0]['fuel_tpe'];
		//$ve = $vehi[0]['ve'];
		//$ed = $vehi[0]['ed'];
		$ed = $this -> getCaliPramVal('Eng_Disp_C',$devId);
		
		$eLoad = $row[$f1[0]];		
		$maf = $row[$f2[0]];
		$rpm = $row[$f3[0]];
		$map = $row[$f4[0]];
		$vehspd = $row[$f7[0]];
		$baro_value = $row[$f8[0]];
		$inTakeTemp = $row[$f5[0]]+273;
		$obdIntakeTemperature = $row[$f5[0]];
		$ratio = $row[$f6[0]];
		$obdCommandedEGR = $row[$f11[0]];
		$obdManifoldPressure = $map;
		$obdRpm = $rpm;
		$obdCoolantTemperature = $row[$f12[0]];
		$obdEngineLoad = $eLoad;
		
		$pi =  3.14159;
		$mm = 28.97;
		$r = 8.314;
		
		if($baro_value == 0 || $baro_value == "")
		{
			$obdBarometricPressure = $this -> cityPres($devId);
		}
		else
		{
			$obdBarometricPressure = $baro_value;
		}
		
		//*****************==================Volumetric Efficiency calculation calibration labels start============================*****************//
	 	$Turbo_Avail_C = $this -> getCaliPramVal('TC_Status_C',$devId);
		$Injection_Type_C = $this -> getCaliPramVal('Injection_Type_C',$devId);
		$MAP_Type_C = $this -> getCaliPramVal('MAP_Type_C',$devId);
		$intercoolerPresent = $this -> getCaliPramVal('IC_Status_C',$devId);
		$VVT_Status_C = $this -> getCaliPramVal('VVT_Status_C',$devId);
		$Veh_Type_C = $this -> getCaliPramVal('Veh_Type_C',$devId);
		$RPM_Limit1 = $this -> getCaliPramVal('Eff_RPM_Limit1_C',$devId);
		$RPM_Limit2 = $this -> getCaliPramVal('Eff_RPM_Limit2_C',$devId);
		$RPM_Limit3 = $this -> getCaliPramVal('Eff_RPM_Limit3_C',$devId);
		$RPM_Limit4 = $this -> getCaliPramVal('Eff_RPM_Limit4_C',$devId);
		$RPM_Limit5 = $this -> getCaliPramVal('Eff_RPM_Limit5_C',$devId);
		//Veh_type_C == 0 => Cars; Veh_type_C == 1 => Bikes; Veh_type_C == 2 => Trucks
		
		/*============================================== To be deleted as new labels below ================================================*/
		$VolEff_Eload_1_10_C = $this -> getCaliPramVal('VolEff_Eload_1_10_C',$devId);
		$VolEff_Eload_11_20_C = $this -> getCaliPramVal('VolEff_Eload_11_20_C',$devId);
		$VolEff_Eload_21_30_C = $this -> getCaliPramVal('VolEff_Eload_21_30_C',$devId);
		$VolEff_Eload_31_40_C = $this -> getCaliPramVal('VolEff_Eload_31_40_C',$devId);
		$VolEff_Eload_41_50_C = $this -> getCaliPramVal('VolEff_Eload_41_50_C',$devId);
		$VolEff_Eload_51_60_C = $this -> getCaliPramVal('VolEff_Eload_51_60_C',$devId);
		$VolEff_Eload_61_80_C = $this -> getCaliPramVal('VolEff_Eload_61_80_C',$devId);
		$VolEff_Eload_81_90_C = $this -> getCaliPramVal('VolEff_Eload_81_90_C',$devId);
		$VolEff_Eload_91_94_C = $this -> getCaliPramVal('VolEff_Eload_91_94_C',$devId);
		$VolEff_Eload_95_98_C = $this -> getCaliPramVal('VolEff_Eload_95_98_C',$devId);
		$VolEff_Eload_99_100_C = $this -> getCaliPramVal('VolEff_Eload_99_100_C',$devId);
		/*============================================== To be deleted as new labels below ================================================*/
		
		$VolEff_Eload_1_10_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_1_10_RPMLimit1_C',$devId);
		$VolEff_Eload_1_10_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_1_10_RPMLimit2_C',$devId);
		$VolEff_Eload_1_10_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_1_10_RPMLimit3_C',$devId);
		$VolEff_Eload_1_10_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_1_10_RPMLimit4_C',$devId);
		$VolEff_Eload_1_10_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_1_10_RPMLimit5_C',$devId);
		$VolEff_Eload_11_20_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_11_20_RPMLimit1_C',$devId);
		$VolEff_Eload_11_20_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_11_20_RPMLimit2_C',$devId);
		$VolEff_Eload_11_20_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_11_20_RPMLimit3_C',$devId);
		$VolEff_Eload_11_20_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_11_20_RPMLimit4_C',$devId);
		$VolEff_Eload_11_20_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_11_20_RPMLimit5_C',$devId);
		$VolEff_Eload_21_30_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_21_30_RPMLimit1_C',$devId);
		$VolEff_Eload_21_30_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_21_30_RPMLimit2_C',$devId);
		$VolEff_Eload_21_30_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_21_30_RPMLimit3_C',$devId);
		$VolEff_Eload_21_30_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_21_30_RPMLimit4_C',$devId);
		$VolEff_Eload_21_30_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_21_30_RPMLimit5_C',$devId);
		$VolEff_Eload_31_40_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_31_40_RPMLimit1_C',$devId);
		$VolEff_Eload_31_40_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_31_40_RPMLimit2_C',$devId);
		$VolEff_Eload_31_40_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_31_40_RPMLimit3_C',$devId);
		$VolEff_Eload_31_40_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_31_40_RPMLimit4_C',$devId);
		$VolEff_Eload_31_40_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_31_40_RPMLimit5_C',$devId);
		$VolEff_Eload_41_50_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_41_50_RPMLimit1_C',$devId);
		$VolEff_Eload_41_50_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_41_50_RPMLimit2_C',$devId);
		$VolEff_Eload_41_50_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_41_50_RPMLimit3_C',$devId);
		$VolEff_Eload_41_50_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_41_50_RPMLimit4_C',$devId);
		$VolEff_Eload_41_50_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_41_50_RPMLimit5_C',$devId);
		$VolEff_Eload_51_60_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_51_60_RPMLimit1_C',$devId);
		$VolEff_Eload_51_60_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_51_60_RPMLimit2_C',$devId);
		$VolEff_Eload_51_60_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_51_60_RPMLimit3_C',$devId);
		$VolEff_Eload_51_60_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_51_60_RPMLimit4_C',$devId);
		$VolEff_Eload_51_60_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_51_60_RPMLimit5_C',$devId);
		$VolEff_Eload_61_80_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_61_80_RPMLimit1_C',$devId);
		$VolEff_Eload_61_80_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_61_80_RPMLimit2_C',$devId);
		$VolEff_Eload_61_80_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_61_80_RPMLimit3_C',$devId);
		$VolEff_Eload_61_80_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_61_80_RPMLimit4_C',$devId);
		$VolEff_Eload_61_80_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_61_80_RPMLimit5_C',$devId);
		$VolEff_Eload_81_90_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_81_90_RPMLimit1_C',$devId);
		$VolEff_Eload_81_90_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_81_90_RPMLimit2_C',$devId);
		$VolEff_Eload_81_90_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_81_90_RPMLimit3_C',$devId);
		$VolEff_Eload_81_90_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_81_90_RPMLimit4_C',$devId);
		$VolEff_Eload_81_90_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_81_90_RPMLimit5_C',$devId);
		$VolEff_Eload_91_94_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_91_94_RPMLimit1_C',$devId);
		$VolEff_Eload_91_94_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_91_94_RPMLimit2_C',$devId);
		$VolEff_Eload_91_94_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_91_94_RPMLimit3_C',$devId);
		$VolEff_Eload_91_94_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_91_94_RPMLimit4_C',$devId);
		$VolEff_Eload_91_94_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_91_94_RPMLimit5_C',$devId);
		$VolEff_Eload_95_98_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_95_98_RPMLimit1_C',$devId);
		$VolEff_Eload_95_98_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_95_98_RPMLimit2_C',$devId);
		$VolEff_Eload_95_98_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_95_98_RPMLimit3_C',$devId);
		$VolEff_Eload_95_98_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_95_98_RPMLimit4_C',$devId);
		$VolEff_Eload_95_98_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_95_98_RPMLimit5_C',$devId);
		$VolEff_Eload_99_100_RPMLimit1_C = $this -> getCaliPramVal('VolEff_Eload_99_100_RPMLimit1_C',$devId);
		$VolEff_Eload_99_100_RPMLimit2_C = $this -> getCaliPramVal('VolEff_Eload_99_100_RPMLimit2_C',$devId);
		$VolEff_Eload_99_100_RPMLimit3_C = $this -> getCaliPramVal('VolEff_Eload_99_100_RPMLimit3_C',$devId);
		$VolEff_Eload_99_100_RPMLimit4_C = $this -> getCaliPramVal('VolEff_Eload_99_100_RPMLimit4_C',$devId);
		$VolEff_Eload_99_100_RPMLimit5_C = $this -> getCaliPramVal('VolEff_Eload_99_100_RPMLimit5_C',$devId);
		
		//********************===================Volumetric Efficiency Calculations calibration labels  End===================******************//
		
		//===========================To get whether MAF and MAP is supported in the car.=================================//
			$MAP_Valid_C = $this -> getCaliPramVal('MAP_Valid_C',$devId);
			$MAF_Valid_C = $this -> getCaliPramVal('MAF_Valid_C',$devId);
		
		//********************----------------New MAF to MAP Formula Start--------------------******************//
			if($MAP_Valid_C == 1)
			{
				if($obdManifoldPressure <= 0 || $obdManifoldPressure == "")
				{
					if($maf > 0 && $MAF_Valid_C == 1)
					{
						//*****************==================Volumetric Efficiency calculation start============================*****************//
						//Diesel Engine Volumetric Efficiency
						if($vType == 2)
						{
							if($MAP_Type_C == "Differential")
							{
							 	if($eLoad <= 55)
								{
									$fact11 = pow($eLoad, 4);
									$fact21 = pow($eLoad, 3);
									$fact31 = pow($eLoad, 2);
									$fact41 = $eLoad;
									
									$fact1 = (0.000004 * $fact11);
									$fact2 = (0.0021 * $fact21);
									$fact3 = (0.1497 * $fact31);
									$fact4 = (1.9792 * $fact41);
									
									$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
									//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
								}
								elseif($eLoad <= 85)
								{
									$ve = 98 + (0.08 * $eLoad);
								}
								elseif($eLoad <= 98)
								{
									$ve = 99 + (0.05 * $eLoad);
								}
								else
								{
									$ve = 99 + (0.04 * $eLoad);
								}
							}
							else
							{							
								//This condition is added so that the calculation is proper even when IAT is not read/supported
								if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
								{
									$obdIntakeTemperature = $obdCoolantTemperature;
								}
								else
								{
									//Do nothing
								}
								$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
								$barofactor = (101.3 / $obdBarometricPressure);
				
								if($row[$engStat] == 2)
								{
									if($obdIntakeTemperature < 20)
									{
										$ve1 = (34.4803 * $factor) - 0.2799;
										$ve = $ve1 * $barofactor;
										$ve = round($ve,0);
									}
									else
									{
										$ve1 = (34.6803 * $factor) - 0.2799;
										$ve = $ve1 * $barofactor;
										$ve = round($ve,0);
									}
								}
								else
								{
									if($vehspd > 20)
									{
										$ve = (-0.55 * $factor * $factor) + (11.35 * $factor) + 44.398;
										$ve = round($ve,0);
									}
									else
									{
										$ve = (-0.65 * $factor * $factor) + (11.35 * $factor) + 38.398;
										$ve = round($ve,0);
									}
								}
							}
						}
						
						//Petrol Engine Volumetric Efficiency
						else
						{
							if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
							{
								$obdIntakeTemperature = $obdCoolantTemperature;
							}
							else
							{
								//Do nothing
							}
							$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
							$barofactor = (101.3 / $obdBarometricPressure);
				
							if($row[$engStat] == 2)
							{
								if($obdIntakeTemperature < 20)
								{
									$ve1 = (91.9803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
								else
								{
									$ve1 = (92.1803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
							}
							else
							{
								if($vehspd > 20)
								{
									$ve = (70.03 * $factor * $factor) + (-328.78 * $factor) + 475.32;
									$ve = round($ve,0);
								}
								else
								{
									$ve = (70.03 * $factor * $factor) + (-328.78 * $factor) + 375.32;
									$ve = round($ve,0);
								}
							}
						}
						//********************===================Volumetric Efficiency Calculations End===================******************//
						
					 	$CurHmdty = 50;
						$CurHumiper = $CurHmdty/100;
						
						$num = (7.5 * $obdIntakeTemperature);
						$denom = ($obdIntakeTemperature + 237.3);
						$divFact = $num/$denom;
						$fact = pow(10, $divFact);
						$pv = $CurHumiper * ((6.1078 * $fact)/10);
		            	
			 			//	Density Calculation
			            $density = $maf * (100 * 120) / ($rpm * $ve * $ed * 1000);
			 			
						$MAPfact = ($density * (8.314 * ($obdIntakeTemperature + 273))) - ($pv * 0.018016);
					 	$pd = $MAPfact / 0.028964;
					 	$map = $pd + $pv;
						
						//echo "CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | density = $density | MAPfact = $MAPfact | pd = $pd | map = $map |";
						$map = round($map,0);
												
						if($row[$engStat] == 4)
						{
							$map = "";
						}
						elseif($rpm <= 500)
						{
							$map = $obdBarometricPressure;
						}
						elseif(($map < 0) || (($map > $obdBarometricPressure) && ($vType == 1)))
						{
							$map = $obdBarometricPressure;
						}
						else
						{
							//Do nothing;
						}
					}
					else
					{
						//do nothing;	
					}
				}
				else
				{
					//do nothing;	
				}
				$row[$f4[0]] = $map;
			}
			else
			{
				if($maf > 0 && $MAF_Valid_C == 1)
				{
					//*****************==================Volumetric Efficiency calculation start============================*****************//
					//Diesel Engine Volumetric Efficiency
					if($vType == 2)
					{
						if($MAP_Type_C == "Differential")
						{
						 	if($eLoad <= 55)
							{
								$fact11 = pow($eLoad, 4);
								$fact21 = pow($eLoad, 3);
								$fact31 = pow($eLoad, 2);
								$fact41 = $eLoad;
								
								$fact1 = (0.000004 * $fact11);
								$fact2 = (0.0021 * $fact21);
								$fact3 = (0.1497 * $fact31);
								$fact4 = (1.9792 * $fact41);
								
								$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
								//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
							}
							elseif($eLoad <= 85)
							{
								$ve = 98 + (0.08 * $eLoad);
							}
							elseif($eLoad <= 98)
							{
								$ve = 99 + (0.05 * $eLoad);
							}
							else
							{
								$ve = 99 + (0.04 * $eLoad);
							}
						}
						else
						{							
							//This condition is added so that the calculation is proper even when IAT is not read/supported
							if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
							{
								$obdIntakeTemperature = $obdCoolantTemperature;
							}
							else
							{
								//Do nothing
							}
							$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
							$barofactor = (101.3 / $obdBarometricPressure);
			
							if($row[$engStat] == 2)
							{
								if($obdIntakeTemperature < 20)
								{
									$ve1 = (34.4803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
								else
								{
									$ve1 = (34.6803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
							}
							elseif($row[$engStat] == 4)
							{
								$power = pow(10, -11);
								$ve = (-3 * $power * $factor * $factor) + (11.35 * $factor) + 27.398;
								$ve = round($ve,0);
							}
							else
							{
								if($vehspd > 20)
								{
									// $power = pow(10, -11);
									$ve = (-0.55 * $factor * $factor) + (11.35 * $factor) + 44.398;
									$ve = round($ve,0);
								}
								else
								{
									// $power = pow(10, -11);
									$ve = (-0.65 * $factor * $factor) + (11.35 * $factor) + 38.398;
									$ve = round($ve,0);
								}
							}
						}
					}
			
					//Petrol Engine Volumetric Efficiency
					else
					{
						if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
						{
							$obdIntakeTemperature = $obdCoolantTemperature;
						}
						else
						{
							//Do nothing
						}
						$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
						$barofactor = (101.3 / $obdBarometricPressure);
			
						if($row[$engStat] == 2)
						{
							if($obdIntakeTemperature < 20)
							{
								$ve1 = (91.9803 * $factor) - 0.2799;
								$ve = $ve1 * $barofactor;
								$ve = round($ve,0);
							}
							else
							{
								$ve1 = (92.1803 * $factor) - 0.2799;
								$ve = $ve1 * $barofactor;
								$ve = round($ve,0);
							}
						}
						else
						{
							if($vehspd > 20)
							{
								// $power = pow(10, -11);
								$ve = (70.03 * $factor * $factor) + (-328.78 * $factor) + 475.32;
								$ve = round($ve,0);
							}
							else
							{
								// $power = pow(10, -11);
								$ve = (70.03 * $factor * $factor) + (-328.78 * $factor) + 375.32;
								$ve = round($ve,0);
							}
						}
					}
					//********************===================Volumetric Efficiency Calculations End===================******************//
					
				 	$CurHmdty = 50;
					$CurHumiper = $CurHmdty/100;
					
					$num = (7.5 * $obdIntakeTemperature);
					$denom = ($obdIntakeTemperature + 237.3);
					$divFact = $num/$denom;
					$fact = pow(10, $divFact);
					$pv = $CurHumiper * ((6.1078 * $fact)/10);
	            	
		 			//	Density Calculation
		            $density = $maf * (100 * 120)/ ($rpm * $ve * $ed * 1000);
		 			
					$MAPfact = ($density * (8.314 * ($obdIntakeTemperature + 273))) - ($pv * 0.018016);
				 	$pd = $MAPfact / 0.028964;
				 	$map = $pd + $pv;
					
					//echo "CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | density = $density | MAPfact = $MAPfact | pd = $pd | map = $map |";
					$map = round($map,0);
					
					if($row[$engStat] == 4)
					{
						$map = "";
					}
					elseif($rpm <= 500)
					{
						$map = $obdBarometricPressure;
					}
					elseif(($map < 0) || (($map > $obdBarometricPressure) && ($vType == 1)))
					{
						$map = $obdBarometricPressure;
					}
					else
					{
						//Do nothing;
					}
				}
				else
				{
					//do nothing;	
				}
				$row[$f4[0]] = $map;
			}	
			//********************----------------New MAF to MAP Formula End--------------------*******************//
			
			//********************----------------New MAP to MAF Formula Start--------------------*******************//
			if($MAF_Valid_C == 1)
			{
				if($maf <= 0 || $maf == "")
				{
					if($obdManifoldPressure > 0 && $MAP_Valid_C == 1)
					{
						//*****************==================Volumetric Efficiency calculation start============================*****************//
						//Diesel Engine Volumetric Efficiency
						if($vType == 2)
						{
							if($MAP_Type_C == "Differential")
							{
							 	if($eLoad <= 55)
								{
									$fact11 = pow($eLoad, 4);
									$fact21 = pow($eLoad, 3);
									$fact31 = pow($eLoad, 2);
									$fact41 = $eLoad;
									
									$fact1 = (0.000004 * $fact11);
									$fact2 = (0.0021 * $fact21);
									$fact3 = (0.1497 * $fact31);
									$fact4 = (1.9792 * $fact41);
									
									$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
									//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
								}
								elseif($eLoad <= 85)
								{
									$ve = 98 + (0.08 * $eLoad);
								}
								elseif($eLoad <= 98)
								{
									$ve = 99 + (0.05 * $eLoad);
								}
								else
								{
									$ve = 99 + (0.04 * $eLoad);
								}
							}
							else
							{
								if($eLoad <= 35)
								{
									$ve = 92;
									if($obdIntakeTemperature > 50)
									{
										$ve = $ve + 5;
									}
									else
									{
										//do nothing;
									}
								}
								elseif($eLoad <= 55)
								{
									$fact11 = pow($eLoad, 4);
									$fact21 = pow($eLoad, 3);
									$fact31 = pow($eLoad, 2);
									$fact41 = $eLoad;
									
									$fact1 = (0.0000002 * $fact11);
									$fact2 = (0.00007 * $fact21);
									$fact3 = (0.0082 * $fact31);
									$fact4 = (0.3661 * $fact41);
									
									$ve = -$fact1 + $fact2 - $fact3 + $fact4 + 86.679;
									//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
								}
								elseif($eLoad <= 85)
								{
									$ve = 92 + (0.06 * $eLoad);
								}
								elseif($eLoad <= 98)
								{
									$ve = 94 + (0.05 * $eLoad);
								}
								else
								{
									$ve = 94 + (0.025 * $eLoad);
								}
								
								//Logic added for cars with no intercooler. VE reduces if intercooler not present.	Added on 25/05/2017.
								if($intercoolerPresent == "0")
								{
									if($eLoad <= 25)
									{
										if($vehspd < 10)
										{
											$ve = $ve - 36.5;
										}
										else
										{
											$ve = $ve;
										}
									}
									elseif($eLoad <= 35)
									{
										$ve = $ve - 28;
									}
									elseif($eLoad <= 55)
									{
										$ve = $ve - 20;
									}
									elseif($eLoad <= 85)
									{
										$ve = $ve - 15;
									}
									elseif($eLoad <= 98)
									{
										$ve = $ve - 9;
									}
									else
									{
										$ve = $ve - 2;
									}
								}
								else
								{
									//do nothing
								}
							}
						}

						//Petrol Engine Volumetric Efficiency
						else
						{
							if($Veh_Type_C == 0)
							{
								if($eLoad >= 1 &&  $eLoad <= 10)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_1_10_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_1_10_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_1_10_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_1_10_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_1_10_C;
									}
									else
									{
										$ve = $VolEff_Eload_1_10_C;
									}
								}
								elseif($eLoad >= 11 &&  $eLoad <= 20)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_11_20_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_11_20_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_11_20_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_11_20_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_11_20_C;
									}
									else
									{
										$ve = $VolEff_Eload_11_20_C;
									}
								}
								elseif($eLoad >= 21 &&  $eLoad <= 30)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_21_30_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_21_30_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_21_30_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_21_30_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_21_30_C;
									}
									else
									{
										$ve = $VolEff_Eload_21_30_C;
									}
								}
								elseif($eLoad >= 31 &&  $eLoad <= 40)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_31_40_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_31_40_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_31_40_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_31_40_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_31_40_C;
									}
									else
									{
										$ve = $VolEff_Eload_31_40_C;
									}
								}
								elseif($eLoad >= 41 &&  $eLoad <= 50)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_41_50_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_41_50_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_41_50_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_41_50_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_41_50_C;
									}
									else
									{
										$ve = $VolEff_Eload_41_50_C;
									}
								}
								elseif($eLoad >= 51 &&  $eLoad <= 60)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_51_60_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_51_60_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_51_60_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_51_60_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_51_60_C;
									}
									else
									{
										$ve = $VolEff_Eload_51_60_C;
									}
								}
								elseif($eLoad >= 61 &&  $eLoad <= 80)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_61_80_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_61_80_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_61_80_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_61_80_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_61_80_C;
									}
									else
									{
										$ve = $VolEff_Eload_61_80_C;
									}
								}
								elseif($eLoad >= 81 &&  $eLoad <= 90)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_81_90_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_81_90_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_81_90_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_81_90_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_81_90_C;
									}
									else
									{
										$ve = $VolEff_Eload_81_90_C;
									}
								}
								elseif($eLoad >= 91 &&  $eLoad <= 94)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_91_94_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_91_94_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_91_94_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_91_94_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_91_94_C;
									}
									else
									{
										$ve = $VolEff_Eload_91_94_C;
									}
								}
								elseif($eLoad >= 95 && $eLoad <= 98)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_95_98_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_95_98_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_95_98_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_95_98_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_95_98_C;
									}
									else
									{
										$ve = $VolEff_Eload_95_98_C;
									}
								}
								elseif($eLoad >= 99 &&  $eLoad <= 100)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_C;
									}
								}
								else
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_C;
									}
								}
							}
							elseif($Veh_Type_C == 1)
							{
								if($eLoad >= 1 &&  $eLoad <= 10)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_1_10_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 11 &&  $eLoad <= 20)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_11_20_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 21 &&  $eLoad <= 30)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_21_30_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 31 &&  $eLoad <= 40)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_31_40_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 41 &&  $eLoad <= 50)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_41_50_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 51 &&  $eLoad <= 60)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_51_60_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 61 &&  $eLoad <= 80)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_61_80_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 81 &&  $eLoad <= 90)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_81_90_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 91 &&  $eLoad <= 94)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_91_94_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 95 && $eLoad <= 98)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_95_98_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 99 &&  $eLoad <= 100)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
								}
								else
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
								}
							}
						}
						//********************===================Volumetric Efficiency Calculations End===================******************//
								
						//MAP to MAF old formula
						//$maf = ($inTakeTemp > 0 && $r > 0) ? ($rpm * $map * $ve * $ed * $mm) / ($inTakeTemp * 120 * 100 * $r) : 0;
						
						$CurHmdty = 50;
						$CurHumiper = $CurHmdty/100;
						if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
						{
							$obdIntakeTemperature = $obdCoolantTemperature;
						}
						else
						{
							//Do nothing
						}
						
						$num = (7.5 * $obdIntakeTemperature);
						$denom = ($obdIntakeTemperature + 237.3);
						$divFact = $num/$denom;
						$fact = pow(10, $divFact);
						$pv = $CurHumiper * ((6.1078 * $fact)/10);
		            	$pd = $map - $pv;
						
						//	Formula for density
			            $density = (($pd * 0.028964) + ($pv * 0.018016)) / (8.314 * ($obdIntakeTemperature + 273));
						
						//	MAF Calculation
			            $maf = ($rpm * $ve * $ed * $density * 1000) / (100 * 120);
						$maf = round($maf,3);
						//echo "ve = $ve | fact1 = $fact1 | CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | pd = $pd | density = $density | maf = $maf | \n";
						
						$row[$f2[0]] = $maf;
					}
					else
					{
						//do nothing;
					}
				}
				else
				{
					//do nothing;
				}
			}
			else
			{
				if($obdManifoldPressure > 0 && $MAP_Valid_C == 1)
				{
					//*****************==================Volumetric Efficiency calculation start============================*****************//
					//Diesel Engine Volumetric Efficiency
					if($vType == 2)
					{
						if($MAP_Type_C == "Differential")
						{
						 	if($eLoad <= 55)
							{
								$fact11 = pow($eLoad, 4);
								$fact21 = pow($eLoad, 3);
								$fact31 = pow($eLoad, 2);
								$fact41 = $eLoad;
								
								$fact1 = (0.000004 * $fact11);
								$fact2 = (0.0021 * $fact21);
								$fact3 = (0.1497 * $fact31);
								$fact4 = (1.9792 * $fact41);
								
								$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
								//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
							}
							elseif($eLoad <= 85)
							{
								$ve = 98 + (0.08 * $eLoad);
							}
							elseif($eLoad <= 98)
							{
								$ve = 99 + (0.05 * $eLoad);
							}
							else
							{
								$ve = 99 + (0.04 * $eLoad);
							}
						}
						else
						{
							if($eLoad <= 35)
							{
								$ve = 92;
								if($obdIntakeTemperature > 50)
								{
									$ve = $ve + 5;
								}
								else
								{
									//do nothing;
								}
							}
							elseif($eLoad <= 55)
							{
								$fact11 = pow($eLoad, 4);
								$fact21 = pow($eLoad, 3);
								$fact31 = pow($eLoad, 2);
								$fact41 = $eLoad;
								
								$fact1 = (0.0000002 * $fact11);
								$fact2 = (0.00007 * $fact21);
								$fact3 = (0.0082 * $fact31);
								$fact4 = (0.3661 * $fact41);
								
								$ve = -$fact1 + $fact2 - $fact3 + $fact4 + 86.679;
								//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
							}
							elseif($eLoad <= 85)
							{
								$ve = 92 + (0.06 * $eLoad);
							}
							elseif($eLoad <= 98)
							{
								$ve = 94 + (0.05 * $eLoad);
							}
							else
							{
								$ve = 94 + (0.025 * $eLoad);
							}
							
							//Logic added for cars with no intercooler. VE reduces if intercooler not present.	Added on 25/05/2017.
							if($intercoolerPresent == "0")
							{
								if($eLoad <= 25)
								{
									if($vehspd < 10)
									{
										$ve = $ve - 36.5;
									}
									else
									{
										$ve = $ve;
									}
								}
								elseif($eLoad <= 35)
								{
									$ve = $ve - 28;
								}
								elseif($eLoad <= 55)
								{
									$ve = $ve - 20;
								}
								elseif($eLoad <= 85)
								{
									$ve = $ve - 15;
								}
								elseif($eLoad <= 98)
								{
									$ve = $ve - 9;
								}
								else
								{
									$ve = $ve - 2;
								}
							}
							else
							{
								//do nothing
							}
						}
					}
			
					//Petrol Engine Volumetric Efficiency
					else
					{
						if($Veh_Type_C == 0)
						{
							if($eLoad >= 1 &&  $eLoad <= 10)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_1_10_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_1_10_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_1_10_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_1_10_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_1_10_C;
								}
								else
								{
									$ve = $VolEff_Eload_1_10_C;
								}
							}
							elseif($eLoad >= 11 &&  $eLoad <= 20)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_11_20_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_11_20_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_11_20_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_11_20_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_11_20_C;
								}
								else
								{
									$ve = $VolEff_Eload_11_20_C;
								}
							}
							elseif($eLoad >= 21 &&  $eLoad <= 30)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_21_30_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_21_30_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_21_30_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_21_30_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_21_30_C;
								}
								else
								{
									$ve = $VolEff_Eload_21_30_C;
								}
							}
							elseif($eLoad >= 31 &&  $eLoad <= 40)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_31_40_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_31_40_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_31_40_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_31_40_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_31_40_C;
								}
								else
								{
									$ve = $VolEff_Eload_31_40_C;
								}
							}
							elseif($eLoad >= 41 &&  $eLoad <= 50)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_41_50_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_41_50_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_41_50_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_41_50_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_41_50_C;
								}
								else
								{
									$ve = $VolEff_Eload_41_50_C;
								}
							}
							elseif($eLoad >= 51 &&  $eLoad <= 60)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_51_60_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_51_60_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_51_60_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_51_60_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_51_60_C;
								}
								else
								{
									$ve = $VolEff_Eload_51_60_C;
								}
							}
							elseif($eLoad >= 61 &&  $eLoad <= 80)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_61_80_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_61_80_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_61_80_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_61_80_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_61_80_C;
								}
								else
								{
									$ve = $VolEff_Eload_61_80_C;
								}
							}
							elseif($eLoad >= 81 &&  $eLoad <= 90)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_81_90_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_81_90_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_81_90_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_81_90_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_81_90_C;
								}
								else
								{
									$ve = $VolEff_Eload_81_90_C;
								}
							}
							elseif($eLoad >= 91 &&  $eLoad <= 94)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_91_94_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_91_94_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_91_94_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_91_94_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_91_94_C;
								}
								else
								{
									$ve = $VolEff_Eload_91_94_C;
								}
							}
							elseif($eLoad >= 95 && $eLoad <= 98)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_95_98_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_95_98_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_95_98_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_95_98_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_95_98_C;
								}
								else
								{
									$ve = $VolEff_Eload_95_98_C;
								}
							}
							elseif($eLoad >= 99 &&  $eLoad <= 100)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_99_100_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_99_100_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_99_100_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_99_100_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_99_100_C;
								}
								else
								{
									$ve = $VolEff_Eload_99_100_C;
								}
							}
							else
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_99_100_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_99_100_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_99_100_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_99_100_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_99_100_C;
								}
								else
								{
									$ve = $VolEff_Eload_99_100_C;
								}
							}
						}
						elseif($Veh_Type_C == 1)
						{
							if($eLoad >= 1 &&  $eLoad <= 10)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_1_10_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_1_10_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_1_10_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_1_10_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_1_10_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_1_10_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 11 &&  $eLoad <= 20)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_11_20_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_11_20_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_11_20_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_11_20_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_11_20_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_11_20_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 21 &&  $eLoad <= 30)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_21_30_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_21_30_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_21_30_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_21_30_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_21_30_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_21_30_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 31 &&  $eLoad <= 40)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_31_40_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_31_40_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_31_40_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_31_40_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_31_40_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_31_40_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 41 &&  $eLoad <= 50)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_41_50_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_41_50_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_41_50_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_41_50_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_41_50_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_41_50_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 51 &&  $eLoad <= 60)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_51_60_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_51_60_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_51_60_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_51_60_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_51_60_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_51_60_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 61 &&  $eLoad <= 80)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_61_80_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_61_80_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_61_80_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_61_80_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_61_80_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_61_80_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 81 &&  $eLoad <= 90)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_81_90_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_81_90_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_81_90_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_81_90_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_81_90_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_81_90_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 91 &&  $eLoad <= 94)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_91_94_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_91_94_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_91_94_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_91_94_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_91_94_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_91_94_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 95 && $eLoad <= 98)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_95_98_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_95_98_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_95_98_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_95_98_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_95_98_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_95_98_RPMLimit5_C;
								}
							}
							elseif($eLoad >= 99 &&  $eLoad <= 100)
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_99_100_RPMLimit5_C;
								}
							}
							else
							{
								if($obdRpm < $RPM_Limit1)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit1_C;
								}
								elseif($obdRpm <= $RPM_Limit2)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit2_C;
								}
								elseif($obdRpm <= $RPM_Limit3)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit3_C;
								}
								elseif($obdRpm <= $RPM_Limit4)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit4_C;
								}
								elseif($obdRpm <= $RPM_Limit5)
								{
									$ve = $VolEff_Eload_99_100_RPMLimit5_C;
								}
								else
								{
									$ve = $VolEff_Eload_99_100_RPMLimit5_C;
								}
							}
						}
					}
					//********************===================Volumetric Efficiency Calculations End===================******************//
					
					//MAP to MAF old formula	
					//$maf = ($inTakeTemp > 0 && $r > 0) ? ($rpm * $map * $ve * $ed * $mm) / ($inTakeTemp * 120 * 100 * $r) : 0;
					
					$CurHmdty = 50;
					$CurHumiper = $CurHmdty/100;
					if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
					{
						$obdIntakeTemperature = $obdCoolantTemperature;
					}
					else
					{
						//Do nothing
					}
					
					$num = (7.5 * $obdIntakeTemperature);
					$denom = ($obdIntakeTemperature + 237.3);
					$divFact = $num/$denom;
					$fact = pow(10, $divFact);
					$pv = $CurHumiper * ((6.1078 * $fact)/10);
	            	$pd = $map - $pv;
					
					//	Formula for density
		            $density = (($pd * 0.028964) + ($pv * 0.018016)) / (8.314 * ($obdIntakeTemperature + 273));
					
					//	MAF Calculation
		            $maf = ($rpm * $ve * $ed * $density * 1000) / (100 * 120);
					$maf = round($maf,3);
					//echo "ve = $ve | fact1 = $fact1 | CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | pd = $pd | density = $density | maf = $maf | \n";
					
					$row[$f2[0]] = $maf;
				}
				else
				{
					//do nothing;
				}
			}
			
		//********************----------------New MAP to MAF Formula End--------------------*******************/
		
		/************=========================Efficiency Calculation Start================================*********************/
		
			$veh = $this -> getvehicle($devId);
			/*$baseN = $veh[0]['nfact'] / 100;
			$n = $baseN;*/
			$baseN = $this -> getCaliPramVal('Eng_eff_C',$devId); 
			$baseN = $baseN / 100;
			$n = $baseN;
			$intercoolerPresent = $this -> getCaliPramVal('IC_Status_C',$devId);
			//$EGRCoolerPresent = $this -> getCaliPramVal('EgrC_Status_C',$devId);
			//$EGRCommandPresent = $this -> getCaliPramVal('Egr_Status_C',$devId); // added new
			// $RPM_Limit1 = $this -> getCaliPramVal('Eff_RPM_Limit1_C',$devId);
			// $RPM_Limit2 = $this -> getCaliPramVal('Eff_RPM_Limit2_C',$devId);
			// $RPM_Limit3 = $this -> getCaliPramVal('Eff_RPM_Limit3_C',$devId);
			// $RPM_Limit4 = $this -> getCaliPramVal('Eff_RPM_Limit4_C',$devId);
			// $RPM_Limit5 = $this -> getCaliPramVal('Eff_RPM_Limit5_C',$devId);
			
			$MAP_Type_C = $this -> getCaliPramVal('MAP_Type_C',$devId);
			$Turbo_Avail_C = $this -> getCaliPramVal('TC_Status_C',$devId);
			$Injection_Type_C = $this -> getCaliPramVal('Injection_Type_C',$devId);
			$VVT_Status_C = $this -> getCaliPramVal('VVT_Status_C',$devId);
			
			$obdManifoldPressure = $map; //so that $obdManifoldPressure has the new calculated value of MAP.
			
			//Changing Efficiencies for Diesel Engines
			if($vType == 2)//|| ($vType == 1 && $Turbo_Avail_C == "Yes"))
			{
				if($obdEngineLoad <= 15)
				{
					if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
					{
						$n = $n - 0.07;
					}
					elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.06; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.03; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.04;
					}
					elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.06;
					}
					else
					{
						$n = $n - 0.07;
					}
				}
				elseif($obdEngineLoad <= 35)
				{
					if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
					{
						$n = $n - 0.06;
					}
					elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.05; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.02; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.03;
					}
					elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.05;
					}
					else
					{
						$n = $n - 0.06;
					}
				}
				elseif($obdEngineLoad <= 55)
				{
					if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
					{
						$n = $n - 0.05;
					}
					elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.04; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.01; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.02;
					}
					elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.04;
					}
					else
					{
						$n = $n - 0.05;
					}
				}
				elseif($obdEngineLoad <= 75)
				{
					if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
					{
						$n = $n - 0.04;
					}
					elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.03; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.02;
					}
					elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.03;
					}
					else
					{
						$n = $n - 0.05;
					}
				}
				else
				{
					if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
					{
						$n = $n - 0.03;
					}
					elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.02; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.02;
					}
					elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.03;
					}
					else
					{
						$n = $n - 0.05;
					}
					//do nothing
				}

				// $n is calclated
				if ($obdManifoldPressure <= $obdBarometricPressure)
				{
					$n = $n;
				} 
				else
				{
					if($MAP_Type_C == "Absolute")
					{
						if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 140)
						{
							$n = $n + 0.01; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
						}
						else 
						{				
							if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
							{
								if(140 < $obdManifoldPressure && $obdManifoldPressure <= 175)
								{
									$n = $n + 0.03;
								}
								elseif(175 < $obdManifoldPressure && $obdManifoldPressure <= 200)
								{
									$n = $n + 0.04;
								}
								elseif(200 < $obdManifoldPressure && $obdManifoldPressure <= 210)
								{
									$n = $n + 0.05;
								}
								elseif(210 < $obdManifoldPressure && $obdManifoldPressure <= 225)
								{
									$n = $n + 0.06;
								}
								elseif(225 < $obdManifoldPressure && $obdManifoldPressure <= 250)
								{
									$n = $n + 0.07;
								}
								elseif($obdManifoldPressure > 250)
								{
									$n = $n + 0.08;
								}
							}
							else 
							{
								if(140 < $obdManifoldPressure && $obdManifoldPressure <= 175)
								{
									$n = $n + 0.02;
								}
								elseif(175 < $obdManifoldPressure && $obdManifoldPressure <= 200)
								{
									$n = $n + 0.03;
								}
								elseif(200 < $obdManifoldPressure && $obdManifoldPressure <= 210)
								{
									$n = $n + 0.04;
								}
								elseif(210 < $obdManifoldPressure && $obdManifoldPressure <= 225)
								{
									$n = $n + 0.05;
								}
								elseif(225 < $obdManifoldPressure && $obdManifoldPressure <= 250)
								{
									$n = $n + 0.06;
								}
								elseif($obdManifoldPressure > 250)
								{
									$n = $n + 0.07;
								}
							}
						}
					}
					else
					{
						if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 100)
						{
							$n = $n + 0.01; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
						}
						else
						{
							if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
							{
								if(100 < $obdManifoldPressure && $obdManifoldPressure <= 110)
								{
									$n = $n + 0.03;
								}
								elseif(110 < $obdManifoldPressure && $obdManifoldPressure <= 120)
								{
									$n = $n + 0.04;
								}
								elseif(120 < $obdManifoldPressure && $obdManifoldPressure <= 130)
								{
									$n = $n + 0.05;
								}
								elseif(130 < $obdManifoldPressure && $obdManifoldPressure <= 142)
								{
									$n = $n + 0.06;
								}
								elseif(142 < $obdManifoldPressure && $obdManifoldPressure <= 152)
								{
									$n = $n + 0.07;
								}
								elseif($obdManifoldPressure > 152)
								{
									$n = $n + 0.08;
								}
							}
							else 
							{
								if(100 < $obdManifoldPressure && $obdManifoldPressure <= 110)
								{
									$n = $n + 0.02;
								}
								elseif(110 < $obdManifoldPressure && $obdManifoldPressure <= 120)
								{
									$n = $n + 0.03;
								}
								elseif(120 < $obdManifoldPressure && $obdManifoldPressure <= 130)
								{
									$n = $n + 0.04;
								}
								elseif(130 < $obdManifoldPressure && $obdManifoldPressure <= 142)
								{
									$n = $n + 0.05;
								}
								elseif(142 < $obdManifoldPressure && $obdManifoldPressure <= 152)
								{
									$n = $n + 0.06;
								}
								elseif($obdManifoldPressure > 152)
								{
									$n = $n + 0.07;
								}
							}
						}
					}
				}
			}

			//Changing Efficiencies for Petrol Engines
			elseif($vType == 1)
			{
				if($Turbo_Avail_C == "No")
				{
					if($Injection_Type_C == "Direct") //efficiency of direct injection non turbocharged
					{
						$n = $n + 0.01;
					}
					elseif($Injection_Type_C == "Indirect") //efficiency of indirect injection non turbocharged
					{
						$n = $n;
					}
					
					//For cars
					if($Veh_Type_C == 0)
					{
						//Efficiency change with ENGINE RPM CONDITION and ENGINE LOAD CONDITION
						if($obdEngineLoad < 20)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.06;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
							{
								$n = $n - 0.05;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
							{
								$n = $n - 0.03;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
							{
								$n = $n - 0.01;
							}
							else
							{
								$n = $n - 0.02;
							}
						}
						elseif($obdEngineLoad < 45)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.05;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
							{
								$n = $n - 0.04;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
							{
								$n = $n - 0.02;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
							{
								$n = $n;
							}
							else
							{
								$n = $n - 0.01;
							}
						}
						elseif($obdEngineLoad < 65)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.04;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
							{
								$n = $n - 0.03;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
							{
								$n = $n - 0.01;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
							{
								$n = $n + 0.02;
							}
							else
							{
								$n = $n;
							}
						}
						elseif($obdEngineLoad < 85)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.03;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
							{
								$n = $n - 0.02;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
							{
								$n = $n;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
							{
								$n = $n + 0.03;
							}
							else
							{
								$n = $n + 0.01;
							}
						}
						else
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.02;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
							{
								$n = $n - 0.01;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
							{
								$n = $n + 0.01;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
							{
								if($VVT_Status_C == 0)
								{
									$n = $n + 0.04;
								}
								else
								{
									$n = $n + 0.06;
								}
							}
							else
							{
								$n = $n + 0.02;
							}
						}
					}
					elseif($Veh_Type_C == 1)		//----------------------for Bikes
					{
						if($obdEngineLoad < 20)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.06;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
							{
								$n = $n - 0.05;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
							{
								$n = $n - 0.02;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
							{
								$n = $n + 0.01;
							}
							else
							{
								$n = $n;
							}
						}
						elseif($obdEngineLoad < 45)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.05;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
							{
								$n = $n - 0.04;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
							{
								$n = $n - 0.01;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
							{
								$n = $n + 0.03;
							}
							else
							{
								$n = $n + 0.01;
							}
						}
						elseif($obdEngineLoad < 65)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.03;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
							{
								$n = $n - 0.02;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
							{
								$n = $n;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
							{
								$n = $n + 0.05;
							}
							else
							{
								$n = $n + 0.03;
							}
						}
						elseif($obdEngineLoad < 85)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.01;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
							{
								$n = $n;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
							{
								$n = $n + 0.02;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
							{
								$n = $n + 0.07;
							}
							else
							{
								$n = $n + 0.05;
							}
						}
						else
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 1400
							{
								$n = $n + 0.02;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 2000
							{
								$n = $n + 0.03;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 3000
							{
								$n = $n + 0.05;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 4500
							{
								$n = $n + 0.08;
							}
							else
							{
								$n = $n + 0.06;
							}
						}
					}
				}
				elseif($Turbo_Avail_C == "Yes")
				{
					if($Injection_Type_C == "Direct") //efficiency of direc injection turbocharged
					{
						$n = $n + 0.01;
					}
					elseif($Injection_Type_C == "Indirect") //efficiency of indirec injection turbocharged
					{
						$n = $n;
					}
					
					//Efficiency change with ENGINE RPM CONDITION and ENGINE LOAD CONDITION
					if($obdEngineLoad < 20)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.06;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.05;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n - 0.03;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n - 0.01;
						}
						else
						{
							$n = $n - 0.02;
						}
					}
					elseif($obdEngineLoad < 45)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.05;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.04;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n;
						}
						else
						{
							$n = $n - 0.01;
						}
					}
					elseif($obdEngineLoad < 65)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.04;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.03;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n - 0.01;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n + 0.02;
						}
						else
						{
							$n = $n;
						}
					}
					elseif($obdEngineLoad < 85)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.03;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n + 0.03;
						}
						else
						{
							$n = $n + 0.01;
						}
					}
					else
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 1400
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 2000
						{
							$n = $n - 0.01;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 3000
						{
							$n = $n + 0.01;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 4500
						{
							$n = $n + 0.04;
						}
						else
						{
							$n = $n + 0.02;
						}
					}
					
					// Efficiency change with Boost / MAP
					if ($obdManifoldPressure <= $obdBarometricPressure)
					{
						$n = $n;
					} 
					else
					{
						if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 125)
						{
							$n = $n + 0.005; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
						}
						else 
						{				
							if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
							{
								if(125 < $obdManifoldPressure && $obdManifoldPressure <= 150)
								{
									$n = $n + 0.015;
								}
								elseif(150 < $obdManifoldPressure && $obdManifoldPressure <= 180)
								{
									$n = $n + 0.025;
								}
								elseif($obdManifoldPressure > 180)
								{
									$n = $n + 0.03;
								}
							}
							else 
							{
								if(125 < $obdManifoldPressure && $obdManifoldPressure <= 150)
								{
									$n = $n + 0.01;
								}
								elseif(150 < $obdManifoldPressure && $obdManifoldPressure <= 180)
								{
									$n = $n + 0.02;
								}
								elseif ($obdManifoldPressure > 180)
								{
									$n = $n + 0.025;
								}
							}
						}
					}
				}
			}
			//CHECKS FOR COOLANT TEMPERATURE CONDITION FOR BOTH PETROL AND DIESEL
			if($obdCoolantTemperature < 65)
			{
				$n = $n - 0.01;
			}
			else 
			{
				$n = $n;
			}
		
		/************=========================Efficiency Calculation End================================*********************/
		$qH = 45;
		$pFuel =  0.85;
		if($vType == 1)
		{
			$qH = 43.5;
			$pFuel =  0.737;
		}
		//$newN = $this -> nCalc($devId,$row,$n);
		//$n = $newN;
		//echo " | n = $newN |";

		//===================================PETROL ENGINE TORQUE CALCULATIONS===============================================//		
		if($vType == 1)
		{
			//-------------- Equivalence Ratio Calculation ----------------------//
			if($ratio <= 0)	
			{
				$ratio = 1;
				if($Turbo_Avail_C == "No")
				{
					if($Injection_Type_C == "Indirect")
					{
						if($VVT_Status_C == 2) //For Vtec cars, i.e. Honda's; VVT status is 2 and equivalence ratios values are different
						{
							if($eLoad >= 1 && $eLoad <= 40)
							{
								$ratio = 1;
							}
							elseif($eLoad >= 41 && $eLoad <= 60)
							{
								$ratio = 1;
							}
							elseif($eLoad >= 61 && $eLoad <= 80)
							{
								$ratio = 0.995;
							}
							elseif($eLoad >= 81 && $eLoad <= 89)
							{
								$ratio = 0.94;
							}
							elseif($eLoad >= 90 && $eLoad <= 98)
							{
								$ratio = 0.90;
							}
							elseif($eLoad >= 99 && $eLoad <= 100)
							{
								$ratio = 0.85;
							}
							else 
							{
								$ratio = 1.99;
							}
						}
						elseif($VVT_Status_C == 0) //No VVT or Vtec technology
						{
							if($Veh_Type_C == 0)
							{
								if($eLoad >= 1 && $eLoad <= 40)
								{
									$ratio = 1;
								}
								elseif($eLoad >= 41 && $eLoad <= 60)
								{
									$ratio = 0.985;
								}
								elseif($eLoad >= 61 && $eLoad <= 80)
								{
									$ratio = 0.985;
								}
								elseif($eLoad >= 81 && $eLoad <= 89)
								{
									$ratio = 0.93;
								}
								elseif($eLoad >= 90 && $eLoad <= 98)
								{
									$ratio = 0.86;
								}
								elseif($eLoad >= 99 && $eLoad <= 100)
								{
									$ratio = 0.80;
								}
								else 
								{
									$ratio = 1.99;
								}
							}
							elseif($Veh_Type_C == 1)
							{
								if($eLoad >= 1 && $eLoad <= 40)
								{
									$ratio = 1;
								}
								elseif($eLoad >= 41 && $eLoad <= 60)
								{
									$ratio = 0.985;
								}
								elseif($eLoad >= 61 && $eLoad <= 80)
								{
									$ratio = 0.985;
								}
								elseif($eLoad >= 81 && $eLoad <= 89)
								{
									$ratio = 0.93;
								}
								elseif($eLoad >= 90 && $eLoad <= 98)
								{
									$ratio = 0.80;
								}
								elseif($eLoad >= 99 && $eLoad <= 100)
								{
									$ratio = 0.80;
								}
								else 
								{
									$ratio = 1.99;
								}
							}
						}
						elseif($VVT_Status_C == 1) //For VVT cars, i.e. Maruti's; VVT status is 1
						{
							if($eLoad >= 1 && $eLoad <= 40)
							{
								$ratio = 1;
							}
							elseif($eLoad >= 41 && $eLoad <= 60)
							{
								$ratio = 0.985;
							}
							elseif($eLoad >= 61 && $eLoad <= 80)
							{
								$ratio = 0.985;
							}
							elseif($eLoad >= 81 && $eLoad <= 89)
							{
								$ratio = 0.93;
							}
							elseif($eLoad >= 90 && $eLoad <= 98)
							{
								$ratio = 0.86;
							}
							elseif($eLoad >= 99 && $eLoad <= 100)
							{
								$ratio = 0.80;
							}
							else 
							{
								$ratio = 1.99;
							}
						}
					}
					else
					{
						if($eLoad >= 1 && $eLoad <= 40)
						{
							$ratio = 1;
						}
						elseif($eLoad >= 41 && $eLoad <= 60)
						{
							$ratio = 0.985;
						}
						elseif($eLoad >= 61 && $eLoad <= 80)
						{
							$ratio = 0.985;
						}
						elseif($eLoad >= 81 && $eLoad <= 89)
						{
							$ratio = 0.93;
						}
						elseif($eLoad >= 90 && $eLoad <= 98)
						{
							$ratio = 0.86;
						}
						elseif($eLoad >= 99 && $eLoad <= 100)
						{
							$ratio = 0.80;
						}
						else 
						{
							$ratio = 1.99;
						}
					}
				}
				else
				{
					if($Injection_Type_C == "Indirect")
					{
						if($eLoad <= 15)
						{
							$ratio = 1;
						}
						elseif($eLoad < 40)
						{
							$ratio = 1;
						}
						elseif($eLoad < 60)
						{
							$ratio = 0.995;
						}
						elseif($eLoad < 80)
						{
							$ratio = 0.99;
						}
						elseif($eLoad < 90)
						{
							$ratio = 0.97;
						}
						elseif($eLoad <= 98)
						{
							$ratio = 0.94;
						}
						elseif($eLoad <= 100)
						{
							$ratio = 0.93;
						}
						else 
						{
							$ratio = 1.99;
						}
					}
					else
					{
						if($eLoad <= 15)
						{
							$ratio = 1;
						}
						elseif($eLoad < 40)
						{
							$ratio = 1;
						}
						elseif($eLoad < 60)
						{
							$ratio = 0.995;
						}
						elseif($eLoad < 80)
						{
							$ratio = 0.99;
						}
						elseif($eLoad < 90)
						{
							$ratio = 0.97;
						}
						elseif($eLoad <= 98)
						{
							$ratio = 0.94;
						}
						elseif($eLoad <= 100)
						{
							$ratio = 0.93;
						}
						else 
						{
							$ratio = 1.99;
						}
					}
				}
				$row[$f6[0]] = $ratio;
			}
			
			if($row[$engStat] != 4)
			{
				if($row[$engStat] != 0 || $row[$engStat] != 7)
				{
					//𝐹𝑢𝑒𝑙 𝐹𝑙𝑜𝑤 (𝑔/𝑠)=𝑀𝐴𝐹  / 𝐸𝑞𝑢𝑖𝑣𝑎𝑙𝑒𝑛𝑐𝑒 𝑅𝑎𝑡𝑖𝑜×14.7			
					$fuelFlowG = ($ratio == 0) ? 0 : ($maf / ($ratio * 14.7));
					$fuelFlowL = $fuelFlowG / ($pFuel * 5 / 18);
					
					if($rpm >= 450) //($eLoad > 0)
					{
						//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100
						$powerK = $fuelFlowG * $qH * $n;
						
						//Power (HP) = Power (kW) * 1.341
						$powerG = $powerK * 1.341;
						
						//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
						$tor = ($rpm > 0) ? ($powerK * 1000 * 60) / (2*$pi*$rpm) : 0;
						
						//Temperature compensation algo for torque
						// $fact = 1;
						$num = (273 + $obdIntakeTemperature);
						$denom = 298;
						$divFact = $num/$denom;
						$fact = pow($divFact,0.6);
						$torcomp = ($tor * $fact);
						//$row[$f70[0]] = $torcomp;
						$row['Ftorcomp'] = round($torcomp,4);
					}
					else
					{
						$powerK = 0;
						$powerG = 0;
						$tor = 0;
					}
				}
				else
				{
					$fuelFlowG = 0;
					$fuelFlowL = 0;
					$powerK = 0;
					$powerG = 0;
					$tor = 0;
				}	
			}
			else
			{
				$fuelFlowG = 0;
				$fuelFlowL = 0;
				
				//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100
				$powerK = 0;
				
				//Power (HP) = Power (kW) * 1.341
				$powerG = 0;
				
				//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
				$tor = 0;
			}	
		}
		
		//DIESEL ENGINES
		else 
		{
			if($row[$engStat] != 4)
			{
				if($row[$engStat] != 0 || $row[$engStat] != 7)
				{
					//constant factor changing with engine capacity
					$ELoad_Idle_Warm_C = $this -> getCaliPramVal('ELoad_Idle_Warm_C',$devId);
					$constfact_Eload_0_20 = $this -> getCaliPramVal('Fact_Fuel_Run_ELoad_0_20_C',$devId);
					$constfact_Eload_20_40 = $this -> getCaliPramVal('Fact_Fuel_Run_ELoad_20_40_C',$devId);
					$constfact_Eload_40_60 = $this -> getCaliPramVal('Fact_Fuel_Run_ELoad_40_60_C',$devId);
					$constfact_Eload_60_80 = $this -> getCaliPramVal('Fact_Fuel_Run_ELoad_60_80_C',$devId);
					$constfact_Eload_80_100 = $this -> getCaliPramVal('Fact_Fuel_Run_ELoad_80_100_C',$devId);
					$constfactidle_Eload1 = $this -> getCaliPramVal('Fact_Fuel_Idle_Eload1_C',$devId);
					$constfactidle_Eload2 = $this -> getCaliPramVal('Fact_Fuel_Idle_Eload2_C',$devId);
					
					/*Made a calibratable label
					$constfact = 0.35;
					$constfactidle = 0.15;
					if($ed <= 1.8)
					{
						$constfact = 0.35;
						$constfactidle = 0.15;
					}
					elseif($ed < 2.4)
					{
						$constfact = 0.4;		//0.8	//0.6	//0.5	//0.4
						$constfactidle = 0.25;	//0.25	//0.4	//0.15
					}
					else
					{
						$constfact = 0.5;		//0.9
						$constfactidle = 0.35;	//0.35
					}
					*/
					if($row[$engStat] != 2)	
					{	
						//Fuel Flow (L/hr) = 0.0022*(MAF*Engine Load) + 1.0305	
						if($eLoad < 20)
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_0_20;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
						}
						elseif($eLoad < 40)
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_20_40;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
						}
						elseif($eLoad < 60)
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_40_60;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
						}
						elseif($eLoad < 80)
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_60_80;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
						}
						else
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_80_100;	//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
						}
					}
					else
					{
						//Fuel Flow (L/hr) = 0.0022*(MAF*Engine Load) + 1.0305
						if($eLoad < ($ELoad_Idle_Warm_C + 3))
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfactidle_Eload1;		//changed on 29.08.2016		constant = 0.30
						}
						else
						{
							$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfactidle_Eload2;
						}
					}	
					
					//Fuel Flow (g/s) = Fuel Flow (L/hr) * ρfuel * 5/18
					$fuelFlowG = $fuelFlowL * $pFuel * 5 / 18;
				}
				else 
				{
					$fuelFlowL = 0;
					$fuelFlowG = 0;
				}		
			}
			else 
			{
				$fuelFlowL = 0;
				$fuelFlowG = 0;
			}
			
			if($rpm >= 450) //($eLoad > 0)
			{
				//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100
				$powerK = $fuelFlowG * $qH * $n;
				
				//Power (HP) = Power (kW) * 1.341
				$powerG = $powerK * 1.341;
				
				//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
				$tor = ($rpm > 0) ? ($powerK * 1000 * 60) / (2*$pi*$rpm) : 0;
				
				//Temperature compensation algo for torque
				// $fact = 1;
				$num = (273 + $obdIntakeTemperature);
				$denom = 298;
				$divFact = $num/$denom;
				$fact = pow($divFact,0.6);
				$torcomp = ($tor * $fact);
				//$row[$f70[0]] = $torcomp;
				$row['Ftorcomp'] = round($torcomp,4);
			}
			else 
			{
				//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100	
				$powerK = 0;
				
				//Power (HP) = Power (kW) * 1.341
				$powerG = 0;
				
				//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
				$tor = 0;
			}
		}
		
		//echo "$f1[0] $f2[0] | ratio = $ratio inTakeTemp = $inTakeTemp n = $n map = $map ve = $ve ed = $ed EL = $eLoad MAF = $maf RPM = $rpm| FueType = $vType N = $n <br/>";
		
		/*//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100
		$powerK = $fuelFlowG * $qH * $n;
		//Power (HP) = Power (kW) * 1.341
		$powerG = $powerK * 1.341;
		//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
		$tor = ($rpm > 0) ? ($powerK * 1000 * 60) / (2*$pi*$rpm) : 0;*/
		
		//Instantaneous Mileage = (km/L) = Vehicle Velocity (km/hr) / Fuel flow (L/hr)		
		$intmil = ($fuelFlowL > 0) ? $row[$eSpe] / $fuelFlowL : 0;
		
		//Mileage (km/L) = Average Vehicle Velocity (km/hr) / Average Fuel flow (L/hr)
		// $avgFFlow = $datAct->valuefield('obdAvgFuelFlow');
		// $avgSpeed = $datAct->valuefield('obdAvgSpeed');
		// $avgSpeed = $avgSpeed[0];
		// $avgFFlow = $avgFFlow[0];
		
		//$eSpeA = $row[$avgSpeed]; //$this -> callArgage($devId,$eSpe,$row['Fsprint']);
		$eSpeA = $this -> callArgage($devId,$eSpe,$row['Fsprint']);
		//$fFlowA = $row[$avgFFlow]; //$this -> callArgage($devId,'Ffuelflow_l',$row['Fsprint']);
		$fFlowA = $this -> callArgage($devId,'Ffuelflow_l',$row['Fsprint']);
		
		/*$Fsprint = $row['Fsprint'];
		$sql = "select avg(Ffuelflow_l*1) from `device_data` where Fsprint = '$Fsprint' and F5 <> 0 order by time_s asc";
		$dat = $this -> select($sql);
		$avgFFlow = $dat[0]['avg(Ffuelflow_l*1)'];
		$fFlowA = $avgFFlow;
		$sql = "select avg(F5*1) from `device_data` where Fsprint = '$Fsprint' and F5 <> 0 order by time_s asc";
		$dat = $this -> select($sql);
		$avgSpeed = $dat[0]['avg(F5*1)'];
		$eSpeA = $avgSpeed;*/
		$mil = ($fFlowA > 0) ? ($eSpeA / $fFlowA) : 0;
		
		//Fuel Consumed (L) = Distance Travelled (km) / Mileage (km/L)
		// $distT = $row['Fdist'];//$this -> callSum($devId,'Fdist',$row['Fsprint']);
		$distT = $this -> callSum($devId,'Fdist',$row['Fsprint']);
		$Ffcon = ($mil > 0) ? ($distT / $mil) : 0 ;
		//echo "$eSpeA | $fFlowA = $mil";
		
		//Average Speed (km/hr) = Sum of all speeds of the Vehicle for one drive (km/hr) / No. of instances
		$Favspeed = $eSpeA;
		
		//echo "fuelFlowL = $fuelFlowL | fuelFlowG = $fuelFlowG | powerK = $powerK | powerG = $powerG | tor = $tor | inm =  $intmil mil = $mil | dist = $distT fc= $Ffcon | $eSpeA  | $fFlowA";
		$row['Ffuelflow_l'] = round($fuelFlowL,4);
		$row['Fpower_hp'] = round($powerG,4);
		$row['Ftorque'] = round($tor,4);	
		$row['Fintmil'] = round($intmil,4);	
		$row['Fmil'] = round($mil,4);
		$row['Ffcon'] = round($Ffcon,4);
		$row['Favspeed'] = round($Favspeed,4);	
		//$json = json_encode($row);	
		//print_r($json);
		//print_r($row);	
		//echo "<br/> ------------------------- <br/> ";		
		
		/*if($row[$engStat] == 6)
		{
			$dtDrive = date('Y-m-d',strtotime($row['time_v']));
			$this -> callDriveUpdate($row['Fsprint'],$devId,$dtDrive,$row);
			//coldStart	
			$obdCT = $datAct->valuefield('obdCoolantTemperature');
			$obdCT = $obdCT[0];
			if($row[$obdCT]<=55)
			{
				$sql = "update device set cools = (cools+1) where device_id='$devId'";
				mysql_query($sql);
			}
			
		}
		$this -> CreateFileCSV($row,$devId);*/
		return $row;	
	}
	
	function calcOilEngLife($reqFor,$deviceID,$dt,$row,$durTr)
	{
		//============== Calc for Eng Oil Life ==========
			$obdCTInit = $this -> totSpeedLast($reqFor,'obdCoolantTemperature',0,'asc');
			$obdCTMax = $this -> maxSpeed($reqFor,'obdCoolantTemperature');
			$obdLDMax = $this -> maxSpeed($reqFor,'obdEngineLoad');
			
			$sql = "select sum(dist*1) from device_drive where device_id ='$deviceID' and (dist*1) <> 0 and (avg_ml*1) <> 0";
			$numDist = mysql_fetch_array(mysql_query($sql));
			$numDist = round($numDist[0],2);
			
			$valP = $this -> getvehicle($deviceID);
			//$factVal = ($valP[0]['co2fact'] >= 1) ? $valP[0]['co2fact'] : 2320;
			$lastOilChngDt = ($valP[0]['oil_dt'] != "0000-00-00" && $valP[0]['oil_dt'] != "") ? $valP[0]['oil_dt'] : "2017-01-01"; // set and get frm veh info
			$lastOilChngKm = ($valP[0]['oil_dist'] >= 1) ? $valP[0]['oil_dist'] : 1000; // set and get frm veh info
			$distTrSPur = ($valP[0]['travel_purchase'] >= 1) ? $valP[0]['travel_purchase'] : 100; // get from veh info
			$distSinOnB = ($numDist >= 1) ? ($distTrSPur + $numDist) : $distTrSPur; // get from chart distance since on board
			
			$oilLifeDaysC = ($valP[0]['oill_day'] >= 1) ? $valP[0]['oill_day'] : 365; // set and get frm veh cal info
			$oilLifeKmsC = ($valP[0]['oill_dist'] >= 1) ? $valP[0]['oill_dist'] : 10000; // set and get frm veh cal info
			$dustEnvC = ($valP[0]['dust_env'] >= 1) ? $valP[0]['dust_env'] : 0; // set and get frm veh cal info
			
			//calc days since oil chng=====================================
			$daySOChng = round((strtotime(date('Y-m-d')) - strtotime($lastOilChngDt)) / (60*60*24));
			
			//calc km sinse oil chng=====================================
			$factdist = floor($distSinOnB/$oilLifeKmsC);
			$factoilchng = floor($lastOilChngKm/$oilLifeKmsC);
			if($factdist == $factoilchng)
			{
				$kmSOChng = ($distSinOnB - ($oilLifeKmsC * $factdist)) - ($lastOilChngKm  - ($oilLifeKmsC * $factoilchng));
			}
			else
			{
				$kmSOChng = $distSinOnB - $lastOilChngKm;
			}
			
			//Corresponding days for distance travelled=====================================
			$daysDistTrv = ($kmSOChng*$oilLifeDaysC)/$oilLifeKmsC;
			$daysDistTrv = floor($daysDistTrv);
			
			//Engine oil operating temperature (T)=====================================
			$engOilOpTemp = 0;			
			if($obdCTMax < 100)
			{
				$engOilOpTemp = $obdCTMax + 5;
			}
			else 
			{
				$engOilOpTemp = $obdCTMax + 10;
			}
			
			//Time after cranking (t)=====================================
			$timeAfCrnk = 40 - ($obdCTInit * 0.25);
			
			//Temperature Correction Factor (Ct)=====================================
			$tempCorrFact = 0;
			if($engOilOpTemp < 0)
			{
				$tempCorrFact = 8;
			}
			elseif($engOilOpTemp < 80) 
			{
				$tempCorrFact = 1 + (7 * ((80 - $engOilOpTemp)/80));
				$tempCorrFact = round($tempCorrFact,2);
			}
			elseif($engOilOpTemp < 130) 
			{
				$tempCorrFact = 1;
			}
			else
			{
				$expNt = ($engOilOpTemp - 130)/(80);
				$tempCorrFact = pow(10,$expNt);
				$tempCorrFact = round($tempCorrFact,2);
			}
			
			//Engine Load Correction Factor (Cl)=====================================
			$engLoadCorFact = 0;
			if($obdLDMax < 70)
			{
				$engLoadCorFact = 1;
			}
			else
			{
				$expNt = ($obdLDMax - 70)/(30);
				$engLoadCorFact = round(pow(2,$expNt),2);
			}
			
			//Engine RPM Correction Factor (Crpm)=====================================
			$engRPMCorFact = 1; //fixed value to reduce complexity. Values not affected much. Factor is very small.
			
			//Ccck0=====================================
			$cck = 0;
			if($obdCTMax <= 0)
			{
				$cck = 10;
			}
			elseif($obdCTMax < 80)
			{
				$cck = 10 - ($obdCTMax/16);
			}
			else
			{
				$cck = 5;
			}
			
			//T0=====================================
			$t0 = 0;
			if($obdCTMax <= 0)
			{
				$t0 = 40;
			}
			elseif($obdCTMax < 80)
			{
				$t0 = 40 - ($obdCTMax/4);
			}
			else
			{
				$t0 = 20;
			}
			
			//Engine Cranking Correction Factor=====================================
			$engCrCorFact = 0;
			if($timeAfCrnk < $t0)
			{
				$engCrCorFact = 1 + (($cck - 1) * (($t0 - $timeAfCrnk)/$t0));
				$engCrCorFact = round($engCrCorFact,2);
			}
			else
			{
				$engCrCorFact = 1;
			}
			
			//Oil Age Factor=====================================
			$oilAgeFact = 0;
			if($daySOChng <= 365)
			{
				$oilAgeFact = 1;
			}
			else
			{
				$expN = (365 - $daySOChng)/365;				
				$oilAgeFact = round(exp($expN),2);
			}
			//Dust Correction factor=====================================
			$dustCorFact = 0;
			if($dustEnvC == 1)
			{
				$dustCorFact = 1.5;
			}
			else
			{
				$dustCorFact = 1;
			}
			
			//Cumulated Oil Life Initialisation=====================================
			$cumOilLifeLastDrive = $this -> driveValue(1,'oillife');//get from Database of last driveData
			$cumOilLife = ($cumOilLifeLastDrive > 0 && $cumOilLifeLastDrive <= 365) ? $cumOilLifeLastDrive : $daySOChng;
			
			//Oil used life=====================================
			$oilUsedLife = 0;
			if($cumOilLife <= $daysDistTrv)
			{
				$oilUsedLife = $daysDistTrv + (($durTr/60) * $tempCorrFact * $engLoadCorFact * $engRPMCorFact * $engCrCorFact * $dustCorFact);
			}
			else
			{
				$oilUsedLife = $cumOilLife + (($durTr/60) * $tempCorrFact * $engLoadCorFact * $engRPMCorFact * $engCrCorFact * $dustCorFact);
			}
			$oilUsedLife = floor($oilUsedLife);
			
			//Cumulated Oil Life Calculation=====================================
			$cumOilLife = $oilUsedLife;
			
			//% of Oil life remaining=====================================
			$pOfOilLf = 0;
			if($cumOilLife >= $oilLifeDaysC || $daysDistTrv >= $oilLifeDaysC)
			{
				$pOfOilLf = 0;
			}
			else
			{
				$pOfOilLf = 100 * (1 - ($oilUsedLife/($oilLifeDaysC * $oilAgeFact)));
			}
			$pOfOilLf = round(($pOfOilLf),0);
			
			//Point 20 need to be set in text
			return array($oilUsedLife,$pOfOilLf);
	
			//============== End ==========
	}
	
	function SetDeviceInfoLoc($obj) //to set lat long added on 21/08/2017
	{
		$deviceid = $obj['devID'];
		$lat = $obj['lat'];
		$long = $obj['long'];
		$alt =$obj['alt'];
		if($obj['ts'] < 946684800)
		{
			$time_spam = ($obj['ts'] + 946684800) * 1000;
		}
		else
		{
			$time_spam = $obj['ts'];
		}
		$sql = "update device set latitude='$lat',longitude='$long',alt='$alt',time_stamp='$time_spam' where device_id='$deviceid'";
		if(mysql_query($sql))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function setgetDeviceAction($deviceid,$act,$type="")	//to set flag for calibrataion label update added on 11/10/2017
	{
		if($type == 1)
		{
			$sql = "select dactval from device where device_id='$deviceid' limit 0,1";
			$dat = mysql_fetch_array(mysql_query($sql));
			return $dat['dactval'];
		}
		else
		{
			$sql = "update device set dactval ='$act' where device_id='$deviceid'";
			if(mysql_query($sql))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	function SetDeviceEvent($obj) //to set lat long added on 21/08/2017
	{
		$data['device_id'] = $obj['devID'];
		$data['lat'] = $obj['lat'];
		$data['lon'] = $obj['lon'];
		$data['alt'] = $obj['alt'];
		$data['time_s'] = ($obj['ts'] + 946684800) * 1000;
		$data['ad_dt'] = date('Y-m-d H:i:s',substr($data['time_s'],0,10));
		$data['event'] = $obj['ev'];
		$data['nodrive'] = $obj['dn'];
		$data['seqno'] = $obj['s'];
		$sql = "select id from event where device_id='".$data['device_id']."' and time_s='".$data['time_s']."' and event='".$data['event']."' limit 0,1";
		$oldD = $this -> select($sql);
		$data['idVal'] = $oldD[0]['id'];
		
		$saveData = new saveForm('event',$data);
		if($data['idVal'] >= 1)
		{
			if($saveData -> updateData('id','idVal'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			if($saveData -> addData())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	
	function callSaveDData($postVal,$type="",$dataOnly="",$fspLast="")
	{
		//$sql = "select device_id,time_s,id from device_data where device_id = '".$postVal['device_id']."' and time_s = '".$postVal['time_s']."'";
		//$dat = $this -> select($sql);
		//if(count($dat) <= 0)
		$postVal['ad_dtin'] = date('Y-m-d',strtotime($postVal['time_v']));
		{
			$postVal = $this -> devFlowInfo($postVal['device_id'],$postVal,$type,$dataOnly,$fspLast);
			if($dataOnly >= 1)
			{
				$devId = $postVal['device_id'];
				$this -> CreateFileCSV($postVal,$devId);
				return $postVal;
				unset($postVal);
			}
			else
			{
				$saveData = new saveForm('device_data',$postVal);
				if($saveData->addData())
				{
					$devId = $postVal['device_id'];
					$row = $postVal;
					$datAct = new datAct('device1');
					$f9 = $datAct->valuefield('obdCalcEngineState');
					$engStat = $f9[0];
					unset($postVal);
					if($row[$engStat] == 6)
					{
						$dtDrive = date('Y-m-d',strtotime($row['time_v']));
						$this -> callDriveUpdate($row['Fsprint'],$devId,$dtDrive,$row);
						if($row['lat'] != 0 && $row['lon'] !=0)
						{
							$loc=array('devID' => $devId,'lat'=>$row['lat'],'long'=>$row['lon'],'alt'=>$row['alt'],'ts'=>$row['time_s']);
							$this -> SetDeviceInfoLoc($loc);
						}
						//coldStart
						$obdCT = $datAct -> valuefield('obdCoolantTemperature');
						$obdCT = $obdCT[0];
						if($row[$obdCT] <= 55)
						{
							$sql = "update device set cools = (cools+1) where device_id='$devId'";
							mysql_query($sql);
						}
						//$this -> CreateFileCSV($row,$devId);
					}
					$this -> CreateFileCSV($row,$devId);
					return $row;
					unset($row);
				}
			}
		}
		//else
		//{
			/*$postVal['id'] = $dat[0]['id'];
			$saveData = new saveForm('device_data',$postVal);
			if($saveData->updateData('id','id'));*/
		//}
		unset($postVal);
	}
	
	function callSaveDDataNew($postVal,$type="",$dataOnly="",$fspLast="")
	{
		//$sql = "select device_id,time_s,id from device_data where device_id = '".$postVal['device_id']."' and time_s = '".$postVal['time_s']."'";
		//$dat = $this -> select($sql);
		//if(count($dat) <= 0)
		$postVal['ad_dtin'] = date('Y-m-d',strtotime($postVal['time_v']));
		$postVal = $this -> devFlowInfo($postVal['device_id'],$postVal,$type,$dataOnly,$fspLast);
		$devId = $postVal['device_id'];
		$this -> CreateFileCSV($postVal,$devId);
		return $postVal;
		unset($postVal);
	}
	
	/***********************************************************************************************
	 * Function Called by API to Calculate PIds, Statistics and Monitoring results
	 * THis Function is typicalled called by APIs getting data in CSv file Format
	 * The Aim of this function is to do Inmemeory pocessing and reduce the number of DB accessing
	 ***********************************************************************************************/
	function calcStatMonData($devId,$iLoopData,$iLoopSize,$driveID)
	{
		/*$deviceDataHeader = array('id','device_id','time_s','time_v','lat','lon','alt','alldata','ad_dt',
		'C1','C2','C3','C4','C5','F1','F2','F3','F4','F5','F6','F7','F8','F9','F10','F11','F12',
		'F13','F14','F15','F16','F17','F18','F19','F20','F21','F22','F23','F24','F25','F26','F27'
		,'F28','F29','F30','F31','F30_1','F30_2','F30_3','F32','F32_4','F32_5','F32_6','F33',
		'Ffuelflow_l','Fpower_hp','Ftorque','F34','F35','Fsprint','Fdist','Fintmil','Fmil','Ffcon',
		'Favspeed','F36','F37','F38','F39','F40','obdCurrentDtcs','F41','F42','F43','F14_7','F14_8',
		'F14_9','F14_10','F14_11','F44','F45','ad_dtin','F46','F47','F48','F49','F50','F51','F52',
		'F53','F54','F55','F56','F57','F58','F59','F60','F61','F62','F63','F64','F65','F66','F67',
		'F68','F69','Ftorcomp');*/
		/********************************************************************************************
		 * INITILASE all required variables
		 * *****************************************************************************************/
		$calval = array();
		$count = 0;
		$countstats = 0;
		$distT = 0;
		$totalDist = 0;
		$oldtimestamp = 0;
		$totaldur = 0;
		$rowcount = 0;
		$maxVehSpd = 0;
		$maxTorque = 0;
		$firsttimestamp = 0;
		$idletime = 0;
		$ovrruntime = 0;
		$ovrrundist = 0;
		$fFlowSum = 0;
		$fFlowAvg = 0;
		$vehspdSum = 0;
		$vehSpdAvg = 0;
		$maxCRP = 0;
		$maxCRPcsv = 0;
		$crpEload = 0;
		$crpRpm = 0;
		$idleCRP = 0;
		$startNewSegment = 1; 		// 1 for new segment has to start and 0 for the ongoing segment.
		$MAPPeak0 = 0;
		$MAPPeak1 = 0;
		$maxMAP = 0;
		$maxMAPcon = 0;
		$maxMAPcsv = 0;
		$maxMAPEload = 0;
		$maxairMAP = 0;
		$idleMAPFlag = 0;
		$idleMAP = 0;
		$baroIdleMAP = 0;
		$baroMaxMAP = 0;
		$maxMAF = 0;
		//$minBatVolt = 0;
		$maxBatVolt = 0;
		$ignOnMinBatVolt = 0;
		$ignOnMaxBatVolt = 0;
		$batcool = 0;
		$maxCool = 0;
		$minCool = 0;
		$prevCool = 0;
		$countcool = 0;
		$countcoollow = 0;
		$countcool = 0;
		$calcMonBuf = array();
		$deltaRPM = 0;
		$deltaRPMcurr = 0;
		$currRpmOscScore = 0;
		$curCO2 = 0;
		$totCO2 = 0;
		$batState = 0;
		$altState = 0;
		$regState = 0;
		$batOverallState = 0;
		$IdleBoostSCORE = 0;
		$PeakBoostSCORE = 0;
		$IdleAirBoostSCORE = 0;
		$PeakairBoostSCORE = 0;
		$IdleCRPSCORE = 0;
		$PeakCRPSCORE = 0;
		$CombPresState = 0;
		$coolHelScore = 0;
		$pi =  3.14159;
		$mm = 28.97;
		$r = 8.314;
		$gasconst = 287.053;		//Universal Gas constant
		$gacc = 9.80665;			//Gravitational acceleration
		$stdBaro = 101325;			//Standard barometric pressure @sea level
		$rpmIndex = 0;
		/********************************************************************************************
		 * STEP1: Get all calibration data based on device ID
		 * *****************************************************************************************/
		
		$calval = $this -> getCaliPramValArry($param,$devId);
		//print_r($calval);
		if($calval != "")
		{
			$vehi = $this -> getvehicle($devId);
			$vType = $vehi[0]['fuel_tpe'];
			$baseN = $calval['Eng_eff_C']; 
			$ed = $calval['Eng_Disp_C'];
			$CurrCityPress = $calval['Baro_Press_City_C'];
			$MAP_Valid_C = $calval['MAP_Valid_C'];
			$MAF_Valid_C = $calval['MAF_Valid_C'];
			$Turbo_Avail_C = $calval['TC_Status_C'];
			$Injection_Type_C = $calval['Injection_Type_C'];
			$MAP_Type_C = $calval['MAP_Type_C'];
			$intercoolerPresent = $calval['IC_Status_C'];
			$VVT_Status_C = $calval['VVT_Status_C'];
			$Veh_Type_C = $calval['Veh_Type_C'];
			$Cool_Lmt_C = $calval['Cool_Lmt_C'];
			$RPM_Idle_Warm_C = $calval['RPM_Idle_Warm_C'];
			$RPM_Idle_Ld1_C = $calval['RPM_Idle_Ld1_C'];
			$RPM_Idle_Ld2_C = $calval['RPM_Idle_Ld2_C'];
			$Eload_Max_C = $calval['Eload_Max_C'];
			$RPM_Limit1 = $calval['Eff_RPM_Limit1_C'];
			$RPM_Limit2 = $calval['Eff_RPM_Limit2_C'];
			$RPM_Limit3 = $calval['Eff_RPM_Limit3_C'];
			$RPM_Limit4 = $calval['Eff_RPM_Limit4_C'];
			$RPM_Limit5 = $calval['Eff_RPM_Limit5_C'];
			$VolEff_Eload_1_10_RPMLimit1_C = $calval['VolEff_Eload_1_10_RPMLimit1_C'];
			$VolEff_Eload_1_10_RPMLimit2_C = $calval['VolEff_Eload_1_10_RPMLimit2_C'];
			$VolEff_Eload_1_10_RPMLimit3_C = $calval['VolEff_Eload_1_10_RPMLimit3_C'];
			$VolEff_Eload_1_10_RPMLimit4_C = $calval['VolEff_Eload_1_10_RPMLimit4_C'];
			$VolEff_Eload_1_10_RPMLimit5_C = $calval['VolEff_Eload_1_10_RPMLimit5_C'];
			$VolEff_Eload_11_20_RPMLimit1_C = $calval['VolEff_Eload_11_20_RPMLimit1_C'];
			$VolEff_Eload_11_20_RPMLimit2_C = $calval['VolEff_Eload_11_20_RPMLimit2_C'];
			$VolEff_Eload_11_20_RPMLimit3_C = $calval['VolEff_Eload_11_20_RPMLimit3_C'];
			$VolEff_Eload_11_20_RPMLimit4_C = $calval['VolEff_Eload_11_20_RPMLimit4_C'];
			$VolEff_Eload_11_20_RPMLimit5_C = $calval['VolEff_Eload_11_20_RPMLimit5_C'];
			$VolEff_Eload_21_30_RPMLimit1_C = $calval['VolEff_Eload_21_30_RPMLimit1_C'];
			$VolEff_Eload_21_30_RPMLimit2_C = $calval['VolEff_Eload_21_30_RPMLimit2_C'];
			$VolEff_Eload_21_30_RPMLimit3_C = $calval['VolEff_Eload_21_30_RPMLimit3_C'];
			$VolEff_Eload_21_30_RPMLimit4_C = $calval['VolEff_Eload_21_30_RPMLimit4_C'];
			$VolEff_Eload_21_30_RPMLimit5_C = $calval['VolEff_Eload_21_30_RPMLimit5_C'];
			$VolEff_Eload_31_40_RPMLimit1_C = $calval['VolEff_Eload_31_40_RPMLimit1_C'];
			$VolEff_Eload_31_40_RPMLimit2_C = $calval['VolEff_Eload_31_40_RPMLimit2_C'];
			$VolEff_Eload_31_40_RPMLimit3_C = $calval['VolEff_Eload_31_40_RPMLimit3_C'];
			$VolEff_Eload_31_40_RPMLimit4_C = $calval['VolEff_Eload_31_40_RPMLimit4_C'];
			$VolEff_Eload_31_40_RPMLimit5_C = $calval['VolEff_Eload_31_40_RPMLimit5_C'];
			$VolEff_Eload_41_50_RPMLimit1_C = $calval['VolEff_Eload_41_50_RPMLimit1_C'];
			$VolEff_Eload_41_50_RPMLimit2_C = $calval['VolEff_Eload_41_50_RPMLimit2_C'];
			$VolEff_Eload_41_50_RPMLimit3_C = $calval['VolEff_Eload_41_50_RPMLimit3_C'];
			$VolEff_Eload_41_50_RPMLimit4_C = $calval['VolEff_Eload_41_50_RPMLimit4_C'];
			$VolEff_Eload_41_50_RPMLimit5_C = $calval['VolEff_Eload_41_50_RPMLimit5_C'];
			$VolEff_Eload_51_60_RPMLimit1_C = $calval['VolEff_Eload_51_60_RPMLimit1_C'];
			$VolEff_Eload_51_60_RPMLimit2_C = $calval['VolEff_Eload_51_60_RPMLimit2_C'];
			$VolEff_Eload_51_60_RPMLimit3_C = $calval['VolEff_Eload_51_60_RPMLimit3_C'];
			$VolEff_Eload_51_60_RPMLimit4_C = $calval['VolEff_Eload_51_60_RPMLimit4_C'];
			$VolEff_Eload_51_60_RPMLimit5_C = $calval['VolEff_Eload_51_60_RPMLimit5_C'];
			$VolEff_Eload_61_80_RPMLimit1_C = $calval['VolEff_Eload_61_80_RPMLimit1_C'];
			$VolEff_Eload_61_80_RPMLimit2_C = $calval['VolEff_Eload_61_80_RPMLimit2_C'];
			$VolEff_Eload_61_80_RPMLimit3_C = $calval['VolEff_Eload_61_80_RPMLimit3_C'];
			$VolEff_Eload_61_80_RPMLimit4_C = $calval['VolEff_Eload_61_80_RPMLimit4_C'];
			$VolEff_Eload_61_80_RPMLimit5_C = $calval['VolEff_Eload_61_80_RPMLimit5_C'];
			$VolEff_Eload_81_90_RPMLimit1_C = $calval['VolEff_Eload_81_90_RPMLimit1_C'];
			$VolEff_Eload_81_90_RPMLimit2_C = $calval['VolEff_Eload_81_90_RPMLimit2_C'];
			$VolEff_Eload_81_90_RPMLimit3_C = $calval['VolEff_Eload_81_90_RPMLimit3_C'];
			$VolEff_Eload_81_90_RPMLimit4_C = $calval['VolEff_Eload_81_90_RPMLimit4_C'];
			$VolEff_Eload_81_90_RPMLimit5_C = $calval['VolEff_Eload_81_90_RPMLimit5_C'];
			$VolEff_Eload_91_94_RPMLimit1_C = $calval['VolEff_Eload_91_94_RPMLimit1_C'];
			$VolEff_Eload_91_94_RPMLimit2_C = $calval['VolEff_Eload_91_94_RPMLimit2_C'];
			$VolEff_Eload_91_94_RPMLimit3_C = $calval['VolEff_Eload_91_94_RPMLimit3_C'];
			$VolEff_Eload_91_94_RPMLimit4_C = $calval['VolEff_Eload_91_94_RPMLimit4_C'];
			$VolEff_Eload_91_94_RPMLimit5_C = $calval['VolEff_Eload_91_94_RPMLimit5_C'];
			$VolEff_Eload_95_98_RPMLimit1_C = $calval['VolEff_Eload_95_98_RPMLimit1_C'];
			$VolEff_Eload_95_98_RPMLimit2_C = $calval['VolEff_Eload_95_98_RPMLimit2_C'];
			$VolEff_Eload_95_98_RPMLimit3_C = $calval['VolEff_Eload_95_98_RPMLimit3_C'];
			$VolEff_Eload_95_98_RPMLimit4_C = $calval['VolEff_Eload_95_98_RPMLimit4_C'];
			$VolEff_Eload_95_98_RPMLimit5_C = $calval['VolEff_Eload_95_98_RPMLimit5_C'];
			$VolEff_Eload_99_100_RPMLimit1_C = $calval['VolEff_Eload_99_100_RPMLimit1_C'];
			$VolEff_Eload_99_100_RPMLimit2_C = $calval['VolEff_Eload_99_100_RPMLimit2_C'];
			$VolEff_Eload_99_100_RPMLimit3_C = $calval['VolEff_Eload_99_100_RPMLimit3_C'];
			$VolEff_Eload_99_100_RPMLimit4_C = $calval['VolEff_Eload_99_100_RPMLimit4_C'];
			$VolEff_Eload_99_100_RPMLimit5_C = $calval['VolEff_Eload_99_100_RPMLimit5_C'];
			$Eqratio_Eload_1_10_RPMLimit1_C = $calval['Eqratio_Eload_1_10_RPMLimit1_C'];
			$Eqratio_Eload_1_10_RPMLimit2_C = $calval['Eqratio_Eload_1_10_RPMLimit2_C'];
			$Eqratio_Eload_1_10_RPMLimit3_C = $calval['Eqratio_Eload_1_10_RPMLimit3_C'];
			$Eqratio_Eload_1_10_RPMLimit4_C = $calval['Eqratio_Eload_1_10_RPMLimit4_C'];
			$Eqratio_Eload_1_10_RPMLimit5_C = $calval['Eqratio_Eload_1_10_RPMLimit5_C'];
			$Eqratio_Eload_11_20_RPMLimit1_C = $calval['Eqratio_Eload_11_20_RPMLimit1_C'];
			$Eqratio_Eload_11_20_RPMLimit2_C = $calval['Eqratio_Eload_11_20_RPMLimit2_C'];
			$Eqratio_Eload_11_20_RPMLimit3_C = $calval['Eqratio_Eload_11_20_RPMLimit3_C'];
			$Eqratio_Eload_11_20_RPMLimit4_C = $calval['Eqratio_Eload_11_20_RPMLimit4_C'];
			$Eqratio_Eload_11_20_RPMLimit5_C = $calval['Eqratio_Eload_11_20_RPMLimit5_C'];
			$Eqratio_Eload_21_30_RPMLimit1_C = $calval['Eqratio_Eload_21_30_RPMLimit1_C'];
			$Eqratio_Eload_21_30_RPMLimit2_C = $calval['Eqratio_Eload_21_30_RPMLimit2_C'];
			$Eqratio_Eload_21_30_RPMLimit3_C = $calval['Eqratio_Eload_21_30_RPMLimit3_C'];
			$Eqratio_Eload_21_30_RPMLimit4_C = $calval['Eqratio_Eload_21_30_RPMLimit4_C'];
			$Eqratio_Eload_21_30_RPMLimit5_C = $calval['Eqratio_Eload_21_30_RPMLimit5_C'];
			$Eqratio_Eload_31_40_RPMLimit1_C = $calval['Eqratio_Eload_31_40_RPMLimit1_C'];
			$Eqratio_Eload_31_40_RPMLimit2_C = $calval['Eqratio_Eload_31_40_RPMLimit2_C'];
			$Eqratio_Eload_31_40_RPMLimit3_C = $calval['Eqratio_Eload_31_40_RPMLimit3_C'];
			$Eqratio_Eload_31_40_RPMLimit4_C = $calval['Eqratio_Eload_31_40_RPMLimit4_C'];
			$Eqratio_Eload_31_40_RPMLimit5_C = $calval['Eqratio_Eload_31_40_RPMLimit5_C'];
			$Eqratio_Eload_41_50_RPMLimit1_C = $calval['Eqratio_Eload_41_50_RPMLimit1_C'];
			$Eqratio_Eload_41_50_RPMLimit2_C = $calval['Eqratio_Eload_41_50_RPMLimit2_C'];
			$Eqratio_Eload_41_50_RPMLimit3_C = $calval['Eqratio_Eload_41_50_RPMLimit3_C'];
			$Eqratio_Eload_41_50_RPMLimit4_C = $calval['Eqratio_Eload_41_50_RPMLimit4_C'];
			$Eqratio_Eload_41_50_RPMLimit5_C = $calval['Eqratio_Eload_41_50_RPMLimit5_C'];
			$Eqratio_Eload_51_60_RPMLimit1_C = $calval['Eqratio_Eload_51_60_RPMLimit1_C'];
			$Eqratio_Eload_51_60_RPMLimit2_C = $calval['Eqratio_Eload_51_60_RPMLimit2_C'];
			$Eqratio_Eload_51_60_RPMLimit3_C = $calval['Eqratio_Eload_51_60_RPMLimit3_C'];
			$Eqratio_Eload_51_60_RPMLimit4_C = $calval['Eqratio_Eload_51_60_RPMLimit4_C'];
			$Eqratio_Eload_51_60_RPMLimit5_C = $calval['Eqratio_Eload_51_60_RPMLimit5_C'];
			$Eqratio_Eload_61_80_RPMLimit1_C = $calval['Eqratio_Eload_61_80_RPMLimit1_C'];
			$Eqratio_Eload_61_80_RPMLimit2_C = $calval['Eqratio_Eload_61_80_RPMLimit2_C'];
			$Eqratio_Eload_61_80_RPMLimit3_C = $calval['Eqratio_Eload_61_80_RPMLimit3_C'];
			$Eqratio_Eload_61_80_RPMLimit4_C = $calval['Eqratio_Eload_61_80_RPMLimit4_C'];
			$Eqratio_Eload_61_80_RPMLimit5_C = $calval['Eqratio_Eload_61_80_RPMLimit5_C'];
			$Eqratio_Eload_81_90_RPMLimit1_C = $calval['Eqratio_Eload_81_90_RPMLimit1_C'];
			$Eqratio_Eload_81_90_RPMLimit2_C = $calval['Eqratio_Eload_81_90_RPMLimit2_C'];
			$Eqratio_Eload_81_90_RPMLimit3_C = $calval['Eqratio_Eload_81_90_RPMLimit3_C'];
			$Eqratio_Eload_81_90_RPMLimit4_C = $calval['Eqratio_Eload_81_90_RPMLimit4_C'];
			$Eqratio_Eload_81_90_RPMLimit5_C = $calval['Eqratio_Eload_81_90_RPMLimit5_C'];
			$Eqratio_Eload_91_94_RPMLimit1_C = $calval['Eqratio_Eload_91_94_RPMLimit1_C'];
			$Eqratio_Eload_91_94_RPMLimit2_C = $calval['Eqratio_Eload_91_94_RPMLimit2_C'];
			$Eqratio_Eload_91_94_RPMLimit3_C = $calval['Eqratio_Eload_91_94_RPMLimit3_C'];
			$Eqratio_Eload_91_94_RPMLimit4_C = $calval['Eqratio_Eload_91_94_RPMLimit4_C'];
			$Eqratio_Eload_91_94_RPMLimit5_C = $calval['Eqratio_Eload_91_94_RPMLimit5_C'];
			$Eqratio_Eload_95_98_RPMLimit1_C = $calval['Eqratio_Eload_95_98_RPMLimit1_C'];
			$Eqratio_Eload_95_98_RPMLimit2_C = $calval['Eqratio_Eload_95_98_RPMLimit2_C'];
			$Eqratio_Eload_95_98_RPMLimit3_C = $calval['Eqratio_Eload_95_98_RPMLimit3_C'];
			$Eqratio_Eload_95_98_RPMLimit4_C = $calval['Eqratio_Eload_95_98_RPMLimit4_C'];
			$Eqratio_Eload_95_98_RPMLimit5_C = $calval['Eqratio_Eload_95_98_RPMLimit5_C'];
			$Eqratio_Eload_99_100_RPMLimit1_C = $calval['Eqratio_Eload_99_100_RPMLimit1_C'];
			$Eqratio_Eload_99_100_RPMLimit2_C = $calval['Eqratio_Eload_99_100_RPMLimit2_C'];
			$Eqratio_Eload_99_100_RPMLimit3_C = $calval['Eqratio_Eload_99_100_RPMLimit3_C'];
			$Eqratio_Eload_99_100_RPMLimit4_C = $calval['Eqratio_Eload_99_100_RPMLimit4_C'];
			$Eqratio_Eload_99_100_RPMLimit5_C = $calval['Eqratio_Eload_99_100_RPMLimit5_C'];
			$Ovr_Run_EqRatio_C = $calval['Ovr_Run_EqRatio_C'];
			$Ovr_Run_VolEff_C = $calval['Ovr_Run_VolEff_C'];
			$Ovr_Run_FuelFact_C = $calval['Ovr_Run_FuelFact_C'];
			$Ovr_Run_FuelFact_Eload_C = $calval['Ovr_Run_FuelFact_Eload_C'];
			$constfact_Eload_0_20 = $calval['Fact_Fuel_Run_ELoad_0_20_C'];
			$constfact_Eload_20_40 = $calval['Fact_Fuel_Run_ELoad_20_40_C'];
			$constfact_Eload_40_60 = $calval['Fact_Fuel_Run_ELoad_40_60_C'];
			$constfact_Eload_60_80 = $calval['Fact_Fuel_Run_ELoad_60_80_C'];
			$constfact_Eload_80_100 = $calval['Fact_Fuel_Run_ELoad_80_100_C'];
			$constfactidle_Eload1 = $calval['Fact_Fuel_Idle_ELoad1_C'];
			$constfactidle_Eload2 = $calval['Fact_Fuel_Idle_ELoad2_C'];
			$co2_Cnv_Fac_C = $calval['Co2_Cnv_Fac_C'];
			//**************************** Battery Algo labels *************************************//
			$Bat_Coolant_Lmt1_C = $calval['Bat_Cool_Lmt1_C'];
			$Bat_Coolant_Lmt2_C = $calval['Bat_Cool_Lmt2_C'];
			//battery condition limts when coolant is below 15 deg
			$Bat_UnderCoolLmt1_Lmt1_C = $calval['Bat_UnderCoolLmt1_Lmt1_C'];
			$Bat_UnderCoolLmt1_Lmt2_C = $calval['Bat_UnderCoolLmt1_Lmt2_C'];
			$Bat_UnderCoolLmt1_Lmt3_C = $calval['Bat_UnderCoolLmt1_Lmt3_C'];
			$Bat_UnderCoolLmt1_Lmt4_C = $calval['Bat_UnderCoolLmt1_Lmt4_C'];
			//battery condition limts when coolant is below 35 deg
			$Bat_OverCoolLmt1_Lmt1_C = $calval['Bat_OverCoolLmt1_Lmt1_C'];
			$Bat_OverCoolLmt1_Lmt2_C = $calval['Bat_OverCoolLmt1_Lmt2_C'];
			$Bat_OverCoolLmt1_Lmt3_C = $calval['Bat_OverCoolLmt1_Lmt3_C'];
			$Bat_OverCoolLmt1_Lmt4_C = $calval['Bat_OverCoolLmt1_Lmt4_C'];
			//battery condition limts when coolant is above 35 deg
			$Bat_OverCoolLmt2_Lmt1_C = $calval['Bat_OverCoolLmt2_Lmt1_C'];
			$Bat_OverCoolLmt2_Lmt2_C = $calval['Bat_OverCoolLmt2_Lmt2_C'];
			$Bat_OverCoolLmt2_Lmt3_C = $calval['Bat_OverCoolLmt2_Lmt3_C'];
			$Bat_OverCoolLmt2_Lmt4_C = $calval['Bat_OverCoolLmt2_Lmt4_C'];
			//*************************** Torque Algo labels *************************************//
			$MaxTorque = $calval['Max_Torque_C'];
			$Trq_Lmt1_C = $calval['Trq_Lmt1_C'];
			$Trq_Lmt2_C = $calval['Trq_Lmt2_C'];
			$Trq_Lmt3_C = $calval['Trq_Lmt3_C'];
			$Trq_Lmt4_C = $calval['Trq_Lmt4_C'];
			$Trq_RPM_PkLmt_C = $calval['Trq_RPM_PkLmt_C'];
			$Trq_EloadOffset_PkLmt_C = $calval['Trq_EloadOffset_PkLmt_C'];
			$Trq_Cool_PkLmt_C = $calval['Trq_Cool_PkLmt_C'];
			//$RPM_Limit2 = $calval['Eff_RPM_Limit2_C'];
			//$RPM_Limit3 = $calval['Eff_RPM_Limit3_C'];
			//$RPM_Limit4 = $calval['Eff_RPM_Limit4_C'];
			$MaxMAP_C = $calval['MAP_Abs_PkLmt3_C'];
			//************************* Coolant Algo labels *************************************//
			$Base_OvrHt_Cool_Lmt_C = $calval['Base_OvrHt_Cool_Lmt_C'];
			$Cool_Temp_Lmt4_C = $calval['Cool_Temp_Lmt4_C'];
			$Cool_Temp_Lmt1percent_C = $calval['Cool_Temp_Lmt1_C'];
			$Cool_Temp_Lmt2percent_C = $calval['Cool_Temp_Lmt2_C'];
			$Cool_Temp_Lmt3percent_C = $calval['Cool_Temp_Lmt3_C'];
			//************************ Fuel Rail Pressure labels *******************************//
			$CRP_Type_C = $calval['CRP_Type_C'];
			$Base_CRP_Lmt_C = $calval['Base_CRP_Lmt_C'];
			$CRP_MinLmtScore3_C = $calval['CRP_MinLmt3_C'];
			$CRP_MinLmtScore1_C = $calval['CRP_MinLmt1_C'];
			$CRP_MaxLmtScore3_C = $calval['CRP_MaxLmt3_C'];
			$CRP_MaxLmtScore1_C = $calval['CRP_MaxLmt1_C'];
			$CRP_RPM_PkLmt_C = $calval['CRP_RPM_PkLmt_C'];
			$CRP_RPMOffset_PkLmt_C = $calval['CRP_RPMOffset_PkLmt_C'];
			$CRP_ELoadOffset_PkLmt_C = $calval['CRP_ELoadOffset_PkLmt_C'];
			//******************* Turbocharger Manifold Air Pressure labels *******************************//
			$MAP_Dif_IdleLmt3_C = $calval['MAP_Dif_IdleLmt3_C'];
			$MAP_Dif_IdleLmt1_C = $calval['MAP_Dif_IdleLmt1_C'];
			$MAP_Abs_PkLmt_C = $calval['MAP_Abs_PkLmt_C'];
			$MAP_Dif_PkLmt_C = $calval['MAP_Dif_PkLmt_C'];
			$MAP_Abs_PkLmt3old_C = $calval['MAP_Abs_PkLmt3_C'];
			$MAP_Abs_PkLmt1_C = $calval['MAP_Abs_PkLmt1_C'];
			$MAP_Dif_PkLmt3old_C = $calval['MAP_Dif_PkLmt3_C'];
			$MAP_Dif_PkLmt1_C = $calval['MAP_Dif_PkLmt1_C'];
			$MAP_RPM_PkLmt_C = $RPM_Limit2;			//This is the limit above which peak MAP will be got
			$MAP_RPMOffset_PkLmt_C = $calval['MAP_RPMOffset_PkLmt_C'];
			$MAP_EloadOffset_PkLmt_C = $calval['MAP_EloadOffset_PkLmt_C'];
			//****************** Air System Manifold Air Pressure labels **************************//
			$MAP_Air_IdleLmt_C = $calval['MAP_Air_IdleLmt_C'];
			$MAP_Air_IdleLmt3_C = $calval['MAP_Air_IdleLmt3_C'];
			$MAP_Air_IdleLmt1_C = $calval['MAP_Air_IdleLmt1_C'];
			$MAP_Air_PkLmt_C = $calval['MAP_Air_PkLmt_C'];
			$MAP_Air_PkLmt3_C = $calval['MAP_Air_PkLmt3_C'];
			$MAP_Air_PkLmt1_C = $calval['MAP_Air_PkLmt1_C'];
			$MAP_Air_EloadOffset_PkLmt_C = $calval['MAP_Air_EloadOffset_PkLmt_C'];
			$MAP_Air_RPM_LwrLmt_C = $calval['MAP_Air_RPM_LwrLmt_C'];
			$MAP_Air_RPM_PkLmt_C = $calval['MAP_Air_RPM_PkLmt_C'];
			$Max_Air_Trq_RPM_C = $calval['Max_Air_Trq_RPM_C'];
			$Multi_Cyl_C = $calval['Multi_Cyl_C'];
			//**************** RPM Oscillation Anomaly Analysis labels **********************//
			$Osc_RPM_Lmt1_C = $calval['Osc_RPM_Lmt1_C'];
			$Osc_RPM_Lmt2_C = $calval['Osc_RPM_Lmt2_C'];
			$Osc_RPM_Lmt3_C = $calval['Osc_RPM_Lmt3_C'];
			$Osc_RPM_Lmt4_C = $calval['Osc_RPM_Lmt4_C'];
			$Osc_EloadOffset_Lmt_C = $calval['Osc_EloadOffset_Lmt_C'];
			$Eload_Idle_Warm_C = $calval['ELoad_Idle_Warm_C'];
			$Eload_Idle_Ld1_C = $calval['ELoad_Idle_Ld1_C'];
			$Eload_Idle_Ld2_C = $calval['ELoad_Idle_Ld2_C'];
		}
		$MAPrpmValA = $RPM_Idle_Warm_C - $MAP_RPMOffset_PkLmt_C;
		$MAPrpmValB = $RPM_Idle_Warm_C + $MAP_RPMOffset_PkLmt_C;
		$MAPrpmValA1 = $RPM_Idle_Ld1_C - $MAP_RPMOffset_PkLmt_C;
		$MAPrpmValB1 = $RPM_Idle_Ld1_C + $MAP_RPMOffset_PkLmt_C;
		$MAPrpmValA2 = $RPM_Idle_Ld2_C - $MAP_RPMOffset_PkLmt_C;
		$MAPrpmValB2 = $RPM_Idle_Ld2_C + $MAP_RPMOffset_PkLmt_C;
		$MAPPeakrpmValA = $Max_Air_Trq_RPM_C - $MAP_Air_RPM_LwrLmt_C;
		$MAPPeakrpmValB = $Max_Air_Trq_RPM_C + $MAP_Air_RPM_PkLmt_C;
		$rpmValA = $RPM_Idle_Warm_C - $CRP_RPMOffset_PkLmt_C;
		$rpmValB = $RPM_Idle_Warm_C + $CRP_RPMOffset_PkLmt_C;
		$rpmValA1 = $RPM_Idle_Ld1_C - $CRP_RPMOffset_PkLmt_C;
		$rpmValB1 = $RPM_Idle_Ld1_C + $CRP_RPMOffset_PkLmt_C;
		$rpmValA2 = $RPM_Idle_Ld2_C - $CRP_RPMOffset_PkLmt_C;
		$rpmValB2 = $RPM_Idle_Ld2_C + $CRP_RPMOffset_PkLmt_C;

		/********************************************************************************************
		 * STEP2: Compute Modelled PIDs
		 * ******************************************************************************************/
		//print_r($iLoopData);
		foreach($iLoopData as & $currRowData)
		{
			//print_r($currRowData);
			//Get value for various PIDs in current row
			$obdCoolantTemperature = $currRowData['F1'];
			$map = $currRowData['F3'];
			$obdManifoldPressure = $map;
			$rpm = $currRowData['F4'];
			$obdRpm = $rpm;
			$vehspd = $currRowData['F5'];
			$maf = $currRowData['F6'];
			$throtPos = $currRowData['F7'];
			$distWithMIL = $currRowData['F8'];
			$engineState = $currRowData['F16']; 
			$obdIntakeTemperature = $currRowData['F18'];
			$inTakeTemp = $obdIntakeTemperature + 273;
			$eLoad = $currRowData['F19'];
			$obdEngineLoad = $eLoad;
			$batVolt = $currRowData['F34'];
			$actEquiRatio = $currRowData['F35'];
			$ratio = $actEquiRatio;
			$flSys = $currRowData['F36'];
			$crp = $currRowData['F37'];
			$obdCommandedEGR = $currRowData['F38'];
			$baro_value = $currRowData['F41'];
			$accPosD = $currRowData['F42'];
			$currtimestamp = $currRowData['time_s'];
			$fuelFlow = $currRowData['Ffuelflow_l'];
			
			//Get current engine state
			$calcEngState = $this -> calcEngStateNew($devId,$engineState,$rpm,$vehspd,$vType,$flSys,$eLoad);
			$currRowData['F43'] = $calcEngState;
			// echo "\n Chk pt 0. im here calcEngState = $calcEngState \n";
			
			//Get Drive sprint id
			$Fsprint = $driveID;
			$currRowData['Fsprint'] = $Fsprint;
			// echo"\n Chk pt 1. im here $Fsprint \n";
			//$postVal = $this -> devFlowInfoAPI($devId,$currRowData,'',$fspLast,$calval,$count);	
			$count++;
			if($rpm > 0)
			{
				$countstats++;
			}
			else
			{
				//Do nothing
			}
			/********************************************************************************************
			 * STEP 2.1 : Compute Distance Travelled
			 * ******************************************************************************************/
			//Distance Travelled (km) = Previous Distance (km) + (Velocity (km/hr) * (Current Time Stamp (s) – Previous Time Stamp (s)) /3600
			if($oldtimestamp == 0)
			{
				$oldtimestamp = $currtimestamp;
				$firsttimestamp = $oldtimestamp;
			}
			$timedif = ($currtimestamp - $oldtimestamp);
			if($timedif < 0)
			{
				$timedif = 0;
			}
			else
			{
				//do nothing
			}
			$timedif = $timedif / 1000;		//divided by 1000 to get time in seconds
			//Get Distance Travelled
			$currRowData['Fdist'] += (($vehspd * $timedif) / 3600);
			
			$oldtimestamp = $currtimestamp;
			//Get Duration of Drive in seconds
			$currRowData['F49'] = (($currtimestamp - $firsttimestamp) / 1000);
			$totaldur = $currRowData['F49'];
			//Get Duration in Idle
			if($calcEngState == 2)
			{
				$idletime += ($timedif);
				$currRowData['F65'] = $idletime;
			}
			//Get Duration and Distance Travelled in Overrun 
			elseif($calcEngState == 4)
			{
				//Time spent in overrun
				$ovrruntime += ($timedif);
				$currRowData['F61'] = $ovrruntime;
				
				//Distance covered in overrun
				$ovrrundist += (($vehspd * $timedif) / 3600);
				$currRowData['F63'] = $ovrrundist;
			}
			//echo "ovrrundist = $ovrrundist | ovrruntime = $ovrruntime | idletime = $idletime\n";
			
			/********************************************************************************************
			 * STEP 2.2 : Fuel FLow, Power and Torque Calculations 
			 * ******************************************************************************************/
			$pi =  3.14159;
			$mm = 28.97;
			$r = 8.314;		
			if($baro_value == 0 || $baro_value == "")
			{
				//Calculate baro pressure if not available
				$altbaro = $currRowData['alt'];
				if(($altbaro == 0) || ($altbaro == ""))
				{
					$altbaro = 810;
				}
				else
				{
					//Do nothing
				}
				$exponent = ($altbaro * $gacc) / ($gasconst * $inTakeTemp);
				$calBaro = $stdBaro * exp(-$exponent);
				$calBaroP = floor($calBaro / 1000);
				$calBaroP = ($calBaroP - 1);
				$currRowData['F41'] = $calBaroP;
				$obdBarometricPressure = $calBaroP;
				//echo "altbaro = $altbaro | exponent = $exponent | calBaroP = $calBaroP | obdBarometricPressure = $obdBarometricPressure \n";
			}
			else
			{
				$obdBarometricPressure = $baro_value;
			}
			
			//********************----------------New MAF to MAP Formula Start--------------------********************//
			if($MAP_Valid_C == 1)
			{
				if($obdManifoldPressure <= 0 || $obdManifoldPressure == "")
				{
					if($maf > 0 && $MAF_Valid_C == 1)
					{
						//*****************==================Volumetric Efficiency calculation start============================*****************//
						//Diesel Engine Volumetric Efficiency
						if($vType == 2)
						{
							if($MAP_Type_C == "Differential")
							{
							 	if($eLoad <= 55)
								{
									$fact11 = pow($eLoad, 4);
									$fact21 = pow($eLoad, 3);
									$fact31 = pow($eLoad, 2);
									$fact41 = $eLoad;
									
									$fact1 = (0.000004 * $fact11);
									$fact2 = (0.0021 * $fact21);
									$fact3 = (0.1497 * $fact31);
									$fact4 = (1.9792 * $fact41);
									
									$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
									//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
								}
								elseif($eLoad <= 85)
								{
									$ve = 98 + (0.08 * $eLoad);
								}
								elseif($eLoad <= 98)
								{
									$ve = 99 + (0.05 * $eLoad);
								}
								else
								{
									$ve = 99 + (0.04 * $eLoad);
								}
							}
							else
							{
								//This condition is added so that the calculation is proper even when IAT is not read/supported
								if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
								{
									$obdIntakeTemperature = $obdCoolantTemperature;
								}
								else
								{
									//Do nothing
								}
								$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
								$barofactor = (101.3 / $obdBarometricPressure);
				
								if($calcEngState == 2)
								{
									if($obdIntakeTemperature < 20)
									{
										$ve1 = (34.4803 * $factor) - 0.2799;
										$ve = $ve1 * $barofactor;
										$ve = round($ve,0);
									}
									else
									{
										$ve1 = (34.6803 * $factor) - 0.2799;
										$ve = $ve1 * $barofactor;
										$ve = round($ve,0);
									}
								}
								else
								{
									if($vehspd > 20)
									{
										$ve = (-0.55 * $factor * $factor) + (11.35 * $factor) + 44.398;
										$ve = round($ve,0);
									}
									else
									{
										$ve = (-0.65 * $factor * $factor) + (11.35 * $factor) + 38.398;
										$ve = round($ve,0);
									}
								}
							}
						}
						
						//Petrol Engine Volumetric Efficiency
						else
						{
							if($Turbo_Avail_C == "No")
							{
								if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
								{
									$obdIntakeTemperature = $obdCoolantTemperature;
								}
								else
								{
									//Do nothing
								}
								$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
								$barofactor = (101.3 / $obdBarometricPressure);
					
								if($calcEngState == 2)
								{
									if($obdIntakeTemperature < 20)
									{
										$ve1 = (91.9803 * $factor) - 0.2799;
										$ve = $ve1 * $barofactor;
										$ve = round($ve,0);
									}
									else
									{
										$ve1 = (92.1803 * $factor) - 0.2799;
										$ve = $ve1 * $barofactor;
										$ve = round($ve,0);
									}
								}
								else
								{
									if($vehspd > 20)
									{
										$ve = (68.03 * $factor * $factor) + (-325.78 * $factor) + 475.32;
										$ve = round($ve,0);
									}
									else
									{
										$ve = (68.03 * $factor * $factor) + (-325.78 * $factor) + 375.32;
										$ve = round($ve,0);
									}
								}
							}
							else
							{
								if($calcEngState != 4)
								{
									if($eLoad >= 1 &&  $eLoad <= 10)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_1_10_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 11 &&  $eLoad <= 20)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_11_20_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 21 &&  $eLoad <= 30)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_21_30_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 31 &&  $eLoad <= 40)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_31_40_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 41 &&  $eLoad <= 50)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_41_50_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 51 &&  $eLoad <= 60)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_51_60_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 61 &&  $eLoad <= 80)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_61_80_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 81 &&  $eLoad <= 90)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_81_90_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 91 &&  $eLoad <= 94)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_91_94_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 95 && $eLoad <= 98)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_95_98_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 99 &&  $eLoad <= 100)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
									}
									else
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
									}
								}
								else
								{
									$ve = $Ovr_Run_VolEff_C;
								}
							}
						}
						//********************===================Volumetric Efficiency Calculations End===================******************//
						
					 	$CurHmdty = 50;
						$CurHumiper = $CurHmdty/100;
						
						$num = (7.5 * $obdIntakeTemperature);
						$denom = ($obdIntakeTemperature + 237.3);
						$divFact = $num/$denom;
						$fact = pow(10, $divFact);
						$pv = $CurHumiper * ((6.1078 * $fact)/10);
		            	
			 			//	Density Calculation
			            $density = $maf * (100 * 120) / ($rpm * $ve * $ed * 1000);
			 			
						$MAPfact = ($density * (8.314 * ($obdIntakeTemperature + 273))) - ($pv * 0.018016);
					 	$pd = $MAPfact / 0.028964;
					 	$map = $pd + $pv;
						
						//echo "CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | density = $density | MAPfact = $MAPfact | pd = $pd | map = $map |";
						$map = round($map,0);
						
						$PressDiff = $obdBarometricPressure - 91;
						$currMaxMAP = $MAP_Abs_PkLmt3old_C + $PressDiff;
						if(($Turbo_Avail_C == "Yes") && ($calcEngState == 3) && ($map > $currMaxMAP))
						{
							$map = $currMaxMAP;
						}
						elseif(($vType == 2) && ($calcEngState == 3) && ($map < $obdBarometricPressure))
						{
							if($obdCoolantTemperature < 55)
							{
								$map = $obdBarometricPressure;
							}
							elseif($obdCoolantTemperature < 72)
							{
								$map = $obdBarometricPressure + 1;
							}
							else
							{
								$map = $obdBarometricPressure + 2;
							}
						}
						elseif($calcEngState == 4)
						{
							$map = "";
						}
						elseif($rpm <= 500)
						{
							$map = $obdBarometricPressure;
						}
						elseif(($map < 0) || (($map > $obdBarometricPressure) && ($vType == 1) && ($Turbo_Avail_C == "No")))
						{
							$map = $obdBarometricPressure;
						}
						else
						{
							//Do nothing;
						}
						$currRowData['F3'] = $map;
					}
					else
					{
						//do nothing;	
					}
				}
				else
				{
					//do nothing;	
				}
			}
			else
			{
				if($maf > 0 && $MAF_Valid_C == 1)
				{
					//*****************==================Volumetric Efficiency calculation start============================*****************//
					//Diesel Engine Volumetric Efficiency
					if($vType == 2)
					{
						if($MAP_Type_C == "Differential")
						{
						 	if($eLoad <= 55)
							{
								$fact11 = pow($eLoad, 4);
								$fact21 = pow($eLoad, 3);
								$fact31 = pow($eLoad, 2);
								$fact41 = $eLoad;
								
								$fact1 = (0.000004 * $fact11);
								$fact2 = (0.0021 * $fact21);
								$fact3 = (0.1497 * $fact31);
								$fact4 = (1.9792 * $fact41);
								
								$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
								//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
							}
							elseif($eLoad <= 85)
							{
								$ve = 98 + (0.08 * $eLoad);
							}
							elseif($eLoad <= 98)
							{
								$ve = 99 + (0.05 * $eLoad);
							}
							else
							{
								$ve = 99 + (0.04 * $eLoad);
							}
						}
						else
						{
							//This condition is added so that the calculation is proper even when IAT is not read/supported
							if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
							{
								$obdIntakeTemperature = $obdCoolantTemperature;
							}
							else
							{
								//Do nothing
							}
							$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
							$barofactor = (101.3 / $obdBarometricPressure);
			
							if($calcEngState == 2)
							{
								if($obdIntakeTemperature < 20)
								{
									$ve1 = (34.4803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
								else
								{
									$ve1 = (34.6803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
							}
							elseif($calcEngState == 4)
							{
								$power = pow(10, -11);
								$ve = (-3 * $power * $factor * $factor) + (11.35 * $factor) + 27.398;
								$ve = round($ve,0);
							}
							else
							{
								if($vehspd > 20)
								{
									// $power = pow(10, -11);
									$ve = (-0.55 * $factor * $factor) + (11.35 * $factor) + 44.398;
									$ve = round($ve,0);
								}
								else
								{
									// $power = pow(10, -11);
									$ve = (-0.65 * $factor * $factor) + (11.35 * $factor) + 38.398;
									$ve = round($ve,0);
								}
							}
						}
					}
					//Petrol Engine Volumetric Efficiency
					else
					{
						if($Turbo_Avail_C == "No")
						{
							if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
							{
								$obdIntakeTemperature = $obdCoolantTemperature;
							}
							else
							{
								//Do nothing
							}
							$factor = (($maf * ($obdIntakeTemperature + 273.15)) / ($rpm * $ed));
							$barofactor = (101.3 / $obdBarometricPressure);
				
							if($calcEngState == 2)
							{
								if($obdIntakeTemperature < 20)
								{
									$ve1 = (91.9803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
								else
								{
									$ve1 = (92.1803 * $factor) - 0.2799;
									$ve = $ve1 * $barofactor;
									$ve = round($ve,0);
								}
							}
							else
							{
								if($vehspd > 20)
								{
									// $ve = (70.03 * $factor * $factor) + (-328.78 * $factor) + 475.32;
									$ve = (68.03 * $factor * $factor) + (-325.78 * $factor) + 475.32;
									$ve = round($ve,0);
								}
								else
								{
									// $ve = (70.03 * $factor * $factor) + (-328.78 * $factor) + 375.32;
									$ve = (68.03 * $factor * $factor) + (-325.78 * $factor) + 375.32;
									$ve = round($ve,0);
								}
							}
						}
						else
						{
							if($calcEngState != 4)
							{
								if($eLoad >= 1 &&  $eLoad <= 10)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_1_10_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_1_10_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 11 &&  $eLoad <= 20)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_11_20_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_11_20_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 21 &&  $eLoad <= 30)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_21_30_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_21_30_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 31 &&  $eLoad <= 40)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_31_40_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_31_40_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 41 &&  $eLoad <= 50)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_41_50_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_41_50_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 51 &&  $eLoad <= 60)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_51_60_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_51_60_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 61 &&  $eLoad <= 80)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_61_80_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_61_80_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 81 &&  $eLoad <= 90)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_81_90_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_81_90_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 91 &&  $eLoad <= 94)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_91_94_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_91_94_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 95 && $eLoad <= 98)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_95_98_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_95_98_RPMLimit5_C;
									}
								}
								elseif($eLoad >= 99 &&  $eLoad <= 100)
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
								}
								else
								{
									if($obdRpm < $RPM_Limit1)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit1_C;
									}
									elseif($obdRpm <= $RPM_Limit2)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit2_C;
									}
									elseif($obdRpm <= $RPM_Limit3)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit3_C;
									}
									elseif($obdRpm <= $RPM_Limit4)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit4_C;
									}
									elseif($obdRpm <= $RPM_Limit5)
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
									else
									{
										$ve = $VolEff_Eload_99_100_RPMLimit5_C;
									}
								}
							}
							else
							{
								$ve = $Ovr_Run_VolEff_C;
							}
						}	
					}
					//********************===================Volumetric Efficiency Calculations End===================******************//
					
				 	$CurHmdty = 50;
					$CurHumiper = $CurHmdty/100;
					
					$num = (7.5 * $obdIntakeTemperature);
					$denom = ($obdIntakeTemperature + 237.3);
					$divFact = $num/$denom;
					$fact = pow(10, $divFact);
					$pv = $CurHumiper * ((6.1078 * $fact)/10);
	            	
		 			//	Density Calculation
		            $density = $maf * (100 * 120)/ ($rpm * $ve * $ed * 1000);
		 			
					$MAPfact = ($density * (8.314 * ($obdIntakeTemperature + 273))) - ($pv * 0.018016);
				 	$pd = $MAPfact / 0.028964;
				 	$map = $pd + $pv;
					
					//echo "CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | density = $density | MAPfact = $MAPfact | pd = $pd | map = $map |";
					$map = round($map,0);
					
					$PressDiff = $obdBarometricPressure - 91;
					$currMaxMAP = $MAP_Abs_PkLmt3old_C + $PressDiff;
					if(($Turbo_Avail_C == "Yes") && ($calcEngState == 3) && ($map > $currMaxMAP))
					{
						$map = $currMaxMAP;
					}
					elseif(($vType == 2) && ($calcEngState == 3) && ($map < $obdBarometricPressure))
					{
						if($obdCoolantTemperature < 55)
						{
							$map = $obdBarometricPressure;
						}
						elseif($obdCoolantTemperature < 72)
						{
							$map = $obdBarometricPressure + 1;
						}
						else
						{
							$map = $obdBarometricPressure + 2;
						}
					}
					elseif($calcEngState == 4)
					{
						$map = "";
					}
					elseif($rpm <= 500)
					{
						$map = $obdBarometricPressure;
					}
					elseif(($map < 0) || (($map > $obdBarometricPressure) && ($vType == 1) && ($Turbo_Avail_C == "No")))
					{
						$map = $obdBarometricPressure;
					}
					else
					{
						//Do nothing;
					}
					$currRowData['F3'] = $map;
				}
				else
				{
					//do nothing;	
				}
			}	
			//********************----------------New MAF to MAP Formula End--------------------*********************//
			
			//********************----------------New MAP to MAF Formula Start--------------------*******************//
			if($Veh_Type_C != 2)	//Veh_Type_C = 2 :- is for trucks. MAF is not supported but fuel flow is directly got. So don't calculate.
			{
				if($MAF_Valid_C == 1)
				{
					if($maf <= 0 || $maf == "")
					{
						if($obdManifoldPressure > 0 && $MAP_Valid_C == 1)
						{
							//*****************==================Volumetric Efficiency calculation start============================*****************//
							//Diesel Engine Volumetric Efficiency
							if($vType == 2)
							{
								if($MAP_Type_C == "Differential")
								{
								 	if($eLoad <= 55)
									{
										$fact11 = pow($eLoad, 4);
										$fact21 = pow($eLoad, 3);
										$fact31 = pow($eLoad, 2);
										$fact41 = $eLoad;
										
										$fact1 = (0.000004 * $fact11);
										$fact2 = (0.0021 * $fact21);
										$fact3 = (0.1497 * $fact31);
										$fact4 = (1.9792 * $fact41);
										
										$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
										//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
									}
									elseif($eLoad <= 85)
									{
										$ve = 98 + (0.08 * $eLoad);
									}
									elseif($eLoad <= 98)
									{
										$ve = 99 + (0.05 * $eLoad);
									}
									else
									{
										$ve = 99 + (0.04 * $eLoad);
									}
								}
								else
								{
									if($eLoad <= 35)
									{
										$ve = 92;
										if($obdIntakeTemperature > 50)
										{
											$ve = $ve + 5;
										}
										else
										{
											//do nothing;
										}
									}
									elseif($eLoad <= 55)
									{
										$fact11 = pow($eLoad, 4);
										$fact21 = pow($eLoad, 3);
										$fact31 = pow($eLoad, 2);
										$fact41 = $eLoad;
										
										$fact1 = (0.0000002 * $fact11);
										$fact2 = (0.00007 * $fact21);
										$fact3 = (0.0082 * $fact31);
										$fact4 = (0.3661 * $fact41);
										
										$ve = -$fact1 + $fact2 - $fact3 + $fact4 + 86.679;
										//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
									}
									elseif($eLoad <= 85)
									{
										$ve = 92 + (0.06 * $eLoad);
									}
									elseif($eLoad <= 98)
									{
										$ve = 94 + (0.05 * $eLoad);
									}
									else
									{
										$ve = 94 + (0.025 * $eLoad);
									}
									
									//Logic added for cars with no intercooler. VE reduces if intercooler not present.	Added on 25/05/2017.
									if($intercoolerPresent == "0")
									{
										if($eLoad <= 25)
										{
											if($vehspd < 10)
											{
												$ve = $ve - 36.5;
											}
											else
											{
												$ve = $ve;
											}
										}
										elseif($eLoad <= 35)
										{
											$ve = $ve - 28;
										}
										elseif($eLoad <= 55)
										{
											$ve = $ve - 20;
										}
										elseif($eLoad <= 85)
										{
											$ve = $ve - 15;
										}
										elseif($eLoad <= 98)
										{
											$ve = $ve - 9;
										}
										else
										{
											$ve = $ve - 2;
										}
									}
									else
									{
										//do nothing
									}
								}
							}
	
							//Petrol Engine Volumetric Efficiency
							else
							{
								if($Veh_Type_C == 0)
								{
									if($calcEngState != 4)
									{
										if($eLoad >= 1 &&  $eLoad <= 10)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_1_10_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_1_10_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_1_10_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_1_10_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_1_10_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_1_10_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 11 &&  $eLoad <= 20)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_11_20_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_11_20_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_11_20_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_11_20_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_11_20_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_11_20_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 21 &&  $eLoad <= 30)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_21_30_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_21_30_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_21_30_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_21_30_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_21_30_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_21_30_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 31 &&  $eLoad <= 40)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_31_40_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_31_40_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_31_40_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_31_40_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_31_40_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_31_40_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 41 &&  $eLoad <= 50)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_41_50_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_41_50_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_41_50_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_41_50_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_41_50_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_41_50_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 51 &&  $eLoad <= 60)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_51_60_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_51_60_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_51_60_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_51_60_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_51_60_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_51_60_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 61 &&  $eLoad <= 80)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_61_80_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_61_80_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_61_80_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_61_80_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_61_80_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_61_80_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 81 &&  $eLoad <= 90)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_81_90_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_81_90_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_81_90_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_81_90_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_81_90_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_81_90_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 91 &&  $eLoad <= 94)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_91_94_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_91_94_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_91_94_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_91_94_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_91_94_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_91_94_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 95 && $eLoad <= 98)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_95_98_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_95_98_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_95_98_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_95_98_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_95_98_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_95_98_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 99 &&  $eLoad <= 100)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_99_100_RPMLimit5_C;
											}
										}
										else
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_99_100_RPMLimit5_C;
											}
										}
									}
									else
									{
										$ve = $Ovr_Run_VolEff_C;
									}
								}
								elseif($Veh_Type_C == 1)
								{
									if($calcEngState != 4)
									{
										if($eLoad >= 1 &&  $eLoad <= 10)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_1_10_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_1_10_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_1_10_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_1_10_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_1_10_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_1_10_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 11 &&  $eLoad <= 20)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_11_20_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_11_20_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_11_20_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_11_20_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_11_20_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_11_20_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 21 &&  $eLoad <= 30)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_21_30_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_21_30_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_21_30_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_21_30_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_21_30_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_21_30_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 31 &&  $eLoad <= 40)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_31_40_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_31_40_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_31_40_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_31_40_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_31_40_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_31_40_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 41 &&  $eLoad <= 50)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_41_50_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_41_50_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_41_50_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_41_50_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_41_50_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_41_50_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 51 &&  $eLoad <= 60)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_51_60_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_51_60_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_51_60_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_51_60_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_51_60_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_51_60_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 61 &&  $eLoad <= 80)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_61_80_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_61_80_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_61_80_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_61_80_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_61_80_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_61_80_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 81 &&  $eLoad <= 90)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_81_90_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_81_90_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_81_90_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_81_90_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_81_90_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_81_90_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 91 &&  $eLoad <= 94)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_91_94_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_91_94_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_91_94_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_91_94_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_91_94_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_91_94_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 95 && $eLoad <= 98)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_95_98_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_95_98_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_95_98_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_95_98_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_95_98_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_95_98_RPMLimit5_C;
											}
										}
										elseif($eLoad >= 99 &&  $eLoad <= 100)
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_99_100_RPMLimit5_C;
											}
										}
										else
										{
											if($obdRpm < $RPM_Limit1)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit1_C;
											}
											elseif($obdRpm <= $RPM_Limit2)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit2_C;
											}
											elseif($obdRpm <= $RPM_Limit3)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit3_C;
											}
											elseif($obdRpm <= $RPM_Limit4)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit4_C;
											}
											elseif($obdRpm <= $RPM_Limit5)
											{
												$ve = $VolEff_Eload_99_100_RPMLimit5_C;
											}
											else
											{
												$ve = $VolEff_Eload_99_100_RPMLimit5_C;
											}
										}
									}
									else
									{
										$ve = $Ovr_Run_VolEff_C;
									}
								}
							}
							//********************===================Volumetric Efficiency Calculations End===================******************//
									
							//MAP to MAF old formula
							//$maf = ($inTakeTemp > 0 && $r > 0) ? ($rpm * $map * $ve * $ed * $mm) / ($inTakeTemp * 120 * 100 * $r) : 0;
							
							$CurHmdty = 50;
							$CurHumiper = $CurHmdty / 100;
							if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
							{
								$obdIntakeTemperature = $obdCoolantTemperature;
							}
							else
							{
								//Do nothing
							}
							
							$num = (7.5 * $obdIntakeTemperature);
							$denom = ($obdIntakeTemperature + 237.3);
							$divFact = $num / $denom;
							$fact = pow(10, $divFact);
							$pv = $CurHumiper * ((6.1078 * $fact)/10);
			            	$pd = $map - $pv;
							
							//	Formula for density
				            $density = (($pd * 0.028964) + ($pv * 0.018016)) / (8.314 * ($obdIntakeTemperature + 273));
							
							//	MAF Calculation
				            $maf = ($rpm * $ve * $ed * $density * 1000) / (100 * 120);
							$maf = round($maf,3);
							//echo "ve = $ve | fact1 = $fact1 | CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | pd = $pd | density = $density | maf = $maf | \n";
							
							$currRowData['F6'] = $maf;
						}
						else
						{
							//do nothing;
						}
					}
					else
					{
						//do nothing;
					}
				}
				else
				{
					if($obdManifoldPressure > 0 && $MAP_Valid_C == 1)
					{
						//*****************==================Volumetric Efficiency calculation start============================*****************//
						//Diesel Engine Volumetric Efficiency
						if($vType == 2)
						{
							if($MAP_Type_C == "Differential")
							{
							 	if($eLoad <= 55)
								{
									$fact11 = pow($eLoad, 4);
									$fact21 = pow($eLoad, 3);
									$fact31 = pow($eLoad, 2);
									$fact41 = $eLoad;
									
									$fact1 = (0.000004 * $fact11);
									$fact2 = (0.0021 * $fact21);
									$fact3 = (0.1497 * $fact31);
									$fact4 = (1.9792 * $fact41);
									
									$ve = $fact1 - $fact2 + $fact3 - $fact4 + 86.86;
									//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
								}
								elseif($eLoad <= 85)
								{
									$ve = 98 + (0.08 * $eLoad);
								}
								elseif($eLoad <= 98)
								{
									$ve = 99 + (0.05 * $eLoad);
								}
								else
								{
									$ve = 99 + (0.04 * $eLoad);
								}
							}
							else
							{
								if($eLoad <= 35)
								{
									$ve = 92;
									if($obdIntakeTemperature > 50)
									{
										$ve = $ve + 5;
									}
									else
									{
										//do nothing;
									}
								}
								elseif($eLoad <= 55)
								{
									$fact11 = pow($eLoad, 4);
									$fact21 = pow($eLoad, 3);
									$fact31 = pow($eLoad, 2);
									$fact41 = $eLoad;
									
									$fact1 = (0.0000002 * $fact11);
									$fact2 = (0.00007 * $fact21);
									$fact3 = (0.0082 * $fact31);
									$fact4 = (0.3661 * $fact41);
									
									$ve = -$fact1 + $fact2 - $fact3 + $fact4 + 86.679;
									//echo "$fact1 | $fact2 | $fact3 | $fact4 \n";
								}
								elseif($eLoad <= 85)
								{
									$ve = 92 + (0.06 * $eLoad);
								}
								elseif($eLoad <= 98)
								{
									$ve = 94 + (0.05 * $eLoad);
								}
								else
								{
									$ve = 94 + (0.025 * $eLoad);
								}
								
								//Logic added for cars with no intercooler. VE reduces if intercooler not present.	Added on 25/05/2017.
								if($intercoolerPresent == "0")
								{
									if($eLoad <= 25)
									{
										if($vehspd < 10)
										{
											$ve = $ve - 36.5;
										}
										else
										{
											$ve = $ve;
										}
									}
									elseif($eLoad <= 35)
									{
										$ve = $ve - 28;
									}
									elseif($eLoad <= 55)
									{
										$ve = $ve - 20;
									}
									elseif($eLoad <= 85)
									{
										$ve = $ve - 15;
									}
									elseif($eLoad <= 98)
									{
										$ve = $ve - 9;
									}
									else
									{
										$ve = $ve - 2;
									}
								}
								else
								{
									//do nothing
								}
							}
						}
				
						//Petrol Engine Volumetric Efficiency
						else
						{
							if($Veh_Type_C == 0)
							{
								if($calcEngState != 4)
								{
									if($eLoad >= 1 &&  $eLoad <= 10)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_1_10_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 11 &&  $eLoad <= 20)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_11_20_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 21 &&  $eLoad <= 30)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_21_30_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 31 &&  $eLoad <= 40)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_31_40_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 41 &&  $eLoad <= 50)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_41_50_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 51 &&  $eLoad <= 60)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_51_60_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 61 &&  $eLoad <= 80)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_61_80_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 81 &&  $eLoad <= 90)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_81_90_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 91 &&  $eLoad <= 94)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_91_94_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 95 && $eLoad <= 98)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_95_98_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 99 &&  $eLoad <= 100)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
									}
									else
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
									}
								}
								else
								{
									$ve = $Ovr_Run_VolEff_C;
								}
							}
							elseif($Veh_Type_C == 1)
							{
								if($calcEngState != 4)
								{
									if($eLoad >= 1 &&  $eLoad <= 10)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_1_10_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_1_10_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 11 &&  $eLoad <= 20)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_11_20_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_11_20_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 21 &&  $eLoad <= 30)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_21_30_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_21_30_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 31 &&  $eLoad <= 40)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_31_40_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_31_40_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 41 &&  $eLoad <= 50)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_41_50_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_41_50_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 51 &&  $eLoad <= 60)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_51_60_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_51_60_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 61 &&  $eLoad <= 80)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_61_80_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_61_80_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 81 &&  $eLoad <= 90)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_81_90_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_81_90_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 91 &&  $eLoad <= 94)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_91_94_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_91_94_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 95 && $eLoad <= 98)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_95_98_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_95_98_RPMLimit5_C;
										}
									}
									elseif($eLoad >= 99 &&  $eLoad <= 100)
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
									}
									else
									{
										if($obdRpm < $RPM_Limit1)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit1_C;
										}
										elseif($obdRpm <= $RPM_Limit2)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit2_C;
										}
										elseif($obdRpm <= $RPM_Limit3)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit3_C;
										}
										elseif($obdRpm <= $RPM_Limit4)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit4_C;
										}
										elseif($obdRpm <= $RPM_Limit5)
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
										else
										{
											$ve = $VolEff_Eload_99_100_RPMLimit5_C;
										}
									}
								}
								else
								{
									$ve = $Ovr_Run_VolEff_C;
								}
							}
						}
						//********************===================Volumetric Efficiency Calculations End===================******************//
						
						//echo "obdIntakeTemperature = $obdIntakeTemperature | map = $map | rpm = $rpm | ed = $ed\n";
						//MAP to MAF old formula	
						//$maf = ($inTakeTemp > 0 && $r > 0) ? ($rpm * $map * $ve * $ed * $mm) / ($inTakeTemp * 120 * 100 * $r) : 0;
						$CurHmdty = 50;
						$CurHumiper = $CurHmdty/100;
						if($obdIntakeTemperature == 0 || $obdIntakeTemperature == "")
						{
							$obdIntakeTemperature = $obdCoolantTemperature;
						}
						else
						{
							//Do nothing
						}
						
						$num = (7.5 * $obdIntakeTemperature);
						$denom = ($obdIntakeTemperature + 237.3);
						$divFact = $num/$denom;
						$fact = pow(10, $divFact);
						$pv = $CurHumiper * ((6.1078 * $fact)/10);
		            	$pd = $map - $pv;
						
						//	Formula for density
			            $density = (($pd * 0.028964) + ($pv * 0.018016)) / (8.314 * ($obdIntakeTemperature + 273));
						
						//	MAF Calculation
			            $maf = ($rpm * $ve * $ed * $density * 1000) / (100 * 120);
						$maf = round($maf,3);
						//echo "ve = $ve | fact1 = $fact1 | CurHumiper = $CurHumiper | num = $num | denom = $denom | fact = $fact | pv = $pv | pd = $pd | density = $density | maf = $maf | \n";
						
						$currRowData['F6'] = $maf;
						//echo "\n ve = $ve | maf = $maf \n";
					}
					else
					{
						//do nothing;
					}
				}	
			}
			else
			{
				//MAF is not supported but fuel flow is directly got. So don't calculate.
			}
			//********************----------------New MAP to MAF Formula End--------------------********************//
			
			/************=========================Efficiency Calculation Start================================*********************/
			$obdManifoldPressure = $map; //so that $obdManifoldPressure has the new calculated value of MAP.
			$n = ($baseN / 100);
			//Changing Efficiencies for Diesel Engines
			if($vType == 2)
			{
				if($obdEngineLoad <= 15)
				{
					if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
					{
						$n = $n - 0.07;
					}
					elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.06; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.03; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.04;
					}
					elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.06;
					}
					else
					{
						$n = $n - 0.07;
					}
				}
				elseif($obdEngineLoad <= 35)
				{
					if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
					{
						$n = $n - 0.06;
					}
					elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.05; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.02; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.03;
					}
					elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.05;
					}
					else
					{
						$n = $n - 0.06;
					}
				}
				elseif($obdEngineLoad <= 55)
				{
					if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
					{
						$n = $n - 0.05;
					}
					elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.04; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.01; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.02;
					}
					elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.04;
					}
					else
					{
						$n = $n - 0.05;
					}
				}
				elseif($obdEngineLoad <= 75)
				{
					if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
					{
						$n = $n - 0.04;
					}
					elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.03; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.02;
					}
					elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.03;
					}
					else
					{
						$n = $n - 0.05;
					}
				}
				else
				{
					if($obdRpm < $RPM_Limit1) // RPM_Limit1 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit1 = 1400
					{
						$n = $n - 0.03;
					}
					elseif($obdRpm <= $RPM_Limit2) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n - 0.02; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit3) // RPM_Limit2 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit2 = 2000
					{
						$n = $n; // since obdRpm = 1400 > RPM_Limit1, but < RPM_Limit2. Thus code will hit here. Hence ƞfinal = ƞ - 1. ƞfinal = 26%
					}
					elseif($obdRpm <= $RPM_Limit4) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.02;
					}
					elseif($obdRpm <= $RPM_Limit5) // RPM_Limit3 to be entered by admin. Provision to be made for the same. for e.g. RPM_Limit3 = 5000
					{
						$n = $n - 0.03;
					}
					else
					{
						$n = $n - 0.05;
					}
					//do nothing
				}

				// $n is calclated. Now find efficiency changes due to xurrent MAP value.
				if ($obdManifoldPressure <= $obdBarometricPressure)
				{
					$n = $n;
				} 
				else
				{
					if($MAP_Type_C == "Absolute")
					{
						if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 140)
						{
							$n = $n + 0.01; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
						}
						else 
						{				
							if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
							{
								if(140 < $obdManifoldPressure && $obdManifoldPressure <= 175)
								{
									$n = $n + 0.03;
								}
								elseif(175 < $obdManifoldPressure && $obdManifoldPressure <= 200)
								{
									$n = $n + 0.04;
								}
								elseif(200 < $obdManifoldPressure && $obdManifoldPressure <= 210)
								{
									$n = $n + 0.05;
								}
								elseif(210 < $obdManifoldPressure && $obdManifoldPressure <= 225)
								{
									$n = $n + 0.06;
								}
								elseif(225 < $obdManifoldPressure && $obdManifoldPressure <= 250)
								{
									$n = $n + 0.07;
								}
								elseif($obdManifoldPressure > 250)
								{
									$n = $n + 0.08;
								}
							}
							else 
							{
								if(140 < $obdManifoldPressure && $obdManifoldPressure <= 175)
								{
									$n = $n + 0.02;
								}
								elseif(175 < $obdManifoldPressure && $obdManifoldPressure <= 200)
								{
									$n = $n + 0.03;
								}
								elseif(200 < $obdManifoldPressure && $obdManifoldPressure <= 210)
								{
									$n = $n + 0.04;
								}
								elseif(210 < $obdManifoldPressure && $obdManifoldPressure <= 225)
								{
									$n = $n + 0.05;
								}
								elseif(225 < $obdManifoldPressure && $obdManifoldPressure <= 250)
								{
									$n = $n + 0.06;
								}
								elseif($obdManifoldPressure > 250)
								{
									$n = $n + 0.07;
								}
							}
						}
					}
					else
					{
						if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 100)
						{
							$n = $n + 0.01; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
						}
						else
						{
							if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
							{
								if(100 < $obdManifoldPressure && $obdManifoldPressure <= 110)
								{
									$n = $n + 0.03;
								}
								elseif(110 < $obdManifoldPressure && $obdManifoldPressure <= 120)
								{
									$n = $n + 0.04;
								}
								elseif(120 < $obdManifoldPressure && $obdManifoldPressure <= 130)
								{
									$n = $n + 0.05;
								}
								elseif(130 < $obdManifoldPressure && $obdManifoldPressure <= 142)
								{
									$n = $n + 0.06;
								}
								elseif(142 < $obdManifoldPressure && $obdManifoldPressure <= 152)
								{
									$n = $n + 0.07;
								}
								elseif($obdManifoldPressure > 152)
								{
									$n = $n + 0.08;
								}
							}
							else 
							{
								if(100 < $obdManifoldPressure && $obdManifoldPressure <= 110)
								{
									$n = $n + 0.02;
								}
								elseif(110 < $obdManifoldPressure && $obdManifoldPressure <= 120)
								{
									$n = $n + 0.03;
								}
								elseif(120 < $obdManifoldPressure && $obdManifoldPressure <= 130)
								{
									$n = $n + 0.04;
								}
								elseif(130 < $obdManifoldPressure && $obdManifoldPressure <= 142)
								{
									$n = $n + 0.05;
								}
								elseif(142 < $obdManifoldPressure && $obdManifoldPressure <= 152)
								{
									$n = $n + 0.06;
								}
								elseif($obdManifoldPressure > 152)
								{
									$n = $n + 0.07;
								}
							}
						}
					}
				}
			}

			//Changing Efficiencies for Petrol Engines	
			elseif($vType == 1)
			{
				if($Turbo_Avail_C == "No")
				{
					if($Injection_Type_C == "Direct") //efficiency of direct injection non turbocharged
					{
						$n = $n + 0.01;
					}
					elseif($Injection_Type_C == "Indirect") //efficiency of indirect injection non turbocharged
					{
						$n = $n;
					}
					
					//For cars
					if($Veh_Type_C == 0)
					{
						//Efficiency change with ENGINE RPM CONDITION and ENGINE LOAD CONDITION
						if($obdEngineLoad < 20)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.06;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
							{
								$n = $n - 0.05;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
							{
								$n = $n - 0.03;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
							{
								$n = $n - 0.01;
							}
							else
							{
								$n = $n - 0.02;
							}
						}
						elseif($obdEngineLoad < 45)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.05;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
							{
								$n = $n - 0.04;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
							{
								$n = $n - 0.02;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
							{
								$n = $n;
							}
							else
							{
								$n = $n - 0.01;
							}
						}
						elseif($obdEngineLoad < 65)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.04;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
							{
								$n = $n - 0.03;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
							{
								$n = $n - 0.01;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
							{
								$n = $n + 0.02;
							}
							else
							{
								$n = $n;
							}
						}
						elseif($obdEngineLoad < 85)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.03;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
							{
								$n = $n - 0.02;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
							{
								$n = $n;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
							{
								$n = $n + 0.03;
							}
							else
							{
								$n = $n + 0.01;
							}
						}
						else
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.02;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
							{
								$n = $n - 0.01;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
							{
								$n = $n + 0.01;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
							{
								if($VVT_Status_C == 0)
								{
									$n = $n + 0.04;
								}
								else
								{
									$n = $n + 0.06;
								}
							}
							else
							{
								$n = $n + 0.02;
							}
						}
					}
					elseif($Veh_Type_C == 1)		//----------------------for Bikes
					{
						if($obdEngineLoad < 20)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.06;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
							{
								$n = $n - 0.05;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
							{
								$n = $n - 0.02;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
							{
								$n = $n + 0.01;
							}
							else
							{
								$n = $n;
							}
						}
						elseif($obdEngineLoad < 45)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.05;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
							{
								$n = $n - 0.04;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
							{
								$n = $n - 0.01;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
							{
								$n = $n + 0.03;
							}
							else
							{
								$n = $n + 0.01;
							}
						}
						elseif($obdEngineLoad < 65)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.03;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
							{
								$n = $n - 0.02;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
							{
								$n = $n;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
							{
								$n = $n + 0.05;
							}
							else
							{
								$n = $n + 0.03;
							}
						}
						elseif($obdEngineLoad < 85)
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
							{
								$n = $n - 0.01;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 3000
							{
								$n = $n;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 4500
							{
								$n = $n + 0.02;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 5500
							{
								$n = $n + 0.07;
							}
							else
							{
								$n = $n + 0.05;
							}
						}
						else
						{
							if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 1400
							{
								$n = $n + 0.02;
							}
							elseif($obdRpm < $RPM_Limit2)// $RPM_Limit1 = 2000
							{
								$n = $n + 0.03;
							}
							elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit1 = 3000
							{
								$n = $n + 0.05;
							}
							elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit1 = 4500
							{
								$n = $n + 0.08;
							}
							else
							{
								$n = $n + 0.06;
							}
						}
					}
				}
				elseif($Turbo_Avail_C == "Yes")
				{
					if($Injection_Type_C == "Direct") //efficiency of direc injection turbocharged
					{
						$n = $n + 0.01;
					}
					elseif($Injection_Type_C == "Indirect") //efficiency of indirec injection turbocharged
					{
						$n = $n;
					}
					
					//Efficiency change with ENGINE RPM CONDITION and ENGINE LOAD CONDITION
					if($obdEngineLoad < 20)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.06;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.05;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n - 0.03;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n - 0.01;
						}
						else
						{
							$n = $n - 0.02;
						}
					}
					elseif($obdEngineLoad < 45)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.05;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.04;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n;
						}
						else
						{
							$n = $n - 0.01;
						}
					}
					elseif($obdEngineLoad < 65)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.04;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.03;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n - 0.01;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n + 0.02;
						}
						else
						{
							$n = $n;
						}
					}
					elseif($obdEngineLoad < 85)
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 2000
						{
							$n = $n - 0.03;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 3000
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 4500
						{
							$n = $n;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 5500
						{
							$n = $n + 0.03;
						}
						else
						{
							$n = $n + 0.01;
						}
					}
					else
					{
						if($obdRpm < $RPM_Limit1) // $RPM_Limit1 = 1400
						{
							$n = $n - 0.02;
						}
						elseif($obdRpm < $RPM_Limit2)// $RPM_Limit2 = 2000
						{
							$n = $n - 0.01;
						}
						elseif($obdRpm <= $RPM_Limit3) // $RPM_Limit3 = 3000
						{
							$n = $n + 0.01;
						}
						elseif($obdRpm <= $RPM_Limit4) // $RPM_Limit4 = 4500
						{
							$n = $n + 0.04;
						}
						else
						{
							$n = $n + 0.02;
						}
					}
					
					// Efficiency change with Boost / MAP
					if ($obdManifoldPressure <= $obdBarometricPressure)
					{
						$n = $n;
					} 
					else
					{
						if (($obdBarometricPressure <= $obdManifoldPressure) && $obdManifoldPressure <= 125)
						{
							$n = $n + 0.005; // Due to the obdManifoldPressure value being 100, the code will	hit here, and hence ƞ = ƞcalc + 2%, ƞ = 25 + 2, ƞ = 27%
						}
						else 
						{				
							if($intercoolerPresent == 1) // Read Intercooler = YES or NO from the Vehicle Info Form
							{
								if(125 < $obdManifoldPressure && $obdManifoldPressure <= 150)
								{
									$n = $n + 0.015;
								}
								elseif(150 < $obdManifoldPressure && $obdManifoldPressure <= 180)
								{
									$n = $n + 0.025;
								}
								elseif($obdManifoldPressure > 180)
								{
									$n = $n + 0.03;
								}
							}
							else 
							{
								if(125 < $obdManifoldPressure && $obdManifoldPressure <= 150)
								{
									$n = $n + 0.01;
								}
								elseif(150 < $obdManifoldPressure && $obdManifoldPressure <= 180)
								{
									$n = $n + 0.02;
								}
								elseif ($obdManifoldPressure > 180)
								{
									$n = $n + 0.025;
								}
							}
						}
					}
				}
			}
			//CHECKS FOR COOLANT TEMPERATURE CONDITION FOR BOTH PETROL AND DIESEL
			if($obdCoolantTemperature < 65)
			{
				$n = $n - 0.01;
			}
			else 
			{
				$n = $n;
			}
			/************=========================Efficiency Calculation End================================*********************/
		
			$qH = 45;
			$pFuel = 0.85;
			if($vType == 1)
			{
				$qH = 43.5;
				$pFuel = 0.737;
			}
			//===================================PETROL ENGINE TORQUE CALCULATIONS START================================================//
			if($vType == 1)
			{
				//-------------- Equivalence Ratio Calculation ----------------------//
				//$ratio = 0;
				if($ratio <= 0 || $ratio == "")	
				{
					$ratio = 1;
					if($calcEngState != 4)
					{
						if($eLoad >= 1 &&  $eLoad <= 10)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_1_10_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_1_10_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_1_10_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_1_10_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_1_10_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_1_10_RPMLimit5_C;
							}
						}
						elseif($eLoad >= 11 &&  $eLoad <= 20)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_11_20_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_11_20_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_11_20_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_11_20_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_11_20_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_11_20_RPMLimit5_C;
							}
						}
						elseif($eLoad >= 21 &&  $eLoad <= 30)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_21_30_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_21_30_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_21_30_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_21_30_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_21_30_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_21_30_RPMLimit5_C;
							}
						}
						elseif($eLoad >= 31 &&  $eLoad <= 40)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_31_40_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_31_40_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_31_40_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_31_40_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_31_40_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_31_40_RPMLimit5_C;
							}
						}
						elseif($eLoad >= 41 &&  $eLoad <= 50)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_41_50_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_41_50_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_41_50_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_41_50_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_41_50_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_41_50_RPMLimit5_C;
							}
						}
						elseif($eLoad >= 51 &&  $eLoad <= 60)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_51_60_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_51_60_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_51_60_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_51_60_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_51_60_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_51_60_RPMLimit5_C;
							}
						}
						elseif($eLoad >= 61 &&  $eLoad <= 80)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_61_80_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_61_80_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_61_80_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_61_80_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_61_80_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_61_80_RPMLimit5_C;
							}
						}
						elseif($eLoad >= 81 &&  $eLoad <= 90)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_81_90_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_81_90_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_81_90_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_81_90_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_81_90_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_81_90_RPMLimit5_C;
							}
						}
						elseif($eLoad >= 91 &&  $eLoad <= 94)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_91_94_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_91_94_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_91_94_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_91_94_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_91_94_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_91_94_RPMLimit5_C;
							}
						}
						elseif($eLoad >= 95 && $eLoad <= 98)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_95_98_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_95_98_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_95_98_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_95_98_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_95_98_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_95_98_RPMLimit5_C;
							}
						}
						elseif($eLoad >= 99 &&  $eLoad <= 100)
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit5_C;
							}
						}
						else
						{
							if($obdRpm < $RPM_Limit1)
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit1_C;
							}
							elseif($obdRpm <= $RPM_Limit2)
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit2_C;
							}
							elseif($obdRpm <= $RPM_Limit3)
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit3_C;
							}
							elseif($obdRpm <= $RPM_Limit4)
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit4_C;
							}
							elseif($obdRpm <= $RPM_Limit5)
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit5_C;
							}
							else
							{
								$ratio = $Eqratio_Eload_99_100_RPMLimit5_C;
							}
						}
					}
					else
					{
						$ratio = $Ovr_Run_EqRatio_C;
					}
					$currRowData['F35'] = $ratio;
				}

				if($calcEngState != 0 || $calcEngState != 7)
				{
					//$sql = "update device set act='I am here $maf | $ratio' where device_id='$deviceID'";if(mysql_query($sql)){exec($cmd . " > /dev/null &");}
					//𝐹𝑢𝑒𝑙 𝐹𝑙𝑜𝑤 (𝑔/𝑠)=𝑀𝐴𝐹  / 𝐸𝑞𝑢𝑖𝑣𝑎𝑙𝑒𝑛𝑐𝑒 𝑅𝑎𝑡𝑖𝑜×14.7			
					$fuelFlowG = ($ratio == 0) ? 0 : ($maf / ($ratio * 14.7));
					$fuelFlowL = $fuelFlowG / ($pFuel * 5 / 18);
					
					if($rpm >= 450) //($eLoad > 0)
					{
						//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100
						$powerK = $fuelFlowG * $qH * $n;
						
						//Power (HP) = Power (kW) * 1.341
						$powerG = $powerK * 1.341;
						
						//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
						$tor = ($rpm > 0) ? ($powerK * 1000 * 60) / (2 * $pi * $rpm) : 0;
						
						// Temperature compensation algo for torque
						$fact = 1;
						$num = (273 + $obdIntakeTemperature);
						$denom = 298;
						$divFact = $num/$denom;
						$fact = pow($divFact,0.6);
						$torcomp = ($tor * $fact);
						$currRowData['Ftorcomp'] = round($torcomp,4);
					}
					else
					{
						$powerK = 0;
						$powerG = 0;
						$tor = 0;
					}
				}
				else
				{
					$fuelFlowG = 0;
					$fuelFlowL = 0;
					$powerK = 0;
					$powerG = 0;
					$tor = 0;
				}
			}
			//====================================PETROL ENGINE TORQUE CALCULATIONS END=================================================//
			//===================================DIESEL ENGINES TORQUE CALCULATIONS START===============================================//
			else
			{
				if($Veh_Type_C != 2)	//Veh_Type_C = 2 :- is for trucks. MAF is not supported but fuel flow is directly got. So don't calculate.
				{
					if($calcEngState != 0 || $calcEngState != 7)
					{
						if($calcEngState == 3)
						{
							//Fuel Flow (L/hr) = 0.0022*(MAF*Engine Load) + 1.0305	
							if($eLoad < 20)
							{
								$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_0_20;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
							}
							elseif($eLoad < 40)
							{
								$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_20_40;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
							}
							elseif($eLoad < 60)
							{
								$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_40_60;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
							}
							elseif($eLoad < 80)
							{
								$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_60_80;		//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
							}
							else
							{
								$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfact_Eload_80_100;	//1.0305 changed to 0.86 on 29.08.2016	constant = 0.78
							}
						}
						elseif($calcEngState == 2)
						{
							//Fuel Flow (L/hr) = 0.0022*(MAF*Engine Load) + 1.0305
							if($eLoad < ($Eload_Idle_Warm_C + 5))
							{
								$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfactidle_Eload1;		//changed on 29.08.2016		constant = 0.30
							}
							else
							{
								$fuelFlowL = (0.0021 * $maf * $eLoad) + $constfactidle_Eload2;
							}
						}
						elseif($calcEngState == 4)
						{
							$fuelFlowL = (0.0021 * $maf * $Ovr_Run_FuelFact_Eload_C) + $Ovr_Run_FuelFact_C;
						}
						else
						{
							$fuelFlowL = 0;
						}
					}
					else 
					{
						$fuelFlowL = 0;
					}
				}
				else
				{
					//echo "fuelFlow = $Veh_Type_C :- $fuelFlow \n";
					$fuelFlowL = $fuelFlow;
				}
				
				//Fuel Flow (g/s) = Fuel Flow (L/hr) * ρfuel * 5/18
				$fuelFlowG = $fuelFlowL * $pFuel * 5 / 18;
				
				//Power and Torque calculations.
				if($rpm >= 450) //($eLoad > 0)
				{
					//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100
					$powerK = $fuelFlowG * $qH * $n;
					
					//Power (HP) = Power (kW) * 1.341
					$powerG = $powerK * 1.341;
					
					//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
					$tor = ($rpm > 0) ? ($powerK * 1000 * 60) / (2 * $pi * $rpm) : 0;
					
					// Temperature compensation algo for torque
					$fact = 1;
					$num = (273 + $obdIntakeTemperature);
					$denom = 298;
					$divFact = $num/$denom;
					$fact = pow($divFact,0.6);
					$torcomp = ($tor * $fact);
					$currRowData['Ftorcomp'] = round($torcomp,4);
					//echo "constfact_Eload_20_40 = $constfact_Eload_20_40 | fuelFlowL = $fuelFlowL | n = $n | powerK = $powerK | powerG = $powerG | tor = $tor \n";
				}
				else 
				{
					//Power (kW) = Fuel Flow (g/s) * QH * ƞ Dev By 100	
					$powerK = 0;
					
					//Power (HP) = Power (kW) * 1.341
					$powerG = 0;
					
					//Torque (Nm) = Power (kW) * 1000 * 60 / (2 * π * RPM)	
					$tor = 0;
				}
			}		
			//====================================DIESEL ENGINES TORQUE CALCULATIONS END===============================================//	
			//Instantaneous Mileage = (km/L) = Vehicle Velocity (km/hr) / Fuel flow (L/hr)		
			$intmil = ($fuelFlowL > 0) ? ($currRowData['F5'] / $fuelFlowL) : 0;
			
			//Average Speed and average fuel flow calculations
			$fFlowSum += $fuelFlowL;
			$vehspdSum += $vehspd;
			if($countstats != 0)
			{
				$vehSpdAvg = ($vehspdSum / $countstats);
				$fFlowAvg = ($fFlowSum / $countstats);
			}
			//Mileage
			$mil = ($fFlowAvg > 0) ? ($vehSpdAvg / $fFlowAvg) : 0;
			$currRowData['F58'] = $mil;
			
			//Fuel Consumed (L) = Distance Travelled (km) / Mileage (km/L)
			$distT = $currRowData['Fdist'];//$this -> callSum($devId,'Fdist',$row['Fsprint']);
			$totalDist += $distT;
			$currRowData['Fdist'] = $totalDist;
			$Ffcon = ($mil > 0) ? ($totalDist / $mil) : 0;
			//echo "$eSpeA | $fFlowAvg = $mil";
			
			//Average Speed (km/hr) = Sum of all speeds of the Vehicle for one drive (km/hr) / No. of instances
			$Favspeed = $vehSpdAvg;
			$currRowData['F53'] = $Favspeed;
			
			//echo "fuelFlowL = $fuelFlowL | fuelFlowG = $fuelFlowG | powerK = $powerK | powerG = $powerG | tor = $tor | inm =  $intmil mil = $mil | dist = $distT fc= $Ffcon | $eSpeA  | $fFlowAvg";
			$currRowData['Ffuelflow_l'] = round($fuelFlowL,4);
			$currRowData['Fpower_hp'] = round($powerG,4);
			$currRowData['Ftorque'] = round($tor,4);	
			$currRowData['Fintmil'] = round($intmil,4);	
			$currRowData['Fmil'] = round($mil,4);
			$currRowData['Ffcon'] = round($Ffcon,4);
			$currRowData['Favspeed'] = round($Favspeed,4);
			$currRowData['ad_dtin'] = date('Y-m-d');
			//echo "\n";
			//print_r($currRowData);
				
			//Calculate max speed
			if($vehspd > $maxVehSpd)
			{
				$maxVehSpd = $vehspd;
				$currRowData['F60'] = $maxVehSpd;
			}
			
			//===================== Combustion Pressure System Algo Value Start ==========================//
			//Calculate max Torque
			if($tor > $maxTorque)
			{
				$maxTorque = $tor;
				$rpmMaxTor = $rpm;
				$mapMaxTor = $map;
				$eloadMaxTor = $eLoad;
				$mafMaxTor = $maf;
				$iatMaxTor = $obdIntakeTemperature;
				$atmpress = $baro_value;
				$latMaxTor = $currRowData['lat'];
				$lonMaxTor = $currRowData['lon'];
				$altMaxTor = $currRowData['alt'];
			}
			//===================== Combustion Pressure System Algo Value End ==========================//
			
			//===================== Fuel Common Rail Pressure System Algo Value Start ==========================//
			//Calculate max rail pressure
			if(($calcEngState == 3) && ($eLoad > ($Eload_Max_C - $CRP_ELoadOffset_PkLmt_C)) && ($rpm > $CRP_RPM_PkLmt_C))
			{
				if($crp > $maxCRP)
				{
					$maxCRP = $crp;
				}
			}
			//Get Max rail pressure in incoming csv
			elseif(($calcEngState == 3) || ($calcEngState == 4))
			{
				if($crp > $maxCRPcsv)
				{
					$maxCRPcsv = $crp;
					$crpEload = $eLoad;
					$crpRpm = $rpm;
				}
			}
			
			//Take maximum value of rail pressure if test conditions are satisfied.
			if(($maxCRP != 0) && ($maxCRP < $maxCRPcsv))
			{
				//If macCRP is less than maxCRPcsv then we need to use the CRP table and get the relevant value at that RPM and Engine Load. 
				//Therefore put maxcrp to 0 so that maxCRPcsv code can run. 
				$maxCRP = 0;
			}
			//echo "maxCRP = $maxCRP | maxCRPcsv = $maxCRPcsv \n";
			
			//Calculate idle rail pressure
			if(($calcEngState == 2) && ($rpm > $rpmValA) && ($rpm < $rpmValB) && ($obdCoolantTemperature > $Cool_Lmt_C))
			{
				if($idleCRP == 0)
				{
					$idleCRP = $crp;
				}
				if($crp > $idleCRP)
				{
					$idleCRP = $crp;
				}
			}
			elseif(($calcEngState == 2) && ($rpm > $rpmValA1) && ($rpm < $rpmValB1) && ($obdCoolantTemperature > $Cool_Lmt_C))
			{
				if($idleCRP == 0)
				{
					$idleCRP = $crp;
				}
				if($crp > $idleCRP)
				{
					$idleCRP = $crp;
				}
			}
			elseif(($calcEngState == 2) && ($rpm > $rpmValA2) && ($rpm < $rpmValB2) && ($obdCoolantTemperature > $Cool_Lmt_C))
			{
				if($idleCRP == 0)
				{
					$idleCRP = $crp;
				}
				if($crp > $idleCRP)
				{
					$idleCRP = $crp;
				}
			}
			//===================== Fuel Common Rail Pressure System Algo Value End ==========================//
			
			//===================== Turbo/Air System Algo Value Start ==========================//
			//Calculate max MAP
			if($Turbo_Avail_C == "Yes")
			{
				if(($calcEngState == 3) && ($eLoad > ($Eload_Max_C - $MAP_EloadOffset_PkLmt_C)) && ($rpm > $MAP_RPM_PkLmt_C))
				{
					if($maxMAPcon == 0)
					{
						$maxMAPcon = $obdManifoldPressure;
						$baroMaxMAP = $obdBarometricPressure;
					}
					if($obdManifoldPressure > $maxMAP)
					{
						$maxMAPcon = $obdManifoldPressure;
						$baroMaxMAP = $obdBarometricPressure;
					}
				}
				
				//Get Max MAP in incoming csv
				if(($calcEngState == 3) || ($calcEngState == 4))
				{
					if($obdManifoldPressure > $maxMAPcsv)
					{
						$maxMAPcsv = $obdManifoldPressure;
						$maxMAPEload = $eLoad;
					}
				}
			}
			else
			{
				if(($rpm > $MAPPeakrpmValA) && ($rpm < $MAPPeakrpmValB) && ($eLoad > ($Eload_Max_C - $MAP_Air_EloadOffset_PkLmt_C)) && ($obdCoolantTemperature > $Cool_Lmt_C))
				{
					//echo "rpm = $rpm | eLoad = $eLoad | obdCoolantTemperature = $obdCoolantTemperature \n";
					if($startNewSegment == 1)
					{
						$startNewSegment = 0;					//So that next point can be considered as P1
						if($obdManifoldPressure > $MAPPeak0)
						{
							//$MAPPeak0 = $obdManifoldPressure;
							$prevMAP = $obdManifoldPressure;
						}
						else
						{
							//Do nothing;
						}
					}
					else
					{
						if ($obdManifoldPressure > $MAPPeak1)
						{
							$MAPPeak1 = $obdManifoldPressure;
							$MAPPeak0 = $prevMAP;
						}
						else
						{
							//Do nothing
						}
					}
				}
				else
				{
					$startNewSegment = 1;
				}
			}
			//echo "Row No: $count | MAP = $obdManifoldPressure | MapPeak0 = $MAPPeak0 | MapPeak1 = $MAPPeak1 | startNewSegment = $startNewSegment\n";
			//Calculate idle manifold air pressure (MAP)
			if($Turbo_Avail_C == "Yes")
			{
				if($idleMAPFlag == 0)		//Idle MAP value not got. Search for idle MAP value
				{
					if(($calcEngState == 2) && ($rpm > $MAPrpmValA) && ($rpm < $MAPrpmValB) && ($obdCoolantTemperature > $Cool_Lmt_C))
					{
						//If idle MAP is 0 initialise with first MAP value
						if($idleMAP == 0)
						{
							$idleMAP = $obdManifoldPressure;
							$baroIdleMAP = $obdBarometricPressure;
						}
						//This is written to pick max MAP value at idle
						if($obdManifoldPressure > $idleMAP)
						{
							$idleMAP = $obdManifoldPressure;
							$baroIdleMAP = $obdBarometricPressure;
							if(($idleMAP >= $baroIdleMAP) && ($idleMAP <= $baroIdleMAP + 5))
							{
								$idleMAPFlag = 1;
							}
						}
					}
					elseif(($calcEngState == 2) && ($rpm > $MAPrpmValA1) && ($rpm < $MAPrpmValB1) && ($obdCoolantTemperature > $Cool_Lmt_C))
					{
						if($idleMAP == 0)
						{
							$idleMAP = $obdManifoldPressure;
							$baroIdleMAP = $obdBarometricPressure;
						}
						//This is written to pick max MAP value at idle
						if($obdManifoldPressure > $idleMAP)
						{
							$idleMAP = $obdManifoldPressure;
							$baroIdleMAP = $obdBarometricPressure;
							if(($idleMAP >= $baroIdleMAP) && ($idleMAP <= $baroIdleMAP + 5))
							{
								$idleMAPFlag = 1;
							}
						}
					}
					elseif(($calcEngState == 2) && ($rpm > $MAPrpmValA2) && ($rpm < $MAPrpmValB2) && ($obdCoolantTemperature > $Cool_Lmt_C))
					{
						if($idleMAP == 0)
						{
							$idleMAP = $obdManifoldPressure;
							$baroIdleMAP = $obdBarometricPressure;
						}
						//This is written to pick max MAP value at idle
						if($obdManifoldPressure > $idleMAP)
						{
							$idleMAP = $obdManifoldPressure;
							$baroIdleMAP = $obdBarometricPressure;
							if(($idleMAP >= $baroIdleMAP) && ($idleMAP <= $baroIdleMAP + 5))
							{
								$idleMAPFlag = 1;
							}
						}
					}
				}
				else		//Idle MAP value recieved. Do not search for it again.
				{
					//Do nothing
				}
			}
			else
			{
				//Do nothing. Idle MAP not required for naturally aspirated engines.
			}
			//echo "maxMAP = $maxMAP | baroMaxMAP = $baroMaxMAP | idleMAP = $idleMAP | baroIdleMAP = $baroIdleMAP \n";
			//echo "($eLoad > ($Eload_Max_C - $MAP_EloadOffset_PkLmt_C)) && ($rpm > $MAP_RPM_PkLmt_C)";
			
			//Calculate max MAF
			if($maf > $maxMAF)
			{
				$maxMAF = $maf;
			}
			//===================== Turbo/Air System Algo Value End ==========================//
			
			//===================== Battery System Algo Value Start ==========================//
			//Calculate/get maximum and minimum battery voltage at ignition on
			if(($calcEngState == 0) && ($obdCoolantTemperature > 70))
			{
				//Minimum battery voltage
				if(($batVolt < $ignOnMinBatVolt) || ($ignOnMinBatVolt == 0))
				{
					$ignOnMinBatVolt = $batVolt;
				}
				//Maximum battery voltage
				if($batVolt > $ignOnMaxBatVolt)
				{
					$ignOnMaxBatVolt = $batVolt;
				}
			}
			/*//Calculate/get min battery voltage
			if($batVolt < $minBatVolt)
			{
				$minBatVolt = $batVolt;
				$batcool = $obdCoolantTemperature;
			}*/
			//Calculate/get max battery voltage during idle/running/overrun
			if(($batVolt > $maxBatVolt) && (($calcEngState == 2) || ($calcEngState == 3) || ($calcEngState == 4)))
			{
				$maxBatVolt = $batVolt;
			}
			//echo "ignOnBatVolt = $ignOnBatVolt | minBatVolt = $minBatVolt | maxBatVolt = $maxBatVolt \n";
			//===================== Battery System Algo Value End ==========================//
			
			//===================== Coolant System Algo Value Start ==========================//
			//Calculate max Coolant Temperature
			if($calcEngState == 3)
			{
				if(($obdCoolantTemperature > $maxCool) && ($vehspd > 10))
				{
					$maxCool = $obdCoolantTemperature;
					$minCool = $maxCool;
					$countcool = $currtimestamp;				//Get timestamp of highest coolant value
					$countcoollow = $currtimestamp + 600000;	//Limit till next 10 mins
				}
				if(($currtimestamp > $countcool) && ($currtimestamp <= $countcoollow))
				{
					if($obdCoolantTemperature < $minCool)
					{
						$minCool = $obdCoolantTemperature;
					}
					// echo "2-- maxCool = $maxCool | minCool = $minCool | count = $currtimestamp | countcool = $countcool | countcoollow = $countcoollow \n";
				}
				//echo "maxCool = $maxCool | minCool = $minCool \n";
			}
			
			/*//If max coolant temperature is above coolant limit 4 then find the minimum temperature the coolant falls to after reaching peaks.
			if($maxCool > $Cool_Temp_Lmt4_C)
			{
				if($prevCool == 0)
				{
					$prevCool = $obdCoolantTemperature;
				}
				if($obdCoolantTemperature < $prevCool)
				{
					//If later found that due to thermostat kicking in, reduces the temp by 1 and disturbs the mincool scenerio,
					//we'll introduce 'no. of points' check in the code by putting counter
					//Initialise minCool with current coolant value
					if($minCool == 0)
					{
						$minCool = $obdCoolantTemperature;
					}
					//Get minimum coolant drop
					if($obdCoolantTemperature < $minCool)
					{
						$minCool = $obdCoolantTemperature;
					}
					else
					{
						//Do nothing
					}
				}
				else
				{
					//Do nothing
				}
				$prevCool = $obdCoolantTemperature;
			}
			else
			{
				$minCool = $maxCool;
			}*/
			//===================== Coolant System Algo Value End ==========================//
			
			//===================== RPM OSCILLATION ANALYSIS Algo Value Start ==========================//
			//Check if Engine State is IDLE and get IDLE RPM
			if($calcEngState == 2 )
			{
				/*calcMonBuf[0] -> Holds the Buffer Status
				 * 0 = Buffer is Empty
				 * 1 = Buffer Contains RPM values at Warm Idle
				 * 2 = Buffer Contains RPM values at ELOAD 1
				 * 3 = Buffer Contains RPM values at ELOAD 2  */
				/* calcMonBuf[1] -> Holds count on values in the buffer
				 * 0 = Counter not started and hence Buffer invalid
				 * 1 to 18 =  Number of RPM Values stored in Buffer */
				
				//Get Engine RPM at Warm IDLE
				if((($calcMonBuf[0] == 0) || ($calcMonBuf[0] == 1)) && ($calcMonBuf[1] < 10))
				{
					if(($obdEngineLoad >= ($Eload_Idle_Warm_C - $Osc_EloadOffset_Lmt_C)) &&
							($obdEngineLoad <= ($Eload_Idle_Warm_C + $Osc_EloadOffset_Lmt_C)) &&
							($obdCoolantTemperature > $Cool_Lmt_C))
					{
						$calcMonBuf[0] = 1 ; // Buffer Contains Warm Idle Values
						$rpmIndex =  $calcMonBuf[1] + 2; // +2 is to offset for index
						$calcMonBuf[$rpmIndex] = $obdRpm; //Update curr RPM Val in Buffer
						$calcMonBuf[1] = $calcMonBuf[1] + 1; // Increment the buffer counter
					}
					else
					{
						// RESET Status and Counter
						$calcMonBuf[0] = 0;
						$calcMonBuf[1] = 0;
						
					}
				}
				else
				{
					//Warm Idle Conditions Not MET
				}
		
				//Get Engine RPM at Warm IDLE under ELOAD1
				if((($calcMonBuf[0] == 0) || ($calcMonBuf[0] == 2)) && ($calcMonBuf[1] < 10))
				{
					//Get Engine RPM at Warm IDLE under ELOAD1
					if(($obdEngineLoad >= ($Eload_Idle_Ld1_C - $Osc_EloadOffset_Lmt_C)) &&
							($obdEngineLoad <= ($Eload_Idle_Ld1_C + $Osc_EloadOffset_Lmt_C)) &&
							($obdCoolantTemperature > $Cool_Lmt_C))
					{
						$calcMonBuf[0] = 2 ; // Buffer Contains Idle Values at ELoad 1
						$rpmIndex =  $calcMonBuf[1] + 2; // +2 is to offset for index
						$calcMonBuf[$rpmIndex] = $obdRpm; //Update curr RPM Val in Buffer
						$calcMonBuf[1] = $calcMonBuf[1] + 1; // Increment the buffer counter
					}
					else
					{
						// RESET Status and Counter
						$calcMonBuf[0] = 0;
						$calcMonBuf[1] = 0;
					}
				}
				else
				{
					//Warm Idle under Load 1 Conditions Not MET
				}
				
				//Get Engine RPM at Warm IDLE under ELOAD2
				if((($calcMonBuf[0] == 0) || ($calcMonBuf[0] == 3)) && ($calcMonBuf[1] < 10))
				{
					//Get Engine RPM at Warm IDLE under ELOAD2
					if(($obdEngineLoad >= ($Eload_Idle_Ld2_C - $Osc_EloadOffset_Lmt_C)) &&
							($obdEngineLoad <= ($Eload_Idle_Ld2_C + $Osc_EloadOffset_Lmt_C)) &&
							($obdCoolantTemperature > $Cool_Lmt_C))
					{
						$calcMonBuf[0] = 3 ; // Buffer Contains Idle Values at ELoad 2
						$rpmIndex =  $calcMonBuf[1] + 2; // +2 is to offset for index
						$calcMonBuf[$rpmIndex] = $obdRpm; //Update curr RPM Val in Buffer
						$calcMonBuf[1] = $calcMonBuf[1] + 1; // Increment the buffer counter
					}
					else
					{
						// RESET Status and Counter
						$calcMonBuf[0] = 0;
						$calcMonBuf[1] = 0;
					}
				}
				else
				{
					//Warm Idle under Load 1 Conditions Not MET
				}
				
				// Once Buffer Reaches a Value of 16 Run the Algorithm.
				if(($calcMonBuf[0] > 0) && ($calcMonBuf[1] >= 10))
				{
					//print_r($calcMonBuf);
					//TODO: Buffer is full so run algorithm and get results
					// Ignore first 4 values and Last 2 values
					// GET Minimum and Maximum RPM Value from  The buffer.
					$MinobdRpm = $calcMonBuf[3];
					$MaxobdRpm = $calcMonBuf[3];
					for($rpmIndex = 4;$rpmIndex < 9; $rpmIndex++)
					{
						if($calcMonBuf[($rpmIndex)] <= $MinobdRpm)
						{
							$MinobdRpm = $calcMonBuf[$rpmIndex];
						}
						if($calcMonBuf[($rpmIndex)] > $MaxobdRpm)
						{
							$MaxobdRpm = $calcMonBuf[$rpmIndex];
						}
					}
					//echo "\n MinobdRpm = $MinobdRpm | MaxobdRpm = $MaxobdRpm \n";
					if(($MinobdRpm != 0) || ($MaxobdRpm != 0))
					{
						$deltaRPMcurr = $MaxobdRpm - $MinobdRpm;
					}
					//Update RPM Oscillation score only if the current value is higher than the previous one
					if($deltaRPMcurr < $deltaRPM)
					{
						$deltaRPM = $deltaRPMcurr;
					}
				}
			}
			else
			{
				//Other Engine States Do nothing
				// RESET BUFFER Status and Counter
				$calcMonBuf[0] = 0;
				$calcMonBuf[1] = 0;
			}
			//====================== RPM OSCILLATION ANALYSIS Algo End ===========================//
			
			//$saveData = new saveForm('device_data',$currRowData);
			//if($saveData -> addData());			
		}
	/********************************************************************************************************************
	 * ************************************** END OF FOR LOOP TO PROCESS EACH ROW ***************************************
	 * *****************************************************************************************************************/
		/*// Put last row into the database
		$saveData = new saveForm('device_data',$currRowData);
		if($saveData -> addData());
		*/
		//print_r($iLoopData);
		unset($currRowData);
		
	/***************************************************************************************************************
	 ************************************** To send mail on recieving file start************************************
	 **************************************************************************************************************/
		/*$head = $this -> dev . " : $type";
		$to = "support@enginecal.com";
		$api = ($api != 2) ? "../"	: "";
		$fileFormat = $api."send-mail.php";*/
	 /***************************************************************************************************************
	 ************************************** To send mail on recieving file end************************************
	 **************************************************************************************************************/
			
		// Copy all data to csv to view in GOT
		//$this -> CreateStatMonDataCSV($iLoopData,$devId,$Fsprint);
		
		//CO2 emmission calculation
		$curCO2 = ($mil > 0) ? ($co2_Cnv_Fac_C / $mil) : 0;
		
		$dt = date('Y-m-d');
		// After all calculations update database
		$computedSatatistics = array();
		$computedSatatistics['device_id'] = $devId;
		$computedSatatistics['dt'] = $dt;
		$computedSatatistics['drive'] = $Fsprint;
		$computedSatatistics['dur'] = $totaldur;
		$computedSatatistics['dist'] = round($totalDist,2);
		$computedSatatistics['max_tor'] = round($maxTorque,2);
		$computedSatatistics['max_sp'] = $maxVehSpd;
		$computedSatatistics['dur_ovr'] = $ovrruntime;
		$computedSatatistics['ful_c'] = round($Ffcon,2);
		$computedSatatistics['avg_ml'] = round($mil,4);
		$computedSatatistics['idt'] = $idletime;
		$computedSatatistics['avg_speed'] = round($Favspeed,2);
		$computedSatatistics['dist_ovr'] = round($ovrrundist,2);
		$computedSatatistics['max_railp'] = $maxCRP;
		$computedSatatistics['max_map'] = $maxMAP;
		$computedSatatistics['max_maf'] = $maxMAF;
		$computedSatatistics['min_batv'] = $ignOnMinBatVolt;
		$computedSatatistics['ad_dt'] = date('Y-m-d H:i:s');
		$computedSatatistics['cur_co2'] = round($curCO2,2);
		//$computedSatatistics['tot_co2'] = $totCO2;		//Commented as no query is being made to get previous value
		$computedSatatistics['nodrive'] = $count;
		$computedSatatistics['dfrm'] = date('Y-m-d H:i:s',substr($firsttimestamp,0,10));//$currtimestamp - $firsttimestamp
		$computedSatatistics['dto'] = date('Y-m-d H:i:s',substr($currtimestamp,0,10));
		//$fileN = "$floder/". date('dmY',strtotime($fRow['time_v'])). ".csv";
		
		// Save data to data drive table
		/*$saveData = new saveForm('device_drive',$computedSatatistics);
		if($saveData -> addData());
		
		// Copy all data to csv to view in GOT
		$this -> CreateStatMonDataCSV($iLoopData,$devId,$Fsprint);
		*/
		//============================================= Monitoring Results ===============================================//
		//Get previous monitoring results for the device
		$sqlvalidApo = "select * from device_monitor where device_id ='$devId' order by id desc limit 0,1";
		$dat = $this -> select($sqlvalidApo);
		$data = $dat[0];
		
		//Get Pressure,Temperature and Humidity from the lat, long and alt
		if($latMaxTor != "" || $lonMaxTor != "" || $altMaxTor != "")
		{
			if($latMaxTor != "null" || $lonMaxTor != "null" || $altMaxTor != "null")
			{
				$url = "http://api.openweathermap.org/data/2.5/weather?lat=$latMaxTor&lon=$lonMaxTor&units=metric&cnt=7&lang=en&appid=8f91e1e5881797629a26f45ab3298ac3";
				$json = file_get_contents($url);
				$data = json_decode($json,true);
				//Get current Temperature in Celsius
				$CurTemp = $data['main']['temp'];
				//echo $data['main']['temp'];
				//Get current Humidity in Percentage
				$CurHumi = $data['main']['humidity'];
				//echo $data['main']['humidity'];
				//Get current Absolute/corrected Pressure in hPa
				//$CurAbsPress = $data['main']['pressure'];
				//echo $data['main']['pressure'];
				//Get current Ground Level Pressure in hPa
				//$CurGrndPress = $data['main']['grnd_level'];
				//echo "CurGrndPress = $CurGrndPress \n";
				
				$CurrBaroPress = $obdBarometricPressure;
			}
			else
			{
				$CurTemp = 20;
				$CurHumi = 91;
				if($atmpress != "")
				{
					$CurrBaroPress = $atmpress;
				}
				else
				{
					$CurrBaroPress = $CurrCityPress;
				}
			}
		}
		else
		{
			$CurTemp = 20;
			$CurHumi = 91;
			if($atmpress != "")
			{
				$CurrBaroPress = $atmpress;
			}
			else
			{
				$CurrBaroPress = $CurrCityPress;
			}
		}
		
		//Get drivability
		$Drivability = "Intermediate";
		if($CurTemp != "" && $CurHumi != "")
		{
			if($CurTemp < 20)
			{
				if($CurHumi > 95)
				{
					$Drivability = "Bad";
				}
				elseif($CurHumi >= 90)
				{
					$Drivability = "Intermediate";
				}
				else
				{
					$Drivability = "Good";
				}
			}
			elseif($CurTemp <= 22.8)
			{
				if($CurHumi > 95)
				{
					$Drivability = "Bad";
				}
				elseif($CurHumi >= 90)
				{
					$Drivability = "Intermediate";
				}
				else
				{
					$Drivability = "Good";
				}
			}
			elseif($CurTemp < 30)
			{
				if($CurHumi > 85)
				{
					$Drivability = "Bad";
				}
				elseif($CurHumi > 60)
				{
					$Drivability = "Intermediate";
				}
				else
				{
					$Drivability = "Good";
				}
			}
			else
			{
				if($CurHumi > 60)
				{
					$Drivability = "Bad";
				}
				elseif($CurHumi > 45)
				{
					$Drivability = "Intermediate";
				}
				else
				{
					$Drivability = "Good";
				}
			}
		}
		else
		{
			$Drivability = "Intermediate";
		}
		
		//===================== Battery Algo Start ========================================//
		//Initalise $batState to previous rating
		$batState = $data['bat_st'];
		if(($ignOnMaxBatVolt != 0) && ($ignOnMaxBatVolt != ""))
		{
			if($ignOnMaxBatVolt > 12.2)
			{
				$batState = 5;
			}
			elseif($ignOnMaxBatVolt > 11.9)
			{
				$batState = 4;
			}
			elseif($ignOnMaxBatVolt > 11.6)
			{
				$batState = 3;
			}
			elseif($ignOnMaxBatVolt > 10.9)
			{
				$batState = 2;
			}
			elseif($ignOnMaxBatVolt < 11)
			{
				$batState = 1;
			}	
		}
		
		//Code for alternator and regulator scoring
		if($maxBatVolt < 12.7)
		{
			$regState = 2;
			if($maxBatVolt < ($ignOnMinBatVolt + 0.4))
			{
				$altState = 1;
				//$batState = $batState + 10;			//This score is to get the alternator comment
			}
			else
			{
				$altState = 2;
				//$batState = ($batState - 1) + 30;	//This score is to get the battery comment and reduce the battery score by 1.
			}
		}
		elseif($batVolt > 15.4)
		{
			$altState = 2;
			$regState = 1;
			//$batState = $batState + 20;	//This score is to get the voltage regulator comment
		}
		else
		{
			$altState = 2;
			$regState = 2;
		}
	
		//Final Battery Health Score
		if($batState == 1 || $batState == 2)
		{
			if($altState == 1)
			{
				if($regState == 1)
				{
					//$batState = $batState + 10; 		//altState and regState both will never have a score 1 together.
					$batOverallState = 11;
				}
				elseif($regState == 2)
				{
					$batOverallState = 11;
				}
			}
			elseif($altState == 2)
			{
				if($regState == 1)
				{
					//$batState = $batState + 20;
					$batOverallState = 22;
				}
				elseif($regState == 2)
				{
					if($maxBatVolt < 12.7)
					{
						//$batState = $batState + 30;
						$batOverallState = 32;
					}
				}
			}
		}
		elseif($batState == 3 || $batState == 4 || $batState == 5)
		{
			if($altState == 1)
			{
				if($regState == 1)
				{
					//$batState = $batState + 10; 		//altState and regState both will never have a score 1 together.
					$batOverallState = 12;
				}
				elseif($regState == 2)
				{
					$batOverallState = 12;
				}
			}
			elseif($altState == 2)
			{
				if($regState == 1)
				{
					//$batState = $batState + 20;
					$batOverallState = 23;
				}
				elseif($regState == 2)
				{
					if($maxBatVolt < 12.7)
					{
						//$batState = $batState + 30;
						$batOverallState = $batState + 30;
					}
				}
			}
		}
		//===================== Battery Algo End =========================================//
		
		//===================== Coolant Algo Start =======================================//
		$Diff = $Base_OvrHt_Cool_Lmt_C - $Cool_Temp_Lmt4_C;
		$Cool_Temp_Lmt1fact = ($Diff * $Cool_Temp_Lmt1percent_C) / 100;
		$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt1fact;
		$Cool_Temp_Lmt2fact = ($Diff * $Cool_Temp_Lmt2percent_C) / 100;
		$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt2fact;
		$Cool_Temp_Lmt3fact = ($Diff * $Cool_Temp_Lmt3percent_C) / 100;
		$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt3fact;
		//Initalise $coolHelScore to previous rating
		$coolHelScore = $data['coolp'];
		
		if($CurTemp == "" || $CurTemp == 0)
		{
			$CurTemp = 25;
		}
		else
		{
			$CurTemp = $CurTemp;
		}
		
		if($CurTemp < 30)
		{
			$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C;
			$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C;
			$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C;
			$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C;
		}
		elseif($CurTemp <= 40)
		{
			$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C + 2;
			$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C + 2;
			$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C + 2;
			$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C + 2;
		}
		else
		{
			$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C + 4;
			$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C + 4;
			$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C + 4;
			$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C + 4;
		}
		
		if($minCool > $Cool_Temp_Lmt1_C)
		{
			$coolHelScore = 1;
		}
		elseif($minCool >= $Cool_Temp_Lmt2_C)
		{
			$coolHelScore = 2;
		}
		elseif($minCool >= $Cool_Temp_Lmt3_C)
		{
			$coolHelScore = 3;
		}
		elseif($minCool > $Cool_Temp_Lmt4_C)
		{
			$coolHelScore = 4;
			//Get Coolant System score with Maximum coolant temp. If max coolant temp is above limit 2 then reduce score by 1
			if($maxCool >= $Cool_Temp_Lmt2_C)
			{
				$coolHelScore = $coolHelScore - 1;
			}
			else
			{
				//Do nothing;
			}
		}
		else
		{
			$coolHelScore = 5;
			//Get Coolant System score with Maximum coolant temp. If max coolant temp is above limit 2 then reduce score by 1
			if($maxCool >= $Cool_Temp_Lmt2_C)
			{
				$coolHelScore = $coolHelScore - 1;
			}
			else
			{
				//Do nothing;
			}
		}
		//===================== Coolant Algo End =======================================//
		
		//===================== Combustion Pressure Algo Start =======================================//
		//Initalise $CombPresState to previous rating
		$CombPresState = $data['comp_pres'];
		$LoadFact1 = 0.95;	//this is a 5% factor for manufacturing variations.
		$LoadFact2 = 0.95;	//this is a 5% factor for accessory loads.
		
		/******************************************************************************************************* /
		 * The compensation for drivability is being removed as the temperature and pressure are already being *
		 * compensated through pressure factor and compensated torque. So we were over compensating with this *
		 * factor. therefore it is now removed. Also the change in atm conditions can be seen in engine load *
		 * values in petrol NA engines.
		/********************************************************************************************************/
		/*if($Drivability == "Good")
		{
			$LoadFact3 = 1;
		}
		elseif($Drivability == "Intermediate")
		{
			$LoadFact3 = 0.98;
		}
		elseif($Drivability == "Bad")
		{
			$LoadFact3 = 0.96;
		}*/
		$LoadFact3 = 1;
		//Pressure correction factor
		//turbocharged petrols do not require the pressure correction factor as seen in the TSI in bangalore and mumbai (max MAP doesn't change)
		if($vType == 1 && $Turbo_Avail_C == "Yes")
		{
			$PresFact = 1; //no correction required
		}
		else
		{
			$PresFact = $obdBarometricPressure / 100;
		}
		
		//Engine Load Factor
		$AvgEloadFact = ($eloadMaxTor / $Eload_Max_C); // Engine Load correction factor
		
		//MAP Factor
		//$MAPFact = ($mapMaxTor / $MaxMAP_C);
		$MAPFact = 1;
		
		//RPM Factor
		$Torquemax3RPM = $rpmMaxTor;
		$RPMDiffPer = 0;
		if($vType == 1)
		{
			if(($Torquemax3RPM > $RPM_Limit3) && ($Torquemax3RPM < $RPM_Limit4))
			{
				//do nothing;
			}
			else
			{
				if($Torquemax3RPM > $RPM_Limit4)
				{
					$RPMDiff = $Torquemax3RPM - $RPM_Limit4;
					$RPMDiffPer = ($RPMDiff * 100) / $RPM_Limit4;
					$RPMDiffPer = round($RPMDiffPer,0);
				}
				else
				{
					//If the max torque is recieved before RPM limit 3.
					$RPMDiff = $RPM_Limit3 - $Torquemax3RPM;
					$RPMDiffPer = ($RPMDiff * 100) / $RPM_Limit3;
					$RPMDiffPer = round($RPMDiffPer,0);
				}
			}
		}
		else
		{
			if(($Torquemax3RPM > $RPM_Limit2) && ($Torquemax3RPM < $RPM_Limit3))
			{
				//do nothing;
			}
			else
			{
				if($Torquemax3RPM > $RPM_Limit3)
				{
					$RPMDiff = $Torquemax3RPM - $RPM_Limit3;
					$RPMDiffPer = ($RPMDiff * 100) / $RPM_Limit3;
					$RPMDiffPer = round($RPMDiffPer,0);
				}
				else
				{
					//If the max torque is recieved before RPM limit 2.
					//do nothing;
				}
			}
		}
		if($RPMDiffPer == 0)
		{
			$RPMFactor = 1;
		}
		elseif($RPMDiffPer <= 3)
		{
			$RPMFactor = 0.97;
		}
		elseif($RPMDiffPer <= 5)
		{
			$RPMFactor = 0.964;
		}
		elseif($RPMDiffPer <= 15)
		{
			$RPMFactor = 0.94;
		}
		elseif($RPMDiffPer <= 27)
		{
			$RPMFactor = 0.91;
		}
		elseif($RPMDiffPer <= 34)
		{
			$RPMFactor = 0.875;
		}
		elseif($RPMDiffPer <= 43.5)
		{
			$RPMFactor = 0.81;
		}
		elseif($RPMDiffPer <= 50)
		{
			$RPMFactor = 0.76;
		}
		elseif($RPMDiffPer <= 60)
		{
			$RPMFactor = 0.72;
		}
		elseif($RPMDiffPer <= 80)
		{
			$RPMFactor = 0.55;
		}
		else
		{
			$RPMFactor = 0.50;
		}
		
		$TotalFact = $LoadFact1 * $LoadFact2 * $LoadFact3 * $PresFact * $AvgEloadFact * $MAPFact * $RPMFactor;
		
		$NewMaxTorque = $MaxTorque * $TotalFact;
		$TorquePer = ($NewMaxTorque != 0 ) ? ($maxTorque / $NewMaxTorque) * 100 : 0;
		//echo "$TotalFact = $LoadFact1 * $LoadFact2 * $LoadFact3 * $PresFact * $AvgEloadFact * $RPMFactor \n";
		//echo "$NewMaxTorque = $MaxTorque * $TotalFact | TorquePer : $TorquePer | MaxTorque : $maxTorque \n";
		
		if($TorquePer < $Trq_Lmt1_C)
		{
			if($TorquePer == 0)
			{
				//do nothing;
			}
			else
			{
				$CombPresState = 1;
			}
		}
		elseif($TorquePer <= $Trq_Lmt2_C)
		{
			$CombPresState = 2;
		}
		elseif($TorquePer <= $Trq_Lmt3_C)
		{
			$CombPresState = 3;
		}
		elseif($TorquePer <= $Trq_Lmt4_C)
		{
			$CombPresState = 4;
		}
		elseif($TorquePer > $Trq_Lmt4_C)
		{
			$CombPresState = 5;
		}
		//===================== Combustion Pressure Algo End =======================================//
		
		//===================== Fuel Rail Pressure Algo Start =======================================//
		$CRP_MinLmt3_C = $Base_CRP_Lmt_C;
		$CRP_MinLmt1_C = $Base_CRP_Lmt_C - $CRP_MinLmtScore1_C;
		$CRP_MaxLmt3_C = $CRP_Type_C - $CRP_MaxLmtScore3_C;
		$CRP_MaxLmt1_C = $CRP_MaxLmt3_C - $CRP_MaxLmtScore1_C;
		//Initalise $IdleCRPSCORE and $PeakCRPSCORE to previous rating
		//$IdleCRPSCORE = $data['idl_crp'];
		//$PeakCRPSCORE = $data['peak_crp'];
		
		//Fuel Idle Score not judged
		/*if($idleCRP != 0)
		{
			if($idleCRP > $CRP_MinLmt3_C)
			{
				$IdleCRPSCORE = 3;
			}
			elseif($idleCRP < $CRP_MinLmt1_C)
			{
				$IdleCRPSCORE = 1;
			}
			else
			{
				$IdleCRPSCORE = 2;
			}
		}
		else
		{
			$IdleCRPSCORE = 0;
		}*/
		
		//Fuel Peak Score
		if(($maxCRP != 0) || ($maxCRP != ""))
		{
			//Idle Score is not judged. Given 3/3 directly
			$IdleCRPSCORE = 3;
			if($maxCRP > $CRP_MaxLmt3_C)
			{
				$PeakCRPSCORE = 3;
			}
			elseif($maxCRP < $CRP_MaxLmt1_C)
			{
				$PeakCRPSCORE = 1;
			}
			else
			{
				$PeakCRPSCORE = 2;
			}
		}
		else
		{
			if(($maxCRPcsv != 0) || ($maxCRPcsv != ""))
			{
				//Idle Score is not judged. Given 3/3 directly
				$IdleCRPSCORE = 3;
				
				$maxCRP = $maxCRPcsv / 100;
				//Get the CRP type for the vehicle to get the csv file name
				$crp = ($CRP_Type_C / 100);
				$crptype = $crp."bar";
				$folder = "../datacsv/". "RailP" . "_". $crptype .".csv";
				//Open the csv file for reading
				$handle = fopen("$folder", "r");
				
				//Get the row number of the Engine Load closest to the Eload at which max CRP is got ($crpEload)
				$i = floor($crpEload / 5) + 1;
				
				if($crpRpm >= 1000)
				{
					//Get the column numbers of the RPM between which the rpm at which max CRP is got ($crpRpm)
					$j1 = floor($crpRpm / 200) - 4;		//-4 is added as the table starts from 1000 and not from 0
					$j2 = $j1 + 1;
					$rpmlimit1 = ($j1 + 4) * 200;
					$rpmlimit2 = ($j2 + 4) * 200;
					// echo "\n i = $i | j1 = $j1 | j2 = $j2 | rpm = $rpm | rpm1 = $rpmlimit1 | rpm2 = $rpmlimit2 \n";
					if($handle)
					{
						while(($row = fgetcsv($handle, 4096, ",")) !== false)
					    {
					    	$csv[] = $row;
					    }
						$crpRpmLimit1 = $csv[$i][$j1];
						$crpRpmLimit2 = $csv[$i][$j2];
						$crp = $crpRpmLimit1 + ((($crpRpmLimit2 - $crpRpmLimit1)/($rpmlimit2 - $rpmlimit1)) * ($crpRpm - $rpmlimit1));
					}
				}
				//RPM of getting peak CRP is very low, lower than 1000rpm 
				else
				{
					//do nothing
				}
				
				//Rate the CRP system based on the CRP value got from the table
				if($maxCRP <= $crp - 60)
				{
					$PeakCRPSCORE = 1;
				}
				elseif($maxCRP <= $crp - 35)
				{
					$PeakCRPSCORE = 2;
				}
				elseif($maxCRP <= $crp - 20)
				{
					$IdleCRPSCORE = 2;
					$PeakCRPSCORE = 3;
				}
				else
				{
					$PeakCRPSCORE = 3;
				}
			}
		}
		//echo "Fuel Sys Hel : CRPIdle: $idleCRP | CRPPeak: $maxCRP | IDLECRPSCORE = $IdleCRPSCORE | PeakCRPSCORE = $PeakCRPSCORE \n";
		//===================== Fuel Rail Pressure Algo End =================================//
		
		//===================== Turbocharged System Algo Start ==============================//
		if($Turbo_Avail_C == "Yes")
		{
			if(($baroIdleMAP == 0) || ($baroIdleMAP == ""))
			{
				$BaroPress = $obdBarometricPressure;
			}
			else
			{
				$BaroPress = $baroIdleMAP;
			}
			
			$PressDiff = $baroMaxMAP - $CurrCityPress;
			if($PressDiff >= 2)
			{
				$PressDiff = $PressDiff - 2;
			}
			else
			{
				//do nothing;
			}
			
			if($idleMAP != 0)
			{
				if($vType == 2)
				{
					if($MAP_Type_C == "Absolute")
					{
						//echo "Turbo idle Score: idleMAP = $idleMAP | BaroPress = $BaroPress \n";
						//Step 3. Check Idle Boost State for Absolute Pressure Sensor
						if($idleMAP >= $BaroPress)
						{
							$IdleBoostSCORE = 3;
						}
						elseif($idleMAP == ($BaroPress - 1))
						{
							$IdleBoostSCORE = 2;
						}
						else//if($MAPIdle < $BaroPress)
						{
							$IdleBoostSCORE = 1;
						}
					}
					else
					{
						//Step 4. Check Idle Boost State for Differential Pressure Sensor
						if($idleMAP >= ($BaroPress - $MAP_Dif_IdleLmt3_C))
						{
							$IdleBoostSCORE = 3;
						}
						elseif($idleMAP < ($BaroPress - $MAP_Dif_IdleLmt1_C))
						{
							$IdleBoostSCORE = 1;
						}
						else
						{
							$IdleBoostSCORE = 2;
						}
					}
				}
				else
				{
					$IdleBoostSCORE = 3;
				}
			}
			else
			{
				$IdleBoostSCORE = 0;
			}
			
			//Peak Score
			if($maxMAPcon != 0)
			{
				//Max MAP got in defined test conditions
				if($maxMAPcon > $maxMAPcsv)
				{
					$maxMAP = $maxMAPcon;
					if($MAP_Type_C == "Absolute")
					{
						if($vType == 2)
						{
							$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C + $PressDiff;
						}
						else
						{
							$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C;
						}
						
						//echo "Turbo Score: maxMAP = $maxMAP | BaroPress = $BaroPress | MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3_C | MAP_Abs_PkLmt1_C = $MAP_Abs_PkLmt1_C \n";
						if($maxMAP >= $MAP_Abs_PkLmt3_C)
						{
							if($maxMAP >= ($MAP_Abs_PkLmt3_C + $MAP_Abs_PkLmt_C))
							{
								$PeakBoostSCORE = 3;
								$msgPeak = "Your MAP values are a little higher than usual. We will keep you posted";
							}
							else
							{
								$PeakBoostSCORE = 3;
							}
						}
						elseif($maxMAP <= ($MAP_Abs_PkLmt3_C - $MAP_Abs_PkLmt1_C))
						{
							$PeakBoostSCORE = 1;
						}
						else
						{
							if($IdleBoostSCORE == 3)
							{
								$diff = $MAP_Abs_PkLmt3_C - $maxMAP;
								if($diff <= 2)
								{
									$IdleBoostSCORE = 2;
									$PeakBoostSCORE = 3;
								}
								else
								{
									$PeakBoostSCORE = 2;
								}
							}
							else
							{
								$PeakBoostSCORE = 2;
							}
						}
						
						if(($PeakBoostSCORE == 1) || ($PeakBoostSCORE == 2))
						{
							if($CombPresState == 4)
							{
								$PeakBoostSCORE = 2;
							}
							elseif($CombPresState == 5)
							{
								$PeakBoostSCORE = 3;
							}
						}
					}
					else
					{
						if($vType == 2)
						{
							$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C + $PressDiff;
						}
						else
						{
							$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C;
						}
						
						if($maxMAP >= $MAP_Dif_PkLmt3_C)
						{
							if($maxMAP >= ($MAP_Dif_PkLmt3_C + $MAP_Dif_PkLmt_C))
							{
								$PeakBoostSCORE = 3;
								$msgPeak = "Your MAP values are a little higher than usual. We will keep you posted";
							}
							else
							{
								$PeakBoostSCORE = 3;
							}
						}
						elseif($maxMAP <= ($MAP_Dif_PkLmt3_C - $MAP_Dif_PkLmt1_C))
						{
							$PeakBoostSCORE = 1;
						}
						else
						{
							if($IdleBoostSCORE == 3)
							{
								$diff = $MAP_Dif_PkLmt3_C - $maxMAP;
								if($diff <= 2)
								{
									$IdleBoostSCORE = 2;
									$PeakBoostSCORE = 3;
								}
								else
								{
									$PeakBoostSCORE = 2;
								}
							}
							else
							{
								$PeakBoostSCORE = 2;
							}
						}
						if(($PeakBoostSCORE == 1) || ($PeakBoostSCORE == 2))
						{
							if($CombPresState == 4)
							{
								$PeakBoostSCORE = 2;
							}
							elseif($CombPresState == 5)
							{
								$PeakBoostSCORE = 3;
							}
						}
					}
				}
				//Max MAP got at lower Eloads
				else
				{
					$maxMAP = $maxMAPcsv;
					if($MAP_Type_C == "Absolute")
					{
						if($vType == 2)
						{
							$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C + $PressDiff;
						}
						else
						{
							$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C;
						}
						
						//echo "Turbo Score: maxMAP = $maxMAP | BaroPress = $BaroPress | MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3_C | MAP_Abs_PkLmt1_C = $MAP_Abs_PkLmt1_C \n";
						if($maxMAPEload <= 70)
						{
							if($maxMAP >= $MAP_Abs_PkLmt3_C - 10)
							{
								$PeakBoostSCORE = 3;
							}
							else
							{
								$PeakBoostSCORE = 0;
							}
						}
						elseif($maxMAPEload <= 85)
						{
							if($maxMAP >= $MAP_Abs_PkLmt3_C - 5)
							{
								$PeakBoostSCORE = 3;
							}
							else
							{
								$PeakBoostSCORE = 0;
							}
						}
					}
					else
					{
						if($vType == 2)
						{
							$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C + $PressDiff;
						}
						else
						{
							$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C;
						}
						
						if($maxMAPEload <= 70)
						{
							if($maxMAP >= $MAP_Abs_PkLmt3_C - 10)
							{
								$PeakBoostSCORE = 3;
							}
							else
							{
								$PeakBoostSCORE = 0;
							}
						}
						elseif($maxMAPEload <= 85)
						{
							if($maxMAP >= $MAP_Abs_PkLmt3_C - 5)
							{
								$PeakBoostSCORE = 3;
							}
							else
							{
								$PeakBoostSCORE = 0;
							}
						}
					}
				}
			}
			//Test Conditions not met
			else
			{
				$PeakBoostSCORE = 0;
			}
			$IdleAirBoostSCORE = 0;
			$PeakAirBoostSCORE = 0;
			//echo "Turbo Sys Hel : MAPIdle: $idleMAP | MAPPeak: $maxMAP | IDLEboostSCORE = $IDLEboostSCORE | PeakBoostSCORE = $PeakBoostSCORE \n";
		}
		//===================== Turbocharged System Algo End ==================================//
		//===================== Air System Algo Start =========================================//
		else
		{
			if(($obdBarometricPressure == 0) || ($obdBarometricPressure == ""))
			{
				$BaroPress = $baroIdleMAP;
			}
			else
			{
				$BaroPress = $obdBarometricPressure;
			}
			
			//$maxairMAP = $maxMAP;
			$MAPvalidity = $this -> getCaliPramVal('MAP_Valid_C',$devId);
			if($MAPvalidity == 1)
			{
				/*$IdleAirBoostSCORE = 3; 	//Air system not judged for NA petrols at idle
				if($maxairMAP != 0)
				{
					if($maxairMAP >= ($BaroPress - $MAP_Air_PkLmt3_C))
					{
						if($maxairMAP >= ($BaroPress + $MAP_Air_PkLmt_C))
						{
							$PeakAirBoostSCORE = 3; 
							$msgPeakAir = "Your MAP values are a little higher than usual. We will keep you posted";
						}
						elseif($maxairMAP == ($BaroPress - $MAP_Air_PkLmt3_C)) 
						{
							$PeakAirBoostSCORE = 3;
							$IdleAirBoostSCORE = 2;
						}
						else
						{
							$PeakAirBoostSCORE = 3;
						}
					}
					elseif($maxairMAP < ($BaroPress - $MAP_Air_PkLmt1_C))
					{
						$PeakAirBoostSCORE = 1;
					}
					elseif($maxairMAP < ($BaroPress - $MAP_Air_PkLmt3_C))
					{
						$PeakAirBoostSCORE = 2;
					}
				}
				else
				{
					$PeakAirBoostSCORE = 0;
					$msgPeakAir = "Conditions not met.";
				}*/
				
				//*************************** Idle Air MAP Score *********************************//
				//echo "MAPPeak0 = $MAPPeak0 | MAPPeak1 = $MAPPeak1 | MAP_Air_IdleLmt3_C = $MAP_Air_IdleLmt3_C | obdBarometricPressure = $obdBarometricPressure \n";
				if($MAPPeak0 != 0)
				{
					if($MAPPeak0 >= ($obdBarometricPressure - $MAP_Air_IdleLmt3_C))
					{
						$IdleAirBoostSCORE = 3;
					}
					elseif($MAPPeak0 < ($obdBarometricPressure - $MAP_Air_IdleLmt1_C))
					{
						$IdleAirBoostSCORE = 1;
					}
					else
					{
						$IdleAirBoostSCORE = 2;
					}
				}
				else
				{
					$IdleAirBoostSCORE = 0;
				}
				//*************************** Peak Air MAP Score *********************************//
				if($MAPPeak1 != 0)
				{
					if($MAPPeak1 >= ($obdBarometricPressure - $MAP_Air_PkLmt3_C))
					{
						$PeakAirBoostSCORE = 3;
						if(($IdleAirBoostSCORE < 3) && ($IdleAirBoostSCORE != 0))
						{
							$IdleAirBoostSCORE = $IdleAirBoostSCORE + 1;
						}
						else
						{
							//Do nothing
						}
					}
					elseif($MAPPeak1 < ($obdBarometricPressure - $MAP_Air_PkLmt1_C))
					{
						$PeakAirBoostSCORE = 1;
					}
					else//if($MAPPeak1 < ($obdBarometricPressure - $MAP_Air_PkLmt3_C))
					{
						$PeakAirBoostSCORE = 2;
					}
				}
				else
				{
					$PeakAirBoostSCORE = 0;
				}
			}
			else
			{
				$IdleAirBoostSCORE = 0;
				$PeakAirBoostSCORE = 0;
				$msgPeakAir = "MAP not supported";
			}
			
			$IdleBoostSCORE = 0;
			$PeakBoostSCORE = 0;
			//echo "Air Sys Hel : MAPIdle: $idleMAP | MAPPeak: $maxMAP | IdleAirBoostSCORE = $IdleAirBoostSCORE | PeakairBoostSCORE = $PeakAirBoostSCORE \n";
		}
		//===================== Air System Algo End ==========================================//
		
		//===================== RPM OSCILLATION ANALYSIS Algo Start ==========================//
		//echo "\n deltaRPM = $deltaRPM \n";
		if($deltaRPM >= $Osc_RPM_Lmt1_C)
		{
			$currRpmOscScore = 1;
		}
		elseif($deltaRPM > $Osc_RPM_Lmt2_C)
		{
			$currRpmOscScore = 2;
		}
		elseif($deltaRPM > $Osc_RPM_Lmt3_C)
		{
			$currRpmOscScore = 3;
		}
		elseif($deltaRPM > $Osc_RPM_Lmt4_C)
		{
			$currRpmOscScore = 4;
		}
		elseif(($deltaRPM <= $Osc_RPM_Lmt4_C) && ($deltaRPM != 0))
		{
			$currRpmOscScore = 5;
		}
		else
		{
			//do nothing
		}
		
		//If score is 4 we give score 5 because of accuracy issue with the incoming data
		if(($currRpmOscScore == 4) && ($CombPresState == 5))
		{
			$currRpmOscScore = 5;
		}
		//If RPM Osc score is 2 and Comb score is greater than 3 we ignore RPM Osc score and check RPM Osc in next drive
		elseif(($currRpmOscScore == 2) && ($CombPresState > 3))
		{
			$currRpmOscScore = "";
		}
		//If RPM Osc score is 1 and Comb score is greater than 2 we ignore RPM Osc score and check RPM Osc in next drive
		elseif(($currRpmOscScore == 1) && ($CombPresState > 2))
		{
			$currRpmOscScore = "";
		}
		//====================== RPM OSCILLATION ANALYSIS Algo End ===========================//
		
		//If turbo score is good then the torque cannot be low. This is due to data missing (slow raster)
		if($CombPresState <= 3)
		{
			if($IdleBoostSCORE == 2 && $PeakBoostSCORE == 3)
			{
				$CombPresState = 3;
			}
			elseif($IdleBoostSCORE == 3 && $PeakBoostSCORE == 3)
			{
				$CombPresState = 4;
			}
		}
		
		// After all calculations update database
		$computedMonRes = array();
		$computedMonRes['device_id'] = $devId;
		$computedMonRes['dt'] = date('Y-m-d');
		$computedMonRes['drive'] = $Fsprint;
		$computedMonRes['dur'] = $totaldur;
		$computedMonRes['press'] = $CurrBaroPress;
		$computedMonRes['temp'] = $CurTemp;
		$computedMonRes['hmdty'] = $CurHumi;
		$computedMonRes['drvblty'] = $Drivability;
		$computedMonRes['ad_dt'] = date('Y-m-d H:i:s');
		$computedMonRes['bat_st'] = $batState;
		$computedMonRes['bat_c'] = $baroIdleMAP;
		$computedMonRes['idlb_c'] = $IdleBoostSCORE;
		$computedMonRes['idlb_m'] = $idleMAP;
		$computedMonRes['pekb_c'] = $PeakBoostSCORE;
		$computedMonRes['pekb_m'] = $maxMAP;
		$computedMonRes['idlbc_v'] = $IdleAirBoostSCORE;
		$computedMonRes['peakb_v'] = $PeakAirBoostSCORE;
		$computedMonRes['peakb_m'] = $maxairMAP;
		$computedMonRes['idl_crp'] = $IdleCRPSCORE;
		$computedMonRes['peak_crp'] = $PeakCRPSCORE;
		$computedMonRes['crpm'] = $idleCRP;
		$computedMonRes['tune_msg'] = $maxCRP;
		$computedMonRes['comp_pres'] = $CombPresState;
		$computedMonRes['eng_block_ej'] = $currRpmOscScore;
		$computedMonRes['erg_cmd_wrm'] = $deltaRPM;
		$computedMonRes['coolp'] = $coolHelScore;
		$computedMonRes['coolm'] = $minCool;
		$computedMonRes['max_drive_torque'] = $maxTorque;
		$computedMonRes['reference_torque'] = $NewMaxTorque;
		
	//print_r($computedMonRes);
	/*$saveData = new saveForm('device_monitor',$computedMonRes);
	if($saveId = $saveData -> addData())
	{
		$this -> setReportData($devId,$dt,$saveId,2);
	}*/
	
		unset($computedSatatistics);
		unset($iLoopData);
	}
	
	function CreateStatMonDataCSV($iLoopData,$devId,$Fsprint)
	{
		//print_r($iLoopData);
		//clearstatcache();
		$folder = "datacsv/".$devId;
		if(!file_exists($folder))
		{
			mkdir($folder,0777,true);
		}
		//echo "\n floder = $floder \n";
		$folder1 = "$folder/".date('dmY',time());
		if(!file_exists($folder1))
		{
			mkdir($folder1,0777,true);
		}
		$fileN = "$folder1/". date('dmY',time()) ."_". $devId ."_". $Fsprint . ".csv";
		//$fileN = "$floder/". date('dmY',time()) ."_". $devId .".csv";
		//echo "\n fileN = $fileN \n";
		
		//Header array for the csv file
		$csvheader = array("device_id","time_s","time_v","lat","lon","alt","","ad_dt","","","","","","obdCoolantTemperature","obdMil",
		"obdManifoldPressure","obdRpm","obdSpeed","obdAirFlowRate","obdThrottle","obdDistanceWithMil","obdFuelLevel","obdVin","obdCurrentDtcs",
		"obdPendingDtcs","obdPermanentDtcs","obdEot","obdRuntimeSinceEngineStart","obdEngineState","obdProtocol","obdIntakeTemperature",
		"obdEngineLoad","obdShortTermFuelTrim1","obdShortTermFuelTrim2","obdCommandedEquivalenceRatio","obdSot","obdRunningVoltage",
		"obdSleepVoltage","obdCommandedThrottleActuator","gpsMode","gpsSatsUsed","gpsCourse","gpsDop","gpsFixType","gpsDop-pdop","gpsDop-vdop",
		"gpsDop-hdop","gpsPosition","gpsPosition-alt","gpsPosition-lon","gpsPosition-lat","ts","Ffuelflow_l","Fpower_hp","Ftorque",
		"obdControlModuleVoltage","obdActualEquivalenceRatio","Fsprint","Fdist","Fintmil","Fmil","Ffcon","Favspeed","obdFuelSystemStatus",
		"obdFuelCommonRailPressure","obdCommandedEGR","obdEGRError","obdOxygenSensorVoltage","obdCurrentDtcs","obdBarometricPressure","obdAccPosD",
		"obdCalcEngineState","obdEot-distance","obdEot-totalTime","obdEot-maxSpeed","obdEot-totalFuelCons","obdEot-idleTime",
		"obdCatalystTemperature","obdIgnition Timing Advance","ad_dtin","corePowerState","obdCalStage","obdDistanceTravelled",
		"obdDriveDuration","","","obdAvgFuelFlow","obdAvgSpeed","","","","obdTestState","obdMileage","obdMaxTorque","obdMaxVehSpeed",
		"obdOvrRunDuration","obdFuelSavedOvrRun","obdOvrRunDistance","obdFuelConsumed","obdIdleTime","obdDistanceSinceErrorCleared",
		"obdDriveQuality","obdActiveAlerts","obdCalcBarometricPressure","Ftorcomp");
		//print_r($csvheader);
		
		$fh = fopen("$fileN",'w');
		# write out the headers
        fputcsv($fh, $csvheader);
        
		//echo "\n fh = $fh \n";
        # write out the data
        foreach($iLoopData as $row)
        {      	
        	array_shift($row);
        	fputcsv($fh, $row);	
        }
        fclose($fh);
		unset($iLoopData);
	}

	function getStatsMonResultData($deviceID,$driveID)
	{
		//Query to check whether the report is active and get the test scores for the report id
		/******************************************************************************************************
			************************************** Start of Statistics **************************************
		 *****************************************************************************************************/
		$sql = "select * from device_drive where device_id ='$deviceID' and drive = '$driveID' order by id desc limit 0,1";
		$dat = $this -> select($sql);
		
		$arrayData = array();
		
		$arrayData['success'] = TRUE;
		$arrayData['driveNum'] = intval($driveID);
				
		$dataStats = array();
		//$dataStats['date'] = $dat[0]['dt'];
		$dataStats['ts'] = $dat[0]['drive']*1000;
		$dataStats['drive_duration'] = intval($dat[0]['dur']);
		$dataStats['drive_dist'] = floatval($dat[0]['dist']);
		$dataStats['max_tor'] = floatval($dat[0]['max_tor']);
		$dataStats['max_speed'] = intval($dat[0]['max_sp']);
		$dataStats['time_ovr'] = intval($dat[0]['dur_ovr']);
		$dataStats['fuel_save_ovr'] = floatval($dat[0]['ful_ovr']);
		$dataStats['fuel_consumed'] = floatval($dat[0]['ful_c']);
		$dataStats['mileage'] = floatval($dat[0]['avg_ml']);
		$dataStats['idle_time'] = intval($dat[0]['idt']);
		$dataStats['avg_speed'] = floatval($dat[0]['avg_speed']);
		$dataStats['dist_ovr'] = floatval($dat[0]['dist_ovr']);	
		$Fsprint = $driveID;
		
		$arrayData['driveStatistics'] = $dataStats;
		
		/******************************************************************************************************
		************************************* Start of Monitoring Results *************************************
		 *****************************************************************************************************/
		$sqlvalidApo = "select * from device_monitor where device_id ='$deviceID' and drive = '$Fsprint' order by id desc limit 0,1";
		$datvalidApo = $this -> select($sqlvalidApo);
		
		if(count($datvalidApo) >= 1)
		{
			//query to get report input given by user
			//$devAct = new devAct($deviceID);
			$veh = $this -> getvehicle($deviceID);
			$vType = $veh[0]['fuel_tpe'];
			//$datAct = new datAct("na");
			
			//$devAct = new devAct($deviceID);
			$devInfo = $this -> getdevice($deviceID);	
			if($deviceID != "" && $devInfo[0] != "")
			{
				//using this piece of code from previous code in order to reuse the below code without any changes
				$dat = $this -> select($sqlvalidApo);
				$data = $dat[0];
	
				//Get the type of vehicle - Petrol or Diesel
				if($vType == 1)
				{
					$fueltpe = "Petrol";
				}
				else
				{
					$fueltpe = "Diesel";
				}
				
				//Battery Health Score
				$batHel = array();
				$batHel['rate'] = "-1";
				$batHel['con'] = "";
				$batHel1['msg'] = "";
				$batHel2['msg'] = "";
				//Get Charging System comments
				if(($data['bat_st'] < 10) && ($data['bat_st'] != "") && ($data['bat_st'] != 0))
				{
					//$data['bat_st'] = $data['bat_st'];
					if($data['bat_st'] == 5)
					{
						$batHel2['msg'] = "";
					}
					else
					{
						$batHel2['msg'] = "Alternator Okay.";
					}
				}
				elseif(($data['bat_st'] < 20) && ($data['bat_st'] != "") && ($data['bat_st'] != 0))
				{
					$data['bat_st'] = $data['bat_st'] - 10;
					$batHel2['msg'] = "Please check the Alternator.";
				}
				elseif(($data['bat_st'] > 20) && ($data['bat_st'] != "") && ($data['bat_st'] != 0))
				{
					$data['bat_st'] = $data['bat_st'] - 20;
					$batHel2['msg'] = "Please check the Alternator Voltage Regulator.";
				}
				//Get Battery System comments
				if($data['bat_st'] == 1)
				{
					$batHel['rate'] = "1";
					$batHel['con'] = "Critical";
					$batHel1['msg'] = "\"Battery Critical\" Please check distilled water level and drive to charge. Also consider checking the Starter Motor. If Battery is more than 3 years old consider replacement.";
				}
				elseif($data['bat_st'] == 2)
				{
					$batHel['rate'] = "2";
					$batHel['con'] = "Bad";
					$batHel1['msg'] = "\"Battery Bad\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
				}
				elseif($data['bat_st'] == 3)
				{
					$batHel['rate'] = "3";
					$batHel['con'] = "Okay";
					$batHel1['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge.";
				}
				elseif($data['bat_st'] == 4)
				{
					$batHel['rate'] = "4";
					$batHel['con'] = "Good";
					$batHel1['msg'] = "\"Battery Fine\" Please drive to charge.";
				}
				elseif($data['bat_st'] == 5)
				{
					$batHel['rate'] = "5";
					$batHel['con'] = "Good";
					$batHel1['msg'] = "";
				}
				//Final Battery Comment
				$batHel['msg'] = $batHel1['msg'].$batHel2['msg'];
				
				//Turbocharger System
				$turbo =array();
				$turbo['con'] = "";
				$turbo['msg'] = "";
				$turbo['rate'] = "-1";
				if($data['idlb_c'] == 1)
				{
					if($data['pekb_c'] == 1)
					{
						$turbo['rate'] = "1";
						$turbo['con'] = "Critical";
						$turbo['msg'] = "Turbo boost pressure build-up critical. Replace air filter element or check for disconnected air path hoses and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
					}
					elseif($data['pekb_c'] == 2)
					{
						$turbo['rate'] = "2";
						$turbo['con'] = "Bad";
						$turbo['msg'] = "Turbo boost pressure build-up poor. Replace air filter element or check for disconnected air path hoses and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
					}
					elseif($data['pekb_c'] == 3)
					{
						$turbo['rate'] = "3";
						$turbo['con'] = "Okay";
						$turbo['msg'] = "Turbo boost pressure build-up below average. Replace air filter element or check for disconnected air path hoses and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
					}
				}
				elseif($data['idlb_c'] == 2)
				{
					if($data['pekb_c'] == 1)
					{
						$turbo['rate'] = "1";
						$turbo['con'] = "Critical";
						$turbo['msg'] = "Turbo boost pressure build-up critical. Replace air filter element or check for disconnected air path hoses and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
					}
					elseif($data['pekb_c'] == 2)
					{
						$turbo['rate'] = "3";
						$turbo['con'] = "Okay";
						$turbo['msg'] = "Turbo boost pressure build-up below average. Replace air filter element or check for disconnected air path hoses and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
					}
					elseif($data['pekb_c'] == 3)
					{
						$turbo['rate'] = "4";
						$turbo['con'] = "Okay-Good";
						$turbo['msg'] = "Turbo boost pressure build-up average. Clean the air filter element.";
					}
				}
				elseif($data['idlb_c'] == 3)
				{
					if($data['pekb_c'] == 1)
					{
						$turbo['rate'] = "2";
						$turbo['con'] = "Bad";
						$turbo['msg'] = "Turbo boost pressure build-up poor. Replace air filter element or check for disconnected air path hoses and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
					}
					elseif($data['pekb_c'] == 2)
					{
						$turbo['rate'] = "3";
						$turbo['con'] = "Okay";
						$turbo['msg'] = "Turbo boost pressure build-up below average. Replace air filter element or check for disconnected air path hoses and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
					}
					elseif($data['pekb_c'] == 3)
					{
						$turbo['rate'] = "5";
						$turbo['con'] = "Good";
						$msg = $data['pekb_m'];
						if(($msg == "Not Applicable") || ($msg == ""))
						{
							$turbo['msg'] = "";
						}
						else
						{
							$turbo['msg'] = "Possible axial play in turbocharger. Visit Manufacturer Dealership whenever possible.";
						}
					}
				}
				elseif($data['idlb_c'] == 0 || $data['pekb_c'] == 0 || $data['idlb_c'] == "" || $data['pekb_c'] == "")
				{
					$turbo['rate'] = "-1";
					//$turbo['msg'] = "Test conditions not reached";
				}
				//$turbo['msg'] = (trim($data['idlb_m']) != trim($data['pekb_m']) ) ? trim($data['idlb_m']) ."<br/>". trim($data['pekb_m']) : trim($data['pekb_m']);
				
				//Air System
				$air =array();
				$air['rate'] = "-1";
				$air['con'] = "";
				$air['msg'] = "";
				if($data['idlbc_v'] == 1)
				{
					if($data['peakb_v'] == 1)
					{
						$air['rate'] = "1";
						$air['con'] = "Critical";
						$air['msg'] = "Air system pressure build-up critical. Replace air filter element. Check Throttle Body. Visit the Manufacturer Dealership whenever possible.";
					}
					elseif($data['peakb_v'] == 2)
					{
						$air['rate'] = "2";
						$air['con'] = "Bad";
						$air['msg'] = "Air system pressure build-up poor. Clean/replace air filter element. Check Throttle Body. Visit the Manufacturer Dealership whenever possible.";
					}
					elseif($data['peakb_v'] == 3)
					{
						$air['rate'] = "3";
						$air['con'] = "Okay";
						$air['msg'] = "Air system pressure build-up below average. Clean/replace air filter element. Check Throttle Body.";
					}
				}
				elseif($data['idlbc_v'] == 2)
				{
					if($data['peakb_v'] == 1)
					{
						$air['rate'] = "1";
						$air['con'] = "Critical";
						$air['msg'] = "Air system pressure build-up critical. Replace air filter element. Check Throttle Body. Visit the Manufacturer Dealership whenever possible.";
					}
					elseif($data['peakb_v'] == 2)
					{
						$air['rate'] = "3";
						$air['con'] = "Okay";
						$air['msg'] = "Air system pressure build-up below average. Clean/replace air filter element. Check Throttle Body.";
					}
					elseif($data['peakb_v'] == 3)
					{
						$air['rate'] = "4";
						$air['con'] = "Okay-Good";
						$air['msg'] = "Air system pressure build-up average. Clean the air filter element.";
					}
				}
				elseif($data['idlbc_v'] == 3)
				{
					if($data['peakb_v'] == 1)
					{
						$air['rate'] = "2";
						$air['con'] = "Bad";
						$air['msg'] = "Air system pressure build-up poor. Clean/replace air filter element. Check Throttle Body. Visit the Manufacturer Dealership whenever possible.";
					}
					elseif($data['peakb_v'] == 2)
					{
						$air['rate'] = "3";
						$air['con'] = "Okay";
						$air['msg'] = "Air system pressure build-up below average. Clean/replace air filter element. Check Throttle Body.";
					}
					elseif($data['peakb_v'] == 3)
					{
						$air['rate'] = "5";
						$air['con'] = "Good";
						$air['msg'] = "";
					}
				}
				elseif($data['idlbc_v'] == 0 || $data['peakb_v'] == 0 || $data['idlbc_v'] == "" || $data['peakb_v'] == "")
				{
					$air['rate'] = "-1";
					//$air['msg'] = "Test conditions not reached";
				}
				//$air['msg'] = (trim($data['idlbc_m']) != trim($data['peakb_m']) ) ? trim($data['idlbc_m']) ."<br/>". trim($data['peakb_m']) :  trim($data['peakb_m']);
				
				//Fuel System
				$fuel =array();
				$fuel['rate'] = "-1";		
				$fuel['con'] = "";
				$fuel['msg'] = "";
				if($data['idl_crp'] == 1)
				{
					if($data['peak_crp'] == 1)
					{
						$fuel['rate'] = "1";
						$fuel['con'] = "Critical";
						$fuel['msg'] = "Fuel pressure critical. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
					}
					elseif($data['peak_crp'] == 2)
					{
						$fuel['rate'] = "2";
						$fuel['con'] = "Bad";
						$fuel['msg'] = "Fuel pressure low. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
					}
					elseif($data['peak_crp'] == 3)
					{
						$fuel['rate'] = "3";
						$fuel['con'] = "Okay";
						$fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
					}
				}
				elseif($data['idl_crp'] == 2)
				{
					if($data['peak_crp'] == 1)
					{
						$fuel['rate'] = "1";
						$fuel['con'] = "Critical";
						$fuel['msg'] = "Fuel pressure critical. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
					}
					elseif($data['peak_crp'] == 2)
					{
						$fuel['rate'] = "3";
						$fuel['con'] = "Okay";
						$fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
					}
					elseif($data['peak_crp'] == 3)
					{
						$fuel['rate'] = "4";
						$fuel['con'] = "Okay-Good";
						$fuel['msg'] = "Fuel pressure build-up average. Consider changing the fuel-filter.";
					}
				}
				elseif($data['idl_crp'] == 3)
				{
					if($data['peak_crp'] == 1)
					{
						$fuel['rate'] = "2";
						$fuel['con'] = "Bad";
						$fuel['msg'] = "Fuel pressure low. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
					}
					elseif($data['peak_crp'] == 2)
					{
						$fuel['rate'] = "3";
						$fuel['con'] = "Okay";
						$fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
					}
					elseif($data['peak_crp'] == 3)
					{
						$fuel['rate'] = "5";
						$fuel['con'] = "Good";
						$fuel['msg'] = "";
					}
				}
				elseif($data['idl_crp'] == "" || $data['peak_crp'] == "" || $data['idl_crp'] == 0 || $data['peak_crp'] == 0)
				{
					$fuel['rate'] = "-1";
					//$fuel['msg'] = "Test conditions not reached";
				}
				//$fuel['msg'] = $data['crpm'];
				
				//Compression Pressure Score
				$copPres = array();
				$copPres['rate'] = "-1";
				$copPres['con'] = "";
				$copPres['msg'] = "";
				if($data['comp_pres'] == 1)
				{
					$copPres['rate'] = "1";
					$copPres['con'] = "Critical";
				}
				elseif($data['comp_pres'] == 2)
				{
					$copPres['rate'] = "2";
					$copPres['con'] = "Bad";
				}
				elseif($data['comp_pres'] == 3)
				{
					$copPres['rate'] = "3";
					$copPres['con'] = "Okay";
				}
				elseif($data['comp_pres'] == 4)
				{
					$copPres['rate'] = "4";
					$copPres['con'] = "Okay-Good";
				}
				elseif($data['comp_pres'] == 5)
				{
					$copPres['rate'] = "5";
					$copPres['con'] = "Good";
				}
				else
				{
					$copPres['msg'] = "Test Conditions not reached";
				}
				//If MAF not available the torque calculated is wrong and hence gets the wrong rating. So we ignoring this rating
				/*if($data['dist'] <= 20)
				{
					$copPres['rate'] = "-1";
				}*/
				if($data['max_drive_torque'] <= 20 && $data['dist'] == "")
				{
					$copPres['rate'] = "-1";
				}
				
				//rpmOscillationAnomalyAnalysis
				$rcOcil = array();
				$rcOcil['rate'] = "-1";
				$rcOcil['con'] = "";
				$rcOcil['msg'] = "";
				if($data['eng_block_ej'] == 1)
				{
					$rcOcil['rate'] = "1";
					$rcOcil['con'] = "Critical";
					if($dataRep['fuel'] == "Petrol")
					{
						if($copPres['rate'] <= 2)
						{
							$rcOcil1['msg'] = "Engine block bore failure detected. Check Injector bank and ignition system too. If white/excessive tail-pipe smoke observed, possible bore failure.";
						}
						elseif($copPres['rate'] == 3)
						{
							$rcOcil1['msg'] = "Engine block bore deterioration detected. Check Injector bank and ignition system too. If white/excessive tail-pipe smoke observed, possible bore deterioration.";
						}
						elseif($copPres['rate'] == 4)
						{
							$rcOcil1['msg'] = "Injector clogging or spark plug fouling detected. Accordingly, use fuel system flush/injector additives, or replace spark plugs. Decarbonize the EGR system. Check throttle body and belt tensioner.";
						}
						else
						{
							$rcOcil1['msg'] = "Clean spark plugs and check Ignition system. Clean fuel system, use fuel system flush/injector additives. Clean throttle body. Check belt tensioner.";
						}
					}
					else 
					{
						if($copPres['rate'] <= 2)
						{
							$rcOcil1['msg'] = "Engine block bore failure detected. Check injector bank too. If white/excessive tail-pipe smoke observed, possible bore failure.";
						}
						elseif($copPres['rate'] == 3)
						{
							$rcOcil1['msg'] = "Engine block bore deterioration detected. Check injector bank too. If white/excessive tail-pipe smoke observed, possible bore deterioration.";
						}
						elseif($copPres['rate'] == 4)
						{
							$rcOcil1['msg'] = "Injector clogging detected. Use fuel system flush/injector additives. Decarbonize the EGR. Check belt tensioner.";
						}
						else
						{
							$rcOcil1['msg'] = "Clean fuel injectors. Use fuel system flush/injector additives. Check belt tensioner.";
						}
					}
				}
				elseif($data['eng_block_ej'] == 2)
				{
					$rcOcil['rate'] = "2";
					$rcOcil['con'] = "Bad";
					if($dataRep['fuel'] == "Petrol")
					{
						if($copPres['rate'] <= 2)
						{
							$rcOcil1['msg'] = "Engine block bore deterioration detected. Check Injector bank and ignition system too. If white/excessive tail-pipe smoke observed, possible bore failure.";
						}
						elseif($copPres['rate'] == 3)
						{
							$rcOcil1['msg'] = "Injector or Ignition system issue detected. Please check respective wiring too.";
						}
						else
						{
							$rcOcil1['msg'] = "Clean spark plugs and use fuel system flush/injector additives. Clean throttle body. Consider checking belt tensioner";
						}
					}
					else 
					{
						if($copPres['rate'] <= 2)
						{
							$rcOcil1['msg'] = "Engine block bore deterioration detected. Check injector bank too. If white/excessive tail-pipe smoke observed, possible bore failure.";
						}
						elseif($copPres['rate'] == 3)
						{
							$rcOcil1['msg'] = "Injector clogging detected. Use fuel system flush/injector additives. Decarbonize the EGR. Check belt tensioner.";
						}
						else
						{
							$rcOcil1['msg'] = "Clean fuel injectors. Use fuel system flush/injector additives. Check belt tensioner.";
						}
					}
				}
				elseif($data['eng_block_ej'] == 3)
				{
					$rcOcil['rate'] = "3";
					$rcOcil['con'] = "Okay";
					if($dataRep['fuel'] == "Petrol")
					{
						if($copPres['rate'] == 1)
						{
							$rcOcil1['msg'] = "Engine block bore deterioration or Injector clogging or spark plug fouling detected. Consider checking/servicing them.";
						}
						elseif(($copPres['rate'] == 2) || ($copPres['rate'] == 3))
						{
							$rcOcil1['msg'] = "Injector clogging or spark plug fouling detected. Accordingly, use fuel system flush/injector additives, or replace spark plugs. Decarbonize the EGR system. Check throttle body and belt tensioner.";
						}
						else
						{
							$rcOcil1['msg'] = "Clean spark plugs and use fuel system flush/injector additives. Consider checking throttle body and belt tensioner.";
						}
					}
					else 
					{
						if(($copPres['rate'] == 1) || ($copPres['rate'] == 2))
						{
							$rcOcil1['msg'] = "Engine block bore deterioration or Injector issue detected. Consider checking/servicing them.";
						}
						elseif($copPres['rate'] == 3)
						{
							$rcOcil1['msg'] = "Injector clogging detected. Use fuel system flush/injector additives. Decarbonize the EGR. Check belt tensioner.";
						}
						else
						{
							$rcOcil1['msg'] = "Clean fuel injectors. Use fuel system flush/injector additives. Consider checking belt tensioner.";
						}
					}
				}
				elseif($data['eng_block_ej'] == 4)
				{
					$rcOcil['rate'] = "4";
					$rcOcil['con'] = "Okay-Good";
					if($dataRep['fuel'] == "Petrol")
					{
						if($copPres['rate'] == 1)
						{
							$rcOcil1['msg'] = "Clean or replace spark plugs. Use fuel system flush/injector additives. Consider decarbonising the EGR system.";
						}
						elseif(($copPres['rate'] == 2) || ($copPres['rate'] == 3))
						{
							$rcOcil1['msg'] = "Clean or replace spark plugs. Use fuel system flush/injector additives. Consider decarbonising the EGR system.";
						}
						else
						{
							$rcOcil1['msg'] = "Clean spark plugs. Consider fuel system cleaning. Use fuel system flush/injector additives.";
						}
					}
					else 
					{
						if($copPres['rate'] == 1)
						{
							$rcOcil1['msg'] = "Injector clogging detected. Use fuel system flush/injector additives. Decarbonize the EGR system.";
						}
						elseif(($copPres['rate'] == 2) || ($copPres['rate'] == 3))
						{
							$rcOcil1['msg'] = "Injector clogging detected. Use fuel system flush/injector additives. Decarbonize the EGR system.";
						}
						else
						{
							$rcOcil1['msg'] = "Consider fuel system flush/injector additives.";
						}
					}
				}
				elseif($data['eng_block_ej'] == 5)
				{
					$rcOcil['rate'] = "5";
					$rcOcil['con'] = "Good";
					$rcOcil1['msg'] = "";
				}
				
				//falseOdometerReading
				$falOdMr = array();
				$falOdMr['rate'] = "-1";
				$falOdMr['con'] = "";
				$falOdMr['msg'] = $data['falsemeter'];
				if($data['falsemeter'] == "Odometer reading Okay")
				{
					$falOdMr['rate'] = "1";
				}
				elseif($data['falsemeter'] == "Odometer tampered")
				{
					$falOdMr['rate'] = "0";
				}
				else
				{
					//do nothing
				}
				
				//getVehSpeedSence
				$vehSpeed = array();
				$vehSpeed['rate'] = "-1";
				$vehSpeed['con'] = "";
								
				if($data['vehspeed'] == "1")
				{
					$vehSpeed['rate'] = "1";
				}
				elseif($data['vehspeed'] == "0")
				{
					$vehSpeed['rate'] = "0";
				}
				else
				{
					//Do nothing;
				}
				$vehSpeed['msg'] = ($vehSpeed['rate'] == 0) ? "Vehicle Speed Sensor Failure possibility" : "Vehicle Speed Sensor Okay";
				
				//Coolant System
				$coolTemp = array();
				$coolTemp['rate'] = "-1";
				$coolTemp['msg'] = "";
				$coolTemp['con'] = "";
				if($data['coolp'] == 1)
				{
					$coolTemp['rate'] = "1";
					$coolTemp['con'] = "Critical";
					$coolTemp['msg'] = "Engine cooling poor. Check Radiator Fan functioning. Check Coolant and Engine Oil Level. Consider spray washing the radiator. Visit the Manufacturer Dealership whenever possible.";
				}
				elseif($data['coolp'] == 2)
				{
					$coolTemp['rate'] = "2";
					$coolTemp['con'] = "Bad";
					$coolTemp['msg'] = "Engine cooling inadequate. Check Radiator Fan functioning. Check Coolant and Engine Oil Level. Top-up Coolant and Engine Oil if low. Consider spray washing the radiator.";
				}
				elseif($data['coolp'] == 3)
				{
					$coolTemp['rate'] = "3";
					$coolTemp['con'] = "Okay";
					$coolTemp['msg'] = "Engine cooling below average. Check Coolant and Engine Oil Level. Top-up Coolant and Engine Oil if low. Consider spray washing the radiator.";
				}
				elseif($data['coolp'] == 4)
				{
					$coolTemp['rate'] = "4";
					$coolTemp['con'] = "Okay-Good";
					$coolTemp['msg'] = "Engine cooling average. Check Coolant and Engine Oil Level.";
				}
				elseif($data['coolp'] == 5)
				{
					$coolTemp['rate'] = "5";
					$coolTemp['con'] = "Good";
					$coolTemp['msg'] = "";
				}
				else
				{
					$coolTemp['msg'] = $data['coolm'];
				}
				
				//to check if turbo is available
				if(($turbo['rate'] <= 0) || ($turbo['rate'] >= 6) || ($turbo['rate'] == ""))
				{
					$Turbo_Avail_C = "No";
				}
				else 
				{
					$Turbo_Avail_C = "Yes";
				}
				
				//combustion pressure message
				if($copPres['rate'] == 5)
				{
					$copPres2['msg'] = "";
				}
				elseif($copPres['rate'] <= 4)
				{
					if($fueltpe == "Diesel")
					{
						if(($turbo['rate'] == 5 && $rcOcil['rate'] <= 4 && $fuel['rate'] == 5) || 
							($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] > 5) || 
							($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] == 5) || 
							($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4) ||
							($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] > 5) ||
							($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4))
						{
							if($copPres['rate'] > 0)
							{
								if($copPres['rate'] <= 3 && $rcOcil['rate'] <= 2)
								{
									//$copPres['msg'] = "Your engine's combustion pressures are not good. This is attributed to the engine's compression pressure 
									//which may be due to engine block bore deterioration or an injector issue.";
									$copPres2['msg'] = "This is attributed to the engine's compression pressure which may be due to engine block bore deterioration or an injector issue. If white/excessive tail-pipe smoke observed, possible bore failure.";
								}
								else
								{
									$n = array();
									$n = array($turbo['rate'],$fuel['rate']);
									$nVal = min($n);
					
									if($n[0] == $n[1])
									{
										if($nVal <= 3)
										{
											$copPres2['msg'] = "Please consider checking your High Pressure Fuel System and/or Air System.";
										}
										elseif($nVal == 5)
										{
											$copPres2['msg'] = "Please consider checking/cleaning your EGR System.";
										}
										else
										{
											//do nothing;
										}
									}
									else
									{
										if($fuel['rate'] <= 0 || $fuel['rate'] > 5)
										{
											if($n[0] == 5)
											{	
												$copPres2['msg'] = "We could not check your High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
											}
											else
											{
												$copPres2['msg'] = "Please consider checking your Air System.";
											}
										}
										else
										{
											if($n[0] > $n[1])
											{
												$copPres2['msg'] = "Please consider checking your High Pressure Fuel System.";
											}
											else
											{
												$copPres2['msg'] = "Please consider checking your Air System.";
											}
										}
									}
								}
							}
							else
							{
								//do nothing
							}
						}
						else
						{
							//do nothing
						}
					}
					else
					{
						if($Turbo_Avail_C == "No")
						{
							if(($air['rate'] == 5 && $rcOcil['rate'] <= 4) || ($air['rate'] <= 4 && $rcOcil['rate'] <= 5) || 
							($air['rate'] <= 4 && $rcOcil['rate'] <= 5) || ($air['rate'] == 5 && $rcOcil['rate'] <= 5) || 
							($air['rate'] <= 4 && $rcOcil['rate'] <= 5))
							{
								if($copPres['rate'] > 0)
								{
									if($copPres['rate'] <= 3 && $rcOcil['rate'] <= 1)
									{
										$copPres2['msg'] = "This is attributed to the engine's compression pressure which may be due to 
										engine block bore deterioration or there may be an injector issue. If white/excessive tail-pipe smoke observed, 
										possible bore failure.";
									}
									else
									{
										if($air['rate'] <= 4)
										{
											if($rcOcil['rate'] <= 4 && $air['rate'] == 4)
											{
												$copPres2['msg'] = "Please consider checking your Air System and fuel injectors.";
											}
											else
											{
												$copPres2['msg'] = "Please consider checking your Air System.";
											}
										}
										elseif($rcOcil['rate'] <= 4)
										{
											$copPres2['msg'] = "Please consider checking the fuel injectors and spark plugs.";
										}
									}
								}
								else
								{
									//Do nothing
								}
							}
						}
						else
						{
							if(($turbo['rate'] == 5 && $rcOcil['rate'] <= 4 && $fuel['rate'] == 5) || 
								($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] > 5) || 
								($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] == 5) || 
								($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4) ||
								($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] > 5) ||
								($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4))
							{
								if($copPres['rate'] > 0)
								{
									if($copPres['rate'] <= 3 && $rcOcil['rate'] <= 2)
									{
										$copPres2['msg'] = "This is attributed to the engine's compression pressure which may be due to engine block bore deterioration or an injector issue. If white/excessive tail-pipe smoke observed, possible bore failure.";
									}
									else
									{
										$n = array();
										$n = array($turbo['rate'],$fuel['rate']);
										$nVal = min($n);
						
										if($n[0] == $n[1])
										{
											if($nVal <= 3)
											{
												$copPres2['msg'] = "Please consider checking your High Pressure Fuel System and/or Air System.";
											}
											elseif($nVal == 5)
											{
												$copPres2['msg'] = "Please consider checking/cleaning your EGR System.";
											}
											else
											{
												//do nothing;
											}
										}
										else
										{
											if($fuel['rate'] <= 0 || $fuel['rate'] > 5)
											{
												if($n[0] == 5)
												{	
													$copPres2['msg'] = "We could not check your High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
												}
												else 
												{
													$copPres2['msg'] = "Please consider checking your Air System.";
												}
											}
											else 
											{
												if($n[0] > $n[1])
												{
													$copPres2['msg'] = "Please consider checking your High Pressure Fuel System.";
												}
												else
												{
													$copPres2['msg'] = "Please consider checking your Air System.";
												}
											}
										}
									}
								}
								else
								{
									//do nothing
								}
							}
							else
							{
								//do nothing
							}
						}
					}
				}
				
				//if($copPres['msg'] == "")
				//{
					$copPres[0] = $copPres['rate'];
					if($copPres['rate'] == 5)
					{
						$copPres1['msg'] = "";
						$copPres3['msg'] = "";
					}
					elseif($copPres['rate'] == 4)
					{
						$copPres1['msg'] = "Combustion pressure build-up average.";
						$copPres3['msg'] = "Consider general engine service.";
					}
					elseif($copPres['rate'] == 3)
					{
						$copPres1['msg'] = "Combustion pressure build-up below peak capability.";
						$copPres3['msg'] = "";
					}
					elseif($copPres['rate'] == 2)
					{
						$copPres1['msg'] = "Combustion pressure build-up low.";
						$copPres3['msg'] = "Visit the Manufacturer Dealership whenever possible.";
					}
					elseif($copPres['rate'] == 1)
					{
						$copPres1['msg'] = "Combustion pressure build-up critical.";
						$copPres3['msg'] = "Visit the Manufacturer Dealership whenever possible.";
					}
					
					$copPres['msg'] = $copPres1['msg'].$copPres2['msg'].$copPres3['msg'];
					//print_r($copPres['msg'].$copPres2['msg'].$copPres3['msg']);
				
				/*=========================================================================================================*/
					//Master Score Logic ------ Overall Health Score and it's Message
					//to check if turbo is available
					if(($turbo['rate'] <= 0) || ($turbo['rate'] >= 6) || ($turbo['rate'] == ""))
					{
						$Turbo_Avail_C = "No";
					}
					else 
					{
						$Turbo_Avail_C = "Yes";
					}
					//=====================================================================================//
					
					// $fueltpe = $dataRep['fuel'];
					if($fueltpe != "Diesel")
					{
						if($Turbo_Avail_C == "No")
						{
							$vTypeVal = $air['rate'];
						}
						else 
						{
							$vTypeVal = $turbo['rate'];
						}
					}
					else 
					{
						$vTypeVal = $turbo['rate'];
					}
					
					$MasterScore = 9;
					$CoolScore = 10;
					$BatScore = 10;
					$RPMOscScore = 10;
					$MAPScore = 10;
					$CRPScore = 10;
					$FTAOscMasterScore = 9;
					$FTAOscScore = 9;
					$CoolFTAOscScore = 9;
					$BatFTAOscScore = 9;
					//$FTAMasterScore = 10;
					//$CoolFTAMasterScore = 10;
					//$BatFTAMasterScore = 10;
					//$OscFTAMasterScore = 10;
					$MAPMasterScore = 9;
					$CoolMAPMasterScore = 9;
					$BatMAPMasterScore = 9;
					$OscMAPMasterScore = 9;
					$CRPMasterScore = 9;
					$CoolCRPMasterScore = 9;
					$BatCRPMasterScore = 9;
					$OscCRPMasterScore = 9;
					
					
					//3. Coolant Score
					if($coolTemp['rate'] <= 4 && $coolTemp['rate'] > 0)
					{
						if($coolTemp['rate'] <= 2)
						{
							$CoolScore = 6;
						}
						elseif($coolTemp['rate'] == 3)
						{
							$CoolScore = 7;
						}
						elseif($coolTemp['rate'] == 4)
						{
							$CoolScore = 8;
						}
					}
					
					//4. Battery Score
					if($batHel['rate'] <= 4 && $batHel['rate'] > 0)
					{
						if($batHel['rate'] == 1)
						{
							$BatScore = 5;
						}
						elseif($batHel['rate'] == 2)
						{
							$BatScore = 6;
						}
						elseif($batHel['rate'] == 3)
						{
							$BatScore = 8;
						}
						elseif($batHel['rate'] == 4)
						{
							$BatScore = 10;
						}
					}
					
					//5. RPM Oscillation Score
					if($rcOcil['rate'] <= 4 && $rcOcil['rate'] > 0)
					{	
						if(($fueltpe == "Petrol" && $rcOcil['rate'] <= 2) || ($fueltpe == "Diesel" && $rcOcil['rate'] <= 3))
						{
							$RPMOscScore = 5;
						}
						elseif($fueltpe == "Petrol" && $rcOcil['rate'] == 3)
						{
							$RPMOscScore = 7;
						}
						elseif($rcOcil['rate'] == 4)
						{
							$RPMOscScore = 8;
						}
					}
					
					//6. Air System Health Score (MAP)
					if($vTypeVal <= 4 && $vTypeVal > 0)
					{
						if($vTypeVal <= 2)
						{
							$MAPScore = 6;
						}
						elseif($vTypeVal == 3)
						{
							$MAPScore = 7;
						}
						elseif($vTypeVal == 4)
						{
							$MAPScore = 9;
						}
					}
					
					//6. Fuel System Health Score (CRP)
					if($fuel['rate'] <= 4 && $fuel['rate'] > 0)
					{
						if($fuel['rate'] <= 2)
						{
							$CRPScore = 6;
						}
						elseif($fuel['rate'] == 3)
						{
							$CRPScore = 7;
						}
						elseif($fuel['rate'] == 4)
						{
							$CRPScore = 8;
						}
					}
					
					//1. Compression Pressure and Low RPM Oscillation Score
					if((($fueltpe == "Petrol" && $rcOcil['rate'] == 1) || ($fueltpe == "Diesel" && $rcOcil['rate'] <= 2)) && $copPres['rate'] <= 3)
					{
						if($copPres['rate'] == 1)
						{
							$FTAOscMasterScore = 1;
						}
						elseif($copPres['rate'] == 2)
						{
							$FTAOscMasterScore = 2;
						}
						else
						{
							$FTAOscMasterScore = 3;
						}
						$MasterScore = $FTAOscMasterScore;
					}
					
					if($FTAOscMasterScore > 3)
					{
						//2. Compression Pressure Limit
						if($copPres['rate'] <= 4)
						{
							//2A. Compression Pressure and RPM Oscillation Score
							if($fueltpe == "Petrol")
							{
								if($rcOcil['rate'] == 2 || $rcOcil['rate'] == 3)
								{
									if($copPres['rate'] == 1)
									{
										$FTAOscScore = 2;
									}
									elseif($copPres['rate'] == 2)
									{
										$FTAOscScore = 3;
									}
									elseif($copPres['rate'] == 3)
									{
										$FTAOscScore = 4;
									}
									elseif($copPres['rate'] == 4)
									{
										$FTAOscScore = 5;
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
								elseif($rcOcil['rate'] >= 4)
								{
									if($copPres['rate'] == 1)
									{
										$FTAOscScore = 3;
									}
									elseif($copPres['rate'] == 2)
									{
										$FTAOscScore = 4;
									}
									elseif($copPres['rate'] == 3)
									{
										$FTAOscScore = 5;
									}
									elseif($copPres['rate'] == 4)
									{
										$FTAOscScore = 8;
					
										//Coolant Temp Score combination
										if($CoolScore == 6)
										{
											$CoolFTAOscScore = 6;
										}
										elseif($CoolScore == 7)
										{
											$CoolFTAOscScore = 7;
										}
										elseif($CoolScore == 8)
										{
											$CoolFTAOscScore = 8;
										}
										
										//Battery Score combination
										if($BatScore == 5)
										{
											$BatFTAOscScore = 5;
										}
										elseif($BatScore == 6)
										{
											$BatFTAOscScore = 6;
										}
										elseif($BatScore == 8)
										{
											$BatFTAOscScore = 7;
										}
										elseif($BatScore == 10)
										{
											$BatFTAOscScore = 8;
										}
										
										$FTAOscScore = min($FTAOscScore,$CoolFTAOscScore,$BatFTAOscScore);
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
								elseif($rcOcil['rate'] == 1)
								{
									if($copPres['rate'] == 4)
									{
										$FTAOscScore = 4;
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
								elseif($rcOcil['rate'] < 0)
								{
									if($copPres['rate'] == 4)
									{
										$FTAOscScore = 8;
									}
									elseif($copPres['rate'] == 3)
									{
										$FTAOscScore = 7;
									}
									elseif($copPres['rate'] == 2)
									{
										$FTAOscScore = 5;
									}
									elseif($copPres['rate'] == 1)
									{
										$FTAOscScore = 4;
									}
								}	
							}
							else
							{
								if($rcOcil['rate'] == 3)
								{
									if($copPres['rate'] == 1)
									{
										$FTAOscScore = 2;
									}
									elseif($copPres['rate'] == 2)
									{
										$FTAOscScore = 3;
									}
									elseif($copPres['rate'] == 3)
									{
										$FTAOscScore = 4;
									}
									elseif($copPres['rate'] == 4)
									{
										$FTAOscScore = 5;
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
								elseif($rcOcil['rate'] == 4)
								{
									if($copPres['rate'] == 1)
									{
										$FTAOscScore = 3;
									}
									elseif($copPres['rate'] == 2)
									{
										$FTAOscScore = 4;
									}
									elseif($copPres['rate'] == 3)
									{
										$FTAOscScore = 5;
									}
									elseif($copPres['rate'] == 4)
									{
										$FTAOscScore = 7;
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
								elseif($rcOcil['rate'] == 5)
								{
									if($copPres['rate'] == 1)
									{
										$FTAOscScore = 3;
									}
									elseif($copPres['rate'] == 2)
									{
										$FTAOscScore = 4;
									}
									elseif($copPres['rate'] == 3)
									{
										$FTAOscScore = 5;
									}
									elseif($copPres['rate'] == 4)
									{
										$FTAOscScore = 8;
										
										//Coolant Temp Score combination
										if($CoolScore == 6)
										{
											$CoolFTAOscScore = 6;
										}
										elseif($CoolScore == 7)
										{
											$CoolFTAOscScore = 7;
										}
										elseif($CoolScore == 8)
										{
											$CoolFTAOscScore = 8;
										}
										
										//Battery Score combination
										if($BatScore == 5)
										{
											$BatFTAOscScore = 5;
										}
										elseif($BatScore == 6)
										{
											$BatFTAOscScore = 6;
										}
										elseif($BatScore == 8)
										{
											$BatFTAOscScore = 7;
										}
										elseif($BatScore == 10)
										{
											$BatFTAOscScore = 8;
										}
										
										$FTAOscScore = min($FTAOscScore,$CoolFTAOscScore,$BatFTAOscScore);
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
								elseif(($rcOcil['rate'] <= 2) && ($rcOcil['rate'] > 0))
								{
									if($copPres['rate'] == 4)
									{
										$FTAOscScore = 4;
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
								elseif($rcOcil['rate'] < 0)
								{
									if($copPres['rate'] == 4)
									{
										$FTAOscScore = 8;
									}
									elseif($copPres['rate'] == 3)
									{
										$FTAOscScore = 7;
									}
									elseif($copPres['rate'] == 2)
									{
										$FTAOscScore = 5;
									}
									elseif($copPres['rate'] == 1)
									{
										$FTAOscScore = 4;
									}
								}
							}
							
							//2B. Compression Pressure and MAP Score
							if($vTypeVal <= 5 && $vTypeVal > 0)
							{
								if($vTypeVal <= 3)
								{
									if($copPres['rate'] <= 2)
									{
										$MAPMasterScore = 3;
									}
									elseif($copPres['rate'] == 3)
									{
										$MAPMasterScore = 6;
									}
									elseif($copPres['rate'] == 4)
									{
										$MAPMasterScore = 7;
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
								else
								{
									if($copPres['rate'] == 3)
									{
										$MAPMasterScore = 7;
									}
									elseif($copPres['rate'] == 4)
									{
										$MAPMasterScore = 8;
										
										//Coolant Temp Score combination
										if($CoolScore == 6)
										{
											$CoolMAPMasterScore = 6;
										}
										elseif($CoolScore == 7)
										{
											$CoolMAPMasterScore = 7;
										}
										elseif($CoolScore == 8)
										{
											$CoolMAPMasterScore = 8;
										}
										
										//Battery Score combination
										if($BatScore == 5)
										{
											$BatMAPMasterScore = 5;
										}
										elseif($BatScore == 6)
										{
											$BatMAPMasterScore = 6;
										}
										elseif($BatScore == 8)
										{
											$BatMAPMasterScore = 7;
										}
										elseif($BatScore == 10)
										{
											$BatMAPMasterScore = 8;
										}
										
										//RPM Oscillation Score combination
										if($RPMOscScore == 5)
										{
											$OscMAPMasterScore = 5;
										}
										elseif($RPMOscScore == 7)
										{
											$OscMAPMasterScore = 7;
										}
										elseif($RPMOscScore == 8)
										{
											$OscMAPMasterScore = 8;
										}
										
										$MAPMasterScore = min($MAPMasterScore,$CoolMAPMasterScore,$BatMAPMasterScore,$OscMAPMasterScore);
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
							}
							//2C. Compression Pressure and CRP Score
							if($fuel['rate'] <= 5 && $fuel['rate'] > 0)
							{
								if($fuel['rate'] <= 3)
								{
									if($copPres['rate'] <= 2)
									{
										$CRPMasterScore = 2;
									}
									elseif($copPres['rate'] == 3)
									{
										$CRPMasterScore = 5;
									}
									elseif($copPres['rate'] == 4)
									{
										$CRPMasterScore = 7;
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
								else
								{
									if($copPres['rate'] == 3)
									{
										$CRPMasterScore = 7;
									}
									elseif($copPres['rate'] == 4)
									{
										$CRPMasterScore = 8;
										
										//Coolant Temp Score combination
										if($CoolScore == 6)
										{
											$CoolCRPMasterScore = 6;
										}
										elseif($CoolScore == 7)
										{
											$CoolCRPMasterScore = 7;
										}
										elseif($CoolScore == 8)
										{
											$CoolCRPMasterScore = 8;
										}
										
										//Battery Score combination
										if($BatScore == 5)
										{
											$BatCRPMasterScore = 5;
										}
										elseif($BatScore == 6)
										{
											$BatCRPMasterScore = 6;
										}
										elseif($BatScore == 8)
										{
											$BatCRPMasterScore = 7;
										}
										elseif($BatScore == 10)
										{
											$BatCRPMasterScore = 8;
										}
										
										//RPM Oscillation Score combination
										if($RPMOscScore == 5)
										{
											$OscCRPMasterScore = 5;
										}
										elseif($RPMOscScore == 7)
										{
											$OscCRPMasterScore = 7;
										}
										elseif($RPMOscScore == 8)
										{
											$OscCRPMasterScore = 8;
										}
										
										$CRPMasterScore = min($CRPMasterScore,$CoolCRPMasterScore,$BatCRPMasterScore,$OscCRPMasterScore);
									}
									else
									{
										//do nothing as combustion score is 5
									}
								}
							}
						}
						else
						{
							//do nothing as combustion score is 5
						}
					}
					
					//All systems good
					if(($rcOcil['rate'] == 5 && $rcOcil['rate'] < 0) && ($fuel['rate'] == 5 || $fuel['rate'] < 0) && ($vTypeVal >= 5 || $vTypeVal < 0) && $coolTemp['rate'] == 5 && ($batHel['rate'] >= 4 || $batHel['rate'] < 0) && ($copPres['rate'] >= 5 || $copPres['rate'] < 0))
					{
						$MasterScore = 10;
					}	
					elseif($FTAOscMasterScore > 3)
					{
						if($copPres['rate'] <= 4)
						{
							$MasterScore = min($FTAOscScore,$MAPMasterScore,$CRPMasterScore,$BatScore,$CoolScore,$RPMOscScore,$MAPScore,$CRPScore);
						}
						elseif($copPres['rate'] >= 5)
						{
							$MasterScore = min($BatScore,$CoolScore,$RPMOscScore,$MAPScore,$CRPScore);
						}
					}
					else
					{
						$MasterScore = $FTAOscMasterScore;
					}
					
					/*==================================================================================================================*/
					$overallhealth['msg'] = "";
					if($MasterScore <= 3)
					{
						$overallhealth['msg'] = "Overhaul/Component Failure.";
					}
					elseif($MasterScore <= 5)
					{
						$overallhealth['msg'] = "System/Systems performance below average. Major Repair/Component failure forseen.";
					}
					elseif($MasterScore <= 7)
					{
						$overallhealth['msg'] = "Specific Repair/Specific Service required.";
					}
					elseif($MasterScore <= 9)
					{
						if($fueltpe == "Petrol")
						{
							if(($batHel['rate'] < 4) && ($rcOcil['rate'] == 5) && 
								($fuel['rate'] >= 5 || $fuel['rate'] < 0) && 
								($vTypeVal >= 5 || $vTypeVal < 0) && 
								($coolTemp['rate'] >= 5 || $coolTemp['rate'] < 0) && 
								($copPres['rate'] >= 5 || $copPres['rate'] < 0))
							{
								$overallhealth['msg'] = "Good Car. General Service of Battery (Check and top-up distilled water level, clean terminals) required";
							}
							else
							{
								$overallhealth['msg'] = "Good car. General Service (All filters clean/replace, all fluids top-up/replace, spark plug clean/replace, wheel alignment etc.) required.";
							}
						}
						else
						{
							if(($batHel['rate'] < 4) && ($rcOcil['rate'] == 5) && 
								($fuel['rate'] >= 5 || $fuel['rate'] < 0) && 
								($vTypeVal >= 5 || $vTypeVal < 0) && 
								($coolTemp['rate'] >= 5 || $coolTemp['rate'] < 0) && 
								($copPres['rate'] >= 5 || $copPres['rate'] < 0))
							{
								$overallhealth['msg'] = "Good Car. General Service of Battery (Check and top-up distilled water level, clean terminals) required";
							}
							else
							{
								$overallhealth['msg'] = "Good car. General Service (All filters clean/replace, all fluids top-up/replace, wheel alignment etc.) required.";
							}
						}
					}
					else
					{
						$overallhealth['msg'] = "Great car."; // All Engine's Systems Healthy.";
					}
				//===================================== End overall Msg ============================
				
				//============================ DTC Errors =============================================//
				$sqlApo = "select * from device_data where device_id = '$deviceID' and Fsprint = '$Fsprint' order by id desc limit 0,1";
				$dataRep = $this -> select($sqlApo);
				$dtc = $dataRep[0]['F11'];
				
				$dtc = ((trim($dtc) != "") && (trim($dtc) != "0")) ? explode(':',$dtc) : "";
				if($dtc == "")
				{
					$dtccount = 0;
				}
				else
				{
					$dtccount = count($dtc);
				}
				
				$dtcTxt = "";
				$DTC = array();
				$dtcTxt = array();
				$dtcTxtDesc = array();
				
				if($dtccount >= 1 && is_array($dtc))
				{
					for($i=0; $i<$dtccount; $i++)
					{
						//calc's for P U B C	
						$errCodeType = $dtc[$i] & 0xC000;
						$errCodeType = $errCodeType >> 14;
						
						//echo "$errCodeType";
						if($errCodeType == 1) 
						{
						    $errcodeHexStr = "C";
						} 
						elseif($errCodeType == 2) 
						{
						    $errcodeHexStr = "B";
						} 
						elseif($errCodeType == 3) 
						{
						    $errcodeHexStr = "U";
						} 
						else 
						{
						    $errcodeHexStr = "P";
						}
						// end P U B C
						
						//Bit-wise AND the decimal value from ECU to get 2nd byte.
						$errCodeTmp = $dtc[$i] & 0x3000;
						$errCodeTmp = $errCodeTmp >> 12;			//Right shift by 12 bits to get 2nd byte.
						$hx = dechex($errCodeTmp);	
						$errcodeHexStr = $errcodeHexStr."$hx";
						
						//Bit-wise AND the decimal value from ECU to get the 3rd byte
						$errCodeTmp = $dtc[$i] & 0x0F00;
						$errCodeTmp = $errCodeTmp >> 8;				//Right shift by 8 bits to get the last DTC character.
						$hx = dechex($errCodeTmp);	
						$errcodeHexStr = $errcodeHexStr."$hx";
						
						$errCodeTmp = $dtc[$i] & 0x00FF;
						if($errCodeTmp <= 0x0A) 
						{
						    $errcodeHexStr = $errcodeHexStr."0";
						    $hx = dechex($errCodeTmp);	
							$errcodeHexStr = $errcodeHexStr."$hx";
						}
						else
						{
						    $hx = dechex($errCodeTmp);	
							$errcodeHexStr = $errcodeHexStr."$hx";
						}
						
						$errcodeHexStr = strtoupper($errcodeHexStr);
						$dtcTxt[$i] = $errcodeHexStr;
						
						$dtcTxtDesc[$i] = getDTCVal($dtcTxt[$i]);
						//echo $dtcTxtDesc[$i] = getDTCVal("U0552");
						
						if($dtcTxtDesc[$i] == "")
						{
							$dtcTxtDesc[$i] = "Manufacturer specific code";
						}
						else
						{
							//do nothing;
						}
						
						//$DTC[$i] .= $dtcTxt[$i]." : ".$dtcTxtDesc[$i]. "; ";
						//$DTC .= $dtcTxt[$i]." : ".$dtcTxtDesc[$i]. "; ";
						$DTC[$i]['id'] = $dtcTxt[$i];
						$DTC[$i]['desc'] = $dtcTxtDesc[$i];
					}
					// for($i=0;$i<$dtccount;$i++)
					// {
						// //TODO: Values will be overwritten to be fixed later.
						// $DTC[$i]['id'] = $dtcTxt[$i];
						// $DTC[$i]['desc'] = $dtcTxtDesc[$i];
					// }
				}
				//============================================================================//
				
				$dataMonitor = array();
				//Health Rating
				$arrayData['batterySystemHealth'] = array('ratings'=>$batHel['rate'],'condition'=>$batHel['con'],'message'=>$batHel['msg']);
				
				$arrayData['turbochargerSystemHealth'] = array('ratings'=>$turbo['rate'],'condition'=>$turbo['con'],'message'=>$turbo['msg']);
		
				$arrayData['airSystemHealth'] = array('ratings'=>$air['rate'],'condition'=>$air['con'],'message'=>$air['msg']);
				
				$arrayData['fuelSystemHealth'] = array('ratings'=>$fuel['rate'],'condition'=>$fuel['con'],'message'=>$fuel['msg']);
				
				$arrayData['combustionPressure'] = array('ratings'=>$copPres['rate'],'condition'=>$copPres['con'],'message'=>$copPres['msg']);
				
				$arrayData['engineBlockInjectors'] = array('ratings'=>$rcOcil['rate'],'condition'=>$rcOcil['con'],'message'=>$rcOcil['msg']);
				
				$arrayData['coolantSystemHealth'] = array('ratings'=>$coolTemp['rate'],'condition'=>$coolTemp['con'],'message'=>$coolTemp['msg']);
				
				$arrayData['falseOdometerReadingDetection'] = array('ratings'=>$falOdMr['rate'],'condition'=>$falOdMr['con'],'message'=>$falOdMr['msg']);
				
				$arrayData['vehicleSpeedSensorMalfunction'] = array('ratings'=>$vehSpeed['rate'],'condition'=>$vehSpeed['con'],'message'=>$vehSpeed['msg']);
				
				$arrayData['overallHealth'] = array('ratings'=>"$MasterScore",'message'=>$overallhealth['msg']);
		
				$arrayData['dtc_data'] = array('dtc_num'=>"$dtccount",'dtc_info'=>$DTC);
				
				/******************************************************************************************************
				 ************************************* End of Monitoring results **************************************
				 *****************************************************************************************************/
			}
			else
			{
				$arrayData['success'] = FALSE;
			}
		}
		else
		{
			$arrayData['success'] = FALSE;
			
		}
		return $arrayData;
	}
		
	function CreateFileCSV($fRow,$devId)
	{
		//clearstatcache();
		$floder = "datacsv/".$devId;
		if(!file_exists($floder))
		{
			mkdir($floder,0777,true);
		}
		$fileN = "$floder/". date('dmY',strtotime($fRow['time_v'])). ".csv";
		if(!file_exists($fileN))
		{
			$txt = $this -> felidList()."\n";
			file_put_contents($fileN, $txt);
			chmod($fileN, fileperms($fileN) | 128 + 16 + 2);
			file_put_contents($fileN, $txt);
		}
		//$fRow = $row;
		
		$fData = "";
		$flist = $this -> getfields('device_data');
		for($i=1;$i<count($flist);$i++)
		{
			if($flist[$i] == "alldata")
			{
				continue;
			}
			$fData .= ($i>1) ? "," : "";
			$fData .= $fRow[$flist[$i]];
		}
			
		//file_put_contents($fileN, $fData, FILE_APPEND | LOCK_EX);
		$fData .= "\n";
		$current = file_get_contents($fileN);
		if(strpos($current, 'device_id') === FALSE)
		{
			$txt = $this -> felidList()."\n";
			$current = $txt . $current;
		}
		// Append a new person to the file
		$current .= $fData;
		// Write the contents back to the file
		chmod($fileN, fileperms($fileN) | 128 + 16 + 2);
		file_put_contents($fileN, $current);
		chmod($fileN, fileperms($fileN) | 128 + 16 + 2);
		unset($fRow);
	}
	
	function felidList()
	{
		$flist = $this -> getfields('device_data');
		//print_r($flist);
		$fT = "";
		for($i=1;$i<count($flist);$i++)
		{
			$flist[$i];
			$col = substr($flist[$i], 1);
			$colPre = substr($flist[$i], 0,1);
			$f = explode('_',$col);
			if($flist[$i] == "alldata")
			{
				continue;
			}
			$fT .= ($i>1) ? "," : "";
			if($f[0] >= 1 && $f[1] >= 1)
			{
				$sql = "select device1 from valuepart where id = '".$f[0]."'";
				$tName = mysql_fetch_array(mysql_query($sql));
				$fT .= $tName[0];
				$sql = "select va_device1 from valuesubpart where va_id = '".$f[1]."'";
				$tName = mysql_fetch_array(mysql_query($sql));
				$fT .= "-".$tName[0];
			}
			elseif($f[0] >= 1)
			{
				if($colPre == "C")
				{
					$sql = "select device1 from corepart where id = '".$f[0]."'";
				}
				else
				{
					$sql = "select device1 from valuepart where id = '".$f[0]."'";
				}
				$tName = mysql_fetch_array(mysql_query($sql));
				$fT .= $tName[0];
			}
			else
			{
				$fT .= $flist[$i];
			}			
		}
		return $fT;
	}

	//statistics 

	function driveSprint($sp)
	{
		if($sp < 100)
		{
			$dVal = $this -> dev;
			$sql = "select distinct Fsprint from device_data where device_id='$dVal' and Fdist <> '0' order by Fsprint desc limit 0,$sp";
			$data = $this -> select($sql);
			return $data;
		}
		else
		{
			$data = array();
			$data[0] = array();
			$data[0]['Fsprint'] = $sp;
			return $data;
			//return array('Fsprint' => $sp);
		}
	}

	function durationofDriveCAI($sp)
	{
		$data = $this -> driveSprint($sp);
		$reAry = array();
		$dVal = $this -> dev;
		
		$datAct = new datAct('device1');
		$f9 = $datAct->valuefield('obdCalcEngineState');
		$engStat = $f9[0];
		
		$durFld = $datAct->valuefield('obdDriveDuration');
		$durFld = $durFld[0];
		
		echo (trim($durFld)== "") ? "obdDriveDuration not found /n" : "";

		$i = 0;
		foreach($data as $d)
		{
			$sprint = $d['Fsprint'];	
			$sql = "select time_v,Fdist,$durFld from device_data where device_id='$dVal' and Fsprint='$sprint' and (Fdist*1)>=0 order by time_s desc limit 0,1";
			$last = $this -> select($sql);
			
			/*$lastX = $last['Fdist'] ." aaa ". $last[$durFld];
			$sqlTest = "INSERT INTO `engineca_chart`.`testtab` (`mytest`) VALUES (  'came to durcal $lastX | $dVal | $sprint | $durFld' );";
			mysql_query($sqlTest);*/	
			
			$sql = "select time_v from device_data where device_id='$dVal' and Fsprint='$sprint' and (Fdist*1)>=0 order by time_s asc limit 0,1";
			$first = $this -> select($sql);
			//echo $last[0]['time_v'] . " | " . $first[0]['time_v'];
			//$diff = strtotime($last[0]['time_v']) - strtotime($first[0]['time_v']);
			$diff = (($last[0][$durFld]*1) >0 ) ? ($last[0][$durFld]*1) : ( strtotime($last[0]['time_v']) - strtotime($first[0]['time_v']) );
			
			$reAry[$i] = array($diff,$last[0]['Fdist'],$last[0]['time_v'],$first[0]['time_v']);
			$i++;
		}
		unset($data);
		unset($last);
		unset($first);
		//print_r($reAry);
		return $reAry;
	}

	function durationofDrive($sp)
	{
		$data = $this -> driveSprint($sp);
		$reAry = array();
		$dVal = $this -> dev;
		
		$datAct = new datAct('device1');
		$f9 = $datAct->valuefield('obdCalcEngineState');
		$engStat = $f9[0];
		$i = 0;
		foreach($data as $d)
		{
			$sprint = $d['Fsprint'];	
			$sql = "select time_v,Fdist from device_data where device_id='$dVal' and Fsprint='$sprint' and (Fdist*1)>=0 order by time_s desc limit 0,1";
			$last = $this -> select($sql);
			
			$sql = "select time_v from device_data where device_id='$dVal' and Fsprint='$sprint' and (Fdist*1)>=0 order by time_s asc limit 0,1";
			$first = $this -> select($sql);
			//echo $last[0]['time_v'] . " | " . $first[0]['time_v'];
			$diff = strtotime($last[0]['time_v']) - strtotime($first[0]['time_v']);
			$reAry[$i] = array($diff,$last[0]['Fdist'],$last[0]['time_v'],$first[0]['time_v']);
			$i++;
		}
		unset($data);
		unset($last);
		unset($first);
		// print_r($reAry);
		return $reAry; 
	}

	function idleTime($sp)
	{
		$data = $this -> driveSprint($sp);
		$dVal = $this -> dev;
		$datAct = new datAct('device1');
		/*$rpm = $datAct->valuefield('obdRpm');
		$spd = $datAct->valuefield('obdSpeed');
		$eLaoad = $datAct->valuefield('obdEngineLoad');
		$rpm = $rpm[0];
		$spd = $spd[0];
		$eLaoad = $eLaoad[0];*/
		
		$f9 = $datAct->valuefield('obdCalcEngineState');
		$engStat = $f9[0];
		
		//(60*5)
		$idT = 0;
		//$distOver = 0;
		foreach($data as $d)
		{
			$sprint = $d['Fsprint'];	
			$sql = "select time_v,time_s,Fdist from device_data where device_id='$dVal' and Fsprint='$sprint' and $engStat = '2' order by time_s asc";
			$last = $this -> select($sql);
			$lastTm = 0;
			//$lastDist = 0;
			
			foreach($last as $dat)
			{
				//print_r($dat);
				$now = strtotime($dat['time_v']);
				$diff = ($lastTm >= 1) ? ($now - $lastTm) : 0;
				
				//$nowDist = $dat['Fdist'];
				//$diffDist = ($nowDist - $lastDist);
				
				$cTS = $dat['time_s'];
				$sqlLast = "select time_v,time_s, $engStat from device_data where device_id='$dVal' and Fsprint='$sprint' and time_s < '$cTS' order by time_s desc limit 0,1";
				$datLast = $this -> select($sqlLast);
				$lastLess = 1;
				if(count($datLast) >= 1)
				{
					$datLast = $datLast[0];
					//if($datLast[$rpm] < 1200 || $datLast[$eLaoad] != 0)
					if(trim($datLast[$engStat]) != "2")
					{
						$lastLess = 0;
					}
				}
				if($lastLess == 1)
				{
					$idT += $diff;
					//$distOver += $diffDist;//$dat['Fdist'];
				}
				$lastTm = strtotime($dat['time_v']);
				//$lastDist = $dat['Fdist'];
			}
		}
		unset($last);
		unset($data);
		unset($rpm);
		unset($spd);
		unset($datLast);
		unset($eLaoad);
		return $idT;
	}

	function overRun($sp)
	{
		$data = $this -> driveSprint($sp);
		$dVal = $this -> dev;
		$datAct = new datAct('device1');
		/*$rpm = $datAct->valuefield('obdRpm');
		$spd = $datAct->valuefield('obdSpeed');
		$eLaoad = $datAct->valuefield('obdEngineLoad');
		$rpm = $rpm[0];
		$spd = $spd[0];
		$eLaoad = $eLaoad[0];*/
		
		$f9 = $datAct->valuefield('obdCalcEngineState');
		$engStat = $f9[0];
		
		//(60*5)
		$ovT = 0;
		$distOver = 0;
		foreach($data as $d)
		{
			$sprint = $d['Fsprint'];	
			$sql = "select time_v,time_s,Fdist from device_data where device_id='$dVal' and Fsprint='$sprint' and $engStat = '4' order by time_s asc";
			$last = $this -> select($sql);
			$lastTm = 0;
			$lastDist = 0;
			
			foreach($last as $dat)
			{
				//print_r($dat);
				$now = strtotime($dat['time_v']);
				$diff = ($now - $lastTm);
				
				$nowDist = $dat['Fdist'];
				$diffDist = ($nowDist - $lastDist);
				
				$cTS = $dat['time_s'];
				$sqlLast = "select time_v,time_s, $engStat from device_data where device_id='$dVal' and Fsprint='$sprint' and time_s < '$cTS' order by time_s desc limit 0,1";
				$datLast = $this -> select($sqlLast);
				$lastLess = 1;
				if(count($datLast) >= 1)
				{
					$datLast = $datLast[0];
					//if($datLast[$rpm] < 1200 || $datLast[$eLaoad] != 0)
					if(trim($datLast[$engStat]) != "4")
					{
						$lastLess = 0;
					}
				}
				if($lastLess == 1)
				{
					$ovT += $diff;
					$distOver += $diffDist;//$dat['Fdist'];
				}
				$lastTm = strtotime($dat['time_v']);
				$lastDist = $dat['Fdist'];			
			}
		}
		unset($last);
		unset($data);
		unset($rpm);
		unset($spd);
		unset($datLast);
		unset($eLaoad);
		return array($ovT,$distOver);
	}

	function avgSpeed($sp,$fld,$type="")
	{
		$data = $this -> driveSprint($sp);
		$dVal = $this -> dev;
		$datAct = new datAct('device1');
		if($type != 1)
		{
			$spd = $datAct->valuefield($fld);
			$spd = $spd[0];
		}
		else
		{
			$spd = $fld;
		}
		//(60*5)
		$speedT = 0;
		foreach($data as $d)
		{
			$sprint = $d['Fsprint'];	
			$sql = "select time_v,time_s from device_data where device_id='$dVal' and Fsprint='$sprint' and  $spd <> 0 order by time_s asc";
			$tot = mysql_num_rows(mysql_query($sql));
			$sql = "select sum($spd) from device_data where device_id='$dVal' and Fsprint='$sprint' and  $spd <> 0 order by time_s asc";
			$speed = $this -> select($sql);
			$speed = $speed[0][0];
			//echo "$speed / $tot";
			$avg = ($tot > 0) ? ($speed / $tot) : 0;
			$speedT += $avg;		
		}
		$totC = count($data);
		$totC = ($totC > 0) ? $totC : 1;
		//echo "<br/>$speedT/$totC;";
		$speedT = $speedT/$totC;
		unset($last);
		unset($data);
		unset($speed);
		unset($spd);
		return $speedT;
	}

	function totSpeed($sp,$fld,$type="")
	{
		$data = $this -> driveSprint($sp);
		$dVal = $this -> dev;
		$datAct = new datAct('device1');
		if($type != 1)
		{
			$spd = $datAct->valuefield($fld);
			$spd = $spd[0];
		}
		else
		{
			$spd = $fld;
		}
		//(60*5)
		$speedT = 0;
		foreach($data as $d)
		{
			$sprint = $d['Fsprint'];
			
			$sql = "select sum($spd) from device_data where device_id='$dVal' and Fsprint='$sprint' and  $spd <> 0 order by time_s asc";
			$speed = $this -> select($sql);
			$speed = $speed[0][0];
			//echo "$speed / $tot";
			//$avg = $speed / $tot;
			$speedT += $speed;		
		}	
		$speedT = $speedT;
		//unset($last);
		unset($data);
		unset($speed);
		return $speedT;
	}

	function totSpeedLast($sp,$fld,$type="",$aD="desc")
	{
		$data = $this -> driveSprint($sp);
		$dVal = $this -> dev;
		$datAct = new datAct('device1');
		if($type != 1)
		{
			$spd = $datAct->valuefield($fld);
			$spd = $spd[0];
		}
		else
		{
			$spd = $fld;
		}
		//(60*5)
		$speedT = 0;
		foreach($data as $d)
		{
			$sprint = $d['Fsprint'];
			
			$sql = "select $spd from device_data where device_id='$dVal' and Fsprint='$sprint' and  ($spd*1) <> 0 order by time_s $aD limit 0,1";
			$speed = $this -> select($sql);
			$speed = $speed[0][0];
			//echo "$speed / $tot";
			//$avg = $speed / $tot;
			$speedT += $speed;		
		}	
		$speedT = $speedT;
		//unset($last);
		unset($data);
		unset($speed);
		return $speedT; 
	}

	function totSpeedLastAvg($sp,$fld,$type="")
	{
		$data = $this -> driveSprint($sp);
		$dVal = $this -> dev;
		$datAct = new datAct('device1');
		if($type != 1)
		{
			$spd = $datAct->valuefield($fld);
			$spd = $spd[0];
		}
		else
		{
			$spd = $fld;
		}
		//(60*5)
		$speedT = 0;
		foreach($data as $d)
		{
			$sprint = $d['Fsprint'];
			
			$sql = "select $spd from device_data where device_id='$dVal' and Fsprint='$sprint' and  ($spd*1) <> 0 order by time_s desc limit 0,1";
			$speed = $this -> select($sql);
			$speed = $speed[0][0];
			//echo "$speed / $tot";
			//$avg = $speed / $tot;
			$speedT += $speed;		
		}
		$totC = count($data);
		$totC = ($totC > 0) ? $totC : 1;	
		$speedT = $speedT / $totC;
		//unset($last);
		unset($data);
		unset($speed);
		return $speedT; 
	}

	function maxSpeed($sp,$fld,$type="")
	{
		$data = $this -> driveSprint($sp);
		$dVal = $this -> dev;
		$datAct = new datAct('device1');
		if($type != 1)
		{
			$spd = $datAct->valuefield($fld);
			$spd = $spd[0];
		}
		else
		{
			$spd = $fld;
		}
		//(60*5)
		$speedT = 0;
		$spVal = array();
		foreach($data as $d)
		{
			$sprint = $d['Fsprint'];
			$sql = "select max($spd*1) from device_data where device_id='$dVal' and Fsprint='$sprint' and  ($spd*1) <> 0 order by time_s asc";
			$speed = $this -> select($sql);
			$spVal[$speedT] = $speed[0][0];
			$speedT++;			
		}
		unset($last);
		unset($data);
		unset($speed);
		unset($spd);
		$max = (count($spVal) >= 1) ? max($spVal) : 0;
		//echo "$max |";
		return $max;
	}

	function minSpeed($sp,$fld,$type="")
	{
		$data = $this -> driveSprint($sp);
		$dVal = $this -> dev;
		$datAct = new datAct('device1');
		if($type != 1)
		{
			$spd = $datAct->valuefield($fld);
			$spd = $spd[0];
		}
		else
		{
			$spd = $fld;
		}
		//(60*5)
		$speedT = 0;
		$spVal = array();
		foreach($data as $d)
		{
			$sprint = $d['Fsprint'];
			$sql = "select min($spd*1) from device_data where device_id='$dVal' and Fsprint='$sprint' and  ($spd*1) <> 0 order by time_s asc";
			$speed = $this -> select($sql);
			$spVal[$speedT] = $speed[0][0];
			$speedT++;
		}
		unset($last);
		unset($data);
		unset($speed);
		unset($spd);
		$max = (count($spVal) >= 1) ? min($spVal) : 0;
		return $max;
	}

	function getNoDrive($deviceID)
	{
		$sqlLastD = "select nodrive from device_drive where device_id='$deviceID' and (dur*1)>0 order by id desc limit 0,1";
		$datLastD = $this -> select($sqlLastD);
		$drives = $datLastD[0]['nodrive'];
		if($drives <= 0)
		{
			$sql = "select count((dur*1)) dur from device_drive where device_id='$deviceID' and (dur*1) >= 1";
			$dat = $this -> select($sql);
			$dat = $dat[0]['dur'];
			$drives = $dat;
		}
		return $drives;
	}

	function callDriveUpdate($reqFor,$deviceID,$dt,$row)
	{
		if($reqFor > 100)
		{
			/*distance
			 totalTime
			 maxSpeed
			 totalFuelCons
			 idleTime			 
			 */
			$datAct = new datAct('va_device1');
			$fA1 = $datAct->valueSubfield('distance');
			$fA2 = $datAct->valueSubfield('totalTime');
			$fA3 = $datAct->valueSubfield('maxSpeed');
			$fA4 = $datAct->valueSubfield('totalFuelCons');
			$fA5 = $datAct->valueSubfield('idleTime');
			
			// $datActNew = new datAct('device1');
			// $fA5 = $datActNew->valuefield('obdIdleTime');
			// $fB1 = $datActNew->valuefield('obdMileage');
			// $fB2 = $datActNew->valuefield('obdMaxTorque');
			// $fB3 = $datActNew->valuefield('obdMaxVehSpeed');
			// $fB4 = $datActNew->valuefield('obdOvrRunDuration');
			// $fB5 = $datActNew->valuefield('obdFuelSavedOvrRun');
			// $fB6 = $datActNew->valuefield('obdOvrRunDistance');
			// $fB7 = $datActNew->valuefield('obdFuelConsumed');
			// $fB8 = $datAct->valuefield('obdAvgSpeed');
			
			/*$Ch1 = $row[$fA5[0]];
			$Ch2 = $row[$fB1[0]];
			$Ch3 = $row[$fB2[0]];
			$Ch4 = $row[$fB3[0]];
			
			echo "$Ch1 | $Ch2 | $Ch3 | $Ch4 | ";*/	
			//echo "$fA5[0] | $fB1[0] |  $fB2[0] | $fB3[0] | $fB4[0] | $fB5[0] | $fB6[0] | $fB7[0] |";	
			
			//echo $fA5[0];		
			//print_r($row);
			
			$this -> dev = $deviceID;
			$dur = $this -> durationofDrive($reqFor);	
				
			//Duration of drive	
			$idT = ($row[$fA5[0]] > 0) ? $row[$fA5[0]] : $this -> idleTime(1);
			$durTr = ($row[$fA2[0]] > 0) ? $row[$fA2[0]] : $dur[0][0];
			$distTr = ($row[$fA1[0]] > 0) ? $row[$fA1[0]] : round($dur[0][1],2);
			//echo "$durTr != 0 && $distTr != 0";
			if($durTr != 0 && $distTr != 0)
			{		
				$fDrT = $dur[0][3];
				$tDrT = $dur[0][2];
				
				//Max Torque
				$maxTor = ($row[$fB2[0]] > 0) ? $row[$fB2[0]] : $this -> maxSpeed($reqFor,'Ftorque',1);
				
				//Maximum speed
				$maxSpeed = ($row[$fB3[0]] > 0) ? $row[$fB3[0]] : $this -> maxSpeed($reqFor,'obdSpeed');
				
				//Fuel consumed
				$totCon = ($row[$fB7[0]] > 0) ? $row[$fB7[0]] :  $this -> totSpeedLast($reqFor,'Ffcon',1);
				
				//Duration of Overrun $ovT
				//Fuel saved due to Overrun $fOver
				if($row[$fB4[0]] > 0 && $row[$fB6[0]] > 0)
				{
					$ovT = $row[$fB4[0]]; //* (60*60);
					$ovD = $row[$fB6[0]];
				}
				else
				{
					$overRun = $this -> overRun($reqFor);
					$ovT = $overRun[0];	// * (60*60);
					$ovD = $overRun[1];
				}
				
				$fOver = 0;
				//$ovT = 10;
				if($row[$fB5[0]] > 0)
				{
					$fOver = $row[$fB5[0]];
				}
				else
				{
					if($ovT > 0 && $durTr > 0)
					{
						$ovDCalcOnly = $overRun[1]*1;
						$fOver = $totCon * ($ovDCalcOnly/$distTr);
						//echo "$fOver = $totCon * ($ovDCalcOnly/$distTr)";
						//Fuel Consumed (L) * (Distance of Overrun / Total Distance Travelled);
						//$ovTCalcOnly = $overRun[0]*1;
						//$fOver = $totCon * ($ovTCalcOnly/$durTr);
						//echo "$fOver = $totCon * ($ovTCalcOnly/$durTr);";
						//Fuel Consumed (L) * (Duration of Overrun / Total Time of Drive);
					}
				}
				
				//Average Mileage
				$avfMil = ($row[$fB1[0]] > 0) ? $row[$fB1[0]] : $this -> totSpeedLastAvg($reqFor,'Fmil',1);
				
				//Idle time
				$idT = ($row[$fA5[0]] > 0) ? $row[$fA5[0]] : $this -> idleTime($reqFor);
				
				//Average speed
				$avSpeed = ($row[$fB8[0]] > 0) ? $row[$fB8[0]] : $this -> avgSpeed($reqFor,'obdSpeed');
								
				
				//Total distance covered above value is there	$distTr;
				 
				//Total distance covered in Overrun	$ovD;
				
				//Mileage
				$avfMil = ($row[$fB1[0]] > 0) ? $row[$fB1[0]] : $this -> totSpeedLastAvg($reqFor,'Fmil',1);
				
				$PeakRailPressure = $this -> maxSpeed(1,'obdFuelCommonRailPressure');
				$PeakMAP = $this -> maxSpeed(1,'obdManifoldPressure');
				$PeakMAF = $this -> maxSpeed(1,'obdAirFlowRate');
				$LowestBatteryVoltage = $this -> minSpeed(1,'obdControlModuleVoltage');
				
				$sqlCo2 = "select sum((cur_co2*1)) tot_co2 from device_drive where device_id='$deviceID' and (cur_co2*1)>=1 limit 0,1";
				$datCo2 = $this -> select($sqlCo2);
				$datCo2Drive = $this -> getNoDrive($deviceID);
				
				$totCon = $this -> totSpeedLast($reqFor,'Ffcon',1);
				$totMil = $this -> totSpeedLastAvg($reqFor,'Fmil',1);
				$valP = $this -> getvehicle($deviceID);
				$factVal = ($valP[0]['co2fact'] >= 1) ? $valP[0]['co2fact'] : 2320;
				$cur_co2 = ($avfMil >= 1) ? $factVal/$avfMil : 0; // 2320 fetch from calibration setings of device
				$tot_co2 = $datCo2[0]['tot_co2'] + $cur_co2;
				$avg_co2 = ($datCo2Drive > 0) ? ($tot_co2 / $datCo2Drive) : 0;
				
				$oilVal = $this -> calcOilEngLife($reqFor,$deviceID,$dt,$row,$durTr);		
				$oilUsedLife = $oilVal[0];
				$pOfOilLf = $oilVal[1];
				
				/*$bathel = $this -> calcBatHel($reqFor,$deviceID,$dt,$row,$durTr);
				$boost = $this -> turboChargerHealth($reqFor,$deviceID,$dt,$row,$durTr);*/
				//echo "callCaliSet($reqFor,$deviceID,$dt,$row,$durTr)";
				$driveNo = $this -> getNoDrive($deviceID);
				//$this -> callCaliSet($reqFor,$deviceID,$dt,$row,$durTr);
				//$this -> callMonitorSet($reqFor,$deviceID,$dt,$row,$durTr,$driveNo);
				//$this -> setRegMonitor($reqFor,$deviceID,$dt,$row,$durTr);			
				$postVal = array();
				$postVal['device_id'] = $deviceID;
				$postVal['dt'] = $dt;
				$postVal['nodrive'] = $driveNo;
				$postVal['drive'] = $reqFor;
				$postVal['dur'] = $durTr;
				$postVal['dist'] = $distTr;
				$postVal['max_tor'] = $maxTor;
				$postVal['max_sp'] = $maxSpeed;
				$postVal['dur_ovr'] = $ovT;
				$postVal['ful_ovr'] = $fOver;
				$postVal['ful_c'] = $totCon;
				$postVal['avg_ml'] = $avfMil;
				$postVal['idt'] = $idT;
				$postVal['avg_speed'] = $avSpeed;
				$postVal['dist_ovr'] = $ovD;
				
				$postVal['max_railp'] = $PeakRailPressure;
				$postVal['max_map'] = $PeakMAP;
				$postVal['max_maf'] = $PeakMAF;
				$postVal['min_batv'] = $LowestBatteryVoltage;
				
				$postVal['cur_co2'] = $cur_co2;
				$postVal['tot_co2'] = $tot_co2;
				$postVal['avg_co2'] = $avg_co2;
				
				$postVal['oillife'] = $oilUsedLife;
				$postVal['oillifep'] = $pOfOilLf;
				
				$postVal['ad_dt'] = date('Y-m-d H:i:s');
				
				$postVal['dfrm'] = $fDrT;
				$postVal['dto'] = $tDrT;			
				
			 	//$sql = "select * from device_drive where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
				$sql = "select * from device_drive where device_id='$deviceID' and drive = '$reqFor' limit 0,1";
				$dat = $this -> select($sql);
				if(count($dat) <= 0)
				{
					$postVal['nodrive'] += 1;	
					$saveData = new saveForm('device_drive',$postVal);
					if($saveData -> addData());
				}
				else
				{
					$postVal['id'] = $dat[0]['id'];
					$saveData = new saveForm('device_drive',$postVal);
					if($saveData -> updateData('id','id'));
				}
			}
			else
			{
				//$this -> callCaliSet($reqFor,$deviceID,$dt,$row,$durTr);
				//$sql_log = "update device set act='Calib: CP0' where device_id='$deviceID'"; if(mysql_query($sql_log)){ }
			}
		}
	}

	function driveValue($drive,$fld,$op='sum')
	{
		$dVal = $this -> dev;
		//$sql = "select $op($fld*1) as val from device_drive where device_id='$dVal' order by id desc, ad_dt desc limit 0,$drive";
		$sql = "select $op($fld*1) as val from ( select $fld from device_drive where device_id='$dVal' and ($fld*1)>=0 and (dur*1)>0 order by id desc, ad_dt desc limit 0,$drive) as $fld";
		//echo ($fld == 'max_maf') ? $sql : '';
		$dat = $this->select($sql);
		return (trim($dat[0]['val']) != "") ? $dat[0]['val'] : 0;
	}
	
	function driveValuedt($fr,$to,$fld,$op='sum')
	{
		$dVal = $this -> dev;
		//$sql = "select $op($fld*1) as val from device_drive where device_id='$dVal' order by id desc, ad_dt desc limit 0,$drive";
		$sql = "select $op($fld*1) as val from device_drive where  device_id='$dVal' and dt>='$fr' and dt<='$to' and ($fld*1)>=0 and (dur*1)>0 order by id desc, ad_dt";
		$dat = $this->select($sql);
		return (trim($dat[0]['val']) != "") ? $dat[0]['val'] : 0;
	}
	
	//function for calibration ==================================
	
	function callCaliSet($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$sql = "select v.calib from vehicles as v, device as d where d.id = v.device and d.device_id ='$deviceID'";
		$vehicle = mysql_fetch_array(mysql_query($sql));
		
		if($vehicle['calib'] == 1)
		{
			$setC = $this -> coldIdleLearnings($reqFor,$deviceID,$dt,$row,$durTr);
			$setC = $this -> idleUnderELoad1($reqFor,$deviceID,$dt,$row,$durTr);
			$setC = $this -> idleUnderELoad2($reqFor,$deviceID,$dt,$row,$durTr);
			$setC = $this -> warmIdleLearning($reqFor,$deviceID,$dt,$row,$durTr);
			$setC = $this -> throttleAccPedPositionLearning($reqFor,$deviceID,$dt,$row,$durTr);
			$setC = $this -> eGRCommandPercentageLearning($reqFor,$deviceID,$dt,$row,$durTr);
			$setC = $this -> maxEngineLoadLearning($reqFor,$deviceID,$dt,$row,$durTr);

			$postVal = array();
			$postVal['device_id'] = $deviceID;
			$postVal['dt'] = $dt;
			$postVal['drive'] = $reqFor;
			$postVal['dur'] = $durTr;
			$postVal['dist'] = $distTr;
			
			$atmconditions = $this -> getCurrWeather($reqFor,$deviceID,$dt,$row,$durTr);
			$postVal['press'] = $atmconditions[2];
			$postVal['temp'] = $atmconditions[0];
			$postVal['hmdty'] = $atmconditions[1];
			
			$driveconditions = $this -> getDrivabilityCondition($atmconditions[0],$atmconditions[1],$atmconditions[2]);
			$postVal['drvblty'] = $driveconditions;
			
			$bathel = $this -> calcBatHel($reqFor,$deviceID,$dt,$row,$durTr);
			$postVal['bat_st'] = $bathel;
				
			$boost = $this -> turboChargerHealth($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2]);
			$postVal['idlb_c'] = $boost[0];
			$postVal['idlb_m'] = $boost[1];
			$postVal['pekb_c'] = $boost[2];
			$postVal['pekb_m'] = $boost[3];
			
			$boost = $this -> airSystemHealth($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2]);
			$postVal['idlbc_v'] = $boost[0];
			$postVal['idlbc_m'] = $boost[1];
			$postVal['peakb_v'] = $boost[2];
			$postVal['peakb_m'] = $boost[3];
			
			$boost = $this -> fuelSystemHealth($reqFor,$deviceID,$dt,$row,$durTr);
			$postVal['crpm'] = $boost[0];
			$postVal['idl_crp'] = $boost[1];
			$postVal['peak_crp'] = $boost[2];
			
			$boost = $this -> combustionPressure($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2],$driveconditions);
			$postVal['comp_pres'] = $boost[0];
			$postVal['tune_msg'] = $boost[1];
			$postVal['max_drive_torque'] = $boost[2];
			$postVal['reference_torque'] = $boost[3];
			
			$boost = $this -> combustionPressureValue($reqFor,$deviceID,$dt,$row,$durTr);
			$postVal['ocilv'] = $boost;
			
			$boost = $this -> rpmOscillationAnomalyAnalysis($reqFor,$deviceID,$dt,$row,$durTr);
			$postVal['eng_block_ej'] = $boost;			
			
			$falMet = $this -> falseOdometerReading($reqFor,$deviceID,$dt,$row,$durTr);
			$postVal['falsemeter'] = $falMet;
			
			$vehSpeed = $this ->getVehSpeedSence($reqFor,$deviceID);
			$postVal['vehspeed'] = $vehSpeed;
			
			$coolSys = $this ->coolantSysHealth($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[0]);
			$postVal['coolp'] = $coolSys;
			
			/*$boost = $this -> backPressureBuildup($reqFor,$deviceID,$dt,$row,$durTr);
			$postVal['erg_cmd_wrm'] = $boost[0];
			$postVal['ergsmd_ms'] = $boost[1];*/
			//commented  not required in calibration stage

			$alertAct = new alertAct($deviceID);
			$alertAct -> alertCalibAlgo($postVal);
			
			$postVal['ad_dt'] = date('Y-m-d H:i:s');
			$sql = "select * from device_calib where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
			$dat = $this -> select($sql);
			if(count($dat) <= 0)
			{
				$saveData = new saveForm('device_calib',$postVal);
				//if($saveData->addData());
				if($saveId = $saveData->addData())
				{
					$this -> setReportData($deviceID,$dt,$saveId,1);
				}
			}
			else
			{
				$postVal['id'] = $dat[0]['id'];
				$saveData = new saveForm('device_calib',$postVal);
				if($saveData->updateData('id','id'));
			}
			
			//If calibration score is 3 or less next drive should be monitoring
			
			$vehi = $this->getvehicle($deviceID);		
			$vType = $vehi[0]['fuel_tpe'];
			$Turbo_Avail_C = $this -> getCaliPramVal('TC_Status_C',$deviceID);
			
			if ($vType == 2)
			{
				if(($postVal['idlb_c'] <= 2 && $postVal['pekb_c'] < 3) || ($postVal['idl_crp'] <= 2 && $postVal['peak_crp'] < 3) || 
				$postVal['comp_pres'] < 4 || $postVal['eng_block_ej'] < 4 || $postVal['coolp'] < 4)
			 	{
			 		$devAct = new devAct($deviceID);
					$vehInfo = $devAct -> getvehicle($deviceID);
					$MonDrvNum = array();
					$MonDrvNum['id'] = $vehInfo[0]['id'];
					$MonDrvNum['calib'] = 0;
					$MonDrvNum['monitor'] = 2;
					$saveData = new saveForm("vehicles",$MonDrvNum);
					if($saveData->updateData('id','id'));
			 	}
				else
			 	{
			 		$devAct = new devAct($deviceID);
					$vehInfo = $devAct -> getvehicle($deviceID);
					$MonDrvNum = array();
					$MonDrvNum['id'] = $vehInfo[0]['id'];
					$MonDrvNum['calib'] = 0;
					$MonDrvNum['monitor'] = 20;
					$saveData = new saveForm("vehicles",$MonDrvNum);
					if($saveData->updateData('id','id'));
			 	}
			}
			elseif ($vType == 1 && $Turbo_Avail_C == "Yes")
			{
				if(($postVal['idlb_c'] <= 2 && $postVal['pekb_c'] < 3) || $postVal['comp_pres'] < 4 || $postVal['eng_block_ej'] < 4 || 
				$postVal['coolp'] < 4)
			 	{
			 		$devAct = new devAct($deviceID);
					$vehInfo = $devAct -> getvehicle($deviceID);
					$MonDrvNum = array();
					$MonDrvNum['id'] = $vehInfo[0]['id'];
					$MonDrvNum['calib'] = 0;
					$MonDrvNum['monitor'] = 2;
					$saveData = new saveForm("vehicles",$MonDrvNum);
					if($saveData->updateData('id','id'));
			 	}
				else
			 	{
			 		$devAct = new devAct($deviceID);
					$vehInfo = $devAct -> getvehicle($deviceID);
					$MonDrvNum = array();
					$MonDrvNum['id'] = $vehInfo[0]['id'];
					$MonDrvNum['calib'] = 0;
					$MonDrvNum['monitor'] = 20;
					$saveData = new saveForm("vehicles",$MonDrvNum);
					if($saveData->updateData('id','id'));
			 	}
			}
			elseif ($vType == 1 && $Turbo_Avail_C == "No")
			{
				if(($postVal['idlbc_v'] <= 2 && $postVal['peakb_v'] < 3) || $postVal['comp_pres'] < 4 || $postVal['eng_block_ej'] < 4 || 
				$postVal['coolp'] < 4)
			 	{
			 		$devAct = new devAct($deviceID);
					$vehInfo = $devAct -> getvehicle($deviceID);
					$MonDrvNum = array();
					$MonDrvNum['id'] = $vehInfo[0]['id'];
					$MonDrvNum['calib'] = 0;
					$MonDrvNum['monitor'] = 2;
					$saveData = new saveForm("vehicles",$MonDrvNum);
					if($saveData->updateData('id','id'));
			 	}
				else
			 	{
			 		$devAct = new devAct($deviceID);
					$vehInfo = $devAct -> getvehicle($deviceID);
					$MonDrvNum = array();
					$MonDrvNum['id'] = $vehInfo[0]['id'];
					$MonDrvNum['calib'] = 0;
					$MonDrvNum['monitor'] = 20;
					$saveData = new saveForm("vehicles",$MonDrvNum);
					if($saveData->updateData('id','id'));
			 	}
			}
			
			/*
			====== Commented to make next drive monitoring ======
			$devAct = new devAct($deviceID);
			$vehInfo = $devAct -> getvehicle($deviceID);
			$MonDrvNum = array();
			$MonDrvNum['id'] = $vehInfo[0]['id'];
			$MonDrvNum['calib'] = 0;
			$MonDrvNum['monitor'] = 20;
			$saveData = new saveForm("vehicles",$MonDrvNum);
			if($saveData->updateData('id','id'));
			//{}
			*/
		}
	}
	
	// function to fetch weather information using lat long from the device
	function getCurrWeather($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		
		$fld = $datAct -> valuefield('obdBarometricPressure');	 	
		$atmpress  = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceID);
		
		$CurrCityPress = $this -> cityPres($deviceID);
		
		$lat = $this -> getCalStageData(8,"lat","max",$reqFor,$calStg,$deviceID);
		$long = $this -> getCalStageData(8,"lon","max",$reqFor,$calStg,$deviceID);
		$alt = $this -> getCalStageData(8,"alt","max",$reqFor,$calStg,$deviceID);
		
		if($lat != "" || $long != "" || $alt != "")
		{
			if($lat != "null" || $lon != "null" || $alt != "null")
			{
				$url="http://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&units=metric&cnt=7&lang=en&appid=8f91e1e5881797629a26f45ab3298ac3";
				$json=file_get_contents($url);
				$data=json_decode($json,true);
				//Get current Temperature in Celsius
				$CurTemp = $data['main']['temp'];
				//echo $data['main']['temp'];
				//Get current Humidity in Percentage
				$CurHumi = $data['main']['humidity'];
				//echo $data['main']['humidity'];
				//Get current Absolute/corrected Pressure in hPa
				$CurAbsPress = $data['main']['pressure'];
				//echo $data['main']['pressure'];
				//Get current Ground Level Pressure in hPa
				$CurGrndPress = $data['main']['grnd_level'];
				//echo "CurGrndPress = $CurGrndPress \n";
				
				if($CurGrndPress == "")
				{
					//Apply pressure correction
					$CurTempKelvin = $CurTemp + 273.15;
					$den = $CurTempKelvin * 29.263;
					$num = $alt;
					$expfact = $alt / $den;
					$fact = 1 / exp($expfact);
					
					$CurrBaroPresshPa = $CurAbsPress * $fact;
					$CurrBaroPresskPa = $CurrBaroPresshPa / 10;
					$CurrBaroPress = floor($CurrBaroPresskPa);
					$CurrBaroPress = $CurrBaroPress - 1;
					
					//$CurrCityPress = $this -> cityPres($deviceID);
					/*$diff = $CurrBaroPress - $CurrCityPress;
					if($diff > 2)
					{
						$CurrBaroPress = $CurrCityPress;
					}
					else
					{
						$CurrBaroPress = $CurrBaroPress;
					}*/
					//echo "\n CurTempKelvin = $CurTempKelvin \n den = $den \n num = $num \n expfact = $expfact \n fact = $fact \n CurrBaroPresshPa = $CurrBaroPresshPa \n CurrBaroPresskPa = $CurrBaroPresskPa \n CurrBaroPress = $CurrBaroPress \n";	
				}
				else
				{
					//No correction required
					$CurrBaroPresshPa = $CurGrndPress;
					$CurrBaroPresskPa = $CurrBaroPresshPa / 10;
					$CurrBaroPress = floor($CurrBaroPresskPa);
					$CurrBaroPress = $CurrBaroPress - 1;
					
					//$CurrCityPress = $this -> cityPres($deviceID);
					/*$diff = $CurrBaroPress - $CurrCityPress;
					if($diff > 2)
					{
						$CurrBaroPress = $CurrCityPress;
					}
					else
					{
						$CurrBaroPress = $CurrBaroPress;
					}*/
				}
			}
			else
			{
				$CurTemp = 20;
				$CurHumi = 91;
				if($atmpress != "")
				{
					$CurrBaroPress = $atmpress;
				}
				else
				{
					$CurrBaroPress = $CurrCityPress;
				}
			}
		}
		else
		{
			$CurTemp = 20;
			$CurHumi = 91;
			if($atmpress != "")
			{
				$CurrBaroPress = $atmpress;
			}
			else
			{
				$CurrBaroPress = $CurrCityPress;
			}
		}
		return array($CurTemp,$CurHumi,$CurrBaroPress);
	}
	
	//Get drivability condition
	function getDrivabilityCondition($temp,$hmdty,$press)
	{
		//$datAct = new datAct('device1');
		//$calStg = $datAct->valuefield('obdCalStage');
		//$calStg = $calStg[0];

		//$sql = "select * from device_calib where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
		//$dat = $this -> select($sql);
		$CurTemp = $temp;
		$CurHumi = $hmdty;
		$CurrPres = $press;
		
		//echo "$deviceID | $dt | $reqFor | $CurTemp | $CurHumi | $CurrPres | ";	
		//$weatherArray = $this -> getCurrWeather($reqFor,$deviceID,$dt,$row,$durTr);
		//$CurTemp = $weatherArray[0];
		//$CurHumi = $weatherArray[1];
		//$CurrPres = $weatherArray[2];
		
		$Drivability = "Intermediate";
		if($CurTemp != "" && $CurHumi != "")
		{	
			if($CurTemp < 20)
			{
				if($CurHumi > 95)
				{
					$Drivability = "Bad";
				}
				elseif($CurHumi >= 90)
				{
					$Drivability = "Intermediate";
				}
				else
				{
					$Drivability = "Good";
				}
			}
			elseif($CurTemp <= 22.8)
			{
				if($CurHumi > 95)
				{
					$Drivability = "Bad";
				}
				elseif($CurHumi >= 90)
				{
					$Drivability = "Intermediate";
				}
				else
				{
					$Drivability = "Good";
				}
			}
			elseif($CurTemp < 30)
			{
				if($CurHumi > 85)
				{
					$Drivability = "Bad";
				}
				elseif($CurHumi > 60)
				{
					$Drivability = "Intermediate";
				}
				else
				{
					$Drivability = "Good";
				}
			}
			else
			{
				if($CurHumi > 60)
				{
					$Drivability = "Bad";
				}
				elseif($CurHumi > 45)
				{
					$Drivability = "Intermediate";
				}
				else
				{
					$Drivability = "Good";
				}
			}
		}
		else
		{
			$Drivability = "Intermediate";
		}
		return $Drivability;
	}
	
	// start function to set Calibration Variables
	
	//Idle Learnings
	function coldIdleLearnings($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];

		$vehi = $this->getvehicle($deviceID);		
		$vType = $vehi[0]['fuel_tpe'];
		
		$fld = $datAct->valuefield('obdCoolantTemperature');	 	
		$CoolTemp = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID,'asc','in');
		
		$fld = $datAct->valuefield('obdRpm');	 	
		$RPMColdMax = $this -> getCalStageDataRangeSet(3,$fld[0],"max",$reqFor,$calStg,$deviceID,10);
		$RPMColdMin = $this -> getCalStageDataRangeSet(3,$fld[0],"min",$reqFor,$calStg,$deviceID,10);
		$RPMColdAvg = $this -> getCalStageDataRangeSet(3,$fld[0],"avg",$reqFor,$calStg,$deviceID,10);
		
		//echo "AAAA : RPMColdMax $RPMColdMax | RPMColdMin $RPMColdMin | RPMColdAvg $RPMColdAvg";
		$fld = $datAct->valuefield('obdManifoldPressure');	 	
		$MAPColdMin = $this -> getCalStageDataRangeSet(3,$fld[0],"min",$reqFor,$calStg,$deviceID,10);
		
		$RPM_Idle_Cold_C = 0;
		$MAP_Idle_Cold_C = 0;
		if($vType == 1)
		{
			if($CoolTemp <= 63 && $RPMColdMax < 1250)
			{
				$RPM_Idle_Cold_C = $RPMColdMin;
				$MAP_Idle_Cold_C = $MAPColdMin;
			}
			elseif($CoolTemp > 63 && $RPMColdMax < 1000)
			{
				$RPM_Idle_Cold_C = $RPMColdAvg;
				$MAP_Idle_Cold_C = $MAPColdMin;
			}
			else
			{
				//Do nothing
			}
		}
		elseif($vType == 2)
		{
			if($CoolTemp <= 63 && $RPMColdMax < 1100)
			{
				$RPM_Idle_Cold_C = $RPMColdMin;
				$MAP_Idle_Cold_C = $MAPColdMin;
			}
			elseif($CoolTemp > 63 && $RPMColdMax < 900)
			{
				$RPM_Idle_Cold_C = $RPMColdAvg;
				$MAP_Idle_Cold_C = $MAPColdMin;
			}
			else
			{
				//Do nothing
			}
		}
		$this -> setCaliPramVal('RPM_Idle_Cold_C',$deviceID,$RPM_Idle_Cold_C);
		$this -> setCaliPramVal('MAP_Idle_Cold_C',$deviceID,$MAP_Idle_Cold_C);
		return array($RPM_Idle_Cold_C,$MAP_Idle_Cold_C);
	}
	
	//Idle Under E.Load 1
	function idleUnderELoad1($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		$vehi = $this->getvehicle($deviceID);		
		$vType = $vehi[0]['fuel_tpe'];
		
		$fld = $datAct->valuefield('obdEngineLoad');	 	
		$ELoadColdMin4 = $this -> getCalStageDataRangeSet(4,$fld[0],"min",$reqFor,$calStg,$deviceID);
		$ELoadWarmMin11 = $this -> getCalStageDataRangeSet(12,$fld[0],"min",$reqFor,$calStg,$deviceID);
		
		$fld = $datAct->valuefield('obdRpm');	 	
		$RPMColdMax4 = $this -> getCalStageDataRangeSet(4,$fld[0],"max",$reqFor,$calStg,$deviceID);
		$RPMColdMin4 = $this -> getCalStageDataRangeSet(4,$fld[0],"min",$reqFor,$calStg,$deviceID);
		$RPMColdAvg4 = $this -> getCalStageDataRangeSet(4,$fld[0],"avg",$reqFor,$calStg,$deviceID);
		
		$RPMWarmMax11 = $this -> getCalStageDataRangeSet(12,$fld[0],"max",$reqFor,$calStg,$deviceID);
		$RPMWarmMin11 = $this -> getCalStageDataRangeSet(12,$fld[0],"min",$reqFor,$calStg,$deviceID);
		$RPMWarmAvg11 = $this -> getCalStageDataRangeSet(12,$fld[0],"avg",$reqFor,$calStg,$deviceID);
		
		$fld = $datAct->valuefield('obdManifoldPressure');	 	
		$MAPColdMin4 = $this -> getCalStageDataRangeSet(4,$fld[0],"min",$reqFor,$calStg,$deviceID);
		$MAPWarmMin11 = $this -> getCalStageDataRangeSet(12,$fld[0],"min",$reqFor,$calStg,$deviceID);
		
		$RPM_Idle_Ld1_C = 0;
		$MAP_Idle_Ld1_C = 0;
		$ELoad_Idle_Ld1_C = 0;
		//RPM
		if($RPMColdMin4 < $RPMWarmAvg11)
		{
			$RPM_Idle_Ld1_C = $RPMColdMin4;
		}
		else
		{
			$RPM_Idle_Ld1_C = $RPMWarmAvg11;
		}
		//MAP
		if($MAPColdMin4 < $MAPWarmMin11)
		{
			$MAP_Idle_Ld1_C = $MAPColdMin4;
		}
		else
		{
			$MAP_Idle_Ld1_C = $MAPWarmMin11;
		}
		//Engine Load
		if($ELoadColdMin4 < $ELoadWarmMin11)
		{
			$ELoad_Idle_Ld1_C = $ELoadColdMin4;
		}
		else
		{
			$ELoad_Idle_Ld1_C = $ELoadWarmMin11;
		}
		$this -> setCaliPramVal('RPM_Idle_Ld1_C',$deviceID,$RPM_Idle_Ld1_C);
		$this -> setCaliPramVal('MAP_Idle_Ld1_C',$deviceID,$MAP_Idle_Ld1_C);
		$this -> setCaliPramVal('ELoad_Idle_Ld1_C',$deviceID,$ELoad_Idle_Ld1_C);
		
		return array($RPM_Idle_Ld1_C,$MAP_Idle_Ld1_C,$ELoad_Idle_Ld1_C);
	}
	
	//Idle Under E.Load 2
	function idleUnderELoad2($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		$vehi = $this->getvehicle($deviceID);		
		$vType = $vehi[0]['fuel_tpe'];
		
		$fld = $datAct->valuefield('obdEngineLoad');	 	
		$ELoadColdMin6 = $this -> getCalStageDataRangeSet(6,$fld[0],"min",$reqFor,$calStg,$deviceID);
		$ELoadWarmMin13 = $this -> getCalStageDataRangeSet(14,$fld[0],"min",$reqFor,$calStg,$deviceID,6);
		
		$fld = $datAct->valuefield('obdRpm');	 	
		$RPMColdMax6 = $this -> getCalStageDataRangeSet(6,$fld[0],"max",$reqFor,$calStg,$deviceID);
		$RPMColdMin6 = $this -> getCalStageDataRangeSet(6,$fld[0],"min",$reqFor,$calStg,$deviceID);
		$RPMColdAvg6 = $this -> getCalStageDataRangeSet(6,$fld[0],"avg",$reqFor,$calStg,$deviceID);
		
		$RPMWarmMax13 = $this -> getCalStageDataRangeSet(14,$fld[0],"max",$reqFor,$calStg,$deviceID,6);
		$RPMWarmMin13 = $this -> getCalStageDataRangeSet(14,$fld[0],"min",$reqFor,$calStg,$deviceID,6);
		$RPMWarmAvg13 = $this -> getCalStageDataRangeSet(14,$fld[0],"avg",$reqFor,$calStg,$deviceID,6);
		
		$fld = $datAct->valuefield('obdManifoldPressure');	 	
		$MAPColdMin6 = $this -> getCalStageDataRangeSet(6,$fld[0],"min",$reqFor,$calStg,$deviceID);
		$MAPWarmMin13 = $this -> getCalStageDataRangeSet(14,$fld[0],"min",$reqFor,$calStg,$deviceID,6);
		
		$RPM_Idle_Ld2_C = 0;
		$MAP_Idle_Ld2_C = 0;
		$ELoad_Idle_Ld2_C = 0;
		//RPM
		if($RPMColdMin6 < $RPMWarmAvg13)
		{
			$RPM_Idle_Ld2_C = $RPMColdMin6;
		}
		else
		{
			$RPM_Idle_Ld2_C = $RPMWarmAvg13;
		}
		//MAP
		if($MAPColdMin6 < $MAPWarmMin13)
		{
			$MAP_Idle_Ld2_C = $MAPColdMin6;
		}
		else
		{
			$MAP_Idle_Ld2_C = $MAPWarmMin13;
		}
		//Engine Load
		if($ELoadColdMin6 < $ELoadWarmMin13)
		{
			$ELoad_Idle_Ld2_C = $ELoadColdMin6;
		}
		else
		{
			$ELoad_Idle_Ld2_C = $ELoadWarmMin13;
		}
		
		$this -> setCaliPramVal('RPM_Idle_Ld2_C',$deviceID,$RPM_Idle_Ld2_C);
		$this -> setCaliPramVal('MAP_Idle_Ld2_C',$deviceID,$MAP_Idle_Ld2_C);
		$this -> setCaliPramVal('ELoad_Idle_Ld2_C',$deviceID,$ELoad_Idle_Ld2_C);
		
		return array($RPM_Idle_Ld2_C,$MAP_Idle_Ld2_C,$ELoad_Idle_Ld2_C);
	}
	
	//Warm Idle Learning
	function warmIdleLearning($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		$vehi = $this->getvehicle($deviceID);		
		$vType = $vehi[0]['fuel_tpe'];
		
		$fld = $datAct->valuefield('obdEngineLoad');	 	
		$ELoadWarmMin = $this -> getCalStageDataRangeSet(11,$fld[0],"min",$reqFor,$calStg,$deviceID);	
		
		$fld = $datAct->valuefield('obdRpm');	 	
		$RPMWarmMax = $this -> getCalStageDataRangeSet(10,$fld[0],"max",$reqFor,$calStg,$deviceID);
		$RPMWarmMin = $this -> getCalStageDataRangeSet(10,$fld[0],"min",$reqFor,$calStg,$deviceID);
		$RPMWarmAvg = $this -> getCalStageDataRangeSet(10,$fld[0],"avg",$reqFor,$calStg,$deviceID);	
		
		$fld = $datAct->valuefield('obdManifoldPressure');	 	
		$MAPWarmMin = $this -> getCalStageDataRangeSet(9,$fld[0],"min",$reqFor,$calStg,$deviceID);
		
		$fld = $datAct->valuefield('obdCoolantTemperature');	 	
		$CoolTemp = $this -> getCalStageData(9,$fld[0],"max",$reqFor,$calStg,$deviceID);
		
		$RPM_Idle_Warm_C = 0;
		$MAP_Idle_Warm_C = 0;
		$ELoad_Idle_Warm_C= 0;
		if($vType == 1)
		{
			if($CoolTemp > 63 && $RPMWarmMax < 1000)
			{
				$RPM_Idle_Warm_C = $RPMWarmAvg;
				$MAP_Idle_Warm_C = $MAPWarmMin;
				$ELoad_Idle_Warm_C= $ELoadWarmMin;
			}
			else
			{
				//Do nothing;
			}
		}
		elseif($vType == 2)
		{
			if($CoolTemp > 63 && $RPMWarmMax < 900)
			{
				$RPM_Idle_Warm_C = $RPMWarmAvg;
				$MAP_Idle_Warm_C = $MAPWarmMin;
				$ELoad_Idle_Warm_C= $ELoadWarmMin;
			}
			else
			{
				//Do nothing;
			}
		}
		$RPM_Idle_Warm_C = round($RPM_Idle_Warm_C);
		$this -> setCaliPramVal('RPM_Idle_Warm_C',$deviceID,$RPM_Idle_Warm_C);
		$this -> setCaliPramVal('MAP_Idle_Warm_C',$deviceID,$MAP_Idle_Warm_C);
		$this -> setCaliPramVal('ELoad_Idle_Warm_C',$deviceID,$ELoad_Idle_Warm_C);
		
		return array($RPM_Idle_Warm_C,$MAP_Idle_Warm_C,$ELoad_Idle_Warm_C);
	}
	
	//Throttle & AccPed Position Learning
	function throttleAccPedPositionLearning($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		$vehi = $this->getvehicle($deviceID);		
		$vType = $vehi[0]['fuel_tpe'];
		
		$fld = $datAct->valuefield('obdThrottle');	 	
		$ThrotMax1 = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceID);
		$ThrotMin1 = $this -> getCalStageData(1,$fld[0],"min",$reqFor,$calStg,$deviceID);	
		$ThrotMin3 = $this -> getCalStageData(7,$fld[0],"min",$reqFor,$calStg,$deviceID);
		$ThrotMin10 = $this -> getCalStageData(11,$fld[0],"min",$reqFor,$calStg,$deviceID);	
		
		$fld = $datAct->valuefield('obdAccPosD');	 	
		$AccPedMax1 = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceID);
		$AccPedMin1 = $this -> getCalStageData(1,$fld[0],"min",$reqFor,$calStg,$deviceID);	
		$ret = array();
		if($vType == 1)
		{
			$ThrMech_DefPos_C = 0;
			$ThrMech_MaxPos_C = 0;
			$ThrMech_IdlePos_C = 0;	
			
			// ZERO/Default Throttle Position – Learning
			$Thr_DefPos_C = $ThrotMin1;
		
			//MAX/100% Throttle Position – Learning
			$Thr_MaxPos_C = $ThrotMax1;
		
			//Idle Throttle position – Learning
			if($ThrotMin10 < $ThrotMin3)
			{
				$Thr_IdlePos_C = $ThrotMin10;
			}
			else
			{
				$Thr_IdlePos_C = $ThrotMin3;
			}
			
			$this -> setCaliPramVal('Thr_DefPos_C',$deviceID,$ThrMech_DefPos_C);
			$this -> setCaliPramVal('Thr_MaxPos_C',$deviceID,$ThrMech_MaxPos_C);
			$this -> setCaliPramVal('Thr_IdlePos_C',$deviceID,$ThrMech_IdlePos_C);
			
			$ret = array($ThrMech_DefPos_C,$ThrMech_MaxPos_C,$ThrMech_IdlePos_C);
		}
		elseif($vType == 2)
		{
					
			$AccPed_DefPos_C = 0;
			$AccPed_MaxPos_C = 0;
		
			// ZERO/Default Throttle Position – Learning
			$AccPed_DefPos_C = $AccPedMin1;
			// MAX/100% Throttle Position – Learning
			$AccPed_MaxPos_C = $AccPedMax1;
			
			$this -> setCaliPramVal('AccPed_DefPos_C',$deviceID,$AccPed_DefPos_C);
			$this -> setCaliPramVal('AccPed_MaxPos_C',$deviceID,$AccPed_MaxPos_C);
			
			$ret = array($AccPed_DefPos_C,$AccPed_MaxPos_C);
		}
		
		return $ret;
	}		
	
	//EGR Command Percentage Learning
	function eGRCommandPercentageLearning($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		$vehi = $this->getvehicle($deviceID);		
		$vType = $vehi[0]['fuel_tpe'];
		
		$fld = $datAct->valuefield('obdCommandedEGR');	 	
		$EGRCMDColdMin = $this -> getCalStageDataRangeSet(7,$fld[0],"min",$reqFor,$calStg,$deviceID);
		$EGRCMDWarmMin = $this -> getCalStageDataRangeSet(9,$fld[0],"min",$reqFor,$calStg,$deviceID);
		
		$EGR_Cmd_WrmIdle_C =0;
		if($EGRCMDColdMin == 0 && $EGRCMDWarmMin == 0)
		{
			$EGR_Cmd_WrmIdle_C = 0;
		}
		elseif($EGRCMDWarmMin > $EGRCMDColdMin)
		{
			$EGR_Cmd_WrmIdle_C = $EGRCMDWarmMin;
		}
		elseif ($EGRCMDWarmMin < $EGRCMDColdMin)
		{
			$EGR_Cmd_WrmIdle_C = $EGRCMDColdMin;
		}
		else
		{
			//do nothing;
		}
		
		$this -> setCaliPramVal('EGR_Cmd_WrmIdle_C',$deviceID,$EGR_Cmd_WrmIdle_C);
		
		return $EGR_Cmd_WrmIdle_C;
	}
	
	//Max Engine Load Learning
	function maxEngineLoadLearning($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		$vehi = $this->getvehicle($deviceID);		
		$vType = $vehi[0]['fuel_tpe'];
		
		$fld = $datAct->valuefield('obdSpeed');	 	
		$CurVehSpeed = $this -> getCalStageData(8,$fld[0],"",$reqFor,$calStg,$deviceID,'desc');
		//$CurVehSpeed = $row[$fld[0]];		
			
		$fld = $datAct->valuefield('obdEngineLoad');	 
		$ELoadWarmMax = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
		
		$Eload_Max_C = 0;		// to be added in calibration list
	
		if($vType == 1)
		{
			//$CurThrPos = Current value of obdThrottle		
			//$fld = $datAct->valuefield('obdThrottle');	 	
			//$CurThrPos = $this -> getCalStageData(8,$fld[0],"",$reqFor,$calStg,$deviceID,'desc');
			//$CurThrPos = $row[$fld[0]];
			
			//if($CurThrPos >= ($Thr_MaxPos_C - 7) && $CurVehSpeed > 65)
			if($CurVehSpeed > 65)
			{
				$Eload_Max_C = $ELoadWarmMax;
			}
			else 
			{
				$Eload_Max_C = $ELoadWarmMax;
			}
		}
		elseif($vType == 2)
		{
			//$CurAccPos = Current value of obdAccPosD
			//$fld = $datAct->valuefield('obdAccPosD');	 	
			//$CurAccPos = $this -> getCalStageData(8,$fld[0],"",$reqFor,$calStg,$deviceID,'desc');
			//$CurAccPos = $row[$fld[0]];
			
			//if($CurAccPos >= ($AccPed_MaxPos_C - 7)&& $CurVehSpeed > 65)
			if($CurVehSpeed > 65)
			{
				$Eload_Max_C = $ELoadWarmMax;
			}
			else 
			{
				$Eload_Max_C = $ELoadWarmMax;
			}
		}	
		
		$this -> setCaliPramVal('Eload_Max_C',$deviceID,$Eload_Max_C);	
		return $Eload_Max_C;
	}
	
	// ===================Learning Fun End ==============================
	
	//Battery System Health	
	function calcBatHel($reqFor,$deviceID,$dt,$row,$durTr)
	{			
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		
		//max of obdCoolantTemperature @ calStage 1
		$colTemp = $datAct->valuefield('obdCoolantTemperature');	 	
		$maxColTemp = $this -> getCalStageData(1,$colTemp[0],"max",$reqFor,$calStg,$deviceID);
		
		//min of obdControlModuleVoltage @ calStage 2 
		$contMVolt = $datAct->valuefield('obdControlModuleVoltage');	 	
		$battVoltLow  = $this -> getCalStageData(2,$contMVolt[0],"min",$reqFor,$calStg,$deviceID);
		
		//echo "$maxColTemp | $battVoltLow";
		
		$Bat_Coolant_Lmt1_C = $this -> getCaliPramVal('Bat_Cool_Lmt1_C',$deviceID);
		$Bat_Coolant_Lmt2_C = $this -> getCaliPramVal('Bat_Cool_Lmt2_C',$deviceID);
		//battery condition limts when coolant is below 15 deg
		$Bat_UnderCoolLmt1_Lmt1_C = $this -> getCaliPramVal('Bat_UnderCoolLmt1_Lmt1_C',$deviceID);
		$Bat_UnderCoolLmt1_Lmt2_C = $this -> getCaliPramVal('Bat_UnderCoolLmt1_Lmt2_C',$deviceID);
		$Bat_UnderCoolLmt1_Lmt3_C = $this -> getCaliPramVal('Bat_UnderCoolLmt1_Lmt3_C',$deviceID);
		$Bat_UnderCoolLmt1_Lmt4_C = $this -> getCaliPramVal('Bat_UnderCoolLmt1_Lmt4_C',$deviceID);
		//battery condition limts when coolant is below 35 deg
		$Bat_OverCoolLmt1_Lmt1_C = $this -> getCaliPramVal('Bat_OverCoolLmt1_Lmt1_C',$deviceID);
		$Bat_OverCoolLmt1_Lmt2_C = $this -> getCaliPramVal('Bat_OverCoolLmt1_Lmt2_C',$deviceID);
		$Bat_OverCoolLmt1_Lmt3_C = $this -> getCaliPramVal('Bat_OverCoolLmt1_Lmt3_C',$deviceID);
		$Bat_OverCoolLmt1_Lmt4_C = $this -> getCaliPramVal('Bat_OverCoolLmt1_Lmt4_C',$deviceID);
		//battery condition limts when coolant is above 35 deg
		$Bat_OverCoolLmt2_Lmt1_C = $this -> getCaliPramVal('Bat_OverCoolLmt2_Lmt1_C',$deviceID);
		$Bat_OverCoolLmt2_Lmt2_C = $this -> getCaliPramVal('Bat_OverCoolLmt2_Lmt2_C',$deviceID);
		$Bat_OverCoolLmt2_Lmt3_C = $this -> getCaliPramVal('Bat_OverCoolLmt2_Lmt3_C',$deviceID);
		$Bat_OverCoolLmt2_Lmt4_C = $this -> getCaliPramVal('Bat_OverCoolLmt2_Lmt4_C',$deviceID);
		
		$batState = 0;
		if($maxColTemp <= $Bat_Coolant_Lmt1_C)
		{	
			if($battVoltLow <= $Bat_UnderCoolLmt1_Lmt1_C)
			{
				if($battVoltLow != 0)
				{
					$batState = 1;
				}
				else
				{
					//do nothing;
				}
			}
			elseif($battVoltLow <= $Bat_UnderCoolLmt1_Lmt2_C)
			{
				$batState = 2;
			}
			elseif($battVoltLow <= $Bat_UnderCoolLmt1_Lmt3_C)
			{
				$batState = 3;
			}
			elseif($battVoltLow <= $Bat_UnderCoolLmt1_Lmt4_C)
			{
				$batState = 4;
			}
			elseif($battVoltLow > $Bat_UnderCoolLmt1_Lmt4_C)
			{
				$batState = 5;
			}
			else
			{
				//do nothing;
			}
		}
		elseif($maxColTemp <= $Bat_Coolant_Lmt2_C)
		{
			if($battVoltLow < $Bat_OverCoolLmt1_Lmt1_C)
			{
				if($battVoltLow != 0)
				{
					$batState = 1;
				}
				else
				{
					//do nothing;
				}
			}
			elseif($battVoltLow <= $Bat_OverCoolLmt1_Lmt2_C)
			{
				$batState = 2;
			}
			elseif($battVoltLow <= $Bat_OverCoolLmt1_Lmt3_C)
			{
				$batState = 3;
			}
			elseif($battVoltLow <= $Bat_OverCoolLmt1_Lmt4_C)
			{
				$batState = 4;
			}
			elseif($battVoltLow > $Bat_OverCoolLmt1_Lmt4_C)
			{
				$batState = 5;
			}
			else
			{
				//do nothing;
			}
		}
		else//if($maxColTemp > $Bat_Coolant_Lmt2_C) 
		{
			if($battVoltLow < $Bat_OverCoolLmt2_Lmt1_C)
			{
				if($battVoltLow != 0)
				{
					$batState = 1;
				}
				else
				{
					//do nothing;
				}
			}
			elseif($battVoltLow <= $Bat_OverCoolLmt2_Lmt2_C)
			{
				$batState = 2;
			}
			elseif($battVoltLow <= $Bat_OverCoolLmt2_Lmt3_C)
			{
				$batState = 3;
			}
			elseif($battVoltLow <= $Bat_OverCoolLmt2_Lmt4_C)
			{
				$batState = 4;
			}
			elseif($battVoltLow > $Bat_OverCoolLmt2_Lmt4_C)
			{
				$batState = 5;
			}
			else
			{
				//do nothing;
			}	
		}
		return $batState;	
	}
	
	//Turbocharger Health
	function turboChargerHealth($reqFor,$deviceID,$dt,$row,$durTr,$baroPress)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
			
		$Turbo_Avail_C = $this -> getCaliPramVal('TC_Status_C',$deviceID);
		if($Turbo_Avail_C == "Yes")
		{
			$fld = $datAct->valuefield('obdManifoldPressure');
			$MAPStatic = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$MAPTest3 = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$MAPTest8 = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$MAPTest9 = $this -> getCalStageData(9,$fld[0],"max",$reqFor,$calStg,$deviceID);
			
			$MAPvalidity = 1;
			if(($MAPTest3 == $MAPTest8) && ($MAPTest8 == $MAPTest9))
			{
				$MAPvalidity = 0;//MAP is invalid
			}
			else
			{
				$MAPvalidity = 1; //MAP is valid
			}
			$this -> setCaliPramVal('MAP_Valid_C',$deviceID,$MAPvalidity); //To change MAP valid calibration label
			
			if($MAPvalidity == 1)
			{
				$MAP_Type_C = $this -> getCaliPramVal('MAP_Type_C',$deviceID);
				$MAP_Type_MinLmt_C = $this -> getCaliPramVal('MAP_Type_MinLmt_C',$deviceID);
				$MAP_Type_MaxLmt_C = $this -> getCaliPramVal('MAP_Type_MaxLmt_C',$deviceID);
				$MAP_Abs_IdleLmt_Wegr_C = $this -> getCaliPramVal('MAP_Abs_IdleLmt_Wegr_C',$deviceID);
				$MAP_Abs_IdleLmt_WOegr_C = $this -> getCaliPramVal('MAP_Abs_IdleLmt_WOegr_C',$deviceID);
				$MAP_Dif_IdleLmt_Wegr_C = $this -> getCaliPramVal('MAP_Dif_IdleLmt_Wegr_C',$deviceID);
				$MAP_Dif_IdleLmt_WOegr_C = $this -> getCaliPramVal('MAP_Dif_IdleLmt_WOegr_C',$deviceID);
				$MAP_Dif_IdleLmt3_C = $this -> getCaliPramVal('MAP_Dif_IdleLmt3_C',$deviceID);
				$MAP_Dif_IdleLmt1_C = $this -> getCaliPramVal('MAP_Dif_IdleLmt1_C',$deviceID);
				$MAP_Abs_PkLmt_C = $this -> getCaliPramVal('MAP_Abs_PkLmt_C',$deviceID);
				$MAP_Dif_PkLmt_C = $this -> getCaliPramVal('MAP_Dif_PkLmt_C',$deviceID);
				$MAP_Abs_PkLmt3old_C = $this -> getCaliPramVal('MAP_Abs_PkLmt3_C',$deviceID);
				$MAP_Abs_PkLmt1_C = $this -> getCaliPramVal('MAP_Abs_PkLmt1_C',$deviceID);
				$MAP_Dif_PkLmt3old_C = $this -> getCaliPramVal('MAP_Dif_PkLmt3_C',$deviceID);
				$MAP_Dif_PkLmt1_C = $this -> getCaliPramVal('MAP_Dif_PkLmt1_C',$deviceID);
				$MAP_EloadOffset_PkLmt_C = $this -> getCaliPramVal('MAP_EloadOffset_PkLmt_C',$deviceID);
				$Eload_Max_C = $this -> getCaliPramVal('Eload_Max_C',$deviceID);
				$CityPress = $this -> getCaliPramVal('Baro_Press_City_C',$deviceID);
	
				$datAct = new datAct('device1');
	
				$fld = $datAct->valuefield('obdBarometricPressure');
				$BaroPress  = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceID,'asc','in');
				$BaroPressMin  = $this -> getCalStageData(1,$fld[0],"min",$reqFor,$calStg,$deviceID,'asc','in');
	
				//Get Environmental Pressure condition
				//$sql = "select * from device_calib where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
				//$dat = $this -> select($sql);
				//$CurrPres = $dat['press'];
				$CurrPres = $baroPress;
				//$weatherArray = $this -> getCurrWeather($reqFor,$deviceID,$dt,$row,$durTr);
				//$CurrPres = $weatherArray[2];
	
				$Barodiff = $CurrPres - $BaroPress;
				if($Barodiff >= 3 || $Barodiff <= -3)
				{
					$Barovalid = "No";
				}
				else
				{
					$Barovalid = "Yes";
				}
	
				if(($BaroPressMin == 0 && $BaroPress == 0) || $Barovalid == "No")
				{
					$BaroPress = $CurrPres;
				}
	
				//$CityPress = $this -> cityPres($deviceID);
				
				$PressDiff = $BaroPress - $CityPress;
				if($PressDiff >= 2) //This logic was added on 24/10/2017 based on the boost produced in mumbai(253kPa) 
				{																//in the figo aspire. algo is been made a little less stringent.
					$PressDiff = $PressDiff - 2;
				}
				else
				{
					//do nothing;
				}
				//Step 3: Check the type of Pressure Sensor
				$vlChk = ($BaroPress - $MAPStatic);
				if($MAP_Type_MinLmt_C < $vlChk &&  $vlChk < $MAP_Type_MaxLmt_C)
				{			 
					$MAP_Type_C = "Absolute";	//DIRECT INPUT INTO CARNET (CALIBRATABLE)//
				}
				else
				{
					$MAP_Type_C = "Differential";  //DIRECT INPUT INTO CARNET (CALIBRATABLE)//
				}
		
				//Set the type of MAP sensor
				$this -> setCaliPramVal('MAP_Type_C',$deviceID,$MAP_Type_C);
				
				//Step 4. Check Idle Boost State for Absolute Pressure Sensor
				$fld = $datAct->valuefield('obdManifoldPressure');
				$MAPIdle = $this -> getCalStageDataRangeSet(9,$fld[0],"max",$reqFor,$calStg,$deviceID);
				$MAPPeak = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
				
				$fld = $datAct->valuefield('obdEngineLoad');
				$eLoadPeak = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
				
				$IDLEboostSCORE = 0;
				$PeakBoostSCORE = 0;
				if($vType == 2)
				{
					$fld = $datAct->valuefield('obdCommandedEGR');
					$EGRCMD = $this -> getCalStageData(9,$fld[0],"",$reqFor,$calStg,$deviceID,'desc'); // last value of the
					$EGRCMDMax  = $this -> getCalStageDataRangeSet(9,$fld[0],"max",$reqFor,$calStg,$deviceID);
					$EGRCMDMin  = $this -> getCalStageDataRangeSet(9,$fld[0],"min",$reqFor,$calStg,$deviceID);
					
					//echo "Turbo Hel : MAPIdle : $MAPIdle | MAPPeak : $MAPPeak | eLoadPeak : $eLoadPeak | EGRCMDMax : $EGRCMDMax | EGRCMDMin : $EGRCMDMin | EGRCMD : $EGRCMD\n";
					
					//Step 4. Check Idle Boost State for Absolute Pressure Sensor
					if($MAP_Type_C == "Absolute")
					{
						if($MAPIdle != 0)
						{
							if($MAPIdle > $BaroPress)
							{
								if($EGRCMD >= 10 && ($MAPIdle >= ($BaroPress + $MAP_Abs_IdleLmt_Wegr_C)))
								{
									$IDLEboostSCORE = 3;
									$msg = "Your MAP values are a little higher than usual. We will keep you posted";
								}
								elseif(($EGRCMDMax == 0 && $EGRCMDMin == 0) && ($MAPIdle >= ($BaroPress + $MAP_Abs_IdleLmt_WOegr_C)))
								{
									$IDLEboostSCORE = 3;
									$msg = "Your MAP values are a little higher than usual. We will keep you posted";
								}
								else
								{
									$IDLEboostSCORE = 3;
								}
							}
							elseif($MAPIdle < $BaroPress)
							{
								$IDLEboostSCORE = 1;
							}
							elseif($MAPIdle == $BaroPress)
							{
								if($EGRCMD >= 12)
								{
									$IDLEboostSCORE = 3;
								}
								else
								{
									$IDLEboostSCORE = 2;
								}
							}
							else
							{
								//do nothing;
							}
						}
						else
						{
							//do nothing;
						}
					}
					else
					{
						//Step 5. Check Idle Boost State for Differential Pressure Sensor
						if($MAPIdle != 0)
						{
							if($MAPIdle >= ($BaroPress - $MAP_Dif_IdleLmt3_C))
							{					
								if($EGRCMD >= 10 && ($MAPIdle >= ($BaroPress - $MAP_Dif_IdleLmt_Wegr_C)))
								{
									$IDLEboostSCORE = 3;
									$msg = "Your MAP values are a little higher than usual. We will keep you posted";
								}
								elseif(($EGRCMDMax == 0 && $EGRCMDMin == 0) && ($MAPIdle >= ($BaroPress - $MAP_Dif_IdleLmt_WOegr_C)))
								{
									$IDLEboostSCORE = 3;
									$msg = "Your MAP values are a little higher than usual. We will keep you posted";
								}
								else 
								{
									$IDLEboostSCORE = 3;
									//echo "I am Here\n";
								}
							}
							elseif($MAPIdle < ($BaroPress - $MAP_Dif_IdleLmt1_C))
							{
								$IDLEboostSCORE = 1;
							}
							else
							{
								$IDLEboostSCORE = 2;
							}
						}
						else 
						{
							//do nothing;
						}
					}
				}
				else
				{
					$IDLEboostSCORE = 3;
				}

				//Step 6. Check Peak Boost State for Absolute Pressure Sensor
				if($MAP_Type_C == "Absolute")
				{
					if($vType == 2)
					{
						$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C + $PressDiff;
					}
					else
					{
						$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C;
					}

					if($MAPPeak != 0)
					{
						if($MAPPeak >= $MAP_Abs_PkLmt3_C)
						{
							if($MAPPeak >= ($MAP_Abs_PkLmt3_C + $MAP_Abs_PkLmt_C))
							{
								$PeakBoostSCORE = 3;
								$msgPeak = "Your MAP values are a little higher than usual. We will keep you posted";
							}
							else
							{
								$PeakBoostSCORE = 3;
							}
						}
						elseif($MAPPeak <= ($MAP_Abs_PkLmt3_C - $MAP_Abs_PkLmt1_C))
						{
							$PeakBoostSCORE = 1;
						}
						else
						{
							if($IDLEboostSCORE == 3)
							{
								$diff = $MAP_Abs_PkLmt3_C - $MAPPeak;
								if($diff <= 2)
								{
									$IDLEboostSCORE = 2;
									$PeakBoostSCORE = 3;
								}
								else
								{
									$PeakBoostSCORE = 2;
								}
							}
							else
							{
								$PeakBoostSCORE = 2;
							}
						}
					}
					else 
					{
						//do nothing
					}
				}
				//Step 7. Check Peak Boost State for Differential Pressure Sensor
				else
				{
					if($vType == 2)
					{
						$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C + $PressDiff;
					}
					else
					{
						$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C;
					}
					
					if($MAPPeak != 0)
					{
						if($MAPPeak >= $MAP_Dif_PkLmt3_C)
						{
							if($MAPPeak >= ($MAP_Dif_PkLmt3_C + $MAP_Dif_PkLmt_C))
							{
								$PeakBoostSCORE = 3;
								$msgPeak = "Your MAP values are a little higher than usual. We will keep you posted";
							}
							else
							{
								$PeakBoostSCORE = 3;
							}
						}
						elseif($MAPPeak <= ($MAP_Dif_PkLmt3_C - $MAP_Dif_PkLmt1_C))
						{
							$PeakBoostSCORE = 1;
						}
						else
						{
							if($IDLEboostSCORE == 3)
							{
								$diff = $MAP_Dif_PkLmt3_C - $MAPPeak;
								if($diff <= 2)
								{
									$IDLEboostSCORE = 2;
									$PeakBoostSCORE = 3;
								}
								else
								{
									$PeakBoostSCORE = 2;
								}
							}
							else
							{
								$PeakBoostSCORE = 2;
							}
						}
					}
					else 
					{
						//do nothing
					}
				}
				//}
			}
			else
			{
				$IDLEboostSCORE = 0;
				$PeakBoostSCORE = 0;
				$msgPeak = "Junk MAP values";
			}
			return array($IDLEboostSCORE,$msg,$PeakBoostSCORE,$msgPeak);
		}
		else
		{
			return array('','Not Applicable','','Not Applicable');
		}
	}

	//Air System Health
	function airSystemHealth($reqFor,$deviceID,$dt,$row,$durTr,$baroPress) // is only for petrol check the option out
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
			
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		$Turbo_Avail_C = $this -> getCaliPramVal('TC_Status_C',$deviceID);

		if($Turbo_Avail_C == "No" && $vType == 1)
		{
			$fld = $datAct->valuefield('obdManifoldPressure');
			//$MAPStatic = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$MAPTest3 = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$MAPTest8 = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$MAPTest9 = $this -> getCalStageData(9,$fld[0],"max",$reqFor,$calStg,$deviceID);
			
			$MAPvalidDB = $this -> getCaliPramVal('MAP_Valid_C',$deviceID);
			if($MAPvalidDB == 1)
			{
				if(($MAPTest3 == $MAPTest8) && ($MAPTest8 == $MAPTest9))
				{
					$MAPvalidity = 0;//MAP is invalid
				}
				else
				{
					$MAPvalidity = 1; //MAP is valid
				}
			}
			else
			{
				$MAPvalidity = 0;	//MAP is invalid
			}
			$this -> setCaliPramVal('MAP_Valid_C',$deviceID,$MAPvalidity); //To change MAP valid calibration label
			
			if($MAPvalidity == 1)
			{
				//$MAP_Air_IdleLmt_C = $this -> getCaliPramVal('MAP_Air_IdleLmt_C',$deviceID);
				//$MAP_Air_IdleLmt3_C = $this -> getCaliPramVal('MAP_Air_IdleLmt3_C',$deviceID);
				//$MAP_Air_IdleLmt1_C = $this -> getCaliPramVal('MAP_Air_IdleLmt1_C',$deviceID);
				$MAP_Air_PkLmt_C = $this -> getCaliPramVal('MAP_Air_PkLmt_C',$deviceID);
				$MAP_Air_PkLmt3_C = $this -> getCaliPramVal('MAP_Air_PkLmt3_C',$deviceID);
				$MAP_Air_PkLmt1_C = $this -> getCaliPramVal('MAP_Air_PkLmt1_C',$deviceID);
				$MAP_Air_RPM_PkLmt_C = $this -> getCaliPramVal('MAP_Air_RPM_PkLmt_C',$deviceID);
				$MAP_Air_RPMOffset_PkLmt_C = $this -> getCaliPramVal('MAP_Air_RPMOffset_PkLmt_C',$deviceID);
				$MAP_Air_EloadOffset_PkLmt_C = $this -> getCaliPramVal('MAP_Air_EloadOffset_PkLmt_C',$deviceID);
				$Eload_Max_C = $this -> getCaliPramVal('Eload_Max_C',$deviceID);
				
				$fld = $datAct->valuefield('obdBarometricPressure');	 	
				$BaroPress  = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceID,'asc','in');
				$BaroPressMin  = $this -> getCalStageData(1,$fld[0],"min",$reqFor,$calStg,$deviceID,'asc','in');
				
				//Get Environmental Pressure condition
				//$sql = "select * from device_calib where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
				//$dat = $this -> select($sql);
				//$CurrPres = $dat['press'];
				$CurrPres = $baroPress;
				//$weatherArray = $this -> getCurrWeather($reqFor,$deviceID,$dt,$row,$durTr);
				//$CurrPres = $weatherArray[2];
				
				$Barodiff = $CurrPres - $BaroPress;
				if($Barodiff >= 3 || $Barodiff <= -3)
				{
					$Barovalid = "No";
				}
				else
				{
					$Barovalid = "Yes";
				}
				
				if(($BaroPressMin == 0 && $BaroPress == 0) || $Barovalid == "No")
				{
					$BaroPress = $CurrPres;
				}

				$fld = $datAct->valuefield('obdEngineLoad');
				$eLoadPeak = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
				
				//Step 3. Check Peak Boost State		
				$fld = $datAct->valuefield('obdManifoldPressure');
				$rpm = $datAct->valuefield('obdRpm');
				$rpmValA = $MAP_Air_RPM_PkLmt_C;
				$rpmValB = 6000; //set as max value which wnt exeed
				$eng = $datAct->valuefield('obdEngineLoad');
				$engVal = $eLoadPeak - $MAP_Air_EloadOffset_PkLmt_C;
				$MAPPeak = $this -> getCalStageDataRange(8,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$eng[0],$engVal);
				
				//Step 3. Check Idle Boost State	
				//$fld = $datAct->valuefield('obdManifoldPressure');	 	
				//$MAPIdle = $this -> getCalStageDataRangeSet(9,$fld[0],"max",$reqFor,$calStg,$deviceID);
				//$MAPPeak = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
				
				$IDLEboostSCORE = 3;
				$msgIdleAir = "";
				
				//==============idle map algo removed as some cars do not support MAP and we do not have all limits==================//
				/*if($MAPIdle >= $MAP_Air_IdleLmt3_C)
				{
					if($MAPIdle >= ($MAP_Air_IdleLmt3_C + $MAP_Air_IdleLmt_C))
					{
						$IDLEboostSCORE = 3;
						$msgIdleAir= "Your MAP values are a little higher than usual. We will keep you posted";
					}
					else 
					{
						$IDLEboostSCORE = 3;
					}
				}
				elseif($MAPIdle <= ($MAP_Air_IdleLmt3 - $MAP_Air_IdleLmt1_C))
				{
					$IDLEboostSCORE = 1;
				}
				elseif($MAPIdle < $MAP_Air_IdleLmt3)
				{
					$IDLEboostSCORE = 2;
				}
				*/
	
				//Step 4. Check Peak Boost State
				//$fld = $datAct->valuefield('obdEngineLoad');
				//$eLoadPeak = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
	
				//echo "Air Sys Hel : BaroPress : $BaroPress |  BaroPressMin : $BaroPressMin | MAPIdle: $MAPIdle | MAPPeak: $MAPPeak\n";
	
				$PeakBoostSCORE = 0;
				//if($eLoadPeak > ($Eload_Max_C - $MAP_Air_EloadOffset_PkLmt_C))
				//{
				if($MAPPeak >= ($BaroPress - $MAP_Air_PkLmt3_C))
				{
					if($MAPPeak >= ($BaroPress + $MAP_Air_PkLmt_C))
					{
						$PeakBoostSCORE = 3;
						$msgPeakAir = "Your MAP values are a little higher than usual. We will keep you posted";
					}
					elseif($MAPPeak == ($BaroPress - $MAP_Air_PkLmt3_C)) 
					{
						$PeakBoostSCORE = 3;
						$IDLEboostSCORE = 2;
					}
					else
					{
						$PeakBoostSCORE = 3;
					}
				}
				elseif($MAPPeak < ($BaroPress - $MAP_Air_PkLmt1_C))
				{
					$PeakBoostSCORE = 1;
				}
				elseif($MAPPeak < ($BaroPress - $MAP_Air_PkLmt3_C))
				{
					$PeakBoostSCORE = 2;
				}
				//}
			}
			else
			{
				$IDLEboostSCORE = 0;
				$PeakBoostSCORE = 0;
				$msgPeakAir = "Junk MAP values or MAP not supported";
			}
			return array($IDLEboostSCORE,$msgIdleAir,$PeakBoostSCORE,$msgPeakAir);
		}
		else
		{
			return array('','Not Applicable','','Not Applicable');
		}
	}
	
	//Fuel System Health
	function fuelSystemHealth($reqFor,$deviceID,$dt,$row,$durTr) // is only for diesel check the option out
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];

		$Injection_Type_C = $this -> getCaliPramVal('Injection_Type_C',$devId);

		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		if($vType == 2 || ($vType == 1 && $Injection_Type_C == "Direct"))
		{
			$fld = $datAct->valuefield('obdFuelCommonRailPressure');	 	
			$FuelPeak  = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$FuelIdle  = $this -> getCalStageDataRangeSet(11,$fld[0],"min",$reqFor,$calStg,$deviceID);
			
			$fld = $datAct->valuefield('obdEngineLoad');	 	
			$eLoadPeak = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
			
			$CRP_Type_C = $this -> getCaliPramVal('CRP_Type_C',$deviceID);
			$Base_CRP_Lmt_C = $this -> getCaliPramVal('Base_CRP_Lmt_C',$deviceID);
			$CRP_MinLmtScore3_C = $this -> getCaliPramVal('CRP_MinLmt3_C',$deviceID);
			//$CRP_MinLmt3_C = $Base_CRP_Lmt_C + $CRP_MinLmtScore3_C;
			$CRP_MinLmt3_C = $Base_CRP_Lmt_C;
			$CRP_MinLmtScore1_C = $this -> getCaliPramVal('CRP_MinLmt1_C',$deviceID);
			//$CRP_MinLmt1_C = $Base_CRP_Lmt_C + $CRP_MinLmtScore1_C;
			$CRP_MinLmt1_C = $Base_CRP_Lmt_C - $CRP_MinLmtScore1_C;
			$CRP_MaxLmtScore3_C = $this -> getCaliPramVal('CRP_MaxLmt3_C',$deviceID);
			$CRP_MaxLmt3_C = $CRP_Type_C - $CRP_MaxLmtScore3_C;
			$CRP_MaxLmtScore1_C = $this -> getCaliPramVal('CRP_MaxLmt1_C',$deviceID);
			$CRP_MaxLmt1_C = $CRP_MaxLmt3_C - $CRP_MaxLmtScore1_C;
			$CRP_ELoadOffset_PkLmt_C = $this -> getCaliPramVal('CRP_ELoadOffset_PkLmt_C',$deviceID);
			$Eload_Max_C = $this -> getCaliPramVal('Eload_Max_C',$deviceID);
			
			//echo "fuelSystemHealth : FuelPeak : $FuelPeak | FuelIdle : $FuelIdle \n";
			$IDLECRPSCORE = 0;
			$msgfuel = "";	
			
			if($FuelPeak == 0 && $FuelIdle == 0)
			{
				$msgfuel = "PID Not Supported";
			}
			else
			{
				//Check Idle Fuel System Health
				if($FuelIdle > $CRP_MinLmt3_C)
				{
					$IDLECRPSCORE = 3;
				}
				elseif($FuelIdle < $CRP_MinLmt1_C)
				{
					$IDLECRPSCORE = 1;
				}
				else
				{
					$IDLECRPSCORE = 2;
				}
				
				//Check Peak Fuel System Health
				$PeakCRPSCORE = 0;
				if($eLoadPeak > ($Eload_Max_C - $CRP_ELoadOffset_PkLmt_C))
				{	
					if($FuelPeak > $CRP_MaxLmt3_C)
					{
						$PeakCRPSCORE = 3;
					}
					elseif($FuelPeak < $CRP_MaxLmt1_C)
					{
						$PeakCRPSCORE = 1;
					}
					else
					{
						$PeakCRPSCORE = 2;
					}
				}
				else
				{
					$msgfuel = "Test conditions not reached.";	
				}
			}
			return array($msgfuel,$IDLECRPSCORE,$PeakCRPSCORE);
		}
		else
		{
			return array('Not Applicable','','');
		}
	}
	
	//Combustion Pressure
	function combustionPressure($reqFor,$deviceID,$dt,$row,$durTr,$baroPress,$drivcond)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		
		$fld = $datAct->valuefield('obdAirFlowRate');	 	
		$MAFTest3 = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID);
		$MAFTest8 = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
		$MAFTest9 = $this -> getCalStageData(9,$fld[0],"max",$reqFor,$calStg,$deviceID);
		
		$MAFvalidity = 1;
		if(($MAFTest3 == $MAFTest8) && ($MAFTest8 == $MAFTest9))
		{
			$MAFvalidity = 0;//MAP is invalid
		}
		else
		{
			$MAFvalidity = 1; //MAP is valid
		}
		$this -> setCaliPramVal('MAF_Valid_C',$deviceID,$MAFvalidity); //To change MAF valid calibration label
		
		if($MAFvalidity == 1)
		{
			$MaxTorque = $this -> getCaliPramVal('Max_Torque_C',$deviceID);	
			$Trq_Lmt1_C = $this -> getCaliPramVal('Trq_Lmt1_C',$deviceID);
			$Trq_Lmt2_C = $this -> getCaliPramVal('Trq_Lmt2_C',$deviceID);
			$Trq_Lmt3_C = $this -> getCaliPramVal('Trq_Lmt3_C',$deviceID);
			$Trq_Lmt4_C = $this -> getCaliPramVal('Trq_Lmt4_C',$deviceID);
			$Trq_RPM_PkLmt_C = $this -> getCaliPramVal('Trq_RPM_PkLmt_C',$deviceID);
			$Trq_EloadOffset_PkLmt_C = $this -> getCaliPramVal('Trq_EloadOffset_PkLmt_C',$deviceID);
			$Trq_Cool_PkLmt_C = $this -> getCaliPramVal('Trq_Cool_PkLmt_C',$deviceID);
			$Eload_Max_C = $this -> getCaliPramVal('Eload_Max_C',$deviceID);
			$RPM_PkLmt_Diesel_C = $this -> getCaliPramVal('Eff_RPM_Limit2_C',$deviceID);
			$RPM_PkLmt_Petrol_C = $this -> getCaliPramVal('Eff_RPM_Limit3_C',$deviceID);
			
			$fld = $datAct->valuefield('obdBarometricPressure');	 	
			$BaroPress  = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceID,'asc','in');
			$BaroPressMin  = $this -> getCalStageData(1,$fld[0],"min",$reqFor,$calStg,$deviceID,'asc','in');
			
			$fld = $datAct->valuefield('obdEngineLoad');	 	
			$eLoadPeak = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
			
			//$TorqueMax3  = $this -> getCalStageData(8,"Ftorque","max",$reqFor,$calStg,$deviceID);
	
			if($vType == 1)
			{
				$Trq_RPM_PkLmt_C = $RPM_PkLmt_Petrol_C; //Max torque is observed above this RPM value
				$MaxTorquecheck = ($MaxTorque * 0.9);
				// $fld = $datAct->valuefield('Ftorque');
				$rpm = $datAct->valuefield('obdRpm');
				$rpmValA = $Trq_RPM_PkLmt_C;
				$rpmValB = 5000;
				$ctemp = $datAct->valuefield('obdEngineLoad');
				$ctempVal= $eLoadPeak - $Trq_EloadOffset_PkLmt_C;
				//$eng = $datAct->valuefield('Ftorque');
				$engVal = $MaxTorquecheck;
				$TorqueMax3 = $this -> getCalStageDataRangeBaseM(8,'Ftorcomp',"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,'Ftorcomp',$engVal);
				/*$fld = $datAct->valuefield('Ftorque');		
				$rpm = $datAct->valuefield('obdRpm');
				$rpmValA = $Trq_RPM_PkLmt_C;
				$rpmValB = 5000;
				$ctemp = $datAct->valuefield('obdEngineLoad');
				$ctempVal= $eLoadPeak - 5;
				$TorqueMax3 = $this -> getCalStageDataTorqueMAX(8,'Ftorque',$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);*/
			}
			else
			{
				$Trq_RPM_PkLmt_C = $RPM_PkLmt_Diesel_C; //Max torque is observed above this RPM value
				// $fld = $datAct->valuefield('Ftorque');
				$rpm = $datAct->valuefield('obdRpm');
				$rpmValA = $Trq_RPM_PkLmt_C;
				$rpmValB = 4000;
				$ctemp = $datAct->valuefield('obdEngineLoad');
				$ctempVal= $eLoadPeak - $Trq_EloadOffset_PkLmt_C;
				$TorqueMax3 = $this -> getCalStageDataRange(8,'Ftorcomp',"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
			}
			
			//Getting average engine load in above max torque condition
			$fld = $datAct->valuefield('obdEngineLoad');
			$rpm = $datAct->valuefield('obdRpm');
			$rpmValA = $Trq_RPM_PkLmt_C;
			$rpmValB = 5000; //set as max value which wnt exceed
			$eng = $datAct->valuefield('obdEngineLoad');
			$engVal = $eLoadPeak - $Trq_EloadOffset_PkLmt_C;
			$col = $datAct->valuefield('obdCoolantTemperature');
			$colVal = $Trq_Cool_PkLmt_C;
			$AvgEload = $this -> getCalStageDataRange(8,$fld[0],"avg",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$eng[0],$engVal,$col[0],$colVal);
			
			if($AvgEload == 0)
			{
				$AvgEload = 1;
			}
			else
			{
				$AvgEload = $AvgEload / $eLoadPeak;
			}
			
			//Get Environmental Pressure condition
			//$sql = "select * from device_calib where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
			//$dat = $this -> select($sql);
			//$CurrPres = $dat['press'];
			$CurrPres = $baroPress;
			//$weatherArray = $this -> getCurrWeather($reqFor,$deviceID,$dt,$row,$durTr);
			//$CurrPres = $weatherArray[2];
			
			$CombPresState = 0;
			$msgTuning = "";
			if($TorqueMax3 > 0)
			{
				$Barodiff = $CurrPres - $BaroPress;
				if($Barodiff >= 3 || $Barodiff <= -3)
				{
					$Barovalid = "No";
				}
				else
				{
					$Barovalid = "Yes";
				}
				
				if(($BaroPressMin == 0 && $BaroPress == 0) || $Barovalid == "No")
				{
					$BaroPress = $CurrPres;
				}
				
				//Get drivability condition
				//$Drivability = $this -> getDrivabilityCondition($reqFor,$deviceID,$dt,$row,$durTr);
				$Drivability =  $drivcond;
				
				$LoadFact1 = 0.95;	//this is a 5% factor for manufacturing variations.
				$LoadFact2 = 0.95;	//this is a 5% factor for accessory loads.
				if($Drivability == "Good")
				{
					$LoadFact3 = 1;
				}
				elseif($Drivability == "Intermediate")
				{
					$LoadFact3 = 0.98;
				}
				elseif($Drivability == "Bad")
				{
					$LoadFact3 = 0.96;
				}
				
				//Pressure correction factor
				//turbocharged petrols do not require the pressure correction factor as seen in the TSI in bangalore and mumbai (max MAP doesn't change)
				if($vType == 1 && $Turbo_Avail_C == "Yes")
				{
					$PresFact = 1; //no correction required
				}
				else
				{
					$PresFact = $BaroPress / 100;
				}
				
				//Engine Load Factor
				$AvgEloadFact = $AvgEload; // Engine Load correction factor
				
				//RPM Factor
				$sprint = $reqFor;
				$sql = "select F4 from device_data where device_id='$deviceID' and Fsprint='$sprint' and (Ftorcomp*1) ='$TorqueMax3' order by time_s asc limit 0,1";
				$ts = $this -> select($sql);
				$Torquemax3RPM = $ts[0]['F4'];
	
				$RPMDiffPer = 0;
				if($vType == 1)
				{
					$RPM_Limit3 = $this -> getCaliPramVal('Eff_RPM_Limit3_C',$deviceID);
					$RPM_Limit4 = $this -> getCaliPramVal('Eff_RPM_Limit4_C',$deviceID);
					
					if(($Torquemax3RPM > $RPM_Limit3) && ($Torquemax3RPM < $RPM_Limit4))
					{
						//do nothing;
					}
					else
					{
						if($Torquemax3RPM > $RPM_Limit4)
						{
							$RPMDiff = $Torquemax3RPM - $RPM_Limit4;
							$RPMDiffPer = ($RPMDiff * 100) / $RPM_Limit4;
							$RPMDiffPer = round($RPMDiffPer,0);
						}
						else
						{
							//If the max torque is recieved before RPM limit 3.
							//do nothing;
						}
					}
				}
				else
				{
					$RPM_Limit2 = $this -> getCaliPramVal('Eff_RPM_Limit2_C',$deviceID);
					$RPM_Limit3 = $this -> getCaliPramVal('Eff_RPM_Limit3_C',$deviceID);
					
					if(($Torquemax3RPM > $RPM_Limit2) && ($Torquemax3RPM < $RPM_Limit3))
					{
						//do nothing;
					}
					else
					{
						if($Torquemax3RPM > $RPM_Limit3)
						{
							$RPMDiff = $Torquemax3RPM - $RPM_Limit3;
							$RPMDiffPer = ($RPMDiff * 100) / $RPM_Limit3;
							$RPMDiffPer = round($RPMDiffPer,0);
						}
						else
						{
							//If the max torque is recieved before RPM limit 2.
							//do nothing;
						}
					}
				}
				if($RPMDiffPer == 0)
				{
					$RPMFactor = 1;
				}
				elseif($RPMDiffPer <= 3)
				{
					$RPMFactor = 0.97;
				}
				elseif($RPMDiffPer <= 5)
				{
					$RPMFactor = 0.964;
				}
				elseif($RPMDiffPer <= 15)
				{
					$RPMFactor = 0.94;
				}
				elseif($RPMDiffPer <= 27)
				{
					$RPMFactor = 0.91;
				}
				elseif($RPMDiffPer <= 34)
				{
					$RPMFactor = 0.875;
				}
				elseif($RPMDiffPer <= 43.5)
				{
					$RPMFactor = 0.81;
				}
				elseif($RPMDiffPer <= 50)
				{
					$RPMFactor = 0.76;
				}
				elseif($RPMDiffPer <= 60)
				{
					$RPMFactor = 0.72;
				}
				elseif($RPMDiffPer <= 80)
				{
					$RPMFactor = 0.55;
				}
				else
				{
					$RPMFactor = 0.50;
				}
				
				$TotalFact = $LoadFact1 * $LoadFact2 * $LoadFact3 * $PresFact * $AvgEloadFact * $RPMFactor;
				
				$NewMaxTorque = $MaxTorque * $TotalFact;
				$TorquePer = ($NewMaxTorque != 0 ) ? ($TorqueMax3 / $NewMaxTorque) * 100 : 0;
						
				//echo "$NewMaxTorque = $MaxTorque * $PresFact | BaroPress : $BaroPress | TorquePer : $TorquePer | MaxTorque : $TorqueMax3\n";
				
				if($TorquePer < $Trq_Lmt1_C)
				{
					if($TorquePer == 0)
					{
						//do nothing;
					}
					else
					{
						$CombPresState = 1;
					}
				}
				elseif($TorquePer <= $Trq_Lmt2_C)
				{
					$CombPresState = 2;
				}
				elseif($TorquePer <= $Trq_Lmt3_C)
				{
					$CombPresState = 3;
				}
				elseif($TorquePer <= $Trq_Lmt4_C)
				{
					$CombPresState = 4;
				}
				elseif($TorquePer > $Trq_Lmt4_C)
				{
					$CombPresState = 5;
				}
				
				//Tuning Detection
				$TuningFact = $LoadFact2 * $PresFact;
				$TuningTorque = $MaxTorque * $TuningFact;
				if($TorqueMax3 > $TuningTorque)
				{
					if($TuningTorque == 0 || $TorqueMax3 == 0)
					{
						//do nothing;
					}
					else
					{
						$msgTuning = "Your Torque values are a little higher than usual. We will keep you posted";
					}
				}
				else
				{
					//do nothing;
				}
			}
			else 
			{
				//condition when engine load is less than 90, i.e. when conditions are not met.
			}
		}
		else
		{
			$CombPresState = 0;
			$msgTuning = "Junk MAF values";
		}
		return array ($CombPresState,$msgTuning,$TorqueMax3,$NewMaxTorque);
	}
	
	function combustionPressureValue($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		
		$RPM_Idle_Warm_C = $this -> getCaliPramVal('RPM_Idle_Warm_C',$deviceID);
		$ELoad_Idle_Warm_C = $this -> getCaliPramVal('ELoad_Idle_Warm_C',$deviceID);
		$Trq_EloadOffset_PkLmt_C = $this -> getCaliPramVal('TrqRating_EloadOffset_PkLmt_C',$deviceID);
		$Trq_RPMOffset_PkLmt_C = $this -> getCaliPramVal('TrqRating_RPMOffset_PkLmt_C',$deviceID);
		
		$fld = $datAct->valuefield('obdEngineLoad');
		$rpm = $datAct->valuefield('obdRpm');
		$rpmValA = $RPM_Idle_Warm_C - $Trq_RPMOffset_PkLmt_C;
		$rpmValB = $RPM_Idle_Warm_C + $Trq_RPMOffset_PkLmt_C;
		$ctemp = $datAct->valuefield('obdEngineLoad');
		$ctempVal= $ELoad_Idle_Warm_C + $Trq_EloadOffset_PkLmt_C;
		$EloadMax = $this -> getCalStageDataRange(11,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
		return $EloadMax;
	}
	
	//RPM Oscillation anomaly analysis
	function rpmOscillationAnomalyAnalysis($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		
		$fld = $datAct->valuefield('obdCoolantTemperature');
		$coolTempMin = $this -> getCalStageData(9,$fld[0],"min",$reqFor,$calStg,$deviceID);
		
		$fld = $datAct->valuefield('obdRpm');
		$MaxobdRpm = $this -> getCalStageDataRangeSet(10,$fld[0],"max",$reqFor,$calStg,$deviceID);
		$MinobdRpm = $this -> getCalStageDataRangeSet(10,$fld[0],"min",$reqFor,$calStg,$deviceID);
		
		$Osc_RPM_Lmt1_C = $this -> getCaliPramVal('Osc_RPM_Lmt1_C',$deviceID);
		$Osc_RPM_Lmt2_C = $this -> getCaliPramVal('Osc_RPM_Lmt2_C',$deviceID);
		$Osc_RPM_Lmt3_C = $this -> getCaliPramVal('Osc_RPM_Lmt3_C',$deviceID);
		$Osc_RPM_Lmt4_C = $this -> getCaliPramVal('Osc_RPM_Lmt4_C',$deviceID);
		$Cool_Lmt_C = $this -> getCaliPramVal('Cool_Lmt_C',$deviceID);
		
		$DeltaRPM = $MaxobdRpm - $MinobdRpm;
		if($coolTempMin >= $Cool_Lmt_C)
		{
			if($vType == 1)
			{
				if($DeltaRPM > $Osc_RPM_Lmt1_C)
				{
					$EngineBlockInjectorState = 1;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt2_C)
				{
					$EngineBlockInjectorState = 2;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt3_C)
				{
					$EngineBlockInjectorState = 3;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 4;
				}
				elseif($DeltaRPM <= $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 5;
				}
			}
			elseif($vType == 2)
			{
				if($DeltaRPM > $Osc_RPM_Lmt1_C)
				{
					$EngineBlockInjectorState = 1;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt2_C)
				{
					$EngineBlockInjectorState = 2;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt3_C)
				{
					$EngineBlockInjectorState = 3;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 4;
				}
				elseif($DeltaRPM <= $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 5;
				}
			}
		}
		else 
		{
			//Do nothing
		}
		
		//echo "coolTempMin : $coolTempMin | MaxobdRpm : $MaxobdRpm | MinobdRpm : $MinobdRpm | DeltaRPM : $DeltaRPM | EngineBlockInjectorState : $EngineBlockInjectorState\n";
		return $EngineBlockInjectorState;
	}
	
	//Back-Pressure build-up increase detection
	function backPressureBuildup($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$vehi = $this->getvehicle($deviceID);		
		$vType = $vehi[0]['fuel_tpe'];
		
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		
		if($vType == 2)
		{
			$fld = $datAct->valuefield('obdCommandedEGR');
			$EGRCMD = $this -> getCalStageData(9,$fld[0],"",$reqFor,$calStg,$deviceID,'desc'); // last value of the	
			$EGRCMDMax10  = $this -> getCalStageData(9,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$EGRCMDMin10  = $this -> getCalStageData(9,$fld[0],"min",$reqFor,$calStg,$deviceID);
			
			$EGRCMDMax3  = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$EGRCMDMin3  = $this -> getCalStageData(3,$fld[0],"min",$reqFor,$calStg,$deviceID);
			
			$EGRCMDWarmAvg  = $this -> getCalStageData(9,$fld[0],"avg",$reqFor,$calStg,$deviceID);
			$EGRCMDColdAvg  = $this -> getCalStageData(3,$fld[0],"avg",$reqFor,$calStg,$deviceID);
			
			$EGRCMDLast = $this -> getCalStageData(9,$fld[0],"",$reqFor,$calStg,$deviceID,'desc');
			//Last value of PID obdCommandedEGR
			
			$EGR_Cmd_Lmt1_C = $this -> getCaliPramVal('EGR_Cmd_Lmt1_C',$deviceID);
			$EGR_Cmd_Lmt2_C = $this -> getCaliPramVal('EGR_Cmd_Lmt2_C',$deviceID);
			$EGR_Cmd_Lmt4_C = $this -> getCaliPramVal('EGR_Cmd_Lmt4_C',$deviceID);
			
			
			if($EGRCMDWarmAvg > $EGRCMDColdAvg)
			{
				$EGR_Cmd_WrmIdle_C = $EGRCMDColdAvg;
			}
			else
			{
				$EGR_Cmd_WrmIdle_C = $EGRCMDWarmAvg;
			}
			
			if($EGRCMDLast >= ($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt1_C))
			{
				$EGRCMDScore = 1;
			}
			elseif((($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt2) <= $EGRCMDLast) && ($EGRCMDLast < ($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt1_C)))
			{
				$EGRCMDScore = 2;
			}
			elseif((($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt4_C) <= $EGRCMDLast) && ($EGRCMDLast < ($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt2)))
			{
				$EGRCMDScore = 4;
			}
			elseif(($EGR_Cmd_WrmIdle_C <= $EGRCMDLast) && ($EGRCMDLast < ($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt4_C)))
			{
				$EGRCMDScore = 5;
			}
			$this -> setCaliPramVal('EGR_Cmd_WrmIdle_C',$deviceID,$EGR_Cmd_WrmIdle_C);	
			return array($EGR_Cmd_WrmIdle_C,$EGRCMDScore);
		}
		else
		{
			$this -> setCaliPramVal('NA',$deviceID,$EGR_Cmd_WrmIdle_C);	
			return array('Not Applicable','Not Applicable');
		}
	}
	
	//False Odometer reading detection
	function falseOdometerReading($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		
		$fld = $datAct->valuefield('obdDistanceSinceErrorCleared');	 	
		$MaxDistErrClrd = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceID);

		$msgodo = "";
		
		$disT = $this -> getDistSince($deviceID);
		$distTrvAll = ($disT[0]+$disT[1]); //get from chart Distance Travelled Since Purchase
		
		if($MaxDistErrClrd <= 65535) //65535 is max value of this PID. values above these are considered rubbish values.
		{
			if($MaxDistErrClrd <= $distTrvAll)
			{
				$msgodo = "Odometer reading Okay";
			}
			else
			{
				$msgodo = "Odometer tampered";
			}
		}
		else
		{
			$msgodo = "Odometer reading Okay";
		}
		return $msgodo;
	}
	
	//coolant system health
	function coolantSysHealth($reqFor,$deviceID,$dt,$row,$durTr,$temp)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		
		//Logic added to ignore the coollant temperature rising during idling
		$Cool_Lmt_C = $this -> getCaliPramVal('Cool_Lmt_C',$deviceID); 
		
		$fld = $datAct->valuefield('obdCoolantTemperature');		
		$speed = $datAct->valuefield('obdSpeed');
		$speedValA = 10;
		$speedValB = 500;
		$ctemp = $datAct->valuefield('obdCoolantTemperature');
		$ctempVal= $Cool_Lmt_C;
		$maxCoolTemp = $this -> getCalStageDataRange(3,$fld[0],"max",$reqFor,$calStg,$deviceID,$speed[0],$speedValA,$speedValB,$ctemp[0],$ctempVal);
		
		//Find maximum coolant temperature during test state 3 irrespective of vehicle idle or running.
		//$fld = $datAct->valuefield('obdCoolantTemperature');	 	
		//$maxCoolTemp = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID);
		
		$sprint = $reqFor;
		$fld = $fld[0];
		$sql = "select time_s from device_data where device_id='$deviceID' and Fsprint='$sprint' and  (time_s*1) <> 0 and ($fld*1) ='$maxCoolTemp' and ($calStg*1)='8' order by time_s asc limit 0,1";
		$ts = $this -> select($sql);
		$ts = $ts[0]['time_s'];
		//echo "AAAA $sql \n";
		
		$sql = "select time_s from device_data where device_id='$deviceID' and Fsprint='$sprint' and  (time_s*1) <> 0 and time_s > '$ts' order by time_s asc limit 15,1";
		$tsN = $this -> select($sql);
		$tsN = $tsN[0]['time_s'];
		//echo "BBBB $sql \n";

		$sql = "select min($fld) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  (time_s*1) <> 0 and time_s >= '$ts' and time_s <= '$tsN' ";
		$minCoolTemp = $this -> select($sql);
		$minCoolTemp = ($minCoolTemp[0]['val'] > 0) ? $minCoolTemp[0]['val'] : $maxCoolTemp;
		
		//echo "minCoolTemp : $minCoolTemp | maxCoolTemp : $maxCoolTemp  \n";
		
		$Base_OvrHt_Cool_Lmt_C = $this -> getCaliPramVal('Base_OvrHt_Cool_Lmt_C',$deviceID);
		$Cool_Temp_Lmt4_C = $this -> getCaliPramVal('Cool_Temp_Lmt4_C',$deviceID);
		
		$Diff = $Base_OvrHt_Cool_Lmt_C - $Cool_Temp_Lmt4_C;
		
		$Cool_Temp_Lmt1percent_C = $this -> getCaliPramVal('Cool_Temp_Lmt1_C',$deviceID);
		$Cool_Temp_Lmt1fact = ($Diff * $Cool_Temp_Lmt1percent_C) / 100;
		$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt1fact;
		
		$Cool_Temp_Lmt2percent_C = $this -> getCaliPramVal('Cool_Temp_Lmt2_C',$deviceID);
		$Cool_Temp_Lmt2fact = ($Diff * $Cool_Temp_Lmt2percent_C) / 100;
		$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt2fact;
		
		$Cool_Temp_Lmt3percent_C = $this -> getCaliPramVal('Cool_Temp_Lmt3_C',$deviceID);
		$Cool_Temp_Lmt3fact = ($Diff * $Cool_Temp_Lmt3percent_C) / 100;
		$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt3fact;
		
		//Intake air/ Atm temperature based correction
		$CurTemp = $temp;
		if($CurTemp == "" || $CurTemp == 0)
		{
			$CurTemp = 25;
		}
		else
		{
			$CurTemp = $CurTemp;
		}
		
		if($CurTemp < 30)
		{
			$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C;
			$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C;
			$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C;
			$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C;
		}
		elseif($CurTemp <= 40)
		{
			$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C + 2;
			$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C + 2;
			$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C + 2;
			$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C + 2;
		}
		else
		{
			$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C + 4;
			$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C + 4;
			$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C + 4;
			$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C + 4;
		}
		
		$coolHelScore = 0;
		if($minCoolTemp > $Cool_Temp_Lmt1_C)
		{
			$coolHelScore = 1;
		}
		elseif($minCoolTemp >= $Cool_Temp_Lmt2_C)
		{
			$coolHelScore = 2;
		}
		elseif($minCoolTemp >= $Cool_Temp_Lmt3_C)
		{
			$coolHelScore = 3;
		}
		elseif($minCoolTemp > $Cool_Temp_Lmt4_C)
		{
			$coolHelScore = 4;
		}
		else
		{
			$coolHelScore = 5;
		}
		return $coolHelScore;
	} 

	//speed Sensor failure
	function getVehSpeedSence($reqFor,$deviceID,$ts="")
	{	
		$datAct = new datAct('device1');		
		$calStg = $datAct->valuefield('obdCalStage');
		$calStg = $calStg[0];
		$fld = $datAct->valuefield('obdSpeed');	 	
		$maxVehSpeed = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);	
		
		if($maxVehSpeed != 0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
		
//End of calibration stage algorithms ==============================================
	
//function for monitor =============================================================

	function callMonitorSet($reqFor,$deviceID,$dt,$row,$durTr,$driveNo)
	{
		$sql = "select v.calib,v.monitor from vehicles as v, device as d where d.id = v.device and d.device_id ='$deviceID'";
		$vehicle = mysql_fetch_array(mysql_query($sql));
		if($vehicle['calib'] != 1 && $vehicle['monitor'] >= 1)
		{
			$driveNo++;
			if(($driveNo % $vehicle['monitor']) == 0)
			{
				$sql = "select * from device_monitor where device_id='$deviceID' and drive<>'' and dur <> '' order by id desc limit 0,1";
				$dat = $this -> select($sql);
				if(count($dat) >= 1 && $dat[0]['repal'] != 1)
				{
					$postVal = $dat[0];
					$alertAct = new alertAct($deviceID);
					$alertAct -> alertMoniAlgo($postVal);
				}
				
				$postVal = array();
				$postVal['device_id'] = $deviceID;
				$postVal['dt'] = $dt;
				$postVal['drive'] = $reqFor;
				$postVal['dur'] = $durTr;
				$postVal['dist'] = $distTr;
				
				$atmconditions = $this -> getCurrWeatherM($reqFor,$deviceID,$dt,$row,$durTr);
				$postVal['press'] = $atmconditions[2];
				$postVal['temp'] = $atmconditions[0];
				$postVal['hmdty'] = $atmconditions[1];
				
				$driveconditions = $this -> getDrivabilityConditionM($atmconditions[0],$atmconditions[1],$atmconditions[2]);
				$postVal['drvblty'] = $driveconditions;
				
				$bathel = $this -> calcBatHelM($reqFor,$deviceID,$dt,$row,$durTr);
				$postVal['bat_st'] = $bathel[1];
				$postVal['bat_c'] = $bathel[0];
				$postVal['bat_f'] = $bathel['flag'];
				
				$boost = $this -> turboChargerHealthM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2]);
				$postVal['idlb_c'] = $boost[0];
				$postVal['idlb_m'] = $boost[1];
				$postVal['pekb_c'] = $boost[2];
				$postVal['pekb_m'] = $boost[3];
				$postVal['tur_f'] = $boost['flag'];
				
				$boost = $this -> airSystemHealthM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2]);
				$postVal['idlbc_v'] = $boost[0];
				$postVal['idlbc_m'] = $boost[1];
				$postVal['peakb_v'] = $boost[2];
				$postVal['peakb_m'] = $boost[3];
				$postVal['air_f'] = $boost['flag'];
				
				$boost = $this -> fuelSystemHealthM($reqFor,$deviceID,$dt,$row,$durTr);
				$postVal['crpm'] = $boost[0];
				$postVal['idl_crp'] = $boost[1];
				$postVal['peak_crp'] = $boost[2];
				$postVal['fuel_f'] = $boost['flag'];
								
				$boost = $this -> combustionPressureM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2],$driveconditions);
				$postVal['comp_pres'] = $boost[0];
				$postVal['tune_msg'] = $boost[1];
				$postVal['max_drive_torque'] = $boost[2];
				$postVal['reference_torque'] = $boost[3];
				$postVal['comp_f'] = ($boost[0] >= 1) ? 1 : 0;
				
				$boost = $this -> combustionPressureValueM($reqFor,$deviceID,$dt,$row,$durTr);
				$postVal['ocilv'] = $boost;
				
				$boost = $this -> rpmOscillationAnomalyAnalysisM($reqFor,$deviceID,$dt,$row,$durTr);
				$postVal['eng_block_ej'] = $boost;
				$postVal['rpm_f'] = ($boost >= 1) ? 1 : 0;
				
				$falMet = $this -> falseOdometerReadingM($reqFor,$deviceID,$dt,$row,$durTr);
				$postVal['falsemeter'] = $falMet;
				
				$vehSpeed = $this ->getVehSpeedSenceM($reqFor,$deviceID);
				$postVal['vehspeed'] = $vehSpeed;
				
				$boost = $this -> backPressureBuildupM($reqFor,$deviceID,$dt,$row,$durTr);
				//$postVal['erg_cmd_wrm'] = $boost[0];
				$postVal['ergsmd_ms'] = $boost[0];
				$postVal['bak_f'] = $boost['flag'];
				
				$coolSys = $this ->coolantSysHealthM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[0]);
				$postVal['coolp'] = $coolSys[0];
				$postVal['coolm'] = $coolSys[1];
				$postVal['cool_f'] = $coolSys['flag'];
				
				$postVal['ad_dt'] = date('Y-m-d H:i:s');
				$sql = "select * from device_monitor where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
				$dat = $this -> select($sql);
				if(count($dat) <= 0)
				{
					$saveData = new saveForm('device_monitor',$postVal);
					//if($saveData->addData());
					if($saveId = $saveData -> addData())
					{
						$this -> setReportData($deviceID,$dt,$saveId,2);
					}
				}
				else
				{
					$postVal['id'] = $dat[0]['id'];
					$saveData = new saveForm('device_monitor',$postVal);
					if($saveData->updateData('id','id'));
				}
			}
			else
			{
				$sql = "select * from device_monitor where device_id='$deviceID' and drive<>'' and dur <> '' order by id desc limit 0,1";
				$dat = $this -> select($sql);
				$postVal = $dat[0];
				if($postVal['id'] >= 1)
				{
					$update = 0;
					if($postVal['bat_f'] == 0)
					{
						$bathel = $this -> calcBatHelM($reqFor,$deviceID,$dt,$row,$durTr);
						$postVal['bat_st'] = $bathel[1];
						$postVal['bat_c'] = $bathel[0];
						$postVal['bat_f'] = $bathel['flag'];
						$update = 1;
					}
					
					$vehi = $this->getvehicle($deviceID);
					$vType = $vehi[0]['fuel_tpe'];
					if($vType == 2)
					{
						if($postVal['tur_f'] == 0 || $postVal['fuel_f'] == 0 || $postVal['comp_f'] == 0)
						{
							$boost = $this -> turboChargerHealthM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2]);
							$postVal['idlb_c'] = $boost[0];
							$postVal['idlb_m'] = $boost[1];
							$postVal['pekb_c'] = $boost[2];
							$postVal['pekb_m'] = $boost[3];
							$postVal['tur_f'] = $boost['flag'];
							
					 		$boost = $this -> fuelSystemHealthM($reqFor,$deviceID,$dt,$row,$durTr);
							$postVal['crpm'] = $boost[0];
							$postVal['idl_crp'] = $boost[1];
							$postVal['peak_crp'] = $boost[2];
							$postVal['fuel_f'] = $boost['flag'];
							
							$boost = $this -> combustionPressureM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2],$driveconditions);
							$postVal['comp_pres'] = $boost[0];
							$postVal['tune_msg'] = $boost[1];
							$postVal['max_drive_torque'] = $boost[2];
							$postVal['reference_torque'] = $boost[3];
							$postVal['comp_f'] = ($boost[0] >= 1) ? 1 : 0;
							
					 		$update = 1;
						}
						else
						{
							//do nothing;
					 	}
					}
					else
					{
						if($postVal['tur_f'] == 0 || $postVal['air_f'] == 0 || $postVal['comp_f'] == 0)
						{
							$boost = $this -> turboChargerHealthM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2]);
							$postVal['idlb_c'] = $boost[0];
							$postVal['idlb_m'] = $boost[1];
							$postVal['pekb_c'] = $boost[2];
							$postVal['pekb_m'] = $boost[3];
							$postVal['tur_f'] = $boost['flag'];
							
							$boost = $this -> airSystemHealthM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2]);
							$postVal['idlbc_v'] = $boost[0];
							$postVal['idlbc_m'] = $boost[1];
							$postVal['peakb_v'] = $boost[2];
							$postVal['peakb_m'] = $boost[3];
							$postVal['air_f'] = $boost['flag'];
							
							$boost = $this -> combustionPressureM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2],$driveconditions);
							$postVal['comp_pres'] = $boost[0];
							$postVal['tune_msg'] = $boost[1];
							$postVal['max_drive_torque'] = $boost[2];
							$postVal['reference_torque'] = $boost[3];
							$postVal['comp_f'] = ($boost[0] >= 1) ? 1 : 0;
							
							$update = 1;
						}
						else
						{
							//do nothing;
						}
					}
					/*
					if($postVal['tur_f'] == 0)
					{	
						$boost = $this -> turboChargerHealthM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2]);
						$postVal['idlb_c'] = $boost[0];
						$postVal['idlb_m'] = $boost[1];
						$postVal['pekb_c'] = $boost[2];
						$postVal['pekb_m'] = $boost[3];
						$postVal['tur_f'] = $boost['flag'];
						$update = 1;
					}
					if($postVal['air_f'] == 0)
					{
						$boost = $this -> airSystemHealthM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2]);
						$postVal['idlbc_v'] = $boost[0];
						$postVal['idlbc_m'] = $boost[1];
						$postVal['peakb_v'] = $boost[2];
						$postVal['peakb_m'] = $boost[3];
						$postVal['air_f'] = $boost['flag'];
						$update = 1;			
					}
					if($postVal['fuel_f'] == 0)
					{
						$boost = $this -> fuelSystemHealthM($reqFor,$deviceID,$dt,$row,$durTr);
						$postVal['crpm'] = $boost[0];
						$postVal['idl_crp'] = $boost[1];
						$postVal['peak_crp'] = $boost[2];
						$postVal['fuel_f'] = $boost['flag'];
						$update = 1;
					}
					if($postVal['comp_f'] == 0)
					{
						$boost = $this -> combustionPressureM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[2],$driveconditions);
						$postVal['comp_pres'] = $boost;
						$postVal['comp_f'] = ($boost >= 1) ? 1 : 0;
						$update = 1;
					}
					*/
					if($postVal['rpm_f'] == 0)
					{
						$boost = $this -> rpmOscillationAnomalyAnalysisM($reqFor,$deviceID,$dt,$row,$durTr);
						$postVal['eng_block_ej'] = $boost;
						$postVal['rpm_f'] = ($boost >= 1) ? 1 : 0;
						$update = 1;
					}
					
					if($postVal['bak_f'] == 0)
					{
						$boost = $this -> backPressureBuildupM($reqFor,$deviceID,$dt,$row,$durTr);
						//$postVal['erg_cmd_wrm'] = $boost[0];
						$postVal['ergsmd_ms'] = $boost[0];
						$postVal['bak_f'] = $boost['flag'];
						//commented  not required in calibration stage
						$update = 1;
					}
					if($postVal['cool_f'] == 0)
					{
						$coolSys = $this ->coolantSysHealthM($reqFor,$deviceID,$dt,$row,$durTr,$atmconditions[0]);
						$postVal['coolp'] = $coolSys[0];
						$postVal['coolm'] = $coolSys[1];
						$postVal['cool_f'] = $coolSys['flag'];
						$update = 1;
					}
					
					if($update == 1)
					{
						$saveData = new saveForm('device_monitor',$postVal);
						if($saveData -> updateData('id','id'));
						
						//added by sarvesh.to be checked if it is right
						$alertAct = new alertAct($deviceID);
						$alertAct -> alertMoniAlgo($postVal);
					}
					elseif($postVal['repal'] != 1)
					{
						//$postVal = $dat[0];
						
						//Commented by sarvesh.to be checked if it is right
						//$alertAct = new alertAct($deviceID);
						//$alertAct -> alertMoniAlgo($postVal);
						
						$saveData = new saveForm('device_monitor',$postVal);
						$postVal['repal'] = 1;
						if($saveData -> updateData('id','id'));
					}
				}
			}
		}
	}
	
	function getCurrWeatherM($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		$fld = $datAct -> valuefield('obdBarometricPressure');
		$atmpress  = $this -> getCalStageData(1,$fld[0],"max",$reqFor,$calStg,$deviceID);
		
		$CurrCityPress = $this -> cityPres($deviceID);
		
		$lat = $this -> getCalStageData(3,"lat","max",$reqFor,$calStg,$deviceID);
		$long = $this -> getCalStageData(3,"lon","max",$reqFor,$calStg,$deviceID);
		$alt = $this -> getCalStageData(3,"alt","max",$reqFor,$calStg,$deviceID);
		
		//echo "$lat | $long | $alt \n";
		
		if($lat != "" || $long != "" || $alt != "")
		{
			if($lat != "null" || $lon != "null" || $alt != "null")
			{
				$url="http://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&units=metric&cnt=7&lang=en&appid=8f91e1e5881797629a26f45ab3298ac3";
				$json=file_get_contents($url);
				$data=json_decode($json,true);
				//echo "$url";
				//Get current Temperature in Celsius
				$CurTemp = $data['main']['temp'];
				//echo $data['main']['temp'];
				//Get current Humidity in Percentage
				$CurHumi = $data['main']['humidity'];
				//echo $data['main']['humidity'];
				//Get current Absolute/corrected Pressure in hPa
				$CurAbsPress = $data['main']['pressure'];
				//echo $data['main']['pressure'];
				//Get current Ground Level Pressure in hPa
				$CurGrndPress = $data['main']['grnd_level'];
				//echo "\n CurGrndPress = $CurGrndPress \n";
				
				if($CurGrndPress == "")
				{
					//Apply pressure correction
					$CurTempKelvin = $CurTemp +273.15;
					$den = $CurTempKelvin * 29.263;
					$num = $alt;
					$expfact = $alt / $den;
					$fact = 1 / exp($expfact);
					
					$CurrBaroPresshPa = $CurAbsPress * $fact;
					$CurrBaroPresskPa = $CurrBaroPresshPa / 10;
					$CurrBaroPress = floor($CurrBaroPresskPa);
					$CurrBaroPress = $CurrBaroPress - 1;
					
					//$CurrCityPress = $this -> cityPres($deviceID);
					/*$diff = $CurrBaroPress - $CurrCityPress;
					if($diff > 2)
					{
						$CurrBaroPress = $CurrCityPress;
					}
					else
					{
						$CurrBaroPress = $CurrBaroPress;
					}*/
					//echo "\n CurTempKelvin = $CurTempKelvin \n den = $den \n num = $num \n expfact = $expfact \n fact = $fact \n CurrBaroPresshPa = $CurrBaroPresshPa \n CurrBaroPresskPa = $CurrBaroPresskPa \n CurrBaroPress = $CurrBaroPress \n";
				}
				else
				{
					//No correction required
					$CurrBaroPresshPa = $CurGrndPress;
					$CurrBaroPresskPa = $CurrBaroPresshPa / 10;
					$CurrBaroPress = floor($CurrBaroPresskPa);
					$CurrBaroPress = $CurrBaroPress - 1;
					
					//$CurrCityPress = $this -> cityPres($deviceID);
					/*$diff = $CurrBaroPress - $CurrCityPress;
					if($diff > 2)
					{
						$CurrBaroPress = $CurrCityPress;
					}
					else
					{
						$CurrBaroPress = $CurrBaroPress;
					}*/
				}
			}
			else
			{
				$CurTemp = 20;
				$CurHumi = 80;
				if($atmpress != "")
				{
					$CurrBaroPress = $atmpress;
				}
				else
				{
					$CurrBaroPress = $CurrCityPress;
				}
			}
		}
		else
		{
			$CurTemp = 20;
			$CurHumi = 80;
			if($atmpress != "")
			{
				$CurrBaroPress = $atmpress;
			}
			else
			{
				$CurrBaroPress = $CurrCityPress;
			}
		}
		
		//echo "\n CurTemp = $CurTemp,CurHumi = $CurHumi,CurAbsPress = $CurAbsPress \n";
		return array($CurTemp,$CurHumi,$CurrBaroPress);
	}
	
	//Get drivability condition
	function getDrivabilityConditionM($temp,$hmdty,$press)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];

		$sql = "select * from device_monitor where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
		$dat = $this -> select($sql);
		$CurTemp = $dat['temp'];
		$CurHumi = $dat['hmdty'];
		$CurrPres = $dat['press'];
		
		//$weatherArray = $this -> getCurrWeatherM($reqFor,$deviceID,$dt,$row,$durTr);
		//$CurTemp = $weatherArray[0];
		//$CurHumi = $weatherArray[1];
		//$CurrPres = $weatherArray[2];
		
		$Drivability = "Intermediate";
		if($CurTemp < 20)
		{
			if($CurHumi > 95)
			{
				$Drivability = "Bad";
			}
			elseif($CurHumi > 90)
			{
				$Drivability = "Intermediate";
			}
			else
			{
				$Drivability = "Good";
			}
		}
		elseif($CurTemp <= 22.8)
		{
			if($CurHumi > 95)
			{
				$Drivability = "Bad";
			}
			elseif($CurHumi > 90)
			{
				$Drivability = "Intermediate";
			}
			else
			{
				$Drivability = "Good";
			}
		}
		elseif($CurTemp < 30)
		{
			if($CurHumi > 85)
			{
				$Drivability = "Bad";
			}
			elseif($CurHumi > 60)
			{
				$Drivability = "Intermediate";
			}
			else
			{
				$Drivability = "Good";
			}
		}
		else
		{
			if($CurHumi > 60)
			{
				$Drivability = "Bad";
			}
			elseif($CurHumi > 45)
			{
				$Drivability = "Intermediate";
			}
			else
			{
				$Drivability = "Good";
			}
		}
		//echo "\n Drivability = $Drivability \n";
		return $Drivability;
	}
	
	//Battery Health- Done
	function calcBatHelM($reqFor,$deviceID,$dt,$row,$durTr,$up=0)
	{
		$flag = 0;
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		//max of obdCoolantTemperature @ calStage 1
		$colTemp = $datAct->valuefield('obdCoolantTemperature');
		$CoolHigh1 = $this -> getCalStageData('(0,1)',$colTemp[0],"min",$reqFor,$calStg,$deviceID,'asc','in');
		//$CoolHigh1 = 26;

		//min of obdControlModuleVoltage @ calStage 2
		$contMVolt = $datAct->valuefield('obdControlModuleVoltage');
		$VoltLow1 = $this -> getCalStageData('(0,1)',$contMVolt[0],"min",$reqFor,$calStg,$deviceID,'asc','in');
		
		
		$sqlGet = "select bathel_c from device  where device_id='$deviceID'";
		$batHel = mysql_fetch_array(mysql_query($sqlGet));
		$Counter = $batHel['bathel_c'];
		//echo "Battery Health : CoolHigh1 $CoolHigh1 | VoltLow1 $VoltLow1";
		
		$Bat_Coolant_Lmt1_C = $this -> getCaliPramVal('Bat_Cool_Lmt1_C',$deviceID);
		$Bat_Coolant_Lmt2_C = $this -> getCaliPramVal('Bat_Cool_Lmt2_C',$deviceID);
		//battery condition limts when coolant is below 15 deg
		$Bat_UnderCoolLmt1_Lmt1_C = $this -> getCaliPramVal('Bat_UnderCoolLmt1_Lmt1_C',$deviceID);
		$Bat_UnderCoolLmt1_Lmt2_C = $this -> getCaliPramVal('Bat_UnderCoolLmt1_Lmt2_C',$deviceID);
		//$Bat_UnderCoolLmt1_Lmt3_C = $this -> getCaliPramVal('Bat_UnderCoolLmt1_Lmt3_C',$deviceID);
		//$Bat_UnderCoolLmt1_Lmt4_C = $this -> getCaliPramVal('Bat_UnderCoolLmt1_Lmt4_C',$deviceID);
		//battery condition limts when coolant is below 35 deg
		$Bat_OverCoolLmt1_Lmt1_C = $this -> getCaliPramVal('Bat_OverCoolLmt1_Lmt1_C',$deviceID);
		$Bat_OverCoolLmt1_Lmt2_C = $this -> getCaliPramVal('Bat_OverCoolLmt1_Lmt2_C',$deviceID);
		//$Bat_OverCoolLmt1_Lmt3_C = $this -> getCaliPramVal('Bat_OverCoolLmt1_Lmt3_C',$deviceID);
		//$Bat_OverCoolLmt1_Lmt4_C = $this -> getCaliPramVal('Bat_OverCoolLmt1_Lmt4_C',$deviceID);
		//battery condition limts when coolant is above 35 deg
		$Bat_OverCoolLmt2_Lmt1_C = $this -> getCaliPramVal('Bat_OverCoolLmt2_Lmt1_C',$deviceID);
		$Bat_OverCoolLmt2_Lmt2_C = $this -> getCaliPramVal('Bat_OverCoolLmt2_Lmt2_C',$deviceID);
		//$Bat_OverCoolLmt2_Lmt3_C = $this -> getCaliPramVal('Bat_OverCoolLmt2_Lmt3_C',$deviceID);
		//$Bat_OverCoolLmt2_Lmt4_C = $this -> getCaliPramVal('Bat_OverCoolLmt2_Lmt4_C',$deviceID);
	
		$batState = 5;
		if($VoltLow1 > 0)
		{
			if($CoolHigh1 <= $Bat_Coolant_Lmt1_C)
			{
				if($VoltLow1 <= $Bat_UnderCoolLmt1_Lmt1_C)
				{
					$batState = 1;
				}
				elseif($VoltLow1 <= $Bat_UnderCoolLmt1_Lmt2_C)
				{
					$Counter++;
					if($up == 1)
					{
						$sql = "update device set bathel_c = (bathel_c+1) where device_id='$deviceID'";
						mysql_query($sql);
					}
					//Non-volatile counter. State to be retained across starts
					if($Counter == 2)
					{
						$batState = 3;
					}
					elseif($Counter >= 3)
					{
						$batState = 2;
					}
					else
					{
						$batState = 4;
					}
				}
				else
				{
					$Counter = 0;	
					// Non-volatile counter. State to be retained across starts								
					if($up == 1)
					{
						$sql = "update device set bathel_c = 0 where device_id='$deviceID'";
						mysql_query($sql);
					}
					
					$batState = 5;
				}
			}
			elseif($CoolHigh1 <= $Bat_Coolant_Lmt2_C)
			{
				if($VoltLow1 <= $Bat_OverCoolLmt1_Lmt1_C)
				{
					$batState = 1;
				}
				elseif($VoltLow1 <= $Bat_OverCoolLmt1_Lmt2_C)
				{
					$Counter++;	
					//Non-volatile counter. State to be retained across starts
					if($up == 1)
					{
						$sql = "update device set bathel_c = (bathel_c+1) where device_id='$deviceID'";
						mysql_query($sql);
					}	
					if($Counter == 2)
					{
						$batState = 3;
					}
					elseif($Counter >= 3)
					{
						$batState = 2;
					}
					else
					{
						$batState = 4;
					}
				}
				else
				{
					$Counter = 0;	
					// Non-volatile counter. State to be retained across starts
					if($up == 1)
					{
						$sql = "update device set bathel_c = 0 where device_id='$deviceID'";
						mysql_query($sql);						
					}
					$batState = 5;
				}
			}
			else//if($CoolHigh1 > $Bat_Coolant_Lmt2_C)
			{
				if($VoltLow1 <= $Bat_OverCoolLmt2_Lmt1_C)
				{
					$batState = 1;
				}
				elseif($VoltLow1 <= $Bat_OverCoolLmt2_Lmt2_C)
				{
					$Counter++;	
					//Non-volatile counter. State to be retained across starts
					if($up == 1)
					{
						$sql = "update device set bathel_c = (bathel_c+1) where device_id='$deviceID'";
						mysql_query($sql);
					}	
					if($Counter == 2)
					{
						$batState = 3;
					}
					elseif($Counter >= 3)
					{
						$batState = 2;
					}
					else
					{
						$batState = 4;
					}
				}
				else
				{
					$Counter = 0;	
					// Non-volatile counter. State to be retained across starts
					if($up == 1)
					{
						$sql = "update device set bathel_c = 0 where device_id='$deviceID'";
						mysql_query($sql);						
					}
					$batState = 5;
				}
			}
		}
		else
		{
			$batState = "5";
			$msg = "PID Not Supported";
		}
		//echo "Bat Counter : $Counter \n";
		$flag = ($batState >= 1 || $batState == "") ? 1 : 0;
		return array ($Counter, $batState,'flag'=>$flag);
	}
	
	//Turbocharger Health - Done
	function turboChargerHealthM($reqFor,$deviceID,$dt,$row,$durTr,$baroPress)
	{
		$flag = 0;
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		$vehi = $this->getvehicle($deviceID);		
		$vType = $vehi[0]['fuel_tpe'];
		
		$Turbo_Avail_C = $this -> getCaliPramVal('TC_Status_C',$deviceID);
		if($Turbo_Avail_C == "Yes")
		{
			$MAP_Type_C = $this -> getCaliPramVal('MAP_Type_C',$deviceID);
			$Eload_Def_C = $this -> getCaliPramVal('Eload_Def_C',$deviceID);
			$MAP_Type_MinLmt_C = $this -> getCaliPramVal('MAP_Type_MinLmt_C',$deviceID);
			$MAP_Type_MaxLmt_C = $this -> getCaliPramVal('MAP_Type_MaxLmt_C',$deviceID);
			$MAP_Abs_IdleLmt_Wegr_C = $this -> getCaliPramVal('MAP_Abs_IdleLmt_Wegr_C',$deviceID);
			$MAP_Abs_IdleLmt_WOegr_C = $this -> getCaliPramVal('MAP_Abs_IdleLmt_WOegr_C',$deviceID);
			$MAP_Dif_IdleLmt_Wegr_C = $this -> getCaliPramVal('MAP_Dif_IdleLmt_Wegr_C',$deviceID);
			$MAP_Dif_IdleLmt_WOegr_C = $this -> getCaliPramVal('MAP_Dif_IdleLmt_WOegr_C',$deviceID);
			$MAP_Dif_IdleLmt3_C = $this -> getCaliPramVal('MAP_Dif_IdleLmt3_C',$deviceID);
			$MAP_Dif_IdleLmt1_C = $this -> getCaliPramVal('MAP_Dif_IdleLmt1_C',$deviceID);
			$MAP_Abs_PkLmt_C = $this -> getCaliPramVal('MAP_Abs_PkLmt_C',$deviceID);
			$MAP_Dif_PkLmt_C = $this -> getCaliPramVal('MAP_Dif_PkLmt_C',$deviceID);
			$MAP_Abs_PkLmt3old_C = $this -> getCaliPramVal('MAP_Abs_PkLmt3_C',$deviceID);
			$MAP_Abs_PkLmt1_C = $this -> getCaliPramVal('MAP_Abs_PkLmt1_C',$deviceID);
			$MAP_Dif_PkLmt3old_C = $this -> getCaliPramVal('MAP_Dif_PkLmt3_C',$deviceID);
			$MAP_Dif_PkLmt1_C = $this -> getCaliPramVal('MAP_Dif_PkLmt1_C',$deviceID);
			$MAP_RPM_PkLmt_C = $this -> getCaliPramVal('MAP_RPM_PkLmt_C',$deviceID);
			$MAP_RPMOffset_PkLmt_C = $this -> getCaliPramVal('MAP_RPMOffset_PkLmt_C',$deviceID);
			$RPM_Idle_Warm_C = $this -> getCaliPramVal('RPM_Idle_Warm_C',$deviceID);
			$RPM_Idle_Ld1_C = $this -> getCaliPramVal('RPM_Idle_Ld1_C',$deviceID);
			$RPM_Idle_Ld2_C = $this -> getCaliPramVal('RPM_Idle_Ld2_C',$deviceID);
			$Cool_Lmt_C = $this -> getCaliPramVal('Cool_Lmt_C',$deviceID);
			$Eload_Max_C = $this -> getCaliPramVal('Eload_Max_C',$deviceID);
			$MAP_EloadOffset_PkLmt_C = $this -> getCaliPramVal('MAP_EloadOffset_PkLmt_C',$deviceID);
			$CityPress = $this -> getCaliPramVal('Baro_Press_City_C',$deviceID);
			
			$datAct = new datAct('device1'); 
			
			$fld = $datAct->valuefield('obdBarometricPressure');
			$BaroPress  = $this -> getCalStageData('(0,4)',$fld[0],"max",$reqFor,$calStg,$deviceID,'asc','in');
			$BaroPressMin  = $this -> getCalStageData('(0,4)',$fld[0],"min",$reqFor,$calStg,$deviceID,'asc','in');
			//$BaroPress  = $this -> getCalStageData('(0,1)',$fld[0],"max",$reqFor,$calStg,$deviceID);
			//$BaroPressMin  = $this -> getCalStageData('(0,1)',$fld[0],"min",$reqFor,$calStg,$deviceID);
			
			//$fld = $datAct->valuefield('obdRpm');
			//$CurrRPM2 = $this -> getCalStageData(2,$fld[0],"max",$reqFor,$calStg,$deviceID);
			//$CurrRPM3 = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID);
			
			$fld = $datAct->valuefield('obdManifoldPressure');
			$rpm = $datAct->valuefield('obdRpm');
			$rpmValA = $RPM_Idle_Warm_C - $MAP_RPMOffset_PkLmt_C;
			$rpmValB = $RPM_Idle_Warm_C + $MAP_RPMOffset_PkLmt_C;
			$ctemp = $datAct->valuefield('obdCoolantTemperature');
			$ctempVal= $Cool_Lmt_C;
			$eload = $datAct->valuefield('obdEngineLoad');
			$eloadVal = $Eload_Def_C;
			$MAPIdle = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$eload[0],$eloadVal,'asc','>');

			$rpmValA = $MAP_RPM_PkLmt_C;
			$rpmValB = 7000; //set as max value which wnt exeed
			$eng = $datAct->valuefield('obdEngineLoad');
			$engVal = $Eload_Max_C - $MAP_EloadOffset_PkLmt_C;
			$MAPPeak = $this -> getCalStageDataRange(3,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$eng[0],$engVal);
			
			//$fld = $datAct->valuefield('obdCommandedEGR');
			//$EGRCMD = $this -> getCalStageDataRange(2,$fld[0],"avg",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
			
			//Get Environmental Pressure condition
			//$sql = "select * from device_monitor where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
			//$dat = $this -> select($sql);
			//$CurrPres = $dat['press'];
			$CurrPres = $baroPress;
			//$weatherArray = $this -> getCurrWeatherM($reqFor,$deviceID,$dt,$row,$durTr);
			//$CurrPres = $weatherArray[2];
			
			$Barodiff = $CurrPres - $BaroPress;
			if($Barodiff >= 3 || $Barodiff <= -3)
			{
				$Barovalid = "No";
			}
			else
			{
				$Barovalid = "Yes";
			}
			
			if(($BaroPressMin == 0 && $BaroPress == 0) || $Barovalid == "No")
			{
				$BaroPress = $CurrPres;
			}
			
			/*$CityPress = $this -> cityPres($deviceID);
			if($BaroPress > $CityPress)
			{
				$PressDiff = $BaroPress - $CityPress;
			}
			else
			{
				$PressDiff = $CityPress - $BaroPress;
			}*/
			
			$PressDiff = $BaroPress - $CityPress;
			if($PressDiff >= 2) //This logic was added on 24/10/2017 based on the boost produced in mumbai(253kPa) 
			{																//in the figo aspire. algo is been made a little less stringent.
				$PressDiff = $PressDiff - 2;
			}
			else
			{
				//do nothing;
			}
			
			//Step 3. Check Idle Boost State for Absolute Pressure Sensor
			//$fld = $datAct->valuefield('obdManifoldPressure');
			//$MAPIdle = $this -> getCalStageData(2,$fld[0],"max",$reqFor,$calStg,$deviceID);
			//$MAPPeak = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID);
			
			//$fld = $datAct->valuefield('obdEngineLoad');
			//$eLoadMax3 = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
				
			$fld = $datAct->valuefield('obdCommandedEGR');
			$EGRCMD = $this -> getCalStageData(2,$fld[0],"",$reqFor,$calStg,$deviceID,'desc'); // last value of EGR
			$EGRCMDMax  = $this -> getCalStageData(2,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$EGRCMDMin  = $this -> getCalStageData(2,$fld[0],"min",$reqFor,$calStg,$deviceID);
			
			//echo "Turbo Hel : rpmValA=$rpmValA rpmValB=$rpmValB engVal=$engVal MAPIdle : $MAPIdle | MAPPeak : $MAPPeak | EGRCMDMax : $EGRCMDMax | EGRCMDMin : $EGRCMDMin | EGRCMD : $EGRCMD\n";
			if($MAPPeak >= 0)
			{
				if($MAPIdle >= 0)
				{
					$MAPIdle = $MAPIdle;
				}
				else
				{
					$fld = $datAct->valuefield('obdManifoldPressure');
					$rpm = $datAct->valuefield('obdRpm');
					$rpmValA = $RPM_Idle_Ld1_C - $MAP_RPMOffset_PkLmt_C;
					$rpmValB = $RPM_Idle_Ld1_C + $MAP_RPMOffset_PkLmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $Cool_Lmt_C;
					$eload = $datAct->valuefield('obdEngineLoad');
					$eloadVal = $Eload_Def_C;
					$MAPIdleLd1 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$eload[0],$eloadVal,'asc','>');
					
					if($MAPIdleLd1 >= 0)
					{
						$MAPIdle = $MAPIdleLd1;
					}
					else
					{
						$fld = $datAct->valuefield('obdManifoldPressure');
						$rpm = $datAct->valuefield('obdRpm');
						$rpmValA = $RPM_Idle_Ld2_C - $MAP_RPMOffset_PkLmt_C;
						$rpmValB = $RPM_Idle_Ld2_C + $MAP_RPMOffset_PkLmt_C;
						$ctemp = $datAct->valuefield('obdCoolantTemperature');
						$ctempVal= $Cool_Lmt_C;
						$eload = $datAct->valuefield('obdEngineLoad');
						$eloadVal = $Eload_Def_C;
						$MAPIdleLd2 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$eload[0],$eloadVal,'asc','>');
						
						if($MAPIdleLd2 >= 0)
						{
							$MAPIdle = $MAPIdleLd2;
						}
						else
						{
							$MAPIdle = $BaroPress + 2;
						}	
					}
				}
				
				$IDLEboostSCORE = 0;
				$PeakBoostSCORE = 0;
				$msg = "";
				$msgPeak = "";
				if($vType == 2)
				{
					if($MAP_Type_C == "Absolute")
					{
						//Step 3. Check Idle Boost State for Absolute Pressure Sensor
						if($MAPIdle > $BaroPress)
						{	
							if($EGRCMD >= 10 && ($MAPIdle >= ($BaroPress + $MAP_Abs_IdleLmt_Wegr_C)))
							{
								$IDLEboostSCORE = 3;
								$msg = "Your MAP values are a little higher than usual. We will keep you posted";
							}
							elseif(($EGRCMDMax == 0 && $EGRCMDMin == 0) && ($MAPIdle >= ($BaroPress + $MAP_Abs_IdleLmt_WOegr_C)))
							{
								$IDLEboostSCORE = 3;
								$msg = "Your MAP values are a little higher than usual. We will keep you posted";
							}
							else
							{
								$IDLEboostSCORE = 3;
							}
						}
						elseif($MAPIdle == BaroPress)
						{
							if($EGRCMD >= 12)
							{
								$IDLEboostSCORE = 3;
							}
							else
							{
								$IDLEboostSCORE = 2;
							}
						}
						else//if($MAPIdle < $BaroPress)
						{
							$IDLEboostSCORE = 1;
						}
					}
					else
					{
						//Step 4. Check Idle Boost State for Differential Pressure Sensor
						if($MAPIdle >= ($BaroPress - $MAP_Dif_IdleLmt3_C))
						{
							if($EGRCMD >= 10 && ($MAPIdle >= ($BaroPress + $MAP_Dif_IdleLmt_Wegr_C)))
							{
								$IDLEboostSCORE = 3;
								$msg = "Your MAP values are a little higher than usual. We will keep you posted";
							}
							elseif(($EGRCMDMax == 0 && $EGRCMDMin == 0) && ($MAPIdle >= ($BaroPress + $MAP_Dif_IdleLmt_WOegr_C)))
							{
								$IDLEboostSCORE = 3;
								$msg = "Your MAP values are a little higher than usual. We will keep you posted";
							}
							else 
							{
								$IDLEboostSCORE = 3;
							}
						}
						elseif($MAPIdle < ($BaroPress - $MAP_Dif_IdleLmt1_C))
						{
							$IDLEboostSCORE = 1;
						}
						else
						{
							$IDLEboostSCORE = 2;
						}
					}
				}
				else
				{
					$IDLEboostSCORE = 3;
				}
				
				//Step: Check Peak Boost State Score
				//$PeakBoostSCORE = 0;
				//$msgPeak = "";
				//if($eLoadMax3 > 90)
				//{
				if($MAP_Type_C == "Absolute")
				{
					if($vType == 2)
					{
						$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C + $PressDiff;
					}
					else
					{
						$MAP_Abs_PkLmt3_C = $MAP_Abs_PkLmt3old_C;
					}
					
					if($MAPPeak >= $MAP_Abs_PkLmt3_C)
					{
						if($MAPPeak >= ($MAP_Abs_PkLmt3_C + $MAP_Abs_PkLmt_C))
						{
							$PeakBoostSCORE = 3;
							$msgPeak = "Your MAP values are a little higher than usual. We will keep you posted";
						}
						else
						{
							$PeakBoostSCORE = 3;
						}
					}
					elseif($MAPPeak <= ($MAP_Abs_PkLmt3_C - $MAP_Abs_PkLmt1_C))
					{
						$PeakBoostSCORE = 1;
					}
					else
					{
						if($IDLEboostSCORE == 3)
						{
							$diff = $MAP_Abs_PkLmt3_C - $MAPPeak;
							if($diff <= 2)
							{
								$IDLEboostSCORE = 2;
								$PeakBoostSCORE = 3;
							}
							else
							{
								$PeakBoostSCORE = 2;
							}
						}
						else
						{
							$PeakBoostSCORE = 2;
						}
					}
				}
				else
				{
					if($vType == 2)
					{
						$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C + $PressDiff;
					}
					else
					{
						$MAP_Dif_PkLmt3_C = $MAP_Dif_PkLmt3old_C;
					}
					
					if($MAPPeak >= $MAP_Dif_PkLmt3_C)
					{
						if($MAPPeak >= ($MAP_Dif_PkLmt3_C + $MAP_Dif_PkLmt_C))
						{
							$PeakBoostSCORE = 3;
							$msgPeak = "Your MAP values are a little higher than usual. We will keep you posted";
						}
						else
						{
							$PeakBoostSCORE = 3;
						}
					}
					elseif($MAPPeak <= ($MAP_Dif_PkLmt3_C - $MAP_Dif_PkLmt1_C))
					{
						$PeakBoostSCORE = 1;
					}
					else
					{
						if($IDLEboostSCORE == 3)
						{
							$diff = $MAP_Dif_PkLmt3_C - $MAPPeak;
							if($diff <= 2)
							{
								$IDLEboostSCORE = 2;
								$PeakBoostSCORE = 3;
							}
							else
							{
								$PeakBoostSCORE = 2;
							}
						}
						else
						{
							$PeakBoostSCORE = 2;
						}
					}
				}
				//}
				$flag = 1;		
				return array($IDLEboostSCORE,$msg,$PeakBoostSCORE,$msgPeak,'flag'=>$flag);
			}
			else
			{
				$flag = 0;	
				return array('','Test condition not reached','','Test condition not reached','flag'=>$flag);
			}
		}
		else
		{
			$flag = 1;	
			return array('','Not Applicable','','Not Applicable','flag'=>$flag);
		}
	}
	
	//Air System Health - Done
	function airSystemHealthM($reqFor,$deviceID,$dt,$row,$durTr,$baroPress) // is only for petrol check the option out
	{
		$flag = 0;	
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		$Turbo_Avail_C = $this -> getCaliPramVal('TC_Status_C',$deviceID);
		
		//echo "Air System : $Turbo_Avail_C  $vType \n";
		if($Turbo_Avail_C == "No" && $vType == 1)
		{
			//$MAP_Air_IdleLmt_C = $this -> getCaliPramVal('MAP_Air_IdleLmt_C',$deviceID);
			//$MAP_Air_IdleLmt3_C = $this -> getCaliPramVal('MAP_Air_IdleLmt3_C',$deviceID);
			//$MAP_Air_IdleLmt1_C = $this -> getCaliPramVal('MAP_Air_IdleLmt1_C',$deviceID);
			$MAP_Air_PkLmt_C = $this -> getCaliPramVal('MAP_Air_PkLmt_C',$deviceID);
			$MAP_Air_PkLmt3_C = $this -> getCaliPramVal('MAP_Air_PkLmt3_C',$deviceID);
			$MAP_Air_PkLmt1_C = $this -> getCaliPramVal('MAP_Air_PkLmt1_C',$deviceID);
			$MAP_Air_RPM_PkLmt_C = $this -> getCaliPramVal('MAP_Air_RPM_PkLmt_C',$deviceID);
			$MAP_Air_RPMOffset_PkLmt_C = $this -> getCaliPramVal('MAP_Air_RPMOffset_PkLmt_C',$deviceID);
			$RPM_Idle_Warm_C = $this -> getCaliPramVal('RPM_Idle_Warm_C',$deviceID);
			$Cool_Lmt_C = $this -> getCaliPramVal('Cool_Lmt_C',$deviceID);
			$Eload_Max_C = $this -> getCaliPramVal('Eload_Max_C',$deviceID);
			$MAP_Air_EloadOffset_PkLmt_C = $this -> getCaliPramVal('MAP_Air_EloadOffset_PkLmt_C',$deviceID);
			
			/*$fld = $datAct->valuefield('obdManifoldPressure');
			$rpm = $datAct->valuefield('obdRpm');
			$rpmValA = $RPM_Idle_Warm_C - $MAP_Air_RPMOffset_PkLmt_C;
			$rpmValB = $RPM_Idle_Warm_C + $MAP_Air_RPMOffset_PkLmt_C;
			$ctemp = $datAct->valuefield('obdCoolantTemperature');
			$ctempVal= $Cool_Lmt_C;
			$MAPIdle = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);*/
			
			$fld = $datAct->valuefield('obdManifoldPressure');
			$rpm = $datAct->valuefield('obdRpm');
			$rpmValA = $MAP_Air_RPM_PkLmt_C;
			$rpmValB = 6000; //set as max value which wnt exeed
			$eng = $datAct->valuefield('obdEngineLoad');
			$engVal = $Eload_Max_C - $MAP_Air_EloadOffset_PkLmt_C;
			$MAPPeak = $this -> getCalStageDataRange(3,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$eng[0],$engVal);
			
			$fld = $datAct->valuefield('obdBarometricPressure');
			$BaroPress  = $this -> getCalStageData('(0,1)',$fld[0],"max",$reqFor,$calStg,$deviceID,'asc','in');
			$BaroPressMin  = $this -> getCalStageData('(0,1)',$fld[0],"min",$reqFor,$calStg,$deviceID,'asc','in');
			
			$CurrPres = $baroPress;
			
			$Barodiff = $CurrPres - $BaroPress;
			if($Barodiff >= 3 || $Barodiff <= -3)
			{
				$Barovalid = "No";
			}
			else
			{
				$Barovalid = "Yes";
			}
			
			if(($BaroPressMin == 0 && $BaroPress == 0) || $Barovalid == "No")
			{
				$BaroPress = $CurrPres;
			}
			
			//==============idle map algo removed as some cars do not support MAP and we do not have all limits==================//
			//Step 3. Check Idle Boost State
			if($MAPPeak >= 0)
			{
				$IDLEboostSCORE = 3;
				$msgIdleAir = "";
				/*if($MAPIdle >= $MAP_Air_IdleLmt3_C)
				{
					if($MAPIdle >= ($MAP_Air_IdleLmt3_C + $MAP_Air_IdleLmt_C))
					{
						$IDLEboostSCORE = 3;
						$msgIdleAir= "Your MAP values are a little higher than usual. We will keep you posted";
					}
					else
					{
						$IDLEboostSCORE = 3;
					}
				}
				elseif($MAPIdle <= ($MAP_Air_IdleLmt3 - $MAP_Air_IdleLmt1_C))
				{
					$IDLEboostSCORE = 1;
				}
				else//if($MAPIdle < $MAP_Air_IdleLmt3)
				{
					$IDLEboostSCORE = 2;
				}
				*/

				//Step 4. Check Peak Boost State
				//$fld = $datAct->valuefield('obdEngineLoad');	 	
				//$eLoadPeak = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
				
				//echo "Air Sys Hel : BaroPress : $BaroPress |  BaroPressMin : $BaroPressMin | MAPIdle: $MAPIdle | MAPPeak: $MAPPeak\n";
				
				$PeakBoostSCORE = 0;
				if($MAPPeak >= ($BaroPress - $MAP_Air_PkLmt3_C))
				{
					if($MAPPeak >= ($BaroPress + $MAP_Air_PkLmt_C))
					{
						$PeakBoostSCORE = 3; 
						$msgPeakAir = "Your MAP values are a little higher than usual. We will keep you posted";
					}
					elseif($MAPPeak == ($BaroPress - $MAP_Air_PkLmt3_C)) 
					{
						$PeakBoostSCORE = 3;
						$IDLEboostSCORE = 2;
					}
					else
					{
						$PeakBoostSCORE = 3;
					}
				}
				elseif($MAPPeak < ($BaroPress - $MAP_Air_PkLmt1_C))
				{
					$PeakBoostSCORE = 1;
				}
				elseif($MAPPeak < ($BaroPress - $MAP_Air_PkLmt3_C))
				{
					$PeakBoostSCORE = 2;
				}
				//}	
				$flag = 1;
				return array($IDLEboostSCORE,$msgIdleAir,$PeakBoostSCORE,$msgPeakAir,'flag'=>$flag);
			}
			else
			{
				$flag = 0;
				return array('','Test condition not reached','','Test condition not reached','flag'=>$flag);
			}
		}
		else
		{
			$flag = 1;
			return array('','Not Applicable','','Not Applicable','flag'=>$flag);
		}
	}
	
	//Fuel System Health - Done
	function fuelSystemHealthM($reqFor,$deviceID,$dt,$row,$durTr) // is only for diesel check the option out
	{
		$flag = 0;
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		$Injection_Type_C = $this -> getCaliPramVal('Injection_Type_C',$devId);
		
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		if($vType == 2 || ($vType == 1 && $Injection_Type_C == "Direct"))
		{
			$fld = $datAct->valuefield('obdFuelCommonRailPressure');
			$FuelPeak  = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID);
			$FuelIdle  = $this -> getCalStageData(2,$fld[0],"min",$reqFor,$calStg,$deviceID);
			if($FuelPeak == 0 && $FuelIdle == 0)
			{
				$flag = 1;
				return array('PID Not Supported','','','flag'=>$flag);
				//$msgfuel = "PID Not Supported";
			}
			else
			{
				$CRP_Type_C = $this -> getCaliPramVal('CRP_Type_C',$deviceID);
				$Base_CRP_Lmt_C = $this -> getCaliPramVal('Base_CRP_Lmt_C',$deviceID);
				$CRP_MinLmtScore3_C = $this -> getCaliPramVal('CRP_MinLmt3_C',$deviceID);
				//$CRP_MinLmt3_C = $Base_CRP_Lmt_C + $CRP_MinLmtScore3_C;
				$CRP_MinLmt3_C = $Base_CRP_Lmt_C;
				$CRP_MinLmtScore1_C = $this -> getCaliPramVal('CRP_MinLmt1_C',$deviceID);
				//$CRP_MinLmt1_C = $Base_CRP_Lmt_C + $CRP_MinLmtScore1_C;
				$CRP_MinLmt1_C = $Base_CRP_Lmt_C - $CRP_MinLmtScore1_C;
				$CRP_MaxLmtScore3_C = $this -> getCaliPramVal('CRP_MaxLmt3_C',$deviceID);
				$CRP_MaxLmt3_C = $CRP_Type_C - $CRP_MaxLmtScore3_C;
				$CRP_MaxLmtScore1_C = $this -> getCaliPramVal('CRP_MaxLmt1_C',$deviceID);
				$CRP_MaxLmt1_C = $CRP_MaxLmt3_C - $CRP_MaxLmtScore1_C;

				$RPM_Idle_Warm_C = $this -> getCaliPramVal('RPM_Idle_Warm_C',$deviceID);
				$RPM_Idle_Ld1_C = $this -> getCaliPramVal('RPM_Idle_Ld1_C',$deviceID);
				$RPM_Idle_Ld2_C = $this -> getCaliPramVal('RPM_Idle_Ld2_C',$deviceID);
				$Cool_Lmt_C = $this -> getCaliPramVal('Cool_Lmt_C',$deviceID);
				$Eload_Max_C = $this -> getCaliPramVal('Eload_Max_C',$deviceID);
				$CRP_RPM_PkLmt_C = $this -> getCaliPramVal('CRP_RPM_PkLmt_C',$deviceID);
				$CRP_RPMOffset_PkLmt_C = $this -> getCaliPramVal('CRP_RPMOffset_PkLmt_C',$deviceID);
				$CRP_ELoadOffset_PkLmt_C = $this -> getCaliPramVal('CRP_ELoadOffset_PkLmt_C',$deviceID);
				$Eload_Def_C = $this -> getCaliPramVal('Eload_Def_C',$deviceID);
				
				if($Eload_Max_C == 0)
				{
					$Eload_Max_C = 100;
				}
				else
				{
					//do nothing;
				}
				
				$fld = $datAct->valuefield('obdFuelCommonRailPressure');
				$rpm = $datAct->valuefield('obdRpm');
				$rpmValA = $RPM_Idle_Warm_C - $CRP_RPMOffset_PkLmt_C;
				$rpmValB = $RPM_Idle_Warm_C + $CRP_RPMOffset_PkLmt_C;
				$ctemp = $datAct->valuefield('obdCoolantTemperature');
				$ctempVal= $Cool_Lmt_C;
				$eload = $datAct->valuefield('obdEngineLoad');
				$eloadVal = $Eload_Def_C + 1;
				$FuelIdle = $this -> getCalStageDataRange(2,$fld[0],"min",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$eload[0],$eloadVal,'asc','>');
	
				$fld = $datAct->valuefield('obdFuelCommonRailPressure');
				$rpm = $datAct->valuefield('obdRpm');
				$rpmValA = $CRP_RPM_PkLmt_C;
				$rpmValB = 7000; //set as max value which wnt exeed
				$eng = $datAct->valuefield('obdEngineLoad');
				$engVal = $Eload_Max_C - $CRP_ELoadOffset_PkLmt_C;
				$FuelPeak = $this -> getCalStageDataRange(3,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$eng[0],$engVal);
				
				//echo "Monit fuelSystemHealth : $rpmValA | $engVal = $Eload_Max_C - $CRP_ELoadOffset_PkLmt_C | FuelPeak : $FuelPeak | FuelIdle : $FuelIdle \n";	
				if($FuelIdle > 0)
				{
					$IDLECRPSCORE = 0;
					$msgfuel = "";
				
					//Check Idle Fuel System Health
					if($FuelIdle > $CRP_MinLmt3_C)
					{
						$IDLECRPSCORE = 3;
					}
					elseif($FuelIdle < $CRP_MinLmt1_C)
					{
						$IDLECRPSCORE = 1;
					}
					else
					{
						$IDLECRPSCORE = 2;
					}
					$flag1 = 0.5;
				}
				else
				{
					$fld = $datAct->valuefield('obdFuelCommonRailPressure');		
					$rpm = $datAct->valuefield('obdRpm');
					$rpmValA = $RPM_Idle_Ld1_C - $CRP_RPMOffset_PkLmt_C;
					$rpmValB = $RPM_Idle_Ld1_C + $CRP_RPMOffset_PkLmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $Cool_Lmt_C;
					$eload = $datAct->valuefield('obdEngineLoad');
					$eloadVal = $Eload_Def_C + 1;
					$FuelIdleLd1 = $this -> getCalStageDataRange(2,$fld[0],"min",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$eload[0],$eloadVal,'asc','>');
					
					if($FuelIdleLd1 > 0)
					{
						$FuelIdle = $FuelIdleLd1;
					}
					else
					{
						$fld = $datAct->valuefield('obdFuelCommonRailPressure');		
						$rpm = $datAct->valuefield('obdRpm');
						$rpmValA = $RPM_Idle_Ld2_C - $CRP_RPMOffset_PkLmt_C;
						$rpmValB = $RPM_Idle_Ld2_C + $CRP_RPMOffset_PkLmt_C;
						$ctemp = $datAct->valuefield('obdCoolantTemperature');
						$ctempVal= $Cool_Lmt_C;
						$eload = $datAct->valuefield('obdEngineLoad');
						$eloadVal = $Eload_Def_C + 1;
						$FuelIdleLd2 = $this -> getCalStageDataRange(2,$fld[0],"min",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$eload[0],$eloadVal,'asc','>');
						
						if($FuelIdleLd2 > 0)
						{
							$FuelIdle = $FuelIdleLd2;
						}
						else
						{
							$FuelIdle = 30000;
						}
					}
					
					$IDLECRPSCORE = 0;
					$msgfuel = "";
				
					//Check Idle Fuel System Health
					if($FuelIdle > $CRP_MinLmt3_C)
					{
						$IDLECRPSCORE = 3;
					}
					elseif($FuelIdle < $CRP_MinLmt1_C)
					{
						$IDLECRPSCORE = 1;
					}
					else
					{
						$IDLECRPSCORE = 2;
					}
					$flag1 = 0.5;	
					//return array('Test condition not reached','','','flag'=>$flag);	
				}

				if($FuelPeak > 0)
				{
					$PeakCRPSCORE = 0;
					$msgfuel = "";
					
					//Check Peak Fuel System Health
					if($FuelPeak > $CRP_MaxLmt3_C)
					{
						$PeakCRPSCORE = 3;
					}
					elseif($FuelPeak < $CRP_MaxLmt1_C)
					{
						$PeakCRPSCORE = 1;
					}
					else
					{
						$PeakCRPSCORE = 2;
					}
					$flag2 = 0.5;
					$flag = $flag1 + $flag2;
					return array($msgfuel,$IDLECRPSCORE,$PeakCRPSCORE,'flag'=>$flag);
				}
				else
				{
					$flag = 0;
					return array('Test condition not reached','','','flag'=>$flag);
				}
			}
		}
		else
		{
			$flag = 1;
			return array('Not Applicable','','','flag'=>$flag);
		}
	}
	
	//Combustion Pressure - Done
	function combustionPressureM($reqFor,$deviceID,$dt,$row,$durTr,$baroPress,$drivcond)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
	
		$MaxTorque = $this -> getCaliPramVal('Max_Torque_C',$deviceID);
		$Trq_Lmt1_C = $this -> getCaliPramVal('Trq_Lmt1_C',$deviceID);
		$Trq_Lmt2_C = $this -> getCaliPramVal('Trq_Lmt2_C',$deviceID);
		$Trq_Lmt3_C = $this -> getCaliPramVal('Trq_Lmt3_C',$deviceID);
		$Trq_Lmt4_C = $this -> getCaliPramVal('Trq_Lmt4_C',$deviceID);
		$Eload_Max_C = $this -> getCaliPramVal('Eload_Max_C',$deviceID);
		$Trq_RPM_PkLmt_C = $this -> getCaliPramVal('Trq_RPM_PkLmt_C',$deviceID);
		$Trq_EloadOffset_PkLmt_C = $this -> getCaliPramVal('Trq_EloadOffset_PkLmt_C',$deviceID);
		$Trq_Cool_PkLmt_C = $this -> getCaliPramVal('Trq_Cool_PkLmt_C',$deviceID);
		$RPM_PkLmt_Diesel_C = $this -> getCaliPramVal('Eff_RPM_Limit2_C',$deviceID);
		$RPM_PkLmt_Petrol_C = $this -> getCaliPramVal('Eff_RPM_Limit3_C',$deviceID);
		
		if($Eload_Max_C == 0)
		{
			$Eload_Max_C = 100;
		}
		else
		{
			//do nothing;
		}
		
		$fld = $datAct->valuefield('obdBarometricPressure');
		$BaroPress  = $this -> getCalStageData('(0,1)',$fld[0],"max",$reqFor,$calStg,$deviceID,'asc','in');
		$BaroPressMin  = $this -> getCalStageData('(0,1)',$fld[0],"min",$reqFor,$calStg,$deviceID,'asc','in');
		
		//$fld = $datAct->valuefield('obdEngineLoad');
		//$eLoadPeak = $this -> getCalStageData(8,$fld[0],"max",$reqFor,$calStg,$deviceID);
		
		if($vType == 1)
		{
			$Trq_RPM_PkLmt_C = $RPM_PkLmt_Petrol_C; //Max torque is observed above this RPM value
			$MaxTorquecheck = ($MaxTorque * 0.9);
			// $fld = $datAct->valuefield('Ftorque');
			$rpm = $datAct->valuefield('obdRpm');
			$rpmValA = $Trq_RPM_PkLmt_C;
			$rpmValB = 5000;
			$ctemp = $datAct->valuefield('obdEngineLoad');
			$ctempVal= $Eload_Max_C - $Trq_EloadOffset_PkLmt_C;
			//$eng = $datAct->valuefield('Ftorque');
			$engVal = $MaxTorquecheck;
			$TorqueMax3 = $this -> getCalStageDataRangeBaseM(3,'Ftorcomp',"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,'Ftorcomp',$engVal);
			/*$fld = $datAct->valuefield('Ftorque');		
			$rpm = $datAct->valuefield('obdRpm');
			$rpmValA = $Trq_RPM_PkLmt_C;
			$rpmValB = 6000; //set as max value which wnt exceed
			$eng = $datAct->valuefield('obdEngineLoad');
			$engVal = $Eload_Max_C - $Trq_EloadOffset_PkLmt_C;
			$col = $datAct->valuefield('obdCoolantTemperature');
			$colVal = $Trq_Cool_PkLmt_C;
			$TorqueMax3 = $this -> getCalStageDataTorqueMAX(3,'Ftorque',$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$eng[0],$engVal,$col[0],$colVal);*/
		}
		else
		{
			$Trq_RPM_PkLmt_C = $RPM_PkLmt_Diesel_C; //Max torque is observed above this RPM value
			// $fld = $datAct->valuefield('Ftorque');
			$rpm = $datAct->valuefield('obdRpm');
			$rpmValA = $Trq_RPM_PkLmt_C;
			$rpmValB = 4000; //set as max value which will not exceed
			$eng = $datAct->valuefield('obdEngineLoad');
			$engVal = $Eload_Max_C - $Trq_EloadOffset_PkLmt_C;
			$col = $datAct->valuefield('obdCoolantTemperature');
			$colVal = $Trq_Cool_PkLmt_C;
			$TorqueMax3 = $this -> getCalStageDataRange(3,'Ftorcomp',"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$eng[0],$engVal,$col[0],$colVal);
		}
		
		//Getting average engine load in above max torque condition
		$fld = $datAct->valuefield('obdEngineLoad');
		$rpm = $datAct->valuefield('obdRpm');
		$rpmValA = $Trq_RPM_PkLmt_C;
		$rpmValB = 5000; //set as max value which wnt exceed
		$eng = $datAct->valuefield('obdEngineLoad');
		$engVal = $Eload_Max_C - $Trq_EloadOffset_PkLmt_C;
		$col = $datAct->valuefield('obdCoolantTemperature');
		$colVal = $Trq_Cool_PkLmt_C;
		$AvgEload = $this -> getCalStageDataRange(3,$fld[0],"avg",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$eng[0],$engVal,$col[0],$colVal);
		
		if($AvgEload == 0)
		{
			$AvgEload = 1;
		}
		else
		{
			$AvgEload = $AvgEload / $Eload_Max_C;
		}
		
		//Get Environmental Pressure condition
		//$sql = "select * from device_monitor where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
		//$dat = $this -> select($sql);
		//$CurrPres = $dat['press'];
		$CurrPres = $baroPress;
		//$weatherArray = $this -> getCurrWeatherM($reqFor,$deviceID,$dt,$row,$durTr);
		//$CurrPres = $weatherArray[2];
		
		$CombPresState = 0;
		$msgTuning = "";
		if($TorqueMax3 > 0)
		{
			//if($eLoadPeak > 90)
			//{
			$Barodiff = $CurrPres - $BaroPress;
			if($Barodiff >= 3 || $Barodiff <= -3)
			{
				$Barovalid = "No";
			}
			else
			{
				$Barovalid = "Yes";
			}
			
			if(($BaroPressMin == 0 && $BaroPress == 0) || $Barovalid == "No")
			{
				$BaroPress = $CurrPres;
			}
			
			//Get drivability condition
			//$Drivability = $this -> getDrivabilityConditionM($reqFor,$deviceID,$dt,$row,$durTr);
			$Drivability =  $drivcond;
			
			$LoadFact1 = 0.95;	//this is a 5% factor for manufacturing variations.
			$LoadFact2 = 0.95;	//this is a 5% factor for accessory loads.
			
			//Drivability condition correction factor
			if($Drivability == "Good")
			{
				$LoadFact3 = 1;
			}
			elseif($Drivability == "Intermediate")
			{
				$LoadFact3 = 0.98;
			}
			elseif($Drivability == "Bad")
			{
				$LoadFact3 = 0.96;
			}
			
			//Pressure correction factor
			//turbocharged petrols do not require the pressure correction factor as seen in the TSI in bangalore and mumbai (max MAP doesn't change)
			if($vType == 1 && $Turbo_Avail_C == "Yes")
			{
				$PresFact = 1; //no correction required
			}
			else
			{
				$PresFact = $BaroPress / 100;
			}
			
			//Engine Load Factor
			$AvgEloadFact = $AvgEload; // Engine Load correction factor
			
			//RPM Factor
			$sprint = $reqFor;
			$sql = "select F4 from device_data where device_id='$deviceID' and Fsprint='$sprint' and (Ftorcomp*1) ='$TorqueMax3' order by time_s asc limit 0,1";
			$ts = $this -> select($sql);
			$Torquemax3RPM = $ts[0]['F4'];

			$RPMDiffPer = 0;
			if($vType == 1)
			{
				$RPM_Limit3 = $this -> getCaliPramVal('Eff_RPM_Limit3_C',$deviceID);
				$RPM_Limit4 = $this -> getCaliPramVal('Eff_RPM_Limit4_C',$deviceID);
				
				if(($Torquemax3RPM > $RPM_Limit3) && ($Torquemax3RPM < $RPM_Limit4))
				{
					//do nothing;
				}
				else
				{
					if($Torquemax3RPM > $RPM_Limit4)
					{
						$RPMDiff = $Torquemax3RPM - $RPM_Limit4;
						$RPMDiffPer = ($RPMDiff * 100) / $RPM_Limit4;
						$RPMDiffPer = round($RPMDiffPer,0);
					}
					else
					{
						//If the max torque is recieved before RPM limit 3.
						//do nothing;
					}
				}
			}
			else
			{
				$RPM_Limit2 = $this -> getCaliPramVal('Eff_RPM_Limit2_C',$deviceID);
				$RPM_Limit3 = $this -> getCaliPramVal('Eff_RPM_Limit3_C',$deviceID);
				
				if(($Torquemax3RPM > $RPM_Limit2) && ($Torquemax3RPM < $RPM_Limit3))
				{
					//do nothing;
				}
				else
				{
					if($Torquemax3RPM > $RPM_Limit3)
					{
						$RPMDiff = $Torquemax3RPM - $RPM_Limit3;
						$RPMDiffPer = ($RPMDiff * 100) / $RPM_Limit3;
						$RPMDiffPer = round($RPMDiffPer,0);
					}
					else
					{
						//If the max torque is recieved before RPM limit 2.
						//do nothing;
					}
				}
			}
			if($RPMDiffPer == 0)
			{
				$RPMFactor = 1;
			}
			elseif($RPMDiffPer <= 3)
			{
				$RPMFactor = 0.97;
			}
			elseif($RPMDiffPer <= 5)
			{
				$RPMFactor = 0.964;
			}
			elseif($RPMDiffPer <= 15)
			{
				$RPMFactor = 0.94;
			}
			elseif($RPMDiffPer <= 27)
			{
				$RPMFactor = 0.91;
			}
			elseif($RPMDiffPer <= 34)
			{
				$RPMFactor = 0.875;
			}
			elseif($RPMDiffPer <= 43.5)
			{
				$RPMFactor = 0.81;
			}
			elseif($RPMDiffPer <= 50)
			{
				$RPMFactor = 0.76;
			}
			elseif($RPMDiffPer <= 60)
			{
				$RPMFactor = 0.72;
			}
			elseif($RPMDiffPer <= 80)
			{
				$RPMFactor = 0.55;
			}
			else
			{
				$RPMFactor = 0.50;
			}
			
			$TotalFact = $LoadFact1 * $LoadFact2 * $LoadFact3 * $PresFact * $AvgEloadFact * $RPMFactor;
			
			//echo "\n CombustionDrivability= $Drivability | LoadFact3 = $LoadFact3 | BaroPress = $BaroPress \n";
			
			$NewMaxTorque = $MaxTorque * $TotalFact;
			$TorquePer = ($NewMaxTorque != 0 ) ? ($TorqueMax3 / $NewMaxTorque) * 100 : 0;
			
			//echo "Combustion Pressure : rpmValA : $rpmValA TorqueMax3 : $TorqueMax3 engVal : $engVal MaxTorque : $MaxTorque BaroPress : $BaroPress : BaroPressMin | $BaroPressMin | TorquePer : $TorquePer | MaxTorque : $TorqueMax3\n";
			
			if($TorquePer < $Trq_Lmt1_C)
			{
				if($TorquePer == 0)
				{
					//do nothing
				}
				else
				{
					$CombPresState = 1;
				}
			}
			elseif($TorquePer <= $Trq_Lmt2_C)
			{
				$CombPresState = 2;
			}
			elseif($TorquePer <= $Trq_Lmt3_C)
			{
				$CombPresState = 3;
			}
			elseif($TorquePer <= $Trq_Lmt4_C)
			{
				$CombPresState = 4;
			}
			elseif($TorquePer > $Trq_Lmt4_C)
			{
				$CombPresState = 5;
			}
			
			//Tuning Detection
			$TuningFact = $LoadFact2 * $PresFact;
			$TuningTorque = $MaxTorque * $TuningFact;
			if($TorqueMax3 > $TuningTorque)
			{
				if($TuningTorque == 0 || $TorqueMax3 == 0)
				{
					//do nothing
				}
				else
				{
					$msgTuning = "Your Torque values are a little higher than usual. We will keep you posted";
				}
			}
			else
			{
				//do nothing
			}

			return array ($CombPresState,$msgTuning,$TorqueMax3,$NewMaxTorque);
		}
		else 
		{
			return array ('','Test condition not reached',$TorqueMax3,$NewMaxTorque);
		}
	}
	
	function combustionPressureValueM($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		$RPM_Idle_Warm_C = $this -> getCaliPramVal('RPM_Idle_Warm_C',$deviceID);
		$ELoad_Idle_Warm_C = $this -> getCaliPramVal('ELoad_Idle_Warm_C',$deviceID);
		$Trq_EloadOffset_PkLmt_C = $this -> getCaliPramVal('TrqRating_EloadOffset_PkLmt_C',$deviceID);
		$Trq_RPMOffset_PkLmt_C = $this -> getCaliPramVal('TrqRating_RPMOffset_PkLmt_C',$deviceID);
		
		$fld = $datAct->valuefield('obdEngineLoad');
		$rpm = $datAct->valuefield('obdRpm');
		$rpmValA = $RPM_Idle_Warm_C - $Trq_RPMOffset_PkLmt_C;
		$rpmValB = $RPM_Idle_Warm_C + $Trq_RPMOffset_PkLmt_C;
		$ctemp = $datAct->valuefield('obdEngineLoad');
		$ctempVal= $ELoad_Idle_Warm_C + $Trq_EloadOffset_PkLmt_C;
		$EloadMax = $this -> getCalStageDataRPMRangeMAX(2,$fld[0],$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
		return $EloadMax;
	}
			
	//RPM Oscillation anomaly analysis - Done
	function rpmOscillationAnomalyAnalysisM($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		$Osc_RPM_Lmt1_C = $this -> getCaliPramVal('Osc_RPM_Lmt1_C',$deviceID);
		$Osc_RPM_Lmt2_C = $this -> getCaliPramVal('Osc_RPM_Lmt2_C',$deviceID);
		$Osc_RPM_Lmt3_C = $this -> getCaliPramVal('Osc_RPM_Lmt3_C',$deviceID);
		$Osc_RPM_Lmt4_C = $this -> getCaliPramVal('Osc_RPM_Lmt4_C',$deviceID);
		$Osc_EloadOffset_Lmt_C = $this -> getCaliPramVal('Osc_EloadOffset_Lmt_C',$deviceID);
		$Cool_Lmt_C = $this -> getCaliPramVal('Cool_Lmt_C',$deviceID);
		$Eload_Idle_Warm_C = $this -> getCaliPramVal('ELoad_Idle_Warm_C',$deviceID);
		$Eload_Idle_Ld1_C = $this -> getCaliPramVal('ELoad_Idle_Ld1_C',$deviceID);
		$Eload_Idle_Ld2_C = $this -> getCaliPramVal('ELoad_Idle_Ld2_C',$deviceID);
		
		$fld = $datAct->valuefield('obdRpm');
		$eload = $datAct->valuefield('obdEngineLoad');
		$eloadValA = $Eload_Idle_Warm_C - $Osc_EloadOffset_Lmt_C;
		$eloadValB = $Eload_Idle_Warm_C + $Osc_EloadOffset_Lmt_C;
		$ctemp = $datAct->valuefield('obdCoolantTemperature');
		$ctempVal= $Cool_Lmt_C;
		$MaxobdRpm = $this -> getCalStageDataRPMRangeMAX(2,$fld[0],$reqFor,$calStg,$deviceID,$eload[0],$eloadValA,$eloadValB,$ctemp[0],$ctempVal);
		$MinobdRpm = $this -> getCalStageDataRPMRangeMIN(2,$fld[0],$reqFor,$calStg,$deviceID,$eload[0],$eloadValA,$eloadValB,$ctemp[0],$ctempVal);
		
		//echo " RPM Oscillation : Eload_Idle_Warm_C : $Eload_Idle_Warm_C | eloadValA : $eloadValA  eloadValB : $eloadValB ctempVal: $ctempVal MaxobdRpm : $MaxobdRpm  MinobdRpm : $MinobdRpm\n";
		if($MaxobdRpm >= 0 && $MinobdRpm >= 0)
		{
			$DeltaRPM = $MaxobdRpm - $MinobdRpm;
			$EngineBlockInjectorState = 0;
			if($vType == 1)
			{
				if($DeltaRPM >= $Osc_RPM_Lmt1_C)
				{
					$EngineBlockInjectorState = 1;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt2_C)
				{
					$EngineBlockInjectorState = 2;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt3_C)
				{
					$EngineBlockInjectorState = 3;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 4;
				}
				elseif($DeltaRPM <= $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 5;
				}
			}
			elseif($vType == 2)
			{
				if($DeltaRPM > $Osc_RPM_Lmt1_C)
				{
					$EngineBlockInjectorState = 1;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt2_C)
				{
					$EngineBlockInjectorState = 2;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt3_C)
				{
					$EngineBlockInjectorState = 3;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 4;
				}
				elseif($DeltaRPM <= $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 5;
				}
			}
			//echo " RPM Oscillation  : MaxobdRpm : $MaxobdRpm | MinobdRpm : $MinobdRpm | DeltaRPM : $DeltaRPM | EngineBlockInjectorState : $EngineBlockInjectorState\n";
			return $EngineBlockInjectorState;
		}
		else
		{
			$fld = $datAct->valuefield('obdRpm');
			$eload = $datAct->valuefield('obdEngineLoad');
			$eloadValA = $Eload_Idle_Ld1_C - $Osc_EloadOffset_Lmt_C;
			$eloadValB = $Eload_Idle_Ld1_C + $Osc_EloadOffset_Lmt_C;
			$ctemp = $datAct->valuefield('obdCoolantTemperature');
			$ctempVal= $Cool_Lmt_C;
			$MaxobdRpmLd1 = $this -> getCalStageDataRPMRangeMAX(2,$fld[0],$reqFor,$calStg,$deviceID,$eload[0],$eloadValA,$eloadValB,$ctemp[0],$ctempVal);
			$MinobdRpmLd1 = $this -> getCalStageDataRPMRangeMIN(2,$fld[0],$reqFor,$calStg,$deviceID,$eload[0],$eloadValA,$eloadValB,$ctemp[0],$ctempVal);
			
			if($MaxobdRpmLd1 >= 0 && $MinobdRpmLd1 >= 0)
			{
					$DeltaRPM = $MaxobdRpmLd1 - $MinobdRpmLd1;
			}
			else
			{
				$fld = $datAct->valuefield('obdRpm');
				$eload = $datAct->valuefield('obdEngineLoad');
				$eloadValA = $Eload_Idle_Ld2_C - $Osc_EloadOffset_Lmt_C;
				$eloadValB = $Eload_Idle_Ld2_C + $Osc_EloadOffset_Lmt_C;
				$ctemp = $datAct->valuefield('obdCoolantTemperature');
				$ctempVal= $Cool_Lmt_C;
				$MaxobdRpmLd2 = $this -> getCalStageDataRPMRangeMAX(2,$fld[0],$reqFor,$calStg,$deviceID,$eload[0],$eloadValA,$eloadValB,$ctemp[0],$ctempVal);
				$MinobdRpmLd2 = $this -> getCalStageDataRPMRangeMIN(2,$fld[0],$reqFor,$calStg,$deviceID,$eload[0],$eloadValA,$eloadValB,$ctemp[0],$ctempVal);

				if($MaxobdRpmLd2 >= 0 && $MinobdRpmLd2 >= 0)
				{
					$DeltaRPM = $MaxobdRpmLd2 - $MinobdRpmLd2;
				}
				else
				{
					$DeltaRPM = 0;
				}
			}
				
			$EngineBlockInjectorState = 0;
			if($vType == 1)
			{
				if($DeltaRPM >= $Osc_RPM_Lmt1_C)
				{
					$EngineBlockInjectorState = 1;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt2_C)
				{
					$EngineBlockInjectorState = 2;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt3_C)
				{
					$EngineBlockInjectorState = 3;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 4;
				}
				elseif($DeltaRPM <= $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 5;
				}
			}
			elseif($vType == 2)
			{
				if($DeltaRPM > $Osc_RPM_Lmt1_C)
				{
					$EngineBlockInjectorState = 1;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt2_C)
				{
					$EngineBlockInjectorState = 2;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt3_C)
				{
					$EngineBlockInjectorState = 3;
				}
				elseif($DeltaRPM > $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 4;
				}
				elseif($DeltaRPM <= $Osc_RPM_Lmt4_C)
				{
					$EngineBlockInjectorState = 5;
				}
			}
			//echo " RPM Oscillation  : MaxobdRpm : $MaxobdRpm | MinobdRpm : $MinobdRpm | DeltaRPM : $DeltaRPM | EngineBlockInjectorState : $EngineBlockInjectorState\n";
		
		return $EngineBlockInjectorState;
		//return "Test condition not reached";
		}
	}
	
	//Back-Pressure build-up increase detection - Done
	function backPressureBuildupM($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
			
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		
		$flag = 0;
		if($vType == 2)
		{
			$EGR_Cmd_Lmt1_C = $this -> getCaliPramVal('EGR_Cmd_Lmt1_C',$deviceID);
			$EGR_Cmd_Lmt2_C = $this -> getCaliPramVal('EGR_Cmd_Lmt2_C',$deviceID);
			$EGR_Cmd_Lmt4_C = $this -> getCaliPramVal('EGR_Cmd_Lmt4_C',$deviceID);
			$EGR_Cmd_WrmIdle_C = $this -> getCaliPramVal('EGR_Cmd_WrmIdle_C',$deviceID);
			$Cool_Lmt_C = $this -> getCaliPramVal('Cool_Lmt_C',$deviceID);
			$RPM_Idle_Warm_C = $this -> getCaliPramVal('RPM_Idle_Warm_C',$deviceID);
			$EGR_Cmd_RPMOffset_Lmt_C = $this -> getCaliPramVal('EGR_Cmd_RPMOffset_Lmt_C',$deviceID);
			
			$fld = $datAct->valuefield('obdCommandedEGR');
			$rpm = $datAct->valuefield('obdRpm');
			$rpmValA = $RPM_Idle_Warm_C - $EGR_Cmd_RPMOffset_Lmt_C;
			$rpmValB = $RPM_Idle_Warm_C + $EGR_Cmd_RPMOffset_Lmt_C;
			$ctempVal= $Cool_Lmt_C;
			$ctemp = $datAct->valuefield('obdCoolantTemperature');
			$EGRCMDMax = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
			
			$EGRCMDLast = $EGRCMDMax;
			if($EGRCMDLast >= 0)
			{
				$EGRCMDScore = 0;		
				if($EGRCMDLast >= ($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt1_C))
				{
					$EGRCMDScore = 1;
				}
				elseif((($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt2_C) <= $EGRCMDLast) && ($EGRCMDLast < ($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt1_C)))
				{
					$EGRCMDScore = 2;
				}
				elseif((($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt4_C) <= $EGRCMDLast) && ($EGRCMDLast < ($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt2_C)))
				{
					$EGRCMDScore = 4;
				}
				//elseif(($EGR_Cmd_WrmIdle_C <= $EGRCMDLast) && ($EGRCMDLast < ($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt4_C)))
				elseif($EGRCMDLast < ($EGR_Cmd_WrmIdle_C + $EGR_Cmd_Lmt4_C))
				{
					$EGRCMDScore = 5;
				}
				//$this -> setCaliPramVal('EGR_Cmd_WrmIdle_C',$deviceID,$EGR_Cmd_WrmIdle_C);
				//echo "Back-Pressure : EGRCMDMax : $EGRCMDMax EGR_Cmd_WrmIdle_C : $EGR_Cmd_WrmIdle_C EGRCMDScore : $EGRCMDScore
				$flag = 1;	
				return array($EGRCMDScore ,'flag'=>$flag);
			}
			else 
			{
				$flag = 0;	
				return array("Test condition not reached",'flag'=>$flag);
			}
		}
		else
		{
			//$this -> setCaliPramVal('NA',$deviceID,$EGR_Cmd_WrmIdle_C);	
			$flag = 1;
			return array('Not Applicable','flag'=>$flag);
		}
	}
	
	//Coolant Health
	function coolantSysHealthM($reqFor,$deviceID,$dt,$row,$durTr,$temp)
	{
		$flag = 0;
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		//Logic added to ignore the coollant temperature rising during idling
		$Cool_Lmt_C = $this -> getCaliPramVal('Cool_Lmt_C',$deviceID);
		
		$fld = $datAct->valuefield('obdCoolantTemperature');
		$speed = $datAct->valuefield('obdSpeed');
		$speedValA = 10;
		$speedValB = 500;
		$ctemp = $datAct->valuefield('obdCoolantTemperature');
		$ctempVal= $Cool_Lmt_C;
		$maxCoolTemp = $this -> getCalStageDataRange(3,$fld[0],"max",$reqFor,$calStg,$deviceID,$speed[0],$speedValA,$speedValB,$ctemp[0],$ctempVal);
		
		//Find maximum coolant temperature during test state 3 irrespective of vehicle idle or running.
		//$fld = $datAct->valuefield('obdCoolantTemperature');
		//$maxCoolTemp = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID);
		
		$sprint = $reqFor;
		$fld = $fld[0];
		$sql = "select time_s from device_data where device_id='$deviceID' and Fsprint='$sprint' and  (time_s*1) <> 0 and ($fld*1) ='$maxCoolTemp' and ($calStg*1)='3' order by time_s asc limit 0,1";
		$ts = $this -> select($sql);
		$ts = $ts[0]['time_s'];
		//echo "AAAA $sql \n";
		
		$sql = "select time_s from device_data where device_id='$deviceID' and Fsprint='$sprint' and  (time_s*1) <> 0 and time_s > '$ts' order by time_s asc limit 15,1";
		$tsN = $this -> select($sql);
		$tsN = $tsN[0]['time_s'];
		//echo "BBBB $sql \n";
		$sql = "select min($fld) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  (time_s*1) <> 0 and time_s >= '$ts' and time_s <= '$tsN' ";
		$minCoolTemp = $this -> select($sql);
		$minCoolTemp = ($minCoolTemp[0]['val'] > 0) ? $minCoolTemp[0]['val'] : $maxCoolTemp;
		
		$Base_OvrHt_Cool_Lmt_C = $this -> getCaliPramVal('Base_OvrHt_Cool_Lmt_C',$deviceID);
		$Cool_Temp_Lmt4_C = $this -> getCaliPramVal('Cool_Temp_Lmt4_C',$deviceID);
		
		$Diff = $Base_OvrHt_Cool_Lmt_C - $Cool_Temp_Lmt4_C;
		
		$Cool_Temp_Lmt1percent_C = $this -> getCaliPramVal('Cool_Temp_Lmt1_C',$deviceID);
		$Cool_Temp_Lmt1fact = ($Diff * $Cool_Temp_Lmt1percent_C) / 100;
		$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt1fact;
		
		$Cool_Temp_Lmt2percent_C = $this -> getCaliPramVal('Cool_Temp_Lmt2_C',$deviceID);
		$Cool_Temp_Lmt2fact = ($Diff * $Cool_Temp_Lmt2percent_C) / 100;
		$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt2fact;
		
		$Cool_Temp_Lmt3percent_C = $this -> getCaliPramVal('Cool_Temp_Lmt3_C',$deviceID);
		$Cool_Temp_Lmt3fact = ($Diff * $Cool_Temp_Lmt3percent_C) / 100;
		$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt4_C + $Cool_Temp_Lmt3fact;
		
		$Cool_Lmt_C = $this -> getCaliPramVal('Cool_Lmt_C',$deviceID);
		
		//Intake air/ Atm temperature based correction
		$CurTemp = $temp;
		if($CurTemp == "" || $CurTemp == 0)
		{
			$CurTemp = 25;
		}
		else
		{
			$CurTemp = $CurTemp;
		}
		
		if($CurTemp < 30)
		{
			$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C;
			$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C;
			$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C;
			$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C;
		}
		elseif($CurTemp <= 40)
		{
			$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C + 2;
			$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C + 2;
			$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C + 2;
			$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C + 2;
		}
		else
		{
			$Cool_Temp_Lmt4_C = $Cool_Temp_Lmt4_C + 4;
			$Cool_Temp_Lmt3_C = $Cool_Temp_Lmt3_C + 4;
			$Cool_Temp_Lmt2_C = $Cool_Temp_Lmt2_C + 4;
			$Cool_Temp_Lmt1_C = $Cool_Temp_Lmt1_C + 4;
		}
		
		$coolHelScore = '';
		$msg = "";
		if($minCoolTemp > $Cool_Lmt_C)
		{
			if($minCoolTemp > $Cool_Temp_Lmt1_C)
			{
				$coolHelScore = 1;
			}
			elseif($minCoolTemp >= $Cool_Temp_Lmt2_C)
			{
				$coolHelScore = 2;
			}
			elseif($minCoolTemp >= $Cool_Temp_Lmt3_C)
			{
				$coolHelScore = 3;
			}
			elseif($minCoolTemp > $Cool_Temp_Lmt4_C)
			{
				$coolHelScore = 4;
			}
			else
			{
				$coolHelScore = 5;
			}
			$flag = 1;
		}
		else
		{
			$flag = 0;	
			$msg = "Test condition not reached";
		}
		return array($coolHelScore,$msg,'flag'=>$flag);
	}
	
	//False Odometer reading detection - Done
	function falseOdometerReadingM($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		$fld = $datAct->valuefield('obdDistanceSinceErrorCleared');
		$MaxDistErrClrd = $this -> getCalStageData(4,$fld[0],"max",$reqFor,$calStg,$deviceID);
		
		$msgodo = "";
		
		$disT = $this -> getDistSince($deviceID);
		$distTrvAll = ($disT[0]+$disT[1]); //get from chart Distance Travelled Since Purchase
		
		if($MaxDistErrClrd <= 65535) //65535 is max value of this PID. values above these are considered rubbish values.
		{
			if($MaxDistErrClrd <= $distTrvAll)
			{
				$msgodo = "Odometer reading Okay";
			}
			else
			{
				$msgodo = "Odometer tampered";
			}
		}
		else
		{
			$msgodo = "Odometer reading Okay";
		}
		//echo "False Odometer : MaxDistErrClrd <= distTrvAll $MaxDistErrClrd <= $distTrvAll \n";
		return $msgodo;
	}
	
	//Speed Sensor failure - Done
	function getVehSpeedSenceM($reqFor,$deviceID,$ts="")
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		
		$fld = $datAct->valuefield('obdSpeed');
		$maxVehSpeed = $this -> getCalStageData(3,$fld[0],"max",$reqFor,$calStg,$deviceID);
		
		if($maxVehSpeed != 0)
		{
			return 1;
		}
		else
		{
			if($vType == 1)
			{
				$VehSpeed_RPMLmt1_C = $this -> getCaliPramVal('VehSpeed_RPMLmt1_C',$deviceID);
				$VehSpeed_ThOffset_Lmt_C = $this -> getCaliPramVal('VehSpeed_ThOffset_Lmt_C',$deviceID);
				
				$fld = $datAct->valuefield('obdSpeed');
				$rpm = $datAct->valuefield('obdRpm');
				$rpmValA = $VehSpeed_RPMLmt1_C;
				$rpmValB = 7000;
				$throt = $datAct->valuefield('obdThrottle');
				$thortVal= $VehSpeed_ThOffset_Lmt_C;
				$MaxobdSpeed = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$thort[0],$thortVal);
				//echo "speed Sensor failure MaxobdSpeed $MaxobdSpeed \n";
				if($MaxobdSpeed != 0)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				$VehSpeed_RPMLmt1_C = $this -> getCaliPramVal('VehSpeed_RPMLmt1_C',$deviceID);
				$VehSpeed_ThOffset_Lmt_C = $this -> getCaliPramVal('VehSpeed_ThOffset_Lmt_C',$deviceID);
				
				$fld = $datAct->valuefield('obdSpeed');
				$rpm = $datAct->valuefield('obdRpm');
				$rpmValA = $VehSpeed_RPMLmt1_C;
				$rpmValB = 6000;
				$accPos = $datAct->valuefield('obdAccPosD');
				$accPosVal= $VehSpeed_ThOffset_Lmt_C;
				$MaxobdSpeed = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$accPos[0],$accPosVal);
				//echo "speed Sensor failure MaxobdSpeed $MaxobdSpeed \n";
				if($MaxobdSpeed != 0)
				{
					return 1;
				}
				else
				{
					return 0;
				}	
			}
		}
	}

//End of monitoring algorithms ==============================================
	
	function getCalStageDataRange($stage,$key,$fun,$sprint,$calStg,$deviceID,$rpm,$rpmValA,$rpmValB,$ctemp,$ctempVal,$ctempA='',$ctempValA='',$oby='asc',$comA='>=')
	{
		$eQExtNew = ($ctempA !="" && $ctempValA!="") ? " and ($ctempA*1) $comA $ctempValA" : "";
		$eQExt = "$calStg = '$stage'";
		$eQExt .= " and ( ($rpm*1) >= $rpmValA and ($rpm*1) <= $rpmValB and ($ctemp*1) >= $ctempVal $eQExtNew)";
		$sql = "select $fun($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and  $eQExt order by time_s $oby limit 0,1";
		//echo ($key == "F3") ? "$sql \n" : "";
		$dat = $this->select($sql);
		return (trim($dat[0]['val']) != "") ? $dat[0]['val'] : 0;
	}
	
	function getCalStageDataRPMRangeMAX($stage,$key,$sprint,$calStg,$deviceID,$rpm,$rpmValA,$rpmValB,$ctemp,$ctempVal,$ctempA='',$ctempValA='',$oby='asc',$comA='>=')
	{
		$eQExtNew = ($ctempA !="" && $ctempValA!="") ? " and ($ctempA*1) $comA $ctempValA" : "";
		$eQExt = "$calStg = '$stage'";
		$eQExt .= " and ( ($rpm*1) >= $rpmValA and ($rpm*1) <= $rpmValB and ($ctemp*1) >= $ctempVal $eQExtNew)";
		$sqlcount = "select count($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and  $eQExt order by time_s $oby limit 0,1";	
		$dat = $this->select($sqlcount);
		$count = $dat[0]['val'];
		//$count = count($countA);
		$limitMIN = round($count * 0.05);
		$limitMAX = $count - $limitMIN;
		//echo ($key == "F4") ? "$count \n $sqlcount \n $limitMIN \n  $limitMAX \n" : "";
		$sql = "select ($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and  $eQExt order by time_s $oby limit $limitMIN,$limitMAX";
		$dat = array();
		$dat = $this->select($sql);
		$retval = max(array($dat));
		//echo ($key == "F4") ? "$sql \n" : "";
		return (trim($retval[0]['val']) != "") ? ($retval[0]['val']) : 0;
	}
	
	function getCalStageDataTorqueMAX($stage,$key,$sprint,$calStg,$deviceID,$rpm,$rpmValA,$rpmValB,$ctemp,$ctempVal,$ctempA='',$ctempValA='',$oby='desc',$comA='>=')
	{
		$eQExtNew = ($ctempA !="" && $ctempValA!="") ? " and ($ctempA*1) $comA $ctempValA" : "";
		$eQExt = "$calStg = '$stage'";
		$eQExt .= " and ( ($rpm*1) >= $rpmValA and ($rpm*1) <= $rpmValB and ($ctemp*1) >= $ctempVal $eQExtNew)";
		$sqlcount = "select count($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and  $eQExt order by time_s $oby limit 0,1";	
		$dat = $this->select($sqlcount);
		$count = $dat[0]['val'];
		//$count = count($countA);
		$limitMIN = round($count * 0.15);
		$limitMAX = $count - $limitMIN;
		//echo ($key == "Ftorque") ? "$count \n $sqlcount \n $limitMIN \n  $limitMAX \n" : "";
		$sql = "select ($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and  $eQExt order by time_s $oby limit $limitMIN,$limitMAX";
		$dat = array();
		$dat = $this->select($sql);
		$retval = max(array($dat));
		//echo ($key == "Ftorque") ? "$sql \n" : "";
		return (trim($retval[0]['val']) != "") ? ($retval[0]['val']) : 0;
	}
	
	function getCalStageDataRPMRangeMIN($stage,$key,$sprint,$calStg,$deviceID,$rpm,$rpmValA,$rpmValB,$ctemp,$ctempVal,$ctempA='',$ctempValA='',$oby='asc',$comA='>=')
	{
		$eQExtNew = ($ctempA !="" && $ctempValA!="") ? " and ($ctempA*1) $comA $ctempValA" : "";
		$eQExt = "$calStg = '$stage'";
		$eQExt .= " and ( ($rpm*1) >= $rpmValA and ($rpm*1) <= $rpmValB and ($ctemp*1) >= $ctempVal $eQExtNew)";
		$sqlcount = "select count($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and  $eQExt order by time_s $oby limit 0,1";	
		$dat = $this->select($sqlcount);
		$count = $dat[0]['val'];
		$limitMIN = round($count * 0.05);
		$limitMAX = $count - $limitMIN;
		//echo ($key == "F4") ? "$sqlcount \n $limitMIN \n  $limitMAX \n" : "";
		$sql = "select ($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and  $eQExt order by time_s $oby limit $limitMIN,$limitMAX";
		//echo ($key == "F4") ? "$sql \n" : "";
		$dat = array();
		$dat = $this->select($sql);
		$retval = min(array($dat));
		return (trim($retval[0]['val']) != "") ? ($retval[0]['val']) : 0;
	}
	
	function getCalStageDataBaseMAPRangeMIN($stage,$key,$sprint,$calStg,$deviceID,$rpm,$rpmValA,$rpmValB,$ctemp,$ctempVal,$ctempA='',$ctempValA='',$oby='asc',$comA='>=')
	{
		$eQExtNew = ($ctempA !="" && $ctempValA!="") ? " and ($ctempA*1) $comA $ctempValA" : "";
		$eQExt = "$calStg = '$stage'";
		$eQExt .= " and ( ($rpm*1) >= $rpmValA and ($rpm*1) <= $rpmValB and ($ctemp*1) >= $ctempVal $eQExtNew)";
		$sqlcount = "select count($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and  $eQExt order by time_s $oby limit 0,1";	
		//echo ($key == "F3") ? "$sqlcount \n" : "";
		$dat = $this->select($sqlcount);
		$count = $dat[0]['val'];
		if($count >= 10)
		{
			$limitMIN = 5;
		}
		else
		{
			$limitMIN = round($count * 0.90);
		}
		$limitMAX = $count;
		//echo ($key == "F3") ? "$sqlcount \n $limitMIN \n $limitMAX \n" : "";
		$sql = "select ($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and  $eQExt order by time_s $oby limit $limitMIN,$limitMAX";
		//echo ($key == "F3") ? "$sql \n" : "";
		$dat = array();
		$dat = $this->select($sql);
		$retval = min(array($dat));
		return (trim($retval[0]['val']) != "") ? ($retval[0]['val']) : 0;
	}
	
	function getCalStageDataRangeBaseM($stage,$key,$fun,$sprint,$calStg,$deviceID,$rpm,$rpmValA,$rpmValB,$ctemp,$ctempVal,$ctempA='',$ctempValA='',$oby='asc')
	{
		$eQExtNew = ($ctempA !="" && $ctempValA!="") ? " and ($ctempA*1) <= $ctempValA " : "";
		$eQExt = "$calStg = '$stage'";
		$eQExt .= " and ( ($rpm*1) >= $rpmValA and ($rpm*1) <= $rpmValB and ($ctemp*1) >= $ctempVal $eQExtNew)";
		$sql = "select $fun($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and ($key*1) >= 0 and $key <>'' and  $eQExt order by time_s $oby limit 0,1";
		$dat = $this->select($sql);
		return (trim($dat[0]['val']) != "") ? $dat[0]['val'] : 0;
	}
	
	// this function must be called every time 
	function setRegMonitor($reqFor,$deviceID,$dt,$row,$durTr)
	{
		//Overheat Detection ==============================
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		
		//Total Distance travelled by user
		//Check to see if it is the users first drive. If it is the first drive store the first value of obdDistanceSinceErrorCleared
		$sql = "select * from device_drive where device_id='$deviceID' limit 0,1";
		$dat = $this -> select($sql);
		if(count($dat) <= 0)
		{
			$fld = $datAct -> valuefield('obdDistanceSinceErrorCleared');
			$MinDistErrClrdfirst = $this -> getCalStageData(4,$fld[0],"min",$reqFor,$calStg,$deviceID);
			if($MinDistErrClrdfirst > 65535) //65535 is max value of this PID. values above these are considered rubbish values.
			{
				$MinDistErrClrdfirst = 100000;
			}
			else
			{
				//do nothing;
			}
			
			$sql = "update device set first_dist_since_err_clrd = '$MinDistErrClrdfirst' where device_id = '$deviceID'";
			mysql_query($sql);
		}
		
		$sql = "select total_dist, first_dist_since_err_clrd from device where device_id ='$deviceID' limit 0,1";
		$numDist = $this -> select($sql);
		$numDist = $numDist[0];
		$numDistance = $numDist[0]['total_dist'];
		$intDistSinceErrClrd = $numDist[0]['first_dist_since_err_clrd'];
		
		if($intDistSinceErrClrd != "")
		{
			$intDistSinceErrClrd = 0;
			$sql = "update device set first_dist_since_err_clrd = '$intDistSinceErrClrd' where device_id = '$deviceID'";
			mysql_query($sql);
		}
		else
		{
			//Do nothing;
		}
		
		if($intDistSinceErrClrd < 65535)
		{
			$travel_purchase = $vehi[0]['travel_purchase'];
			if($numDistance == 0 || $numDistance == "")
			{
				$numDistance = $travel_purchase;
			}
			else
			{
				//do nothing;
			}
			
			$fld = $datAct -> valuefield('obdDistanceSinceErrorCleared');
			$MaxDistErrClrd = $this -> getCalStageData(4,$fld[0],"max",$reqFor,$calStg,$deviceID);
			
			if($MaxDistErrClrd <= 65535 && ($MaxDistErrClrd > $intDistSinceErrClrd)) //65535 is max value of this PID. values above these are considered rubbish values.
			{
				$diff = $MaxDistErrClrd - $intDistSinceErrClrd;
				$numDistance = $travel_purchase + $diff;
				$sql = "update device set total_dist = '$numDistance' where device_id = '$deviceID'";
				mysql_query($sql);
			}
			else
			{
				//do nothing;
			}
		}
		
		//All base algos are shifted to the app and read through the PID obdActiveAlerts. The final result is used to find which base algo was triggered.
		$sql = "select * from `device_data` where `device_id` = '$deviceID' and `Fsprint` = '$reqFor' order by id desc limit 0,1";
		$dat = $this -> select($sql);
		$alert = $dat[0];
		$alert = $dat[0]['F68'];
		
		$alertbin = decbin($alert);
		$cool = 8;
		$rail = 4096;
		$bat = 8192;
		$map = 16384;
		$spdsns = 32768;
		
		$vehi = $this -> getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		
		$msgCTempCN = 1;
		$msgidleCRPCN = 1;
		$msgidleMapCN = 1;
		$msgMapCN = 1;
		$vehSpeed = 1;
		
		if(($cool & $alert) != 0)
		{
			$msgCTempCN = 0;
		}
		if(($rail & $alert) != 0)
		{
			$msgidleCRPCN = 0;
		}
		if(($map & $alert) != 0)
		{
			//$mapset = 1;
			if($vType == 1)
			{
				$msgMapCN = 0;
			}
			else
			{
				$msgidleMapCN = 0;
			}
			
		}
		if(($spdsns & $alert) != 0)
		{
			$vehSpeed = 0;
		}
		
		//return array($msgCTemp,$msgidleMap,$msgMap);
		$postVal = array();
		$postVal['device_id'] = $deviceID;
		$postVal['dt'] = $dt;
		$postVal['drive'] = $reqFor;
		$postVal['dur'] = $durTr;
		$postVal['dist'] = $distTr;
		
		$postVal['tempmsg'] = $msgCTempCN;
		$postVal['mapimsg'] = $msgidleMapCN;
		$postVal['mapmsg'] = $msgMapCN;
		$postVal['crpmsg'] = $msgidleCRPCN;
		$postVal['vehspeed'] = $vehSpeed;
		
		$bathel = $this -> calcBatHelM($reqFor,$deviceID,$dt,$row,$durTr,1);
		$postVal['bat_st'] = $bathel[1];
		$postVal['bat_c'] = $bathel[0];
	
		//$vehSpeed = $this ->getVehSpeedSenceM($reqFor,$deviceID);
		//$postVal['vehspeed'] = $vehSpeed;
		
		//$vehChng = $this -> vehChangeDet($reqFor,$deviceID,$dt,$row,$durTr);
		//$postVal['vehchng'] = $vehChng;
		
		$dataMyVal = array();
		$dataMyVal['tempmsg'] = $msgCTempCN;
		$dataMyVal['mapimsg'] = $msgidleMapCN;
		$dataMyVal['mapmsg'] = $msgMapCN;
		$dataMyVal['crpmsg'] = $msgidleCRPCN;
		$dataMyVal['vehspeed'] = $vehSpeed;
		$dataMyVal['bat_st'] = $bathel[1];
		//$dataMyVal['vehchng'] = (trim($vehChng)== "Car is Not Authorized") ? 0 : 1;
		//$dataMyVal['maf'] = $this -> maxSpeed($reqFor,'obdAirFlowRate');
		//$dataMyVal['map'] = $this -> maxSpeed($reqFor,'obdManifoldPressure');
		//$dataMyVal['torque'] = $this -> maxSpeed($reqFor,'Ftorque',1);
		//print_r($dataMyVal);
		
		$alertAct = new alertAct($deviceID);
		$alertAct -> alertBaseAlgo($dataMyVal);
		
		$postVal['ad_dt'] = date('Y-m-d H:i:s');
		$sql = "select * from device_monitormain where device_id='$deviceID' and dt = '$dt' and drive = '$reqFor' limit 0,1";
		$dat = $this -> select($sql);
		if(count($dat) <= 0)
		{
			$saveData = new saveForm('device_monitormain',$postVal);
			if($saveData -> addData());
		}
		else
		{
			$postVal['id'] = $dat[0]['id'];
			$saveData = new saveForm('device_monitormain',$postVal);
			if($saveData -> updateData('id','id'));
		}
	}

	//veh change detection
	function vehChangeDet($reqFor,$deviceID,$dt,$row,$durTr)
	{
		$datAct = new datAct('device1');
		$calStg = $datAct->valuefield('obdCalcEngineState');
		$calStg = $calStg[0];
		$vehi = $this->getvehicle($deviceID);
		$vType = $vehi[0]['fuel_tpe'];
		
		$Thr_DefPos_C = $this -> getCaliPramVal('Thr_DefPos_C',$deviceID);
		$AccPed_DefPos_C = $this -> getCaliPramVal('AccPed_DefPos_C',$deviceID);
		
		$VehChng_RPM_Lmt_C = $this -> getCaliPramVal('VehChng_RPM_Lmt_C',$deviceID);
		$VehChng_ELoad_Lmt_C = $this -> getCaliPramVal('VehChng_ELoad_Lmt_C',$deviceID);
		$VehChng_Coolant_Lmt_C = $this -> getCaliPramVal('VehChng_Coolant_Lmt_C',$deviceID);
		$RPM_Idle_Warm_C = $this -> getCaliPramVal('RPM_Idle_Warm_C',$deviceID);
		$RPM_Idle_Ld1_C = $this -> getCaliPramVal('RPM_Idle_Ld1_C',$deviceID);
		$RPM_Idle_Ld2_C = $this -> getCaliPramVal('RPM_Idle_Ld2_C',$deviceID);
		
		$ELoad_Idle_Warm_C = $this -> getCaliPramVal('ELoad_Idle_Warm_C',$deviceID);
		$ELoad_Idle_Ld1_C = $this -> getCaliPramVal('ELoad_Idle_Ld1_C',$deviceID);
		$ELoad_Idle_Ld2_C = $this -> getCaliPramVal('ELoad_Idle_Ld2_C',$deviceID);
		$msg = "";
		
		$colTemp = $datAct->valuefield('obdCoolantTemperature');
		$CoolHigh1 = $this -> getCalStageData('(2,3)',$colTemp[0],"max",$reqFor,$calStg,$deviceID,'asc','in');
		
		//$RPMCount is found to check if the user has done a warm idle to run the unauthorised car algo
		//$fld = $datAct->valuefield('obdRpm');
		//$rpm = $datAct->valuefield('obdRpm');
		$rpmValA = 450;
		$rpmValB = 2000;
		//$ctemp = $datAct->valuefield('obdCoolantTemperature');
		$ctempVal= $VehChng_Coolant_Lmt_C;
		//$ctempA = $datAct->valuefield('obdThrottle');
		//$ctempValA= $Thr_DefPos_C;
		$RPMCount = $this -> getCalStageDataRange(2,'F4',"count",$reqFor,$calStg,$deviceID,'F4',$rpmValA,$rpmValB,'F1',$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
		//$RPMCount = $this -> getCalStageData(2,'F4',"count",$reqFor,$calStg,$deviceID,'asc');
		
		if($RPMCount > 5)
		{
			if($CoolHigh1 > $VehChng_Coolant_Lmt_C)
			{
				if($vType == 1)
				{
					$fld = $datAct->valuefield('obdRpm');
					$rpm = $datAct->valuefield('obdRpm');
					$rpmValA = $RPM_Idle_Warm_C - $VehChng_RPM_Lmt_C;
					$rpmValB = $RPM_Idle_Warm_C + $VehChng_RPM_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdThrottle');
					//$ctempValA= $Thr_DefPos_C;
					//$RPMIdleWarm = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$RPMIdleWarm = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					$fld = $datAct->valuefield('obdRpm');
					$rpm = $datAct->valuefield('obdRpm');
					$rpmValA = $RPM_Idle_Ld1_C - $VehChng_RPM_Lmt_C;
					$rpmValB = $RPM_Idle_Ld1_C + $VehChng_RPM_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdThrottle');
					//$ctempValA= $Thr_DefPos_C;
					//$RPMEload1 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$RPMEload1 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
	
					$fld = $datAct->valuefield('obdRpm');
					$rpm = $datAct->valuefield('obdRpm');
					$rpmValA = $RPM_Idle_Ld2_C - $VehChng_RPM_Lmt_C;
					$rpmValB = $RPM_Idle_Ld2_C + $VehChng_RPM_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdThrottle');
					//$ctempValA= $Thr_DefPos_C;
					//$RPMEload2 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$RPMEload2 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					$fld = $datAct->valuefield('obdEngineLoad');
					$rpm = $datAct->valuefield('obdEngineLoad');
					$rpmValA = $ELoad_Idle_Warm_C - $VehChng_ELoad_Lmt_C;
					$rpmValB = $ELoad_Idle_Warm_C + $VehChng_ELoad_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdThrottle');
					//$ctempValA= $Thr_DefPos_C;
					//$EloadIdleWarm = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$EloadIdleWarm = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					$fld = $datAct->valuefield('obdEngineLoad');
					$rpm = $datAct->valuefield('obdEngineLoad');
					$rpmValA = $ELoad_Idle_Ld1_C - $VehChng_ELoad_Lmt_C;
					$rpmValB = $ELoad_Idle_Ld1_C + $VehChng_ELoad_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdThrottle');
					//$ctempValA= $Thr_DefPos_C;
					//$Eload1 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$Eload1 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					$fld = $datAct->valuefield('obdEngineLoad');
					$rpm = $datAct->valuefield('obdEngineLoad');
					$rpmValA = $ELoad_Idle_Ld2_C - $VehChng_ELoad_Lmt_C;
					$rpmValB = $ELoad_Idle_Ld2_C + $VehChng_ELoad_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdThrottle');
					//$ctempValA= $Thr_DefPos_C;
					//$Eload2 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$Eload2 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					if(($RPMIdleWarm == 0) && ($EloadIdleWarm == 0))// || ($RPMEload1 == 0 && $Eload1 == 0) || ($RPMEload2 == 0 && $Eload2 == 0))
					{
						if(($RPMEload1 == 0) && ($Eload1 == 0))
						{
							if(($RPMEload2 == 0) && ($Eload2 == 0))
							{
								$msg = "Car is Not Authorized";
							}
							else
							{
								$msg = "Car is Authorized";
							}
						}
						else
						{
							$msg = "Car is Authorized";
						}
					}
					else
					{
						$msg = "Car is Authorized";
					}
				}
				else
				{
					$fld = $datAct->valuefield('obdRpm');
					$rpm = $datAct->valuefield('obdRpm');
					$rpmValA = $RPM_Idle_Warm_C - $VehChng_RPM_Lmt_C;
					$rpmValB = $RPM_Idle_Warm_C + $VehChng_RPM_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdAccPosD');
					//$ctempValA= $AccPed_DefPos_C;
					//$RPMIdleWarm = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$RPMIdleWarm = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					$fld = $datAct->valuefield('obdRpm');
					$rpm = $datAct->valuefield('obdRpm');
					$rpmValA = $RPM_Idle_Ld1_C - $VehChng_RPM_Lmt_C;
					$rpmValB = $RPM_Idle_Ld1_C + $VehChng_RPM_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdAccPosD');
					//$ctempValA= $AccPed_DefPos_C;
					//$RPMEload1 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$RPMEload1 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					$fld = $datAct->valuefield('obdRpm');
					$rpm = $datAct->valuefield('obdRpm');
					$rpmValA = $RPM_Idle_Ld2_C - $VehChng_RPM_Lmt_C;
					$rpmValB = $RPM_Idle_Ld2_C + $VehChng_RPM_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdAccPosD');
					//$ctempValA= $AccPed_DefPos_C;
					//$RPMEload2 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$RPMEload2 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					$fld = $datAct->valuefield('obdEngineLoad');
					$rpm = $datAct->valuefield('obdEngineLoad');
					$rpmValA = $ELoad_Idle_Warm_C - $VehChng_ELoad_Lmt_C;
					$rpmValB = $ELoad_Idle_Warm_C + $VehChng_ELoad_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdAccPosD');
					//$ctempValA= $AccPed_DefPos_C;
					//$EloadIdleWarm = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$EloadIdleWarm = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					$fld = $datAct->valuefield('obdEngineLoad');
					$rpm = $datAct->valuefield('obdEngineLoad');
					$rpmValA = $ELoad_Idle_Ld1_C - $VehChng_ELoad_Lmt_C;
					$rpmValB = $ELoad_Idle_Ld1_C + $VehChng_ELoad_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdAccPosD');
					//$ctempValA= $AccPed_DefPos_C;
					//$Eload1 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$Eload1 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					$fld = $datAct->valuefield('obdEngineLoad');
					$rpm = $datAct->valuefield('obdEngineLoad');
					$rpmValA = $ELoad_Idle_Ld2_C - $VehChng_ELoad_Lmt_C;
					$rpmValB = $ELoad_Idle_Ld2_C + $VehChng_ELoad_Lmt_C;
					$ctemp = $datAct->valuefield('obdCoolantTemperature');
					$ctempVal= $VehChng_Coolant_Lmt_C;
					//$ctempA = $datAct->valuefield('obdAccPosD');
					//$ctempValA= $AccPed_DefPos_C;
					//$Eload2 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal,$ctempA[0],$ctempValA,'asc','<=');
					$Eload2 = $this -> getCalStageDataRange(2,$fld[0],"max",$reqFor,$calStg,$deviceID,$rpm[0],$rpmValA,$rpmValB,$ctemp[0],$ctempVal);
					
					if(($RPMIdleWarm == 0) && ($EloadIdleWarm == 0))// || ($RPMEload1 == 0 && $Eload1 == 0) || ($RPMEload2 == 0 && $Eload2 == 0))
					{
						if(($RPMEload1 == 0) && ($Eload1 == 0))
						{
							if(($RPMEload2 == 0) && ($Eload2 == 0))
							{
								$msg = "Car is Not Authorized";
							}
							else
							{
								$msg = "Car is Authorized";
							}
						}
						else
						{
							$msg = "Car is Authorized";
						}
					}
					else
					{
						$msg = "Car is Authorized";
					}
					/*if(($RPMIdleWarm != 0 && $EloadIdleWarm != 0)||($RPMEload1 != 0 && $Eload1 != 0) || ($RPMEload2 != 0 && $Eload2 != 0))
					{
						$msg = "Car is Authorized";
					}
					else
					{
						$msg = "Car is Not Authorized";
					}*/
				}
			}
			else
			{
				//do nothing;
			}
		}
		else
		{
			//do nothing;
		}
		return $msg;
	}	
	
//Monitor Fun End ================================================================

	function setReportData($deviceID,$dt,$id,$type)
	{
		$dt = date('Y-m-d H:i:s');
		$dtTxt = date('dMY H:i:s',strtotime($dt));
		$typeTxt = ($type == 1) ? "Calibration $dtTxt" : "Monitor $dtTxt" ;
		$postVal = array();
		$postVal['devid'] = $deviceID;
		$dev = $this -> getdevice($deviceID);
		$postVal['did'] = $dev['id'];
		$postVal['type'] = $typeTxt;
		$postVal['status'] = 2;
		$postVal['ad_dt'] = $dt;
		$postVal['ad_by'] = 1;
		$postVal['idmc'] = $id;
		$postVal['mctype'] = $type;
		$saveData = new saveForm('report',$postVal);
		$saveData->addData();
	}
	
	function getDistSince($dev)
	{
		$sql = "select sum((dist*1)) from device_drive where device_id ='$dev' and (dist*1) <> 0 and (avg_ml*1) <> 0";
		$numDist = $this->select($sql);
		$numDist = $numDist[0][0];
		$vehi = $this->getvehicle($dev);
		$travel_purchase = $vehi[0]['travel_purchase'];
		return array($numDist,$travel_purchase);
	}	
	
	function getCalStageData($stage,$key,$fun,$sprint,$calStg,$deviceID,$oby='asc',$eq='=')
	{
		$eQExt = ($eq == "in") ? " $eq $stage" : "$eq '$stage'";
		$sql = "select $fun($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>''  and $calStg $eQExt order by time_s $oby limit 0,1";
		$dat = $this->select($sql);
		//echo ($key == "F37") ? "$sql \n" : "";
		return (trim($dat[0]['val']) != "") ? $dat[0]['val'] : 0;
	}
	
	function getCalStageDataRangeSet($stage,$key,$fun,$sprint,$calStg,$deviceID,$range=10,$oby='asc',$eq='=')
	{
		$eQExt = ($eq == "in") ? " $eq $stage" : "$eq '$stage'";
		$sql = "select count(*) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and $calStg $eQExt order by time_s desc limit 0,1";
		$dat = $this->select($sql);
		
		$x = ($dat[0]['val']>=1) ? 1 : 0;
		$dat[0]['val'] -= $x;
		$countM = ceil($dat[0]['val']/2);
		
		$countMP = $countM+$range;
		$countMM = $countM-$range;
		
		$countMP = ($countMP <= $dat[0]['val']) ? $countMP : $countM;
		$countMM = ($countMM <=0) ? 0 : $countMM;
		
		//echo "C : $countM / $countMP / $countMM \n ";
		
		$sql = "select time_s from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and $calStg $eQExt order by time_s $oby limit $countMM,1";
		$dat = $this->select($sql);
		$frm = $dat[0]['time_s'];
		
		//echo "A : $sql \n";
		
		$sql = "select time_s from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and $calStg $eQExt order by time_s $oby limit $countMP,1";
		$dat = $this->select($sql);
		$to = $dat[0]['time_s'];
		
		//echo "B : $sql \n";
				
		$eQExt .= " and (time_s >='$frm' and time_s <='$to') ";
		$sql = "select $fun($key*1) as val from device_data where device_id='$deviceID' and Fsprint='$sprint' and  ($key*1) >= 0 and $key <>'' and $calStg $eQExt order by time_s $oby limit 0,1";
		//echo "C : $sql \n";
		$dat = $this->select($sql);
		//echo ($key == "F3") ? "$sql \n" : "";
		return (trim($dat[0]['val']) != "") ? $dat[0]['val'] : 0;
	}
	
	function getCaliPramVal($param,$deviceID)
	{
		//return 1;
		 $sqlSett = "select * from vehicle_setting";
		 $vehSett = mysql_fetch_array(mysql_query($sqlSett));
		 $sql = "select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='$deviceID'";
		 $vehicle = mysql_fetch_array(mysql_query($sql));
		 
		 $pDVar = ($vehicle['fuel_tpe'] == 1) ? "p" : "d";
		 $f1 = "calval$pDVar";
		 $vsetVal = json_decode($vehSett[$f1],true);
		 $calval = json_decode($vehicle['calval'],true);
		 
		 $val = trim($param);
		 $retVal = (trim($calval[$val]) != "") ? $calval[$val] : $vsetVal[$val];
		 return $retVal;
		 //return array($vehicle,$vehSett);
	}
	
	function getCaliPramValArry($param,$deviceID)
	{
		//return 1;
		$sqlSett = "select * from vehicle_setting";
		$vehSett = mysql_fetch_array(mysql_query($sqlSett));
		$sql = "select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='$deviceID'";
		$vehicle = mysql_fetch_array(mysql_query($sql));
		
		$pDVar = ($vehicle['fuel_tpe'] == 1) ? "p" : "d";
		$f1 = "calval$pDVar";
		$vsetVal = json_decode($vehSett[$f1],true);
		$calval = json_decode($vehicle['calval'],true);
		// echo "funct = $calval";	 
		return $calval;
		//return array($vehicle,$vehSett);
	}
	
	function setCaliPramVal($param,$deviceID,$valSet,$clear = 0)
	{
		//return 1;
		 $sqlSett = "select * from vehicle_setting";
		 $vehSett = mysql_fetch_array(mysql_query($sqlSett));
		 $sql = "select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='$deviceID'";
		 $vehicle = mysql_fetch_array(mysql_query($sql));
		 
		 $pDVar = ($vehicle['fuel_tpe'] == 1) ? "p" : "d";
		 $f1 = "calval$pDVar";
		 $calval = json_decode($vehicle['calval'],true);
		 $calval = (count($calval)>= 2) ? $calval : json_decode($vehSett[$f1],true);
		 
		 $val = trim($param);
		 $calval[$val] = $valSet;
		 //echo "$val = $valSet;";
		// print_r($calval);
		 $calval = ($clear == 1) ? "" : json_encode($calval);
		 $sqlUp = "update vehicles set calval = '$calval' where id = '".$vehicle['id']."'";
		 mysql_query($sqlUp);
	}
	
	function cityPres($deviceID)
	{
		//return 1;
		$sql = "select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='$deviceID'";
		$vehicle = mysql_fetch_array(mysql_query($sql));
		$city = ($vehicle['city'] >= 1) ? ($vehicle['city'] - 1) : 0;
		$sqlSett = "select citylist from vehicle_setting";
		$vehSett = mysql_fetch_array(mysql_query($sqlSett));
		$cityList =  preg_split('/$\R?^/m',$vehSett['citylist']);
		$valCity = explode(',',$cityList[$city]);
		$ret = intval($valCity[1]);
		return $ret;
	}
	
	function getdeviceTest($deviceID)
	{
	/*	$dt = date('Y-m-d H:i:s');
		$sql = "select * FROM `testid` WHERE `device_id` = '$deviceID' and dt = '$dt'";
		$dat = $this -> select($sql);
		if(count($dat) >= 1)
		{
			//print_r($dat);
			$dt = date('Ymd',strtotime($dat[0]['dt']));
			$id = "ECE".$dt.str_pad($dat[0]['id'], 4, '0', STR_PAD_LEFT);
			return $id;
		}
		else
		{
			//$dt = date('Y-m-d H:i:s');
			$sql = "insert into testid (device_id,status,dt) values('$deviceID', '1', '$dt')";
			if(mysql_query($sql))
			{
				$inId = mysql_insert_id();
				$dt = date('Ymd',strtotime($dt));
				$id = "ECE".$dt.str_pad($inId, 4, '0', STR_PAD_LEFT);
				return $id;
			}
		}
	 */
		return 0;
	}
	
	// vehicle Sec Set / Get
	function getvehSpec($keyMfd,$keyMode,$fuel,$keyVar)
	{
		$sql = "select * from vehspec where mfd = '$keyMfd' and model = '$keyMode' and fuel='$fuel' and varient='$keyVar'";
		$data = mysql_fetch_array(mysql_query($sql));
		$arrayData = array();
		$arrayData = json_decode($data[specinfo],true);
		$arrayData['fuelType'] = $data['fuel'];
		$arrayData['varients'] = $data['varient'];
		$arrayData['engineCapacity'] = $data['engcap'];
		$arrayData['bodyType'] = $data['btype'];
		$arrayData['maxtog'] = $data['maxtog'];
		$arrayData['rpm'] = $data['rpm'];
		$arrayData['maxpow'] = $data['maxpow'];
		$arrayData['rpm2'] = $data['rpm2'];
		$arrayData['maxspeed'] = $data['maxspeed'];
		$arrayData['map'] = $data['map'];
	}
	
	function setCaliPramValue($param,$deviceID)
	{
		//return 1;
		 $sqlSett = "select * from vehicle_setting";
		 $vehSett = mysql_fetch_array(mysql_query($sqlSett));
		 $sql = "select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='$deviceID'";
		 $vehicle = mysql_fetch_array(mysql_query($sql));
		 
		 $pDVar = ($vehicle['fuel_tpe'] == 1) ? "p" : "d";
		 $f1 = "calval$pDVar";
		 $calval = json_decode($vehicle['calval'],true);
		 $calval = (count($calval)>= 2) ? $calval : json_decode($vehSett[$f1],true);
		 
		 foreach($calval as $key => $value)
		 {
		 	$calval[$key]  = (trim($param[$key]) != "") ? $param[$key] : $value;
		 }
		 $calval = ($clear == 1) ? "" : json_encode($calval);
		 $sqlUp = "update vehicles set calval = '$calval' where id = '".$vehicle['id']."'";
		 mysql_query($sqlUp);
		 $this -> setgetDeviceAction($deviceID,1);
	}
	
	function setvehSpec($keyMfd,$keyMode,$fuel,$keyVar,$eng_cap,$deviceID,$mfg_yr,$isId="",$city="")
	{
		if($isId == 1)
		{
			$sql = "select device_id from device where id='$deviceID'";
			$device = mysql_fetch_array(mysql_query($sql));
			$deviceID = $device['device_id'];
		}
		if($city != "")
		{
			$sqlSett = "select citylist from vehicle_setting";
			$vehSett = mysql_fetch_array(mysql_query($sqlSett));
			$cityList =  preg_split ('/$\R?^/m',$vehSett['citylist']);
			$cI = ($city <= 0) ? 0 : $city - 1;
			$cty = explode(',',$cityList[$cI]);
			$cityBp = intval($cty[1]);
			//echo "$cityBp";
			//$setSpec["Baro_Press_City_C",$deviceID,$cityBp);
		}		
		$sql = "select * from vehspec where mfd = '$keyMfd' and model = '$keyMode' and fuel='$fuel' and varient='$keyVar' and engcap ='$eng_cap' and mfg_yr ='$mfg_yr'";
		$res = mysql_query($sql);
		
		$sqlSett = "select * from vehicle_setting";
		$vehSett = mysql_fetch_array(mysql_query($sqlSett));
		$cap = explode(';',$vehSett['engine_capacity']);
		
		$setSpec = array();
		
		if(mysql_num_rows($res) >= 1)
		{
			$data = mysql_fetch_array($res);
			$arrayData = array();
			
			$arrayData = json_decode($data['specinfo'],true);
			foreach($arrayData as $key => $value)
			{
				//echo "setCaliPramVal($key]=$value); <br/>";
				$setSpec[$key]=$value;
			}
			$setSpec["Max_Torque_C"]= 1;
			$setSpec["MAP_Abs_PkLmt3_C"]=$data['map'];
			$setSpec["MAP_Dif_PkLmt3_C"]=$data['map'];
			$setSpec["Max_Torque_C"]=$data['maxtog'];
			$setSpec["Eng_Disp_C"]=$cap[$eng_cap];
			$setSpec["Baro_Press_City_C"]=$cityBp;
			//echo $data['maxtog']."a $deviceID ";
			$this -> setCaliPramValue($setSpec,$deviceID);
			return true;
		}
		else
		{
			$sql = "select * from vehspec where mfd = '$keyMfd' and model = '$keyMode' and fuel='$fuel' and varient='$keyVar' and engcap ='$eng_cap'";
			$res = mysql_query($sql);

			$sqlSett = "select * from vehicle_setting";
		 	$vehSett = mysql_fetch_array(mysql_query($sqlSett));
			$cap = explode(';',$vehSett['engine_capacity']);
			
			if(mysql_num_rows($res) >= 1)
			{
				$data = mysql_fetch_array($res);
				$arrayData = array();
				
				$arrayData = json_decode($data['specinfo'],true);
				foreach($arrayData as $key => $value)
				{
					//echo "setCaliPramVal($key]=$value); <br/>";
					$setSpec[$key]=$value;
				}
				$setSpec["Max_Torque_C"]=1;
				$setSpec["MAP_Abs_PkLmt3_C"]=$data['map'];
				$setSpec["MAP_Dif_PkLmt3_C"]=$data['map'];
				$setSpec["Max_Torque_C"]=$data['maxtog'];
				$setSpec["Eng_Disp_C"]=$cap[$eng_cap];
				$setSpec["Baro_Press_City_C"]=$cityBp;
				$this -> setCaliPramValue($setSpec,$deviceID);
				return true;
			}
			else
			{
				$sql = "select * from vehspec where mfd = '$keyMfd' and model = '$keyMode' limit 0,1";
				// just to get the var list of the model/veh
				$res = mysql_query($sql);
				$data = mysql_fetch_array($res);
				$arrayData = array();							
				$arrayData = json_decode($data['specinfo'],true);
				foreach($arrayData as $key => $value)
				{
					//echo "setCaliPramVal($key]=$value); <br/>";
					$setSpec[$key]="";
				}
				$setSpec["Max_Torque_C"]=1; // to clear all
				$setSpec["MAP_Abs_PkLmt3_C"]="";
				$setSpec["MAP_Dif_PkLmt3_C"]="";
				$setSpec["Max_Torque_C"]="";
				$setSpec["Eng_Disp_C"]=$cap[$eng_cap];
				$setSpec["Baro_Press_City_C"]=$cityBp;
				$this -> setCaliPramValue($setSpec,$deviceID);
				return false;
			}
		}
		return false;
	}
	
	function generateRandomString($email,$length = 10)
	{
	    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++)
	    {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
		$dev = $this->select("select * from activation_code where activation_code = '$randomString'");
		if(count($dev) >= 1)
		{
			$this -> generateRandomString($email);
		}
		else
		{
			$table = "activation_code";
			$postVal = array();
			$postVal['expdt']=date('Y-m-d',(strtotime(date('Y-m-d')) + (365*60*60*24)));
			$postVal['activation_code'] = $randomString;
			$postVal['status'] = 1;
			$postVal['ad_by'] = 1;
			$postVal['email'] = $email;
			$postVal['used_by'] = $email;
			$postVal['ad_dt'] = date('Y-m-d');
			$saveData = new saveForm($table,$postVal);
			if($saveData->addData())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}

$valDec = array(1,
10,
100,
1000,
10000,
100000,
1000000,
10000000,
100000000
);
$noData = array('NO DATA','NODATA','');

//'Fdist','Fintmil','Fmil','Ffcon','Favspeed','Ftorcomp'
$adF = array('fuelflow_l','power_hp','torque','dist','intmil','mil','fcon','avspeed','torcomp');
$adFU = array('fuelflow_l'=>'L/hr','power_hp'=>'HP','torque'=>'Nm','dist'=>'Km','intmil'=>'Km/L','mil'=>'Km/L','fcon'=>'L','avspeed'=>'Km/H','torcomp'=>'Nm');
$adFT = array('fuelflow_l'=>'Fuel Flow','power_hp'=>'Power','torque'=>'Torque','dist'=>'Distance Travelled','intmil'=>'Instantaneous Mileage','mil'=>'Mileage','fcon'=>'Fuel Consumed','avspeed'=>'Average Speed','torcomp'=>'Torque Compensated');

//alert array
$main_array=array("1"=>"ALERT_DTC","2"=>"ALERT_BASE_ALGO","3"=>"ALERT_CALIB_REPORT","4"=>"ALERT_AD_MON_ALGO","5"=>"ALERT_GENERAL");
$sub_Array=array();
$sub_Array[1] = array("1"=>"DTC Detected");
$sub_Array[2] = array("1"=>"Coolant System Failure", "2"=>"Air System Failure (NA)", "3"=>"Air System Failure (Turbo)", "4"=>"Fuel System Failure", "5"=>"Vehicle Speed Sensor Failure", "6"=>"Battery Health", "7"=>"Non Authorized Car Detection","8"=>"Car Not Supported - Diesel","9"=>"Car Not Supported - Petrol");
$sub_Array[3] = array("1"=>"Calib Health Report Generated - Normal", "2"=>"Low Calib Health Report Generated - Low Score DIESEL", "3"=>"Low Calib Health Report Generated - Low Score PETROL");
$sub_Array[4] = array("1"=>"Advanced Mon Results - Normal", "2"=>"Turbocharger Health", "3"=>"Air System", "4"=>"Fuel System", "5"=>"Exhaust System", "6"=>"Combustion Pressure", "7"=>"Combustion Pressure + RPM Oscillation DIESEL ", "8"=>"Combustion Pressure + RPM Oscillation PETROL", "9"=>"Rpm Oscillation Anomaly Analysis DIESEL", "10"=>"Rpm Oscillation Anomaly Analysis PETROL", "11"=>"Engine Cooling System");
$sub_Array[5] = array("1"=>"New Device Added", "2"=>"Valid Activation Code Entered", "3"=>"Activation Code Expiry Nearing", "4"=>"Activation Code Expired", "5"=>"Reset Password", "6"=>"Email Verification", "7"=>"Service Due", "8"=>"Welcome Mail");
$eventMainArray = array('1'=>'Wrong Stop','2'=>'Wrong Start','3'=>'Engine Failure','4'=>'Engine Heating','5'=>'Engine Wrong Stop');

date_default_timezone_set("Asia/Calcutta");

function closeAllDataFull()
{
	$vars = array_keys(get_defined_vars());
	for ($i = 0; $i < sizeOf($vars); $i++) 
	{
	    unset($$vars[$i]);
	}
	unset($vars,$i);
}
?>