<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/* -----------------------------------------------------------------------------------------------------------------------------
------------------------------------------------BACKEND PROCESS STARTS'S HERE---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------*/
// Login Routes
Route::get('', 'Login@index');
Route::get('login', 'Login@index');
Route::post('Auth', 'Login@varify');
Route::get('logout', 'Login@logout');
Route::get('flush', 'Login@destroy');
Route::get('menu', 'Login@menu');

Route::get('register', 'Register@index');
Route::get('profile', 'Dashboard@profile')->middleware('checkauth');
Route::get('dashboard', 'Dashboard@index')->middleware('checkauth');
Route::get('dashboard/{y}/{m}', 'Dashboard@index')->middleware('checkauth');
Route::post('get-graph1','Dashboard@getGraphData1')->middleware('checkauth');

// User Configuration Routes
Route::get('new-config', 'Configuration@newconfig')->middleware('checkauth');
Route::post('create-config', 'Configuration@createconfig')->middleware('checkauth');
Route::get('allot-config', 'Configuration@allotconfig')->middleware('checkauth');
Route::post('getUserConfig', 'Configuration@getuserconfig')->middleware('checkauth');
Route::post('update-config-usertype', 'Configuration@update_config_usertype')->middleware('checkauth');
Route::get('API_key', 'Device@api_key')->middleware('checkauth');
Route::get('add_apikey', 'Device@add_apikey')->middleware('checkauth');
Route::get('edit_apikey/{id}', 'Device@edit_apikey')->middleware('checkauth');
Route::post('edit_apikey_save', 'Device@edit_apikey_save')->middleware('checkauth');
Route::post('save_apikey', 'Device@save_apikey')->middleware('checkauth');

// User Add Routes
Route::get('add-user', 'Users@add')->middleware('checkauth');
Route::get('edit-user/{uid}', 'Users@edit')->middleware('checkauth');
Route::post('edit-user-save', 'Users@editUser')->middleware('checkauth');
Route::get('user-list', 'Users@list')->middleware('checkauth');
Route::post('create-user', 'Users@create')->middleware('checkauth');
Route::post('getUserPartner', 'Users@getUserPartner')->middleware('checkauth');
Route::post('getPartnerDevice', 'Users@getPartnerDevice')->middleware('checkauth');
Route::post('getCategoryPartner', 'Users@getCategoryPartner')->middleware('checkauth');
Route::get('add-operator', 'Users@addOperator')->middleware('checkauth');
Route::get('edit-operator/{uid}', 'Users@editOperator')->middleware('checkauth');
Route::get('operator', 'Users@operator')->middleware('checkauth'); 
Route::post('create-operator', 'Users@createOperator')->middleware('checkauth'); 
Route::post('update-operator', 'Users@updateOperator')->middleware('checkauth');


// Password
Route::get('change-password', 'Users@changepassword')->middleware('checkauth');
Route::post('save-password', 'Users@savepassword')->middleware('checkauth');
Route::get('reset-password-enginecal', 'Users@resetpasswordouter');
Route::post('reset-password-request', 'Users@resetpasswordrequest');
Route::get('enginecal-reset-password', 'Users@passwordreset');
// Route::post('reset_success', 'Users@reset_success');
Route::post('reset-password-save', 'Users@resetpasswordsave');

// Devices
Route::get('add-device', 'Device@add')->middleware('checkauth'); 
Route::post('deactive_app', 'Device@deactive_app')->middleware('checkauth');
Route::post('deactive_app1', 'Device@deactive_app1')->middleware('checkauth');
Route::get('delete_app', 'Device@delete_app')->middleware('checkauth');
Route::get('delete_firm', 'Device@delete_firm')->middleware('checkauth');
Route::get('delete_app_device', 'Device@delete_app_device')->middleware('checkauth');
Route::get('delete_firm_device', 'Device@delete_firm_device')->middleware('checkauth');
Route::post('deactive_firm', 'Device@deactive_firm')->middleware('checkauth');
Route::post('deactive_firm1', 'Device@deactive_firm1')->middleware('checkauth');
Route::get('edit-device/{id}', 'Device@edit')->middleware('checkauth');
Route::post('edit-device-save', 'Device@editdevice')->middleware('checkauth');
Route::get('device-list', 'Device@list')->middleware('checkauth');
Route::post('save-device', 'Device@saveDevice')->middleware('checkauth');
Route::post('checkDeviceIdStatus', 'Device@checkDeviceIdStatus')->middleware('checkauth');
Route::post('getDeviceList', 'Device@getDeviceList')->middleware('checkauth');
Route::post('getDeviceList1', 'Device@getDeviceList1')->middleware('checkauth');
Route::get('device_manager/{id}', 'Device@device_manager')->middleware('checkauth');
Route::post('getDeviceListcan', 'Device@getDeviceListcan')->middleware('checkauth');
Route::post('getDeviceListobd', 'Device@getDeviceListobd')->middleware('checkauth');
Route::get('device_log/{id}', 'Device@device_log')->middleware('checkauth');
Route::get('firmware/{id}', 'Device@firmware')->middleware('checkauth');
Route::get('firmware_mast', 'Device@firmware_mast')->middleware('checkauth');
Route::get('app_update/{id}', 'Device@app_update')->middleware('checkauth');
Route::get('add_bt/{id}', 'Device@add_bt')->middleware('checkauth');
Route::get('add_bt_dev/{id}', 'Device@add_bt_dev')->middleware('checkauth');
Route::get('save_bt_dev/{id}', 'Device@save_bt_dev')->middleware('checkauth');  
Route::get('update_bt_dev/{id}', 'Device@update_bt_dev')->middleware('checkauth');
Route::get('remove_bt_device/{id}', 'Device@remove_bt_device')->middleware('checkauth'); 
Route::get('update_bt_device/{id}', 'Device@update_bt_device')->middleware('checkauth'); 

Route::get('update_verion', 'Device@update_verion')->middleware('checkauth');
Route::get('update_verion_firm', 'Device@update_verion_firm')->middleware('checkauth');
Route::get('install_app_version/{id}', 'Device@install_app_version')->middleware('checkauth');
Route::get('uninstall_app/{id}', 'Device@uninstall_app')->middleware('checkauth');
Route::get('uninstall_firm/{id}', 'Device@uninstall_firm')->middleware('checkauth');
Route::get('install_firm_version/{id}', 'Device@install_firm_version')->middleware('checkauth');
Route::get('install_app', 'Device@install_app')->middleware('checkauth');
Route::post('app-save', 'Device@save_app')->middleware('checkauth');
Route::post('app-ver-save', 'Device@app_ver_save')->middleware('checkauth');
Route::post('app-ver-update', 'Device@app_ver_update')->middleware('checkauth');
Route::get('save_cmd', 'Device@save_cmd')->middleware('checkauth');
Route::post('firm-ver-update', 'Device@firm_ver_update')->middleware('checkauth');
Route::post('firm-ver-save', 'Device@firm_ver_save')->middleware('checkauth');
Route::post('firmware-save', 'Device@firmaware_app')->middleware('checkauth');
Route::get('device_conf/{id}', 'Device@device_conf')->middleware('checkauth');

// Settings
Route::get('system-settings', 'Settings@index')->middleware('checkauth');
Route::get('system-settings-delete-logo', 'delete_logo@index')->middleware('checkauth');
Route::post('update-system-settings', 'Settings@updateSystemSettings')->middleware('checkauth');
Route::get('database-optimization', 'Settings@dbDatabase')->middleware('checkauth');
Route::post('db-optimize','Settings@updateDbOptimize')->middleware('checkauth');
Route::post('clear-db-optimize', 'Settings@clearDbOptimize')->middleware('checkauth');
Route::post('clear-db-optimize-range', 'Settings@clearDbOptimizeRange')->middleware('checkauth');

// Alerts
// alerts
Route::get('alerts', 'Alerts@index')->middleware('checkauth');
Route::get('getFailedTest', 'Alerts@getFailedTest')->middleware('checkauth');
Route::get('getIncompleateTest', 'Alerts@getIncompleateTest')->middleware('checkauth');


// Vehicle
Route::get('vehicle-settings', 'Settings@vehicleSettings')->middleware('checkauth');
Route::post('update-vehicle-settings', 'Settings@updateVehicleSettings')->middleware('checkauth');
Route::get('vehicle-specification', 'Settings@vehicleSpecification')->middleware('checkauth');
Route::post('load-list-vehicle-specification', 'Settings@loadListVehicleSpecification')->middleware('checkauth');
Route::post('upload-vehicle-csv', 'Settings@vehicleSpecificationcsvToMysql')->middleware('checkauth');
Route::get('add-vehicle/{id}', 'Vehicle@index')->middleware('checkauth');
Route::get('add-vehicle_new/{id}', 'Vehicle@add_vehicle_new')->middleware('checkauth');
Route::post('getModelList', 'Vehicle@getModelList')->middleware('checkauth');
Route::post('getVarientList', 'Vehicle@getVarientList')->middleware('checkauth');
Route::post('process-vehicle-data', 'Vehicle@processVehicleData')->middleware('checkauth');
Route::post('process-vehicle-data1', 'Vehicle@processVehicleData1')->middleware('checkauth');
Route::get('vehicle-calibration/{id}', 'Vehicle@calibration')->middleware('checkauth');
Route::get('vehicle-calibration_new/{id}', 'Vehicle@calibration_new')->middleware('checkauth');
Route::post('update-vehicle-calibration', 'Vehicle@saveVehicleCalibration')->middleware('checkauth');
Route::post('getEngineCap', 'Vehicle@getEngineCap')->middleware('checkauth');
 
// Core data
Route::get('data-core-list', 'Data@coreList')->middleware('checkauth');
Route::post('create-core-data', 'Data@createCoreNew')->middleware('checkauth');
Route::get('add-core', 'Data@addNewCore')->middleware('checkauth');
Route::get('edit-core/{id}', 'Data@editCore')->middleware('checkauth');
Route::post('edit-core-data-save', 'Data@editCoreData')->middleware('checkauth');

// Value Data
Route::get('data-value-list', 'Data@valueList')->middleware('checkauth');
Route::get('add-value', 'Data@addNewValue')->middleware('checkauth');
Route::post('create-value-data', 'Data@createValueNew')->middleware('checkauth');
Route::get('edit-value/{id}', 'Data@editValue')->middleware('checkauth');
Route::post('edit-value-data-save', 'Data@editValueData')->middleware('checkauth');

// Description Data
Route::get('desc-list', 'Settings@descList')->middleware('checkauth');
Route::get('edit-desc/{id}', 'Settings@editDesc')->middleware('checkauth');
Route::post('edit-desc-save', 'Settings@editDescList')->middleware('checkauth');
Route::get('add-desc', 'Settings@addNewDesc')->middleware('checkauth');
Route::post('create-desc-type', 'Settings@createDescType')->middleware('checkauth');

// Device Pending
Route::get('device-pending', 'Device@devicePending')->middleware('checkauth');
Route::post('api-device-pending', 'Device@apiDevicePending')->middleware('checkauth');
Route::post('api-device-pending_delete', 'Device@apiDevicePending_delete')->middleware('checkauth');

// Map
Route::get('map', 'Map@index')->middleware('checkauth');

// API Key
Route::get('apiKey', 'ApiKey@index');
Route::get('add-apiKey', 'ApiKey@add_apiKey')->middleware('checkauth');
Route::get('edit-apiKey/{id}', 'ApiKey@edit_apiKey')->middleware('checkauth');
Route::post('edit-apiKey-save', 'ApiKey@save_apiKey')->middleware('checkauth');
Route::get('apiKey-list', 'ApiKey@list_apikey')->middleware('checkauth');
Route::post('save-apiKey', 'ApiKey@save_apiKey')->middleware('checkauth');

// Services
Route::get('services', 'Services@index')->middleware('checkauth');
Route::post('service-record', 'Services@add')->middleware('checkauth');
Route::post('service-add-sub', 'Services@serviceAddSub')->middleware('checkauth');
Route::post('getServiceLoad', 'Services@getServiceLoad')->middleware('checkauth');

// Actions
Route::get('actions', 'Actions@index')->middleware('checkauth');
Route::post('getActionLoad', 'Actions@getActionLoad')->middleware('checkauth');
Route::post('ask-assistance-save', 'Actions@askAssistanceSave')->middleware('checkauth');
Route::post('track-assistance','Actions@trackAssistance')->middleware('checkauth');
Route::post('change-assistance-status','Actions@changeAssistanceStatus')->middleware('checkauth');

// Summary
Route::get('summary', 'Summary@index')->middleware('checkauth');
Route::get('list_html', 'Summary@getSummaryLoad')->middleware('checkauth');
Route::post('getSummaryLoad', 'Summary@getSummaryLoad')->middleware('checkauth');
//Route::post('ask-assistance-save', 'Actions@askAssistanceSave')->middleware('checkauth');
//Route::post('track-assistance','Actions@trackAssistance')->middleware('checkauth');
//Route::post('change-assistance-status','Actions@changeAssistanceStatus')->middleware('checkauth');

// Drive
Route::get('drive', 'Drive@index')->middleware('checkauth');
Route::post('getDriveLoad', 'Drive@getDriveLoad')->middleware('checkauth');
Route::get('google_map_key', 'Drive@google_map_key')->middleware('checkauth');
Route::post('update_gkey', 'Drive@update_gkey')->middleware('checkauth');

// Drive SD
Route::get('drivesd', 'Drivesd@index')->middleware('checkauth');
Route::post('getDriveSdLoad', 'Drivesd@getSdDriveLoad')->middleware('checkauth');

// Eva
Route::get('eva', 'Eva@index')->middleware('checkauth');
Route::get('eva_can', 'Eva@eva_can')->middleware('checkauth');
Route::get('getVehicle', 'Eva@getVehicle')->middleware('checkauth');
Route::get('getVinNo', 'Eva@getVinNo')->middleware('checkauth');
Route::get('getTestType', 'Eva@getTestType')->middleware('checkauth'); 
Route::post('getEvaGraph','Eva@getEvaGraph');
Route::post('getportdata','Eva@getportdata');
Route::post('downloadCsvEva','Eva@downloadCsvEva')->middleware('checkauth');
Route::get('gettestresult', 'Eva@getUservehicleDetails')->middleware('checkauth');


// Activation code
Route::get('activation-code', 'Settings@activeList')->middleware('checkauth');
Route::get('edit-active/{id}', 'Settings@editActive')->middleware('checkauth');
Route::post('edit-active-save', 'Settings@editActiveSave')->middleware('checkauth');
Route::get('add-Active', 'Settings@addNewActive')->middleware('checkauth');
Route::post('create-active-type', 'Settings@createActiveType')->middleware('checkauth');

// Glossary
Route::get('glossary-list', 'Glossary@index')->middleware('checkauth');
Route::post('update-glossary', 'Glossary@updateGlossary')->middleware('checkauth');

// Staticstics
Route::get('statistics', 'Statistics@index')->middleware('checkauth');
Route::post('loadDataStat', 'Statistics@getStaticticsLoadCountDays')->middleware('checkauth');
Route::post('loadDataStatRangeDays', 'Statistics@getStaticticsLoadRangeDays')->middleware('checkauth');
Route::post('loadgraph', 'Statistics@loadgraph')->middleware('checkauth');

// Report
Route::get('reports', 'Reports@index')->middleware('checkauth');
Route::post('loadReport', 'Reports@loadReport')->middleware('checkauth');
Route::get('calibReport/{id}/{ticketid}', 'Reports@generateCalibrationReport')->middleware('checkauth');
Route::get('testreport/{id}/{ticketid}', 'Reports@generateMoniteringReport')->middleware('checkauth');
Route::get('tvs', 'Reports@tvs_fpdf')->middleware('checkauth');
Route::get('tvsCronReport/{did}/{date}/{status}', 'Reports@getTvsCronReport')->middleware('checkauth');
Route::post('file-upload-report', 'Reports@uploadReportFile')->middleware('checkauth');
Route::post('loadReportInactive', 'Reports@loadReportInactive')->middleware('checkauth');
Route::post('activate-reports', 'Reports@activateReports')->middleware('checkauth');
Route::post('delete-reports', 'Reports@deleteReports')->middleware('checkauth');
Route::post('loadReportActive', 'Reports@loadReportActive')->middleware('checkauth');
Route::post('loadReportActive', 'Reports@loadReportActive')->middleware('checkauth');
Route::post('loadReportActive', 'Reports@loadReportActive')->middleware('checkauth');
Route::post('loadHeatGraph1', 'Reports@loadHeatGraph1')->middleware('checkauth');

// Remote Diagnostic
Route::get('remote-diagnostics', 'Diagnostics@index')->middleware('checkauth');
Route::post('getDiagnoLoad', 'Diagnostics@getDiagnoLoad')->middleware('checkauth');
Route::post('getgraph', 'Diagnostics@getgraph')->middleware('checkauth');

// EDAS
Route::get('edas', 'Edas@index')->middleware('checkauth');
Route::post('getEdasLoad', 'Edas@getEdasLoad')->middleware('checkauth');
Route::post('getEdasGraph', 'Edas@getEdasGraph')->middleware('checkauth');
Route::post('getAlertCount', 'Edas@getAlertCount')->middleware('checkauth');
Route::post('getUservehicleDetails', 'Edas@getUservehicleDetails')->middleware('checkauth');

// Crons
Route::get('daily-report-tvs', 'Cronjob@generateDailyReport')->middleware('checkauth');

/* ------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------BACKEND PROCESS END'S HERE------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------*/

/* ------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------API START'S HERE-----------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------*/
//version v1
// User Data
Route::get('v1/testloginapi','Apiv1@test_login');
// drive Range
Route::get('v1/drived_range_api','Apiv1@drived_range');
// device Location
Route::get('v1/device_location_api','Apiv1@device_location');
// Vehicle
Route::get('v1/vehicle_api','Apiv1@vehicle');
// moniter Data
Route::get('v1/monitor_data_api','Apiv1@monitor_data');
// vehicle Specification
Route::get('v1/vehicle_specification','Apiv1@vehicle_spec');
// vehicle Model
Route::get('v1/vehicle_model','Apiv1@vehicle_model');
// Statastics
Route::get('v1/statistics','Apiv1@statistics');
// Statastics Main
Route::get('v1/statistics_main','Apiv1@statistics_main');
// Drive Score
Route::get('v1/drivescrore_sd','Apiv1@drivescrore_sd');
// Drived Loaction
Route::get('v1/device_location_drive','Apiv1@device_location_drive');
// Activation Code
Route::get('v1/valcode','Apiv1@valcode');
// monitor flag
Route::get('v1/monitor_flag','Apiv1@monitor_flag');


// API_device_v1
// monitor flag
Route::get('v1/get_validsuberm_1','Api_device_v1@get_validsuberm_1');
// Get_device_config
Route::get('v1/w4/get_device_config','Api_device_v1@get_device_config');
// Get_device_config_ERM
Route::get('v1/get_device_config_ERM','Api_device_v1@get_device_config_ERM');
// Get_device_config_ERM
Route::get('v1/w2/get_device_config','Api_device_v1@get_device_config_w2');
// Get_valid_sub
Route::get('v1/w4/get_valid_sub','Api_device_v1@get_valid_sub');
// Get_valid_sub_4
Route::get('v1/w2/get_valid_sub','Api_device_v1@get_valid_sub_2w');
// Get_valid_sub_4
Route::get('v1/get_valid_sub_ERM','Api_device_v1@get_valid_sub_ERM');
// Device_event_4w
Route::post('v1/w4/device_event','Api_device_v1@device_event');
// Device_event_2w
Route::post('v1/w2/device_event','Api_device_v1@device_event_2w');
// Device_data_upload_erm
Route::get('v1/devdatauploadERM','Api_device_v1@devdatauploadERM');
// Device_data_upload_erm
Route::get('v1/devdatauploadERM_1','Api_device_v1@devdatauploadERM_1');
// Device_raw_data_upload_4w
Route::get('v1/w4/devrawdataupload','Api_device_v1@devrawdataupload');
// Device_raw_data_upload_4w
Route::get('v1/w2/devrawdataupload','Api_device_v1@devrawdataupload_2w');
// Device_data_upload
Route::post('v1/w4/devdataupload','Api_device_v1@devdataupload');
// Device_data_upload
Route::post('v1/w2/devdataupload','Api_device_v1@devdataupload2w');
// Device_data_upload
Route::post('v1/w2/devdataupload_tvs','Api_device_v1@devdataupload2w_tvs');
// set_device_dtc
Route::post('v1/set_device_dtc','Api_device_v1@set_device_dtc');
// get_cmdres
Route::post('v1/get_cmdres','Api_device_v1@get_cmdres');
// fileupload_fleet
Route::post('v1/file_upload_fleet','Api_device_v1@file_upload_fleet');

Route::get('v1/mail_test','Api_device_v1@mail_test');


/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************* APIs for iTriangle Start ***************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/
// Get_device_config
Route::get('v1/itriangle/w4/get_device_config','Api_itriangle@get_device_config');
// Get_valid_sub
Route::get('v1/itriangle/w4/get_valid_sub','Api_itriangle@get_valid_sub');
// Device_event_4w
Route::post('v1/itriangle/w4/device_event','Api_itriangle@device_event');
// Device_raw_data_upload_4w
Route::post('v1/itriangle/w4/devrawdataupload','Api_itriangle@devrawdataupload');
// Device_data_upload
Route::post('v1/itriangle/w4/devdataupload','Api_itriangle@devdataupload');
// set_device_dtc
Route::post('v1/itriangle/set_device_dtc','Api_itriangle@set_device_dtc');
// get_cmdres 
Route::get('v1/itriangle/get_cmdres','Api_itriangle@get_cmdres'); 
// registration
Route::get('v1/itriangle/profile','Api_itriangle@profile'); 
// Vehicle registration
Route::post('v1/itriangle/vehicle','Api_itriangle@vehicle');
/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************** APIs for iTriangle End ****************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------*/
/**************************************************** APIs for Cai Start ******************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/
Route::get('v1/cai/checklogin','Api_cai@check_login');
Route::post('v2/cai/checklogin','Api_cai@checklogin');
Route::get('v1/cai/profile','Api_cai@profile');
Route::get('v1/cai/statistics','Api_cai@statistics');
Route::get('v1/cai/getcalvalues','Api_cai@getcalvalues');
Route::get('v1/cai/statistics_main','Api_cai@statistics_main');
Route::get('v1/cai/getdrive','Api_cai@getdrive');
Route::get('v1/cai/drivescrore','Api_cai@drivescore');
Route::get('v1/cai/veh_model','Api_cai@vehicle_model');
Route::get('v1/cai/veh_spec','Api_cai@vehicle_spec');
Route::get('v1/cai/vehicle','Api_cai@vehicle');
Route::get('v1/cai/devicekey','Api_cai@devicekey');
Route::get('v1/cai/forgetpass','Api_cai@forget_password');
Route::get('v1/cai/calibration', 'Api_cai@calibration');
Route::get('v1/cai/valcode','Api_cai@valcode');
Route::post('v1/cai/fileupload','Api_cai@file_upload');
Route::get('v1/cai/monitor_flag','Api_cai@monitor_flag');
Route::get('v1/cai/statistics_drivedata','Api_cai@statistics_drivedata');
Route::get('v1/cai/driveScore_upd','Api_cai@driveScore_upd');
Route::get('v1/cai/service_upd','Api_cai@service_upd');
Route::get('v1/cai/getprofile','Api_cai@get_profile');
Route::get('v1/cai/getactivedtc','Api_cai@getactivedtc');


Route::get('v1/cai/bluetoothkey','Api_cai@bluetoothkey');
// Route::get('v1/cai/device_event','Api_cai@device_event');
// Route::get('v1/cai/device_location_api','Api_cai@device_location');
Route::get('v1/cai/device_location_drive','Api_cai@device_location_drive');
// Route::get('v1/cai/device_location_get','Api_cai@device_location');
// Route::get('v1/cai/device_location','Api_cai@device_event');
// Route::post('v1/cai/device_propend','Api_cai@file_upload_fleet');
// Route::get('v1/cai/deviceupload','Api_cai@file_upload_fleet');
// Route::get('v1/cai/get_valid_sub','Api_cai@get_valid_sub');
// Route::get('v1/cai/get_device_config','Api_cai@get_device_config');
// Route::get('v1/cai/drivescrore_sd','Api_cai@drivescrore_sd');
// Route::get('v1/cai/monitor_data','Api_cai@monitor_data');
// Route::get('v1/cai/set_device_dtc','Api_cai@set_device_dtc');
// Route::get('v1/cai/stati_data','Api_cai@statistics');
// Route::get('v1/cai/calibration_data', 'Api_cai@monitor_data');
// Route::get('v1/cai/getdevconfig','Api_cai@get_device_config');
// Route::get('v1/cai/lastDriveReport','Api_cai@monitor_data');
Route::post('v1/cai/devicekey','Api_cai@devicekey');
/*----------------------------------------------------------------------------------------------------------------------------*/
/***************************************************** APIs for Cai End *******************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************** APIs for Cailite Start ****************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/
Route::get('v1/cailite/checklogin','Api_CaiLite@check_login');
Route::get('v1/cailite/profile','Api_CaiLite@profile');
Route::get('v1/cailite/statistics','Api_CaiLite@statistics');
Route::get('v1/cailite/getcalvalues','Api_CaiLite@getcalvalues');
Route::get('v1/cailite/statistics_main','Api_CaiLite@statistics_main');
Route::get('v1/cailite/getlastloc','Api_CaiLite@getlastloc');
Route::get('v1/cailite/drivescrore','Api_CaiLite@drivescore');
Route::get('v1/cailite/veh_model','Api_CaiLite@vehicle_model');
Route::get('v1/cailite/veh_spec','Api_CaiLite@vehicle_spec');
Route::get('v1/cailite/vehicle','Api_CaiLite@vehicle');
Route::get('v1/cailite/devicekey','Api_CaiLite@devicekey');
Route::get('v1/cailite/forgetpass','Api_CaiLite@forget_password');
Route::get('v1/cailite/calibration', 'Api_CaiLite@calibration');
Route::get('v1/cailite/valcode','Api_CaiLite@valcode');
Route::post('v1/cailite/fileupload','Api_CaiLite@file_upload');
Route::get('v1/cailite/monitor_flag','Api_CaiLite@monitor_flag');
Route::get('v1/cailite/statistics_drivedata','Api_CaiLite@statistics_drivedata');
Route::get('v1/cailite/driveScore_upd','Api_CaiLite@driveScore_upd');
Route::get('v1/cailite/service_upd','Api_CaiLite@service_upd');
Route::get('v1/cailite/service_data','Api_CaiLite@service_data');
Route::get('v1/cailite/getprofile','Api_CaiLite@get_profile');
Route::get('v1/cailite/bluetoothkey','Api_CaiLite@bluetoothkey');
Route::get('v1/cailite/device_location_drive','Api_CaiLite@device_location_drive');
Route::post('v1/cailite/devicekey','Api_CaiLite@devicekey');
Route::get('v1/cailite/statisticsall','Api_CaiLite@statisticsall');
Route::get('v1/cailite/statisticsbyrange','Api_CaiLite@statisticsByDriveIDRange');
Route::get('v1/cailite/monitor-data','Api_CaiLite@monitor_data');
Route::get('v1/cailite/monitor-data-html','Api_CaiLite@monitor_data_html');
Route::get('v1/cailite/getactivedtc','Api_CaiLite@getactivedtc');
Route::get('v1/cailite/getvehicledetails','Api_CaiLite@getUserVehicleDetails');
/*----------------------------------------------------------------------------------------------------------------------------*/
/*************************************************** APIs for Cailite End *****************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************ APIs for Bike Intell Start **************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/
Route::get('v1/bi/checklogin','Api_bi@check_login');
Route::get('v1/bi/profile','Api_bi@profile');
Route::get('v1/bi/statistics','Api_bi@statistics');
Route::get('v1/bi/getcalvalues','Api_bi@getcalvalues');
Route::get('v1/bi/statistics_main','Api_bi@statistics_main');
Route::get('v1/bi/getdrive','Api_bi@getdrive');
Route::get('v1/bi/drivescrore','Api_bi@drivescore');
Route::get('v1/bi/veh_model','Api_bi@vehicle_model');
Route::get('v1/bi/veh_spec','Api_bi@vehicle_spec');
Route::get('v1/bi/vehicle','Api_bi@vehicle');
Route::get('v1/bi/devicekey','Api_bi@devicekey');
Route::get('v1/bi/forgetpass','Api_bi@forget_password');
Route::get('v1/bi/calibration', 'Api_bi@calibration');
Route::get('v1/bi/valcode','Api_bi@valcode');
Route::post('v1/bi/fileupload','Api_bi@file_upload');
Route::get('v1/bi/monitor_flag','Api_bi@monitor_flag');
Route::get('v1/bi/statistics_drivedata','Api_bi@statistics_drivedata');
Route::get('v1/bi/driveScore_upd','Api_bi@driveScore_upd');
Route::post('v1/bi/service_upd','Api_bi@service_upd');
Route::get('v1/bi/getprofile','Api_bi@get_profile');
Route::get('v1/bi/bluetoothkey','Api_bi@bluetoothkey');
Route::get('v1/bi/device_location_drive','Api_bi@device_location_drive');
Route::post('v1/bi/devicekey','Api_bi@devicekey');
/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************* APIs for Bike Intell End ***************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/


Route::get('apiLogin','Api@login');
Route::post('apiAddUser','Api@addUser');
Route::get('check-valid-sub','Api@checkValidSub');
Route::get('monitor-flag','Api@monitorFlag');

/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************ APIs for Linux Device Start *************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/
Route::get('v1/linux/get_devstatus', 'Api_Linux@get_devstatus');
Route::get('v1/linux/get_devconfig2w', 'Api_Linux@get_devconfig2w');
Route::get('v1/linux/get_cmdupdate', 'Api_Linux@get_cmdupdate');
Route::get('v1/linux/get_frmupdate', 'Api_Linux@get_firmwareupdate');
Route::get('v1/linux/get_appupdate', 'Api_Linux@get_appupdate');
Route::get('v1/linux/get_bt1devconf', 'Api_Linux@get_bt1devconfig');
Route::post('v1/linux/post_cmdres', 'Api_Linux@cmdres');
Route::post('v1/linux/post_frmwrupdres', 'Api_Linux@frmwrupdres');
Route::post('v1/linux/post_appupdres', 'Api_Linux@appupdres');
Route::post('v1/linux/post_bt1updres', 'Api_Linux@bt1updres');
Route::post('v1/linux/post_posdata', 'Api_Linux@posdataupload');
Route::post('v1/linux/post_rawdata', 'Api_Linux@rawdataupload');
Route::post('v1/linux/post_eoddata', 'Api_Linux@eodDataUpload');
Route::post('v1/linux/post_bt1data', 'Api_Linux@bt1dataupload');
Route::post('v1/linux/post_dtcdata', 'Api_Linux@dtcdataupload');
/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************* APIs for Linux Device End **************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************ APIs for Sigfox Device Start *************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/
Route::get('v1/tmd102bt/monitor_flag', 'Api_Sigfox@monitor_flag');
Route::get('v1/tmd102bt/get_frmupdate', 'Api_Sigfox@get_firmwareupdate');
Route::post('v1/tmd102bt/post_frmwrupdres', 'Api_Sigfox@frmwrupdres');
Route::get('v1/tmd102bt/get_cmdupdate', 'Api_Sigfox@get_cmdupdate');
Route::post('v1/tmd102bt/post_cmdres', 'Api_Sigfox@cmdres'); 
Route::get('v1/tmd102bt/get_devconfig', 'Api_Sigfox@get_devconfig');
Route::post('v1/tmd102bt/post_rawdata', 'Api_Sigfox@rawdataupload');
Route::post('v1/tmd102bt/post_eoddata', 'Api_Sigfox@eodDataUpload');
Route::get('v1/tmd102bt/getlastloc', 'Api_Sigfox@getlastloc'); 
Route::get('v1/tmd102bt/device_location_drive', 'Api_Sigfox@device_location_drive');
Route::post('v1/tmd102bt/device_event', 'Api_Sigfox@device_event');
Route::post('v1/tmd102bt/post_dtcdata', 'Api_Sigfox@dtcdataupload');

/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************* APIs for Sigfox Device End **************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------*/
/********************************************** APIs for WIPRO EVA Fleet Start ************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/
Route::get('v1/regdev', 'Api_Wipro@reg_device');
Route::get('v1/addveh', 'Api_Wipro@add_vehicle');
Route::get('v1/fileupl', 'Api_Wipro@file_upload');
Route::get('v1/postres', 'Api_Wipro@post_results');
/*----------------------------------------------------------------------------------------------------------------------------*/
/*********************************************** APIs for WIPRO EVA Fleet End *************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------------------*/
/********************************************** APIs for Mtap Start ************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/

Route::get('v1/mtap/drivedata','Api_Mtap@monitor_data');

/*----------------------------------------------------------------------------------------------------------------------------*/
/********************************************** APIs for Mtap End ************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************ APIs for TVSEOL Start *************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/
Route::get('v1/tvseol/checklogin','Api_TVSEOL@check_login');
Route::get('v1/tvseol/profile','Api_TVSEOL@profile');
Route::get('v1/tvseol/valcode','Api_TVSEOL@valcode');
Route::get('v1/tvseol/statistics','Api_TVSEOL@statistics');
Route::get('v1/tvseol/getcalvalues','Api_TVSEOL@getcalvalues');
Route::get('v1/tvseol/statistics_main','Api_TVSEOL@statistics_main');
Route::get('v1/tvseol/getlastloc','Api_TVSEOL@getlastloc');
Route::get('v1/tvseol/drivescrore','Api_TVSEOL@drivescore');
Route::get('v1/tvseol/veh_model','Api_TVSEOL@vehicle_model');
Route::get('v1/tvseol/veh_spec','Api_TVSEOL@vehicle_spec');
Route::get('v1/tvseol/vehicle','Api_TVSEOL@vehicle');
Route::get('v1/tvseol/devicekey','Api_TVSEOL@devicekey');
Route::get('v1/tvseol/forgetpass','Api_TVSEOL@forget_password');
Route::get('v1/tvseol/getallvehicle','Api_TVSEOL@getAllVehicleDetails');
Route::get('v1/tvseol/getalloperator','Api_TVSEOL@getAllOperator');
Route::post('v1/tvseol/file_upload_eol','Api_TVSEOL@file_upload_eol');
Route::post('v1/tvseol/file_upload_other','Api_TVSEOL@file_upload_other');
// Route::get('v1/tvseol/enginecal-reset-password', 'Users@passwordreset');
// // Route::post('reset_success', 'Users@reset_success');
// Route::post('v1/tvseol/reset-password-save', 'Users@resetpasswordsave');

/*----------------------------------------------------------------------------------------------------------------------------*/
/************************************************ APIs for TVSEOL END *************************************************/
/*----------------------------------------------------------------------------------------------------------------------------*/

/* ----------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------API END'S HERE-------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------*/