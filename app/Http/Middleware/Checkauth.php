<?php

namespace App\Http\Middleware;

use Closure;

class Checkauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty(session('userdata'))){
            // return redirect('login');
            echo redirect('login');
            die;
        }
        return $next($request);
    }
}
