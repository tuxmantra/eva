<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newuser;
use Illuminate\Support\Arr;
// use Mail;


class Api_itriangle extends Controller
{
	public function profile(Request $request)
  	{
  		$header = getallheaders();
  		// print_r($header);die;
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
	 		if(!empty($request->data))
	        {	
	           	$json     = $request->data;
				$obj      = json_decode($json,true);	
				$deviceID = $obj['devID'];
				if($deviceID != '')
				{
					$devInfo  = Fetch::getdevice($deviceID);
					if(empty($devInfo))
					{
						$postValD              = array();
						$postValD['device_id'] = $deviceID;
						$postValD['name']      = $deviceID;
						$postValD['descrp']    = $deviceID;
						$postValD['partner']   = 2;		
						$postValD['status']    = 1;
						$postValD['ad_by']     = 2;
						$postValD['ad_dt']     = date('Y-m-d H:i:s');
						$postValD['devtype']   = 2;
						$postValD['dev_cmds']   = "";
						// $postValD['phn_model'] = $obj['user']['phnName'];
						// $postValD['bt_mac_id'] = $obj['user']['btMacId'];
						$update                = Fetch::insert_device($postValD);
						// print_r($update);die;
						if($update)
						{
							$devInfo  = Fetch::getdevice($deviceID);

							//Device Addition Complete. Now Add User.
							
							$drvInfo              = array();
							// $drvInfo['device']    = $devInfo[0]->id;
							$drvInfo['uname']     = $deviceID; 
							$drvInfo['utype']     = 18;
							$drvInfo['ucategory'] = 4;
							$drvInfo['ulocation'] = "Bangalore";
							$drvInfo['umob']      = "1234567890";
							$drvInfo['uemail']    = $deviceID."@enginecal.com";
							$drvInfo['upwd']      = md5('iTri123s');
							$drvInfo['ustatus']   = 1;
							$drvInfo['partner']   = 2;
							$drvInfo['device']	  = $update[0]->id;

							$saveData             = Fetch::save_user($drvInfo);
							if($saveData)
							{
								
								$subject = "Itriangle new device registered";
								$message1 = "<table>
												<tr><td>New device registered from Itriangle</td></tr>
												<tr><td>Details are given below : </td></tr>
												<tr><td>-----------------------------------</td></tr>
												<tr><td>Device ID : ".$deviceID." </td></tr>
												
												<tr><td>-----------------------------------</td></tr>
											</table> \n";
								
								$from     = 'support@enginecal.com';
					            $headers  = 'MIME-Version: 1.0' . "\r\n";
					            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					             
					            // Create email headers
					            $headers .= 'From: '.$from."\r\n".
					                		'Reply-To: '.$from."\r\n" .
					                		'X-Mailer: PHP/' . phpversion();
					            
					            if(mail('support@enginecal.com',$subject,$message1,$headers))
					            {
									// $arrayData['success']   = TRUE;
									// $arrayData['deviceKey'] = $devKey;
									// $arrayData['mail']      = "Key sent successfully!";
									// echo "Mail sent successfully";
								}
								else
								{
									// print_r(error_get_last());
								}

								

					            // Add Vehicle
								$postValVeh['device'] = $devInfo[0]->id;
								$update               = Fetch::insert_vehicle($postValVeh);
								if($update)
								{
									$arrayData['success'] = TRUE;
									$arrayData['message'] = "Registration done successfully!";
								}
								else

								{
									$arrayData['success']      = FALSE;
									$arrayData['errorCode']    = "1003";
									$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
								}
							}
							else
							{
								$arrayData['success']      = FALSE;
								$arrayData['errorCode']    = "1003";
								$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
							}
							
						}
						else
						{
							$arrayData['success']      = FALSE;
							$arrayData['errorCode']    = "1003";
							$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
						}
					}
					else
					{
						$arrayData['success']      = FALSE;
			        	$arrayData['errorCode']    = "1002";
			        	$arrayData['errorMessage'] = "Device id already exist";
					}
	      	   	}
	      	   	else
		        {
		        	$arrayData['success']      = FALSE;
		        	$arrayData['errorCode']    = "1001";
		        	$arrayData['errorMessage'] = "Invalid Input!";
		        }
	      	}
	      	else
	      	{
	      		$arrayData['success']      = FALSE;
	      		$arrayData['errorCode']    = "1001";
	      		$arrayData['errorMessage'] = "Invalid Input!";
	      	}
	    }
    	$resultVal = json_encode($arrayData);
    	echo $resultVal;
  	}

  	public function vehicle(Request $request)
	{
		// echo "I m here";die; 
		$header = getallheaders();
  		if(empty($header['Authentication']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{	
	     	if(!empty(file_get_contents("php://input")))
	        {
	        	$dat      = file_get_contents("php://input");
		     	$data     = json_decode($dat, TRUE);
				$deviceID = $data['deviceid'];
				$devInfo  = Fetch::getdevice($deviceID);
				// print_r($devInfo);die;
				$errorMF  = "";
				if(!empty($devInfo))
				{
				    $did      = $devInfo[0]->id;
				    $prof     = $data;
				    $drvInfo  = Fetch::getVehicleById($did);
				    $settInfo = Fetch::getVehicleSetting();
				    // print_r($devInfo[0]->device_id);die;
				    $mfd = explode(';',$settInfo->manufacturer);
				    $mfd = array_map('strtolower',$mfd);
				    $cap = explode(';',$settInfo->engine_capacity);
				    $cap = array_map('strtolower',$cap);
				    $drvInfo = ($drvInfo != "") ? $drvInfo : array();
				    $drvInfo = array();
				   
				    $drvInfo['device']   = $devInfo[0]->id;
				    // $drvInfo['reg_no']   = $prof['veh_registration'];
				    $drvInfo['manufact'] = "";
				    $drvInfo['status']   = 1;
				    $myMfd               = strtolower(trim($prof['veh_manufacturer']));
				    // print_r($mfd);
				    // print_r($myMfd);die;
				    //Checek if valid manufacturer
				    if(in_array($myMfd,$mfd))
				    {
					    $indexM              = array_search($myMfd,$mfd);  
				        $drvInfo['manufact'] = $indexM;

				        $modelList = array();
				    	$varlList  = array();
				        $resM      = Fetch::getVehicleInfoDataById($indexM);
					    if(count($resM) >= 1)
					    {
					        //$arrayData['varients'] = explode(';',$resM[0]['var']);
					        $modelList = explode(';',$resM[0]->model);
					        $varlList  = explode(';',$resM[0]->var);
					    }

					    //Checek if valid model
					    if(in_array($prof['veh_model'],$modelList))
					    {
					        $indexMod           = array_search($prof['veh_model'],$modelList);  
					        $drvInfo['modelid'] = $indexMod;
					        $drvInfo['model']   = $indexMod;

					        //Checek if valid varient
					        if(in_array($prof['veh_varient'],$varlList))
					        {
					            $indexVar             = array_search($prof['veh_varient'],$varlList); 
					            $drvInfo['varientid'] = $indexVar;
					            $drvInfo['varient']   = $indexVar;

					            //Check for valid engine capacity
					            $myCap                = strtolower(trim($prof['engine_capacity']));
							    if(in_array($myCap,$cap))
							    {
							        $indexM             = array_search($myCap,$cap);  
							        $drvInfo['eng_cap'] = $indexM;

							        $drvInfo['fuel_tpe']        = (trim($prof['fuel_type']) == "Petrol") ? '1' : '2';
							        $drvInfo['manufact_yr']     = $prof['mfg_year'];
							        // $drvInfo['travel_purchase'] = $prof['odo'];
							        if(array_key_exists('veh_transmission', $prof))
							        {
							        	$trans_type = $prof['veh_transmission'];
										if($trans_type == 2)
										{
											$drvInfo['trans_type'] = "Automatic";
										}
										else
										{
											$drvInfo['trans_type'] = "Manual";
										}
							        }
							        else
							        {
							        	$drvInfo['trans_type'] = "Manual";
							        }
							        $drvInfo['purchase_dt']     = date('Y-m-d');
							        $drvInfo['last_serv_dt']    = date('Y-m-d');
							        $drvInfo['oil_dt']          = date('Y-m-d');
							        //to add last service kms in multiples of 10000
							        // $a = strlen($prof['odo']);
							        // if($a >= 5)
							        // {
							        // 	$val = round($prof['odo'],-($a - 1));
							        // }
							        // else
							        // {
							        //     $val = 0;
							        // }
							        // $drvInfo['travel_service'] = $val;
							        // $drvInfo['oil_dist']       = $val;
							    }
							    else
							    {
						            $errorMF .= $prof['engine_capacity'] . " engine capacity is not available";
						        }
					        }
					        else
					        {
					            $errorMF = $prof['veh_varient'] . " varient is not available";
					        }
					    }
				        else
				        {
				        	$errorMF = $prof['veh_model'] . " model is not available";
				        }
				    }
				    else
				    {
				        $errorMF = $prof['veh_manufacturer'] . " manufacturer is not available";
				    }
				    
		            if($errorMF != "")
		            {
					    $arrayData['success']      = FALSE;
					    $arrayData['errorCode']    = "1003";
					    $arrayData['errorMessage'] = "Input data Incorrect. $errorMF!";
				    }
		    		else
		    		{
		    			$setFields = array('device');
						$setVal    = array($did);
						$table     = "vehicles";
		    			$saveData  = Fetch::updateDataMultiple($table,$setFields,$setVal,$drvInfo);
				        if($saveData)
				        {	
				        	$setSpecs = Fetch::setvehSpec($drvInfo['manufact'],$drvInfo['model'],$drvInfo['fuel_tpe'],$drvInfo['varient'],$drvInfo['eng_cap'],$drvInfo['manufact_yr'],$did);
				        	if($setSpecs)
				        	{
				        		$arrayData['success'] = TRUE;
					        	$arrayData['message'] = "Data updated successfully!";
				        	}
				        	else
					        {
						        $arrayData['success']      = FALSE;
						        $arrayData['errorCode']    = "1004";
						        $arrayData['errorMessage'] = "Error in adding data. Try again Later!";
					        }
				        }
				        else
				        {
					        $arrayData['success']      = FALSE;
					        $arrayData['errorCode']    = "1003";
					        $arrayData['errorMessage'] = "Error in adding data. Data already available!";
				        }
					}
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1002";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
			    $arrayData['success']      = FALSE;
			    $arrayData['errorCode']    = "1001";
			    $arrayData['errorMessage'] = "Invalid Input!";
			}
		}
        echo json_encode($arrayData);
    }

	public function set_device_dtc(Request $request)
	{
		$header = getallheaders();
  		if(empty($header['Authorization']) ||  $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
		  	if(!empty(file_get_contents("php://input")))
	   		{

		       	 $datMe = file_get_contents("php://input");
		     	 $array    = json_decode($datMe, TRUE);
		       	 $deviceID = $array['devID'];
					
			    // get Device Data
			     $devInfo = Fetch::getdevice($deviceID);
			    if(!empty($devInfo))
			    {
			        $dtc = $array['dtcData'];
					if($dtc != "")
					{
						$dtc = 1;
					}
					else
					{
						$dtc = 0;
					}
					$dtcv = $array['dtcData'];
			        // print_r($devInfo);die;
			        // //To update the same row in the database
			        // $setFields = array('device_id');
			        // $setVal = array($deviceID);
			        $resultC = Fetch::updatedeviceDtcv($deviceID,$dtcv,$dtc);
			        if($resultC)
			        {
			            // Data Uploaded successfully
			            $arrayData['success'] = TRUE;
			            $arrayData['dtc'] = "DTC Updated!";
			        }
			        else
			        {
			            // Data failed to update
			            $arrayData['success'] = FALSE;
			            $arrayData['errorCode'] = "1004";
			            $arrayData['errorMessage'] = "DTC Update Failed!";
			        }
				}
				else
				{
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
	       	}
	       	else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
			}
		}
		echo json_encode($arrayData);
    }

	public function devrawdataupload(Request $request)
	{
		$header = getallheaders();
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
			if(!empty(file_get_contents("php://input")))
	       	{    
	       		$datMe = file_get_contents("php://input");
	     		$array     = json_decode($datMe, TRUE);
	       		$logFilePath = getcwd();
				
	       		
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			    	    $pathInfo = getcwd();
						// echo "datme = $datMe \n";
							$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/devrawdataupload4w_back.php '$datMe'";
						// $cmd.=" ";
						// $cmd.="'$datMe'";

						exec($cmd . " > /dev/null &");
						// print_r($cmd);die;
						
						$arrayData['success'] = TRUE;
						$arrayData['status'] = "File Uploaded and database updating!";
			    }
			    else
				{
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
				// http_response_code(400);
			}
		}
		echo json_encode($arrayData);
	}

	public function devdataupload(Request $request)
	{
		$header = getallheaders();
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
		  	if(!empty(file_get_contents("php://input")))
	       	{    
	       		$datMe = file_get_contents("php://input");
	     		$array     = json_decode($datMe, TRUE);
	       		$logFilePath = getcwd();
				
	       		
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			    	    $pathInfo = getcwd();
						// echo "datme = $datMe \n";
							$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-res4w_itriangle.php '$datMe'";
						$cmd.=" ";
						$cmd.="'$datMe'";

						exec($cmd . " > /dev/null &");
						// print_r($cmd);die;
						
						$arrayData['success'] = TRUE;
						$arrayData['status'] = "File Uploaded and database updating!";
			    }
			    else
				{
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
				// http_response_code(400);
			}
		}
		echo json_encode($arrayData);
	}

 	public function device_event(Request $request)
 	{
 		
 		$header = getallheaders();
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{	// print_r(file_get_contents("php://input"));die;
 		  	if(!empty(file_get_contents("php://input")))
           	{
           		$datMe = file_get_contents("php://input");
         		$array     = json_decode($datMe, TRUE);
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			        $gpsData = array();
			        $gpsData['device_id'] = $array['devID'];
			        $gpsData['lat'] = $array['lat'];
			        $gpsData['lon'] = $array['lon'];
			        $gpsData['alt'] = $array['alt'];
			        if($array['ts'] == 0)
			        {
			            $array['ts'] = time();
			        }
			        else
			        {
			            $array['ts'] = $array['ts'] + 946684800; // To convert time from UTC since 2000 to UTC since 1970   
			        }
			        $gpsData['time_s']  = ($array['ts'] * 1000);
			        $gpsData['ad_dt']   = date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10));
			        $gpsData['event']   = $array['ev'];
			        $gpsData['nodrive'] = $array['dn'];
			        $gpsData['seqno']   = $array['s'];
			        // $gpsData['gps']   = $array['gps'];
			        // print_r($gpsData);die;
			        $saveData = Fetch::insertDbSingle_event($gpsData);
			        // echo $saveData;
			        if($saveData)
			        {
			            //Data Uploaded successfully
			            // Also Update "device" table to get the last know location of the device.
			            $setLoc=Fetch::setDeviceLastLocation($array['lat'],$array['lon'],$array['alt'],date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10)),$deviceID);
			            $arrayData['success'] = TRUE;
			            $arrayData['message'] = "Event updated successfully";
			        }
			        else
			        {
			            //Data failed to upload
			            $arrayData['success']      = FALSE;
			            $arrayData['errorCode']    = "1004";
			            $arrayData['errorMessage'] = "Data cannot be updated kindly try again later";
			        }
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}



		   	}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
			}
		}

		echo json_encode($arrayData);
    }


 	public function get_valid_sub(Request $request)
 	{
 		$header = getallheaders();
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
		  	if(!empty($request->data))
	 		{
	     		$obj      = json_decode($request->data, TRUE);
				$deviceID = $obj['devID'];
				$currVer  = $obj['currVer'];
				$devInfo = Fetch::getdeviceActStatus($deviceID);
				if($deviceID != "" && !empty($devInfo) && $devInfo[0]->status == 1)
				{
					$arrayData['success'] = TRUE;
					$arrayData['status'] = "1";
					if($devInfo[0]->expdt <= date('Y-m-d'))
					{
						$arrayData['status'] = "0";
					}
					$arrayData['subEnd'] = $devInfo[0]->expdt;
					$arrayData['cmds'] = $devInfo[0]->dev_cmds;
					$dev_cmds="";
					$update=Fetch::updateDeviceCmdCol($deviceID,$dev_cmds,$currVer);	
				}
				elseif($deviceID != "" && !empty($devInfo) && $devInfo[0]->status == 0)
				{
					$arrayData['success']      = FALSE;
					$arrayData['status']       = "0";
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Subscription expired!";
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
	     	}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
			}
		}
		echo json_encode($arrayData);

 	}

	public function get_cmdres(Request $request)
	{
		$header = getallheaders();
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
	 		if(!empty($request->data))
	        {
	     		$obj      = json_decode($request->data, TRUE);
				$deviceID = $obj['devID'];
				$res      = $obj['res'];
				$devInfo = Fetch::getdeviceActStatus($deviceID);
				if($deviceID != "" && !empty($devInfo) )
				{
					$arrayData['success'] = TRUE;
					$arrayData['msg']     = "Response Updated in DB!";
					$status=Fetch::updateDeviceCmdres($deviceID,$res);
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
		  	else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
			}
		}
			echo json_encode($arrayData);
		
	}

 	public function get_device_config(Request $request)
 	{
 		$header = getallheaders();
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$result['success']      = FALSE;
      		$result['errorCode']    = "1004";
      		$result['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
	  		if(!empty($request->data))
	        {
	        	// $this->updatelog("get_devconfig",$request->data);
	        	$obj      = json_decode($request->data, TRUE);
				$deviceID = $obj['devID'];

	        	// Get Device Data
				$devInfo = Fetch::devicestatus($deviceID);
				// print_r($devInfo);die;
				$did     = $devInfo[0]->id;
				
				// Get Vehicle details
				$vehi  = Fetch::getVehicleDataById($did);
				// print_r($vehi);die;
				$vType = $vehi->fuel_tpe;

				$encryptKey = rand(25, 300);
				if(!empty($devInfo))
				{
					$dataArr  = array();
					$data     = array();
					$dataArr  = json_decode($vehi->calval);
					$data[0]  = $vType;
					$data[1]  = $dataArr->Eng_eff_C;
					$data[2]  = $dataArr->Eng_Disp_C * 1000;
					if($dataArr->TC_Status_C == "Yes")
					{
						$data[3] = 1;
					}
					else
					{
						$data[3] = 0;
					}
					if($dataArr->Injection_Type_C == "Direct")
					{
						$data[4] = 1;
					}
					else
					{
						$data[4] = 0;
					}
					$data[5]   = round(($dataArr->Eff_RPM_Limit1_C / 50),0);
					$data[6]   = round(($dataArr->Eff_RPM_Limit2_C / 50),0);
					$data[7]   = round(($dataArr->Eff_RPM_Limit3_C / 50),0);
					$data[8]   = round(($dataArr->Eff_RPM_Limit4_C / 50),0);
					$data[9]   = round(($dataArr->Eff_RPM_Limit5_C / 50),0);

					//Equivalence Ratio
					$data[10] = (round(($dataArr->Eqratio_Eload_1_10_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_1_10_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_1_10_RPMLimit3_C * 200),0)) << 16) ;
					$data[11] = (round(($dataArr->Eqratio_Eload_1_10_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_1_10_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_11_20_RPMLimit1_C * 200),0)) << 16); 
					$data[12] = (round(($dataArr->Eqratio_Eload_11_20_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_11_20_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_11_20_RPMLimit4_C * 200),0)) << 16);
					$data[13] = (round(($dataArr->Eqratio_Eload_11_20_RPMLimit5_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit1_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit2_C * 200),0)) << 16);
					$data[14] = (round(($dataArr->Eqratio_Eload_21_30_RPMLimit3_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit4_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit5_C * 200),0)) << 16);	
					$data[15] = (round(($dataArr->Eqratio_Eload_31_40_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_31_40_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_31_40_RPMLimit3_C * 200),0)) << 16) ;
					$data[16] = (round(($dataArr->Eqratio_Eload_31_40_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_31_40_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_41_50_RPMLimit1_C * 200),0)) << 16);
					$data[17] = (round(($dataArr->Eqratio_Eload_41_50_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_41_50_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_41_50_RPMLimit4_C * 200),0)) << 16);
					$data[18] = (round(($dataArr->Eqratio_Eload_41_50_RPMLimit5_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit1_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit2_C * 200),0)) << 16);
					$data[19] = (round(($dataArr->Eqratio_Eload_51_60_RPMLimit3_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit4_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit5_C * 200),0)) << 16);
					$data[20] = (round(($dataArr->Eqratio_Eload_61_80_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_61_80_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_61_80_RPMLimit3_C * 200),0)) << 16);
					$data[21] = (round(($dataArr->Eqratio_Eload_61_80_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_61_80_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_81_90_RPMLimit1_C * 200),0)) << 16);
					$data[22] = (round(($dataArr->Eqratio_Eload_81_90_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_81_90_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_81_90_RPMLimit4_C * 200),0)) << 16);
					$data[23] = (round(($dataArr->Eqratio_Eload_81_90_RPMLimit5_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit1_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit2_C * 200),0)) << 16);
					$data[24] = (round(($dataArr->Eqratio_Eload_91_94_RPMLimit3_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit4_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit5_C * 200),0)) << 16);
					$data[25] = (round(($dataArr->Eqratio_Eload_95_98_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_95_98_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_95_98_RPMLimit3_C * 200),0)) << 16);
					$data[26] = (round(($dataArr->Eqratio_Eload_95_98_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_95_98_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_99_100_RPMLimit1_C * 200),0)) << 16);
					$data[27] = (round(($dataArr->Eqratio_Eload_99_100_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_99_100_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_99_100_RPMLimit4_C * 200),0)) << 16);
					$data[28] = (round(($dataArr->Eqratio_Eload_99_100_RPMLimit5_C * 200),0)) | ((round(($dataArr->Ovr_Run_Cold_EqRatio_C * 100),0)) << 8) | ((round(($dataArr->Ovr_Run_Hot_EqRatio_C * 100),0)) << 16);

					// Engine Efficiency
					$data[29] = ($dataArr->EngEff_Eload_1_10_RPMLimit1_C) | (($dataArr->EngEff_Eload_1_10_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_1_10_RPMLimit3_C) << 16) ;
					$data[30] = ($dataArr->EngEff_Eload_1_10_RPMLimit4_C) | (($dataArr->EngEff_Eload_1_10_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_11_20_RPMLimit1_C) << 16); 
					$data[31] = ($dataArr->EngEff_Eload_11_20_RPMLimit2_C) | (($dataArr->EngEff_Eload_11_20_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_11_20_RPMLimit4_C) << 16);
					$data[32] = ($dataArr->EngEff_Eload_11_20_RPMLimit5_C) | (($dataArr->EngEff_Eload_21_30_RPMLimit1_C) << 8) | (($dataArr->EngEff_Eload_21_30_RPMLimit2_C) << 16);
					$data[33] = ($dataArr->EngEff_Eload_21_30_RPMLimit3_C) | (($dataArr->EngEff_Eload_21_30_RPMLimit4_C) << 8) | (($dataArr->EngEff_Eload_21_30_RPMLimit5_C) << 16);	
					$data[34] = ($dataArr->EngEff_Eload_31_40_RPMLimit1_C) | (($dataArr->EngEff_Eload_31_40_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_31_40_RPMLimit3_C) << 16) ;
					$data[35] = ($dataArr->EngEff_Eload_31_40_RPMLimit4_C) | (($dataArr->EngEff_Eload_31_40_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_41_50_RPMLimit1_C) << 16); 
					$data[36] = ($dataArr->EngEff_Eload_41_50_RPMLimit2_C) | (($dataArr->EngEff_Eload_41_50_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_41_50_RPMLimit4_C) << 16);
					$data[37] = ($dataArr->EngEff_Eload_41_50_RPMLimit5_C) | (($dataArr->EngEff_Eload_51_60_RPMLimit1_C) << 8) | (($dataArr->EngEff_Eload_51_60_RPMLimit2_C) << 16);
					$data[38] = ($dataArr->EngEff_Eload_51_60_RPMLimit3_C) | (($dataArr->EngEff_Eload_51_60_RPMLimit4_C) << 8) | (($dataArr->EngEff_Eload_51_60_RPMLimit5_C) << 16);
					$data[39] = ($dataArr->EngEff_Eload_61_80_RPMLimit1_C) | (($dataArr->EngEff_Eload_61_80_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_61_80_RPMLimit3_C) << 16);
					$data[40] = ($dataArr->EngEff_Eload_61_80_RPMLimit4_C) | (($dataArr->EngEff_Eload_61_80_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_81_90_RPMLimit1_C) << 16);
					$data[41] = ($dataArr->EngEff_Eload_81_90_RPMLimit2_C) | (($dataArr->EngEff_Eload_81_90_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_81_90_RPMLimit4_C) << 16);
					$data[42] = ($dataArr->EngEff_Eload_81_90_RPMLimit5_C) | (($dataArr->EngEff_Eload_91_94_RPMLimit1_C) << 8) | (($dataArr->EngEff_Eload_91_94_RPMLimit2_C) << 16);
					$data[43] = ($dataArr->EngEff_Eload_91_94_RPMLimit3_C) | (($dataArr->EngEff_Eload_91_94_RPMLimit4_C) << 8) | (($dataArr->EngEff_Eload_91_94_RPMLimit5_C) << 16);
					$data[44] = ($dataArr->EngEff_Eload_95_98_RPMLimit1_C) | (($dataArr->EngEff_Eload_95_98_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_95_98_RPMLimit3_C) << 16);
					$data[45] = ($dataArr->EngEff_Eload_95_98_RPMLimit4_C) | (($dataArr->EngEff_Eload_95_98_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_99_100_RPMLimit1_C) << 16);
					$data[46] = ($dataArr->EngEff_Eload_99_100_RPMLimit2_C) | (($dataArr->EngEff_Eload_99_100_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_99_100_RPMLimit4_C) << 16);
					$data[47] = ($dataArr->EngEff_Eload_99_100_RPMLimit5_C);

					// Volumetric Efficiency
					$data[48] = ($dataArr->VolEff_Eload_1_10_RPMLimit1_C) | (($dataArr->VolEff_Eload_1_10_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_1_10_RPMLimit3_C) << 16) ;
					$data[49] = ($dataArr->VolEff_Eload_1_10_RPMLimit4_C) | (($dataArr->VolEff_Eload_1_10_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_11_20_RPMLimit1_C) << 16); 
					$data[50] = ($dataArr->VolEff_Eload_11_20_RPMLimit2_C) | (($dataArr->VolEff_Eload_11_20_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_11_20_RPMLimit4_C) << 16);
					$data[51] = ($dataArr->VolEff_Eload_11_20_RPMLimit5_C) | (($dataArr->VolEff_Eload_21_30_RPMLimit1_C) << 8) | (($dataArr->VolEff_Eload_21_30_RPMLimit2_C) << 16);
					$data[52] = ($dataArr->VolEff_Eload_21_30_RPMLimit3_C) | (($dataArr->VolEff_Eload_21_30_RPMLimit4_C) << 8) | (($dataArr->VolEff_Eload_21_30_RPMLimit5_C) << 16);	
					$data[53] = ($dataArr->VolEff_Eload_31_40_RPMLimit1_C) | (($dataArr->VolEff_Eload_31_40_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_31_40_RPMLimit3_C) << 16) ;
					$data[54] = ($dataArr->VolEff_Eload_31_40_RPMLimit4_C) | (($dataArr->VolEff_Eload_31_40_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_41_50_RPMLimit1_C) << 16); 
					$data[55] = ($dataArr->VolEff_Eload_41_50_RPMLimit2_C) | (($dataArr->VolEff_Eload_41_50_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_41_50_RPMLimit4_C) << 16);
					$data[56] = ($dataArr->VolEff_Eload_41_50_RPMLimit5_C) | (($dataArr->VolEff_Eload_51_60_RPMLimit1_C) << 8) | (($dataArr->VolEff_Eload_51_60_RPMLimit2_C) << 16);
					$data[57] = ($dataArr->VolEff_Eload_51_60_RPMLimit3_C) | (($dataArr->VolEff_Eload_51_60_RPMLimit4_C) << 8) | (($dataArr->VolEff_Eload_51_60_RPMLimit5_C) << 16);
					$data[58] = ($dataArr->VolEff_Eload_61_80_RPMLimit1_C) | (($dataArr->VolEff_Eload_61_80_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_61_80_RPMLimit3_C) << 16);
					$data[59] = ($dataArr->VolEff_Eload_61_80_RPMLimit4_C) | (($dataArr->VolEff_Eload_61_80_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_81_90_RPMLimit1_C) << 16);
					$data[60] = ($dataArr->VolEff_Eload_81_90_RPMLimit2_C) | (($dataArr->VolEff_Eload_81_90_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_81_90_RPMLimit4_C) << 16);
					$data[61] = ($dataArr->VolEff_Eload_81_90_RPMLimit5_C) | (($dataArr->VolEff_Eload_91_94_RPMLimit1_C) << 8) | (($dataArr->VolEff_Eload_91_94_RPMLimit2_C) << 16);
					$data[62] = ($dataArr->VolEff_Eload_91_94_RPMLimit3_C) | (($dataArr->VolEff_Eload_91_94_RPMLimit4_C) << 8) | (($dataArr->VolEff_Eload_91_94_RPMLimit5_C) << 16);
					$data[63] = ($dataArr->VolEff_Eload_95_98_RPMLimit1_C) | (($dataArr->VolEff_Eload_95_98_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_95_98_RPMLimit3_C) << 16);
					$data[64] = ($dataArr->VolEff_Eload_95_98_RPMLimit4_C) | (($dataArr->VolEff_Eload_95_98_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_99_100_RPMLimit1_C) << 16);
					$data[65] = ($dataArr->VolEff_Eload_99_100_RPMLimit2_C) | (($dataArr->VolEff_Eload_99_100_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_99_100_RPMLimit4_C) << 16);
					$data[66] = ($dataArr->VolEff_Eload_99_100_RPMLimit5_C) | (($dataArr->Ovr_Run_Cold_VolEff_C) << 8) | (($dataArr->Ovr_Run_Hot_VolEff_C) << 16);
					
					$data[67]  = 0;
					$data[68]  = 0;
					$data[69]  = 0;
					$data[70]  = $dataArr->DPF_Avl_C;
					$data[71]  = round(($dataArr->MAP_RPM_PkLmt_C / 10),0);
					$data[72]  = $dataArr->MAP_Dif_IdleLmt3_C;
					$data[73]  = $dataArr->MAP_Dif_IdleLmt1_C;
					$data[74]  = $dataArr->MAP_Abs_PkLmt3_C;
					$data[75]  = $dataArr->MAP_Abs_PkLmt1_C;
					$data[76]  = $dataArr->MAP_Dif_PkLmt3_C;
					$data[77]  = $dataArr->MAP_Dif_PkLmt1_C;
					$data[78]  = $dataArr->Take_Off_RPM_factor_C;
					$data[79]  = $dataArr->Speedbump_RPM_factor_C;
					$data[80]  = ($dataArr->GearM_Dwn4Gr_VehSpd1_C) | (($dataArr->GearM_Dwn4Gr_VehSpd2_C) << 16);
					$data[81]  = ($dataArr->GearM_Dwn4Gr_LOAD_C) | ((round(($dataArr->GearM_Dwn4Gr_RPM1_C / 10),0)) << 8) | ((round(($dataArr->GearM_Dwn4Gr_RPM2_C / 10),0)) << 16);
					$data[82]  = round(($dataArr->Debounce_4DOWN_C / 10),0);
					$data[83]  = 64;
					$data[84]  = $encryptKey;
					$data[85]  = 0;
					$data[86]  = 0;
					$data[87]  = 0;
					$data[88]  = 0;
					$data[89]  = 0;
					$data[90]  = 0;
					$data[91]  = 0;
					$data[92]  = 0;
					$data[93]  = 0;
					$data[94]  = 0;
					$data[95]  = 0;
					$data[96]  = 0;
					$data[97]  = 0;
					$data[98]  = 0;
					$data[99]  = 0;
					$data[100] = 0;
					$data[101] = 0;
					$data[102] = 0;
					$data[103] = 0;
					$data[104] = 0;
					$data[105] = 0;
					$data[106] = 0;
					$data[107] = 0;
					$data[108] = 0;
					$data[109] = 0;
					$data[110] = 0;
					$data[111] = 0;
					$data[112] = 0;
					$data[113] = 0;
					$data[114] = 0;
					$data[115] = 0;
					$data[116] = 0;
					$data[117] = 0;
					$data[118] = 0;
					$data[119] = 0;
					$data[120] = 0;
					$data[121] = 0;
					$data[122] = 0;
					$data[123] = 0;
					$data[124] = $dataArr->MAP_Valid_C;
					$data[125] = $dataArr->MAF_Valid_C;
					$data[126] = $dataArr->Baro_Press_City_C;
					$data[127] = $dataArr->MAP_Idle_Ld2_C;
					$data[128] = round(($dataArr->Accl_Val_C * 20),0);
					$data[129] = $dataArr->Accl_Eload_C;
					$data[130] = round(( $dataArr->Decl_Val_C * 20),0);
					$data[131] = $dataArr->Eload_Def_C;
					$data[132] = $dataArr->Eload_Max_C;
					$data[133] = round(($dataArr->Max_Torque_C / 5),0);
					$data[134] = $dataArr->ELoad_Idle_Warm_C;
					$data[135] = $dataArr->ELoad_Idle_Ld1_C;
					$data[136] = $dataArr->ELoad_Idle_Ld2_C;
					$data[137] = $dataArr->Osc_EloadOffset_Lmt_C;
					$data[138] = $dataArr->Cool_Lmt_C;
					$data[139] = $dataArr->Osc_RPM_Lmt1_C;
					$data[140] = $dataArr->Osc_RPM_Lmt2_C;
					$data[141] = $dataArr->Osc_RPM_Lmt3_C;
					$data[142] = $dataArr->Osc_RPM_Lmt4_C;
					$data[143] = round(($dataArr->CRP_RPM_PkLmt_C / 50),0);
					$data[144] = round(($dataArr->CRP_Type_C / 1000),0);
					$data[145] = round(($dataArr->CRP_MaxLmt3_C / 500),0);
					$data[146] = round(($dataArr->CRP_MaxLmt1_C / 500),0);
					$data[147] = $dataArr->Base_OvrHt_Cool_Lmt_C;
					$data[148] = $dataArr->Cool_Temp_Lmt1_C;
					$data[149] = $dataArr->Cool_Temp_Lmt2_C;
					$data[150] = $dataArr->Cool_Temp_Lmt3_C;
					$data[151] = $dataArr->Cool_Temp_Lmt4_C;
					$data[152] = round(($dataArr->RPM_Idle_Warm_C / 10),0);
					$data[153] = round(($dataArr->RPM_Idle_Ld1_C / 10),0);
					$data[154] = round(($dataArr->RPM_Idle_Ld2_C / 10),0);
					$data[155] = $dataArr->MAP_RPMOffset_PkLmt_C;
					$data[156] = round(($dataArr->Max_Air_Trq_RPM_C / 50),0);
					$data[157] = round(($dataArr->MAP_Air_RPM_LwrLmt_C / 50),0);
					$data[158] = round(($dataArr->MAP_Air_RPM_PkLmt_C / 50),0);
					$data[159] = $dataArr->MAP_Air_EloadOffset_PkLmt_C;
					$data[160] = $dataArr->MAP_Air_IdleLmt3_C;
					$data[161] = $dataArr->MAP_Air_IdleLmt1_C;
					$data[162] = $dataArr->MAP_Air_PkLmt3_C;
					$data[163] = $dataArr->MAP_Air_PkLmt1_C;
					$data[164] = round(($dataArr->Time_Upld_Frq_C / 3),0);
					if($dataArr->MAP_Type_C == "Absolute")
					{
						$data[165] = 1;
					}
					else
					{
						$data[165] = 0;
					}
					
					$data[166] = $dataArr->IC_Status_C;
					//Diesel Engine Fuel Factors
					$data[167] = (round(($dataArr->Fact_Fuel_Run_ELoad_0_10_C  * 100),0)) | ((round(($dataArr->Fact_Fuel_Run_ELoad_11_20_C  * 100),0)) << 16);
					$data[168] = (round(($dataArr->Fact_Fuel_Run_ELoad_21_30_C  * 100),0)) | ((round(($dataArr->Fact_Fuel_Run_ELoad_31_40_C  * 100),0)) << 16);
					$data[169] = (round(($dataArr->Fact_Fuel_Run_ELoad_41_50_C  * 100),0)) | ((round(($dataArr->Fact_Fuel_Run_ELoad_51_60_C  * 100),0)) << 16);
					$data[170] = (round(($dataArr->Fact_Fuel_Run_ELoad_61_70_C  * 100),0)) | ((round(($dataArr->Fact_Fuel_Run_ELoad_71_80_C  * 100),0)) << 16);
					$data[171] = (round(($dataArr->Fact_Fuel_Run_ELoad_81_90_C  * 100),0)) | ((round(($dataArr->Fact_Fuel_Run_ELoad_91_94_C  * 100),0)) << 16);
					$data[172] = (round(($dataArr->Fact_Fuel_Run_ELoad_95_98_C  * 100),0)) | ((round(($dataArr->Fact_Fuel_Run_ELoad_99_100_C  * 100),0)) << 16);
					$data[173] = ($dataArr->Ovr_Run_Cold_FuelFact_Eload_C) | (($dataArr->Ovr_Run_Hot_FuelFact_Eload_C) << 16);
					$data[174] = (round(($dataArr->Ovr_Run_Cold_FuelFact_C  * 100),0)) | ((round(($dataArr->Ovr_Run_Hot_FuelFact_C  * 100),0)) << 16);
					$data[175] = (round(($dataArr->Fact_Fuel_Idle_ELoad1_C  * 100),0)) | ((round(($dataArr->Fact_Fuel_Idle_ELoad2_C  * 100),0)) << 16);

					$data[176] = round(($dataArr->gr_5_high_C * 10000),0);
					$data[177] = round(($dataArr->gr_6_low_C  * 10000),0);
					$data[178] = round(($dataArr->gr_6_high_C * 10000),0);
					$data[179] = $dataArr->Eng_Rev_Lmt_C;
					$data[180] = $dataArr->Cool_Temp_AvgLmt_C;
					$data[181] = $dataArr->Gr_RPM_Col0_C;
					$data[182] = $dataArr->Gr_RPM_Col1_C;
					$data[183] = $dataArr->Gr_RPM_Col2_C;
					$data[184] = $dataArr->Gr_RPM_Col3_C;
					$data[185] = $dataArr->Gr_RPM_Col4_C;
					$data[186] = $dataArr->Gr_RPM_Col5_C;
					$data[187] = $dataArr->Gr_RPM_Col6_C;
					$data[188] = $dataArr->Gr_RPM_Col7_C;
					$data[189] = $dataArr->Gr_RPM_Col8_C;
					$data[190] = $dataArr->Gr_RPM_Col9_C;
					$data[191] = $dataArr->Thr_RPM_Col0_C;
					$data[192] = $dataArr->Thr_RPM_Col1_C;
					$data[193] = $dataArr->Thr_RPM_Col2_C;
					$data[194] = $dataArr->Thr_RPM_Col3_C;
					$data[195] = $dataArr->Thr_RPM_Col4_C;
					$data[196] = $dataArr->Thr_RPM_Col5_C;
					$data[197] = $dataArr->Thr_RPM_Col6_C;
					$data[198] = $dataArr->Thr_RPM_Col7_C;
					$data[199] = $dataArr->Thr_RPM_Col8_C;
					$data[200] = $dataArr->Thr_RPM_Col9_C;
					$data[201] = $dataArr->Thr_Pos_Row0_C;
					$data[202] = $dataArr->Thr_Pos_Row1_C;
					$data[203] = $dataArr->Thr_Pos_Row2_C;
					$data[204] = $dataArr->Thr_Pos_Row3_C;
					$data[205] = $dataArr->Thr_Pos_Row4_C;
					$data[206] = $dataArr->Thr_Pos_Row5_C;
					$data[207] = $dataArr->Thr_Pos_Row6_C;
					$data[208] = $dataArr->Thr_Pos_Row7_C;
					$data[209] = $dataArr->Thr_Pos_Row8_C;
					$data[210] = $dataArr->Thr_Pos_Row9_C;
					$data[211] = $dataArr->Veh_Spd_Col0_C;
					$data[212] = $dataArr->Veh_Spd_Col1_C;
					$data[213] = $dataArr->Veh_Spd_Col2_C;
					$data[214] = $dataArr->Veh_Spd_Col3_C;
					$data[215] = $dataArr->Veh_Spd_Col4_C;
					$data[216] = $dataArr->Veh_Spd_Col5_C;
					$data[217] = $dataArr->Veh_Spd_Col6_C;
					$data[218] = $dataArr->Veh_Spd_Col7_C;
					$data[219] = $dataArr->Veh_Spd_Col8_C;
					$data[220] = $dataArr->Veh_Spd_Col9_C;
					$data[221] = round(($dataArr->Spd_Chng_Neg_Row5_C * 20),0);
					$data[222] = round(($dataArr->Spd_Chng_Neg_Row4_C * 20),0);
					$data[223] = round(($dataArr->Spd_Chng_Neg_Row3_C * 20),0);
					$data[224] = round(($dataArr->Spd_Chng_Neg_Row2_C * 20),0);
					$data[225] = round(($dataArr->Spd_Chng_Neg_Row1_C * 20),0);
					$data[226] = round(($dataArr->Spd_Chng_Row0_C * 20),0);
					$data[227] = round(($dataArr->Spd_Chng_Pos_Row1_C * 20),0);
					$data[228] = round(($dataArr->Spd_Chng_Pos_Row2_C * 20),0);
					$data[229] = round(($dataArr->Spd_Chng_Pos_Row3_C * 20),0);
					$data[230] = round(($dataArr->Spd_Chng_Pos_Row4_C * 20),0);
					$data[231] = round(($dataArr->Spd_Chng_Pos_Row5_C * 20),0);
					$data[232] = $dataArr->Ovr_Spd_Lmt_C;
					$data[233] = round(($dataArr->Brake_EngSt3_Acc_C * 20),0);
					$data[234] = round(($dataArr->Brake_EngSt4_Acc_C * 20),0);
					$data[235] = $dataArr->Hlf_Clutch_Thr_C;
					$data[236] = $dataArr->Gr_Cnt_Debounce_C;
					$data[237] = $dataArr->Veh_Spd_Avl_C;
					$data[238] = $dataArr->Trq_RPM_RSE_C;
					$data[239] = $dataArr->Trq_Eload_RSE_C;
					$data[240] = $dataArr->Accln_Debounce_C;
					$data[241] = $dataArr->GPS_Spd_Plaus_C;
					$data[242] = 0;
					$data[243] = 0;
					$data[244] = 0;
					$data[245] = 0;
					$data[246] = 0;
					$data[247] = 0;
					$data[248] = 0;
					$data[249] = 0;
					
					$resultArr = array();
					$resultArr = json_decode(json_encode($data,JSON_NUMERIC_CHECK));
					// print_r($resultArr);die;

					//TODO: Update config status in DB
					Common::updateTable('device',array('id' => $did),array('dactval' => '0'));

					$result['success'] = TRUE;
					$result['data']    = array_values($resultArr);
				}
				else
				{
					$result['success']      = FALSE;
					$result['errorCode']    = "1002";
					$result['errorMessage'] = "Invalid Device ID!";
				}
	        }
	        else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1001";
				$result['errorMessage'] = "Invalid Input!";
			}
		}
     	$resultVal = json_encode($result,JSON_NUMERIC_CHECK);
		echo $resultVal;
  	}

  	// fuction to verify Auth key
  	function verifykey($key)
  	{
  		$res = Fetch::randKeyVerify($key);
  		if($res[0]->count > 0)
  		{
  			return 1;
  		}
  		else
  		{
  			return 0;
  		}
  		// print_r($res);die;
  	}
}