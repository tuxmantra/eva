<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\AskAssistance;
use PHPExcel; 
use PHPExcel_IOFactory;

class Summary extends Controller
{
    public function index(){
      
      $data['title']      = 'Summary';
      $data['summary_open'] = '1';
      $data['menu']          = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      $data['vehiclelist']   = Fetch::getvehicletype(session('userdata')['uid'],session('userdata')['ucategory']);
      $data['rider']         = Fetch::getusername(session('userdata')['uid'],session('userdata')['ucategory']);
      return view('summary/index')->with($data);
    }

    public function getSummaryLoad(Request $request)
    {
        $user                = session('userdata');
        $data['user_type']   = $user['utype'];
        $start               = $request->post('start');
        $end                 = $request->post('end');
        $vehcile             = $request->post('vehcile');
        $rider               = $request->post('rider');
        $result              = $request->post('result');
        //  $end                 = $request->post('end');
        // echo session('userdata')['utype'];die;  
        // print_r(session('userdata'));die;
        if(empty($end) && empty($vehcile) && empty($rider) && empty($result)) 
        {
            $data['reports']     = Fetch::getRepots(session('userdata')['uid'],session('userdata')['ucategory']); 
        }
        else
        {
            $data['reports']     = Fetch::getReportsByDate($start,$end,$vehcile,$rider,$result,session('userdata')['uid'],session('userdata')['ucategory']);  
        }                    
          
        $result = view('summary/list')->with($data);
        echo $result;
    }

    public function export($result){
              $tmpfile = tempnam(sys_get_temp_dir(), 'html');
              file_put_contents($tmpfile, $result);
              $objPHPExcel     = new PHPExcel(); 
              $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
              $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
              $objPHPExcel->getActiveSheet()->setTitle('Daily Report Summary');
              // print_r($objPHPExcel);die;
              unlink($tmpfile);
              $writer   = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
              $pathInfo = getcwd();
              $folder   = "$pathInfo/summary_report/".date('Y-m-d')."/";
              if(!file_exists($folder))
              {
                mkdir($folder,0777,true);
              }
              chmod($folder, 0777);
              // print_r($folder);die;
              $writer->save($folder.date('Y-m-d').".xlsx");
              chmod($folder.date('Y-m-d').".xlsx", 0777);
              exit;
    }

    public function getModel($manu,$model,$varient,$search){
        $modelVarList = Fetch::getVehicleInfoDataById($manu);
        if(!empty($modelVarList)){
            $model_list    = explode(';',$modelVarList[0]->model);
            $varient_list  = explode(';',$modelVarList[0]->var);
            $model_varient =  $model_list[$model]." ".$varient_list[$varient];
            return $model_varient;
        }
    }

    public function getTrafficCondition($sumData){
        if(!empty($sumData)){
            $st            = 0;
            $dursum        = 0;
            foreach($sumData as $avg){
                $st        = $st + ($avg->avg_speed * $avg->dur);
                $dursum    = $dursum + $avg->dur;
            }
            if ($dursum == 0) {
              $avg_speed   = 0;
            }
            else{
              $avg_speed   = round($st/$dursum,0);
            }
        }else{
            $avg_speed     = 0;
        }
        
        // Trafic conditions
        if($avg_speed == 0){
          return "-";
        }elseif($avg_speed < 10){
           return "Congested Traffic Usage";
        }elseif($avg_speed < 25){
           return "Heavy Traffic Usage";
        }elseif($avg_speed < 35){
           return "Medium Traffic Usage";
        }elseif($avg_speed < 50){
           return "City + Highway Usage";
        }else{
           return "Highway Usage";
        }

    }

    public function getTotalDistance($sumDat){
       $distance         = !empty($sumDat[0]->distance)? $sumDat[0]->distance:"0";
       return round($distance,2);
    }

    public function getMilage($sumData){
        if(!empty($sumData)){
            $count_mil  = count($sumData);
            $st         = 0;
            $distsum    = 0;
            foreach($sumData as $sum){
                $st = $st + ($sum->dist * $sum->mil);
                $distsum = $distsum + $sum->dist;
            }
            //Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
            if ($distsum == 0) {
              $mileage   = 0;
            }
            else{
              $mileage   = $st/$distsum;
            }
            return round($mileage,2);
        }
    }

    public function idleTime($sumDat){

        $duration           = !empty($sumDat[0]->duration)? $sumDat[0]->duration:"0";
        // Total Engine Off Time
        $keyon_time         = !empty($sumDat[0]->keyon_time)? $sumDat[0]->keyon_time:"0";


        // Total running duration
        $run_dur            = $duration - $keyon_time;
        $idle_time          = !empty($sumDat[0]->idle_time)? $sumDat[0]->idle_time:"0";
        $idle_time          = ($run_dur == 0) ? 0: round($idle_time*100/$run_dur,0);
        return $idle_time;
    }

    public function engine_start($sumDat){

        $distance         = !empty($sumDat[0]->distance)? $sumDat[0]->distance:"0";
        // distance factor to round up to 100 km
        if($distance != 0){
            $dist_fact = 100/$distance;
        }
        else {
            $dist_fact = 1;
        }
        
        $no_of_starts      = !empty($sumDat[0]->no_starts) ? $sumDat[0]->no_starts:"0";
        $no_starts         = round($no_of_starts*$dist_fact,0);
        return $no_starts;
    }

    public function crank_3($crankCount,$sumDat){
        // Cranks > 3 to start
        $distance         = !empty($sumDat[0]->distance)? $sumDat[0]->distance:"0";
        // distance factor to round up to 100 km
        if($distance != 0){
              $dist_fact = 100/$distance;
        }
        else {
          $dist_fact = 1;
        }
        
        $q = array();
        foreach($crankCount as $crn){
           if($crn->max_crnk_cnt > 3){
              $q[] = $crn->max_crnk_cnt;
           }
        }
        $crankGreater         = !empty($q)? count($q):"0";
        $crankGreater         = round($dist_fact*$crankGreater,0);
        return $crankGreater;
    }

    public function maxSpeed($sumDat){
        return !empty($sumDat[0]->max_speed)? $sumDat[0]->max_speed:"0";
    }

    public function averageSpeed($sumData){
        // Average Speed
        // $avg_speed            = !empty($sumDat[0]->avg_speed)? $sumDat[0]->avg_speed:"0";
        if(!empty($sumData)){
            $st            = 0;
            $dursum        = 0;
            foreach($sumData as $avg){
                $st        = $st + ($avg->avg_speed * $avg->dur);
                $dursum    = $dursum + $avg->dur;
            }
            if ($dursum == 0) {
              $avg_speed   = 0;
            }
            else{
              $avg_speed   = round($st/$dursum,0);
            }
        }else{
            $avg_speed     = 0;
        }
        return $avg_speed;
    }

    public function maxRPM($sumDat){
       // Max RPM
        $rpm = !empty($sumDat[0]->max_rpm)? $sumDat[0]->max_rpm:"0";
        return round($rpm, 0);
    }

    public function averageRPM($sumStdDev){
        // Average RPM
        if(!empty($sumStdDev)){
            $st = 0;
            foreach($sumStdDev as $sum){
              if (is_numeric($sum->rpm)) {
               $st = $st + $sum->rpm;
             }
            }
            if(count($sumStdDev) == 0) {
              $avg_rpm   = 0;
            }
            else{
               $avg_rpm     = $st/count($sumStdDev);
            }
        }else {
            $avg_rpm     = 0;
        }
        return round($avg_rpm,0);
    }

    public function maxCool($sumDat){
        // Max Coolant Temp
        $max_cool          = !empty($sumDat[0]->max_cool)? $sumDat[0]->max_cool:"0";
        return round($max_cool,0); 
    }

    public function gear_changes($sumDat,$gearChange){

        $distance         = !empty($sumDat[0]->distance)? $sumDat[0]->distance:"0";
        // distance factor to round up to 100 km
        if($distance != 0){
            $dist_fact = 100/$distance;
        }
        else {
            $dist_fact = 1;
        }
        // No Of Gear Shift
        $gear_shift         = !empty($gearChange[0]->gear_shift)? $gearChange[0]->gear_shift:"0";
        // $gear_shift         = ($dist_fact*$gear_shift);
        return ceil($dist_fact*$gear_shift);
    }

    public function injectorHealth($systemHealth){
      return !empty($systemHealth[0]->eng_block_ej)? $systemHealth[0]->eng_block_ej:"-";
    }

    public function airHealth($systemHealth){
        // Air Filter Health
        $idlbc_v   = !empty($systemHealth[0]->idlbc_v)? $systemHealth[0]->idlbc_v:"0";
        $peakb_v   = !empty($systemHealth[0]->peakb_v)? $systemHealth[0]->peakb_v:"0";
        $airHealth = "-";

        if($idlbc_v == 1 && $peakb_v == 1){
            $airHealth    = 1;
        }elseif($idlbc_v == 1 && $peakb_v == 2){
            $airHealth    = 2;
        }elseif($idlbc_v == 1 && $peakb_v == 3){
            $airHealth    = 3;
        }elseif($idlbc_v == 2 && $peakb_v == 1){
            $airHealth    = 1;
        }elseif($idlbc_v == 2 && $peakb_v == 2){
            $airHealth    = 3;
        }elseif($idlbc_v == 2 && $peakb_v == 3){
            $airHealth    = 4;
        }elseif($idlbc_v == 3 && $peakb_v == 1){
            $airHealth    = 2;
        }elseif($idlbc_v == 3 && $peakb_v == 2){
            $airHealth    = 3;
        }elseif($idlbc_v == 3 && $peakb_v == 3){
            $airHealth    = 5;
        }
        return $airHealth;
    }

    public function oilHealth($oilHealth){
        // Oil Health
      $oilHealthScore = "-";
        $oilHealth = !empty($oilHealth[0]->oillifep)? $oilHealth[0]->oillifep:"105";
        if($oilHealth >= 52 && $oilHealth <= 100) {
            $oilHealthScore = 5;
        }
        elseif($oilHealth >= 35 && $oilHealth <= 100) {
            $oilHealthScore = 4;
        }
        elseif($oilHealth >= 20 && $oilHealth <= 100) {
            $oilHealthScore = 3;
        }
        elseif($oilHealth >= 6 && $oilHealth <= 100) {
            $oilHealthScore = 2;
        }
        elseif($oilHealth >= 0 && $oilHealth <= 100){
            $oilHealthScore = 1;
        }
        return $oilHealthScore;
    }

    public function coolantHealth($systemHealth){
       return !empty($systemHealth[0]->coolp)? $systemHealth[0]->coolp:"-";
    }

    public function bat_health($systemHealth){
        $batt_hlt       = !empty($systemHealth[0]->bat_c)? $systemHealth[0]->bat_c:"0";
        $batt_rate      = 0;

        if(($batt_hlt < 10) && ($batt_hlt != "") && ($batt_hlt != 0))
        {
            $batt_rate = $batt_hlt;
        }
        elseif(($batt_hlt < 20) && ($batt_hlt != "") && ($batt_hlt != 0))
        {
            $batt_rate = $batt_hlt - 10;
        }
        elseif(($batt_hlt < 30) && ($batt_hlt != "") && ($batt_hlt != 0))
        {
            $batt_rate = $batt_hlt - 20;
        }
        elseif(($batt_hlt > 30) && ($batt_hlt != "") && ($batt_hlt != 0))
        {
            $batt_rate = $batt_hlt - 30;
        }else{
            $batt_rate = "-";
        }
        return $batt_rate;
    }

}