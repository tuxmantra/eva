<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helper;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;

class Vehicle extends Controller
{
    // Load Main Vehicle Add New Form
    public function index($did){
        $did             = Helper::crypt(2,$did);
        $vehicleData     = Fetch::getVehicleDataById($did);
        $vehicleSettings = Fetch::getVehicleSettingData();
        $fuel            = Fetch::getFuelTypeData();
        $mfdList         = explode(';',$vehicleSettings->manufacturer);
        $engcap          = explode(';',$vehicleSettings->engine_capacity);
        $cityList        = preg_split ('/$\R?^/m',$vehicleSettings->citylist);

        $data = array(
            'title'           => 'Add Device',
            'mfdList'         => $mfdList,
            'did'             => $did,
            'fuel'            => $fuel,
            'engcap'          => $engcap,
            'citylist'        => $cityList,
            'vehicledata'     => $vehicleData,
            'form_token'      => Helper::token(8) 
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('vehicle/addvehicle')->with($data);
    }

    public function add_vehicle_new($did){
        $did             = Helper::crypt(2,$did);
        $vehicleData     = Fetch::getVehicleDataById($did);
        $vehicleSettings = Fetch::getVehicleSettingData();
        $fuel            = Fetch::getFuelTypeData();
        $mfdList         = explode(';',$vehicleSettings->manufacturer);
        $engcap          = explode(';',$vehicleSettings->engine_capacity);
        $cityList        = preg_split ('/$\R?^/m',$vehicleSettings->citylist);

        $data = array(
            'title'           => 'Add Device',
            'mfdList'         => $mfdList,
            'did'             => $did,
            'fuel'            => $fuel,
            'engcap'          => $engcap,
            'citylist'        => $cityList,
            'vehicledata'     => $vehicleData,
            'form_token'      => Helper::token(8) 
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('vehicle/addvehiclenew')->with($data);
    }

    // update and edit form process
    public function processVehicleData(Request $request)
    {
        $token = request('form_token');
        $dide  = $request->post('did');
        if($token == session('form_token'))
        {
            session()->forget('form_token');
            if(!empty($request->post('idVal')))
            {
                // Update Vehicle---------------------
                // Update Vehicle Process
                $vehId           = Helper::crypt(2,$request->post('idVal'));
                $did             = Helper::crypt(2,$dide);
                $manufact        = $request->post('manufact');
                $model           = $request->post('model');
                $variant         = $request->post('variant');
                $transmission    = $request->post('transmission');
                $fuel            = $request->post('fuel');
                $engcap          = $request->post('engcap');
                $manu_year       = $request->post('manu_year');
                $dateofpurchase  = $request->post('dateofpurchase');
                $distance        = $request->post('distance');
                $registration    = $request->post('registration');
                $insure          = $request->post('insure');
                $renewalinsure   = $request->post('renewalinsure');
                $modification    = $request->post('modification');
                $lastservicedate = $request->post('lastservicedate');
                $kmlastservice   = $request->post('kmlastservice');
                $status          = $request->post('status');
                $oilchangedate   = $request->post('oilchangedate');
                $kmatoilchange   = $request->post('kmatoilchange');
                $calib           = $request->post('calib');
                $moniter         = $request->post('moniter');
                $city            = $request->post('city');
                $note            = $request->post('note');
                $chassis_num     = $request->post('chassis_num');
                $engine_num      = $request->post('engine_num');

                // validate
                if($manufact == '' || $model == '' || $variant == '' || $transmission == '' || $fuel == '' || $engcap == '' || $manu_year == '' || $dateofpurchase == '' || $distance == '' || $registration == '' ||  $lastservicedate == '' || $kmlastservice == '' || $status == '' || $oilchangedate == '' || $kmatoilchange == '' || $calib == '' || $moniter == '' || $city == '' )
                {
                    \Session::flash('message','Missing Some Fields.');
                    return redirect('device_manager/'.$dide);
                }

                $data = array(
                   'model'           => $model,
                   'manufact'        => $manufact,
                   'varient'         => $variant,
                   'eng_cap'         => $engcap,
                   'fuel_tpe'        => $fuel,
                   'trans_type'      => $transmission,
                   'manufact_yr'     => $manu_year,
                   'purchase_dt'     => date('Y-m-d',strtotime($dateofpurchase)),
                   'travel_purchase' => $distance,
                   'reg_no'          => $registration,
                   'vehicle_insure'  => $insure,
                   'renew_dt_insure' => date('Y-m-d',strtotime($renewalinsure)),
                   'modific'         => $modification,
                   'last_serv_dt'    => date('Y-m-d',strtotime($lastservicedate)),
                   'travel_service'  => $kmlastservice,
                   'status'          => $status,
                   'oil_dt'          => date('Y-m-d',strtotime($oilchangedate)),
                   'oil_dist'        => $kmatoilchange,
                   'calib'           => $calib,
                   'monitor'         => $moniter,
                   'city'            => $city,
                   'note'            => $note,
                   'chassis_num'     => $chassis_num,
                   'engine_num'      => $engine_num,
                   'ad_by'           => session('userdata')['uid'],
                   'modelid'         => $model,
                   'varientid'       => $variant
                );
                $device_data=Fetch::getdevice1($did);
                Common::updateTable_vehicle('vehicles',array('id' => $vehId),$data,$device_data[0]->device_id);
                $return = $this->setvehSpec($manufact,$model,$fuel,$variant,$engcap,$did,$manu_year,$dide,$city);
                if($return == true)
                {
                   \Session::flash('message','Vehicle Updated Successfully.');
                    return redirect('device_manager/'.$dide);
                }
                else
                {
                    \Session::flash('message','Vehicle updated but Sorry Callibration Failed, Please recheck the vehicle details.');
                    //return redirect('add-vehicle/'.$dide);
                    return redirect('device_manager/'.$dide);
                }
            }else{
                // Insert----------------------
                // Add Vehicle Process
                //$deviceID        = Helper::crypt(2,$dide);
                //$did             = Fetch::getIdByDeviceId($deviceID);
                $did               = Helper::crypt(2,$dide);
                
                //$did             = $did[0]->id;
                $manufact        = $request->post('manufact');
                $model           = $request->post('model');
                $variant         = $request->post('variant');
                $transmission    = $request->post('transmission');
                $fuel            = $request->post('fuel');
                $engcap          = $request->post('engcap');
                $manu_year       = $request->post('manu_year');
                $dateofpurchase  = $request->post('dateofpurchase');
                $distance        = $request->post('distance');
                $registration    = $request->post('registration');
                $insure          = $request->post('insure');
                $renewalinsure   = $request->post('renewalinsure');
                $modification    = $request->post('modification');
                $lastservicedate = $request->post('lastservicedate');
                $kmlastservice   = $request->post('kmlastservice');
                $status          = $request->post('status');
                $oilchangedate   = $request->post('oilchangedate');
                $kmatoilchange   = $request->post('kmatoilchange');
                $calib           = $request->post('calib');
                $moniter         = $request->post('moniter');
                $city            = $request->post('city');
                $note            = $request->post('note');
                $chassis_num     = $request->post('chassis_num');
                $engine_num      = $request->post('engine_num');
                
                // validate
                if($manufact == '' || $model == '' || $variant == '' || $transmission == '' || $fuel == '' || $engcap == '' || $manu_year == '' || $dateofpurchase == '' || $distance == '' || $registration == '' ||  $lastservicedate == '' || $kmlastservice == '' || $status == '' || $oilchangedate == '' || $kmatoilchange == '' || $calib == '' || $moniter == '' || $city == '' )
                {
                    \Session::flash('message','Missing Some Fields.');
                    return redirect('device_manager/'.$dide);
                }

                $data = array(
                   'device'          => $did,
                   'model'           => $model,
                   'manufact'        => $manufact,
                   'varient'         => $variant,
                   'eng_cap'         => $engcap,
                   'fuel_tpe'        => $fuel,
                   'trans_type'      => $transmission,
                   'manufact_yr'     => $manu_year,
                   'purchase_dt'     => date('Y-m-d',strtotime($dateofpurchase)),
                   'travel_purchase' => $distance,
                   'reg_no'          => $registration,
                   'vehicle_insure'  => $insure,
                   'renew_dt_insure' => date('Y-m-d',strtotime($renewalinsure)),
                   'modific'         => $modification,
                   'last_serv_dt'    => date('Y-m-d',strtotime($lastservicedate)),
                   'travel_service'  => $kmlastservice,
                   'status'          => $status,
                   'oil_dt'          => date('Y-m-d',strtotime($oilchangedate)),
                   'oil_dist'        => $kmatoilchange,
                   'calib'           => $calib,
                   'monitor'         => $moniter,
                   'city'            => $city,
                   'note'            => $note,
                   'chassis_num'     => $chassis_num,
                   'engine_num'      => $engine_num,
                   'ad_by'           => session('userdata')['uid'],
                   'modelid'         => $model,
                   'varientid'       => $variant
                );
                Common::insert_vehicle('vehicles',$data);
                Common::updateTable('device',array('id' => $did),array('total_dist' => $distance));
                $return = $this->setvehSpec($manufact,$model,$fuel,$variant,$engcap,$did,$manu_year,$did,$city);
                if($return == true)
                {
                    \Session::flash('message','Vehicle Added Successfully.');
                    return redirect('device_manager/'.$dide);
                }
                else
                {
                    \Session::flash('message','Vehicle Added but Sorry Callibration Failed, Please recheck the vehicle details.');
                    //return redirect('add-vehicle/'.$dide);
                    return redirect('device_manager/'.$dide);
                }
            }
        }
        else
        {
            session()->forget('form_token');
           \Session::flash('message','Sorry, token expired. Try again.');
            return redirect('device_manager/'.$dide);
        }
    }

    // update and edit form process
    public function processVehicleData1(Request $request)
    {
        $token = request('form_token');
        $dide  = $request->post('did');
        if($token == session('form_token'))
        {
            session()->forget('form_token');
            if(!empty($request->post('idVal')))
            {
                // Update Vehicle---------------------
                // Update Vehicle Process
                $vehId           = Helper::crypt(2,$request->post('idVal'));
                $did             = Helper::crypt(2,$dide);
                $manufact        = $request->post('manufact');
                $model           = $request->post('model');
                $variant         = $request->post('variant');
                $transmission    = $request->post('transmission');
                $fuel            = $request->post('fuel');
                $engcap          = $request->post('engcap');
                $manu_year       = $request->post('manu_year');
                $dateofpurchase  = $request->post('dateofpurchase');
                $distance        = $request->post('distance');
                $registration    = $request->post('registration');
                $insure          = $request->post('insure');
                $renewalinsure   = $request->post('renewalinsure');
                $modification    = $request->post('modification');
                $lastservicedate = $request->post('lastservicedate');
                $kmlastservice   = $request->post('kmlastservice');
                $status          = $request->post('status');
                $oilchangedate   = $request->post('oilchangedate');
                $kmatoilchange   = $request->post('kmatoilchange');
                $calib           = $request->post('calib');
                $moniter         = $request->post('moniter');
                $city            = $request->post('city');
                $note            = $request->post('note');
                $chassis_num     = $request->post('chassis_num');
                $engine_num      = $request->post('engine_num');

                // validate
                if($manufact == '' || $model == '' || $variant == '' || $transmission == '' || $fuel == '' || $engcap == '' || $manu_year == '' || $dateofpurchase == '' || $distance == '' || $registration == '' ||  $lastservicedate == '' || $kmlastservice == '' || $status == '' || $oilchangedate == '' || $kmatoilchange == '' || $calib == '' || $moniter == '' || $city == '' )
                {
                    \Session::flash('message','Missing Some Fields.');
                    return redirect('add-vehicle/'.$dide);
                }

                $data = array(
                   'model'           => $model,
                   'manufact'        => $manufact,
                   'varient'         => $variant,
                   'eng_cap'         => $engcap,
                   'fuel_tpe'        => $fuel,
                   'trans_type'      => $transmission,
                   'manufact_yr'     => $manu_year,
                   'purchase_dt'     => date('Y-m-d',strtotime($dateofpurchase)),
                   'travel_purchase' => $distance,
                   'reg_no'          => $registration,
                   'vehicle_insure'  => $insure,
                   'renew_dt_insure' => date('Y-m-d',strtotime($renewalinsure)),
                   'modific'         => $modification,
                   'last_serv_dt'    => date('Y-m-d',strtotime($lastservicedate)),
                   'travel_service'  => $kmlastservice,
                   'status'          => $status,
                   'oil_dt'          => date('Y-m-d',strtotime($oilchangedate)),
                   'oil_dist'        => $kmatoilchange,
                   'calib'           => $calib,
                   'monitor'         => $moniter,
                   'city'            => $city,
                   'note'            => $note,
                   'chassis_num'     => $chassis_num,
                   'engine_num'      => $engine_num,
                   'ad_by'           => session('userdata')['uid'],
                   'modelid'         => $model,
                   'varientid'       => $variant
                );
                $device_data=Fetch::getdevice1($did);
                Common::updateTable_vehicle('vehicles',array('id' => $vehId),$data,$device_data[0]->device_id);
                $return = $this->setvehSpec($manufact,$model,$fuel,$variant,$engcap,$did,$manu_year,$dide,$city);
                if($return == true)
                {
                   \Session::flash('message','Vehicle Updated Successfully.');
                    return redirect('device-list');
                }
                else
                {
                    \Session::flash('message','Vehicle updated but Sorry Callibration Failed, Please recheck the vehicle details.');
                    //return redirect('add-vehicle/'.$dide);
                    return redirect('add-vehicle/'.$dide);
                }
            }else{
                // Insert----------------------
                // Add Vehicle Process
                //$deviceID        = Helper::crypt(2,$dide);
                //$did             = Fetch::getIdByDeviceId($deviceID);
                $did               = Helper::crypt(2,$dide);
                
                //$did             = $did[0]->id;
                $manufact        = $request->post('manufact');
                $model           = $request->post('model');
                $variant         = $request->post('variant');
                $transmission    = $request->post('transmission');
                $fuel            = $request->post('fuel');
                $engcap          = $request->post('engcap');
                $manu_year       = $request->post('manu_year');
                $dateofpurchase  = $request->post('dateofpurchase');
                $distance        = $request->post('distance');
                $registration    = $request->post('registration');
                $insure          = $request->post('insure');
                $renewalinsure   = $request->post('renewalinsure');
                $modification    = $request->post('modification');
                $lastservicedate = $request->post('lastservicedate');
                $kmlastservice   = $request->post('kmlastservice');
                $status          = $request->post('status');
                $oilchangedate   = $request->post('oilchangedate');
                $kmatoilchange   = $request->post('kmatoilchange');
                $calib           = $request->post('calib');
                $moniter         = $request->post('moniter');
                $city            = $request->post('city');
                $note            = $request->post('note');
                $chassis_num     = $request->post('chassis_num');
                $engine_num      = $request->post('engine_num');
                
                // validate
                if($manufact == '' || $model == '' || $variant == '' || $transmission == '' || $fuel == '' || $engcap == '' || $manu_year == '' || $dateofpurchase == '' || $distance == '' || $registration == '' ||  $lastservicedate == '' || $kmlastservice == '' || $status == '' || $oilchangedate == '' || $kmatoilchange == '' || $calib == '' || $moniter == '' || $city == '' )
                {
                    \Session::flash('message','Missing Some Fields.');
                    return redirect('add-vehicle/'.$dide);
                }

                $data = array(
                   'device'          => $did,
                   'model'           => $model,
                   'manufact'        => $manufact,
                   'varient'         => $variant,
                   'eng_cap'         => $engcap,
                   'fuel_tpe'        => $fuel,
                   'trans_type'      => $transmission,
                   'manufact_yr'     => $manu_year,
                   'purchase_dt'     => date('Y-m-d',strtotime($dateofpurchase)),
                   'travel_purchase' => $distance,
                   'reg_no'          => $registration,
                   'vehicle_insure'  => $insure,
                   'renew_dt_insure' => date('Y-m-d',strtotime($renewalinsure)),
                   'modific'         => $modification,
                   'last_serv_dt'    => date('Y-m-d',strtotime($lastservicedate)),
                   'travel_service'  => $kmlastservice,
                   'status'          => $status,
                   'oil_dt'          => date('Y-m-d',strtotime($oilchangedate)),
                   'oil_dist'        => $kmatoilchange,
                   'calib'           => $calib,
                   'monitor'         => $moniter,
                   'city'            => $city,
                   'note'            => $note,
                   'chassis_num'     => $chassis_num,
                   'engine_num'      => $engine_num,
                   'ad_by'           => session('userdata')['uid'],
                   'modelid'         => $model,
                   'varientid'       => $variant
                );
                Common::insert_vehicle('vehicles',$data);
                Common::updateTable('device',array('id' => $did),array('total_dist' => $distance));
                $return = $this->setvehSpec($manufact,$model,$fuel,$variant,$engcap,$did,$manu_year,$did,$city);
                if($return == true)
                {
                    \Session::flash('message','Vehicle Added Successfully.');
                    return redirect('device-list');
                }
                else
                {
                    \Session::flash('message','Vehicle Added but Sorry Callibration Failed, Please recheck the vehicle details.');
                    //return redirect('add-vehicle/'.$dide);
                    return redirect('add-vehicle/'.$dide);
                }
            }
        }
        else
        {
            session()->forget('form_token');
           \Session::flash('message','Sorry, token expired. Try again.');
            return redirect('add-vehicle/'.$dide);
        }
    }

    // Set vehicle specification
    public function setvehSpec($keyMfd,$keyMode,$fuel,$keyVar,$eng_cap,$deviceID,$mfg_yr,$isId="",$city="")
    {
        if($city != "")
        {
            $cityL    = Fetch::getVehicleSettingData();
            $cityList =  preg_split ('/$\R?^/m',$cityL->citylist);
            $cI       = ($city <= 0) ? 0 : $city - 1;
            $cty      = explode(',',$cityList[$cI]);
            $cityBp   = intval($cty[1]);
        }       

        $res     = Fetch::getVehicleSpecDataBYFields($keyMfd,$keyMode,$fuel,$keyVar,$eng_cap,$mfg_yr);
        $vehSett = Fetch::getVehicleSettingData();
        $cap     = explode(';',$vehSett->engine_capacity);
        
        $setSpec = array();
        
        if(!empty($res))
        {
            $data = $res;
            $arrayData = array();
            
            $arrayData = json_decode($data[0]->specinfo,true);
            foreach($arrayData as $key => $value)
            {
                //echo "setCaliPramVal($key]=$value); <br/>";
                $setSpec[$key] = $value;
            }
            $setSpec["MAP_Abs_PkLmt3_C"]  = $data[0]->map;
            $setSpec["MAP_Dif_PkLmt3_C"]  = $data[0]->map;
            $setSpec["Max_Torque_C"]      = $data[0]->maxtog;
            $setSpec["Eng_Disp_C"]        = $cap[$eng_cap];
            $setSpec["Baro_Press_City_C"] = $cityBp;
            $return = $this -> setCaliPramValue($setSpec,$deviceID);
            return $return;
        }
        else
        {
            $res = Fetch::getVehicleSpecDataBYFields1($keyMfd,$keyMode,$fuel,$keyVar,$eng_cap);

            if(!empty($res))
            {
                $data      = $res;
                $arrayData = array();
                
                $arrayData = json_decode($data[0]->specinfo,true);
                foreach($arrayData as $key => $value)
                {
                    $setSpec[$key] = $value;
                }
                $setSpec["MAP_Abs_PkLmt3_C"]  = $data[0]->map;
                $setSpec["MAP_Dif_PkLmt3_C"]  = $data[0]->map;
                $setSpec["Max_Torque_C"]      = $data[0]->maxtog;
                $setSpec["Eng_Disp_C"]        = $cap[$eng_cap];
                $setSpec["Baro_Press_City_C"] = $cityBp;
                $return = $this -> setCaliPramValue($setSpec,$deviceID);
                return $return;
            }
            else
            {
              
                $res  = Fetch::getVehicleSpecDataBYFields2($keyMfd,$keyMode);
                $data = $res;
                $arrayData = array();                           
                $arrayData = json_decode($data[0]->specinfo,true);
                foreach($arrayData as $key => $value)
                {
                    $setSpec[$key] = "";
                }
                // $setSpec["Max_Torque_C"]      = 1; // to clear all
                $setSpec["MAP_Abs_PkLmt3_C"]  = "";
                $setSpec["MAP_Dif_PkLmt3_C"]  = "";
                $setSpec["Max_Torque_C"]      = "";
                $setSpec["Eng_Disp_C"]        = $cap[$eng_cap];
                $setSpec["Baro_Press_City_C"] = $cityBp;
                $this->setCaliPramValue($setSpec,$deviceID);
                return false;
            }
        }
/*        return false;*/
    }

    public function setCaliPramValue($param,$deviceID)
    {
        $vehSett = Fetch::getVehicleSettingData();
        $vehicle = Fetch::getVehicleDeviceDataById($deviceID);

        //Get default values of calibration
        $pDVar   = ($vehicle[0]->fuel_tpe == 1) ? "p" : "d";
        $f1      = "calval$pDVar";
        $calval  = json_decode($vehSett->$f1,true);
        // print_r($param['MAP_Valid_C']);die;
        foreach($param as $key => $value)
        {
            $calval[$key] = $param[$key];
            // $calval[$key]  = (trim(!empty($param[$key]))) ? $param[$key] : $value;
        }

        $calval = json_encode($calval);
        Common::updateTable('vehicles',array('id' => $vehicle[0]->id),array('calval' => $calval));
        $return = $this -> setgetDeviceAction($deviceID,1);
        return $return;
    }

    public function setgetDeviceAction($did,$act,$type="")    
    {
        //to set flag for calibrataion label update added on 11/10/2017
        if($type == 1)
        {
            $dat = Fetch::getDactvalFromDeviceByID($did);
            return $dat[0]->dactval;
        }
        else
        {
            Common::updateTable('device',array('id' => $did),array('dactval' => '1'));
            return true;
        }
    }
    
    // Vehicle add new form loading select boxes manufacturer to model to varients start
    public function getModelList(Request $request){
        $mfd     = $request->post('mfd');
        $vehInfo = Fetch::getVehicleInfoDataById($mfd);
        if(count($vehInfo) >= 1)
        {
            $model = explode(';',$vehInfo[0]->model);
        }
        $data = '<option disabled="">Model</option>';
        foreach ($model as $key => $value) {
            $data.= '<option value="'.$key.'">'.$value.'</option>';
        }
        return $data;
    }

    public function getVarientList(Request $request){
        $mfd         = $request->post('mfd');
        $model       = $request->post('model');
        $vehInfo     = Fetch::getVehicleInfoDataById($mfd);
        if(count($vehInfo) >= 1)
        {
            $varList = explode(';',$vehInfo[0]->var);
        }
        $vehSpec    = Fetch::getVehicleSpecDataByMfdModel($mfd,$model);
        $varient = array();
        if(count($vehSpec) >= 1)
        {
            foreach($vehSpec as $vs)
            {
                $varient[$vs->varient] = $varList[$vs->varient];            
            }
        }

        $data = '<option disabled="">Varient</option>';
        foreach ($varient as $key => $value) {
            $data.= '<option value="'.$key.'">'.$value.'</option>';
        }
        return $data;
    }
    // Vehicle add new form loading select boxes manufacturer to model to varients end

    // New vehicle calibration update
    public function calibration($deviceID){
        $did            = Helper::crypt(2,$deviceID);
        $settingData    = Fetch::getVehicleSettingData();
        $vehicleData    = Fetch::getVehicleDataById($did);
        if(empty($vehicleData)){
            session(['msg' => 'Please Add Vehicle']);
           return redirect('add-vehicle/'.$deviceID);
           exit;
        }
        $calList        = Fetch::getSettingData();
        $data = array(
         'title'        => "Vehicle Calibration",
         'setting_data' => $settingData,
         'vehicle_data' => $vehicleData,
         'cal_list'     => $calList,
         'form_token'   => Helper::token(8) 
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);  
        return view('vehicle/vehiclecalibration')->with($data);
    }

    public function calibration_new($deviceID){
        $did            = Helper::crypt(2,$deviceID);
        $settingData    = Fetch::getVehicleSettingData();
        $vehicleData    = Fetch::getVehicleDataById($did);
        if(empty($vehicleData)){
            session(['msg' => 'Please Add Vehicle']);
           return redirect('add-vehicle/'.$deviceID);
           exit;
        }
        $calList        = Fetch::getSettingData();
        $data = array(
         'title'        => "Vehicle Calibration",
         'setting_data' => $settingData,
         'vehicle_data' => $vehicleData,
         'cal_list'     => $calList,
         'form_token'   => Helper::token(8) 
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);  
        return view('vehicle/vehiclecalibration_new')->with($data);
    }

    public function saveVehicleCalibration(Request $request){
        $token = request('form_token');
        $vehID = Helper::crypt(2,$request->post('vehId'));
        $did   = Helper::crypt(2,$request->post('did'));
        if($token == session('form_token')){
            session()->forget('form_token');
            $pDVar          = $request->post('pDVar');
            $calList        = Fetch::getSettingData();
            $callistN       = $calList->callist;
            $cal_list       = preg_split ('/$\R?^/m',$callistN);
            $arrayF         = array();
            for($i=0; $i<count($cal_list); $i++)
            {
                $var          = trim($cal_list[$i]);
                $arrayF[$var] = $request->post($var);
            }
            $calval           = json_encode($arrayF);
            $device_data=Fetch::getdevice1($did);
            Common::updateTable_vehicle('vehicles',array('id' => $vehID),array('calval' => $calval),$device_data[0]->device_id);
            $this->setgetDeviceAction($did,1);
            \Session::flash('message','Calibration Updated Successfully.');
            return redirect('device_manager/'.$request->post('did'));
        }else{
            session()->forget('form_token');
            \Session::flash('message','Sorry, token expired. Try again.');
            return redirect('device_manager/'.$request->post('did'));
        }
    }
}
