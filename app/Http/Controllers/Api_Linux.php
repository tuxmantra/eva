<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newuser;
use Illuminate\Support\Arr;
// use Mail;

class Api_Linux extends Controller
{
	public function get_devstatus(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$obj          = json_decode($request->data, TRUE);
			$deviceID     = $obj['devID'];
			$devicestatus = Fetch::devicestatus($deviceID);
			if(!empty($devicestatus))
          	{
			
				$app  = Fetch::appupdate($deviceID);
				$firm = Fetch::firmupdate($deviceID);
				// print_r($firm[0]->num);die;
				$result['success']         = TRUE;
				date_default_timezone_set('Asia/Kolkata');
				// $result['date']            = date('Y-m-d H:i:s');
				$result['date']            = time();
			    $result['status']          = $devicestatus[0]->status;
				$result['devConfigStatus'] = $devicestatus[0]->dactval;
				$bt_dev = Fetch::getbtdevicedetails($deviceID);
				
				if(($app[0]->num) > 0){
					$result['appUpdStatus']    = 1;
				}else{
					$result['appUpdStatus']    = 0;
				}

				if(($firm[0]->num) > 0){
					 $result['frmwrUpdStatus']  = 1;
				}else{
					 $result['frmwrUpdStatus']  = 0;
				}

				if($devicestatus[0]->dev_cmd_status){
					 $result['devCmdStatus']  = $devicestatus[0]->dev_cmd_status;
				}else{
					 $result['devCmdStatus']  = 0;
				}
				//Blutooth status and type
				if(count($bt_dev) == 1)
				{
					if($bt_dev[0]->bt_dev_make == 'Numato')
					{
					$result['btDevStatus'] = 3;
					}
					if($bt_dev[0]->bt_dev_make == 'MetaMotionR')
					{
				   	$result['btDevStatus'] = 5;
					}
				  
			    }else{
				   $result['btDevStatus'] = 7;
		  	   	}
		  	   	TODO: "We need to make it to update automatically";
		  	   	//Reset Drive ID - 0 -> No reset required, 1 -> reset required
		  	   	$result['resetDID'] = 0;
		  	}else{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
		    }
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function get_firmwareupdate(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$obj       = json_decode($request->data, TRUE);
			$deviceID  = $obj['devID'];
			$appupdate = Fetch::getfirmstatdetails($deviceID);
        	$appNames  = array();
			$appLinks  = array();
			$appVers   = array();
			$cmd       = array();
			$i         = 0;
			
			foreach ($appupdate as $key => $value) 
			{
				$appNames[] = $value->firm_name;
				if(!empty($value->lat_ver_filename)){
					$appLinks[] = $value->lat_ver_filename;
				}else{
					$appLinks[] = $value->lat_ver_path;
				}
				$appVers[] = $value->lat_ver;
				$cmd[]     = $value->cmd;
				$i++;
		   	}
        	$result['success']            = TRUE;
        	if($i == 0)
        	{
        		$result['installFirmStatus']  = 0;
        	}
        	else
        	{
        		$result['installFirmStatus']  = 1;
        	}
        	$result['installFirmCnt']     = $i;
		    $result['installFirmName']    = $appNames;
		    $result['installFirmLink']    = $appLinks;
		    $result['installFirmVersion'] = $appVers;
		    $result['installFirmcmd']     = $cmd;
		    // $result['uninstallAppStatus'] = 0;
		    // $result['uinistallAppName']  = "";
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function get_appupdate(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$obj       = json_decode($request->data, TRUE);
			$deviceID  = $obj['devID'];
			$appupdate = Fetch::getappstatdetails($deviceID);
        	$appNames  = array();
			$appLinks  = array();
			$appVers   = array();
			$i         = 0;
			// print_r($appupdate);
			foreach ($appupdate as $key => $value) 
			{
				$appNames[] = $value->app_name;
				// $appLinks[] = $value->lat_ver_path;
				if(!empty($value->lat_ver_filename)){
					$appLinks[] = $value->lat_ver_filename;
				}else{
					$appLinks[] = "$value->app_name".".update";
				}
				$appVers[] = $value->lat_ver;
				$i++;
		   	}

        	$result['success'] = TRUE;
        	if($i == 0)
        	{
        		$result['installAppStatus']  = 0;
        	}
        	else
        	{
        		$result['installAppStatus']  = 1;
        	}
        	
        	$result['installAppCnt']     = $i;
		    $result['installAppName']    = $appNames;
		    $result['installAppLink']    = $appLinks;
		    $result['installAppVersion'] = $appVers;
		    // $result['uninstallAppStatus'] = 0;
		    // $result['uinistallAppName']  = "";
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

	public function get_cmdupdate(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];

			$devicestatus = Fetch::devicestatus($deviceID);
			if(!empty($devicestatus))
          	{
	          	$result['success'] = TRUE;

	            $cmd = json_decode($devicestatus[0]->dev_cmds,TRUE);
                if(!empty($cmd))
                {
                    $keys   = array_keys($cmd);
                    $values = array_values($cmd);
                    // print_r($values);die;
                  	for($i=0;$i<count($values);$i++) 
                  	{
                  		$result[$keys[$i]]=$values[$i];
                  	}
              	}
          	}
          	else
          	{
	          	$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "No Commands!";
          	}
      	}
      	else
      	{
      	    $result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
      	}
      	$resultVal = json_encode($result,JSON_UNESCAPED_SLASHES);
		echo $resultVal;
  	}

  	public function get_bt1devconfig(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			
			$valueai = 0;
			$valuedi = 0;
			$valuedo = 0;
			$devicestatus = Fetch::devicestatus($deviceID);
			if(!empty($devicestatus))
			{
				$btData = Fetch::getbt1data($deviceID);
				// print_r($btData);die;
				for ($i = 0; $i < 8 ; $i++)
				{ 
					$column = "P".$i."type";
					if($btData[0]->{$column} == 'analog_input'){
						$valueai = $valueai + pow(2,$i);
					}
					if($btData[0]->{$column} == 'digital_input'){
						$valuedi = $valuedi + pow(2,$i);
					}
					if($btData[0]->{$column} == 'digital_output'){
						$valuedo = $valuedo + pow(2,$i);
					}
				}
				
				$result['success']               = TRUE;
				$result['mac-id']                = $btData[0]->mac_id;//"00:06:66:C2:21:A9";
				$result['analog_channel_config'] = $valueai;
				$result['digital_input_config']  = $valuedi;
				$result['digital_output_config'] = $valuedo;
				$result['api_endpoint']          = "/apiv4/bt1dataupload.php";
			}
			else
	      	{
	      	    $result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
	      	}
        }
      	else
      	{
      	    $result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
      	}
      	$resultVal = json_encode($result,JSON_UNESCAPED_SLASHES);
		echo $resultVal;
  	}

  	public function frmwrupdres(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$obj        = json_decode($datMe, TRUE);
			$deviceID   = $obj['devID'];
			$frmName    = $obj['frmName'];
			$frmVersion = $obj['frmVersion'];
			$frmStatus  = $obj['frmStatus'];
			
			for($i = 0;$i < count($frmStatus);$i++)
			{
				if($frmStatus[$i] == 1)
				{
					$st     = 0;
					$update = Fetch::updatefirmres($deviceID,$frmName[$i],$frmVersion[$i],$st);
				}
				else
				{
					$st = 1;
				}
			    // $update=Fetch::updatefirmres1($deviceID);
		    }
			// $update=Fetch::updatefirmres($deviceID,$frmName,$frmVersion,$frmStatus);
        	$result['success']    = TRUE;
        	$result['successmsg'] = "Data Upload Successful!";
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function appupdres(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$obj        = json_decode($datMe, TRUE);

			$deviceID   = $obj['devID'];
			$appName    = $obj['appName'];
			$appVersion = $obj['appVersion'];
			$appStatus  = $obj['appStatus'];
			// print_r($appName);die;
			for($i = 0;$i < count($appStatus);$i++)
			{
				if($appStatus[$i] == 1)
				{
					$st     = 0;
					$update = Fetch::updateappres($deviceID,$appName[$i],$appVersion[$i],$st);
				}
				else
				{
					$st = 1;
				}
				// $update=Fetch::updateappres1($deviceID);
		    }
		    
		    if (in_array("0", $appStatus))
		    {

		    }
		    else
		    {
		     	$update = Fetch::updateappres1($deviceID);
		    }
        	$result['success']    = TRUE;
        	$result['successmsg'] = "Data Upload Successful!";
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function cmdres(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$obj        = json_decode($datMe, TRUE);
			$deviceID   = $obj['devID'];
			$cmdStatus  = $obj['cmdStatus'];
			$cmdMsg     = $obj['cmdMsg'];
			if($cmdStatus == 1)
			{
             	$update = Fetch::updatecmdapi($deviceID,$cmdMsg);
             	$result['success']    = TRUE;
        		$result['successmsg'] = "Data Upload Successful!";
			}
			else
			{
				$update = Fetch::updatecmdfail($deviceID,$cmdMsg);
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Response Update Failed!";
			}
			
			// if($update)
			// {
				
			// }
   //      	else
   //      	{
        		
   //      	}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function bt1updres(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$obj       = json_decode($datMe, TRUE);
			$deviceID  = $obj['devID'];
			$bt1Status = $obj['bt1Status'];
			$bt1Msg    = $obj['bt1Msg'];
			
			if($bt1Status == 1)
			{
             	$update = Fetch::updatebtapi($deviceID,$bt1Msg);
			}
			else
			{
				$update = Fetch::updatebtfail($deviceID,$bt1Msg);
			}
			
			if($update)
			{
				$result['success']    = TRUE;
        		$result['successmsg'] = "Data Upload Successful!";
			}
        	else
        	{
        		$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Response Update Failed!";
        	}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function get_devconfig2w(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];

        	// Get Device Data
			$devInfo = Fetch::devicestatus($deviceID);
			// print_r($devInfo);die;
			$encryptKey = rand(25, 300);
			if(!empty($devInfo))
			{
				$dataArr = array();
				$data    = array();
				
				// Get Vehicle details
				$did   = $devInfo[0]->id;
				$vehi  = Fetch::getVehicleDataById($did);
				// print_r($vehi);die;
				$vType = $vehi->fuel_tpe;

				$dataArr  = json_decode($vehi->calval);
				$data[0]  = $vType;
				$data[1]  = $dataArr->Eng_eff_C;
				$data[2]  = $dataArr->Eng_Disp_C * 1000;
				if($dataArr->TC_Status_C == "Yes")
				{
					$data[3] = 1;
				}
				else
				{
					$data[3] = 0;
				}
				if($dataArr->Injection_Type_C == "Direct")
				{
					$data[4] = 1;
				}
				else
				{
					$data[4] = 0;
				}
				$data[5]   = round(($dataArr->Eff_RPM_Limit1_C / 50),0);
				$data[6]   = round(($dataArr->Eff_RPM_Limit2_C / 50),0);
				$data[7]   = round(($dataArr->Eff_RPM_Limit3_C / 50),0);
				$data[8]   = round(($dataArr->Eff_RPM_Limit4_C / 50),0);
				$data[9]   = round(($dataArr->Eff_RPM_Limit5_C / 50),0);

				//Equivalence Ratio
				$data[10] = (round(($dataArr->Eqratio_Eload_1_10_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_1_10_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_1_10_RPMLimit3_C * 200),0)) << 16) ;
				$data[11] = (round(($dataArr->Eqratio_Eload_1_10_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_1_10_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_11_20_RPMLimit1_C * 200),0)) << 16); 
				$data[12] = (round(($dataArr->Eqratio_Eload_11_20_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_11_20_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_11_20_RPMLimit4_C * 200),0)) << 16);
				$data[13] = (round(($dataArr->Eqratio_Eload_11_20_RPMLimit5_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit1_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit2_C * 200),0)) << 16);
				$data[14] = (round(($dataArr->Eqratio_Eload_21_30_RPMLimit3_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit4_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit5_C * 200),0)) << 16);	
				$data[15] = (round(($dataArr->Eqratio_Eload_31_40_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_31_40_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_31_40_RPMLimit3_C * 200),0)) << 16) ;
				$data[16] = (round(($dataArr->Eqratio_Eload_31_40_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_31_40_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_41_50_RPMLimit1_C * 200),0)) << 16);
				$data[17] = (round(($dataArr->Eqratio_Eload_41_50_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_41_50_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_41_50_RPMLimit4_C * 200),0)) << 16);
				$data[18] = (round(($dataArr->Eqratio_Eload_41_50_RPMLimit5_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit1_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit2_C * 200),0)) << 16);
				$data[19] = (round(($dataArr->Eqratio_Eload_51_60_RPMLimit3_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit4_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit5_C * 200),0)) << 16);
				$data[20] = (round(($dataArr->Eqratio_Eload_61_80_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_61_80_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_61_80_RPMLimit3_C * 200),0)) << 16);
				$data[21] = (round(($dataArr->Eqratio_Eload_61_80_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_61_80_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_81_90_RPMLimit1_C * 200),0)) << 16);
				$data[22] = (round(($dataArr->Eqratio_Eload_81_90_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_81_90_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_81_90_RPMLimit4_C * 200),0)) << 16);
				$data[23] = (round(($dataArr->Eqratio_Eload_81_90_RPMLimit5_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit1_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit2_C * 200),0)) << 16);
				$data[24] = (round(($dataArr->Eqratio_Eload_91_94_RPMLimit3_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit4_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit5_C * 200),0)) << 16);
				$data[25] = (round(($dataArr->Eqratio_Eload_95_98_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_95_98_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_95_98_RPMLimit3_C * 200),0)) << 16);
				$data[26] = (round(($dataArr->Eqratio_Eload_95_98_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_95_98_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_99_100_RPMLimit1_C * 200),0)) << 16);
				$data[27] = (round(($dataArr->Eqratio_Eload_99_100_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_99_100_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_99_100_RPMLimit4_C * 200),0)) << 16);
				$data[28] = (round(($dataArr->Eqratio_Eload_99_100_RPMLimit5_C * 200),0)) | ((round(($dataArr->Ovr_Run_Cold_EqRatio_C * 100),0)) << 8) | ((round(($dataArr->Ovr_Run_Hot_EqRatio_C * 100),0)) << 16);

				// Engine Efficiency
				$data[29] = ($dataArr->EngEff_Eload_1_10_RPMLimit1_C) | (($dataArr->EngEff_Eload_1_10_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_1_10_RPMLimit3_C) << 16) ;
				$data[30] = ($dataArr->EngEff_Eload_1_10_RPMLimit4_C) | (($dataArr->EngEff_Eload_1_10_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_11_20_RPMLimit1_C) << 16); 
				$data[31] = ($dataArr->EngEff_Eload_11_20_RPMLimit2_C) | (($dataArr->EngEff_Eload_11_20_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_11_20_RPMLimit4_C) << 16);
				$data[32] = ($dataArr->EngEff_Eload_11_20_RPMLimit5_C) | (($dataArr->EngEff_Eload_21_30_RPMLimit1_C) << 8) | (($dataArr->EngEff_Eload_21_30_RPMLimit2_C) << 16);
				$data[33] = ($dataArr->EngEff_Eload_21_30_RPMLimit3_C) | (($dataArr->EngEff_Eload_21_30_RPMLimit4_C) << 8) | (($dataArr->EngEff_Eload_21_30_RPMLimit5_C) << 16);	
				$data[34] = ($dataArr->EngEff_Eload_31_40_RPMLimit1_C) | (($dataArr->EngEff_Eload_31_40_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_31_40_RPMLimit3_C) << 16) ;
				$data[35] = ($dataArr->EngEff_Eload_31_40_RPMLimit4_C) | (($dataArr->EngEff_Eload_31_40_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_41_50_RPMLimit1_C) << 16); 
				$data[36] = ($dataArr->EngEff_Eload_41_50_RPMLimit2_C) | (($dataArr->EngEff_Eload_41_50_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_41_50_RPMLimit4_C) << 16);
				$data[37] = ($dataArr->EngEff_Eload_41_50_RPMLimit5_C) | (($dataArr->EngEff_Eload_51_60_RPMLimit1_C) << 8) | (($dataArr->EngEff_Eload_51_60_RPMLimit2_C) << 16);
				$data[38] = ($dataArr->EngEff_Eload_51_60_RPMLimit3_C) | (($dataArr->EngEff_Eload_51_60_RPMLimit4_C) << 8) | (($dataArr->EngEff_Eload_51_60_RPMLimit5_C) << 16);
				$data[39] = ($dataArr->EngEff_Eload_61_80_RPMLimit1_C) | (($dataArr->EngEff_Eload_61_80_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_61_80_RPMLimit3_C) << 16);
				$data[40] = ($dataArr->EngEff_Eload_61_80_RPMLimit4_C) | (($dataArr->EngEff_Eload_61_80_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_81_90_RPMLimit1_C) << 16);
				$data[41] = ($dataArr->EngEff_Eload_81_90_RPMLimit2_C) | (($dataArr->EngEff_Eload_81_90_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_81_90_RPMLimit4_C) << 16);
				$data[42] = ($dataArr->EngEff_Eload_81_90_RPMLimit5_C) | (($dataArr->EngEff_Eload_91_94_RPMLimit1_C) << 8) | (($dataArr->EngEff_Eload_91_94_RPMLimit2_C) << 16);
				$data[43] = ($dataArr->EngEff_Eload_91_94_RPMLimit3_C) | (($dataArr->EngEff_Eload_91_94_RPMLimit4_C) << 8) | (($dataArr->EngEff_Eload_91_94_RPMLimit5_C) << 16);
				$data[44] = ($dataArr->EngEff_Eload_95_98_RPMLimit1_C) | (($dataArr->EngEff_Eload_95_98_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_95_98_RPMLimit3_C) << 16);
				$data[45] = ($dataArr->EngEff_Eload_95_98_RPMLimit4_C) | (($dataArr->EngEff_Eload_95_98_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_99_100_RPMLimit1_C) << 16);
				$data[46] = ($dataArr->EngEff_Eload_99_100_RPMLimit2_C) | (($dataArr->EngEff_Eload_99_100_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_99_100_RPMLimit4_C) << 16);
				$data[47] = ($dataArr->EngEff_Eload_99_100_RPMLimit5_C);

				// Volumetric Efficiency
				$data[48] = ($dataArr->VolEff_Eload_1_10_RPMLimit1_C) | (($dataArr->VolEff_Eload_1_10_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_1_10_RPMLimit3_C) << 16) ;
				$data[49] = ($dataArr->VolEff_Eload_1_10_RPMLimit4_C) | (($dataArr->VolEff_Eload_1_10_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_11_20_RPMLimit1_C) << 16); 
				$data[50] = ($dataArr->VolEff_Eload_11_20_RPMLimit2_C) | (($dataArr->VolEff_Eload_11_20_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_11_20_RPMLimit4_C) << 16);
				$data[51] = ($dataArr->VolEff_Eload_11_20_RPMLimit5_C) | (($dataArr->VolEff_Eload_21_30_RPMLimit1_C) << 8) | (($dataArr->VolEff_Eload_21_30_RPMLimit2_C) << 16);
				$data[52] = ($dataArr->VolEff_Eload_21_30_RPMLimit3_C) | (($dataArr->VolEff_Eload_21_30_RPMLimit4_C) << 8) | (($dataArr->VolEff_Eload_21_30_RPMLimit5_C) << 16);	
				$data[53] = ($dataArr->VolEff_Eload_31_40_RPMLimit1_C) | (($dataArr->VolEff_Eload_31_40_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_31_40_RPMLimit3_C) << 16) ;
				$data[54] = ($dataArr->VolEff_Eload_31_40_RPMLimit4_C) | (($dataArr->VolEff_Eload_31_40_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_41_50_RPMLimit1_C) << 16); 
				$data[55] = ($dataArr->VolEff_Eload_41_50_RPMLimit2_C) | (($dataArr->VolEff_Eload_41_50_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_41_50_RPMLimit4_C) << 16);
				$data[56] = ($dataArr->VolEff_Eload_41_50_RPMLimit5_C) | (($dataArr->VolEff_Eload_51_60_RPMLimit1_C) << 8) | (($dataArr->VolEff_Eload_51_60_RPMLimit2_C) << 16);
				$data[57] = ($dataArr->VolEff_Eload_51_60_RPMLimit3_C) | (($dataArr->VolEff_Eload_51_60_RPMLimit4_C) << 8) | (($dataArr->VolEff_Eload_51_60_RPMLimit5_C) << 16);
				$data[58] = ($dataArr->VolEff_Eload_61_80_RPMLimit1_C) | (($dataArr->VolEff_Eload_61_80_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_61_80_RPMLimit3_C) << 16);
				$data[59] = ($dataArr->VolEff_Eload_61_80_RPMLimit4_C) | (($dataArr->VolEff_Eload_61_80_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_81_90_RPMLimit1_C) << 16);
				$data[60] = ($dataArr->VolEff_Eload_81_90_RPMLimit2_C) | (($dataArr->VolEff_Eload_81_90_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_81_90_RPMLimit4_C) << 16);
				$data[61] = ($dataArr->VolEff_Eload_81_90_RPMLimit5_C) | (($dataArr->VolEff_Eload_91_94_RPMLimit1_C) << 8) | (($dataArr->VolEff_Eload_91_94_RPMLimit2_C) << 16);
				$data[62] = ($dataArr->VolEff_Eload_91_94_RPMLimit3_C) | (($dataArr->VolEff_Eload_91_94_RPMLimit4_C) << 8) | (($dataArr->VolEff_Eload_91_94_RPMLimit5_C) << 16);
				$data[63] = ($dataArr->VolEff_Eload_95_98_RPMLimit1_C) | (($dataArr->VolEff_Eload_95_98_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_95_98_RPMLimit3_C) << 16);
				$data[64] = ($dataArr->VolEff_Eload_95_98_RPMLimit4_C) | (($dataArr->VolEff_Eload_95_98_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_99_100_RPMLimit1_C) << 16);
				$data[65] = ($dataArr->VolEff_Eload_99_100_RPMLimit2_C) | (($dataArr->VolEff_Eload_99_100_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_99_100_RPMLimit4_C) << 16);
				$data[66] = ($dataArr->VolEff_Eload_99_100_RPMLimit5_C) | (($dataArr->Ovr_Run_Cold_VolEff_C) << 8) | (($dataArr->Ovr_Run_Hot_VolEff_C) << 16);
				
				$data[67]  = 0;
				$data[68]  = 0;
				$data[69]  = 0;
				$data[70]  = 0;
				$data[71]  = 0;
				$data[72]  = 0;
				$data[73]  = 0;
				$data[74]  = 0;
				$data[75]  = 0;
				$data[76]  = 0;
				$data[77]  = 0;
				$data[78]  = 0;
				$data[79]  = 0;
				$data[80]  = 0;
				$data[81]  = 0;
				$data[82]  = 0;
				$data[83]  = 64;
				$data[84]  = $encryptKey;
				$data[85]  = 0;
				$data[86]  = 0;
				$data[87]  = 0;
				$data[88]  = 0;
				$data[89]  = 0;
				$data[90]  = 0;
				$data[91]  = 0;
				$data[92]  = 0;
				$data[93]  = 0;
				$data[94]  = 0;
				$data[95]  = 0;
				$data[96]  = 0;
				$data[97]  = 0;
				$data[98]  = 0;
				$data[99]  = 0;
				$data[100] = 0;
				$data[101] = 0;
				$data[102] = 0;
				$data[103] = 0;
				$data[104] = 0;
				$data[105] = 0;
				$data[106] = 0;
				$data[107] = 0;
				$data[108] = 0;
				$data[109] = 0;
				$data[110] = 0;
				$data[111] = 0;
				$data[112] = 0;
				$data[113] = 0;
				$data[114] = 0;
				$data[115] = 0;
				$data[116] = 0;
				$data[117] = 0;
				$data[118] = 0;
				$data[119] = 0;
				$data[120] = 0;
				$data[121] = 0;
				$data[122] = 0;
				$data[123] = 0;
				$data[124] = $dataArr->MAP_Valid_C;
				$data[125] = $dataArr->MAF_Valid_C;
				$data[126] = $dataArr->Baro_Press_City_C;
				$data[127] = $dataArr->MAP_Abs_PkLmt3_C;
				$data[128] = round(($dataArr->Accl_Val_C * 20),0);
				$data[129] = $dataArr->Accl_Eload_C;
				$data[130] =round(( $dataArr->Decl_Val_C * 20),0);
				$data[131] = $dataArr->Eload_Def_C;
				$data[132] = $dataArr->Eload_Max_C;
				$data[133] = round(($dataArr->Max_Torque_C / 5),0);
				$data[134] = $dataArr->ELoad_Idle_Warm_C;
				$data[135] = $dataArr->ELoad_Idle_Ld1_C;
				$data[136] = $dataArr->ELoad_Idle_Ld2_C;
				$data[137] = $dataArr->Osc_EloadOffset_Lmt_C;
				$data[138] = $dataArr->Cool_Lmt_C;
				$data[139] = round(($dataArr->Osc_RPM_Lmt1_C / 50),0);
				$data[140] = round(($dataArr->Osc_RPM_Lmt2_C / 50),0);
				$data[141] = round(($dataArr->Osc_RPM_Lmt3_C / 50),0);
				$data[142] = round(($dataArr->Osc_RPM_Lmt4_C / 50),0);
				$data[143] = round(($dataArr->CRP_RPM_PkLmt_C / 50),0);
				$data[144] = round(($dataArr->CRP_Type_C / 1000),0);
				$data[145] = round(($dataArr->CRP_MaxLmt3_C / 500),0);
				$data[146] = round(($dataArr->CRP_MaxLmt1_C / 500),0);
				$data[147] = $dataArr->Base_OvrHt_Cool_Lmt_C;
				$data[148] = $dataArr->Cool_Temp_Lmt1_C;
				$data[149] = $dataArr->Cool_Temp_Lmt2_C;
				$data[150] = $dataArr->Cool_Temp_Lmt3_C;
				$data[151] = $dataArr->Cool_Temp_Lmt4_C;
				$data[152] = round(($dataArr->RPM_Idle_Warm_C / 50),0);
				$data[153] = round(($dataArr->RPM_Idle_Ld1_C / 50),0);
				$data[154] = round(($dataArr->RPM_Idle_Ld2_C / 50),0);
				$data[155] = $dataArr->MAP_RPMOffset_PkLmt_C;
				$data[156] = round(($dataArr->Max_Air_Trq_RPM_C / 50),0);
				$data[157] = round(($dataArr->MAP_Air_RPM_LwrLmt_C / 50),0);
				$data[158] = round(($dataArr->MAP_Air_RPM_PkLmt_C / 50),0);
				$data[159] = $dataArr->MAP_Air_EloadOffset_PkLmt_C;
				$data[160] = $dataArr->MAP_Air_IdleLmt3_C;
				$data[161] = $dataArr->MAP_Air_IdleLmt1_C;
				$data[162] = $dataArr->MAP_Air_PkLmt3_C;
				$data[163] = $dataArr->MAP_Air_PkLmt1_C;
				$data[164] = round(($dataArr->Time_Upld_Frq_C / 3),0);
				$data[165] = $dataArr->gear_Confidence_C;
				$data[166] = $dataArr->gear_Fact_C;
				$data[167] = round(($dataArr->gr_1_low_C  * 10000),0);
				$data[168] = round(($dataArr->gr_1_high_C * 10000),0);
				$data[169] = round(($dataArr->gr_2_low_C  * 10000),0);
				$data[170] = round(($dataArr->gr_2_high_C * 10000),0);
				$data[171] = round(($dataArr->gr_3_low_C  * 10000),0);
				$data[172] = round(($dataArr->gr_3_high_C * 10000),0);
				$data[173] = round(($dataArr->gr_4_low_C  * 10000),0);
				$data[174] = round(($dataArr->gr_4_high_C * 10000),0);
				$data[175] = round(($dataArr->gr_5_low_C  * 10000),0);
				$data[176] = round(($dataArr->gr_5_high_C * 10000),0);
				$data[177] = round(($dataArr->gr_6_low_C  * 10000),0);
				$data[178] = round(($dataArr->gr_6_high_C * 10000),0);
				$data[179] = $dataArr->Eng_Rev_Lmt_C;
				$data[180] = $dataArr->Cool_Temp_AvgLmt_C;
				$data[181] = $dataArr->Gr_RPM_Col0_C;
				$data[182] = $dataArr->Gr_RPM_Col1_C;
				$data[183] = $dataArr->Gr_RPM_Col2_C;
				$data[184] = $dataArr->Gr_RPM_Col3_C;
				$data[185] = $dataArr->Gr_RPM_Col4_C;
				$data[186] = $dataArr->Gr_RPM_Col5_C;
				$data[187] = $dataArr->Gr_RPM_Col6_C;
				$data[188] = $dataArr->Gr_RPM_Col7_C;
				$data[189] = $dataArr->Gr_RPM_Col8_C;
				$data[190] = $dataArr->Gr_RPM_Col9_C;
				$data[191] = $dataArr->Thr_RPM_Col0_C;
				$data[192] = $dataArr->Thr_RPM_Col1_C;
				$data[193] = $dataArr->Thr_RPM_Col2_C;
				$data[194] = $dataArr->Thr_RPM_Col3_C;
				$data[195] = $dataArr->Thr_RPM_Col4_C;
				$data[196] = $dataArr->Thr_RPM_Col5_C;
				$data[197] = $dataArr->Thr_RPM_Col6_C;
				$data[198] = $dataArr->Thr_RPM_Col7_C;
				$data[199] = $dataArr->Thr_RPM_Col8_C;
				$data[200] = $dataArr->Thr_RPM_Col9_C;
				$data[201] = $dataArr->Thr_Pos_Row0_C;
				$data[202] = $dataArr->Thr_Pos_Row1_C;
				$data[203] = $dataArr->Thr_Pos_Row2_C;
				$data[204] = $dataArr->Thr_Pos_Row3_C;
				$data[205] = $dataArr->Thr_Pos_Row4_C;
				$data[206] = $dataArr->Thr_Pos_Row5_C;
				$data[207] = $dataArr->Thr_Pos_Row6_C;
				$data[208] = $dataArr->Thr_Pos_Row7_C;
				$data[209] = $dataArr->Thr_Pos_Row8_C;
				$data[210] = $dataArr->Thr_Pos_Row9_C;
				$data[211] = $dataArr->Veh_Spd_Col0_C;
				$data[212] = $dataArr->Veh_Spd_Col1_C;
				$data[213] = $dataArr->Veh_Spd_Col2_C;
				$data[214] = $dataArr->Veh_Spd_Col3_C;
				$data[215] = $dataArr->Veh_Spd_Col4_C;
				$data[216] = $dataArr->Veh_Spd_Col5_C;
				$data[217] = $dataArr->Veh_Spd_Col6_C;
				$data[218] = $dataArr->Veh_Spd_Col7_C;
				$data[219] = $dataArr->Veh_Spd_Col8_C;
				$data[220] = $dataArr->Veh_Spd_Col9_C;
				$data[221] = round(($dataArr->Spd_Chng_Neg_Row5_C * 20),0);
				$data[222] = round(($dataArr->Spd_Chng_Neg_Row4_C * 20),0);
				$data[223] = round(($dataArr->Spd_Chng_Neg_Row3_C * 20),0);
				$data[224] = round(($dataArr->Spd_Chng_Neg_Row2_C * 20),0);
				$data[225] = round(($dataArr->Spd_Chng_Neg_Row1_C * 20),0);
				$data[226] = round(($dataArr->Spd_Chng_Row0_C * 20),0);
				$data[227] = round(($dataArr->Spd_Chng_Pos_Row1_C * 20),0);
				$data[228] = round(($dataArr->Spd_Chng_Pos_Row2_C * 20),0);
				$data[229] = round(($dataArr->Spd_Chng_Pos_Row3_C * 20),0);
				$data[230] = round(($dataArr->Spd_Chng_Pos_Row4_C * 20),0);
				$data[231] = round(($dataArr->Spd_Chng_Pos_Row5_C * 20),0);
				$data[232] = $dataArr->Ovr_Spd_Lmt_C;
				$data[233] = round(($dataArr->Brake_EngSt3_Acc_C * 20),0);
				$data[234] = round(($dataArr->Brake_EngSt4_Acc_C * 20),0);
				$data[235] = $dataArr->Hlf_Clutch_Thr_C;
				$data[236] = $dataArr->Gr_Cnt_Debounce_C;
				$data[237] = $dataArr->Veh_Spd_Avl_C;
				$data[238] = $dataArr->Trq_RPM_RSE_C;
				$data[239] = $dataArr->Trq_Eload_RSE_C;
				$data[240] = $dataArr->Accln_Debounce_C;
				$data[241] = $dataArr->GPS_Spd_Plaus_C;
				$data[242] = 0;
				$data[243] = 0;
				$data[244] = 0;
				$data[245] = 0;
				$data[246] = 0;
				$data[247] = 0;
				$data[248] = 0;
				$data[249] = 0;
				
				$resultArr = array();
				$resultArr = json_decode(json_encode($data,JSON_NUMERIC_CHECK));
				// print_r($resultArr);die;

				//TODO: Update config status in DB
				Common::updateTable('device',array('id' => $did),array('dactval' => '0'));

				$result['success'] = TRUE;
				$result['data']    = array_values($resultArr);
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result,JSON_NUMERIC_CHECK);
		echo $resultVal;
  	}

  	public function posdataupload(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$obj      = json_decode($datMe, TRUE);
			$deviceID = $obj['devID'];
			
			// Get Device Data
			$devInfo = Fetch::devicestatus($deviceID);
			if(!empty($devInfo))
			{
				$postval['lat']     = $obj['lat'];
				$postval['lon']     = $obj['lon'];
				$postval['alt']     = $obj['alt'];
				$postval['time_s']  = $obj['ts'];
				$postval['nodrive'] = $obj['dn'];
				$postval['seqno']   = $obj['s'];
				$postval['ad_dt']   = date('Y-m-d H:i:s',time());


				$update = Fetch::insertDbSingle_event($postval);
				if($update)
				{
					$result['success']    = TRUE;
        			$result['successmsg'] = "Data Upload Successful!";
				}
				else
				{
					$result['success']      = FALSE;
					$result['errorCode']    = "1003";
					$result['errorMessage'] = "Update Failed! Please try again later.";
				}
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function rawdataupload(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
		    //If there are empty strings in the incoming JSON, json_decode will fail. Find and replace empty string with 0
		    $json     = str_replace(",,", ",0,", $datMe);
			$array    = json_decode($json, true);
			$deviceID = $array['devID'];

		    // Get Device Data
			$devInfo = Fetch::devicestatus($deviceID);
			// print_r($devInfo);die;
			if(!empty($devInfo))
			{
				$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/devrawdataupload_back_linux.php '$datMe'";
				exec($cmd . " > /dev/null &");
				
				$result['success'] = TRUE;
				$result['status']  = "File Uploaded and database updating!";
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function eodDataUpload(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
		    //If there are empty strings in the incoming JSON, json_decode will fail. Find and replace empty string with 0
		    $json     = str_replace(",,", ",0,", $datMe);
			$array    = json_decode($json, true);
			$deviceID = $array['devID'];
		    // print_r($array);die;

		    // Get Device Data
			$devInfo = Fetch::devicestatus($deviceID);
			if(!empty($devInfo))
			{
				$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/eod_data_process_linux.php '$datMe'";
				exec($cmd . " > /dev/null &");
				
				$result['success'] = TRUE;
				$result['status']  = "File Uploaded and database updating!";
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function bt1dataupload(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	//If there are empty strings in the incoming JSON, json_decode will fail. Find and replace empty string with 0
		    $json     = str_replace(",,", ",0,", $datMe);
			$array    = json_decode($json, true);
			$deviceID = $array['devID'];
		    
		    // Get Device Data
			$devInfo = Fetch::devicestatus($deviceID);
			if(!empty($devInfo))
			{
				//get pin operraters
				$result = array();
				$table  = '';
				$qu     = '';
				$betdev = Fetch::getValuePartList1($deviceID);
				date_default_timezone_set("Asia/Kolkata");
				for ($i = 0; $i < count($array['btData'])  ; $i++)
				{
					$cal     = '';
					$result1 = array();
					for ($j = 0; $j < count($array['btData'][$i])  ; $j++) 
					{
						$k = '';
						if($j == 0)
						{
							if($array['btData'][$i][$j] == 0)
							{
								$time_s = time() + $i;
							}else{
								$time_s = (($array['btData'][$i][$j] + 946684800));
						    }

							$result1[] = $deviceID;
							$result1[] = $time_s * 1000;                      			//time_s
							$result1[] = date('Y-m-d H:i:s',substr($time_s,0,10));   	// time_v
							$result1[] = date('Y-m-d H:i:s',time());
           				    $result1[] = date('Y-m-d',substr($time_s,0,10));
							$result1[] = $array['dn'];                        			//fsprinf
							$time_v    = date('Y-m-d H:i:s',substr($time_s,0,10));
							
			        	    $dateElements = explode('-', $time_v);
							$date         = $dateElements[1];
							if($date == '03'){
								$table = 'dd_mar';
							}
							elseif($date == '04'){
								$table = 'dd_apr';
							}
							elseif($date == '05'){
								$table = 'dd_may';
							}
							elseif($date == '06'){
								$table = 'dd_june';
							}
							elseif($date == '07'){
								$table = 'dd_july';
							}
							elseif($date == '08'){
								$table = 'dd_aug';
							}
							elseif($date == '09'){
								$table = 'dd_sep';
							}
							elseif($date == '10'){
								$table = 'dd_oct';
							}
							elseif($date == '11'){
								$table = 'dd_nov';
							}
							elseif($date == '12'){
								$table = 'dd_dec';
							}
							elseif($date == '01'){
								$table = 'dd_jan';
							}
							elseif($date == '02'){
								$table = 'dd_feb';
							}
						}
						else
						{
							$k   = $j - 1;
							$op  = 'P'.$k.'op';
							$val = 'P'.$k.'val';
							if(!empty($betdev))
							{ 
								$oper  = $betdev[0]->{$op};
								$value = $betdev[0]->{$val};
								if(!empty($oper) && !empty($value)){
									eval('$cal = ' . $array['btData'][$i][$j].$oper.$value . ';');
								}else{
							    	$cal = $array['btData'][$i][$j];
								}
							}else{
								$cal = $array['btData'][$i][$j];
							}
							$result1[] = $cal;
						}
				    }
				    // $result[]=$result1;
				   	if($i == 0)
				   	{
				    	$qu.= "('".implode("','",$result1)."')";
					}else{
						$qu.= ",('".implode("','",$result1)."')";
					}
				} 

               	$queryM  = 'insert into '.$table.'(device_id,time_s,time_v,ad_dt,ad_dtin,Fsprint,P0,P1,P2,P3,P4,P5,P6,P7) VALUES'.$qu;
            	// print_r($queryM);die;
               	$insert = Fetch::insertbt_data($queryM);
				
				if($insert){
					$result['success']      = TRUE;
					$result['status']       = "Data Uploaded Successfully!";
				}else{
					$result['success']      = FALSE;
					$result['errorCode']    = "1003";
					$result['errorMessage'] = "Error in Uploading Data!";
				}
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function dtcdataupload(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$obj      = json_decode($datMe, TRUE);
			$deviceID = $obj['devID'];
			$dtc      = $obj['dtc'];

			// Get Device Data
			$devInfo = Fetch::devicestatus($deviceID);
			if(!empty($devInfo))
			{
				$update = Fetch::updatedeviceDtcv($deviceID,$dtc,1);
				if($update)
				{
					$result['success']    = TRUE;
        			$result['successmsg'] = "Data Upload Successful!";
				}
				else
				{
					$result['success']      = FALSE;
					$result['errorCode']    = "1003";
					$result['errorMessage'] = "Update Failed! Please try again later.";
				}
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal;
  	}

}
?>