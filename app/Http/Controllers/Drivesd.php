<?php
    
    namespace App\Http\Controllers;
    
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Mail;
    use Helper;
    use App\Model\Common;
    use App\Model\Fetch;
    
    class Drivesd_old extends Controller
    {
        public function index(){
            
            $user                = session('userdata');
            $data['user_type']   = $user['utype'];
            $deviceList          = Fetch::getDeviceListByUserType($user['uid'],$user['ucategory']);
            $did                 = $deviceList[0]->device_id;
            $date                = date('Y-m-d');
            $driveDates          = Fetch::getDriveDatesByDeviceIdEvent($did);
            $driveLatLong        = Fetch::getDriveLatLongByIdEvent($did,$date);
            
            $string              = '';
            $start               = "";
            if(!empty($driveLatLong)){
                $i=1;
                foreach($driveLatLong as $key => $list){
                    
                    if($key == 0){
                        $start.=  "{lat: $list->lat, lng: $list->lon}";
                    }
                    
                    $string.=  "{lat: $list->lat, lng: $list->lon},";
                    $i++;
                }
            }
            
            $data_array          = "";
            foreach($driveDates as $sdt){
                $data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',";
            }

            $data = array(
              'title'            => 'Drive',
              'device_list'      => $deviceList,
              'user_category'    => $user['ucategory'],
              'did'              => $did,
              'drive_dates'      => rtrim($data_array,','),
              'drive_latlon'     => rtrim($string,','),
              'start_point'      => $start
            );
            $data['drivesd']  = '1';
            $data['menu']     = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
            return view('drivesd/index')->with($data);
        }
        
        public function getSdDriveLoad(Request $request)
        {
            // date_default_timezone_set("Asia/Kolkata");
            $did    = $request->post('did');
            $date   = $request->post('date');
            $date   = date('Y-m-d',strtotime($date));
            $driveLatLong = Fetch::getDriveLatLongByIdEvent($did,$date);
            $driveDates   = Fetch::getDriveDatesByDeviceIdEvent($did);

            $data_array   = "";
            $string       = '';
            $start        = "";
            $start_lat    = '';
            $start_lon    = '';
            $end_lat      = '';
            $end_lon      = '';
            $i            = 0;

            foreach($driveDates as $sdt){
                $data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',";
            }

            if(!empty($driveLatLong))
            {
                foreach($driveLatLong as $key => $list)
                {
                    if($list->lat == 0){
                       $i = 1;
                    }
                    
                    if($key == 0){
                        $start.=  "{lat: $list->lat, lng: $list->lon}";
                        $start_lat = $list->lat;
                        $start_lon = $list->lon;
                    }
                    if($key == count($driveLatLong)-1){
                        $end_lat = $list->lat;
                        $end_lon = $list->lon;
                    }

                    $string.=  "{lat: ".$list->lat.", lng: ".$list->lon."},";
                }

                $startEnd =  "['Start', $start_lat, $start_lon, 0],['End', $end_lat, $end_lon, 1]";
                if($i!=1){
                   $data   = array(
                    'did'              => $did,
                    'drive_latlon'     => $string,
                    'start_point'      => $start,
                    'start_end'        => $startEnd,
                    'drive_dates'      => rtrim($data_array,',')
                   );
                }else{
                   $data   = array(
                    'did'              => $did,
                    'drive_latlon'     => '',
                    'start_point'      => '',
                    'start_end'        => '',
                    'drive_dates'      => rtrim($data_array,',')
                   );
                }
                
            }else{
                $start   = "{lat: -35.2784167, lng: 149.1294692}";

                $data   = array(
                    'did'              => $did,
                    'drive_latlon'     => '',
                    'start_point'      => '',
                    'start_end'        => '',
                    'drive_dates'      => rtrim($data_array,',')
                );
            }
            $string = rtrim($string,',');
        
            $result = view('drivesd/load')->with($data);
            return $result;
        }
    }
?>