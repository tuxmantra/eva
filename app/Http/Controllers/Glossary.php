<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;

class Glossary extends Controller
{
    public function index(){
      $glossaryList = Fetch::getGlossary();
      $data = array(
          'title'         => 'Glossary',
          'form_token'    => Helper::token(8),
          'glossary_list' => $glossaryList,
          'utype'         => session('userdata')['utype']
      );
      $data['gllr_open'] = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('glossary/index')->with($data);
    }
    public function updateGlossary(Request $request){
    	$token = $request->post('form_token');
    	 if($token == session('form_token')){
           session()->forget('form_token');
    	   $glossary = $request->post('msg');
    	   if(!empty($glossary)){
				Common::updateTable('glossary',array('id' => '1'),array('glossary' => $glossary));
    		    \Session::flash('message','Glossary Updated Successfully.');
            return redirect('glossary-list');
    	   }
    	   else{
    		    \Session::flash('message','Fields Missing.');
            return redirect('glossary-list');
    	   }
    		 
    	}else{
           session()->forget('form_token');
           \Session::flash('message','Sorry, token expired. Try again.');
            return redirect('glossary-list');
       }
    }
}
