<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Helper;
use App\Model\Common;
use App\Model\Fetch;

class Edas extends Controller
{
    public function index(Request $request)
    {
        $data['option']    = $request->post('option');
        $data['adid']      = $request->post('devid');
        $user              = session('userdata');
        $data['user_type'] = $user['utype'];
        $deviceList        = Fetch::getDeviceListByUserType($user['uid'],$user['ucategory']);
        if(!empty($deviceList))
        {
          	if(!empty(session('device_id')))
            {
                $did = session('device_id');
            }
            else
            {       
                 $did = $deviceList[0]->device_id;
            }
          	$driveDates = Fetch::getDriveDatesByDeviceIdEvent($did);
          
          	$data_array = "";
          	foreach($driveDates as $sdt){
             	$data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',"; 
          	}
        }
        else
        {
          	$did = '';
          	$data_array = '';
        }
        
        $date = date('Y-m-d');
        $data = array(
          'title'         => 'EDAS',
          'device_list'   => $deviceList,
          'user_category' => $user['ucategory'],        
          'did'           => $did,
          'drive_dates'   => rtrim($data_array,','),
          'option'        => $request->post('option'),
          'adid'          => $request->post('devid')
        );
        $data['edas_open'] = '1';
        $data['username']  = Fetch::getUserDeviceEmail($did);
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      	return view('edas/index')->with($data);
    }

	public function getEdasLoad(Request $request)
	{
		$did         = $request->post('did');
		session(['device_id' => $did]);
        // $id          = Fetch::getIdByDeviceId($did);
        // $id          = $id[0]->id;
        // $scoreforgraph = Fetch::getEdasScoreGroupbyDate($did);
        $score        = Fetch::getEdasScore($did);
        // print_r($score);
        $rating       ='';
        $scoreComment = [];
        if(!empty($score[0]->drv_score_cmts))
        {
        	$rating = explode(',', $score[0]->drv_score_cmts);
        
	        foreach ($rating as $key => $value) 
	        {
	        	// echo $value;
	        	$scoreCommentarr = Fetch::getEdasComments($value);
	        	if(!empty($scoreCommentarr))
	        	{
	        		$scoreComment[] = array('category' =>$scoreCommentarr[0]->category, 'rate' =>$scoreCommentarr[0]->rating ,'comment'=>$scoreCommentarr[0]->comments,'exp'=>$scoreCommentarr[0]->explanation );
	        	}
	        	else
	        	{
	        		$scoreComment[] = array('category' =>'','rate' =>'' ,'comment'=>'','exp'=>'' );
	        	}
	        	
	        }
	    }
        // print_r($scoreComment);
        if(!empty($score[0]->drv_score))
        {
        	$data['score'] = $score[0]->drv_score;
        	if($score[0]->drv_score == 100)
	        {
	        	$scoreComment[] = array('category' =>'','rate' =>'' ,'comment'=>'Excellent driving','exp'=>'Keep it up.' );
	        }
        }else
        {
        	$data['score'] = 0;
        }

        // print_r($score[0]->drv_score);
        $data['did']   = $did;
        $data['year']  = date('Y');
        $data['month'] = date('m');
        $data['scoreComment'] = $scoreComment;
      	$result = view('edas/load')->with($data);
      	return $result;
  	}

  	public function getEdasGraph(Request $request)
    {
        $did   = $request->post('did');
        $year  = $request->post('year');
        $month = $request->post('month');
        $scoreforgraph = Fetch::getEdasScoreGroupbyMonth($did,$month,$year);
        $i=0;
        $score=''; 
        $user              = session('userdata');
        foreach ($scoreforgraph as $key => $value) 
        {
          $string = ""; 
          if(!empty($value->drv_score) && $value->drv_score != 0)
          { 
            $string = "Datetime : ". $value->ad_dt ."<br>"; 
            
             if($user['utype'] == 1)
            {
              if($value->hlt_score != '' || $value->hlt_score != '0')
              {
                $string.="Health score : ". $value->hlt_score ."<br>"; 
              }
            }
            if($value->dist != '0')
            {
              $string.="Distance : ". round($value->dist,2) ."<br>"; 
            }
            if($value->avg_spd != '0')
            {
              $string.="Avg speed : ". round($value->avg_spd,2) ."<br>"; 
            }

            
            if($user['utype'] == 1)
            {

              if($value->harsh_br_low != '0')
              {
                $string.="Harsh breaking low : ". $value->harsh_br_low ."<br>"; 
              }
              if($value->gr_shift_up != '0')
              {
                $string.="Gear shift up : ". $value->gr_shift_up ."<br>"; 
              }
              
              if($value->long_idle != '0')
              {
                $string.="Long idle : ". $value->long_idle ."<br>"; 
              }
              if($value->harsh_br_high != '0')
              {
                $string.="Harsh breaking high : ". $value->harsh_br_high ."<br>"; 
              }
              if($value->oil_cont != '0')
              {
                $string.="Oil contamination : ". $value->oil_cont ."<br>"; 
              }
              if($value->spd_bump != '0')
              {
                $string.="Speed bump : ". $value->spd_bump ."<br>"; 
              }
              if($value->gr_shift_dwn != '0')
              {
                $string.="Gear shift down : ". $value->gr_shift_dwn ."<br>"; 
              }
              if($value->take_off != '0')
              {
                $string.="Take off : ". $value->take_off ."<br>"; 
              }
              
              if($value->ovr_spd_low != '0')
              {
                $string.="Overspeed low : ". $value->ovr_spd_low ."<br>"; 
              }
              if($value->harsh_acc != '0')
              {
                $string.="Harsh acceleration : ". $value->harsh_acc ."<br>"; 
              }
              if($value->turbo_brch != '0')
              {
                $string.="Turbo timer breach : ". $value->turbo_brch ."<br>"; 
              }
              if($value->fuel_high != '0')
              {
                $string.="Fuel high : ". $value->fuel_high ."<br>"; 
              }
              if($value->cold_drv != '0')
              {
                $string.="Cold drive : ". $value->cold_drv ."<br>"; 
              }
              if($value->gr_shift_dwn_high_fuel != '0')
              {
                $string.="Gear shift down high fuel : ". $value->gr_shift_dwn_high_fuel ."<br>"; 
              }
              if($value->ovr_spd_high != '0')
              {
                $string.="Overspeed high : ". $value->ovr_spd_high ."<br>"; 
              }
              if($value->high_rev != '0')
              {
                $string.="High revving : ". $value->high_rev ."<br>"; 
              }
              if($value->harsh_spd_bmp != '0')
              {
                $string.="Harsh speed bump : ". $value->harsh_spd_bmp ."<br>"; 
              }
              if($value->harsh_turn != '0')
              {
                $string.="Harsh turn : ". $value->harsh_turn ."<br>"; 
              }
              if($value->drv_score_cmts != '0')
              {
                $string.="Drive score comments : ". $value->drv_score_cmts ."<br>"; 
              }
            }
          
            // echo $string;
            $dist    = round($value->dist,2);
            $avg_spd = round($value->avg_spd,2);
            $score  .= "['$string',$value->drv_score],"; 
            $i++;
          }
        }
        // print_r($score);
        $data['score'] = $score;
        $result = view('edas/healthgraph')->with($data);
        return $result;
    }
    public function getAlertCount(Request $request)
    {
        $did           = $request->post('did');
        session(['device_id' => $did]);
        $scoreforgraph = Fetch::getEdasScoreLatest($did);
        $string        = '';
        if(!empty($scoreforgraph))
        {
          $string ='<div class="row">
          <div class="col-md-3">
                      <div class="form-group bmd-form-group is-filled">
                        <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Health score
                        </label>
                        <br>
                        <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->hlt_score.'" readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group bmd-form-group is-filled">
                        <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Drive score
                        </label><br>
                        <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->drv_score.'" readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group bmd-form-group is-filled">
                        <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Distance
                        </label><br>
                        <input class="form-control" style="text-align: center;" value ="'.round($scoreforgraph[0]->dist,2).'" readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group bmd-form-group is-filled">
                        <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Average speed
                        </label><br>
                        <input class="form-control" style="text-align: center;" value ="'.round($scoreforgraph[0]->avg_spd,2).'" readonly>
                      </div>
                    </div>
                    </div>
                    <table id="datatables1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                              
                              <tbody>
                              <tr>
                                <th class="tdr tdrA tdl th-clr"  colspan="2" style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  Low
                                
                                </th><th class="tdr tdrB th-clr" colspan="2" style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  Medium
                                </th>
                                <th class="tdr tdrC th-clr"  colspan="2" style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  High
                                </th>
                              </tr>
                              <tr>
                                <th class="tdr tdrA tdl th-clr" style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  Severity 0
                                
                                </th><th class="tdr tdrA tdl th-clr" style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  Severity 1
                                </th>
                                <th class="tdr tdrA tdl th-clr" style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                 Severity 0
                                
                                </th><th class="tdr tdrA tdl th-clr" style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                 Severity 1
                                </th>
                                <th class="tdr tdrA tdl th-clr" style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                 Severity 0
                                
                                </th><th class="tdr tdrA tdl th-clr" style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                 Severity 1
                                </th>
                              </tr>
                              <tr>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">

                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Harsh breaking low
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->harsh_br_low.'" readonly>
                                  </div>

                                 </td>
                                 <td class="tdr tdl " style="text-align: center;" border: 1px solid rgba(0, 0, 0, 0.06);">

                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Long idle
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->long_idle.'" readonly>
                                  </div>
                    
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Harsh breaking high
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->harsh_br_high.'" readonly>
                                  </div>
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                 <div class="form-group bmd-form-group is-filled">
                                  <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Gear shift down
                                  </label><br>
                                  <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->gr_shift_dwn.'" readonly>
                                </div>
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                <div class="form-group bmd-form-group is-filled">
                                  <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Harsh acceleration
                                  </label><br>
                                  <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->harsh_acc.'" readonly>
                                </div>
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                <div class="form-group bmd-form-group is-filled">
                                  <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Fuel high
                                  </label><br>
                                  <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->fuel_high.'" readonly>
                                </div>
                                </td>

                                </tr>
                                <tr>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                      <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Gear shift up
                                      </label><br>
                                      <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->gr_shift_up.'" readonly>
                                    </div>
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Oil contamination
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->oil_cont.'" readonly>
                                  </div>
                                </td>

                                

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Take off
                                     </label><br>
                                    <input class="form-control"  style="text-align: center;" value ="'.$scoreforgraph[0]->take_off.'" readonly>
                                  </div>
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Turbo timer breach
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->turbo_brch.'" readonly>
                                  </div>
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Cold drive
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->cold_drv.'" readonly>
                                  </div>
                                </td>

                              </tr>
                              <tr>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Speed bump
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->spd_bump.'" readonly>
                                  </div>
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Overspeed low
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->ovr_spd_low.'" readonly>
                                  </div>
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Harsh turn
                                    </label><br> 
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->harsh_turn.'" readonly>
                                  </div>
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Gear shift down high fuel
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->gr_shift_dwn_high_fuel.'" readonly>
                                  </div>
                                </td>

                              </tr>
                              <tr>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Overspeed high
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->ovr_spd_high.'" readonly>
                                  </div>
                                </td>

                              </tr>
                              <tr>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">High revving
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->high_rev.'" readonly>
                                  </div>
                                </td>

                              </tr>
                              <tr>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>

                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                </td>
                                <td class="tdr tdl " style="text-align: center; border: 1px solid rgba(0, 0, 0, 0.06);">
                                  <div class="form-group bmd-form-group is-filled">
                                    <label class="col-form-label bmd-label-static" style="font-weight: 500;font-size: 13px;color: black;">Harsh speed bump
                                    </label><br>
                                    <input class="form-control" style="text-align: center;" value ="'.$scoreforgraph[0]->harsh_spd_bmp.'" readonly>
                                  </div>
                                </td>

                              </tr>
                            </tbody></table>';
        }
        else
        {
          $string ='<div class="col-md-12">
                          No data available.
                        </div>';
        }
        echo $string;
        
    }
  	public function getUservehicleDetails(Request $request)
  	{
  		$did      = $request->post('did');
      $date     = $request->post('date');
      $result   = $request->post('result');
      $date     = date('Y-m-d',strtotime($date));
  		$username = Fetch::getUserDeviceEmail($did);
  		$id       = Fetch::getIdByDeviceId($did);
        $id       = $id[0]->id;
        $vehicle  = Fetch::getVehicleById($id);
        if($vehicle->fuel_tpe == 1)
        {
        	$fuelType = 'Petrol';
        }
        else
        {
        	$fuelType = 'Diesel';
        }
        // print_r($vehicle);
        $vehicle_sett = Fetch::getVehicleSettingData();
        $manuArr      = explode(';', $vehicle_sett->manufacturer);
        $engCapArr    = explode(';', $vehicle_sett->engine_capacity);

        $vehicle_info = Fetch::getVehicleInfoDataById($vehicle->manufact);
        $modelArr     = explode(';', $vehicle_info[0]->model);
        $varArr       = explode(';', $vehicle_info[0]->var);
        // print_r($modelArr);
        // print_r($varArr);
        $resulttext   = "";
        $datetag      = "";
        if($result == 1)
        {
          $resulttext = $date." / Passed";
          $datetag    = "Test Date / Result";
        }
        elseif($result == 2)
        {
          $resulttext = $date." / Failed";
          $datetag    = "Test Date / Result";
        }
        else
        {
          $resulttext = $date;
          $datetag    = "Test Date";
        }
        $mfd          = $manuArr[$vehicle->manufact]; 
        $mfd		  = explode('_', $mfd);
        $string ='<div class="col-md-4">
                        <p style="color: black;margin-bottom: 0rem;" id="uname"><b>User Name</b> : '.$username[0]->uname.'</p>
                        <p style="color: black;margin-bottom: 0rem;" id="title"><b>DeviceID</b> : '.$did.'</p>
                      </div>
                      <div class="col-md-4">
                      </div>
                      <div class="col-md-4" >
                        <p style="color: black;margin-bottom: 0rem;" id="uname"><b>Vehicle Name</b> : '.$modelArr[$vehicle->model].' '.$varArr[$vehicle->varient].'</p>
                        <p style="color: black;margin-bottom: 0rem;" id="testdate"><b>'.$datetag.'</b> : '.$resulttext.'</p>
                      </div>';

                      echo $string;

  	}
}
?>