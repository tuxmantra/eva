<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\AskAssistance;

class Actions extends Controller
{
    public function index(){
    	
	    $data['title']      = 'Actions';
      $data['actio_open'] = '1';
      $data['menu']       = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    	return view('actions/index')->with($data);
    }

    public function getActionLoad(Request $request){
       $user                = session('userdata');
       $data['user_type']   = $user['utype'];
       $deviceList          = Fetch::getDeviceListByUserType($user['uid'],$user['ucategory']);
       if(!empty($deviceList)){
           
           // Start listing ----------------------------
           // user sepration for device_id
           if($user['ucategory'] == 1 || $user['ucategory'] == 2 || $user['ucategory'] == 3){
                  foreach($deviceList as $devl){
                    $devId[] = $devl->device_id;
                  }
                  $devId = implode(',',$devId);
           }else{
                  $devId        = $deviceList[0]->device_id;
           }
           $form_token          = Helper::token(8);
           $cars                = Fetch::getCarsInRed($user['ucategory'], $data['user_type'],$devId);
           $vehicleSettings     = Fetch::getVehicleSettingData();
           $mfdList             = explode(';',$vehicleSettings->manufacturer);
           $engcap              = explode(';',$vehicleSettings->engine_capacity);
           $assistance          = Fetch::getAssistanceMail($user['ucategory'],$devId);
           $fuelType            = array('','Petrol','Diesel');
            echo '<ul class="nav nav-pills nav-pills-warning" role="tablist">
                      '; 
                      if($user['ucategory'] == 4){
                        echo '<li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#link1" role="tablist">
                            Ask for Assistance 
                          </a>
                        </li>';
                      }
                        if($user['ucategory'] != 4){  
                        echo '<li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#link3" role="tablist">
                            Cars in Red
                          </a>
                        </li>';
                        }
                        echo '<li class="nav-item">
                        <a class="nav-link active show" data-toggle="tab" href="#link2" role="tablist">
                          User Assistance
                        </a>
                      </li>';
                      echo '</ul>
                      <div class="tab-content tab-space">';

                      if($user['ucategory'] == 4){
                        echo '<div class="tab-pane" id="link1">

                        <!-- Tab 1 Start -->

                        <div class="col-md-12" align="center">
               
                  <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
                </div>

                  <form method="post" action="'.url('/').'/ask-assistance-save" class="form-horizontal" onsubmit="return validateAssistance()">
                      <input type="hidden" value="'.$form_token.'" name="form_token">
                      <input type="hidden" name="devid" id="devid" value="'.$devId.'">

                      <div class="row">
                          <label class="col-sm-2 col-form-label">Ask for Assistance</label>
                          <div class="col-sm-10">
                            <div class="form-group">
                              <input type="text" class="form-control datepicker" name="topics" id="topics" placeholder="Topics">
                            </div>
                          </div>
                        </div>
                 
                        <div class="row">
                          <label class="col-sm-2 col-form-label">Description/Questions</label>
                          <div class="col-sm-10">
                            <div class="form-group">
                               <textarea class="form-control" name="desc" id="desc" rows="5"></textarea>
                            </div>
                          </div>
                        </div>

                ';

                echo '<div class="col-md-12" align="right" style="margin-top: 30px;">
                  <button type="submit" class="btn btn-fill btn-rose">Ask</button>
                </div>
              </form>
                        <!-- Tab 1 End -->


                        </div>';
                      
                      }
                       
                        echo '<div class="tab-pane" id="link3">
                            <!-- Tab3 Start -->
                           
                      <div class="material-datatables">
                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                          <thead>
                            <tr>
                              <th></th>
                              <th class="table-heading-font">S.No</th>
                              <th class="table-heading-font">Users Name</th>
                              <th class="table-heading-font">Device Id</th>
                              <th class="table-heading-font">Action Required Since</th>
                              <th class="table-heading-font none">Vehicle Info</th>
                            </tr>
                          </thead>
                          <tfoot>
                            <tr>
                              <th></th>
                              <th class="table-heading-font">S.No</th>
                              <th class="table-heading-font">Users Name</th>
                              <th class="table-heading-font">Device Id</th>
                              <th class="table-heading-font">Action Required Since</th>
                              <th class="table-heading-font none">Vehicle Info</th>
                            </tr>
                          </tfoot>
                          <tbody>';
                            if(!empty($cars)){
                            $id = 1;  
                            foreach ($cars as $car){  

                              $car_info   = Helper::getVehicleDataActions($car->did);
                              $timeDiff   = abs(strtotime(date('Y-m-d')) - strtotime(date('Y-m-d',strtotime($car->red_dt))));
                              $numberDays = $timeDiff/86400;
                              $numberDays = intval($numberDays);
                              $days       = ($numberDays > 1)?"Days":"Day";
                              if(!empty($car_info)){
                            echo '<tr>
                              <td></td>
                              <td class="table-heading-font" style="background-color: #f5f5f5;">'; echo $id; echo '</td>
                              <td style="background-color: #ffffff;" class="table-heading-font">'; echo !empty($car_info[0]->name)? $car_info[0]->name:"-"; echo '</td>
                              <td style="background-color: #f5f5f5;" class="table-heading-font">'; echo !empty($car_info[0]->devid)? $car_info[0]->devid:"-"; echo '</td>
                              <td class="table-heading-font" style="background-color: #ffffff;">'; echo ($numberDays == '0')?"Today":$numberDays." ".$days; echo '</td>
                              <td style="background-color: #f5f5f5;" class="table-heading-font none">'; echo !empty($car_info[0]->manufact)? $mfdList[$car_info[0]->manufact].",".$mfdList[$car_info[0]->model].",".$mfdList[$car_info[0]->varient].",".$engcap[$car_info[0]->eng_cap].",".$fuelType[$car_info[0]->fuel_tpe].",".$car_info[0]->manufact_yr:"-"; echo '</td>
                            </tr>';
                          }
                            $id++;  
                            }}else{
                               echo '<tr><td colspan="9" align="center">No Cars in Red.</td></tr>';
                            }
                           
                           
                          echo '</tbody>
                        </table>
                      </div>';

                           echo '<!-- Tab3 End -->

                        </div>';


                        echo '<div class="tab-pane active show" id="link2">

                        <!-- Tab2 Start--->
                        
                        <div class="material-datatables">
                        <table id="datatables1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                          <thead>
                            <tr>
                              <th></th>
                              <th class="table-heading-font">S.No</th>
                              <th class="table-heading-font">Users Name</th>
                              <th class="table-heading-font">Device Id</th>
                              <th class="table-heading-font">Date</th>
                              <th class="table-heading-font none">Information</th>
                              
                            </tr>
                          </thead>
                   
                          <tbody>';
                            if(!empty($assistance)){
                            $id = 1;  
                            foreach ($assistance as $ass){  

                              $car_info   = Helper::getVehicleDataActions($ass->did);

                              
                              if(!empty($car_info)){
                            echo '<tr>
                              <td></td>
                              <td class="table-heading-font" style="background-color: #f5f5f5;">'; echo $id; echo '</td>
                              <td style="background-color: #ffffff;" class="table-heading-font">'; echo !empty($car_info[0]->name)? $car_info[0]->name:"-"; echo '</td>
                              <td style="background-color: #f5f5f5;" class="table-heading-font">'; echo !empty($car_info[0]->devid)? $car_info[0]->devid:"-"; echo '</td>
                              <td class="table-heading-font" style="background-color: #ffffff;">'.date('Y-m-d', strtotime($ass->ad_dt)).'</td>
                              <td class="table-heading-font none"> <br>Vehicle Indo - '; echo !empty($car_info[0]->manufact)? $mfdList[$car_info[0]->manufact].",".$mfdList[$car_info[0]->model].",".$mfdList[$car_info[0]->varient].",".$engcap[$car_info[0]->eng_cap].",".$fuelType[$car_info[0]->fuel_tpe].",".$car_info[0]->manufact_yr:"-"; echo '<br>Topic - '.$ass->topic.'<br>Description - '.$ass->descrp.'</td>
                              
                              </tr>';
                          }
                            $id++;  
                            }}else{
                               echo '<tr><td colspan="9" align="center">No Cars in Red.</td></tr>';
                            }
                           
                           
                          echo '</tbody>
                        </table>
                      </div>';
                      /* Tab 2 End */
                        echo '</div>
                      </div> ';
                      // End listing ----------------------------
       }else{
                   echo '<ul class="nav nav-pills nav-pills-warning" role="tablist">
                      '; 
                      if($user['ucategory'] == 4){
                        echo '<li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#link1" role="tablist">
                            Ask for Assistance 
                          </a>
                        </li>';
                      }
                        if($user['ucategory'] != 4){  
                        echo '<li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#link3" role="tablist">
                            Cars in Red
                          </a>
                        </li>';
                        }
                        echo '<li class="nav-item">
                        <a class="nav-link active show" data-toggle="tab" href="#link2" role="tablist">
                          User Assistance 
                        </a>
                      </li>';
                      echo '</ul>';
                      echo '<div class="tab-content tab-space">';

                      if($user['ucategory'] == 4){
                        echo '<div class="tab-pane" id="link1"><div class="col-md-12">No Device Present</div></div>';
                      }
                      if($user['ucategory'] != 4){  
                        echo '<div class="tab-pane" id="link2"><div class="col-md-12">No Device Present</div></div>';
                      }
                        echo '<div class="tab-pane" id="link3"><div class="col-md-12">No Device Present</div></div>';
       }
      
       
  }

  public function askAssistanceSave(Request $request){
  	  $token = request('form_token');
      if($token == session('form_token')){
         session()->forget('form_token');
         
         $devId          = $request->post('devid');
         $topics         = $request->post('topics');
         $desc           = $request->post('desc');
         $driverDetails  = Fetch::getdriverDetails($devId);
         $uname          = $driverDetails[0]->uname;
         $vehicleDetails = $this->getvehicle($devId);
         $fuelType       = array('','Petrol','Diesel');
         $manufacturer   = explode(';',$vehicleDetails[1]->manufacturer);
         $eng_cap        = explode(';',$vehicleDetails[1]->engine_capacity);
         $vehDetail      = $manufacturer[$vehicleDetails[0]->manufact].",".$vehicleDetails[0]->model.",".$vehicleDetails[0]->varient.",".$eng_cap[$vehicleDetails[0]->eng_cap].",".$fuelType[$vehicleDetails[0]->fuel_tpe].",".$vehicleDetails[0]->manufact_yr;
         $did     = Fetch::getIdByDeviceId($devId);
         $pid     = Fetch::getUserById($did[0]->partner);

         $p_email = $pid[0]->uemail;
         $data = array(
          'topic'  => $topics,
          'descrp' => $desc,
          'devid'  => $devId,
          'did'    => $did[0]->id,
          'status' => '1',
          'ad_by'  => session('userdata')['uid']
         );
         Common::insert('assistance_mail',$data);

         $data  = new \stdClass();
         $data->name      = $uname;
         $data->device_id = $devId;
         $data->vinfo     = $vehDetail;
         $data->topic     = $topics;
         $data->desc      = $desc;

         $partner_email   = $p_email;
         //praveen.rao@enginecal.com,support@enginecal.com
         Mail::to("support@enginecal.com")->send(new AskAssistance($data));
         \Session::flash('message','Your Query has been submited successfully, will get back to you soon.');
         Mail::to("$partner_email")->send(new AskAssistance($data));
         \Session::flash('message','Your Query has been submited successfully, will get back to you soon.');
         return redirect('actions');

      }else{
           session()->forget('form_token');
           \Session::flash('message','Sorry, token expired now try.');
            return redirect('actions');
      }
  }

  public function getvehicle($deviceid){
		 $vehSett = Fetch::getVehicleSettingData();
		 $vehicle = Fetch::getVehicleDeviceDetails($deviceid);

		 $pDVar = ($vehicle[0]->fuel_tpe == 1) ? "p" : "d";
		 $f1       = "calval$pDVar";
		 $vsetVal  = json_decode($vehSett->$f1,true);
		 $calval   = json_decode($vehicle[0]->calval,true);

		 $vehicle[0]->co2fact   = ($calval['Co2_Cnv_Fac_C'] != "")? $calval['Co2_Cnv_Fac_C']:$vsetVal['Co2_Cnv_Fac_C'];
		 $vehicle[0]->oill_day  = ($calval['Oil_Life_Days_C'] != "")? $calval['Oil_Life_Days_C']:$vsetVal['Oil_Life_Days_C'];
		 $vehicle[0]->oill_dist = ($calval['Oil_Life_Kms_C'] != "")? $calval['Oil_Life_Kms_C']:$vsetVal['Oil_Life_Kms_C'];
		 $vehicle[0]->dust_env  = ($calval['DustEnv_Status_C'] != "")? $calval['DustEnv_Status_C']:$vsetVal['DustEnv_Status_C'];
		 $vehicle[0]->nfact    = ($calval['Eng_eff_C'] != "")? $calval['Eng_eff_C']:$vsetVal['Eng_eff_C'];
		 $vehicle[0]->ve        = ($calval['Vol_eff_C'] != "")? $calval['Vol_eff_C']:$vsetVal['Vol_eff_C'];
		 $vehicle[0]->ed        = ($calval['Eng_Disp_C'] != "")? $calval['Eng_Disp_C']:$vsetVal['Eng_Disp_C'];
		 $vehicle[0]->int_cool  = ($calval['IC_Status_C'] != "")? $calval['IC_Status_C']:$vsetVal['IC_Status_C'];
		 $vehicle[0]->egr_cool  = ($calval['EgrC_Status_C'] != "")? $calval['EgrC_Status_C']:$vsetVal['EgrC_Status_C'];
		 $vehicle[0]->egr_cmd   = ($calval['Egr_Status_C'] != "")? $calval['Egr_Status_C']:$vsetVal['Egr_Status_C
		 '];
		 $vehicle[0]->lmt1      = ($calval['Eff_RPM_Limit1_C'] != "")? $calval['Eff_RPM_Limit1_C']:$vsetVal['Eff_RPM_Limit1_C'];
		 $vehicle[0]->lmt2      = ($calval['Eff_RPM_Limit2_C'] != "")? $calval['Eff_RPM_Limit2_C']:$vsetVal['Eff_RPM_Limit2_C'];
		 $vehicle[0]->lmt3      = ($calval['Eff_RPM_Limit3_C'] != "")? $calval['Eff_RPM_Limit3_C']:$vsetVal['Eff_RPM_Limit3_C'];
		 // ve ed int_cool egr_cool egr_cmd lmt1 lmt2 lmt3
		 return array($vehicle[0],$vehSett);
  }
  
  public function trackAssistance(){
    $data              = "";
    $user              = session('userdata');
    $deviceList        = Fetch::getDeviceListByUserType($user['uid'],$user['ucategory']);
    if(!empty($deviceList)){
             // user sepration for device_id
        if($user['ucategory'] == 1 || $user['ucategory'] == 2 || $user['ucategory'] == 3){
                foreach($deviceList as $devl){
                  $devId[] = $devl->device_id;
                }
                $devId = implode(',',$devId);
         }else{
                $devId = $deviceList[0]->device_id;
         }
         $assistState  = Fetch::getAssistanceMailByStatus($user['ucategory'],$devId);
         $assistCount  = count($assistState);
         if(!empty($assistState)){
            foreach($assistState as $assst){
              $data.= ' 
              <a class="dropdown-item" href="javascript:void(0)" onclick="changeAssistanceStatus('.$assst->did.')" id="assistanceTrack">'.Helper::getUserByDeviceId($assst->did).' need Assistance - '.$assst->count.'</a>
              ';
            }
         }

         $request['data']  = $data;
         $request['count'] = $assistCount;
         echo json_encode($request);
         exit;
    }
         $request['data']  = "";
         $request['count'] = 0;
         echo json_encode($request);
    }

    public function changeAssistanceStatus(){
          $user = session('userdata');
          $did  = request('did');
          if($user['ucategory'] == 1){
              $data = array('super_view' => '0');
          }elseif($user['ucategory'] == 1){
              $data = array('admin_view' => '0');
          }elseif($user['ucategory'] == 1){
              $data = array('partner_view' => '0');
          }
          Common::updateTable('assistance_mail',array('did' => $did),$data);
    }
}