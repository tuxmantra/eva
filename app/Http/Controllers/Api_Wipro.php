<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newuser;
use Illuminate\Support\Arr;
// use Mail;

class Api_Wipro extends Controller
{
	public function get_bearer_auth(Request $request)
	{
		if(!empty($request->data))
        {
        	$obj    = json_decode($request->data, TRUE);
			$apiKey = $obj['apiKey'];

			$apiKeyValid = Fetch::checkAPIKeyValid($apiKey);
			if($apiKeyValid)
			{
				//API key valid. Create Bearer key and send response.
				$bearerkey    = $datAct -> randKey(32);
				$bearerkeyexp = date('Y-m-d H:i:s',strtotime("+366 days"));
			}

        	$result['success'] = TRUE;
		    $result['data']    = "1";
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
	}

	public function reg_device(Request $request)
  	{
  		if(!empty($request->data))
        {

        	/****************************************************************************************/
			/**************************** Verify Authorisation Header Key ***************************/
	
			/******************************** Step 1: Get Bearer Key ********************************/
			foreach (getallheaders() as $name => $value)
			{
			    //$name = $value\n;
				if($name == "Authorization")
				{
					//Get complete string for authorization
					$bearerstr = $value;
					
					$bearerkey = str_replace('Bearer ', '', $bearerstr);
					$bearerkey = str_replace('bearer ', '', $bearerkey);
				}
				print_r($name);
			}die;
			/***** Step 2: Verify if the Bearer Key is Valid *******************************/
			
			if($datAct -> bearerKeyVerify($bearerkey))
			{
	        	$obj          = json_decode($request->data, TRUE);
				$deviceID     = $obj['deviceID'];
				$devicestatus = Fetch::devicestatus($deviceID);
				if(!empty($devicestatus))
	          	{
				
					$app  = Fetch::appupdate($deviceID);
					
			  	}
			  	else
			  	{
					$result['success'] = TRUE;
					$result['message'] = "Device Already Registered!";
			    }
			}
			else
			{
				http_response_code(401);
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function add_vehicle(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];

        	$result['success'] = TRUE;
		    $result['data']    = "1";
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function file_upload(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$obj       = json_decode($request->data, TRUE);
			$deviceID  = $obj['devID'];
			$appupdate = Fetch::getfirmstatdetails($deviceID);
        	$appNames  = array();
			$appLinks  = array();
			$appVers   = array();
			$cmd       = array();
			$i         = 0;
			
			foreach ($appupdate as $key => $value) 
			{
				$appNames[] = $value->firm_name;
				if(!empty($value->lat_ver_filename)){
					$appLinks[] = $value->lat_ver_filename;
				}else{
					$appLinks[] = $value->lat_ver_path;
				}
				$appVers[] = $value->lat_ver;
				$cmd[]     = $value->cmd;
				$i++;
		   	}
        	$result['success']            = TRUE;
        	if($i == 0)
        	{
        		$result['installFirmStatus']  = 0;
        	}
        	else
        	{
        		$result['installFirmStatus']  = 1;
        	}
        	$result['installFirmCnt']     = $i;
		    $result['installFirmName']    = $appNames;
		    $result['installFirmLink']    = $appLinks;
		    $result['installFirmVersion'] = $appVers;
		    $result['installFirmcmd']     = $cmd;
		    // $result['uninstallAppStatus'] = 0;
		    // $result['uinistallAppName']  = "";
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function post_results(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$obj       = json_decode($request->data, TRUE);
			$deviceID  = $obj['devID'];
			$appupdate = Fetch::getappstatdetails($deviceID);
        	$appNames  = array();
			$appLinks  = array();
			$appVers   = array();
			$i         = 0;

			foreach ($appupdate as $key => $value) 
			{
				$appNames[] = $value->app_name;
				// $appLinks[] = $value->lat_ver_path;
				if(!empty($value->lat_ver_filename)){
					$appLinks[] = $value->lat_ver_filename;
				}else{
					$appLinks[] = $value->lat_ver_path;
				}
				$appVers[] = $value->lat_ver;
				$i++;
		   	}
        	$result['success']           = TRUE;
        	if($i == 0)
        	{
        		$result['installAppStatus']  = 0;
        	}
        	else
        	{
        		$result['installAppStatus']  = 1;
        	}
        	
        	$result['installAppCnt']     = $i;
		    $result['installAppName']    = $appNames;
		    $result['installAppLink']    = $appLinks;
		    $result['installAppVersion'] = $appVers;
		    // $result['uninstallAppStatus'] = 0;
		    // $result['uinistallAppName']  = "";
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}


}
?>