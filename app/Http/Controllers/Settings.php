<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use App\Mail\Activationcode;
use Illuminate\Support\Facades\Mail;

class Settings extends Controller
{
    // Load system setting form under settings
    public function index(){

    	$settingData    = Fetch::getSettingData();
        $data = array(
         'title'        => "System Settings",
         'setting_data' => $settingData,
         'form_token'   => Helper::token(8) 
        );
        $data['sest_open'] = '1';
        $data['syst_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('settings/index')->with($data);
    }

    // Database setting Start
    public function dbDatabase(){
        $data = array(
         'title'        => "Database Optimize",
         'form_token'   => Helper::token(8) 
        );
        $data['sest_open'] = '1';
        $data['db_open']   = '1';
        $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('settings/database/dboptimize')->with($data);
    }
     public function delete_logo(Request $request){
        $id        = $request->post('id');
        print_r($id);die;
        // $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        // return view('settings/database/dboptimize')->with($data);
    }

       // Database setting End
    
    // Process system setting form update.
    public function updateSystemSettings(Request $request){
    	$token    = request('form_token');
    	if($token == session('form_token')){
           session()->forget('form_token');

            $name        = $request->post('name');
            $email       = $request->post('email');
            $mobile      = $request->post('mobile');
            $address     = $request->post('address');
            $website     = $request->post('website');
            $pincode     = $request->post('pincode');
            $location    = $request->post('location');
            $city        = $request->post('city');
            $callist     = $request->post('callist');

            if(empty($name) || empty($email) || empty ($mobile) || empty($address) || empty($website) || empty($pincode) || empty($location) || empty($city) || empty($callist)){
	            \Session::flash('message','Fields required.');
	             return redirect('system-settings');
            }

            $data = array(
            'sett_name'  => $name,
            'sett_addr'  => $address,
            'sett_loc'   => $location,
            'sett_pin'   => $pincode,
            'sett_city'  => $city,
            'sett_web'   => $website,
            'sett_phn'   => $mobile,
            'sett_ad_by' => session('userdata')['uid'],
            'sett_email' => $email,
            'callist'    => $callist
            );
 
	        if(!empty($_FILES['logo']['tmp_name'])){
	                $photo_person    = $_FILES['logo']['tmp_name'];
	                //sprint_r($_FILES['photo_person']['name']);die;
	                $file_ext1       = explode('.',$_FILES['logo']['name']);
	                $new_mst1        = Helper::v3generate(5).".".$file_ext1[1];
	                if(move_uploaded_file($photo_person,public_path()."/uploads/".$new_mst1))
	                {
	                        $old_data = Fetch::getSettingData();
	                        $path = public_path()."/uploads/".$old_data->sett_logo;
	                        if(file_exists($path)){
                                 unlink($path);
	                        }
	                        $data = array_merge($data, array('sett_logo' => $new_mst1));
	                }
	        }
            $up=Common::updateTable('settings',array('sett_id' => '1'),$data);

	        \Session::flash('message','Settings updated successfully.');
	        return redirect('system-settings');
        }else{
        	session()->forget('form_token');
           \Session::flash('message','Sorry, token expired. Try again.');
            return redirect('system-settings');
        }   
    }
    
    // Load vehicle setting form
    public function vehicleSettings(){

    	$settingData    = Fetch::getVehicleSettingData();
    	$calList        = Fetch::getSettingData();
        $data = array(
         'title'        => "System Settings",
         'setting_data' => $settingData,
         'cal_list'     => $calList,
         'form_token'   => Helper::token(8) 
        );
        $data['sest_open'] = '1';
        $data['vehs_open'] = '1';
        $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('settings/vehiclesettings')->with($data);
    }
    
    // Process update vehicle setting form.
    public function updateVehicleSettings(Request $request){
    	$token    = request('form_token');
    	if($token == session('form_token')){
           session()->forget('form_token');

           $manufacturer = $request->post('manufacturer');
           $capacity     = $request->post('capacity');
           $city         = $request->post('city');

           // extraction start
           $calList        = Fetch::getSettingData();
           $callistN       = $calList->callist;
           $cal_list       = preg_split ('/$\R?^/m',$callistN);
           $arrayP         = array();
	         $arrayD         = array();
	       	for($i=0;$i<count($cal_list);$i++)
    			{
      				$var = trim($cal_list[$i]);
      				$lP  = $var."p";
      				$lD  = $var."d";		
      				$arrayP[$var] = empty($request->post($lP))? "0":$request->post($lP);
      				$arrayD[$var] = empty($request->post($lD))? "0":$request->post($lD);
    			}
           // extraction end

             $vehi=Fetch::select_vehicle();
             foreach ($vehi as $key => $value) 
             {
              if($value->calval != ''){
                $arr_cal=json_decode($value->calval,JSON_OBJECT_AS_ARRAY);
                $arr=array_keys($arr_cal);
                if(end($arr) != end($cal_list))
                {
                  $val=end($cal_list);
                  if($value->fuel_tpe == 1){
                     $arr_cal[$val] = $request->post($val.'p');
                     // print_r($arr_cal[$val]);die;
                  }else{
                     $arr_cal[$val] = $request->post($val.'d');
                  }
                }
                 // print_r($arr_cal);die;
                $obj=(object) $arr_cal;
                $update=Fetch::update_vehicle_val($value->id,json_encode($obj));
              }
             } 
            
           $data = array(
           'manufacturer'    => $manufacturer,
           'engine_capacity' => $capacity,
           'citylist'        => $city,
           'calvalp'         => json_encode($arrayP),
           'calvald'         => json_encode($arrayD),
           'ad_by'           => session('userdata')['uid']
           );

           if(empty($manufacturer) || empty($capacity) || empty($arrayP) || empty($arrayD)){
               \Session::flash('message','Fields required.');
               return redirect('vehicle-settings');
           } 
          
           Common::updateTable('vehicle_setting',array('id' => '1'),$data);
           \Session::flash('message','Vehicle settings updated successfully.');
           return redirect('vehicle-settings');
           
       }else{
       	   session()->forget('form_token');
           \Session::flash('message','Sorry, token expired. Try again.');
           return redirect('vehicle-settings');
       }
    }
    
    // Load vehicle specification page
    public function vehicleSpecification(){
        $vehicleSettings = Fetch::getVehicleSettingData();
        $mfdList         = explode(';',$vehicleSettings->manufacturer);
        $data = array(
              'title'      => 'List User',
              'mfdList'    => $mfdList
        );
        $data['sest_open'] = '1';
        $data['spec_open'] = '1';
        $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('settings/vehiclespecification')->with($data);
    }
    
    // Ajax request to render vehicle specification table in vehicle specification page, default mfd supplied 0.
    public function loadListVehicleSpecification(Request $request){
        $keyMfd          = $request->post('vehData');
        $userList        = Fetch::getUserAll();
        $vehicleSettings = Fetch::getVehicleSettingData();
        $vehicleInfo     = Fetch::getVehicleInfoDataById($keyMfd);
        $vehicleSpec     = Fetch::getVehicleSpecDataById($keyMfd);

        $varList = array();
        $modList = array();
        $fuel = array('','Petrol','Diesel');
        if(!empty($vehicleInfo))
        {
          $varList = explode(';',$vehicleInfo[0]->var);
          $modList = explode(';',$vehicleInfo[0]->model);
        }

        $engCapList      = explode(';',$vehicleSettings->engine_capacity);  
        $fuel            = array('','Petrol','Diesel');
        $data = array(
          'user_list'   => $userList,
          'vehicleSpec' => $vehicleSpec,
          'varList'     => $varList,
          'modList'     => $modList,
          'engCapList'  => $engCapList,
          'fuel'        => $fuel
        );
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('settings/ajaxListVehicleSpecification')->with($data);
    }
    
    // CSV upload in vehicle specification page code, form submit
    public function vehicleSpecificationcsvToMysql()
    {
      $tmpName = $_FILES['upload_csv']['tmp_name'];
      $fileCsv = $tmpName;
      $fileObj = fopen($fileCsv,'r');
      $valSkip = 0;
      $mngname = "";
      $i = 0;
      $xy = 0;
      $err = 0;
      $postVal = array();
      $postValKey = array();
     
      $res = Fetch::getVehicleSettingData();
      $mfdList = explode(';',$res->manufacturer);
      $engCapList = explode(';',$res->engine_capacity);  
      //print_r($engCapList);
      //echo "<br/>"; 
      $mfNew = 0;
      $capNew = 0;
      $aryTit = array();
      while($data=fgetcsv($fileObj, 10000, ",")) 
      {
        $modNew = 0;
        $varNew = 0;
        if($valSkip == 0)
        {
          $xi = 0;
          //print_r($data);
          for($i=13;$i<count($data);$i++)
          {
            $aryTit[$xi] = $data[$i];
            $xi++;
          }
          //print_r($aryTit);
        }   
        if($valSkip != 0)
        {
          $mfd = trim($data[0]);
          if($mfd != "")
          {
            if(in_array($mfd, $mfdList))
            {
              $keyMfd = array_search($mfd, $mfdList);       
            }
            else
            {
              $mfNew++; 
              $keyMfd = count($mfdList);
              $mfdList[$keyMfd] = $mfd;
            }
          }
          
          $engC = trim($data[4]);
          if($engC != "")
          {
            if(in_array($engC, $engCapList))
            {
              $keyEngC = array_search($engC, $engCapList);        
            }
            else
            {
              $capNew++;  
              $keyEngC = count($engCapList);
              $engCapList[$keyEngC] = $engC;
            }
          }
         
          $resM    = Fetch::getVehicleInfoDataById($keyMfd);
          $varList = array();
          $modList = array();

          if(count($resM) >= 1)
          {
            $varList = explode(';',$resM[0]->var);
            $modList = explode(';',$resM[0]->model);
          }

          $fuel = (trim($data[2]) == 'Petrol') ? 1 : 2;
          $mod = trim($data[1]);
          if($mod != "")
          {
            if(in_array($mod, $modList))
            {
              $keyMode = array_search($mod, $modList);        
            }
            else
            {
              $modNew++;  
              $keyMode = count($modList);
              $modList[$keyMode] = $mod;
            }
          }
          $var = trim($data[3]);
          if($var != "")
          {
            if(in_array($var, $varList))
            {
              $keyVar = array_search($var, $varList);       
            }
            else
            {
              $varNew++;  
              $keyVar = count($varList);
              $varList[$keyVar] = $var;
            }
          }

          if($varNew >= 1 || $modNew >= 1)
          {
            if(count($resM) <= 0)
            {
              $postVal = array();
              $postVal['mfd'] = $keyMfd;
              $postVal['var'] = implode(';',$varList);
              $postVal['model'] = implode(';',$modList);
              Common::insert('vehinfo',$postVal);
            }
            else
            {
              $postVal = array();
              $postVal['id'] = $resM[0]->id;        
              $postVal['mfd'] = $keyMfd;
              $postVal['var'] = implode(';',$varList);
              $postVal['model'] = implode(';',$modList);
              Common::updateTable('vehinfo',array('id' => $postVal['id']),$postVal);
            }
          }
          
          $mfg_yr = trim($data[12]);
          $dat = Fetch::getVehicleSpecDataBYFields($keyMfd,$keyMode,$fuel,$keyVar,$keyEngC,$mfg_yr);

          $postVal = array();             
          $postVal['mfd'] = $keyMfd;
          $postVal['model'] = $keyMode;
          $postVal['varient'] = $keyVar;
          $postVal['fuel'] = $fuel;
          $postVal['engcap'] = $keyEngC;
          
          $postVal['maxtog'] = trim($data[5]);
          $postVal['rpm'] = trim($data[6]);
          $postVal['maxpow'] = trim($data[7]);
          $postVal['rpm2'] = trim($data[8]);
          $postVal['maxspeed'] = trim($data[9]);
          $postVal['btype'] = trim($data[10]);
          $postVal['map'] = trim($data[11]);
          $postVal['mfg_yr'] = trim($data[12]);
          
          $datOth = array();
          $xi = 0;
          for($i=13;$i<count($data);$i++)
          {
            $datOth[$aryTit[$xi]] = $data[$i];
            $xi++;
          }
          $postVal['specinfo'] = json_encode($datOth);
          
          if(count($dat) >= 1)
          {
            $version = $dat[0]->version + '0.1';
            $postVal['version'] = round($version,1);
            $postVal['id'] = $dat[0]->id; 
            Common::updateTable('vehspec',array('id' => $postVal['id']),$postVal);
          }
          else
          {
            $postVal['version'] = '1.0';
            Common::insert('vehspec',$postVal);
          }

        }
        else
        {
          if(trim($data[0]) != "Manufacturer")
          {
            return false;
          }
        }   
        $valSkip++;
      }
      if($mfNew >= 1 || $capNew >= 1)
      {
        $postVal = array();
        $postVal['id'] = 1;
        $postVal['manufacturer'] = implode(';',$mfdList);
        $postVal['engine_capacity'] = implode(';',$engCapList);
        Common::updateTable('vehicle_setting',array('id' => $postVal['id']),$postVal);
      }
      session()->forget('form_token');
      \Session::flash('message','Vehicle Data Imported Successfully.');
      return redirect('vehicle-specification');
    }

  // Description for service settings start
  public function descList(){
    $descList = Fetch::getDescPartAll();
    $data = array(
        'title'      => 'Description Type View',
        'data_list'  => $descList
    );
    $data['sest_open'] = '1';
    $data['ss_open']   = '1';
    $data['sst_open']  = '1';
    $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    return view('settings/service-settings/desc/list')->with($data);
  }

  public function addNewDesc(){
      $data = array(
          'title'        => 'Add Desc Data',
          'form_token'   => Helper::token(8) 
      );
      $data['sest_open'] = '1';
      $data['ss_open']   = '1';
      $data['sst_open']  = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('settings/service-settings/desc/add')->with($data);
  }

  public function createDescType(Request $request){
      $token = request('form_token');
      if($token == session('form_token')){
      session()->forget('form_token');
      $title    = $request->post('title');
      $sub_titl = $request->post('sub_titl');
      $status   = $request->post('status');

       // Other field validation
       if(empty($title) || empty($sub_titl) || empty($status)){
           \Session::flash('message','Fields required.');
           return redirect('add-desc-new');
           exit;
       }

       $data = array(
       'title'     => $title,
       'sub_title' => $sub_titl,
       'status'    => $status,
       'ad_by'     => session('userdata')['uid'],
       );
       Common::insert('service_desc_sett',$data);
       \Session::flash('message','value Data Added Successfully.');
        return redirect('desc-list');

       }else{
           session()->forget('form_token');
           \Session::flash('message','Sorry, token expired. Try again.');
            return redirect('add-desc-new');
       }
  }

  public function editDesc($id=null){
        $id           = Helper::crypt(2,$id);
        $editDescList = Fetch::getDescById($id);
        $data = array(
            'title'      => 'Edit Description',
            'desc_data'  => $editDescList,
            'form_token' => Helper::token(8) 
        );
       $data['sest_open'] = '1';
       $data['ss_open']   = '1';
       $data['sst_open']  = '1'; 
       $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
       return view('settings/service-settings/desc/edit')->with($data);
  }

  public function editDescList(Request $request){
       $token    = request('form_token');
       $cid      = $request->post('cid');
       $id       = Helper::crypt(2,$cid);
       $title    = $request->post('title');
       $sub_titl = $request->post('subtitl');
       $status   = $request->post('status');
           // Other field validation

      if(empty($title) || empty($sub_titl) || $status == ''){
           \Session::flash('message','Fields required.');
           return redirect('edit-desc/'.$cid);
           exit;
      }
       $data = array(
       'title'     => $title,
       'sub_title' => $sub_titl,
       'status'    => $status,
       'ad_by'     => session('userdata')['uid']
       );

     Common::updateTable('service_desc_sett',array('id' => $id),$data);
     \Session::flash('message','Description Data Updated Successfully.');
     return redirect('desc-list');
   }
  // Description for service settings end

  public function clearDbOptimize(Request $request){
        $date      = $request->get('date1');
        $date      = str_replace('/', '-', $date );
        $newDate   = date("Y-m-d", strtotime($date));
        $device12  = $request->get('device_id');
        $clearData = Fetch::deleteBefore($newDate,$device12);
        if($clearData){
          return 1;
        }
        else{
          return 2;
        }
  }

  public function clearDbOptimizeRange(Request $request){
    $date       = $request->get('date2');
    $split_date = explode("-",$date);
    $fdate      = $split_date[0];
    $fdate      = date('Y-m-d',strtotime($fdate));
    $todate     = $split_date[1];
    $todate     = date('Y-m-d',strtotime($todate));
    $device_id2 = $request->get('device_id2');
    $clearData  = Fetch::deleteBeforeRange($fdate,$todate,$device_id2);
    if($clearData){
    return 1;
    }
    else{
      return 2;
    }
  }

  public function activeList(){
    $user                = session('userdata');
    $activeList = Fetch::getActiveCodeAll($user['uid'],$user['ucategory']);
    $data = array(
        'title'       => 'Activation Code View',
        'active_list' => $activeList
     );
    $data['mast_open'] = '1';
    $data['acti_open'] = '1';
    $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    return view('settings/activation/activelist')->with($data);
  }

  //Edit Action
  public function editActive($id=null){
    $id             = Helper::crypt(2,$id);
    $editActiveList = Fetch::getActiveById($id);
    $data = array(
        'title'       => 'Edit Activation Code',
        'active_data' => $editActiveList,
        'form_token'  => Helper::token(8) 
    );
    $data['mast_open'] = '1';
    $data['acti_open'] = '1';
    $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    return view('settings/activation/edit')->with($data);
  }

  public function editActiveSave(Request $request){
         $token      = request('form_token');
         $cid        = $request->post('cid');
         $id         = Helper::crypt(2,$cid);
         $act_code   = $request->post('ac_code');
         $used_by    = $request->post('used_by');
         $mc_id      = $request->post('mc_id');
         $exp_dt     = $request->post('exp_dt');
         $used_email = $request->post('used_email');
         $status     = $request->post('status');
        // Other field validation
        if(empty($act_code) || empty($used_by)  || empty($exp_dt) || empty($used_email)){
               \Session::flash('message','Fields required.');
               return redirect('edit-active/'.$cid);
               exit;
        }

         $data = array(
         'activation_code' => $act_code,
         'used_by'         => $used_by,
         'mac_id'          => $mc_id,
         'expdt'           => $exp_dt,
         'email'           => $used_email,
         'status'          => $status,
         'ad_by'           => session('userdata')['uid']
         );

         Common::updateTable('activation_code',array('id' => $id),$data);
         if($request->savem == "savem"){
         $data  = "";
         $data  = new \stdClass();
         $data->name = $act_code;
         Mail::to("".$used_email)->send(new Activationcode($data));
         }
         
         if($mc_id != 0)
         {
            $devicedata = Fetch::getdevice($mc_id);
            if(!empty($devicedata))
            {
              // print_r($devicedata);die;
              $vehicle = Fetch::getVehicleDeviceDetails($mc_id);
              // print_r($vehicle);die;
              $deviceid = Helper::crypt(1,$devicedata[0]->id);
              if(empty($vehicle))
              {
                \Session::flash('message','Activation Data Updated Successfully, Now add Vehicle.');
                return redirect('add-vehicle/'.$deviceid);
              }
              else
              {
                \Session::flash('message','Activation Data Updated Successfully.');
                return redirect('activation-code');
              }
            }
            else
            {
              \Session::flash('message','Activation Data Updated Successfully.');
                return redirect('activation-code');
            }
            // return redirect('add-vehicle/'.$mc_id);
            // return redirect('activation-code');
            exit();
         }
         \Session::flash('message','Activation Data Updated Successfully.');
         return redirect('activation-code');
  }

  public function addNewActive(){
      $data = array(
            'title'       => 'Add Active Data',
            'form_token'  => Helper::token(8),
            'form_token1' => Helper::token1(10)
      );
      $data['mast_open'] = '1';
      $data['acti_open'] = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('settings/activation/add')->with($data);
  }

  public function createActiveType(Request $request){
          $token = request('form_token');
          if($token == session('form_token')){
          session()->forget('form_token');
          $act_code    = $request->post('ac_code');
          $used_by     = $request->post('used_by');
          $mc_id       = $request->post('mc_id');
          $exp_dt      = $request->post('exp_dt');
          $time        = strtotime($exp_dt);
          $newformat   = date('Y-m-d',$time);
          $u_email     = $request->post('u_email');
          $status      = $request->post('status');
           // Other field validation
          if(empty($act_code) || empty($used_by) || empty($exp_dt) || empty($u_email)){
               \Session::flash('message','Fields required.');
               return redirect('add-Active');
               exit;
          }
          
          $data = array(
         'activation_code' => $act_code,
         'used_by'         => $used_by,
         'mac_id'          => $mc_id,
         'expdt'           => $newformat,
         'email'           => $u_email,
         'status'          => $status,
         'ad_by'           => session('userdata')['uid'],
         );

        Common::insert('activation_code',$data);
        if($request->savem == "savem"){
           $data  = "";
           $data  = new \stdClass();
           $data->act_code = $act_code;
           Mail::to("".$u_email)->send(new Activationcode($data));
        }

        \Session::flash('message','Active Data Added Successfully.');
        return redirect('activation-code');

        }else{
              session()->forget('form_token');
              \Session::flash('message','Sorry, token expired. Try again.');
              return redirect('add-Active');
        }
  }
}
