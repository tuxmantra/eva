<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;

class Configuration extends Controller
{
   public function newconfig(){

        $superadmin = Fetch::getUserTypeDataByCategoryId(1);
        $admin      = Fetch::getUserTypeDataByCategoryId(2);
        $partner    = Fetch::getUserTypeDataByCategoryId(3);
        $driver     = Fetch::getUserTypeDataByCategoryId(4);
        $data = array(
        'title'      => 'New Configuration',
        'superadmin' => $superadmin,
        'admin'      => $admin,  
        'partner'    => $partner,  
        'driver'     => $driver  
        );
      $data['conf_open']  = '1';
      $data['new_config'] = '1';
      $data['menu']       = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    	return view('config/newconfig')->with($data);
	}

	public function createconfig(){
		$value    = request('val');
		$label    = request('lab');
        if($label == 'Driver'){
            $value = "Driver ".$value;
        }
        if($label == 'Partner'){
            $value = "Partner ".$value;
        }
        $status   = Fetch::getCountUserTypeByName($value);


        if($label == 'Driver' && $status[0]->count == 0){
              $name     = $value." 1";
              $category = 4;
        }elseif($label == 'Driver' && $status[0]->count != 0){
              $inc      = $status[0]->count+1;
	          $name     = $value." ".$inc;
              $category = 4;
        }else{
		    
            if($label == 'Admin'){
	                
	            if($status[0]->count != 0){

			          $inc      = $status[0]->count+1;
			          $name     = $value." ".$inc;
		              $category = 2;
			    }else{
                      $name     = "Admin 1";
                      $category = 2;
			    }

            }elseif($label == 'Partner'){

	            if($status[0]->count != 0){

			          $inc      = $status[0]->count+1;
			          $name     = $value." ".$inc;
		              $category = 3;
			    }else{
                      $name     = $value." 1";
		              $category = 3;
			    }

            }
	    }

	      $data = array(
              'u_category'      => $category,
              'u_name'          => $name,
              'all_acc'         => '0',
              'home'            => '0',
              'settings'        => '0',
              'masters'         => '0',
              'users'           => '0',
              'device'          => '0',
              'system'          => '0',
              'vehicle'         => '0',
              'data'            => '0',
              'core_part'       => '0',
              'value_part'      => '0',
              'value_sub_part'  => '0',
              'map'             => '0',
              'drive'           => '0',
              'diagnostics'     => '0',
              'service'         => '0',
              'statistics'      => '0',
              'report'          => '0',
              'action'          => '0',
              'user_type'       => '0',
              'service_sett'    => '0',
              'descrp_sett'     => '0',
              'glossary'        => '0',
              'activation_code' => '0',
              'digno'           => '0',
              'dbase'           => '0',
              'engin'           => '0',
              'vehspic'         => '0',
              'alertmail'       => '0',
              'alert'           => '0',
              'devicep'         => '0',
              'drivesd'         => '0',
              'eva'             => '0'
              );
            Common::insert('user_type',$data);
	          echo $name;
	}

	public function allotconfig(){
        
        $userlist = Fetch::getUserTypeDataByIdOrderBy();
        $data = array(
        'title'     => 'User Type',
        'user_type' => $userlist  
        );
      $data['conf_open']    = '1';  
      $data['allot_config'] = '1';
      $data['menu']         = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);  
    	return view('config/allotconfig')->with($data);
    }

    public function getuserconfig(){
    	  $utype   = request('utype');
        $enable  = Fetch::getUserTypeDataByTypeId($utype);
        $modules = Fetch::getMainModules();
        $enable  = json_decode(json_encode($enable), true); 
        echo '<div class="col-sm-2" align="right"><label style="padding: 0 0 0 0;" class="col-form-label">New Config</label></div>
                      <div class="col-sm-10 checkbox-radios">';

                      echo '<div class="container"><div class="row"><div class="col-md-12">';
                    foreach($modules as $key => $mod){
                    	echo '<div class="col-md-6">';
                        echo '<div class="form-check">
                          <label class="form-check-label">
                            <input id="mod-'; echo $key; echo '" name="config[]" class="form-check-input" type="checkbox" value="'; echo $mod->id; echo '" '; if($enable[0][$mod->u_field]){ echo 'checked'; } echo '>'; echo ucwords($mod->name); 
                            echo '<span class="form-check-sign">
                              <span class="check"></span>
                            </span>
                          </label></div>';

                          // Main module line Above

                          // Sub module line Start

                          $submod = Fetch::getSubModule($mod->id);
                          if(!empty($submod)){
	                          	foreach($submod as $sey => $sub){
	                          		
			                          echo '<div class="form-check" style="margin-left: 65px; padding: 11px 11px 11px 11px;">
			                          <label class="form-check-label">
			                            <input id="sub-'; echo $sey.'-'.$key; echo '" name="config[]" class="form-check-input" type="checkbox" value="'; echo $sub->id; echo '"'; if($enable[0][$sub->u_field]){ echo 'checked'; } echo '>'; echo ucwords($sub->name); echo '<span class="form-check-sign">
			                              <span class="check"></span>
			                            </span>
			                          </label></div>';

			                          // Sub sub module line start
			                          $subsubmod = Fetch::getSubModule($sub->id);
                                      if(!empty($subsubmod)){
	                          	         foreach($subsubmod as $ley => $subsub){

		                                      echo '<div class="form-check" style="margin-left: 120px; padding: 11px 11px 11px 11px;">
					                          <label class="form-check-label">
					                            <input id="sub-'; echo $sey.'-'.$key.'-'.$ley; echo '" name="config[]" class="form-check-input" type="checkbox" value="'; echo $subsub->id; echo '"'; if($enable[0][$subsub->u_field]){ echo 'checked'; } echo '>'; echo ucwords($subsub->name); echo '<span class="form-check-sign">
					                              <span class="check"></span>
					                            </span>
					                          </label></div>

 <script type="text/javascript">

      $("#mod-0").change(function() {
          if(this.checked) {
             $( "#sub-0-0").prop("checked", true);
             $( "#sub-1-0").prop("checked", true);
             $( "#sub-2-0").prop("checked", true);
             $( "#sub-3-0").prop("checked", true);
          }else{
             $( "#sub-0-0").prop("checked", false);
             $( "#sub-1-0").prop("checked", false);
             $( "#sub-2-0").prop("checked", false);
             $( "#sub-3-0").prop("checked", false);
          }
      });

      $("#mod-1").change(function() {
          if(this.checked) {
             $( "#sub-0-1").prop("checked", true);
             $( "#sub-1-1").prop("checked", true);
             $( "#sub-2-1").prop("checked", true);
             $( "#sub-2-1-0").prop("checked", true);
             $( "#sub-2-1-1").prop("checked", true);
             $( "#sub-2-1-2").prop("checked", true);
             $( "#sub-3-1").prop("checked", true);
             $( "#sub-4-1").prop("checked", true);
             $( "#sub-4-1-0").prop("checked", true);
             $( "#sub-5-1").prop("checked", true);
             $( "#sub-6-1").prop("checked", true);
             $( "#sub-7-1").prop("checked", true);
          }else{
             $( "#sub-0-1").prop("checked", false);
             $( "#sub-1-1").prop("checked", false);
             $( "#sub-2-1").prop("checked", false);
             $( "#sub-2-1-0").prop("checked", false);
             $( "#sub-2-1-1").prop("checked", false);
             $( "#sub-2-1-2").prop("checked", false);
             $( "#sub-3-1").prop("checked", false);
             $( "#sub-4-1").prop("checked", false);
             $( "#sub-4-1-0").prop("checked", false);
             $( "#sub-5-1").prop("checked", false);
             $( "#sub-6-1").prop("checked", false);
             $( "#sub-7-1").prop("checked", false);
          }
      });

      $("#sub-2-1").change(function() {
          if(this.checked) {
          
             $( "#sub-2-1-0").prop("checked", true);
             $( "#sub-2-1-1").prop("checked", true);
             $( "#sub-2-1-2").prop("checked", true);
           
          }else{
          
             $( "#sub-2-1-0").prop("checked", false);
             $( "#sub-2-1-1").prop("checked", false);
             $( "#sub-2-1-2").prop("checked", false);
          }
      });

       $("#sub-4-1").change(function() {
          if(this.checked) {
          
             $( "#sub-4-1-0").prop("checked", true);
           
          }else{
          
              $( "#sub-4-1-0").prop("checked", false);
          }
      });

 </script>';

					                      }
					                  }

			                          // Sub sub module line end

			                    
			                    }
                          }
                          // Sub module line End

                          // Main module line Down
                    
                    echo '</div>';  
                    }
                    echo '</div></div></div><hr>';

    }

    public function update_config_usertype(Request $request){
    	$config    = $request->post('check');
    	$disconfig = $request->post('uncheck');
    	$utype     = $request->post('utype');
      
      if(!empty($config)){
        	for($i=0;$i<=count($config)-1;$i++){
        		     $val      = $config[$i];
                 $mod_name = Fetch::getModuleById($val);
                 $column   = $mod_name[0]->u_field;
                 Common::updateTable('user_type',array('u_type' => $utype),array($column => '1'));
        	}
      }

      if(!empty($disconfig)){
          for($i=0;$i<=count($disconfig)-1;$i++){
             $val      = $disconfig[$i];
             $mod_name = Fetch::getModuleById($val);
             $column   = $mod_name[0]->u_field;
             Common::updateTable('user_type',array('u_type' => $utype),array($column => '0'));
          }
      }
    }
}
