<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Illuminate\Support\Facades\Schema;
use Helper;

class Data extends Controller
{
    public function coreList(){
      	$dataList = Fetch::getCorePartAll();
      	$data     = array(
  	        'title'      => 'Core-Part-List',
  	        'data_list'  => $dataList
        );
        $data['sest_open'] = '1';
        $data['ddt_open']  = '1';
        $data['cr_open']   = '1';
        $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      	return view('data/core/list')->with($data);
    }

    //Add the data part
    public function addNewCore(){
      $data = array(
          'title'        => 'Add Core Data',
          'form_token'   => Helper::token(8) 
      );
      $data['sest_open'] = '1';
      $data['ddt_open']  = '1';
      $data['cr_open']   = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('data/core/add')->with($data);
    }
    public function createCoreNew(Request $request){
      $token = request('form_token');
      if($token == session('form_token')){

         session()->forget('form_token');
         $title   = $request->post('title');
         $device1 = $request->post('device1');
         $device2 = $request->post('device2');
         $decimal = $request->post('decimal');
         $unit    = $request->post('unit');
         $status  = $request->post('status');

          // Other field validation

           if(empty($title) || empty($device1) || empty($device2) || $decimal == '' || empty($unit) || empty($status)){
               \Session::flash('message','Fields required.');
               return redirect('add-core');
               exit;
           }

           $data = array(
           'title'   => $title,
           'device1' => $device1,
           'device2' => $device2,
           'decim'   => $decimal,
           'unit'    => $unit,
           'status'  => $status,
           'ad_by'   => session('userdata')['uid']
           );
           Common::insert('corepart',$data);
           \Session::flash('message','Core Data Added Successfully.');
           return redirect('data-core-list');

       }else{
           session()->forget('form_token');
           \Session::flash('message','Sorry, token expired now try.');
            return redirect('add-core');
       }
    }

    public function editCore($id){
      $id       = Helper::crypt(2,$id);
      $coreData = Fetch::getDataById($id);
      $data     = array(
        'title'      =>  'Edit User',
        'core_data'  =>   $coreData,
        'form_token' =>   Helper::token(8) 
      );
      $data['sest_open'] = '1';
      $data['ddt_open']  = '1';
      $data['cr_open']   = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('data/core/edit')->with($data);
    }

    public function editCoreData(Request $request){
       $token   = request('form_token');
       $cid     = $request->post('cid');
       $id      = Helper::crypt(2,$cid);
       $title   = $request->post('title');
       $device1 = $request->post('device1');
       $device2 = $request->post('device2');
       $decimal = $request->post('decimal');
       $unit    = $request->post('unit');
       $status  = $request->post('status');
           // Other field validation

      if(empty($title) || empty($device1) || empty($device2) || $decimal == '' || empty($unit) || empty($status)){
           \Session::flash('message','Fields required.');
           return redirect('edit-core/'.$cid);
           exit;
      }
       $data = array(
       'title'   => $title,
       'device1' => $device1,
       'device2' => $device2,
       'decim'   => $decimal,
       'unit'    => $unit,
       'status'  => $status,
       'ad_by'   => session('userdata')['uid'],
       );

     Common::updateTable('corepart',array('id' => $id),$data);
     \Session::flash('message','Core Data Updated Successfully.');
     return redirect('data-core-list');
   }

    // core data end
    // value part start

   //Listing the Value Part
    public function valueList(){
      $valueList = Fetch::getValuePartAll();
      $data = array(
          'title'      => 'Value-Part Details',
          'value_list' => $valueList
      );
      $data['sest_open'] = '1';
      $data['ddt_open']  = '1';
      $data['vl_open']   = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('data/valuepart')->with($data);
    }

     //Add the value part
    public function addNewValue(){
      $data = array(
          'title'      => 'Add Value Data',
          'form_token' => Helper::token(8) 
      );
      $data['sest_open'] = '1';
      $data['ddt_open']  = '1';
      $data['vl_open']   = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('data/value/add')->with($data);
    }

     //save core data part
    public function createValueNew(Request $request){
        $token = request('form_token');
        if($token == session('form_token')){
        session()->forget('form_token');
        $title   = $request->post('title');
        $device1 = $request->post('device1');
        $device2 = $request->post('device2');
        $decimal = $request->post('decimal');
        $unit    = $request->post('unit');
        $type    = $request->post('type');
        $dtype1    = $request->post('dtype');
        $status  = $request->post('status');
        $clmn    = $request->post('clmn_name');
        $dtype   =implode(":",$dtype1);
        // print_r($dtype);die;
         // Other field validation
         if(empty($title) || empty($device1) || empty($device2) || $decimal == '' || empty($type) || empty($clmn) ||  empty($status)){
             \Session::flash('message','Fields required.');
             return redirect('add-value');
             exit;
         }

          $result = Fetch::getColumnStatus('device_data',$clmn);
          if(!empty($result)){
             \Session::flash('message','Entered Column Name Already Exist.');
             return redirect('add-value');
          }else{
            Common::createColumn('device_data',$clmn);
          }

         $data = array(
         'title'                => $title,
         'device1'              => $device1,
         'device2'              => $device2,
         'decim'                => $decimal,
         'unit'                 => $unit,
         'type'                 => $type,
         'dtype'                 => $dtype,
         'status'               => $status,
         'ad_by'                => session('userdata')['uid'],
         'device_data_column'   => $clmn
         );
         Common::insert('valuepart',$data);
         \Session::flash('message','value Data Added Successfully.');
         return redirect('data-value-list');

       }else{
           session()->forget('form_token');
           \Session::flash('message','Sorry, token expired. Try again.');
           return redirect('add-value');
       }
    }

   // value start

   //Edit the Value List
    public function editValue($id=null){
      $id       = Helper::crypt(2,$id);
      $editValueList = Fetch::getValueById($id);
      $data = array(
          'title'      => 'Edit Value',
          'value_data' => $editValueList,
          'form_token' => Helper::token(8) 
      );
      $data['sest_open'] = '1';
      $data['ddt_open']  = '1';
      $data['vl_open']   = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('data/value/edit')->with($data);
    }
   
    public function editValueData(Request $request){

       $token    = request('form_token');
       $cid      = $request->post('cid');
       $id       = Helper::crypt(2,$cid);
       $title    = $request->post('title');
       $device1  = $request->post('device1');
       $device2  = $request->post('device2');
       $decimal  = $request->post('decimal');
       $unit     = $request->post('unit');
       $type     = $request->post('val_type');
       $dtype1     = $request->post('dtype');
       $status   = $request->post('status');
       $dtype   =implode(":",$dtype1);
      // Other field validation

      if(empty($title) || empty($device1) || empty($device2) || $decimal == '' || empty($type)){
           \Session::flash('message','Fields required.');
           return redirect('edit-value/'.$cid);
           exit;
      }

     $data = array(
     'title'              => $title,
     'device1'            => $device1,
     'device2'            => $device2,
     'decim'              => $decimal,
     'unit'               => $unit,
     'type'               => $type,
     'dtype'               => $dtype,
     'status'             => $status,
     'ad_by'              => session('userdata')['uid']
     );

     Common::updateTable('valuepart',array('id' => $id),$data);
     \Session::flash('message','Value Data Updated Successfully.');
     return redirect('data-value-list');
   }
   // value end
}
