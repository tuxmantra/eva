<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newuser;


class Api extends Controller
{

  public function test_login(Request $request){
      $response = array();
      $email    = $request->post('email');
      $password = $request->post('password');
      // echo  $email; exit;
        // varify
        if(empty($email)){
            $response['error'] = '0';
            $response['data']  = 'Email required.';
        }elseif(empty($password)){
            $response['error'] = '0';
            $response['data']  = 'Password required.';
        }
        else{
        // process
          $password = md5($password);
          $token    = Helper::token1(10);
          $exist_email = Fetch::getUserByEmail($email);
          // print_r($password); echo "Mayue";
          // print_r($exist_email[0]->upwd); 
          // die;
          if(!empty($exist_email)){
               if($password != $exist_email[0]->upwd){
                  
                   $response['data']  = 'Wrong password entered.';
               }elseif($password == $exist_email[0]->upwd){
                  // Login Success
                  $login_data    = array(
                  'user_id'      =>  $exist_email[0]->uid,
                  'login_token'  => $token,
                  'ip'           => '',
                  'mode'         => 'Mobile',
                  'login_status' => '1'
                  );
                  $login_id = Common::insertGetId('login_session',$login_data);
                  $userdata = array(
                  'uid'          =>  $exist_email[0]->uid,
                  'utype'        =>  $exist_email[0]->utype,
                  'uemail'       =>  $exist_email[0]->uemail,
                  'login_token'  => $token,
                  'ucategory'    =>  $exist_email[0]->ucategory,
                  'login_status' => '1'
                  );
                  $response['success'] = '1';
                  $response['data']    = $userdata;
               }else{
                   $response['error'] = "0";
                   $response['data']  = 'User not exist.';
               }
            }else{
                   $response['error'] = "0";
                   $response['data']  = 'User not exist.';
            }
        }
        echo json_encode($response);
    }

     public function device_location(Request $request){
        
         $data = json_decode($request->data, TRUE);
         $deviceID=$data['devID'];
         $devInfo = Fetch::getdevice($deviceID);

                   if($deviceID != "" && $devInfo != "")
            {
              $msg= "Data updated successfully";
              $arrayData['success'] = TRUE;

              $lastLocation =Fetch::locationLastApi($deviceID);
              // print_r($lastLocation[0]->lat);Die;
              if(!empty($lastLocation))
              {
                $arrayData['lat']  = $lastLocation[0]->lat;
                $arrayData['long'] = $lastLocation[0]->lon;
                $arrayData['alt']  = $lastLocation[0]->alt;
                $arrayData['ts']   = $lastLocation[0]->time_s;
              }
              else
              {
                $arrayData['success'] = FALSE;
                $arrayData['errorCode'] = "1003";
                $arrayData['errorMessage'] = "No location Info!";
              }
            }
        echo json_encode($arrayData);
    }

public function drived_range(Request $request){
    $data = json_decode($request->data, TRUE);
    $deviceID=$data['devID'];
    $driveStart=$data['driveidstart'];
    $driveEnd=$data['driveidend'];
    $devInfo = Fetch::getdevice($deviceID);
    // $did = $devInfo[0]->id;
    $yVal = 0;
    if(!empty($devInfo)){
           //echo "Mayur";
           $dat1 = Fetch::getStatsByDriveRange($deviceID,$driveStart,$driveEnd);
          // print_r($dat);getStatsByDriveRange_data
           $dat = Fetch::getStatsByDriveRange_data($deviceID,$driveStart,$driveEnd);
           $fr  = 1;
           $to  = $dat1[0]->count;
           $arryI = 0;
           $i=0;
            foreach ($dat as $value){
             
              $reqFor    = $i; //$fr;
              $arrayData[$reqFor] = array();
              $arryI++;
              $dtFA      = $value->dt;
              $dtFA      = strtotime(str_replace(':', '-', $dtFA));
              $date      = date('d:m:Y',$dtFA);
              $durTr     = $value->dur;
              $distTr    = $value->dist;
              $avSpeed   = $value->avg_speed;
              $maxSpeed  = $value->max_sp;
              $totCon    = $value->ful_c;
              $totMil    = $value->avg_ml;
              $maxTor    = $value->max_tor;
              $fOver     = $value->ful_ovr;
              $idT       = $value->idt;
              $ovT       = $value->dur_ovr;
              //EC added extra parameter for dist in overrun
              $ovD       = $value->dist_ovr;
              $hacc      = $value->hacc;
              $hdcc      = $value->hdcc;
              $ovrspd    = $value->ovrspd;
              $vehstall  = $value->vehstall;
              $driveID   = $value->nodrive;
              $timestamp = strtotime($value->dfrm);
              
              // if($timestamp > $durTr)
              // {
              //  $timestamp = $timestamp - $durTr;
              // }
              // else
              // {
              //  //Do nothing
              // }
              
              $arrayData[$reqFor]['driveno']        = $driveID + 0.0;
              $arrayData[$reqFor]['drive_id']       = $driveID + 0.0;
              $arrayData[$reqFor]['date']           = $date;
              $arrayData[$reqFor]['ts']             = $timestamp * 1000;
              $arrayData[$reqFor]['drive_duration'] = round($durTr,0);//$durTr;
              $arrayData[$reqFor]['drive_dist']     = round($distTr,4);
              $arrayData[$reqFor]['avg_speed']      = round($avSpeed,2);
              $arrayData[$reqFor]['max_speed']      = round($maxSpeed,0);//$maxSpeed;
              $arrayData[$reqFor]['fuel_consumed']  = round($totCon,4);
              $arrayData[$reqFor]['mileage']        = round($totMil,4);
              $arrayData[$reqFor]['max_tor']        = round($maxTor,2);
              
              //EC added extra parameter for dist in overrun
              $arrayData[$reqFor]['dist_ovr']       = round($ovD,4);
              $arrayData[$reqFor]['fuel_save_ovr']  = round($fOver,3);
              $arrayData[$reqFor]['idle_time']      = round($idT,0);//$idT;
              $arrayData[$reqFor]['time_ovr']       = round($ovT,0);//$ovT;
              $arrayData[$reqFor]['hacc']           = round($hacc,0);
              $arrayData[$reqFor]['hdcc']           = round($hdcc,0);
              $arrayData[$reqFor]['ovrspd']         = round($ovrspd,0);
              $arrayData[$reqFor]['vehstall']       = round($vehstall,0);
              $i++;
            }
              $myData = array();
              $myData = $arrayData;
              $arrayData = array();
              $arrayData['data'] = $myData;

              $arrayData['success'] = TRUE;
              $arrayData['type']    = "drive";
              $arrayData['count']   = count($myData);
              //print_r($value);echo "</br>";
            
      
           
    }else{
        $arrayData['success'] = FALSE;
          $arrayData['errorCode'] = "1003";
          $arrayData['errorMessage'] = "Invalid Type!";
        }
       // print_r(dd( $arrayData));
          $resultVal = json_encode($arrayData);
           echo json_encode($arrayData);
         // print_r($resultVal);
    }


    public function login(Request $request){
      // echo "i am here";
      
      $response = array();
        // input
      $email    = $request->post('email');
      $password = $request->post('password');
      // print_r($$email);die;
        // varify
        if(empty($email)){
            $response['error'] = '0';
            $response['data']  = 'Email required.';
        }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $response['error'] = '0';
            $response['data']  = 'Enter valid email.';
        }elseif(empty($password)){
            $response['error'] = '0';
            $response['data']  = 'Password required.';
        }else{
        // process
          $password = md5($password);
          $token    = Helper::token1(10);
          $users    = DB::select("select uid,uname,utype,uemail,ucategory,ustatus,(CASE WHEN uemail = '$email' AND upwd != '$password' THEN '1' WHEN uemail != '$email' AND upwd = '$password' THEN '2' WHEN uemail = '$email' AND upwd = '$password' THEN '3' ELSE '4' END) as status from users where uemail = ?",[$email]);
          if(!empty($users)){
               if($users[0]->status == '1'){
                   $response['error'] = "0";
                   $response['data']  = 'Wrong password entered.';
               }elseif($users[0]->status == '2'){
                   $response['error'] = "0";
                   $response['data']  = 'Wrong email entered.';
               }elseif($users[0]->status == '3'){
                  // Login Success
                  $login_data    = array(
                  'user_id'      => $users[0]->uid,
                  'login_token'  => $token,
                  'ip'           => '',
                  'mode'         => 'Mobile',
                  'login_status' => '1'
                  );
                  $login_id = Common::insertGetId('login_session',$login_data);
                  $userdata = array(
                  'uid'          => $users[0]->uid,
                  'utype'        => $users[0]->utype,
                  'uemail'       => $users[0]->uemail,
                  'login_token'  => $token,
                  'ucategory'    => $users[0]->ucategory,
                  'login_status' => '1'
                  );
                  $response['success'] = '1';
                  $response['data']    = $userdata;
               }else{
                   $response['error'] = "0";
                   $response['data']  = 'User not exist.';
               }
            }else{
                   $response['error'] = "0";
                   $response['data']  = 'User not exist.';
            }
        }
        echo json_encode($response);
    }

    public function addUser(Request $request){
           
       $response = array();
       // input
       $user_type = $request->post('user_type');
       $name      = $request->post('name');
       $mobile    = $request->post('mobile');
       $email     = $request->post('email');
       $password  = $request->post('password');
       $address   = $request->post('address');
       $status    = $request->post('status');
       $pd_status = $request->post('partner_device_status');
       $partner   = '0';
       $device    = '0';
       // Varify
       // emil validation
       $exist_email = Fetch::getUserByEmail($email);
       if(!empty($exist_email)){
            $response['error'] = "0";
            $response['data']  = 'User already exist.';
            echo json_encode($response);
            exit;
       }

       if(empty($user_type)){
           $response['error'] = "0";
         $response['data']  = 'User Type required.';
         echo json_encode($response);
         exit;
       }elseif(empty($name)){
           $response['error'] = "0";
         $response['data']  = 'Name required.';
         echo json_encode($response);
         exit;
       }elseif(empty($mobile)){
           $response['error'] = "0";
         $response['data']  = 'Mobile required.';
         echo json_encode($response);
         exit;
       }elseif(empty($email)){
           $response['error'] = "0";
         $response['data']  = 'Email required.';
         echo json_encode($response);
         exit;
       }elseif(empty($password)){
           $response['error'] = "0";
         $response['data']  = 'Password required.';
         echo json_encode($response);
         exit;
       }elseif(empty($address)){
         $response['error'] = "0";
         $response['data']  = 'Address required.';
         echo json_encode($response);
         exit;
       }

       // partner and device validation if selected
       if($pd_status == 1){
            $partner = $request->post('partner');
            $device  = $request->post('device');
            if(empty($partner)){
              $response['error'] = "0";
              $response['data']  = 'Partner field required.';
              echo json_encode($response);
              exit;
            }elseif(empty($device)){
              $response['error'] = "0";
              $response['data']  = 'Device field required.';
              echo json_encode($response);
              exit;
            }
       } 
       // Process

       $category = Fetch::getUserCategoryByUserType($user_type);

       $data = array(
       'utype'     => $user_type,
       'ucategory' => $category[0]->u_category,
       'uname'     => $name,
       'umob'      => $mobile,
       'uemail'    => $email,
       'upwd'      => md5($password),
       'ulocation' => $address,
       'ustatus'   => $status,
       'ad_by'     => '1',
       'partner'   => $partner,
       'device'    => $device
       ); 
       Common::insert('users',$data);
       //$to = "praveen.rao@enginecal.com,support@enginecal.com";
       $response['success'] = "1";
       $response['data']    = 'User created.';
       $data  = new \stdClass();
       $data->name = $name;
       Mail::to("".$email)->send(new Newuser($data));
       echo json_encode($response);
    }

    public function checkValidSub(Request $request){
        $response = array();
        $devId    = $request->get('data');
        $devId    = json_decode($devId);

        if(!empty($devId)){
            $result   = Fetch::getdeviceActStatus($devId->devID);
            if($result[0] != "" && $result[0]->status == 1)
            {
              $response['success'] = TRUE;
              $response['status']  = "1";
              $response['subEnd']  = $result[0]->expdt;
            }
            elseif($result[0] != "" && $result[0]->status == 0)
            {
              $response['success']      = FALSE;
              $response['status']       = "0";
              $response['errorCode']    = "1003";
              $response['errorMessage'] = "Subscription expired!";
            }
            else
            {
              $response['success']      = FALSE;
              $response['errorCode']    = "1001";
              $response['errorMessage'] = "Invalid Device ID!";
            }
        }else{
            $response['success']       = FALSE;
            $response['errorCode']     = "1002";
            $response['errorMessage']  = "Invalid Input!";
        }

        echo json_encode($response);
    }

    public function monitorFlag(Request $request){
        echo "i am here";
        $response = array();
        $devId    = $request->get('data');
        // print_r($devId);die;
        $devId    = json_decode($devId);

        if(!empty($devId)){
            $devInfo = Fetch::getIdByDeviceId($devId->devID);
            if(!empty($devInfo))
            {
              $result   = Fetch::getLastDriveNum($devId->devID);
              $currStat = $result->fetch_assoc();
              $driveNo  = 0;
              if (trim($currStat['nodrive']) != "")
              {
                  $driveNo = $currStat['nodrive'] + 0;
              }
              $arrayData['success'] = TRUE;
              $arrayData['flag']    = 0;
              $arrayData['upcalib'] = 0;
              $arrayData['driveno'] = $driveNo;
            }
            else
            {
              $response['success']      = FALSE;
              $response['errorCode']    = "1001";
              $response['errorMessage'] = "Invalid Device ID!";
            }
        }else{
            $response['success']       = FALSE;
            $response['errorCode']     = "1002";
            $response['errorMessage']  = "Invalid Input!";
        }

        echo json_encode($response);
    }
}
