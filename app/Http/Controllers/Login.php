<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helper;
use Illuminate\Support\Facades\DB;
use App\Model\Common;

class Login extends Controller
{
    //
    public function index(){
        if(!empty(session('userdata'))){
          // print_r(session('userdata'));die;
          if(session('userdata')['ucategory'] == 4)
          {
            return redirect('dashboard');
          }
          else
          {
            return redirect('dashboard');
          }
        }
        $data = array(
        'form_token' => Helper::token(8)
        );
    	return view('main/login')->with($data);
    }

    public function varify(){
      if(!empty(session('userdata'))){
          if(session('userdata')['ucategory'] == 4)
          {
            return redirect('dashboard');
          }
          else
          {
            return redirect('dashboard');
          }
      }
     
      $token    = Helper::cryptoJsAesDecrypt('yhws4p2bo8d50udoab2jznd41t1hmyoichakn7imu1fhjjsujv',request('aw7UWP49pAUsLNYb'));
    	$email    = Helper::cryptoJsAesDecrypt('yhws4p2bo8d50udoab2jznd41t1hmyoichakn7imu1fhjjsujv',request('emailid'));
    	$password = Helper::cryptoJsAesDecrypt('yhws4p2bo8d50udoab2jznd41t1hmyoichakn7imu1fhjjsujv',request('password'));
    	$password = md5($password);

    	if($token == session('form_token')){
           session()->forget('form_token');
           // varify
                $users = DB::select("select uid,uname,utype,uemail,ucategory,ustatus,(CASE WHEN uemail = '$email' AND upwd != '$password' THEN '1' WHEN uemail != '$email' AND upwd = '$password' THEN '2' WHEN uemail = '$email' AND upwd = '$password' THEN '3' ELSE '4' END) as status from users where uemail = ?",[$email]);
                 
                if(!empty($users)){
                   if($users[0]->status == '1'){
                     \Session::flash('message','wrong password entered.');
                     return redirect('login');
                   }elseif($users[0]->status == '2'){
                     \Session::flash('message','wrong email entered.');
                     return redirect('login');
                   }elseif($users[0]->status == '3'){
                   	  $login_data    = array(
                      'user_id'      => $users[0]->uid,
                      'login_token'  => $token,
                      'ip'           => $_SERVER['REMOTE_ADDR'],
                      'mode'         => 'PC',
                      'login_status' => '1'
                      );
                      $login_id = Common::insertGetId('login_session',$login_data);
                      $userdata['userdata'] = array(
                      'uid'         => $users[0]->uid,
                      'utype'       => $users[0]->utype,
                      'uemail'      => $users[0]->uemail,
                      'login_token' => $token,
                      'ucategory'   => $users[0]->ucategory,
                      );
                      session($userdata);
                      if($users[0]->ucategory == 4)
                      {
                        return redirect('dashboard');
                      }
                      else
                      {
                        return redirect('dashboard');
                      }
                   }else{
                    \Session::flash('message','Email password wrong.');
                    return redirect('login');
                   }
                }else{
                   \Session::flash('message','Email password wrong.');
                   return redirect('login');
                }
    	}else{
            session()->forget('form_token');
            \Session::flash('message','No UnAuthorised Access.');
            return redirect('login');
    	}
    	
    }

    public function logout(){	
    	DB::table('login_session')->where('login_token', session('userdata')['login_token'])->update(['logout_time' => date('Y-m-d H:i:s'), 'login_status' => '0']);
    	session()->flush();
    	return redirect('login');
    }

    public function logout_manual(){ 
      DB::table('login_session')->where('login_token', session('userdata')['login_token'])->update(['logout_time' => date('Y-m-d H:i:s'), 'login_status' => '0']);
      session()->flush();
    }

    public function sessionVarify(){
       /* $token = request('wcqr2h8zy1wjqeax6ayay4eh1nw3glpmtwz1m7uevp98wios');
        $data['status'] = 0;
        if(!empty($token)){
            if($token != session('userdata')['login_token'] && !empty(session('userdata'))){
                 $data['status'] = 1;
                 $this->logout_manual();
            }
        }
        echo json_encode($data['status']);*/
    }

    public function destroy(){
       session()->flush();
    }

    public function menu(){  
        return view('main/menu');
    }
}
