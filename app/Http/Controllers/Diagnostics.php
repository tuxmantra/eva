<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Helper;
use App\Model\Common;
use App\Model\Fetch;

class Diagnostics extends Controller
{
    public function index(Request $request)
    {
        $data['option']    = $request->post('option');
        $data['adid']      = $request->post('devid');
        $user              = session('userdata');
        $data['user_type'] = $user['utype'];
        $deviceList        = Fetch::getDeviceListByUserType($user['uid'],$user['ucategory']);
        if(!empty($deviceList))
        {
          	if(!empty(session('device_id')))
            {
                $did           = session('device_id');
            }else
            {       
                 $did               = $deviceList[0]->device_id;
            }
          	$driveDates = Fetch::getDriveDatesByDeviceIdEvent($did);
          
          	$data_array = "";
          	foreach($driveDates as $sdt){
             	$data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',"; 
          	}
        }
        else
        {
          	$did = '';
          	$data_array = '';
        }
        
        $date = date('Y-m-d');
        $data = array(
          'title'         => 'Diagnostics',
          'device_list'   => $deviceList,
          'user_category' => $user['ucategory'],        
          'did'           => $did,
          'drive_dates'   => rtrim($data_array,','),
          'option'        => $request->post('option'),
          'adid'          => $request->post('devid')
        );
        $data['digi'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      	return view('diagnostic/index')->with($data);
    }

	public function getDiagnoLoad(Request $request)
	{
		$did         = $request->post('did');
		session(['device_id' => $did]);
        $id          = Fetch::getIdByDeviceId($did);
        $id          = $id[0]->id;
        $device_data = Fetch::getDeviceById($id);

        /* Diagnostic Start */
        // DTC Value
        $dtc  = [];
        $dtcd = (trim($device_data[0]->dtcv) != ""  && trim($device_data[0]->dtcv) != "0") ? explode(',',$device_data[0]->dtcv) : "";
        if(!empty($dtcd))
        {
        	foreach ($dtcd as $key => $value1) 
			{
				if(strrchr($value1,"P") != $value1)
				{
				            	
	            	//calc's for P U B C  
	            	$errCodeType = $value1 & 0xC000;
	            	$errCodeType = $errCodeType >> 14;
	              
	            	//echo "$errCodeType";
	            	if($errCodeType == 1) 
	              	{
	                  	$errcodeHexStr = "C";
	              	} 
	              	elseif($errCodeType == 2) 
	              	{
	                  	$errcodeHexStr = "B";
	              	} 
	              	elseif($errCodeType == 3) 
	              	{
	                  	$errcodeHexStr = "U";
	              	} 
	              	else 
	              	{
	                  	$errcodeHexStr = "P";
	              	}
	              	// end P U B C
	              	
	              	//Bit-wise AND the decimal value from ECU to get 2nd byte.
	              	$errCodeTmp = $value1 & 0x3000;
	              	$errCodeTmp = $errCodeTmp >> 12;      //Right shift by 12 bits to get 2nd byte.
	             	$hx = dechex($errCodeTmp);  
	              	$errcodeHexStr = $errcodeHexStr."$hx";
	              	
	              	//Bit-wise AND the decimal value from ECU to get the 3rd byte
	              	$errCodeTmp = $value1 & 0x0F00;
	              	$errCodeTmp = $errCodeTmp >> 8;       //Right shift by 8 bits to get the last DTC character.
	              	$hx = dechex($errCodeTmp);  
	              	$errcodeHexStr = $errcodeHexStr."$hx";
	              	
	              	$errCodeTmp = $value1 & 0x00FF;
	              	if($errCodeTmp <= 0x0A) 
	              	{
	                  	$errcodeHexStr = $errcodeHexStr."0";
	                  	$hx = dechex($errCodeTmp);  
	                	$errcodeHexStr = $errcodeHexStr."$hx";
	              	}
	              	else
	              	{
	                  	$hx = dechex($errCodeTmp);  
	                	$errcodeHexStr = $errcodeHexStr."$hx";
	              	}
              	
	              	//$hx = (count($hx) <= 3) ? "0$hx" : $hx; no need
	              	//$dtcTxt[$i] = "P$hx"; old code
              		$dtc[] = strtoupper($errcodeHexStr);
              	}
              	else
              	{
              		$dtc[] = $value1;
              	}
			}
		}
        
        // Coolants
        $cols = $device_data[0]->cools;

        // unset($device_data);
        $dtcTxt = "";
        if(empty($dtc)){
            $dtc = array();
        }
       
        $dtc = implode(',', $dtc);
        $dtcTxt = $dtc;

        $co2      = Fetch::getDriveValue($did,'cur_co2');
        $co2Tot   = Fetch::getDriveValue($did,'tot_co2','max');
        $co2Avg   = Fetch::getDriveValue($did,'avg_co2','max');
        $oillife  = Fetch::getDriveValue($did,'oillife','');
        $oillifep = Fetch::getDriveValue($did,'oillifep','');

        //$MAPIdle = $devAct -> getCalStageDataRange(2,'f3',"max",$reqFor,'F3',$deviceID,'F5',1,2,'F9',3);

        $statusO = "";
        if($oillifep <= 5)
        {
          $statusO = "CHANGE OIL";
        }
        elseif($oillifep <= 50)
        {
          $statusO = "CHECK OIL LEVEL";
        }
        else {
          $statusO = "OIL IS GOOD";
        }
       
        $score = 100;
        for($i=80;$i<=260;$i+=20)
        {
          	$nI = $i-20;
          
          	if($i == 80 && $co2 < $i)
          	{
            	$score = 100;
            	break;
         	}
          	elseif($co2 > $nI && $co2 <= $i)
          	{
            	$score = $score;
            	break;
          	}
          	//echo "$co2 > $nI && $co2 <= $i = $score <br/>"; 
          	$score -= 10;
        }       

        /*$date   = $request->post('date');*/
        /*$date   = date('Y-m-d',strtotime($date));*/
		$mainMoniterData = Fetch::getDriveMoniterMain($did);
        $data['option']  =$request->post('option');
        $data['did']              = $did;
        $data['dtcTxt']           = $dtcTxt;
        $data['co2']              = $co2;
        $data['co2Avg']           = $co2Avg;
        $data['cols']             = $cols;
        $data['mainMoniter']      = $mainMoniterData;
        $data['score']            = $score;

       	/* Diagnostic End */
       	
       	$vehicle     = Fetch::getVehicleById($device_data[0]->id);
        $calval      = json_decode($vehicle->calval);
       	$calib_data  = Fetch::getDeviceCalib($did);

       	/* Calib Start */

       	if(!empty($calib_data))
       	{
	        $batHel = array();
	        $batHel['rate'] = "";
	        $batHel['con']  = "";
	        $batHel['msg']  = "";
	        if($calib_data[0]->bat_st == 1)
	        {
	          	$batHel['rate'] = "1";
	          	$batHel['con'] = "Bad";
	          	$batHel['msg'] = "\"Battery Critical\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
	        }
	        elseif($calib_data[0]->bat_st == 2)
	        {
	          	$batHel['rate'] = "2";
	          	$batHel['con'] = "Bad–Okay";
	          	$batHel['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
	        }
	        elseif($calib_data[0]->bat_st == 3)
	        {
	          	$batHel['rate'] = "3";
	          	$batHel['con'] = "Bad-Okay";
	          	$batHel['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
	        }
	        elseif($calib_data[0]->bat_st == 4)
	        {
	          	$batHel['rate'] = "4";
	          	$batHel['con'] = "Okay-Good";
	          	$batHel['msg'] = "\"Battery Low\" Please drive to charge.";
	        }
	        elseif($calib_data[0]->bat_st == 5)
	        {
	          	$batHel['rate'] = "5";
	          	$batHel['con'] = "Good";
	          	$batHel['msg'] = "";
	        }
	        else
	        {
	          	$batHel['rate'] = "";
	          	$batHel['con'] = "";
	          	$batHel['msg'] = "PID Not Supported";
	        }

	        //Turbo
	        $turbo =array();
	        $turbo['rate'] = "";
	        $turbo['con']  = "";
	        $turbo['msg']  = "";
	        if($calib_data[0]->idlb_c == 1)
	        {
	          	if($calib_data[0]->pekb_c == 1)
	          	{
	           		$turbo['rate'] = "1";
	            	$turbo['con'] = "Bad";
	            	$turbo['msg'] = "Turbo boost pressure build-up critical. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->pekb_c == 2)
	          	{
	            	$turbo['rate'] = "2";
	            	$turbo['con'] = "Bad-Okay";
	            	$turbo['msg'] = "Turbo boost pressure build-up poor. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->pekb_c == 3)
	          	{
	            	$turbo['rate'] = "3";
	            	$turbo['con'] = "Okay";
	            	$turbo['msg'] = "Turbo boost pressure build-up below average. Clean/replace air filter element.";
	          	}
	        }
	        elseif($calib_data[0]->idlb_c == 2)
	        {
	          	if($calib_data[0]->pekb_c == 1)
	          	{
	            	$turbo['rate'] = "1";
	            	$turbo['con'] = "Bad";
	            	$turbo['msg'] = "Turbo boost pressure build-up critical. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->pekb_c == 2)
	          	{
	            	$turbo['rate'] = "3";
	            	$turbo['con'] = "Okay";
	            	$turbo['msg'] = "Turbo boost pressure build-up below average. Clean/replace air filter element.";
	          	}
	          	elseif($calib_data[0]->pekb_c == 3)
	          	{
	            	$turbo['rate'] = "4";
	            	$turbo['con'] = "Okay-Good";
	            	$turbo['msg'] = "Turbo boost pressure build-up average. Clean/replace air filter element. Eva will keep an eye on it during the next monitoring cycle.";
	          	}
	        }
	        elseif($calib_data[0]->idlb_c == 3)
	        {
	          	if($calib_data[0]->pekb_c == 1)
	          	{
	            	$turbo['rate'] = "2";
	            	$turbo['con'] = "Bad-Okay";
	            	$turbo['msg'] = "Turbo boost pressure build-up poor. Replace air filter element, check for disconnected air path hoses, and vacuum line leaks. Visit Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->pekb_c == 2)
	          	{
		            $turbo['rate'] = "3";
		            $turbo['con'] = "Okay";
		            $turbo['msg'] = "Turbo boost pressure build-up below average. Clean/replace air filter element.";
	          	}
	          	elseif($calib_data[0]->pekb_c == 3)
	          	{
		            $turbo['rate'] = "5";
		            $turbo['con'] = "Good";
		            $turbo['msg'] = "";
	          	}
	        }
	        else
	        {
	          	$turbo['rate'] = "";
	        }
	        // $turbo['msg'] = (trim($calib_data[0]->idlb_m) != trim($calib_data[0]->pekb_m) ) ? trim($calib_data[0]->idlb_m) ."<br/>". trim($calib_data[0]->pekb_m) : trim($calib_data[0]->pekb_m);

	        //Air
	        $air = array();
	        $air['rate'] = "";
	        $air['con']  = "";
	        $air['msg']  = "";
	        if($calib_data[0]->idlbc_v == 1)
	        {
	          	if($calib_data[0]->peakb_v == 1)
	          	{
		            $air['rate'] = "1";
		            $air['con'] = "Bad";
		            $air['msg'] = "Air system pressure build-up critical. Check/replace air filter element. Visit the Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->peakb_v == 2)
	         	{
		            $air['rate'] = "2";
		            $air['con'] = "Bad-Okay";
		            $air['msg'] = "Air system pressure build-up poor. Check/replace air filter element. Visit the Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->peakb_v == 3)
	          	{
		            $air['rate'] = "3";
		            $air['con'] = "Okay";
		            $air['msg'] = "Air system pressure build-up below average. Check/replace air filter element.";
	          	}
	        }
	        elseif($calib_data[0]->idlbc_v == 2)
	        {
	          	if($calib_data[0]->peakb_v == 1)
	          	{
		            $air['rate'] = "1";
		            $air['con'] = "Bad";
		            $air['msg'] = "Air system pressure build-up critical. Check/replace air filter element. Visit the Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->peakb_v == 2)
	          	{
		            $air['rate'] = "3";
		            $air['con'] = "Okay";
		            $air['msg'] = "Air system pressure build-up below average. Check/replace air filter element.";
	          	}
	          	elseif($calib_data[0]->peakb_v == 3)
	          	{
		            $air['rate'] = "4";
		            $air['con'] = "Okay-Good";
		            $air['msg'] = "Air system pressure build-up average. Check/replace air filter element. Eva will keep an eye on it during the next monitoring cycle.";
	          	}
	        }
	        elseif($calib_data[0]->idlbc_v == 3)
	        {
	          	if($calib_data[0]->peakb_v == 1)
	          	{
		            $air['rate'] = "2";
		            $air['con'] = "Bad-Okay";
		            $air['msg'] = "Air system pressure build-up poor. Check/replace air filter element. Visit the Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->peakb_v == 2)
	          	{
		            $air['rate'] = "3";
		            $air['con'] = "Okay";
		            $air['msg'] = "Air system pressure build-up below average. Check/replace air filter element.";
	          	}
	          	elseif($calib_data[0]->peakb_v == 3)
	          	{
		            $air['rate'] = "5";
		            $air['con'] = "Good";
		            $air['msg'] = "";
	          	}
	        }
	        else
	        {
	          	$air['rate'] = "";
	        }
	        // $air['msg'] = (trim($calib_data[0]->idlbc_m) != trim($calib_data[0]->peakb_m) ) ? trim($calib_data[0]->idlbc_m) ."<br/>". trim($calib_data[0]->peakb_m) :  trim($calib_data[0]->peakb_m);

	        //Fuel
	        $fuel = array();
	        $fuel['rate'] = "";
	        $fuel['con']  = "";
	        $fuel['msg']  = "";
	        if($calib_data[0]->idl_crp == 1)
	        {
	          	if($calib_data[0]->peak_crp == 1)
	          	{
		            $fuel['rate'] = "1";
		            $fuel['con'] = "Bad";
		            $fuel['msg'] = "Fuel pressure critical. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->peak_crp == 2)
	          	{
		            $fuel['rate'] = "2";
		            $fuel['con'] = "Bad-Okay";
		            $fuel['msg'] = "Fuel pressure low. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->peak_crp == 3)
	          	{
		            $fuel['rate'] = "3";
		            $fuel['con'] = "Okay";
		            $fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
	          	}
	        }
	        elseif($calib_data[0]->idl_crp == 2)
	        {
	          	if($calib_data[0]->peak_crp == 1)
	          	{
		            $fuel['rate'] = "1";
		            $fuel['con'] = "Bad";
		            $fuel['msg'] = "Fuel pressure critical. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->peak_crp == 2)
	          	{
		            $fuel['rate'] = "3";
		            $fuel['con'] = "Okay";
		            $fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
	          	}
	          	elseif($calib_data[0]->peak_crp == 3)
	          	{
		            $fuel['rate'] = "4";
		            $fuel['con'] = "Okay-Good";
		            $fuel['msg'] = "Fuel pressure build-up average. Eva will keep an eye on it during the next monitoring cycle.";
	          	}
	        }
	        elseif($calib_data[0]->idl_crp == 3)
	        {
	          	if($calib_data[0]->peak_crp == 1)
	          	{
		            $fuel['rate'] = "2";
		            $fuel['con'] = "Bad-Okay";
		            $fuel['msg'] = "Fuel pressure low. Consider changing the fuel-filter before moving to deeper investigation. Visit the Manufacturer Dealership whenever possible.";
	          	}
	          	elseif($calib_data[0]->peak_crp == 2)
	          	{
		            $fuel['rate'] = "3";
		            $fuel['con'] = "Okay";
		            $fuel['msg'] = "Fuel pressure build-up below peak capability. Consider changing the fuel-filter.";
	          	}
	          	elseif($calib_data[0]->peak_crp == 3)
	          	{
		            $fuel['rate'] = "5";
		            $fuel['con'] = "Good";
		            $fuel['msg'] = "";
	          	}
	        }
	        else
	        {
	          	$fuel['rate'] = "";
	        }
	        // $fuel['msg'] = $calib_data[0]->crpm;

	        //combustionPressure
	        $copPres = array();
	        $copPres['rate'] = "";
	        $copPres['con']  = "";
	        $copPres['msg']  = "";
	        if($calib_data[0]->comp_pres == 1)
	        {
		        $copPres['rate'] = "1";
		        $copPres['con'] = "Bad";
	        }
	        elseif($calib_data[0]->comp_pres == 2)
	        {
	          	$copPres['rate'] = "2";
	          	$copPres['con'] = "Bad–Okay";
	        }
	        elseif($calib_data[0]->comp_pres == 3)
	        {
	          	$copPres['rate'] = "3";
	          	$copPres['con'] = "Okay";
	        }
	        elseif($calib_data[0]->comp_pres == 4)
	        {
	          	$copPres['rate'] = "4";
	          	$copPres['con'] = "Okay-Good";
	        }
	        elseif($calib_data[0]->comp_pres == 5)
	        {
	          	$copPres['rate'] = "5";
	          	$copPres['con'] = "Good";
	        }
	        else
	        {
	          	$copPres['rate'] = "0";
	          	$copPres['msg'] = $calib_data[0]->comp_pres;
	        }

	        //rpmOscillationAnomalyAnalysis
	        $rcOcil = array();
	        $rcOcil['rate'] = "";
	        $rcOcil['con']  = "";
	        $rcOcil['msg']  = "";
	        if($calib_data[0]->eng_block_ej == 1)
	        {
	          	$rcOcil['rate'] = "1";
	          	$rcOcil['con'] = "Bad";
	        }
	        elseif($calib_data[0]->eng_block_ej == 2)
	        {
	          	$rcOcil['rate'] = "2";
	          	$rcOcil['con'] = "Bad–Okay";
	        }
	        elseif($calib_data[0]->eng_block_ej == 3)
	        {
	          	$rcOcil['rate'] = "3";
	          	$rcOcil['con'] = "Okay";
	        }
	        elseif($calib_data[0]->eng_block_ej == 4)
	        {
	          	$rcOcil['rate'] = "4";
	          	$rcOcil['con'] = "Okay-Good";
	        }
	        elseif($calib_data[0]->eng_block_ej == 5)
	        {
	          	$rcOcil['rate'] = "5";
	          	$rcOcil['con'] = "Good";
	        }
	        else
	        {
	          	$rcOcil['rate'] = "";
	        }

	        //falseOdometerReading
	        $falOdMr = array();
	        $falOdMr['rate'] = "";
	        $falOdMr['con'] = "";
	        $falOdMr['msg'] = $calib_data[0]->eng_block_ej;

	        //getVehSpeedSence
	        $vehSpeed = array();
	        $vehSpeed['rate'] = "";
	        $vehSpeed['con'] = "";
	        //$vehSpeed['msg'] = ($data['vehspeed'] <= 0) ? "Vehicle Speed Sensor Failure possibility" : "Vehicle Speed Okay";
	        if($calib_data[0]->vehspeed == 0)
	        {
	          	$vehSpeed['msg'] = "Vehicle Speed Sensor Failure possibility";
	        }
	        elseif($calib_data[0]->vehspeed == 1)
	        {
	          	$vehSpeed['msg'] = "Vehicle Speed Okay";
	        }
	        elseif($calib_data[0]->vehspeed == "")
	        {
	          	$vehSpeed['msg'] = "";
	        }

	        // cool Temp
	        $coolTemp = array();
	        $coolTemp['rate'] = "";
	        $coolTemp['con'] = "";
	        $coolTemp['msg'] = "";
	        if($calib_data[0]->coolp == 1)
	        {
	          	$coolTemp['rate'] = "1";
	          	$coolTemp['con'] = "Bad";
	          	$coolTemp['msg'] = "Engine cooling poor. Check Coolant and Engine Oil Level. Visit the Manufacturer Dealership whenever possible.";
	        }
	        elseif($calib_data[0]->coolp == 2)
	        {
	          	$coolTemp['rate'] = "2";
	          	$coolTemp['con'] = "Bad–Okay";
	          	$coolTemp['msg'] = "Engine cooling inadequate. Check Coolant and Engine Oil Level. Top-up Coolant and Engine Oil if low.";
	        }
	        elseif($calib_data[0]->coolp == 3)
	        {
	          	$coolTemp['rate'] = "3";
	          	$coolTemp['con'] = "Okay";
	          	$coolTemp['msg'] = "Engine cooling below average. Check Coolant and Engine Oil Level. Top-up Coolant and Engine Oil if low.";
	        }
	        elseif($calib_data[0]->coolp == 4)
	        {
	          	$coolTemp['rate'] = "4";
	          	$coolTemp['con'] = "Okay-Good";
	          	$coolTemp['msg'] = "Engine cooling average. Eva will keep an eye on it during the next monitoring cycle.";
	        }
	        elseif($calib_data[0]->coolp == 5)
	        {
	          	$coolTemp['rate'] = "5";
	          	$coolTemp['con'] = "Good";
	          	$coolTemp['msg'] = "";
	        }
	        else
	        {
	          	$coolTemp['msg'] = $calib_data[0]->coolp;
	        }

	        //combustion pressure message
	       
          	if($copPres['rate'] == 5 || $copPres['rate'] == "")
          	{
            	$copPres['msg'] = "";
          	}
          	elseif($copPres['rate'] <= 4)
			{
				if($vehicle->fuel_tpe ==  "2")
				{
					if(($turbo['rate'] == 5 && $rcOcil['rate'] <= 4 && $fuel['rate'] == 5) || 
						($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] > 5) || 
						($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] == 5) || 
						($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4) ||
						($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] >= 5) ||
						($turbo['rate'] == 5 && $rcOcil['rate'] == 5 && $fuel['rate'] == 5) ||
						($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4))
					{
						if($copPres['rate'] <= 3 && $rcOcil['rate'] <= 2)
						{
							//$copPres['msg'] = "Your engine's combustion pressures are not good. This is attributed to the engine's compression pressure 
							//which may be due to engine block bore deterioration or an injector issue.";
							$copPres['msg'] = "This is attributed to the engine's compression pressure which may be due to engine block bore deterioration or an injector issue. If white/excessive tail-pipe smoke observed, possible bore failure.";
						}
						else 
						{
							$n = array();
							if($turbo['rate'] >= 1 && $turbo['rate'] <= 5)
							{
								$n = array($turbo['rate'],$fuel['rate']);
							}
							else
							{
								$n = array($air['rate'],$fuel['rate']);
							}
							
							$nVal = min($n);
							
							if($n[0] == $n[1])
							{
								if($nVal <= 3)
								{
									$copPres['msg'] = "Please consider checking the High Pressure Fuel System and/or the Turbocharger System.";
								}
								elseif($nVal == 5)
								{
									if($rcOcil['rate'] != 5)
									{
										$copPres['msg'] = "Consider regenerating the Catalytic Converter. Please also consider checking/cleaning the EGR System.";
									}
									else
									{
										$copPres['msg'] = "Additional drivetrain friction detected or possible engine wear.";
									}
								}
								else
								{
									//do nothing;
								}
							}
							else
							{
								if($fuel['rate'] <= 0 || $fuel['rate'] > 5)
								{
									if($n[0] == 5)
									{
										$copPres['msg'] = "We could not check the High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
									}
									else 
									{
										$copPres['msg'] = "Please consider checking the Turbocharger System.";
									}
								}
								else 
								{
									if($n[0] > $n[1])
									{
										$copPres['msg'] = "Please consider checking the High Pressure Fuel System.";
									}
									else
									{
										$copPres['msg'] = "Please consider checking the Turbocharger System.";
									}
								}
							}
						}
					}
				}
				else
				{
					// if($calval->TC_Status_C == 'Yes')
					// {
					// 	if(($air['rate'] == 5 && $rcOcil['rate'] <= 4) || ($air['rate'] <= 4 && $rcOcil['rate'] <= 5) || 
					// 	($air['rate'] <= 4 && $rcOcil['rate'] <= 5) || ($air['rate'] == 5 && $rcOcil['rate'] <= 5) || 
					// 	($air['rate'] <= 4 && $rcOcil['rate'] <= 5))
					// 	{
					// 		if($copPres['rate'] <= 3 && $rcOcil['rate'] <= 1)
					// 		{
					// 			$copPres['msg'] = "This is attributed to the engine's compression pressure which may be due to engine block bore deterioration or there may be an injector issue. If white/excessive tail-pipe smoke observed, possible bore failure.";
					// 		}
					// 		else
					// 		{
					// 			if($air['rate'] <= 4)
					// 			{
					// 				if($rcOcil['rate'] <= 4 && $air['rate'] == 4)
					// 				{
					// 					$copPres['msg'] = "Please consider checking the Air System and fuel injectors.";
					// 				}
					// 				else
					// 				{
					// 					$copPres['msg'] = "Please consider checking the fuel injectors and spark plugs.";
					// 				}
					// 			}
					// 			else
					// 			{
					// 				if($rcOcil['rate'] <= 4)
					// 				{
					// 					$copPres['msg'] = "Please consider checking the fuel injectors and spark plugs.";
					// 				}
					// 				else
					// 				{
					// 					$copPres['msg'] = "Additional drivetrain friction detected.";
					// 				}
					// 			}
					// 		}
					// 	}
					// }
					// else
					// {
						if(($turbo['rate'] == 5 && $rcOcil['rate'] <= 4 && $fuel['rate'] == 5) || 
							($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] > 5) || 
							($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] == 5) || 
							($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4) ||
							($turbo['rate'] == 5 && $rcOcil['rate'] <= 5 && $fuel['rate'] >= 5) ||
							($turbo['rate'] == 5 && $rcOcil['rate'] == 5 && $fuel['rate'] == 5) ||
							($turbo['rate'] <= 4 && $rcOcil['rate'] <= 5 && $fuel['rate'] <= 4))
						{
							if($copPres['rate'] <= 3 && $rcOcil['rate'] <= 2)
							{
								$copPres['msg'] = "This is attributed to the engine's compression pressure which may be due to engine block bore deterioration or an injector issue. If white/excessive tail-pipe smoke observed, possible bore failure.";
							}
							else 
							{
								$n = array();
								$n = array($turbo['rate'],$fuel['rate']);
								$nVal = min($n);
				
								if($n[0] == $n[1])
								{
									if($nVal <= 3)
									{
										$copPres['msg'] = "Please consider checking the High Pressure Fuel System and/or the Turbocharger System.";
									}
									elseif($nVal == 5)
									{
										if($rcOcil['rate'] != 5)
										{
											$copPres['msg'] = "Consider regenerating the Catalytic Converter. Please also consider checking/cleaning the EGR System.";
										}
										else
										{
											$copPres['msg'] = "Additional drivetrain friction detected.";
										}
									}
									else
									{
										//do nothing;
									}
								}
								else
								{
									if($fuel['rate'] <= 0 || $fuel['rate'] > 5)
									{
										if($n[0] == 5)
										{
											//$copPres['msg'] = "Your engine's combustion pressures are not good.";
											$copPres['msg'] = "We could not check the High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
										}
										else 
										{
											$copPres['msg'] = "Please consider checking the Turbocharger System.";
										}
									}
									else 
									{
										if($n[0] > $n[1])
										{
											$copPres['msg'] = "Please consider checking the High Pressure Fuel System.";
										}
										else
										{
											$copPres['msg'] = "Please consider checking the Turbocharger System.";
										}
									}
								}
							}
						}
						else
						{
							//do nothing
						}
					// }
				}
			}

	        $data['batHel']           = $batHel;
	        $data['turbo']            = $turbo;
	        $data['air']              = $air;
	        $data['fuel']             = $fuel;
	        $data['copPres']          = $copPres;
	        $data['rcOcil']           = $rcOcil;
	        $data['falOdMr']          = $falOdMr;
	        $data['vehSpeed']         = $vehSpeed;
	        $data['coolTemp']         = $coolTemp;
    	}
    	else
    	{
	        $data['batHel']           = 0;
	        $data['turbo']            = 0;
	        $data['air']              = 0;
	        $data['fuel']             = 0;
	        $data['copPres']          = 0;
	        $data['rcOcil']           = 0;
	        $data['falOdMr']          = 0;
	        $data['vehSpeed']         = 0;
	        $data['coolTemp']         = 0;
      	}

        /* Calib End */

        /* Monit Start */
      
        $monit_data = Fetch::getDriveMoniter_rating($did);
        $batHel1['rate'] = '5';
        $batHel1['con']  = 'Excellent';
        $batHel1['msg']  = '';

        if($vehicle->fuel_tpe ==  "2")
		{
			// air not supported
			 $air1['rate'] = '';
	         $air1['con']  = '';
	         $air1['msg']  = 'Not Supported';

			$turbo1['rate'] = '5';
	        $turbo1['con']  = 'Excellent';
	        $turbo1['msg']  = '';
		}
		else
		{
			// if($calval->TC_Status_C == 'Yes')
			// {
			// 	// air not supported
			// 	$air1['rate'] = '';
	  //           $air1['con']  = '';
	  //           $air1['msg']  = 'Not Supported';

			//     $turbo1['rate'] = '5';
	  //           $turbo1['con']  = 'Excellent';
	  //           $turbo1['msg']  = '';
			// }
			// else
			// {
				// Turbo Not Supported
				$air1['rate'] = '5';
	            $air1['con']  = 'Excellent';
	            $air1['msg']  = '';

			    $turbo1['rate'] = '';
	            $turbo1['con']  = '';
	            $turbo1['msg']  = 'Not Supported';
		    // }
		}
        $fuel1['rate'] = '5';
        $fuel1['con']  = 'Excellent';
        $fuel1['msg']  = '';

        $copPres1['rate'] = '5';
        $copPres1['con']  = 'Excellent';
        $copPres1['msg']  = '';

        $rcOcil1['rate'] = '5';
	    $rcOcil1['con']  = 'Excellent';
	    $rcOcil1['msg']  = '';

        $falOdMr1['msg']  = 'Odometer reading Okay';
        

        $vehSpeed1['msg'] = 'Vehicle Speed Okay';
        
        $coolTemp1['rate'] = '5';
        $coolTemp1['con']  = 'Excellent';
        $coolTemp1['msg']  = '';

        $backPre1['rate'] = '5';
        $backPre1['con']  = 'Excellent';
        $backPre1['msg']  = '';
        if(!empty($monit_data))
		{
			$batHel1['rate'] = $monit_data[0]->bat_rating;
	        $batHel1['con']  = $monit_data[0]->bat_cond;
	        $batHel1['msg']  = $monit_data[0]->bat_msg;

	        $turbo1['rate'] = $monit_data[0]->turbo_rating;
	        $turbo1['con']  = $monit_data[0]->turbo_cond;
	        $turbo1['msg']  = $monit_data[0]->turbo_msg;

	        $air1['rate'] = $monit_data[0]->air_rating;
	        $air1['con']  = $monit_data[0]->air_cond;
	        $air1['msg']  = $monit_data[0]->air_msg;

	        $fuel1['rate'] = $monit_data[0]->fuel_rating;
	        $fuel1['con']  = $monit_data[0]->fuel_cond;
	        $fuel1['msg']  = $monit_data[0]->fuel_msg;

	        $copPres1['rate'] = $monit_data[0]->comb_rating;
	        $copPres1['con']  = $monit_data[0]->comb_cond;
	        $copPres1['msg']  = $monit_data[0]->comb_msg;

	        $rcOcil1['rate'] = $monit_data[0]->rpm_rating;
	        $rcOcil1['con']  = $monit_data[0]->rpm_cond;
	        $rcOcil1['msg']  = $monit_data[0]->rpm_msg;

	        $falOdMr1['msg'] = $monit_data[0]->odometer_msg;

	        $vehSpeed1['rate'] = $monit_data[0]->speed_sensor_msg;

	        $coolTemp1['rate'] = $monit_data[0]->cooling_rating;
	        $coolTemp1['con']  = $monit_data[0]->cooling_cond;
	        $coolTemp1['msg']  = $monit_data[0]->cooling_msg;

	        $backPre1['rate'] = 5;
	        $backPre1['con']  = 'Excellent';
	        $backPre1['msg']  = '';
		}

	      $data['batHel1']           = $batHel1;
	      $data['turbo1']            = $turbo1;
	      $data['air1']              = $air1;
	      $data['fuel1']             = $fuel1;
	      $data['copPres1']          = $copPres1;
	      $data['rcOcil1']           = $rcOcil1;
	      $data['falOdMr1']          = $falOdMr1;
	      $data['vehSpeed1']         = $vehSpeed1;
	      $data['coolTemp1']         = $coolTemp1;
	      $data['backPre1']          = $backPre1;

        /* Monit End */
      	$result = view('diagnostic/load')->with($data);
      	return $result;
  	}

  	public function getgraph(Request $request)
  	{
        $did         = $request->post('did');
        $year        = $request->post('year');
        $month       = $request->post('month');
        $ptype       = $request->post('ptype');
        $id          = Fetch::getIdByDeviceId($did);
        $id          = $id[0]->id;
        $device_data = Fetch::getDeviceById($id);

        /* Diagnostic Start */

        /* Monit Start */
        if($month == 0)
        {
        	$data['yLabel'] = "Month";
        	$monit_data1 = Fetch::getDriveMoniter1($did,$year);
       	}
       	else
       	{
        	$data['yLabel'] = "Date";
        	$monit_data1 = Fetch::getDriveMoniter2($did,$year,$month);
       	}
        
        if(!empty($monit_data1))
        {
        	foreach ($monit_data1 as $key => $monit_data)
        	{
		        //Bat Hel
		        $batHel1         = array();
		        $batHel1['rate'] = "";
		        if($monit_data->bat_st == 1)
		        {
		        	$batHel1['rate'] = "1";
		        }
		        elseif($monit_data->bat_st == 2)
		        {
		        	$batHel1['rate'] = "2";
		        }
		        elseif($monit_data->bat_st == 3)
		        {
		        	$batHel1['rate'] = "3";
		        }
		        elseif($monit_data->bat_st == 4)
		        {
		        	$batHel1['rate'] = "4";
		        }
		        elseif($monit_data->bat_st == 5)
		        {
		        	$batHel1['rate'] = "5";
		        }
		        else
		        {
		        	$batHel1['rate'] = "";
		        }

		        //Turbo
		        $turbo1          = array();

		        //Air
		        $air1         = array();
		        $air1['rate'] = "";
		        
		        if($monit_data->idlbc_v == 1)
		        {
		        	if($monit_data->peakb_v == 1)
		            {
		            	$air1['rate'] = "1";
		            }
		            elseif($monit_data->peakb_v == 2)
		            {
		            	$air1['rate'] = "2";
		            }
		            elseif($monit_data->peakb_v == 3)
		            {
		            	$air1['rate'] = "3";
		             }
		        }
		        elseif($monit_data->idlbc_v == 2)
		        {
		            if($monit_data->peakb_v == 1)
		            {
		            	$air1['rate'] = "1";
		            }
		            elseif($monit_data->peakb_v == 2)
		            {
		              $air1['rate'] = "3";
		            }
		            elseif($monit_data->peakb_v == 3)
		            {
		              $air1['rate'] = "4";
		            }
		        }
		        elseif($monit_data->idlbc_v == 3)
		        {
		            if($monit_data->peakb_v == 1)
		            {
		              $air1['rate'] = "2";
		            }
		            elseif($monit_data->peakb_v == 2)
		            {
		              $air1['rate'] = "3";
		            }
		            elseif($monit_data->peakb_v == 3)
		            {
		              $air1['rate'] = "5";
		            }
		        }
		       
		        //Fuel
		        $fuel1         = array();
		        $fuel1['rate'] = "";
		         
		        //combustionPressure
		        $copPres1         = array();
		        $copPres1['rate'] = "";
		          
		        if($monit_data->comp_pres == 1)
		        {
		            $copPres1['rate'] = "1";
		        }
		        elseif($monit_data->comp_pres == 2)
		        {
		            $copPres1['rate'] = "2";
		        }
		        elseif($monit_data->comp_pres == 3)
		        {
		        	$copPres1['rate'] = "3";
		        }
		        elseif($monit_data->comp_pres == 4)
		        {
		            $copPres1['rate'] = "4";
		        }
		        elseif($monit_data->comp_pres == 5)
		        {
		            $copPres1['rate'] = "5";
		        }
		        else
		        {
		        	//Do nothing 
		        }

		        //rpmOscillationAnomalyAnalysis
		        $rcOcil1 = array();
		        $rcOcil1['rate'] = "";
		        
		        if($monit_data->eng_block_ej == 1)
		        {
		            $rcOcil1['rate'] = "1";
		        }
		        elseif($monit_data->eng_block_ej == 2)
		        {
		            $rcOcil1['rate'] = "2";
		        }
		        elseif($monit_data->eng_block_ej == 3)
		        {
		            $rcOcil1['rate'] = "3";
		        }
		        elseif($monit_data->eng_block_ej == 4)
		        {
		            $rcOcil1['rate'] = "4";
		        }
		        elseif($monit_data->eng_block_ej == 5)
		        {
		            $rcOcil1['rate'] = "5";
		        }
		        
		        //falseOdometerReading
		        $falOdMr1         = array();
		        $falOdMr1['rate'] = "";
		         
		        //getVehSpeedSence
		        $vehSpeed1 = array();
		        $vehSpeed1['rate'] = "";

		        // cool Temp
		        $coolTemp1         = array();
		        $coolTemp1['rate'] = "";
		          
		        if($monit_data->coolp == 1)
		        {
		            $coolTemp1['rate'] = "1";
		        }
		        elseif($monit_data->coolp == 2)
		        {
		            $coolTemp1['rate'] = "2";
		        }
		        elseif($monit_data->coolp == 3)
		        {
		            $coolTemp1['rate'] = "3";
		        }
		        elseif($monit_data->coolp == 4)
		        {
		            $coolTemp1['rate'] = "4";
		        }
		        elseif($monit_data->coolp == 5)
		        {
		            $coolTemp1['rate'] = "5";
		        }
		        else
		        {
		        	//Do nothing
		        }


		        //combustion pressure message
		        $turbo1['rate']='';
		        if($copPres1['rate'] == 5 || $copPres1['rate'] == "")
		        {
		        	//Do nothing
		        }
		        elseif($copPres1['rate'] <= 4)
		        { 
		            if((($turbo1['rate']==5 || $air1['rate']==5)  && $rcOcil1['rate']<=4) || (($turbo1['rate']<=4 || $air1['rate']<=4)&& $rcOcil1['rate']<=5 && $fuel1['rate'] <=0 )  || (($turbo1['rate']<=4 || $air1['rate']<=4)&& $rcOcil1['rate']<=5 && $fuel1['rate'] == 5)  || (($turbo1['rate']==5 || $air1['rate']==5)&& $rcOcil1['rate']<=5 && $fuel1['rate'] <=4 )  || (($turbo1['rate']<=4 || $air1['rate']<=4)&& $rcOcil1['rate']<=5 && $fuel1['rate'] <=4 ))
		            { 
		            	if($monit_data->ocilv >= 1)
		              	{
		                	//Do nothing
		              	}
		              	else 
		              	{
		                	$n1 = array();
		                	if($turbo1['rate'] >= 1)
		                	{
		                		$n1 = array($turbo1['rate'],$fuel1['rate']);
		                	}
		                	else
		                	{
		                		$n1 = array($air1['rate'],$fuel1['rate']);
		                	}

		                	$nVal1 = array();
		                	$nVal1 = min($n1);
		                	if(!empty($nVal1))
		                	{
		                    	if($nVal1[0] == $nVal1[1])
		                    	{
		                    		if($nVal1 <= 3)
		                    		{ 
		                        		//Do nothing
		                      		}
		                    	}
		                    	else
		                    	{
		                      		if($fuel1['rate'] <= 0)
		                     		{
		                        		if($nVal1[0] == 5)
		                        		{
		                          			//Do Nothinf
				                        }
				                        else 
				                        {
				                         	//Do nothing
				                        }
		                      		}
		                      		else 
		                      		{
		                        		if($nVal1[0]>$nVal1[1])
				                        {
				                        	//Do nothing 
				                        }
				                        else
				                        {
				                         	//Do nothing
				                        }
		                      		}
		                    	}
		                	}
		              	}
		            }
		        }
		        $date[$monit_data->date]              = $monit_data->date;
		        $batHel1a[$monit_data->date]          = $batHel1['rate'];
		        $turbo1a[$monit_data->date]           = $turbo1['rate'];
		        $air1a[$monit_data->date]             = $air1['rate'];
		        $fuel1a[$monit_data->date]            = $fuel1['rate'];
		        $copPres1a[$monit_data->date]         = $copPres1['rate'];
		        $rcOcil1a[$monit_data->date]          = $rcOcil1['rate'];
		        $falOdMr1a[$monit_data->date]         = $falOdMr1['rate'];
		        $vehSpeed1a[$monit_data->date]        = $vehSpeed1['rate'];
		        $coolTemp1a[$monit_data->date]        = $coolTemp1['rate'];
		        $backPre1a[$monit_data->date]         = 0;
			}    

			//forloop
	        // $data['batHel1']          = $batHel1a;
	        // $data['turbo1']           = $turbo1a;
	        // $data['air1']             = $air1a;
	        // $data['fuel1']            = $fuel1a;
	        // $data['copPres1']         = $copPres1a;
	        // $data['rcOcil1']          = $rcOcil1a;
	        // $data['falOdMr1']         = $falOdMr1a;
	        // $data['vehSpeed1']        = $vehSpeed1a;
	        // $data['coolTemp1']        = $coolTemp1a;
	        // $data['backPre1']         = $backPre1a;
      	}
      	else
      	{
            $date[]       = '';
            $batHel1a[]   = '';
            $turbo1a[]    = 0;
            $air1a[]      = 0;
            $fuel1a[]     = 0;
            $copPres1a[]  = 0;
            $rcOcil1a[]   = 0;
            $falOdMr1a[]  = 0;
            $vehSpeed1a[] = 0;
            $coolTemp1a[] = 0;
            $backPre1a[]  = 0;
     	}
        $data['date']      = $date;
        $data['batHel1']   = $batHel1a;
        $data['turbo1']    = $turbo1a;
        $data['air1']      = $air1a;
        $data['fuel1']     = $fuel1a;
        $data['copPres1']  = $copPres1a;
        $data['rcOcil1']   = $rcOcil1a;
        $data['falOdMr1']  = $falOdMr1a;
        $data['vehSpeed1'] = $vehSpeed1a;
        $data['coolTemp1'] = $coolTemp1a;
        $data['backPre1']  = $backPre1a;
         /* Monit End */
        $curYear           = date('Y');
        if($month != 0)
        {
          	// Month cal
          	$dateString      = $year.'-'.$month.'-10';
           	$lastDateOfMonth = date("Y-m-t", strtotime($dateString));
          	$arr             = explode("-", $lastDateOfMonth);
          	$count           = $arr[2];
         	if($month == date('m') && $year == $curYear)
         	{ 
           		$count = date('d');
         	}
         	for($i=1;$i<=$count;$i++)
         	{
    			if($i == 1)
    			{
          			if (array_key_exists("1", $date))
          			{
          				//Do nothing
          			}
          			else
          			{
               			$date[$i]       = $i;
               			$batHel1a[$i]   = '';
               			$turbo1a[$i]    = 0;
               			$air1a[$i]      = 0;
              			$fuel1a[$i]     = 0;
              			$copPres1a[$i]  = 0;
              			$rcOcil1a[$i]   = 0;
              			$falOdMr1a[$i]  = 0;
              			$vehSpeed1a[$i] = 0;
              			$coolTemp1a[$i] = 0;
              			$backPre1a[$i]  = 0;
          			}
        		}
          		elseif($i != 1)
          		{
          			if (array_key_exists($i, $date))
          			{
			            // $date[$i]=$i;
			            // $batHel1a[$i]          = $batHel1a[$i];
			            // $turbo1a[$i]          =  $turbo1a[$i];
			            // $air1a[$i]             = $air1a[$i];
			            // $fuel1a[$i]            = $fuel1a[$i] ;
			            // $copPres1a[$i]       = $copPres1a[$i] ;
			            // $rcOcil1a[$i]          = $rcOcil1a[$i];
			            // $falOdMr1a[$i]         = $falOdMr1a[$i];
			            // $vehSpeed1a[$i]       = $vehSpeed1a[$i];
			            // $coolTemp1a[$i]        = $coolTemp1a[$i];
			            // $backPre1a[$i]        = $backPre1a[$i];
			            if(empty($batHel1a[$i]))
            			{
            				$batHel1a[$i]   = $batHel1a[$i-1];
            			}
            			if(empty($turbo1a[$i]))
            			{
            				$turbo1a[$i]          =  $turbo1a[$i-1];
            			}if(empty($air1a[$i]))
            			{
            				$air1a[$i]             = $air1a[$i-1];
            			}
            			if(empty($fuel1a[$i]))
            			{
            				$fuel1a[$i]            = $fuel1a[$i-1] ;
            			}
            			if(empty($copPres1a[$i]))
            			{
            				$copPres1a[$i]       = $copPres1a[$i-1] ;
            			}
            			if(empty($rcOcil1a[$i]))
            			{
            				$rcOcil1a[$i]          = $rcOcil1a[$i-1];
            			}
            			if(empty($falOdMr1a[$i]))
            			{
            				$falOdMr1a[$i]         = $falOdMr1a[$i-1];
            			}if(empty($vehSpeed1a[$i]))
            			{
            				$vehSpeed1a[$i]       = $vehSpeed1a[$i-1];
            			}
            			if(empty($coolTemp1a[$i]))
            			{
            				$coolTemp1a[$i]        = $coolTemp1a[$i-1];
            			}
            			if(empty($backPre1a[$i]))
            			{
            				$backPre1a[$i]        = $backPre1a[$i-1];
            			}
          			}
          			else
          			{
			            $date[$i]=$i;
			            $batHel1a[$i]          = $batHel1a[$i-1];
			            $turbo1a[$i]          =  $turbo1a[$i-1];
			            $air1a[$i]             = $air1a[$i-1];
			            $fuel1a[$i]            = $fuel1a[$i-1] ;
			            $copPres1a[$i]       = $copPres1a[$i-1] ;
			            $rcOcil1a[$i]          = $rcOcil1a[$i-1];
			            $falOdMr1a[$i]         = $falOdMr1a[$i-1];
			            $vehSpeed1a[$i]       = $vehSpeed1a[$i-1];
			            $coolTemp1a[$i]        = $coolTemp1a[$i-1];
			            $backPre1a[$i]        = $backPre1a[$i-1];
          			}
        		}
        	}
       	}
       	else
       	{
        	//year cal
          	$count = 12;
         	if($year == $curYear)
         	{
           		$count = date('m');
         	}
         	for($i=1;$i<=$count;$i++)
         	{
    			if($i == 1)
    			{
          			if(array_key_exists("1", $date))
          			{
            			$date[$i] = date("M", mktime(0, 0, 0, $i, 10));
					}
					else
					{
			            $date[$i]       = date("M", mktime(0, 0, 0, $i, 10));
			            $batHel1a[$i]   = '';
			            $turbo1a[$i]    = 0;
			            $air1a[$i]      = 0;
			            $fuel1a[$i]     = 0;
			            $copPres1a[$i]  = 0;
			            $rcOcil1a[$i]   = 0;
			            $falOdMr1a[$i]  = 0;
			            $vehSpeed1a[$i] = 0;
			            $coolTemp1a[$i] = 0;
			            $backPre1a[$i]  = 0;
            		}
        		}
          		elseif($i != 1)
          		{
          			if(array_key_exists($i, $date))
          			{
            			$date[$i] = date("M", mktime(0, 0, 0, $i, 10));
            			if(empty($batHel1a[$i]))
            			{
            				$batHel1a[$i]   = $batHel1a[$i-1];
            			}
            			if(empty($turbo1a[$i]))
            			{
            				$turbo1a[$i]          =  $turbo1a[$i-1];
            			}if(empty($air1a[$i]))
            			{
            				$air1a[$i]             = $air1a[$i-1];
            			}
            			if(empty($fuel1a[$i]))
            			{
            				$fuel1a[$i]            = $fuel1a[$i-1] ;
            			}
            			if(empty($copPres1a[$i]))
            			{
            				$copPres1a[$i]       = $copPres1a[$i-1] ;
            			}
            			if(empty($rcOcil1a[$i]))
            			{
            				$rcOcil1a[$i]          = $rcOcil1a[$i-1];
            			}
            			if(empty($falOdMr1a[$i]))
            			{
            				$falOdMr1a[$i]         = $falOdMr1a[$i-1];
            			}if(empty($vehSpeed1a[$i]))
            			{
            				$vehSpeed1a[$i]       = $vehSpeed1a[$i-1];
            			}
            			if(empty($coolTemp1a[$i]))
            			{
            				$coolTemp1a[$i]        = $coolTemp1a[$i-1];
            			}
            			if(empty($backPre1a[$i]))
            			{
            				$backPre1a[$i]        = $backPre1a[$i-1];
            			}
          			}
          			else
          			{
			            $date[$i]       = date("M", mktime(0, 0, 0, $i, 10));
			            $batHel1a[$i]   = $batHel1a[$i-1];
			            $turbo1a[$i]    =  $turbo1a[$i-1];
			            $air1a[$i]      = $air1a[$i-1];
			            $fuel1a[$i]     = $fuel1a[$i-1] ;
			            $copPres1a[$i]  = $copPres1a[$i-1] ;
			            $rcOcil1a[$i]   = $rcOcil1a[$i-1];
			            $falOdMr1a[$i]  = $falOdMr1a[$i-1];
			            $vehSpeed1a[$i] = $vehSpeed1a[$i-1];
			            $coolTemp1a[$i] = $coolTemp1a[$i-1];
			            $backPre1a[$i]  = $backPre1a[$i-1];
          			}
        		}
        	}// for loop
		}
        // print_r($date);
        // print_r($batHel1a);die;

        if($ptype == 1)
        {
          	$data['values'] = $batHel1a;
          	$data['mark']   = "Battery Health";
        }
        elseif($ptype == 2)
        {
          	$data['values'] = $turbo1a;
           	$data['mark']   = "Turbocharger Health";
        }
        elseif($ptype == 3)
        {
          	$data['values'] = $air1a;
          	$data['mark']   = "Air System";
        }
        elseif($ptype == 4){
          	$data['values'] = $fuel1a;
           	$data['mark']   = "Fuel System";
        }
        elseif($ptype == 5){
          	$data['values'] = $copPres1a;
          	$data['mark']   = "Combustion Pressure";
        }
        elseif($ptype == 6){
          	$data['values'] = $rcOcil1a;
           	$data['mark']   = "Rpm Oscillation Anomaly Analysis";
        }
        elseif($ptype == 7)
        {
          	$data['values'] =$falOdMr1a;
           	$data['mark']="False Odometer Reading Detection";
        }
        elseif($ptype == 8)
        {
          	$data['values'] = $coolTemp1a;
           	$data['mark']   = "Engine Cooling System";
        }
        elseif($ptype == 9)
        {
          	$data['values'] = $backPre1a;
           	$data['mark']   = "Vehicle Speed Sensor Malfunction";
        }
        ksort($date);
        ksort($data['values']);
        // print_r(json_encode(array_values($data['values'])));
        $data['values1'] = json_encode(array_values($data['values']));
        $data['date1']   = json_encode(array_values($date));
        // print_r($date);die;

      	$result = view('diagnostic/healthgraph')->with($data);
      	return $result;
  	}
}
?>