<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newuser;
use Illuminate\Support\Arr;
// use Mail;

class Api_bi extends Controller
{
	public function profile(Request $request)
  	{
 		if(!empty($request->data))
        {	
           	$json     = $request->data;
			$obj      = json_decode($json,true);	
			$deviceID = $obj['user']['deviceid'];
			$devInfo  = Fetch::getdevice($deviceID);
			// print_r($devInfo);die;
			if($deviceID != '')
			{
				if(empty($devInfo))
				{
					$postValD              = array();
					$postValD['device_id'] = $deviceID;
					$postValD['name']      = $deviceID;
					$postValD['descrp']    = $obj['user']['name'];
					$postValD['partner']   = 2;		
					$postValD['status']    = 1;
					$postValD['ad_by']     = 2;
					$postValD['ad_dt']     = date('Y-m-d H:i:s');
					$postValD['devtype']   = 1;
					// $postValD['phn_model'] = $obj['user']['phnName'];
					// $postValD['bt_mac_id'] = $obj['user']['btMacId'];
					$update                = Fetch::insert_device($postValD);
					if($update)
					{
						$devInfo  = Fetch::getdevice($deviceID);

						//Device Addition Complete. Now Add User.
						$prof = $obj['user'];
						// print_r($prof);die;
						$us   = Fetch::getUserByEmail($prof['email']);
						if(count($us) >= 1)
						{
							$arrayData['success']      = FALSE;
							$arrayData['errorCode']    = "1004";
							$arrayData['errorMessage'] = "Email ID already exist!";
						}
						else
						{
							$drvInfo              = array();
							// $drvInfo['device']    = $devInfo[0]->id;
							$drvInfo['partner']   = 2;
							$drvInfo['uname']     = $prof['name'];
							$drvInfo['utype']     = 14;
							$drvInfo['ucategory'] = 4;
							$drvInfo['ulocation'] = "Bangalore";
							$drvInfo['umob']      = $prof['mobile'];
							$drvInfo['uemail']    = $prof['email'];
							$drvInfo['upwd']      = md5($prof['password']);
							$drvInfo['ustatus']   = 0;
							$saveData             = Fetch::save_user($drvInfo);
							if($saveData)
							{
								// Add Activation Code
					            $token1  = Helper::token1(10);
					            $t       = strtotime('+1 year');
					            $afteryr = date('Y-m-d', $t);
					            $data1   = array(
						            'activation_code' => $token1,
						            'mac_id'          => $deviceID,
						            'used_by'         => $prof['email'],
						            'status'          => 1,
						            'expdt'           => $afteryr,
						            'email'           => $prof['email'],
						            'ad_by'           => 1,
						        );
					            $activation_id        = Common::insertGetId('activation_code',$data1);
					            
					            // Add Vehicle
								$postValVeh['device'] = $devInfo[0]->id;
								$update               = Fetch::insert_vehicle($postValVeh);
								if($update)
								{
									$arrayData['success'] = TRUE;
									$arrayData['message'] = "Registration done successfully!";
								}
								else

								{
									$arrayData['success']      = FALSE;
									$arrayData['errorCode']    = "1003";
									$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
								}
							}
							else
							{
								$arrayData['success']      = FALSE;
								$arrayData['errorCode']    = "1003";
								$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
							}
						}
					}
					else
					{
						$arrayData['success']      = FALSE;
						$arrayData['errorCode']    = "1003";
						$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
					}
				}
				else
				{
					// The device is already registered. Check if user is assigned to the device.
					$prof     = $obj['user'];
					$drvInfo1 = Fetch::getdriverDetails($deviceID);
					if(empty($drvInfo1))
					{
						$us   = Fetch::getUserByEmail($prof['email']);
						if(count($us) >= 1)
						{
							$arrayData['success']      = FALSE;
							$arrayData['errorCode']    = "1004";
							$arrayData['errorMessage'] = "Email ID already exist!";
						}
						else
						{
							$drvInfo              = array();
							$drvInfo['device']    = $devInfo[0]->id;
							$drvInfo['partner']   = 2;
							$drvInfo['uname']     = $prof['name'];
							$drvInfo['utype']     = 14;
							$drvInfo['ucategory'] = 4;
							$drvInfo['ulocation'] = "Bangalore";
							$drvInfo['umob']      = $prof['mobile'];
							$drvInfo['uemail']    = $prof['email'];
							$drvInfo['upwd']      = md5($prof['password']);
							$drvInfo['ustatus']   = 0;
							$saveData             = Fetch::save_user($drvInfo);
							if($saveData)
							{
								// Add Activation Code
					            $token1  = Helper::token1(10);
					            $t       = strtotime('+1 year');
					            $afteryr = date('Y-m-d', $t);
					            $data1   = array(
						            'activation_code' => $token1,
						            'mac_id'          => $deviceID,
						            'used_by'         => $prof['email'],
						            'status'          => 1,
						            'expdt'           => $afteryr,
						            'email'           => $prof['email'],
						            'ad_by'           => 1,
						        );
					            $activation_id        = Common::insertGetId('activation_code',$data1);
					            
					            // Add Vehicle
								$postValVeh['device'] = $devInfo[0]->id;
								$update               = Fetch::insert_vehicle($postValVeh);
								if($update)
								{
									$arrayData['success'] = TRUE;
									$arrayData['message'] = "Registration done successfully!";
								}
								else

								{
									$arrayData['success']      = FALSE;
									$arrayData['errorCode']    = "1003";
									$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
								}
							}
							else
							{
								$arrayData['success']      = FALSE;
								$arrayData['errorCode']    = "1003";
								$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
							}
						}
					}
					else
					{
						$arrayData['success']      = FALSE;
						$arrayData['errorCode']    = "1002";
						$arrayData['errorMessage'] = "Device already exists!";
					}
				}
      	   	}
      	   	else
	        {
	        	$arrayData['success']      = FALSE;
	        	$arrayData['errorCode']    = "1001";
	        	$arrayData['errorMessage'] = "Invalid Input!";
	        }
      	}
      	else
      	{
      		$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1001";
      		$arrayData['errorMessage'] = "Invalid Input!";
      	}
    	$resultVal = json_encode($arrayData);
    	echo $resultVal;
  	}

	public function check_login(Request $request)
    {
 		if(!empty($request->data))
        {
     		$obj      = json_decode($request->data, TRUE);
     		$username = $obj['u'];
			$password = $obj['p'];
			// $phnName  = $obj['phnName'];
			// $btMacId  = $obj['btMacId'];
			// $devID    = $obj['d'];
			$password = md5($password);//ENCRYPT_DECRYPT($password,$enc);
			// print_r($password);die;
			$utype    = array('0'=>'','1'=>'Super Admin','2'=>'Admin','3'=>'Partner','4'=>'Driver');

			if($res = Fetch::getUserByEmailPass($username,$password))
			{
				$devID = $res[0]->device;
				$data  = Fetch::getDeviceById($devID);
				// print_r($data);die;
				// if ($phnName != "")
				// {
				// 	if($btMacId != "")
				// 	{
						
				// 	}
				// }



				// $actStatus = Fetch::getActivationCode($username);
				$deviceid             = $data[0]->device_id;
				$result['success']    = TRUE;
				$result['userTypeId'] = $res[0]->utype;
				$result['userType']   = $utype[$res[0]->ucategory];
				$result['deviceID']   = $deviceid;
				$result['email']      = $username;
				// $result['actStatus']  = $actStatus[0]->status;
				// $result['actCode']    = $actStatus[0]->code;
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Username or Password!";
			}
     	}
     	else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
    }

	public function forget_password(Request $request)
  	{
     	if(!empty($request->data))
     	{    
        	$data  = json_decode($request->data, TRUE);
        	$email = $data['email'];
           	$info  = Fetch::getUserByEmail($email);

       		if(empty($info))
       		{
	           	$msg                       = "\"$email\" is not a Registered E-Mail Id !";
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1003";
	            $arrayData['errorMessage'] = $msg;
	    	}
	       	else
	       	{
        		$data       = new \stdClass();
	            $data->name = $info[0]->uname;
	            $name       = $info[0]->uname;
	            $data->uid  = $info[0]->uid;
	            $uid        = $info[0]->uid;
	            //print_r( $uid);die;
	           $msg = '
	           <html>
	              <head>
	                <title>Reset Password Request</title>
	              </head>
	              <body>
	                <table id="m_9144868195613793263m_-947074545920247478templateContainer" style="border:1px solid #dddddd;background-color:#ffffff" width="600" cellspacing="0" cellpadding="0" border="0">
	                 <tbody>
	                    <tr>
	                       <td valign="top" align="center">
	                          <table id="m_9144868195613793263m_-947074545920247478templateHeader" width="600" cellspacing="10" cellpadding="10" border="0">
	                             <tbody>
	                                <tr style="background: #eee;">
	                                   <td>
	                                      <table align="left">
	                                         <tbody>
	                                            <tr>
	                                               <td>
	                                                  <a href="#m_9144868195613793263_m_-947074545920247478_">
	                                                  <img src="http://www.enginecal.com/wp-content/uploads/2016/08/Logo_Final-174x66.png" class="CToWUd">
	                                                  </a>
	                                               </td>
	                                            </tr>
	                                         </tbody>
	                                      </table>
	                                   </td>
	                                   <td>
	                                      <table align="right">
	                                         <tbody>
	                                            <tr>
	                                              
	                                            </tr>
	                                         </tbody>
	                                      </table>
	                                   </td>
	                                </tr>
	                             </tbody>
	                          </table>
	                       </td>
	                    </tr>
	                    <tr>
	                       <td valign="top" align="center">
	                          <table id="m_9144868195613793263m_-947074545920247478templateBody" width="600" cellspacing="0" cellpadding="10" border="0">
	                             <tbody>
	                                <tr>
	                                   <td class="m_9144868195613793263m_-947074545920247478bodyContent" valign="top">
	                                      <table width="100%" cellspacing="0" cellpadding="10" border="0">
	                                         <tbody>
	                                            <tr>
	                                               <td valign="top">
	                                                  <div>
	                                                     <span class="m_9144868195613793263m_-947074545920247478h3" style="color:#ec1c23;font-weight:bold;font-size:24px">Hi '.$name.',</span>
	                                                     <br>
	                                                     <p style="ext-align: justify;
	                                                            font-style: normal;
	                                                            font-weight: 500;">
	                                                        Thank you, Your password change link is:
	                                                     </p>
	                                                     <br>
	                                                        <a href="https://eva.enginecal.com/enginecal-reset-password/?qugtyhfr='.$uid.'" >Click Here to reset.</a>
	                                                     <br>
	                                                  </div>
	                                               </td>
	                                            </tr>
	                                         </tbody>
	                                      </table>
	                                   </td>
	                                </tr>
	                             </tbody>
	                          </table>
	                       </td>
	                    </tr>
	                    <tr>
	                       <td valign="top" bgcolor="#E5E5E5" align="center">&nbsp;</td>
	                    </tr>
	                    <tr>
	                       <td valign="top" bgcolor="#E5E5E5" align="center">
	                          <table width="600" cellspacing="0" cellpadding="3" border="0">
	                             <tbody>
	                                <tr>
	                                </tr>
	                                <tr>
	                                </tr>
	                             </tbody>
	                          </table>
	                       </td>
	                    </tr>
	                    <tr>
	                       <td valign="top" bgcolor="#E5E5E5" align="center">&nbsp;</td>
	                    </tr>
	                    <tr>
	                       <td valign="top" bgcolor="#fff" align="center">
	                          <span class="HOEnZb"><font color="#888888">
	                          </font></span><span class="HOEnZb"><font color="#888888">
	                          </font></span>
	                          <table width="600" cellspacing="10" cellpadding="10" border="0">
	                             <tbody>
	                                <tr>
	                                   <td>
	                                      <span class="HOEnZb"><font color="#888888">
	                                      </font></span><span class="HOEnZb"><font color="#888888">
	                                      </font></span>
	                                      <table align="center">
	                                         <tbody>
	                                            <tr>
	                                               <td>
	                                                 <p style="font-size: 14px;font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif;margin-top: 7px;">
	                                                     Copyright 2020 EngineCAL
	                                                     All right reserved
	                                                  </p>
	                                                  <span class="HOEnZb"><font color="#888888">
	                                                  </font></span>
	                                               </td>
	                                            </tr>
	                                         </tbody>
	                                      </table>
	                                      <span class="HOEnZb"><font color="#888888">
	                                      </font></span>
	                                   </td>
	                                </tr>
	                             </tbody>
	                          </table>
	                          <span class="HOEnZb"><font color="#888888">
	                          </font></span>
	                       </td>
	                    </tr>
	                </tbody>
	            	</table>
	            	</body>
	            </html>
	              ';
	            $from     = 'support@enginecal.com';
	            $headers  = 'MIME-Version: 1.0' . "\r\n";
	            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	             
	            // Create email headers
	            $headers .= 'From: '.$from."\r\n".
	                		'Reply-To: '.$from."\r\n" .
	                		'X-Mailer: PHP/' . phpversion();
	            
	            if(mail($email,"Reset Password Request",$msg,$headers))
	            {
		            $arrayData['success'] = TRUE;
		            $arrayData['message'] = "Password sent successfully!";
	          	}
	          	else
	          	{
		            $arrayData['success']      = FALSE;
		            $arrayData['errorCode']    = "1002";
		            $arrayData['errorMessage'] = "Mail sending unsuccessful!";
	        	}
	    	}
    	}
    	else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
  	}

    public function valcode(Request $request)
	{
        if(!empty($request->data))
        {
            $data  = json_decode($request->data, TRUE);
            $acode = $data['acode'];
            $mac   = $data['mac'];
            $uby   = $data['uby'];

            $devAct = Fetch::isActivationCodeValid($acode);
            if(!empty($devAct))
            {
            	if($devAct[0]->status == 1)
                {    
            	    $saveData = Fetch::updateActCode($devAct[0]->id,$mac,$uby,'2');
                    if($saveData)
                    {
                    	$result['success'] = TRUE;
                    	$result['message'] = "Activation code updated successfully!";
                    	$result['expDate'] = date('d-m-Y',strtotime($devAct[0]->expdt));
                    }
                }
                elseif($devAct[0]->status == 2)
                {
                    if($devAct[0]->mac_id == $mac)
                    {
                    	if($devAct[0]->used_by == $uby)
                    	{
                    		$result['success']   = TRUE;
	                        $result['message']   = "Activation code restored successfully!";
	                        $result['strtDate']  = date('d-m-Y',strtotime($devAct[0]->ad_dt));
	                        $result['expDate']   = date('d-m-Y',strtotime($devAct[0]->expdt));
	                        $result['expStatus'] = (strtotime($devAct[0]->expdt) < strtotime(date('d-m-Y'))) ? TRUE : FALSE;
                    	}
                    	else
                    	{
                    		$result['success']      = FALSE;
	                     	$result['errorCode']    = "1004";
	                      	$result['errorMessage'] = "Invalid User Details!";
                    	}
                    }
                    else
                    {
                      	$result['success']      = FALSE;
                     	$result['errorCode']    = "1003";
                      	$result['errorMessage'] = "Invalid Device Details!";
                    }
		        }
		        else
		        {
		        	$result['success']      = FALSE;
		            $result['errorCode']    = "1002";
		            $result['errorMessage'] = "Invalid Activation Code!";
		        } 
            }
            else
            {
            	// $devUser    = Fetch::getActivationAll($mac);//$db->select("select * from activation_code where mac_id = '$mac'");
            	// print_r($devUser);die;
				// $devActUser = $devUser[0];
				// if(!empty($devActUser))
				// {
				// 	if($devActUser->used_by == $uby)
				// 	{
				// 		$result['success']   = TRUE;
				// 		$result['message']   = "Activation code restored successfully!";
				// 		$result['expDate']   = date('d-m-Y',strtotime($devActUser->expdt));
				// 		$result['expStatus'] = (strtotime($devActUser->expdt) < strtotime(date('d-m-Y'))) ? TRUE : FALSE;
				// 	}
				// 	else
				// 	{
				// 		$result['success']      = FALSE;
				// 		$result['errorCode']    = "1004";
				// 		$result['errorMessage'] = "Invalid User Details!";
				// 	}
				// }
				// else
				// {
				// 	$result['success']      = FALSE;
				// 	$result['errorCode']    = "1003";
				// 	$result['errorMessage'] = "Invalid Device Details!";
				// }
				$result['success']      = FALSE;
	            $result['errorCode']    = "1002";
	            $result['errorMessage'] = "Invalid Activation Code!";
           	}
        }
        else
        {
            $result['success']      = FALSE;
            $result['errorCode']    = "1001";      
            $result['errorMessage'] = "Invalid Input!";   
        } 
        echo json_encode($result);  
    }

    public function drivescore(Request $request)
  	{
  		if(!empty($request->data))
	    {
	        $data     = json_decode($request->data, TRUE);
	        $deviceID = $data['devID'];
	        $devInfo  = Fetch::getdevice($deviceID);
	        
	        if(!empty($devInfo))
	        {
	    		$arrayData['success']  = TRUE;
	        	$datLastScore          = Fetch::getDriveStatHlthScr($deviceID);
	        	$drvScore              = $datLastScore[0]->hlt_score;
	        	$arrayData['drvScore'] = round($drvScore);
	        }
	        else
	        {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Invalid Device ID!";
	        }
		}
	    else
	    {
	        $arrayData['success']      = FALSE;
	        $arrayData['errorCode']    = "1001";
	        $arrayData['errorMessage'] = "Invalid Input!";
	    }
	    echo json_encode($arrayData);
  	}

    public function vehicle_model(Request $request)
	{
        if(!empty($request->data))
        {
            $data = json_decode($request->data, TRUE);
            $mfd  = $data['mfd'];
            $res  = Fetch::getVehicleSetting();
            $mfdList    = explode(';',$res->manufacturer);
            $engCapList = explode(';',$res->engine_capacity);
            if(in_array($mfd, $mfdList))
            {
            	$arrayData['success'] = TRUE;
            	$keyMfd = array_search($mfd, $mfdList);
            	$resM   = Fetch::getVehicleInfoDataById($keyMfd);
            	$fuel   = array('','Petrol','Diesel');
                if(count($resM) >= 1)
	            {
	                $arrayData['model'] = explode(';',$resM[0]->model);
	            }
            }
            else
            {
                $arrayData['success']      = FALSE;
                $arrayData['errorCode']    = "1002";
                $arrayData['errorMessage'] = "Manufacturer is not in the list!";
            }
        }//if(!empty($request->data))
        else
        {
           $arrayData['success']      = FALSE;
           $arrayData['errorCode']    = "1001";
           $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }

	public function vehicle_spec(Request $request)
	{
		if(!empty($request->data))
        {
        	$data = json_decode($request->data, TRUE);
        	$mfd  = $data['mfd'];
        	$mod  = $data['model'];
        	$res  = Fetch::getVehicleSetting();
        	$mfdList    = explode(';',$res->manufacturer);
        	$engCapList = explode(';',$res->engine_capacity);
        	$fuel       = array('','Petrol','Diesel');

         	if(in_array($mfd, $mfdList))
          	{
	            $arrayData['success'] = TRUE;
	            $keyMfd  = array_search($mfd, $mfdList);
	            $resM    = Fetch::getVehicleInfoDataById($keyMfd);
	            $varList = array();
	            $modList = array();
	            $fuel    = array('','Petrol','Diesel');
	            if(count($resM) >= 1)
	            {
	            	$varList = explode(';',$resM[0]->var);
	            	$modList = explode(';',$resM[0]->model);
	            }
	            if($mod != "")
	            {
	              	if(in_array($mod, $modList))
	              	{
	                	$keyMode = array_search($mod, $modList);
	              	}
	              	else
	             	{
	                	$modNew++;
	               		$keyMode           = count($modList);
	               		$modList[$keyMode] = $mod;
	              	}
	            }

	            $dat = Fetch::getVehicleSpecDataByMfdModel($keyMfd,$keyMode);
	            $j   = 0;
	            if(count($dat) >= 1)
	            {
	              	foreach($dat as $data)
	              	{
	                	$arrayData['data'][$j]['fuelType']       = $fuel[$data->fuel];
	                	$arrayData['data'][$j]['varients']       = $varList[$data->varient];
	                	$arrayData['data'][$j]['engineCapacity'] = $engCapList[$data->engcap];
	            	    $arrayData['data'][$j]['bodyType']       = $data->btype;
	            	    $j++;
	            	}
	            }
	            else
	            {
	            	$arrayData['success']      = FALSE;
	            	$arrayData['errorCode']    = "1003";
	            	$arrayData['errorMessage'] = "Model is not in the list!";
	            }
        	}
	        else
	        {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Manufacturer is not in the list!";
	        }
        }
        else
        {
        	$arrayData['success']      = FALSE;
        	$arrayData['errorCode']    = "1001";
        	$arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }

	public function vehicle(Request $request)
	{
     	if(!empty($request->data))
        {
	     	$data     = json_decode($request->data, TRUE);
			$deviceID = $data['veh_basic']['deviceid'];
			$devInfo  = Fetch::getdevice($deviceID);
			// print_r($devInfo);die;
			$errorMF  = "";
			if(!empty($devInfo))
			{
			    $did      = $devInfo[0]->id;
			    $prof     = $data['veh_basic'];
			    $drvInfo  = Fetch::getVehicleById($did);
			    $settInfo = Fetch::getVehicleSetting();
			    // print_r($devInfo[0]->device_id);die;
			    $mfd = explode(';',$settInfo->manufacturer);
			    $mfd = array_map('strtolower',$mfd);
			    $cap = explode(';',$settInfo->engine_capacity);
			    $cap = array_map('strtolower',$cap);
			    $drvInfo = ($drvInfo != "") ? $drvInfo : array();
			    $drvInfo = array();
			   
			    $drvInfo['device']   = $devInfo[0]->id;
			    $drvInfo['reg_no']   = $prof['veh_registration'];
			    $drvInfo['manufact'] = "";
			    $myMfd               = strtolower(trim($prof['veh_manufacturer']));
			    // print_r($mfd);
			    // print_r($myMfd);die;
			    //Checek if valid manufacturer
			    if(in_array($myMfd,$mfd))
			    {
				    $indexM              = array_search($myMfd,$mfd);  
			        $drvInfo['manufact'] = $indexM;

			        $modelList = array();
			    	$varlList  = array();
			        $resM      = Fetch::getVehicleInfoDataById($indexM);
				    if(count($resM) >= 1)
				    {
				        //$arrayData['varients'] = explode(';',$resM[0]['var']);
				        $modelList = explode(';',$resM[0]->model);
				        $varlList  = explode(';',$resM[0]->var);
				    }

				    //Checek if valid model
				    if(in_array($prof['veh_model'],$modelList))
				    {
				        $indexMod           = array_search($prof['veh_model'],$modelList);  
				        $drvInfo['modelid'] = $indexMod;
				        $drvInfo['model']   = $indexMod;

				        //Checek if valid varient
				        if(in_array($prof['veh_varient'],$varlList))
				        {
				            $indexVar             = array_search($prof['veh_varient'],$varlList); 
				            $drvInfo['varientid'] = $indexVar;
				            $drvInfo['varient']   = $indexVar;

				            //Check for valid engine capacity
				            $myCap                = strtolower(trim($prof['engine_capacity']));
						    if(in_array($myCap,$cap))
						    {
						        $indexM             = array_search($myCap,$cap);  
						        $drvInfo['eng_cap'] = $indexM;

						        $drvInfo['fuel_tpe']        = (trim($prof['fuel_type']) == "Petrol") ? '1' : '2';
						        $drvInfo['manufact_yr']     = $prof['mfg_year'];
						        $drvInfo['travel_purchase'] = $prof['odo'];
						        if(array_key_exists('veh_transmission', $prof))
						        {
						        	$trans_type = $prof['veh_transmission'];
									if($trans_type == 2)
									{
										$drvInfo['trans_type'] = "Automatic";
									}
									else
									{
										$drvInfo['trans_type'] = "Manual";
									}
						        }
						        else
						        {
						        	$drvInfo['trans_type'] = "Manual";
						        }
						        $drvInfo['purchase_dt']     = date('Y-m-d');
						        $drvInfo['last_serv_dt']    = date('Y-m-d');
						        $drvInfo['oil_dt']          = date('Y-m-d');
						        //to add last service kms in multiples of 10000
						        $a = strlen($prof['odo']);
						        if($a >= 5)
						        {
						        	$val = round($prof['odo'],-($a - 1));
						        }
						        else
						        {
						            $val = 0;
						        }
						        $drvInfo['travel_service'] = $val;
						        $drvInfo['oil_dist']       = $val;
						    }
						    else
						    {
					            $errorMF .= $prof['engine_capacity'] . " engine capacity is not available";
					        }
				        }
				        else
				        {
				            $errorMF = $prof['veh_varient'] . " varient is not available";
				        }
				    }
			        else
			        {
			        	$errorMF = $prof['veh_model'] . " model is not available";
			        }
			    }
			    else
			    {
			        $errorMF = $prof['veh_manufacturer'] . " manufacturer is not available";
			    }
			    
	            if($errorMF != "")
	            {
				    $arrayData['success']      = FALSE;
				    $arrayData['errorCode']    = "1003";
				    $arrayData['errorMessage'] = "Input data Incorrect. $errorMF!";
			    }
	    		else
	    		{
	    			$setFields = array('device');
					$setVal    = array($did);
					$table     = "vehicles";
	    			$saveData  = Fetch::updateDataMultiple($table,$setFields,$setVal,$drvInfo);
			        // if($saveData)
			        // {	
			        	$setSpecs = Fetch::setvehSpec($drvInfo['manufact'],$drvInfo['model'],$drvInfo['fuel_tpe'],$drvInfo['varient'],$drvInfo['eng_cap'],$drvInfo['manufact_yr'],$did);
			        	if($setSpecs)
			        	{
			        		$arrayData['success'] = TRUE;
				        	$arrayData['message'] = "Data updated successfully!";
			        	}
			        	else
				        {
					        $arrayData['success']      = FALSE;
					        $arrayData['errorCode']    = "1004";
					        $arrayData['errorMessage'] = "Error in adding data. Try again Later!";
				        }
			        // }
			        // else
			        // {
				       //  $arrayData['success']      = FALSE;
				       //  $arrayData['errorCode']    = "1003";
				       //  $arrayData['errorMessage'] = "Error in adding data. Data already available!";
			        // }
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
		    $arrayData['success']      = FALSE;
		    $arrayData['errorCode']    = "1001";
		    $arrayData['errorMessage'] = "Invalid Input!";
		}
        echo json_encode($arrayData);
    }

	public function bluetoothkey(Request $request)
	{
		if(!empty($request->data))
        {    
            $obj      = json_decode($request->data, TRUE);
			$deviceID = trim($obj['macID']);
			$apiKey   = $obj['apiKey'];
			// $vKey  = $obj['eKey'];
			$datAct   = Fetch::randKeyVerify($apiKey);
			// print_r($datAct[0]->count);die;
			if($datAct[0]->count)
			{
				if($deviceID != "")
				{
					$arrayData['success'] = TRUE;
					
					// $tm = strtotime(date('Y-m-d'));
					// print_r($tm);die;
					// $tm = $tm*$deviceID;
					// $tm = "$tm";
					// $len = strlen($tm);
					// $len -= 8;
					// $devKey = substr($tm, $len, 4);
					//echo "devKey = $devKey \n";
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid API Key!";
			}
        }
        else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($arrayData);	
		echo $resultVal;
	}

	public function getcalvalues(Request $request)
 	{
 		if(!empty($request->data))
        {
         	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			
			// Get Device Data
			$devInfo = Fetch::getdevice($deviceID);

			// Get Vehicle details
			// $vehi  = Fetch::getVehicleById($did);
			// $vType = $vehi->fuel_tpe;
			$encryptKey = rand(25, 300);
			if(!empty($devInfo))
			{
				$list = array("Eng_eff_C","Vol_eff_C","Eng_Disp_C","Eff_RPM_Limit1_C","Eff_RPM_Limit2_C","Eff_RPM_Limit3_C","Eff_RPM_Limit4_C","Eff_RPM_Limit5_C","IC_Status_C","TC_Status_C","Injection_Type_C","VVT_Status_C","Fact_Fuel_Run_C","Fact_Fuel_Idle_C","MAP_Valid_C","MAF_Valid_C","VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C","VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C","VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C","VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C","VolEff_Eload_31_40_RPMLimit2_C","VolEff_Eload_31_40_RPMLimit3_C","VolEff_Eload_31_40_RPMLimit4_C","VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C","VolEff_Eload_41_50_RPMLimit3_C","VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C","VolEff_Eload_51_60_RPMLimit2_C","VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C","VolEff_Eload_61_80_RPMLimit1_C","VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C","VolEff_Eload_61_80_RPMLimit5_C","VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C","VolEff_Eload_81_90_RPMLimit4_C","VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C","VolEff_Eload_91_94_RPMLimit3_C","VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C","VolEff_Eload_95_98_RPMLimit2_C","VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C","VolEff_Eload_99_100_RPMLimit1_C","VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C","VolEff_Eload_99_100_RPMLimit5_C","Eqratio_Eload_1_10_RPMLimit1_C","Eqratio_Eload_1_10_RPMLimit2_C","Eqratio_Eload_1_10_RPMLimit3_C","Eqratio_Eload_1_10_RPMLimit4_C","Eqratio_Eload_1_10_RPMLimit5_C","Eqratio_Eload_11_20_RPMLimit1_C","Eqratio_Eload_11_20_RPMLimit2_C","Eqratio_Eload_11_20_RPMLimit3_C","Eqratio_Eload_11_20_RPMLimit4_C","Eqratio_Eload_11_20_RPMLimit5_C","Eqratio_Eload_21_30_RPMLimit1_C","Eqratio_Eload_21_30_RPMLimit2_C","Eqratio_Eload_21_30_RPMLimit3_C","Eqratio_Eload_21_30_RPMLimit4_C","Eqratio_Eload_21_30_RPMLimit5_C","Eqratio_Eload_31_40_RPMLimit1_C","Eqratio_Eload_31_40_RPMLimit2_C","Eqratio_Eload_31_40_RPMLimit3_C","Eqratio_Eload_31_40_RPMLimit4_C","Eqratio_Eload_31_40_RPMLimit5_C","Eqratio_Eload_41_50_RPMLimit1_C","Eqratio_Eload_41_50_RPMLimit2_C","Eqratio_Eload_41_50_RPMLimit3_C","Eqratio_Eload_41_50_RPMLimit4_C","Eqratio_Eload_41_50_RPMLimit5_C","Eqratio_Eload_51_60_RPMLimit1_C","Eqratio_Eload_51_60_RPMLimit2_C","Eqratio_Eload_51_60_RPMLimit3_C","Eqratio_Eload_51_60_RPMLimit4_C","Eqratio_Eload_51_60_RPMLimit5_C","Eqratio_Eload_61_80_RPMLimit1_C","Eqratio_Eload_61_80_RPMLimit2_C","Eqratio_Eload_61_80_RPMLimit3_C","Eqratio_Eload_61_80_RPMLimit4_C","Eqratio_Eload_61_80_RPMLimit5_C","Eqratio_Eload_81_90_RPMLimit1_C","Eqratio_Eload_81_90_RPMLimit2_C","Eqratio_Eload_81_90_RPMLimit3_C","Eqratio_Eload_81_90_RPMLimit4_C","Eqratio_Eload_81_90_RPMLimit5_C","Eqratio_Eload_91_94_RPMLimit1_C","Eqratio_Eload_91_94_RPMLimit2_C","Eqratio_Eload_91_94_RPMLimit3_C","Eqratio_Eload_91_94_RPMLimit4_C","Eqratio_Eload_91_94_RPMLimit5_C","Eqratio_Eload_95_98_RPMLimit1_C","Eqratio_Eload_95_98_RPMLimit2_C","Eqratio_Eload_95_98_RPMLimit3_C","Eqratio_Eload_95_98_RPMLimit4_C","Eqratio_Eload_95_98_RPMLimit5_C","Eqratio_Eload_99_100_RPMLimit1_C","Eqratio_Eload_99_100_RPMLimit2_C","Eqratio_Eload_99_100_RPMLimit3_C","Eqratio_Eload_99_100_RPMLimit4_C","Eqratio_Eload_99_100_RPMLimit5_C","Ovr_Run_EqRatio_C","Ovr_Run_Cold_EqRatio_C","Ovr_Run_Hot_EqRatio_C","Ovr_Run_VolEff_C","Ovr_Run_Cold_VolEff_C","Ovr_Run_Hot_VolEff_C","Ovr_Run_FuelFact_C","Ovr_Run_FuelFact_Eload_C","Fact_Fuel_Run_ELoad_0_20_C","Fact_Fuel_Run_ELoad_20_40_C","Fact_Fuel_Run_ELoad_40_60_C","Fact_Fuel_Run_ELoad_60_80_C","Fact_Fuel_Run_ELoad_80_100_C","Fact_Fuel_Idle_ELoad1_C","Fact_Fuel_Idle_ELoad2_C","Baro_Press_City_C","Base_OvrHt_Cool_Lmt_C","Base_MAP_Abs_Lmt_C","Base_MAP_Dif_Lmt_C","Base_MAP_Air_Lmt_C","Base_CRP_Lmt_C","Railp_Fact_C","MAP_Type_C","RPM_Idle_Cold_C","MAP_Idle_Cold_C","RPM_Idle_Warm_C","MAP_Idle_Warm_C","ELoad_Idle_Warm_C","RPM_Idle_Ld1_C","MAP_Idle_Ld1_C","ELoad_Idle_Ld1_C","RPM_Idle_Ld2_C","MAP_Idle_Ld2_C","ELoad_Idle_Ld2_C","Thr_DefPos_C","Thr_MaxPos_C","Thr_IdlePos_C","AccPed_DefPos_C","AccPed_MaxPos_C","Eload_Def_C","Eload_Max_C","EGR_Cmd_WrmIdle_C","Trq_EloadOffset_PkLmt_C","MAP_RPMOffset_PkLmt_C","MAP_Air_RPMOffset_PkLmt_C","VehSpeed_RPMLmt1_C","VehSpeed_RPMLmt2_C","Ovr_Run_ELoad_C","Dist_AC_Warn_C","Fuelling_RPM_C","CoolTemp_Fuel1_C","EngLoad_Fuel_Low1_C","EngLoad_Fuel_High1_C","CoolTemp_Fuel2_C","EngLoad_Fuel_Low2_C","EngLoad_Fuel_High2_C","CoolTemp_Fuel3_C","EngLoad_Fuel_Low31_C","EngLoad_Fuel_High31_C","EngLoad_Fuel_Low32_C","EngLoad_Fuel_High32_C","EngLoad_Fuel_Low33_C","EngLoad_Fuel_High33_C","EngLoad_Fuel_Low41_C","EngLoad_Fuel_High41_C","EngLoad_Fuel_Low42_C","EngLoad_Fuel_High42_C","EngLoad_Fuel_Low43_C","EngLoad_Fuel_High43_C","VehSpeed_Fuel4_C","Turbotimer_Coolt1_C","Turbotimer_RPM1_C","Turbotimer_VehSpeed1_C","Turbotimer_Cool2_C","Turbotimer_RPM2_C","Turbotimer_VehSpeed2_C","Coasting_RPM_C","Coasting_VehSpd1_C","Coasting_VehSpd2_C","Speedbump_RPM_factor_C","Speedbump_Load_C","TakeOff_Load_C","TakeOff_Load_UL_C","TakeOff_Load_LL_C","Take_Off_RPM_factor_C","GearM_Dwn3Gr_VehSpd1_C","GearM_Dwn3Gr_VehSpd2_C","GearM_Dwn3Gr_LOAD_C","GearM_Dwn3Gr_RPM1_C","GearM_Dwn3Gr_RPM2_C","Debounce_3DOWN_C","GearM_Dwn4Gr_VehSpd1_C","GearM_Dwn4Gr_VehSpd2_C","GearM_Dwn4Gr_LOAD_C","GearM_Dwn4Gr_RPM1_C","GearM_Dwn4Gr_RPM2_C","Debounce_4DOWN_C","GearM_Up2Gr_VehSpd1_C","GearM_Up2Gr_VehSpd2_C","GearM_Up2Gr_RPM_C","GearM_Up2Gr_ELoad1_C","GearM_Up2Gr_ELoad2_C","Debounce_2UP_C","GearM_Up3Gr_VehSpd1_C","GearM_Up3Gr_VehSpd2_C","GearM_Up3Gr_RPM_C","GearM_Up3Gr_ELoad1_C","GearM_Up3Gr_ELoad2_C","Debounce_3UP_C","Cold_drive_RPM_C","Time_Upld_Frq_C","Cool_Lmt_C","Max_Torque_C","MAP_Abs_PkLmt3_C","MAP_Dif_PkLmt3_C","B_MAP_Run_Load_C","B_MAP_Run_DEBOUNCE_C","B_MAP_Run2_Load_C","B_MAP_Run2_DEBOUNCE_C","B_MAP_Run2_PFact_C","B_MAP_Idle_PFact_C","B_MAP_Idle_DEBOUNCE_C","PID_Query_Pattern_Type_C","Veh_Spd_Std_Trq_C","Accl_Val_C","Accl_Eload_C","Decl_Val_C","Drv_Score_Fuelling_Count_Lmt_C","gr_1_low_C","gr_1_high_C","gr_2_low_C","gr_2_high_C","gr_3_low_C","gr_3_high_C","gr_4_low_C","gr_4_high_C","gr_5_low_C","gr_5_high_C","gr_6_low_C","gr_6_high_C","gear_Fact_C","gear_Confidence_C","volt_low_C","Hlf_Clutch_Thr_C","Veh_Spd_Avl_C","Peak_Rpm_Eol_C","Cruise_6Rpm_Eol_C","Cruise_5Rpm_Eol_C","Cruise_4Rpm_Eol_C","Cruise_3Rpm_Eol_C","Cruise_2Rpm_Eol_C","Cruise_1Rpm_Eol_C","Cruise_Rpm1_Eol_C","Cruise_Rpm2_Eol_C","Cruise_Rpm3_Eol_C","Cruise_Rpm4_Eol_C","Cruise_Rpm5_Eol_C","Cruise_Rpm6_Eol_C","Cruise_Spd_Eol_C","DPF_Avl_C","EngEff_Eload_1_10_RPMLimit1_C","EngEff_Eload_1_10_RPMLimit2_C","EngEff_Eload_1_10_RPMLimit3_C","EngEff_Eload_1_10_RPMLimit4_C","EngEff_Eload_1_10_RPMLimit5_C","EngEff_Eload_11_20_RPMLimit1_C","EngEff_Eload_11_20_RPMLimit2_C","EngEff_Eload_11_20_RPMLimit3_C","EngEff_Eload_11_20_RPMLimit4_C","EngEff_Eload_11_20_RPMLimit5_C","EngEff_Eload_21_30_RPMLimit1_C","EngEff_Eload_21_30_RPMLimit2_C","EngEff_Eload_21_30_RPMLimit3_C","EngEff_Eload_21_30_RPMLimit4_C","EngEff_Eload_21_30_RPMLimit5_C","EngEff_Eload_31_40_RPMLimit1_C","EngEff_Eload_31_40_RPMLimit2_C","EngEff_Eload_31_40_RPMLimit3_C","EngEff_Eload_31_40_RPMLimit4_C","EngEff_Eload_31_40_RPMLimit5_C","EngEff_Eload_41_50_RPMLimit1_C","EngEff_Eload_41_50_RPMLimit2_C","EngEff_Eload_41_50_RPMLimit3_C","EngEff_Eload_41_50_RPMLimit4_C","EngEff_Eload_41_50_RPMLimit5_C","EngEff_Eload_51_60_RPMLimit1_C","EngEff_Eload_51_60_RPMLimit2_C","EngEff_Eload_51_60_RPMLimit3_C","EngEff_Eload_51_60_RPMLimit4_C","EngEff_Eload_51_60_RPMLimit5_C","EngEff_Eload_61_80_RPMLimit1_C","EngEff_Eload_61_80_RPMLimit2_C","EngEff_Eload_61_80_RPMLimit3_C","EngEff_Eload_61_80_RPMLimit4_C","EngEff_Eload_61_80_RPMLimit5_C","EngEff_Eload_81_90_RPMLimit1_C","EngEff_Eload_81_90_RPMLimit2_C","EngEff_Eload_81_90_RPMLimit3_C","EngEff_Eload_81_90_RPMLimit4_C","EngEff_Eload_81_90_RPMLimit5_C","EngEff_Eload_91_94_RPMLimit1_C","EngEff_Eload_91_94_RPMLimit2_C","EngEff_Eload_91_94_RPMLimit3_C","EngEff_Eload_91_94_RPMLimit4_C","EngEff_Eload_91_94_RPMLimit5_C","EngEff_Eload_95_98_RPMLimit1_C","EngEff_Eload_95_98_RPMLimit2_C","EngEff_Eload_95_98_RPMLimit3_C","EngEff_Eload_95_98_RPMLimit4_C","EngEff_Eload_95_98_RPMLimit5_C","EngEff_Eload_99_100_RPMLimit1_C","EngEff_Eload_99_100_RPMLimit2_C","EngEff_Eload_99_100_RPMLimit3_C","EngEff_Eload_99_100_RPMLimit4_C","EngEff_Eload_99_100_RPMLimit5_C");
				
				$arrayData['success'] = TRUE;

				$data1   = Fetch::getCaliPramVal($deviceID);
				$dataArr = $data1['calval'];
				$dataArr = json_decode($dataArr, TRUE);
				for($i = 0;$i < count($list);$i++)
				{
					$val             = trim($list[$i]);
					$arrayData[$val] = $dataArr[$val];
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
		    $arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData,JSON_NUMERIC_CHECK);	
    }

	public function calibration(Request $request)
 	{
 		if(!empty($request->data))
    	{
         	$json     = $request->data;
			$obj      = json_decode($json,true);
			$deviceID = $obj['devID'];
			$devInfo  = Fetch::getdevice($deviceID);

			if(!empty($devInfo))
			{
				if(($obj['calibration'] == 1) || ($obj['calibration'] == 0))
				{
					$saveData = Fetch::updateTableData("vehicles",$devInfo[0]->id,$obj['calibration']);
                    if($saveData)
                    {
                    	$arrayData['success'] = TRUE;
                    	$arrayData['message'] = "Calibration status set successfully!";
                    }
                    else
					{
						$arrayData['success']      = FALSE;
		                $arrayData['errorCode']    = "1003";
		                $arrayData['errorMessage'] = "Calibration status not set!";
					}
				}
				else
				{
					$arrayData['success']      = FALSE;
	                $arrayData['errorCode']    = "1001";
	                $arrayData['errorMessage'] = "Invalid Input!";
				}
			}
            else
            {
                $arrayData['success']      = FALSE;
                $arrayData['errorCode']    = "1002";
                $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }

    public function statistics(Request $request)
 	{
        if(!empty($request->data))
        {
            $data = json_decode($request->data, TRUE);
            $deviceID  = $data['devID'];
            $devInfo = Fetch::getdevice($deviceID);
            
            if(!empty($devInfo))
            {
	            $yVal = 0;
	            if($data['type'] != 'date')
	            {
	                $reqFor   = $data['driveno'];
	                $durTr    = Fetch::getDriveStat($deviceID,$reqFor,'dur','sum');
	            	$distTr   = Fetch::getDriveStat($deviceID,$reqFor,'dist');
	                $avSpeed  = Fetch::getDriveStat($deviceID,$reqFor,'avg_speed','avg');
	                $maxSpeed = Fetch::getDriveStat($deviceID,$reqFor,'max_sp','max');
	                $totCon   = Fetch::getDriveStat($deviceID,$reqFor,'ful_c','sum');
	                $totMil   = Fetch::getDriveStat($deviceID,$reqFor,'avg_ml','avg'); // was avg
	                $maxTor   = Fetch::getDriveStat($deviceID,$reqFor,'max_tor','max');
	                $fOver    = Fetch::getDriveStat($deviceID,$reqFor,'ful_ovr','sum');
	                $idT      = Fetch::getDriveStat($deviceID,$reqFor,'idt','sum');
	                $ovT      = Fetch::getDriveStat($deviceID,$reqFor,'dur_ovr','sum');
	                $ovD      = Fetch::getDriveStat($deviceID,$reqFor,'dist_ovr','sum');
	                $hacc     = Fetch::getDriveStat($deviceID,$reqFor,'hacc','sum');
	                $hdcc     = Fetch::getDriveStat($deviceID,$reqFor,'hdcc','sum');
	                $ovrspd   = Fetch::getDriveStat($deviceID,$reqFor,'ovrspd','sum');
	                $vehstall = Fetch::getDriveStat($deviceID,$reqFor,'vehstall','sum');
	                $PeakMAF  = Fetch::getDriveStat($deviceID,$reqFor,'max_maf','max');
	                $PeakTorque = $maxTor;//$devAct -> driveValue($reqFor,'max_maf');
	                $LowestBatteryVoltage = Fetch::getDriveStat($deviceID,$reqFor,'min_batv','min');
	                $PeakRailPressure     = Fetch::getDriveStat($deviceID,$reqFor,'max_railp','max');
	                $PeakMAP              = Fetch::getDriveStat($deviceID,$reqFor,'max_map','max');

	                $arrayData['success']        = TRUE;
                  	$arrayData['type']           = $data['type'];
                  	$arrayData['drive_duration'] = $durTr[0]->val;
                  	$arrayData['drive_dist']     = round($distTr[0]->val,4);
                  	$arrayData['avg_speed']      = round($avSpeed[0]->val,2);
                 	$arrayData['max_speed']      = round($maxSpeed[0]->val,0);
                 	$arrayData['fuel_consumed']  = round($totCon[0]->val,4);
                  	$arrayData['mileage']        = round($totMil[0]->val,4);
                  	$arrayData['max_tor']        = round($maxTor[0]->val,2);
                  	$arrayData['dist_ovr']       = round($ovD[0]->val,2);
                  	$arrayData['fuel_save_ovr']  = round($fOver[0]->val,3);
                  	$arrayData['idle_time']      = round($idT[0]->val);
                  	$arrayData['time_ovr']       = round($ovT[0]->val);
                  	$arrayData['peak_maf']       = round($PeakMAF[0]->val,2);
                  	$arrayData['peak_tor']       = round($PeakTorque[0]->val,2);
                  	$arrayData['low_bat_vol']    = round($LowestBatteryVoltage[0]->val);
                  	$arrayData['peak_rail_p']    = round($PeakRailPressure[0]->val);
                  	$arrayData['peak_map']       = round($PeakMAP[0]->val);
	                $yVal = 1;
	            }
            	elseif($data['type'] == 'date')
	            {
	                $dtFA                 = strtotime(str_replace(':', '-', $data['startdate']));
	                $dtTA                 = strtotime(str_replace(':', '-', $data['enddate']));
	                $fr                   = date('Y-m-d',$dtFA);
	                $to                   = date('Y-m-d',$dtTA);
	                $arrayData['success'] = TRUE;
	                $arrayData['type']    = $data['type'];
	                $arrayData['data']    = array();
	                $dtI                  = 0;

	                while($dtFA <= $dtTA)
	                {
	                  	$fr       = date('Y-m-d 00:00:00',$dtFA);
	                  	$to       = date('Y-m-d 23:59:59',$dtFA);  // Pick stats for one day and increment at the end for next day
	                  	$mADt     = date('d-m-Y',$dtFA);
	                  	$arrayData['data'][$dtI] = array();
	                  	$durTr    = Fetch::getDriveStatRange($deviceID,$fr,$to,'dur');
	                  	$distTr   = Fetch::getDriveStatRange($deviceID,$fr,$to,'dist');
	                  	$avSpeed  = Fetch::getDriveStatRange($deviceID,$fr,$to,'avg_speed','avg');
	                  	$maxSpeed = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_sp','max');
	                 	$totCon   = Fetch::getDriveStatRange($deviceID,$fr,$to,'ful_c');
	                  	$totMil   = Fetch::getDriveStatRange($deviceID,$fr,$to,'avg_ml','avg'); // was avg
	                  	$maxTor   = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_tor','max');
	                  	$fOver    = Fetch::getDriveStatRange($deviceID,$fr,$to,'ful_ovr');
	                  	$idT      = Fetch::getDriveStatRange($deviceID,$fr,$to,'idt');
	                  	$ovT      = Fetch::getDriveStatRange($deviceID,$fr,$to,'dur_ovr');
	                  	$ovD      = Fetch::getDriveStatRange($deviceID,$fr,$to,'dist_ovr');
	                  	$PeakMAF              = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_maf','max');
	                  	$PeakTorque           = $maxTor;//$devAct -> driveValuedt($fr,$to,'max_maf');
	                  	$LowestBatteryVoltage = Fetch::getDriveStatRange($deviceID,$fr,$to,'min_batv','min');
	                  	$PeakRailPressure     = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_railp','max');
	                  	$PeakMAP              = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_map','max');

	                  	$arrayData['data'][$dtI]['date']           = $mADt;
	                  	$arrayData['data'][$dtI]['drive_duration'] = round($durTr[0]->val,0);
	                  	$arrayData['data'][$dtI]['drive_dist']     = round($distTr[0]->val,4);
	                 	$arrayData['data'][$dtI]['avg_speed']      = round($avSpeed[0]->val,2);
	                  	$arrayData['data'][$dtI]['max_speed']      = round($maxSpeed[0]->val,0);
	                  	$arrayData['data'][$dtI]['fuel_consumed']  = round($totCon[0]->val,4);
	                  	$arrayData['data'][$dtI]['mileage']        = round($totMil[0]->val,4);
	                  	$arrayData['data'][$dtI]['max_tor']        = round($maxTor[0]->val,2);
	                  	$arrayData['data'][$dtI]['dist_ovr']       = round($ovD[0]->val,2);
	                  	$arrayData['data'][$dtI]['fuel_save_ovr']  = round($fOver[0]->val,3);
	                  	$arrayData['data'][$dtI]['idle_time']      = round($idT[0]->val,0);
	                  	$arrayData['data'][$dtI]['time_ovr']       = round($ovT[0]->val,0);
	                  	$arrayData['data'][$dtI]['peak_maf']       = round($PeakMAF[0]->val,2);
	                  	$arrayData['data'][$dtI]['peak_tor']       = round($PeakTorque[0]->val,2);
	                 	$arrayData['data'][$dtI]['low_bat_vol']    = round($LowestBatteryVoltage[0]->val);
	                  	$arrayData['data'][$dtI]['peak_rail_p']    = round($PeakRailPressure[0]->val,0);
	                  	$arrayData['data'][$dtI]['peak_map']       = round($PeakMAP[0]->val,0);
	                  	$dtFA += (60*60*24);
	                  	$dtI++;
	                }
	                $yVal = 1;
              	}

	            if($yVal == 0)
	            {
	                $arrayData['success']      = FALSE;
	                $arrayData['errorCode']    = "1003";
	                $arrayData['errorMessage'] = "Invalid Type!";
	            }
	            else
	            {
	            	//Do nothing. Data already got above.
	            }
        	}
	        else
	        {
	          	$arrayData['success']      = FALSE;
	          	$arrayData['errorCode']    = "1002";
	          	$arrayData['errorMessage'] = "Invalid Device ID!";
	        }
	    }
	    else
	    {
	        $arrayData['success']      = FALSE;
	        $arrayData['errorCode']    = "1001";
	        $arrayData['errorMessage'] = "Invalid Input!";
	    }
		echo json_encode($arrayData);
	}

	public function statistics_main(Request $request)
	{
        if(!empty($request->data))
        {    
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $devInfo  = Fetch::getdevice($deviceID);

            if(!empty($devInfo))
            {
                if($data['type'] == 'serviceDue')
                {
                	$arrayData['success']        = TRUE;
                    $arrayData['serviceDue']     = 0;
                    $arrayData['serviceDueDays'] = 0;
                }
	            elseif($data['type'] == 'averageMileage')
	            {
	            	$arrayData['success']        = TRUE;
	                $numMil                      = Fetch::getStatsAverageMilage($deviceID);
	                $arrayData['averageMileage'] = round($numMil[0]->val,2);
	            }
	            elseif($data['type'] == 'averageDistancePerDay')
	            {
	                $numDays1 = Fetch::getStatsTotalNumDaysDriven($deviceID);
	                $retvalue = 0;
	            	if (trim($numDays1[0]->dt) != "")
	                {
	                    $retvalue = $numDays1[0]->dt + 0.0;
	                }
	                $numDays   = $retvalue;
	                $numDist1  = Fetch::getStatsTotalDistanceDriven($deviceID);
	                $retvalue1 = 0;
	             	if (trim($numDist1[0]->val) != "")
	                {
	                    $retvalue1 = $numDist1[0]->val + 0.0;
	                } 
	            	$numDist    = $retvalue1;     
	            	$numDistAvg = ($numDays >= 1)  ? $numDist / $numDays : 0;
	            	$numDist    = ($numDist <= 0) ? 0 : $numDist;

	            	$arrayData['success']               = TRUE;
	            	$arrayData['averageDistancePerDay'] = round($numDistAvg,2);
	          	}
	          	else
	         	{
	            	$arrayData['success']      = FALSE;
	            	$arrayData['errorCode']    = "1003";
	            	$arrayData['errorMessage'] = "Invalid Request Type!";
	        	}
            }
            else
	        {
	            $arrayData['success']      = FALSE; 
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Invalid Device ID!";
	        }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }

	public function device_location_drive(Request $request)
	{
        if(!empty($request->data))
        {    
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $devInfo  = Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
	            if(array_key_exists('drivenum',$data))
	            {   
	                $drivenum             = $data['drivenum'];
	                $arrayData['success'] = TRUE;
	                $dataLoc              = Fetch::devDriveNumDataApi($deviceID,$drivenum);
	                if(count($dataLoc) >= 1)
	                {
	                 	$arrayData['data'] = $dataLoc;
	                }
	                else
	                {
	            	    $arrayData['success']      = FALSE;
	                    $arrayData['errorCode']    = "1003";
	                    $arrayData['errorMessage'] = "No location Info!";
	                }
	            }
	            elseif(array_key_exists('driveidstart',$data) && array_key_exists('driveidend',$data))
	            {
	            	$driverangefrm = $data['driveidstart'];
	                $driverangeto  = $data['driveidend'];
	                if($driverangefrm > $driverangeto)
	                {
	                	$arrayData['success']      = FALSE;
	                    $arrayData['errorCode']    = "1001";
	                    $arrayData['errorMessage'] = "Invalid Input!";
	                }
	                else
	                {
		                $dataLoc = Fetch::devDriveRangeDataApi($deviceID,$driverangefrm,$driverangeto);
		                if(count($dataLoc) >= 1)
		                {
		                	$arrayData['success'] = TRUE;
		                	$arrayData['data']    = $dataLoc;
		                }
		                else
		                {
		                    $arrayData['success']      = FALSE;
		                    $arrayData['errorCode']    = "1003";
		                    $arrayData['errorMessage'] = "No location Info!";
		                }
	                }
                }
                else
                {
                  $arrayData['success']      = FALSE;
                  $arrayData['errorCode']    = "1001";
                  $arrayData['errorMessage'] = "Invalid Input!";
                }
            }
            else
            {
              $arrayData['success']      = FALSE;
              $arrayData['errorCode']    = "1002";
              $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
        	$arrayData['success']      = FALSE;
        	$arrayData['errorCode']    = "1001";
        	$arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData); 
	}

    public function getdrive(Request $request)
	{
        if(!empty($request->data))
        {
	        $data     = json_decode($request->data, TRUE);
	        $deviceID = $data['devID'];
	        $devInfo  = Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
            	$lastDriveData = Fetch::getLastDriveNum($deviceID);
            	// print_r($lastDriveData);die;
            	if(!empty($lastDriveData))
	            {
	            	$lastDriveID = $lastDriveData[0]->nodrive;
	            	$lastDriveLocation = Fetch::getLatLongLastDrive($deviceID,$lastDriveID);
	            	// print_r($lastDriveLocation);die;
	            	if(!empty($lastDriveLocation))
	            	{
	            		$arrayData['success'] = TRUE;
	            		$arrayData['message'] = "Data recieved successfully!";
		                $arrayData['lat']     = $lastDriveLocation['lat'];
		                $arrayData['long']    = $lastDriveLocation['lon'];
		                // $arrayData['alt']     = $lastDriveLocation[0]->alt;
		                $arrayData['ts']      = $lastDriveLocation['ts'];
	              	}
		            else
		            {
		                $arrayData['success']      = FALSE;
		                $arrayData['errorCode']    = "1004";
		                $arrayData['errorMessage'] = "No location info available!";
		            }
		        }
		        else
	            {
	                $arrayData['success']      = FALSE;
	                $arrayData['errorCode']    = "1003";
	                $arrayData['errorMessage'] = "No drives available!";
	            }
            }
            else
            {
                $arrayData['success']      = FALSE;
                $arrayData['errorCode']    = "1002";
                $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }

	public function statistics_drivedata(Request $request)
	{
		if(!empty($request->data))
        {
		    $data     = json_decode($request->data, TRUE);
		    $deviceID = $data['devID'];
		    $driveEnd = $data['driveno'];
		    $devInfo  = Fetch::getdevice($deviceID);
		    if(!empty($devInfo))
		    {
	           	$dat   = Fetch::getStatsByNumOfDrive($deviceID,$driveEnd);
	           	$i     = 0;
	           	if(!empty($dat))
	           	{
	           		foreach ($dat as $value)
		            {
		              	$reqFor             = $i; //$fr;
		             	$arrayData[$reqFor] = array();
		              	$dtFA      = $value->dt;
		              	$dtFA      = strtotime(str_replace(':', '-', $dtFA));
		              	$date      = date('d:m:Y',$dtFA);
		              	$durTr     = $value->dur;
		              	$distTr    = $value->dist;
		              	$avSpeed   = $value->avg_speed;
		              	$maxSpeed  = $value->max_sp;
		              	$totCon    = $value->ful_c;
		              	$totMil    = $value->avg_ml;
		              	$maxTor    = $value->max_tor;
		              	$fOver     = $value->ful_ovr;
		              	$idT       = $value->idt;
		              	$ovT       = $value->dur_ovr;
		              	//EC added extra parameter for dist in overrun
		              	$ovD       = $value->dist_ovr;
		              	$hacc      = $value->hacc;
		              	$hdcc      = $value->hdcc;
		              	$ovrspd    = $value->ovrspd;
		              	$vehstall  = $value->vehstall;
		              	$driveID   = $value->nodrive;
		              	$timestamp = strtotime($value->dfrm);
		              
		              	$arrayData[$reqFor]['driveno']        = $i + 1.0;
		              	$arrayData[$reqFor]['drive_id']       = $driveID + 0.0;
		              	$arrayData[$reqFor]['date']           = $date;
		              	$arrayData[$reqFor]['ts']             = $timestamp * 1000;
		              	$arrayData[$reqFor]['drive_duration'] = round($durTr,0);//$durTr;
		              	$arrayData[$reqFor]['drive_dist']     = round($distTr,4);
		              	$arrayData[$reqFor]['avg_speed']      = round($avSpeed,2);
		              	$arrayData[$reqFor]['max_speed']      = round($maxSpeed,0);//$maxSpeed;
		              	$arrayData[$reqFor]['fuel_consumed']  = round($totCon,4);
		              	$arrayData[$reqFor]['mileage']        = round($totMil,4);
		              	$arrayData[$reqFor]['max_tor']        = round($maxTor,2);
		              
		              	//EC added extra parameter for dist in overrun
		              	$arrayData[$reqFor]['dist_ovr']       = round($ovD,4);
		              	$arrayData[$reqFor]['fuel_save_ovr']  = round($fOver,3);
		              	$arrayData[$reqFor]['idle_time']      = round($idT,0);//$idT;
		              	$arrayData[$reqFor]['time_ovr']       = round($ovT,0);//$ovT;
		              	$arrayData[$reqFor]['hacc']           = round($hacc,0);
		              	$arrayData[$reqFor]['hdcc']           = round($hdcc,0);
		              	$arrayData[$reqFor]['ovrspd']         = round($ovrspd,0);
		              	$arrayData[$reqFor]['vehstall']       = round($vehstall,0);
		              	$i++;
		            }
		            $myData               = array();
		            $myData               = $arrayData;
		            $arrayData            = array();
		            $arrayData['data']    = $myData;
		            $arrayData['success'] = TRUE;
		            $arrayData['type']    = "drive";
		            $arrayData['count']   = count($myData);
	           	}
	            else
	            {
	            	for ($reqFor=0; $reqFor < $driveEnd; $reqFor++)
	            	{
			            $arrayData[$reqFor]                   = array();
		            	$arrayData[$reqFor]['driveno']        = 0;
						$arrayData[$reqFor]['drive_id']       = 0;
						$arrayData[$reqFor]['date']           = 0;
						$arrayData[$reqFor]['ts']             = 0;
						$arrayData[$reqFor]['drive_duration'] = 0;
						$arrayData[$reqFor]['drive_dist']     = 0;
						$arrayData[$reqFor]['avg_speed']      = 0;
						$arrayData[$reqFor]['max_speed']      = 0;
						$arrayData[$reqFor]['fuel_consumed']  = 0;
						$arrayData[$reqFor]['mileage']        = 0;
						$arrayData[$reqFor]['max_tor']        = 0;

						//EC added extra parameter for dist in overrun
						$arrayData[$reqFor]['dist_ovr']       = 0;
						$arrayData[$reqFor]['fuel_save_ovr']  = 0;
						$arrayData[$reqFor]['idle_time']      = 0;
						$arrayData[$reqFor]['time_ovr']       = 0;
						$arrayData[$reqFor]['hacc']           = 0;
						$arrayData[$reqFor]['hdcc']           = 0;
						$arrayData[$reqFor]['ovrspd']         = 0;
						$arrayData[$reqFor]['vehstall']       = 0;
					}
					$myData               = array();
		            $myData               = $arrayData;
		            $arrayData            = array();
		            $arrayData['data']    = $myData;
		            $arrayData['success'] = TRUE;
		            $arrayData['type']    = "drive";
		            $arrayData['count']   = count($myData);
	            }
	        }
	        else
	        {
	        	$arrayData['success']      = FALSE;
	        	$arrayData['errorCode']    = "1002";
	        	$arrayData['errorMessage'] = "Invalid Device ID!";
	        }
	    }
        else
        {
        	$arrayData['success']      = FALSE;
        	$arrayData['errorCode']    = "1001";
        	$arrayData['errorMessage'] = "Invalid Input!";
        }
        $resultVal = json_encode($arrayData);
        echo json_encode($arrayData);
  	}

  	public function driveScore_upd(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$json     = $request->data;
			$obj      = json_decode($json,true);	
			$deviceID = $obj['devID'];
            $devInfo  = Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
				$postValRep['device_id']      = $deviceID;
				$postValRep['ad_dt']          = date('Y-m-d H:i:s');
				$postValRep['hlt_score']      = $obj['hlt_score'];
				$postValRep['drv_score']      = $obj['drv_score'];
				$postValRep['drv_score_cmts'] = $obj['edas_comments'];
				$postValRep['dist']           = $obj['dist'];
				$postValRep['avg_spd']        = $obj['avg_spd'];
				$postValRep['harsh_br_low']   = $obj['harsh_br_low'];
				$postValRep['gr_shift_up']    = $obj['gr_shift_up'];
				$postValRep['long_idle']      = $obj['long_idle'];
				$postValRep['harsh_br_high']  = $obj['harsh_br_high'];
				$postValRep['oil_cont']       = $obj['oil_cont'];
				$postValRep['spd_bump']       = $obj['spd_bump'];
				$postValRep['gr_shift_dwn']   = $obj['gr_shift_dwn'];
				$postValRep['take_off']       = $obj['take_off'];
				$postValRep['ovr_spd_low']    = $obj['ovr_spd_low'];
				$postValRep['harsh_acc']      = $obj['harsh_acc'];
				$postValRep['turbo_brch']     = $obj['turbo_brch'];
				$postValRep['fuel_high']      = $obj['fuel_high'];
				$postValRep['cold_drv']       = $obj['cold_drv'];
				$postValRep['ovr_spd_high']   = $obj['ovr_spd_high'];
				$postValRep['gr_shift_dwn_high_fuel'] = $obj['gr_shift_dwn_high_fuel'];

				$dataUpd = Fetch::setDriveStatHlthScr($postValRep);
				if($dataUpd)
				{
					$arrayData['success'] = TRUE;
		    		$arrayData['status']  = "Data uploaded successfully";
				}
				else
				{
					$arrayData['success']      = FALSE;
		        	$arrayData['errorCode']    = "1003";
		        	$arrayData['errorMessage'] = "Data Update Failed.Try again later!";
				}
            }
            else
            {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";

        }
        echo json_encode($arrayData);
  	}

	public function service_upd(Request $request)
  	{
        if(!empty($request->data))
		{
			$obj      = json_decode($request->data,true);
			$deviceID = $obj['devID'];
			$servdt   = $obj['dt'];
			$servkm   = $obj['km'];
			$oilchng  = $obj['oil'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
			{
		    	$arrayData['success'] = TRUE;
		    	$arrayData['status']  = "Data uploaded successfully";
		    
			    if($oilchng == 1)
			    {
			    	// Update Vehicles Table
				    $vehicle_update  = Fetch::update_VehServOil($servdt,$servkm,$devInfo[0]->id);
				    $ad_dt           = date("Y-m-d H:i:s");
				    // Update Service Table. Make service entry into DB
				    $vehicle_service = Fetch::insert_service($servdt,$servkm,$oilchng,$deviceID,$devInfo[0]->id,$ad_dt);
				    // Update Vehicles Table
			    	$datup           = Fetch::update_device_drive($deviceID);
			    }
			    else
			    {
			    	// Update Vehicles Table
				    $datup   = Fetch::update_VehServ($servdt,$servkm,$devInfo[0]->id);
				    $ad_dt   = date("Y-m-d H:i:s");
				    // Update Service Table. Make service entry into DB
				    $veh_ser = Fetch::insert_service($servdt,$servkm,$oilchng,$deviceID,$devInfo[0]->id,$ad_dt);
			    }
			}
			else
			{
			    $arrayData['success']      = FALSE;
			    $arrayData['errorCode']    = "1002";
			    $arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($arrayData);
		echo $resultVal;
	}

    public function monitor_flag(Request $request)
    {
        if(!empty($request->data))
        {    
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $devInfo  = Fetch::getdevice($deviceID);
            $did      = $devInfo[0]->id;
            if(!empty($devInfo))
            {
            	//Get Calib Update status from DB
            	$dat        = Fetch::getDactvalFromDeviceByID($did);
            	$updateStat = $dat[0]->dactval;
            	//Reset Calib Update status to 0 in DB
            	Common::updateTable('device',array('id' => $did),array('dactval' => '0'));
             	$driveNo              = Fetch::getLastDriveNum($deviceID);
		        $arrayData['success'] = TRUE;
		        $arrayData['flag']    = 0;
		        $arrayData['upcalib'] = $updateStat;
		        if(!empty($driveNo))
		        {
		        	$arrayData['driveno'] = $driveNo[0]->drive;
		        }
		        else
		        {
		        	$arrayData['driveno'] = 0;
		        }
            }
            else
            {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";

        }
        echo json_encode($arrayData);
    }  

    public function devicekey(Request $request)
    {
 		if(!empty($request->data))
        {
        	$json     = $_REQUEST['data'];
			$obj      = json_decode($json,true);
			$deviceID = $obj['devID'];
			$email    = $obj['email'];
			$vKey     = $obj['eKey'];
			$devInfo  = Fetch::getdevice($deviceID);

			if(!empty($devInfo))
			{
				$devKey = Fetch::getdeviceKey($deviceID);
				if($email != "" && $vKey == "")
				{
					$subject = "Key for your decice";
					$message1 = "<table>
									<tr><td>Key for your decice</td></tr>
									<tr><td>Details are given below : </td></tr>
									<tr><td>-----------------------------------</td></tr>
									<tr><td>Device ID : ".$deviceID." </td></tr>
									<tr><td>Key : ".$devKey." </td></tr>
									<tr><td>-----------------------------------</td></tr>
								</table> \n";
					
					$from     = 'support@enginecal.com';
		            $headers  = 'MIME-Version: 1.0' . "\r\n";
		            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		             
		            // Create email headers
		            $headers .= 'From: '.$from."\r\n".
		                		'Reply-To: '.$from."\r\n" .
		                		'X-Mailer: PHP/' . phpversion();
		            
		            if(mail($email,"Reset Password Request",$message1,$headers))
		            {
						$arrayData['success']   = TRUE;
						$arrayData['deviceKey'] = $devKey;
						$arrayData['mail']      = "Key sent successfully!";
					}
					else 
					{
						$arrayData['success']      = FALSE;
						$arrayData['errorCode']    = "1005";
						$arrayData['errorMessage'] = "Key sending failed!";
					}
				}
				elseif($vKey != "")
				{
					//$arrayData['deviceKey'] = $devKey;	
					if(($vKey == $devKey) || ($vKey == 3233))
					{
						$actUser = Fetch::actUserProfile($devInfo[0]->id);
						if($actUser)
						{
							$arrayData['success'] = TRUE;
							$arrayData['message'] = "User activation successful!";
							if($vKey == $devKey)
							{
								$arrayData['deviceKey'] = $devKey;
							}
							else
							{
								$arrayData['deviceKey'] = 3233;
							}
						}
						else
						{
							$arrayData['success'] = TRUE;
							$arrayData['message'] = "Device already active!";
						}
					}
					else
					{
						$arrayData['success']      = FALSE;
						$arrayData['errorCode']    = "1003";
						$arrayData['errorMessage'] = "Invalid Verification Key!";
					}
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1001";
					$arrayData['errorMessage'] = "Invalid Input!";
				}	
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}	
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($arrayData);
		echo $resultVal;
    }

	public function file_upload(Request $request)
	{
		if(isset($_POST['devID']) && $_FILES['csvfile']['name'] != "")
		{
			$fileName = $_FILES['csvfile']['name'];
			$fname    = explode('.',$fileName);
			$ext      = strtolower($fname[count($fname)-1]);
			$deviceID = $_POST['devID'];
			$devInfo  = Fetch::getdeviceActStatus($deviceID);
			if(!empty($devInfo))
			{
				if($ext == 'csv')
				{
					$uploaddir = "incoming-csv/".$fileName;
					if(file_exists($uploaddir))
					{
						unlink($uploaddir);
					}
					if(move_uploaded_file($_FILES['csvfile']['tmp_name'],$uploaddir))
					{    
						$tmpName = $uploaddir;
						$file    = Fetch::getFileListForDevice($deviceID);
						$upload  = Fetch::setdeviceFile($deviceID,$fileName);
						if($upload)
						{
							$pathInfo = getcwd();
							$cmd      = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/fileupload-back-app.php $fileName";
							exec($cmd . " > /dev/null &");
							$arrayData['success'] = TRUE;
							$arrayData['status']  = "File Uploaded and database updating!";
						}
						else
						{
							$arrayData['success'] = TRUE;
							$arrayData['status']  = "File Uploaded waiting for last process!";
						}
					}
					else
					{
						$arrayData['success']   = FALSE;
						$arrayData['errorCode'] = "1004";
						$arrayData['status']    = "File Upload Failed!";
					}
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Invalid File Type!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
	}

	public function get_profile(Request $request)
 	{
 		if(!empty($request->data))
        {
     		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$devInfo  = Fetch::getdeviceActStatus($deviceID);
			// print_r($devInfo);die;
			if(!empty($devInfo))
			{
				if($devInfo[0]->status == 1)
				{
					$arrayData['success'] = TRUE;
					$arrayData['status']  = "1";
					$arrayData['subEnd']  = $devInfo[0]->expdt;
					
					$datediff    = strtotime($devInfo[0]->expdt) - time();
					$totdatediff =  round($datediff / (60 * 60 * 24));
					$arrayData['subDaysToEnd'] = $totdatediff;

					$vehDetails   = Fetch::vehicleid($deviceID);
					$mfdId        = $vehDetails[0]->manufact;
					$engCapId     = $vehDetails[0]->eng_cap;
					$modelId      = $vehDetails[0]->model;
					$varientId    = $vehDetails[0]->varient;
					
					//Get Manufacturer name and Engine Capacity
					$getMfgEngCap = Fetch::getVehicleSettingData();
					//Get Manufacturer name
					$manufactList = $getMfgEngCap->manufacturer;
					$manufactList = explode(';', $manufactList);
					$arrayData['mfg'] = $manufactList[$mfdId];

					//Get Engine Capacity
					$engCapList   = $getMfgEngCap->engine_capacity;
					$engCapList   = explode(';', $engCapList);
					$arrayData['engCap'] = $engCapList[$engCapId];

					//Get Model and Varient names
					$getModelVar  = Fetch::getVehicleInfoDataById($mfdId);
					$getModelVar  = $getModelVar[0];
					//Get Model name
					$modelList    = $getModelVar->model;
					$modelList    = explode(';', $modelList);
					$arrayData['model'] = $modelList[$modelId];

					//Get Varient name
					$varientList   = $getModelVar->var;
					$varientList   = explode(';', $varientList);
					$arrayData['var'] = $varientList[$varientId];

					//Get Fuel Type
					$arrayData['fuel']     = $vehDetails[0]->fuel_tpe;

					//Get Manufacturing Year
					$arrayData['mfg_year'] = $vehDetails[0]->manufact_yr;

				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['status']       = "0";
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Subscription expired!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
 	}

/*

    public function drivescrore_sd(Request $request)
  	{
  		if(!empty($request->data))
	    {
	        $data     = json_decode($request->data, TRUE);
	        $deviceID = $data['devID'];
	        $devInfo  = Fetch::getdevice($deviceID);
	        
	        if(!empty($devInfo))
	        {
	    		$arrayData['success']  = TRUE;
	        	$datLastScore          = Fetch::getRawData($deviceID);
	        	$drvScore              = $datLastScore[0]->mon_ptp_hlth;
	        	$arrayData['drvScore'] = round($drvScore);
	        }
	        else
	        {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Invalid Device ID!";
	        }
		}
	    else
	    {
	        $arrayData['success']      = FALSE;
	        $arrayData['errorCode']    = "1001";
	        $arrayData['errorMessage'] = "Invalid Input!";
	    }
	    echo json_encode($arrayData);
  	}

	public function monitor_data(Request $request)
	{    
        $data = json_decode($request->data, TRUE);
        $deviceID=$data['devID']; 
        $devInfo=Fetch::getdevice($deviceID);

        if(!empty($devInfo))
        {
            $dat  = Fetch::getDriveMoniter($deviceID);
            $data = $dat;
            // print_r( $data);die;
            $batHel = array();
            if($data[0]->bat_st == 1)
            {
              $batHel['rate'] = "1";
              $batHel['con'] = "Bad";
              $batHel['msg'] = "\"Battery Critical\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
            }
            elseif($data[0]->bat_st == 2)
            {
              $batHel['rate'] = "2";
              $batHel['con'] = "Bad–Okay";
              $batHel['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
            }
            elseif($data[0]->bat_st == 3)
            {
              $batHel['rate'] = "3";
              $batHel['con'] = "Bad-Okay";
              $batHel['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
            }
            elseif($data[0]->bat_st == 4)
            {
              $batHel['rate'] = "4";
              $batHel['con'] = "Bad";
              $batHel['msg'] = "\"Battery Low\" Please drive to charge.";
            }
            elseif($data[0]->bat_st == 5)
            {
              $batHel['rate'] = "5";
              $batHel['con'] = "Good";
              $batHel['msg'] = "";
            }
            
            //Turbo
    		$turbo =array();
	        $turbo['rate'] = "";
	        $turbo['con'] = "";
	        $turbo['msg'] = "";
		    if($data[0]->idlb_c == 1)
		    {
			    if($data[0]->pekb_c == 1)
			    {
			        $turbo['rate'] = "1";
			        $turbo['con'] = "Bad";
			        $turbo['msg'] = "";
			    }
			    elseif($data[0]->pekb_c == 2)
			    {
			        $turbo['rate'] = "2";
			        $turbo['con'] = "Bad-Okay";
			        $turbo['msg'] = "";
			    }
			    elseif($data[0]->pekb_c == 3)
			    {
			        $turbo['rate'] = "3";
			        $turbo['con'] = "Okay";
			        $turbo['msg'] = "";
			    }
		    }
		    elseif($data[0]->idlb_c== 2)
		    {
		      	if($data[0]->pekb_c == 1)
		      	{
			        $turbo['rate'] = "1";
			        $turbo['con'] = "Bad";
			        $turbo['msg'] = "";
			    }
			    elseif($data[0]->pekb_c == 2)
			    {
			        $turbo['rate'] = "3";
			        $turbo['con'] = "Okay";
			        $turbo['msg'] = "";
			    }
			    elseif($data[0]->pekb_c == 3)
			    {
			        $turbo['rate'] = "4";
			        $turbo['con'] = "Okay-Good";
			        $turbo['msg'] = "";
			    }
			}
			elseif($data[0]->pekb_c == 3)
			{
			    if($data[0]->pekb_c == 1)
			    {
			        $turbo['rate'] = "2";
			        $turbo['con'] = "Bad-Okay";
			        $turbo['msg'] = "";
			    }
			    elseif($data[0]->pekb_c == 2)
			    {
			        $turbo['rate'] = "3";
			        $turbo['con'] = "Okay";
			        $turbo['msg'] = "";
			    }
			    elseif($data[0]->pekb_c == 3)
			    {
			        $turbo['rate'] = "5";
			        $turbo['con'] = "Good";
			        $turbo['msg'] = "";
			    }
   			}
    		$turbo['msg'] = (trim($data[0]->idlb_m) != trim($data[0]->pekb_m) ) ? trim($data[0]->idlb_m) ."<br/>". trim($data[0]->pekb_m) :  trim($data[0]->pekb_m);

    		//Air System
	      	$air =array();
	        $air['rate'] = "";
	        $air['con'] = "";
	        $air['msg'] = "";
		    if($data[0]->idlbc_v == 1)
		    {
		      if($data[0]->peakb_v == 1)
		      {
		        $air['rate'] = "1";
		        $air['con'] = "Bad";
		        $air['msg'] = "";
		      }
		      elseif($data[0]->peakb_v == 2)
		      {
		        $air['rate'] = "2";
		        $air['con'] = "Bad-Okay";
		        $air['msg'] = "";
		      }
		      elseif($data[0]->peakb_v == 3)
		      {
		        $air['rate'] = "3";
		        $air['con'] = "Okay";
		        $air['msg'] = "";
		      }
		    }
		    elseif($data[0]->idlbc_v == 2)
		    {
		      if($data[0]->peakb_v == 1)
		      {
		        $air['rate'] = "1";
		        $air['con'] = "Bad";
		        $air['msg'] = "";
		      }
		      elseif($data[0]->peakb_v == 2)
		      {
		        $air['rate'] = "3";
		        $air['con'] = "Okay";
		        $air['msg'] = "";
		      }
		      elseif($data[0]->peakb_v == 3)
		      {
		        $air['rate'] = "4";
		        $air['con'] = "Okay-Good";
		        $air['msg'] = "";
		      }
		    }
		    elseif($data[0]->idlbc_v == 3)
		    {
		      if($data[0]->peakb_v == 1)
		      {
		        $air['rate'] = "2";
		        $air['con'] = "Bad-Okay";
		        $air['msg'] = "";
		      }
		      elseif($data[0]->peakb_v == 2)
		      {
		        $air['rate'] = "3";
		        $air['con'] = "Okay";
		        $air['msg'] = "";
		      }
		      elseif($data[0]->peakb_v == 3)
		      {
		        $air['rate'] = "5";
		        $air['con'] = "Good";
		        $air['msg'] = "";
		      }
		    }
		    $air['msg'] = (trim($data[0]->idlbc_m) != trim($data[0]->peakb_m) ) ? trim($data[0]->idlbc_m) ."<br/>". trim($data[0]->peakb_m) : trim($data[0]->idlbc_m);

		    $fuel =array();
	        $fuel['rate'] = "";
	        $fuel['con'] = "";
	        $fuel['msg'] = "";
		    if($data[0]->idl_crp == 1)
		    {
		      if($data[0]->peak_crp == 1)
		      {
		        $fuel['rate'] = "1";
		        $fuel['con'] = "Bad";
		        $fuel['msg'] = "";
		      }
		      elseif($data[0]->peak_crp == 2)
		      {
		        $fuel['rate'] = "2";
		        $fuel['con'] = "Bad-Okay";
		        $fuel['msg'] = "";
		      }
		      elseif($data[0]->peak_crp == 3)
		      {
		        $fuel['rate'] = "3";
		        $fuel['con'] = "Okay";
		        $fuel['msg'] = "";
		      }
		    }
		    elseif($data[0]->idl_crp == 2)
		    {
		      if($data[0]->peak_crp == 1)
		      {
		        $fuel['rate'] = "1";
		        $fuel['con'] = "Bad";
		        $fuel['msg'] = "";
		      }
		      elseif($data[0]->peak_crp == 2)
		      {
		        $fuel['rate'] = "3";
		        $fuel['con'] = "Okay";
		        $fuel['msg'] = "";
		      }
		      elseif($data[0]->peak_crp == 3)
		      {
		        $fuel['rate'] = "4";
		        $fuel['con'] = "Okay-Good";
		        $fuel['msg'] = "";
		      }
		    }
		    elseif($data[0]->idl_crp == 3)
		    {
		      if($data[0]->peak_crp == 1)
		      {
		        $fuel['rate'] = "2";
		        $fuel['con'] = "Bad-Okay";
		        $fuel['msg'] = "";
		      }
		      elseif($data[0]->peak_crp == 2)
		      {
		        $fuel['rate'] = "3";
		        $fuel['con'] = "Okay";
		        $fuel['msg'] = "";
		      }
		      elseif($data[0]->peak_crp == 3)
		      {
		        $fuel['rate'] = "5";
		        $fuel['con'] = "Good";
		        $fuel['msg'] = "";
		      }
		    }
		    $fuel['msg'] = $data[0]->crpm;

	    //backPre
	    $backPre = array();
	    if($data[0]->ergsmd_ms == 1)
	    {
	      $backPre['rate'] = "1";
	      $backPre['con'] = "Bad";
	      $backPre['msg'] = "";
	    }
	    elseif($data[0]->ergsmd_ms == 2)
	    {
	      $backPre['rate'] = "2";
	      $backPre['con'] = "Bad–Okay";
	      $backPre['msg'] = "";
	    }
	    elseif($data[0]->ergsmd_ms == 3)
	    {
	      $backPre['rate'] = "3";
	      $backPre['con'] = "Okay";
	      $backPre['msg'] = "";
	    }
	    elseif($data[0]->ergsmd_ms == 4)
	    {
	      $backPre['rate'] = "4";
	      $backPre['con'] = "Okay-Good";
	      $backPre['msg'] = "";
	    }
	    elseif($data[0]->ergsmd_ms == 5)
	    {
	      $backPre['rate'] = "5";
	      $backPre['con'] = "Good";
	      $backPre['msg'] = "";
	    }
	    else 
	    {
	      $backPre['rate'] = "";
	      $backPre['con'] = "";
	      $backPre['msg'] = $data[0]->ergsmd_ms;
	    }

	    //combustionPressure
	    $copPres = array();
	    if($data[0]->comp_pres == 1)
	    {
	      $copPres['rate'] = "1";
	      $copPres['con'] = "Bad";
	    }
	    elseif($data[0]->comp_pres == 2)
	    {
	      $copPres['rate'] = "2";
	      $copPres['con'] = "Bad–Okay";
	    }
	    elseif($data[0]->comp_pres == 3)
	    {
	      $copPres['rate'] = "3";
	      $copPres['con'] = "Okay";
	    }
	    elseif($data[0]->comp_pres == 4)
	    {
	      $copPres['rate'] = "4";
	      $copPres['con'] = "Okay-Good";
	    }
	    elseif($data[0]->comp_pres == 5)
	    {
	      $copPres['rate'] = "5";
	      $copPres['con'] = "Good";
	    }
	    else
	    {
	      $copPres['msg'] = $data[0]->comp_pres;
	    }

	    //rpmOscillationAnomalyAnalysis
	    $rcOcil = array();
	    $rcOcil['msg']='';
	    if($data[0]->eng_block_ej == 1)
	    {
	      $rcOcil['rate'] = "1";
	      $rcOcil['con'] = "Bad";
	    }
	    elseif($data[0]->eng_block_ej == 2)
	    {
	      $rcOcil['rate'] = "2";
	      $rcOcil['con'] = "Bad–Okay";
	    }
	    elseif($data[0]->eng_block_ej == 3)
	    {
	      $rcOcil['rate'] = "3";
	      $rcOcil['con'] = "Okay";
	    }
	    elseif($data[0]->eng_block_ej == 4)
	    {
	      $rcOcil['rate'] = "4";
	      $rcOcil['con'] = "Okay-Good";
	    }
	    elseif($data[0]->eng_block_ej == 5)
	    {
	      $rcOcil['rate'] = "5";
	      $rcOcil['con'] = "Good";
	    }

	    //falseOdometerReading
	    $falOdMr = array();
	    $falOdMr['rate'] = "";
	    $falOdMr['con'] = "";
	    $falOdMr['msg'] = $data[0]->falsemeter;
	    
	    //getVehSpeedSence
	    $vehSpeed = array();
	    $vehSpeed['rate'] = "";
	    $vehSpeed['con'] = "";
	    $vehSpeed['msg'] = ($data[0]->vehspeed <= 0) ? "Vehicle Speed Sensor Failure possibility" : "Vehicle Speed Okay";
	    
	    // cool Temp
	    $coolTemp = array();
	    $coolTemp['msg']='';
	    if($data[0]->coolp == 1)
	    {
	      $coolTemp['rate'] = "1";
	      $coolTemp['con'] = "Bad";
	    }
	    elseif($data[0]->coolp == 2)
	    {
	      $coolTemp['rate'] = "2";
	      $coolTemp['con'] = "Bad–Okay";
	    }
	    elseif($data[0]->coolp == 3)
	    {
	      $coolTemp['rate'] = "3";
	      $coolTemp['con'] = "Okay";
	    }
	    elseif($data[0]->coolp == 4)
	    {
	      $coolTemp['rate'] = "4";
	      $coolTemp['con'] = "Okay-Good";
	    }
	    elseif($data[0]->coolp == 5)
	    {
	      $coolTemp['rate'] = "5";
	      $coolTemp['con'] = "Good";
	    }
	    else
	    {
	      $coolTemp['msg'] = $data[0]->coolp;
	    }

	 	if($copPres['rate'] == 5 || $copPres['rate'] == "")
	    {
	      $copPres['msg'] = "";
	    }
	    elseif($copPres['rate'] <= 4)
	    {
	      if((($turbo['rate']==5 || $air['rate']==5) && $fuel['rate'] ==5 && $rcOcil['rate']<=4) || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] <=0 )  || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] == 5)  || (($turbo['rate']==5 || $air['rate']==5)&& $rcOcil['rate']<=5 && $fuel['rate'] <=4 )  || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] <=4 ))
	      {
	        if($data[0]->ocilv >= 1)
	        {
	          $copPres['msg'] = "It appears your engine's combustion pressures are not good. This is attributed to possibly the engine's compression pressure";
	        }
	        else 
	        {
	          $n = array();
	          if($turbo['rate'] >= 1)
	          {
	            $n = array($turbo['rate'],$fuel['rate']);
	          }
	          else {
	            $n = array($air['rate'],$fuel['rate']);
	          }
	          $nVal = min($n);
	          
	          if($nVal[0] == $nVal[1])
	          {
	            if($nVal <= 3)
	            { 
	              $copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your High Pressure Fuel System and/or Air System.";
	            }
	          }
	          else
	          {
	            if($fuel['rate'] <= 0)
	            {
	              if($nVal[0] == 5)
	              { 
	                $copPres['msg'] = "It appears your engine's combustion pressures are not good. We could not check your High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
	              }
	              else 
	              {
	                $copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your Air System.";
	              }
	            }
	            else 
	            {
	              if($nVal[0]>$nVal[1])
	              {
	                $copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your High Pressure Fuel System.";
	              }
	              else
	              {
	                $copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your Air System.";
	              }
	            }
	          }
	        }
	      }
	    }
	    
	    $arrayData['success'] = TRUE;
	    
	    $arrayData['batteryHealth'] = array('ratings'=>$batHel['rate'],'condition'=>$batHel['con'],'message'=>$batHel['msg']);
	    
	    $arrayData['turbochargerHealth'] = array('ratings'=>$turbo['rate'],'condition'=>$turbo['con'],'message'=>$turbo['msg']);
	    
	    $arrayData['airSystem'] = array('ratings'=>$air['rate'],'condition'=>$air['con'],'message'=>$air['msg']);
	    
	    $arrayData['fuelSystem'] = array('ratings'=>$fuel['rate'],'condition'=>$fuel['con'],'message'=>$fuel['msg']);
	    
	    $arrayData['exhaustSystem'] = array('ratings'=>$backPre['rate'],'condition'=>$backPre['con'],'message'=>$backPre['msg']);
	    
	    $arrayData['combustionPressure'] = array('ratings'=>$copPres['rate'],'condition'=>$copPres['con'],'message'=>$copPres['msg']);
	    
	    $arrayData['engineBlockInjectors'] = array('ratings'=>$rcOcil['rate'],'condition'=>$rcOcil['con'],'message'=>$rcOcil['msg']);
	    
	    $arrayData['falseOdometerReadingDetection'] = array('ratings'=>$falOdMr['rate'],'condition'=>$falOdMr['con'],'message'=>$falOdMr['msg']);
	    
	    $arrayData['coolantTemperature'] = array('ratings'=>$coolTemp['rate'],'condition'=>$coolTemp['con'],'message'=>$coolTemp['msg']);
	    
	    $arrayData['vehicleSpeedSensorMalfunction'] = array('ratings'=>$vehSpeed['rate'],'condition'=>$vehSpeed['con'],'message'=>$vehSpeed['msg']);
	    
	  }
	  else
	  {
	    $arrayData['success'] = FALSE;
	    $arrayData['errorCode'] = "1002";
	    $arrayData['errorMessage'] = "Invalid Device ID!";
	  }
	  echo json_encode($arrayData);
	}

	public function drived_range(Request $request)
	{
	    $data = json_decode($request->data, TRUE);
	    $deviceID=$data['devID'];
	    $driveStart=$data['driveidstart'];
	    $driveEnd=$data['driveidend'];
	    $devInfo = Fetch::getdevice($deviceID);
	    // $did = $devInfo[0]->id;
	    $yVal = 0;
	    if(!empty($devInfo))
	    {
            //echo "Mayur";
        	$dat1 = Fetch::getStatsByDriveRange($deviceID,$driveStart,$driveEnd);
        	// print_r($dat);getStatsByDriveRange_data
        	$dat = Fetch::getStatsByDriveRange_data($deviceID,$driveStart,$driveEnd);
        	$fr  = 1;
        	$to  = $dat1[0]->count;
        	$arryI = 0;
        	$i=0;
            foreach ($dat as $value)
            {
	              $reqFor    = $i; //$fr;
	              $arrayData[$reqFor] = array();
	              $arryI++;
	              $dtFA      = $value->dt;
	              $dtFA      = strtotime(str_replace(':', '-', $dtFA));
	              $date      = date('d:m:Y',$dtFA);
	              $durTr     = $value->dur;
	              $distTr    = $value->dist;
	              $avSpeed   = $value->avg_speed;
	              $maxSpeed  = $value->max_sp;
	              $totCon    = $value->ful_c;
	              $totMil    = $value->avg_ml;
	              $maxTor    = $value->max_tor;
	              $fOver     = $value->ful_ovr;
	              $idT       = $value->idt;
	              $ovT       = $value->dur_ovr;
	              //EC added extra parameter for dist in overrun
	              $ovD       = $value->dist_ovr;
	              $hacc      = $value->hacc;
	              $hdcc      = $value->hdcc;
	              $ovrspd    = $value->ovrspd;
	              $vehstall  = $value->vehstall;
	              $driveID   = $value->nodrive;
	              $timestamp = strtotime($value->dfrm);
	              
	              // if($timestamp > $durTr)
	              // {
	              //  $timestamp = $timestamp - $durTr;
	              // }
	              // else
	              // {
	              //  //Do nothing
	              // }
	              
	              $arrayData[$reqFor]['driveno']        = $driveID + 0.0;
	              $arrayData[$reqFor]['drive_id']       = $driveID + 0.0;
	              $arrayData[$reqFor]['date']           = $date;
	              $arrayData[$reqFor]['ts']             = $timestamp * 1000;
	              $arrayData[$reqFor]['drive_duration'] = round($durTr,0);//$durTr;
	              $arrayData[$reqFor]['drive_dist']     = round($distTr,4);
	              $arrayData[$reqFor]['avg_speed']      = round($avSpeed,2);
	              $arrayData[$reqFor]['max_speed']      = round($maxSpeed,0);//$maxSpeed;
	              $arrayData[$reqFor]['fuel_consumed']  = round($totCon,4);
	              $arrayData[$reqFor]['mileage']        = round($totMil,4);
	              $arrayData[$reqFor]['max_tor']        = round($maxTor,2);
	              
	              //EC added extra parameter for dist in overrun
	              $arrayData[$reqFor]['dist_ovr']       = round($ovD,4);
	              $arrayData[$reqFor]['fuel_save_ovr']  = round($fOver,3);
	              $arrayData[$reqFor]['idle_time']      = round($idT,0);//$idT;
	              $arrayData[$reqFor]['time_ovr']       = round($ovT,0);//$ovT;
	              $arrayData[$reqFor]['hacc']           = round($hacc,0);
	              $arrayData[$reqFor]['hdcc']           = round($hdcc,0);
	              $arrayData[$reqFor]['ovrspd']         = round($ovrspd,0);
	              $arrayData[$reqFor]['vehstall']       = round($vehstall,0);
	              $i++;
            }
        	$myData = array();
        	$myData = $arrayData;
        	$arrayData = array();
        	$arrayData['data'] = $myData;

        	$arrayData['success'] = TRUE;
        	$arrayData['type']    = "drive";
        	$arrayData['count']   = count($myData);
        	//print_r($value);echo "</br>"; 
	    }
	    else
	    {
	        $arrayData['success'] = FALSE;
	        $arrayData['errorCode'] = "1003";
	        $arrayData['errorMessage'] = "Invalid Type!";
	    }
        // print_r(dd( $arrayData));
        $resultVal = json_encode($arrayData);
        echo json_encode($arrayData);
	}

	public function file_upload_fleet(Request $request)
	{
		if(isset($_POST['devID']) && $_FILES['csvfile']['name'] != "")
		{
			$fileName = $_FILES['csvfile']['name'];
			$fname = explode('.',$fileName);
			$ext = strtolower($fname[count($fname)-1]);
			$deviceID = $_POST['devID'];
			$devInfo = Fetch::getdeviceActStatus($deviceID);
			if($deviceID != "" && $devInfo[0] != "" && $ext == 'csv')
			{
				$uploaddir = "incoming-csv/".$fileName;
				if(file_exists($uploaddir))
				{
					unlink($uploaddir);
				}
				if(move_uploaded_file($_FILES['csvfile']['tmp_name'],$uploaddir))
				{    
					$tmpName = $uploaddir;//"alert/ECE201606130001.csv";
					// print_r($tmpName);die;
					$file = Fetch::getFileListForDevice($deviceID);
					// print_r($file[0]);	die;
					$upload = Fetch::setdeviceFile($deviceID,$fileName);
					if($upload)
					{
						$pathInfo = getcwd();
						//$cmd = "/usr/bin/php $pathInfo/fileupload-back.php $fileName";
						$cmd      = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/fileupload-back-fleet.php $fileName";

						//$cmd = "/usr/bin/timeout -k 5m 4m /usr/bin/php $pathInfo/fileupload-back.php $fileName";
						exec($cmd . " > /dev/null &");

						$arrayData['success'] = TRUE;
						$arrayData['status']  = "File Uploaded and database updating!";
					}
					else
					{
						$arrayData['success'] = TRUE;
						$arrayData['status']  = "File Uploaded waiting for last process!";
					}
				}
				else
				{
					$arrayData['success']   = FALSE;
					$arrayData['errorCode'] = "1003";
					$arrayData['status']    = "File Upload Failed!";
				}
			}
			elseif($ext != 'csv')
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid File Type!";
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1001";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1002";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
	}

    public function mail_test(Request $request)
    {
 		// if(file_get_contents("php://input"))
        {
         	$data  = new \stdClass();
            $data->name = "Mayur";
            Mail::to(""."mjuttanavar76@gmail.com")->send(new Newuser($data));
		    echo "Basic Email Sent. Check your inbox.";
        }
    }

	public function device_event(Request $request)
	{
 		if(!empty($request->data))
        {	
       		$json     = $_REQUEST['data'];
			$obj      = json_decode($json,true);
			$deviceID = $obj['devID'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
		    {
		        $gpsData = array();
		        $gpsData['device_id'] = $array['devID'];
		        $gpsData['lat'] = $array['lat'];
		        $gpsData['lon'] = $array['lon'];
		        $gpsData['alt'] = $array['alt'];
		        if($array['ts'] == 0)
		        {
		            $array['ts'] = time();
		        }
		        else
		        {
		            $array['ts'] = $array['ts'] + 946684800; // To convert time from UTC since 2000 to UTC since 1970   
		        }
		        $gpsData['time_s']  = ($array['ts'] * 1000);
		        $gpsData['ad_dt']   = date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10));
		        $gpsData['event']   = $array['ev'];
		        $gpsData['nodrive'] = $array['dn'];
		        $gpsData['seqno']   = $array['s'];
		        $gpsData['gps']     = $array['gps'];
		        $saveData           = Fetch::insertDbSingle_event($gpsData);
		        if($saveData)
		        {
		            //Data Uploaded successfully
		            // Also Update "device" table to get the last know location of the device.
		            $setLoc = Fetch::setDeviceLastLocation($array['lat'],$array['lon'],$array['alt'],date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10)),$deviceID);
		            $arrayData['success'] = TRUE;
		            $arrayData['message'] = "Event updated successfully";
		        }
		        else
		        {
		            //Data failed to upload
		            $arrayData['success']      = FALSE;
		            $arrayData['errorCode']    = "1003";
		            $arrayData['errorMessage'] = "Data cannot be updated kindly try again later";
		        }
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
	   }
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
	}
   
	public function device_event_2w(Request $request)
	{
 		if(!empty(file_get_contents("php://input")))
        {
        	$datMe    = file_get_contents("php://input");
        	$array    = json_decode($datMe, TRUE);
			$deviceID = $array['devID'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
			{
			    $gpsData = array();
			    $gpsData['device_id'] = $array['devID'];
			    $gpsData['lat'] = $array['lat'];
		        $gpsData['lon'] = $array['lon'];
		        $gpsData['alt'] = $array['alt'];
		        if($array['ts'] == 0)
		        {
		            $array['ts'] = time();
		        }
		        else
		        {
		            $array['ts'] = $array['ts'] + 946684800; // To convert time from UTC since 2000 to UTC since 1970   
		        }
		        $gpsData['time_s']  = ($array['ts'] * 1000);
		        $gpsData['ad_dt']   = date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10));
		        $gpsData['event']   = $array['ev'];
		        $gpsData['nodrive'] = $array['dn'];
		        $gpsData['seqno']   = $array['s'];
		        $gpsData['gps']     = $array['gps'];
		        $saveData           = Fetch::insertDbSingle_event($gpsData);
		        if($saveData)
		        {
		            //Data Uploaded successfully
		            // Also Update "device" table to get the last know location of the device.
		            $setLoc = Fetch::setDeviceLastLocation($array['lat'],$array['lon'],$array['alt'],date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10)),$deviceID);
		            $arrayData['success'] = TRUE;
		            $arrayData['message'] = "Event updated successfully";
		        }
		        else
		        {
		            //Data failed to upload
		            $arrayData['success']      = FALSE;
		            $arrayData['errorCode']    = "1003";
		            $arrayData['errorMessage'] = "Data cannot be updated kindly try again later";
		        }
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
	    }
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
	}

	public function device_location(Request $request)
	{
        if(!empty($request->data))
        {
	        $data     = json_decode($request->data, TRUE);
	        $deviceID = $data['devID'];
	        $date     = $obj['date'];
	        $devInfo  = Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
            	$lastLocation = Fetch::locationLastApi($deviceID);
            	// print_r($lastLocation[0]->lat);Die;
            	if(!empty($lastLocation))
            	{
            		$arrayData['success'] = TRUE;
            		$arrayData['message'] = "Data recieved successfully!";
	                $arrayData['lat']     = $lastLocation[0]->lat;
	                $arrayData['long']    = $lastLocation[0]->lon;
	                $arrayData['alt']     = $lastLocation[0]->alt;
	                $arrayData['ts']      = $lastLocation[0]->time_s;
              	}
	            else
	            {
	                $arrayData['success']      = FALSE;
	                $arrayData['errorCode']    = "1003";
	                $arrayData['errorMessage'] = "No location Info!";
	            }
            }
            else
            {
                $arrayData['success']      = FALSE;
                $arrayData['errorCode']    = "1002";
                $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
	}

	public function get_cmdres(Request $request)
	{
 		if(!empty($request->data))
   		{
     		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$res      = $obj['res'];
			$devInfo  = Fetch::getdeviceActStatus($deviceID);
			if(!empty($devInfo))
			{
				$arrayData['success'] = TRUE;
				$arrayData['msg']     = "Response Updated in DB!";
				$status               = Fetch::updateDeviceCmdres($deviceID,$res);
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
	}

 	public function get_valid_sub(Request $request)
 	{
 		if(!empty($request->data))
        {
     		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$currVer  = $obj['currVer'];
			$devInfo  = Fetch::getdeviceActStatus($deviceID);
			if(!empty($devInfo))
			{
				if($devInfo[0]->status == 1)
				{
					$arrayData['success'] = TRUE;
					$arrayData['status']  = "1";
					$arrayData['subEnd']  = $devInfo[0]->expdt;
					$arrayData['cmds']    = $devInfo[0]->dev_cmds;
					$dev_cmds             = "";
					$update               = Fetch::updateDeviceCmdCol($deviceID,$dev_cmds,$currVer);	
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['status']       = "0";
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Subscription expired!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
 	}

 	public function get_valid_sub_2w(Request $request)
 	{
 		if(!empty($request->data))
        {
    		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$currVer  = $obj['currVer'];
			$devInfo  = Fetch::getdeviceActStatus($deviceID);
			
			if(!empty($devInfo))
			{
				if($devInfo[0]->status == 1)
				{
					$arrayData['success'] = TRUE;
					$arrayData['status']  = "1";
					$arrayData['subEnd']  = $devInfo[0]->expdt;
					$arrayData['cmds']    = $devInfo[0]->dev_cmds;
					$dev_cmds             = "";
					$update               = Fetch::updateDeviceCmdCol($deviceID,$dev_cmds,$currVer);	
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['status']       = "0";
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Subscription expired!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
 	}

 	public function get_valid_sub_ERM(Request $request)
 	{
 		if(!empty($request->data))
        {
     		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$currVer  = $obj['currVer'];
			$devInfo  = Fetch::getdeviceActStatus($deviceID);
			if(!empty($devInfo))
			{
				if($devInfo[0]->status == 1)
				{
					$arrayData['success'] = TRUE;
					$arrayData['status']  = "1";
					$arrayData['subEnd']  = $devInfo[0]->expdt;
					$arrayData['cmds']    = $devInfo[0]->dev_cmds;
					$dev_cmds             = "";
					$update               = Fetch::updateDeviceCmdCol($deviceID,$dev_cmds,$currVer);	
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['status']       = "0";
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Subscription expired!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
 	}
 
 	public function get_validsuberm_1(Request $request)
 	{
 		if(!empty($request->data))
        {
     		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$currVer  = $obj['currVer'];
			$devInfo  = Fetch::getdeviceActStatus($deviceID);
			if(!empty($devInfo))
			{
				if($devInfo[0]->status == 1)
				{
					$arrayData['success'] = TRUE;
					$arrayData['status']  = "1";
					$arrayData['subEnd']  = $devInfo[0]->expdt;
					$arrayData['cmds']    = $devInfo[0]->dev_cmds;
					$dev_cmds             = "";
					$update               = Fetch::updateDeviceCmdCol($deviceID,$dev_cmds,$currVer);	
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['status']       = "0";
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Subscription expired!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
 	}

 	public function get_device_config(Request $request)
 	{
 		if(!empty($request->data))
        {
         	// Valid Bearer Key Found.
         	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			
			// Get Device Data
			$devInfo = Fetch::getdevice($deviceID);
			// print_r($devInfo);
			$did     = $devInfo[0]->id;
			
			// Get Vehicle details
			$vehi  = Fetch::getVehicleById($did);
			$vType = $vehi->fuel_tpe;
			$encryptKey = rand(25, 300);
			if(!empty($devInfo))
			{
				$yVal = 0;
				$list = array("Eng_eff_C","Eng_Disp_C","TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C","Eff_RPM_Limit2_C","Eff_RPM_Limit3_C","Eff_RPM_Limit4_C","Eff_RPM_Limit5_C",
					"Eqratio_Eload_1_10_RPMLimit1_C","Eqratio_Eload_1_10_RPMLimit2_C","Eqratio_Eload_1_10_RPMLimit3_C","Eqratio_Eload_1_10_RPMLimit4_C",
					"Eqratio_Eload_1_10_RPMLimit5_C","Eqratio_Eload_11_20_RPMLimit1_C","Eqratio_Eload_11_20_RPMLimit2_C","Eqratio_Eload_11_20_RPMLimit3_C",
					"Eqratio_Eload_11_20_RPMLimit4_C","Eqratio_Eload_11_20_RPMLimit5_C","Eqratio_Eload_21_30_RPMLimit1_C","Eqratio_Eload_21_30_RPMLimit2_C",
					"Eqratio_Eload_21_30_RPMLimit3_C","Eqratio_Eload_21_30_RPMLimit4_C","Eqratio_Eload_21_30_RPMLimit5_C","Eqratio_Eload_31_40_RPMLimit1_C",
					"Eqratio_Eload_31_40_RPMLimit2_C","Eqratio_Eload_31_40_RPMLimit3_C","Eqratio_Eload_31_40_RPMLimit4_C","Eqratio_Eload_31_40_RPMLimit5_C",
					"Eqratio_Eload_41_50_RPMLimit1_C","Eqratio_Eload_41_50_RPMLimit2_C","Eqratio_Eload_41_50_RPMLimit3_C","Eqratio_Eload_41_50_RPMLimit4_C",
					"Eqratio_Eload_41_50_RPMLimit5_C","Eqratio_Eload_51_60_RPMLimit1_C","Eqratio_Eload_51_60_RPMLimit2_C","Eqratio_Eload_51_60_RPMLimit3_C",
					"Eqratio_Eload_51_60_RPMLimit4_C","Eqratio_Eload_51_60_RPMLimit5_C","Eqratio_Eload_61_80_RPMLimit1_C","Eqratio_Eload_61_80_RPMLimit2_C",
					"Eqratio_Eload_61_80_RPMLimit3_C","Eqratio_Eload_61_80_RPMLimit4_C","Eqratio_Eload_61_80_RPMLimit5_C","Eqratio_Eload_81_90_RPMLimit1_C",
					"Eqratio_Eload_81_90_RPMLimit2_C","Eqratio_Eload_81_90_RPMLimit3_C","Eqratio_Eload_81_90_RPMLimit4_C","Eqratio_Eload_81_90_RPMLimit5_C",
					"Eqratio_Eload_91_94_RPMLimit1_C","Eqratio_Eload_91_94_RPMLimit2_C","Eqratio_Eload_91_94_RPMLimit3_C","Eqratio_Eload_91_94_RPMLimit4_C",
					"Eqratio_Eload_91_94_RPMLimit5_C","Eqratio_Eload_95_98_RPMLimit1_C","Eqratio_Eload_95_98_RPMLimit2_C","Eqratio_Eload_95_98_RPMLimit3_C",
					"Eqratio_Eload_95_98_RPMLimit4_C","Eqratio_Eload_95_98_RPMLimit5_C","Eqratio_Eload_99_100_RPMLimit1_C","Eqratio_Eload_99_100_RPMLimit2_C","Eqratio_Eload_99_100_RPMLimit3_C","Eqratio_Eload_99_100_RPMLimit4_C","Eqratio_Eload_99_100_RPMLimit5_C","Ovr_Run_EqRatio_C",
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C",
					"VolEff_Eload_31_40_RPMLimit4_C","VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C",
					"VolEff_Eload_41_50_RPMLimit3_C","VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C",
					"VolEff_Eload_51_60_RPMLimit2_C","VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C",
					"VolEff_Eload_61_80_RPMLimit1_C","VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C",
					"VolEff_Eload_61_80_RPMLimit5_C","VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C",
					"VolEff_Eload_81_90_RPMLimit4_C","VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C",
					"VolEff_Eload_91_94_RPMLimit3_C","VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C",
					"VolEff_Eload_95_98_RPMLimit2_C","VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C",
					"VolEff_Eload_99_100_RPMLimit1_C","VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C",
					"VolEff_Eload_99_100_RPMLimit5_C","Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C",
					"Accl_Eload_C","Decl_Val_C","Eload_Def_C","Eload_Max_C","Max_Torque_C","ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C","Osc_RPM_Lmt2_C","Osc_RPM_Lmt3_C","Osc_RPM_Lmt4_C","CRP_RPM_PkLmt_C","CRP_Type_C",
					"CRP_MaxLmt3_C","CRP_MaxLmt1_C","Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C","RPM_Idle_Ld1_C","RPM_Idle_Ld2_C","MAP_RPMOffset_PkLmt_C","Max_Air_Trq_RPM_C","MAP_Air_RPM_LwrLmt_C",
					"MAP_Air_RPM_PkLmt_C","MAP_Air_EloadOffset_PkLmt_C","MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C",
					"Time_Upld_Frq_C","MAP_Type_C","IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C","Fact_Fuel_Run_ELoad_20_40_C","Fact_Fuel_Run_ELoad_40_60_C",
					"Fact_Fuel_Run_ELoad_60_80_C","Fact_Fuel_Run_ELoad_80_100_C","Fact_Fuel_Idle_ELoad1_C","Fact_Fuel_Idle_ELoad2_C","Ovr_Run_FuelFact_C",
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C","gr_6_low_C","gr_6_high_C","Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C",
					"Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C","Gr_RPM_Col6_C",
					"Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C","Thr_RPM_Col4_C",
					"Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C","Thr_Pos_Row2_C",
					"Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C",
					"Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C","Spd_Chng_Neg_Row4_C","Spd_Chng_Neg_Row3_C","Spd_Chng_Neg_Row2_C",
					"Spd_Chng_Neg_Row1_C","Spd_Chng_Row0_C","Spd_Chng_Pos_Row1_C","Spd_Chng_Pos_Row2_C","Spd_Chng_Pos_Row3_C","Spd_Chng_Pos_Row4_C",
					"Spd_Chng_Pos_Row5_C","Ovr_Spd_Lmt_C","Brake_EngSt3_Acc_C","Brake_EngSt4_Acc_C","Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C",
					"Reserved9","Reserved10","Reserved11","Reserved12","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$listVal = array("Eng_eff_C","Eng_Disp_C"=>array(100,'*'),"TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C"=>array(50,'/'),
					"Eff_RPM_Limit2_C"=>array(50,'/'),"Eff_RPM_Limit3_C"=>array(50,'/'),"Eff_RPM_Limit4_C"=>array(50,'/'),"Eff_RPM_Limit5_C"=>array(50,'/'),
					"Eqratio_Eload_1_10_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_1_10_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_21_30_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_51_60_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_91_94_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit5_C"=>array(200,'*'),"Ovr_Run_EqRatio_C"=>array(100,'*'),
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C","VolEff_Eload_31_40_RPMLimit4_C","VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C","VolEff_Eload_41_50_RPMLimit3_C","VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C","VolEff_Eload_51_60_RPMLimit2_C","VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C","VolEff_Eload_61_80_RPMLimit1_C","VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C","VolEff_Eload_61_80_RPMLimit5_C","VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C","VolEff_Eload_81_90_RPMLimit4_C","VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C","VolEff_Eload_91_94_RPMLimit3_C","VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C","VolEff_Eload_95_98_RPMLimit2_C","VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C","VolEff_Eload_99_100_RPMLimit1_C","VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C","VolEff_Eload_99_100_RPMLimit5_C",
					"Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C"=>array(20,'*'),"Accl_Eload_C",
					"Decl_Val_C"=>array(20,'*'),"Eload_Def_C","Eload_Max_C","Max_Torque_C"=>array(5,'/'),"ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C"=>array(50,'/'),"Osc_RPM_Lmt2_C"=>array(50,'/'),"Osc_RPM_Lmt3_C"=>array(50,'/'),
					"Osc_RPM_Lmt4_C"=>array(50,'/'),"CRP_RPM_PkLmt_C"=>array(50,'/'),"CRP_Type_C"=>array(1000,'/'),"CRP_MaxLmt3_C"=>array(500,'/'),
					"CRP_MaxLmt1_C"=>array(500,'/'),"Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C"=>array(50,'/'),"RPM_Idle_Ld1_C"=>array(50,'/'),"RPM_Idle_Ld2_C"=>array(50,'/'),"MAP_RPMOffset_PkLmt_C",
					"Max_Air_Trq_RPM_C"=>array(50,'/'),"MAP_Air_RPM_LwrLmt_C"=>array(50,'/'),"MAP_Air_RPM_PkLmt_C"=>array(50,'/'),"MAP_Air_EloadOffset_PkLmt_C","MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C","Time_Upld_Frq_C"=>array(3,'/'),"MAP_Type_C","IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_20_40_C"=>array(100,'*'),
					"Fact_Fuel_Run_ELoad_40_60_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_60_80_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_80_100_C"=>array(100,'*'),"Fact_Fuel_Idle_ELoad1_C"=>array(100,'*'),"Fact_Fuel_Idle_ELoad2_C"=>array(100,'*'),"Ovr_Run_FuelFact_C"=>array(100,'*'),
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C"=>array(10000,'*'),"gr_6_low_C"=>array(10000,'*'),"gr_6_high_C"=>array(10000,'*'),
					"Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C","Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C",
					"Gr_RPM_Col6_C","Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C",
					"Thr_RPM_Col4_C","Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C",
					"Thr_Pos_Row2_C","Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C","Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C"=>array(20,'*'),"Spd_Chng_Neg_Row4_C"=>array(20,'*'),
					"Spd_Chng_Neg_Row3_C"=>array(20,'*'),"Spd_Chng_Neg_Row2_C"=>array(20,'*'),"Spd_Chng_Neg_Row1_C"=>array(20,'*'),
					"Spd_Chng_Row0_C"=>array(20,'*'),"Spd_Chng_Pos_Row1_C"=>array(20,'*'),"Spd_Chng_Pos_Row2_C"=>array(20,'*'),
					"Spd_Chng_Pos_Row3_C"=>array(20,'*'),"Spd_Chng_Pos_Row4_C"=>array(20,'*'),"Spd_Chng_Pos_Row5_C"=>array(20,'*'),"Ovr_Spd_Lmt_C",
					"Brake_EngSt3_Acc_C"=>array(20,'*'),"Brake_EngSt4_Acc_C"=>array(20,'*'),"Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C",
					"Reserved9","Reserved10","Reserved11","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$dat        = array('Yes'=>'1', 'No' => '0', 'Direct' => '1', 'Indirect' => '0', 'Absolute'=>'1', 'Differential'=>'0');
				//Eng_type
				$arrayData['success'] = TRUE;
				$arrayDataM = array();
				array_push($arrayDataM,$vType);

				$data1   = Fetch::getCaliPramVal($deviceID);

				$dataArr = $data1['calval'];
				$dataArr = json_decode($dataArr, TRUE);
				// print_r();
				$count = count($list);
				for($i = 0; $i<count($list); $i++)
				{

					$val   = trim($list[$i]);
					// print_r($val);die;
					$data  = $dataArr['Eng_eff_C'];
					$data  = (isset($dat[trim($data)])) ? $dat[trim($data)] : $data ;
					$data  = ($data == "") ? 0 : $data ;

					if(isset($listVal[$list[$i]]) && is_array($listVal[$list[$i]]) )
					{
						$calC = $listVal[$list[$i]];
						if($calC[1] == "*")
						{
							$data = $data * $calC[0];
						}
						elseif($calC[1] == "/")
						{
							$data = round($data / $calC[0],0);
						}
					}

					// Encyption Logic for Config data
					if(($i == 82) || ($i == 83))
					{
						if($i == 82)
						{
							$data = 32; // Value 32 says the array is encrypted
						}
						elseif($i == 83)
						{
							$data = $encryptKey;
						}
					}
					else
					{
						if(($data == 0))// || ($data >= 255))
						{
							// Dont Encrypt if the value is Zero
							$data = 0;
						}
						else
						{
							$data = (($data + 26) ^ $encryptKey);
							// $data = $data;
						}
					}
					array_push($arrayDataM,$data);
				}
				$arrayData['data'] = array_values($arrayDataM);
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
		    $arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData,JSON_NUMERIC_CHECK);	
    }

    public function get_device_config_ERM(Request $request)
    {
 		if(!empty($request->data))
        {
         	// Valid Bearer Key Found.
         	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			// Get Device Data
			$devInfo = Fetch::getdevice($deviceID);
			$did     = $devInfo[0]->id;
			
			// Get Vehicle details
			$vehi  = Fetch::getVehicleById($did);
			$vType = $vehi->fuel_tpe;
			$encryptKey = rand(25, 300);
			if(!empty($devInfo))
			{
				$yVal = 0;
				$list = array("Eng_eff_C","Eng_Disp_C","TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C","Eff_RPM_Limit2_C","Eff_RPM_Limit3_C","Eff_RPM_Limit4_C","Eff_RPM_Limit5_C",
					"Eqratio_Eload_1_10_RPMLimit1_C","Eqratio_Eload_1_10_RPMLimit2_C","Eqratio_Eload_1_10_RPMLimit3_C","Eqratio_Eload_1_10_RPMLimit4_C",
					"Eqratio_Eload_1_10_RPMLimit5_C","Eqratio_Eload_11_20_RPMLimit1_C","Eqratio_Eload_11_20_RPMLimit2_C","Eqratio_Eload_11_20_RPMLimit3_C",
					"Eqratio_Eload_11_20_RPMLimit4_C","Eqratio_Eload_11_20_RPMLimit5_C","Eqratio_Eload_21_30_RPMLimit1_C","Eqratio_Eload_21_30_RPMLimit2_C",
					"Eqratio_Eload_21_30_RPMLimit3_C","Eqratio_Eload_21_30_RPMLimit4_C","Eqratio_Eload_21_30_RPMLimit5_C","Eqratio_Eload_31_40_RPMLimit1_C",
					"Eqratio_Eload_31_40_RPMLimit2_C","Eqratio_Eload_31_40_RPMLimit3_C","Eqratio_Eload_31_40_RPMLimit4_C","Eqratio_Eload_31_40_RPMLimit5_C",
					"Eqratio_Eload_41_50_RPMLimit1_C","Eqratio_Eload_41_50_RPMLimit2_C","Eqratio_Eload_41_50_RPMLimit3_C","Eqratio_Eload_41_50_RPMLimit4_C",
					"Eqratio_Eload_41_50_RPMLimit5_C","Eqratio_Eload_51_60_RPMLimit1_C","Eqratio_Eload_51_60_RPMLimit2_C","Eqratio_Eload_51_60_RPMLimit3_C",
					"Eqratio_Eload_51_60_RPMLimit4_C","Eqratio_Eload_51_60_RPMLimit5_C","Eqratio_Eload_61_80_RPMLimit1_C","Eqratio_Eload_61_80_RPMLimit2_C",
					"Eqratio_Eload_61_80_RPMLimit3_C","Eqratio_Eload_61_80_RPMLimit4_C","Eqratio_Eload_61_80_RPMLimit5_C","Eqratio_Eload_81_90_RPMLimit1_C",
					"Eqratio_Eload_81_90_RPMLimit2_C","Eqratio_Eload_81_90_RPMLimit3_C","Eqratio_Eload_81_90_RPMLimit4_C","Eqratio_Eload_81_90_RPMLimit5_C",
					"Eqratio_Eload_91_94_RPMLimit1_C","Eqratio_Eload_91_94_RPMLimit2_C","Eqratio_Eload_91_94_RPMLimit3_C","Eqratio_Eload_91_94_RPMLimit4_C",
					"Eqratio_Eload_91_94_RPMLimit5_C","Eqratio_Eload_95_98_RPMLimit1_C","Eqratio_Eload_95_98_RPMLimit2_C","Eqratio_Eload_95_98_RPMLimit3_C",
					"Eqratio_Eload_95_98_RPMLimit4_C","Eqratio_Eload_95_98_RPMLimit5_C","Eqratio_Eload_99_100_RPMLimit1_C","Eqratio_Eload_99_100_RPMLimit2_C","Eqratio_Eload_99_100_RPMLimit3_C","Eqratio_Eload_99_100_RPMLimit4_C","Eqratio_Eload_99_100_RPMLimit5_C","Ovr_Run_EqRatio_C",
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C",
					"VolEff_Eload_31_40_RPMLimit4_C","VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C",
					"VolEff_Eload_41_50_RPMLimit3_C","VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C",
					"VolEff_Eload_51_60_RPMLimit2_C","VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C",
					"VolEff_Eload_61_80_RPMLimit1_C","VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C",
					"VolEff_Eload_61_80_RPMLimit5_C","VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C",
					"VolEff_Eload_81_90_RPMLimit4_C","VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C",
					"VolEff_Eload_91_94_RPMLimit3_C","VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C",
					"VolEff_Eload_95_98_RPMLimit2_C","VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C",
					"VolEff_Eload_99_100_RPMLimit1_C","VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C",
					"VolEff_Eload_99_100_RPMLimit5_C","Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C",
					"Accl_Eload_C","Decl_Val_C","Eload_Def_C","Eload_Max_C","Max_Torque_C","ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C","Osc_RPM_Lmt2_C","Osc_RPM_Lmt3_C","Osc_RPM_Lmt4_C","CRP_RPM_PkLmt_C","CRP_Type_C",
					"CRP_MaxLmt3_C","CRP_MaxLmt1_C","Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C","RPM_Idle_Ld1_C","RPM_Idle_Ld2_C","MAP_RPMOffset_PkLmt_C","Max_Air_Trq_RPM_C","MAP_Air_RPM_LwrLmt_C",
					"MAP_Air_RPM_PkLmt_C","MAP_Air_EloadOffset_PkLmt_C","MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C",
					"Time_Upld_Frq_C","MAP_Type_C","IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C","Fact_Fuel_Run_ELoad_20_40_C","Fact_Fuel_Run_ELoad_40_60_C",
					"Fact_Fuel_Run_ELoad_60_80_C","Fact_Fuel_Run_ELoad_80_100_C","Fact_Fuel_Idle_ELoad1_C","Fact_Fuel_Idle_ELoad2_C","Ovr_Run_FuelFact_C",
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C","gr_6_low_C","gr_6_high_C","Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C",
					"Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C","Gr_RPM_Col6_C",
					"Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C","Thr_RPM_Col4_C",
					"Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C","Thr_Pos_Row2_C",
					"Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C",
					"Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C","Spd_Chng_Neg_Row4_C","Spd_Chng_Neg_Row3_C","Spd_Chng_Neg_Row2_C",
					"Spd_Chng_Neg_Row1_C","Spd_Chng_Row0_C","Spd_Chng_Pos_Row1_C","Spd_Chng_Pos_Row2_C","Spd_Chng_Pos_Row3_C","Spd_Chng_Pos_Row4_C",
					"Spd_Chng_Pos_Row5_C","Ovr_Spd_Lmt_C","Brake_EngSt3_Acc_C","Brake_EngSt4_Acc_C","Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C",
					"Reserved9","Reserved10","Reserved11","Reserved12","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$listVal = array("Eng_eff_C","Eng_Disp_C"=>array(100,'*'),"TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C"=>array(50,'/'),
					"Eff_RPM_Limit2_C"=>array(50,'/'),"Eff_RPM_Limit3_C"=>array(50,'/'),"Eff_RPM_Limit4_C"=>array(50,'/'),"Eff_RPM_Limit5_C"=>array(50,'/'),
					"Eqratio_Eload_1_10_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_1_10_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_21_30_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_51_60_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_91_94_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit5_C"=>array(200,'*'),"Ovr_Run_EqRatio_C"=>array(100,'*'),
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C","VolEff_Eload_31_40_RPMLimit4_C",
					"VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C","VolEff_Eload_41_50_RPMLimit3_C",
					"VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C","VolEff_Eload_51_60_RPMLimit2_C",
					"VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C","VolEff_Eload_61_80_RPMLimit1_C",
					"VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C","VolEff_Eload_61_80_RPMLimit5_C",
					"VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C","VolEff_Eload_81_90_RPMLimit4_C",
					"VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C","VolEff_Eload_91_94_RPMLimit3_C",
					"VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C","VolEff_Eload_95_98_RPMLimit2_C",
					"VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C","VolEff_Eload_99_100_RPMLimit1_C",
					"VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C","VolEff_Eload_99_100_RPMLimit5_C",
					"Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C"=>array(20,'*'),"Accl_Eload_C",
					"Decl_Val_C"=>array(20,'*'),"Eload_Def_C","Eload_Max_C","Max_Torque_C"=>array(5,'/'),"ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C"=>array(50,'/'),"Osc_RPM_Lmt2_C"=>array(50,'/'),"Osc_RPM_Lmt3_C"=>array(50,'/'),
					"Osc_RPM_Lmt4_C"=>array(50,'/'),"CRP_RPM_PkLmt_C"=>array(50,'/'),"CRP_Type_C"=>array(1000,'/'),"CRP_MaxLmt3_C"=>array(500,'/'),
					"CRP_MaxLmt1_C"=>array(500,'/'),"Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C"=>array(50,'/'),"RPM_Idle_Ld1_C"=>array(50,'/'),"RPM_Idle_Ld2_C"=>array(50,'/'),"MAP_RPMOffset_PkLmt_C",
					"Max_Air_Trq_RPM_C"=>array(50,'/'),"MAP_Air_RPM_LwrLmt_C"=>array(50,'/'),"MAP_Air_RPM_PkLmt_C"=>array(50,'/'),"MAP_Air_EloadOffset_PkLmt_C",
					"MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C","Time_Upld_Frq_C"=>array(3,'/'),"MAP_Type_C",
					"IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_20_40_C"=>array(100,'*'),
					"Fact_Fuel_Run_ELoad_40_60_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_60_80_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_80_100_C"=>array(100,'*'),
					"Fact_Fuel_Idle_ELoad1_C"=>array(100,'*'),"Fact_Fuel_Idle_ELoad2_C"=>array(100,'*'),"Ovr_Run_FuelFact_C"=>array(100,'*'),
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C"=>array(10000,'*'),"gr_6_low_C"=>array(10000,'*'),"gr_6_high_C"=>array(10000,'*'),
					"Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C","Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C",
					"Gr_RPM_Col6_C","Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C",
					"Thr_RPM_Col4_C","Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C",
					"Thr_Pos_Row2_C","Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C","Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C"=>array(20,'*'),"Spd_Chng_Neg_Row4_C"=>array(20,'*'),
					"Spd_Chng_Neg_Row3_C"=>array(20,'*'),"Spd_Chng_Neg_Row2_C"=>array(20,'*'),"Spd_Chng_Neg_Row1_C"=>array(20,'*'),
					"Spd_Chng_Row0_C"=>array(20,'*'),"Spd_Chng_Pos_Row1_C"=>array(20,'*'),"Spd_Chng_Pos_Row2_C"=>array(20,'*'),
					"Spd_Chng_Pos_Row3_C"=>array(20,'*'),"Spd_Chng_Pos_Row4_C"=>array(20,'*'),"Spd_Chng_Pos_Row5_C"=>array(20,'*'),"Ovr_Spd_Lmt_C",
					"Brake_EngSt3_Acc_C"=>array(20,'*'),"Brake_EngSt4_Acc_C"=>array(20,'*'),"Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C",
					"Reserved9","Reserved10","Reserved11","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$dat        = array('Yes'=>'1', 'No' => '0', 'Direct' => '1', 'Indirect' => '0', 'Absolute'=>'1', 'Differential'=>'0');
				//Eng_type
				$arrayData['success'] = TRUE;
				$arrayDataM = array();
				array_push($arrayDataM,$vType);

				$data1   = Fetch::getCaliPramVal($deviceID);

				$dataArr = $data1['calval'];
				$dataArr = json_decode($dataArr, TRUE);
				// print_r();
				$count = count($list);
				for($i = 0; $i<count($list); $i++)
				{

					$val   = trim($list[$i]);
					// print_r($val);die;
					$data  = $dataArr['Eng_eff_C'];
					$data  = (isset($dat[trim($data)])) ? $dat[trim($data)] : $data ;
					$data  = ($data == "") ? 0 : $data ;

					if(isset($listVal[$list[$i]]) && is_array($listVal[$list[$i]]) )
					{
						$calC = $listVal[$list[$i]];
						if($calC[1] == "*")
						{
							$data = $data * $calC[0];
						}
						elseif($calC[1] == "/")
						{
							$data = round($data / $calC[0],0);
						}
					}

					// Encyption Logic for Config data
					if(($i == 82) || ($i == 83))
					{
						if($i == 82)
						{
							$data = 32; // Value 32 says the array is encrypted
						}
						elseif($i == 83)
						{
							$data = $encryptKey;
						}
					}
					else
					{
						if(($data == 0))// || ($data >= 255))
						{
							// Dont Encrypt if the value is Zero
							$data = 0;
						}
						else
						{
							$data = (($data + 26) ^ $encryptKey);
							// $data = $data;
						}
					}
					array_push($arrayDataM,$data);
				}
				$arrayData['data'] = array_values($arrayDataM);
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}

        }
        else
		{
		    $arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData,JSON_NUMERIC_CHECK);
    }

    public function get_device_config_w2(Request $request)
    {
 		if(!empty($request->data))
        {
         	// Valid Bearer Key Found.
         	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			// print_r($obj);die;
			// Get Device Data
			$devInfo = Fetch::getdevice($deviceID);
			// print_r($devInfo);die;
			$did     = $devInfo[0]->id;
			
			// Get Vehicle details
			$vehi  = Fetch::getVehicleById($did);
			$vType = $vehi->fuel_tpe;
			$encryptKey = rand(25, 300);
			if(!empty($devInfo))
			{
				$yVal = 0;
				$list = array("Eng_eff_C","Eng_Disp_C","TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C","Eff_RPM_Limit2_C","Eff_RPM_Limit3_C","Eff_RPM_Limit4_C","Eff_RPM_Limit5_C",
					"Eqratio_Eload_1_10_RPMLimit1_C","Eqratio_Eload_1_10_RPMLimit2_C","Eqratio_Eload_1_10_RPMLimit3_C","Eqratio_Eload_1_10_RPMLimit4_C",
					"Eqratio_Eload_1_10_RPMLimit5_C","Eqratio_Eload_11_20_RPMLimit1_C","Eqratio_Eload_11_20_RPMLimit2_C","Eqratio_Eload_11_20_RPMLimit3_C",
					"Eqratio_Eload_11_20_RPMLimit4_C","Eqratio_Eload_11_20_RPMLimit5_C","Eqratio_Eload_21_30_RPMLimit1_C","Eqratio_Eload_21_30_RPMLimit2_C",
					"Eqratio_Eload_21_30_RPMLimit3_C","Eqratio_Eload_21_30_RPMLimit4_C","Eqratio_Eload_21_30_RPMLimit5_C","Eqratio_Eload_31_40_RPMLimit1_C",
					"Eqratio_Eload_31_40_RPMLimit2_C","Eqratio_Eload_31_40_RPMLimit3_C","Eqratio_Eload_31_40_RPMLimit4_C","Eqratio_Eload_31_40_RPMLimit5_C",
					"Eqratio_Eload_41_50_RPMLimit1_C","Eqratio_Eload_41_50_RPMLimit2_C","Eqratio_Eload_41_50_RPMLimit3_C","Eqratio_Eload_41_50_RPMLimit4_C",
					"Eqratio_Eload_41_50_RPMLimit5_C","Eqratio_Eload_51_60_RPMLimit1_C","Eqratio_Eload_51_60_RPMLimit2_C","Eqratio_Eload_51_60_RPMLimit3_C",
					"Eqratio_Eload_51_60_RPMLimit4_C","Eqratio_Eload_51_60_RPMLimit5_C","Eqratio_Eload_61_80_RPMLimit1_C","Eqratio_Eload_61_80_RPMLimit2_C",
					"Eqratio_Eload_61_80_RPMLimit3_C","Eqratio_Eload_61_80_RPMLimit4_C","Eqratio_Eload_61_80_RPMLimit5_C","Eqratio_Eload_81_90_RPMLimit1_C",
					"Eqratio_Eload_81_90_RPMLimit2_C","Eqratio_Eload_81_90_RPMLimit3_C","Eqratio_Eload_81_90_RPMLimit4_C","Eqratio_Eload_81_90_RPMLimit5_C",
					"Eqratio_Eload_91_94_RPMLimit1_C","Eqratio_Eload_91_94_RPMLimit2_C","Eqratio_Eload_91_94_RPMLimit3_C","Eqratio_Eload_91_94_RPMLimit4_C",
					"Eqratio_Eload_91_94_RPMLimit5_C","Eqratio_Eload_95_98_RPMLimit1_C","Eqratio_Eload_95_98_RPMLimit2_C","Eqratio_Eload_95_98_RPMLimit3_C",
					"Eqratio_Eload_95_98_RPMLimit4_C","Eqratio_Eload_95_98_RPMLimit5_C","Eqratio_Eload_99_100_RPMLimit1_C","Eqratio_Eload_99_100_RPMLimit2_C","Eqratio_Eload_99_100_RPMLimit3_C","Eqratio_Eload_99_100_RPMLimit4_C","Eqratio_Eload_99_100_RPMLimit5_C","Ovr_Run_EqRatio_C",
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C",
					"VolEff_Eload_31_40_RPMLimit4_C","VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C",
					"VolEff_Eload_41_50_RPMLimit3_C","VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C",
					"VolEff_Eload_51_60_RPMLimit2_C","VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C",
					"VolEff_Eload_61_80_RPMLimit1_C","VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C",
					"VolEff_Eload_61_80_RPMLimit5_C","VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C",
					"VolEff_Eload_81_90_RPMLimit4_C","VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C",
					"VolEff_Eload_91_94_RPMLimit3_C","VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C",
					"VolEff_Eload_95_98_RPMLimit2_C","VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C",
					"VolEff_Eload_99_100_RPMLimit1_C","VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C",
					"VolEff_Eload_99_100_RPMLimit5_C","Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C",
					"Accl_Eload_C","Decl_Val_C","Eload_Def_C","Eload_Max_C","Max_Torque_C","ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C","Osc_RPM_Lmt2_C","Osc_RPM_Lmt3_C","Osc_RPM_Lmt4_C","CRP_RPM_PkLmt_C","CRP_Type_C",
					"CRP_MaxLmt3_C","CRP_MaxLmt1_C","Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C","RPM_Idle_Ld1_C","RPM_Idle_Ld2_C","MAP_RPMOffset_PkLmt_C","Max_Air_Trq_RPM_C","MAP_Air_RPM_LwrLmt_C",
					"MAP_Air_RPM_PkLmt_C","MAP_Air_EloadOffset_PkLmt_C","MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C",
					"Time_Upld_Frq_C","MAP_Type_C","IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C","Fact_Fuel_Run_ELoad_20_40_C","Fact_Fuel_Run_ELoad_40_60_C",
					"Fact_Fuel_Run_ELoad_60_80_C","Fact_Fuel_Run_ELoad_80_100_C","Fact_Fuel_Idle_ELoad1_C","Fact_Fuel_Idle_ELoad2_C","Ovr_Run_FuelFact_C",
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C","gr_6_low_C","gr_6_high_C","Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C",
					"Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C","Gr_RPM_Col6_C",
					"Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C","Thr_RPM_Col4_C",
					"Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C","Thr_Pos_Row2_C",
					"Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C",
					"Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C","Spd_Chng_Neg_Row4_C","Spd_Chng_Neg_Row3_C","Spd_Chng_Neg_Row2_C",
					"Spd_Chng_Neg_Row1_C","Spd_Chng_Row0_C","Spd_Chng_Pos_Row1_C","Spd_Chng_Pos_Row2_C","Spd_Chng_Pos_Row3_C","Spd_Chng_Pos_Row4_C",
					"Spd_Chng_Pos_Row5_C","Ovr_Spd_Lmt_C","Brake_EngSt3_Acc_C","Brake_EngSt4_Acc_C","Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C",
					"Reserved9","Reserved10","Reserved11","Reserved12","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$listVal = array("Eng_eff_C","Eng_Disp_C"=>array(100,'*'),"TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C"=>array(50,'/'),
					"Eff_RPM_Limit2_C"=>array(50,'/'),"Eff_RPM_Limit3_C"=>array(50,'/'),"Eff_RPM_Limit4_C"=>array(50,'/'),"Eff_RPM_Limit5_C"=>array(50,'/'),
					"Eqratio_Eload_1_10_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_1_10_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_21_30_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_51_60_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_91_94_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit5_C"=>array(200,'*'),"Ovr_Run_EqRatio_C"=>array(100,'*'),
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C","VolEff_Eload_31_40_RPMLimit4_C",
					"VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C","VolEff_Eload_41_50_RPMLimit3_C",
					"VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C","VolEff_Eload_51_60_RPMLimit2_C",
					"VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C","VolEff_Eload_61_80_RPMLimit1_C",
					"VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C","VolEff_Eload_61_80_RPMLimit5_C",
					"VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C","VolEff_Eload_81_90_RPMLimit4_C",
					"VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C","VolEff_Eload_91_94_RPMLimit3_C",
					"VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C","VolEff_Eload_95_98_RPMLimit2_C",
					"VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C","VolEff_Eload_99_100_RPMLimit1_C",
					"VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C","VolEff_Eload_99_100_RPMLimit5_C",
					"Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C"=>array(20,'*'),"Accl_Eload_C",
					"Decl_Val_C"=>array(20,'*'),"Eload_Def_C","Eload_Max_C","Max_Torque_C"=>array(5,'/'),"ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C"=>array(50,'/'),"Osc_RPM_Lmt2_C"=>array(50,'/'),"Osc_RPM_Lmt3_C"=>array(50,'/'),
					"Osc_RPM_Lmt4_C"=>array(50,'/'),"CRP_RPM_PkLmt_C"=>array(50,'/'),"CRP_Type_C"=>array(1000,'/'),"CRP_MaxLmt3_C"=>array(500,'/'),
					"CRP_MaxLmt1_C"=>array(500,'/'),"Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C"=>array(50,'/'),"RPM_Idle_Ld1_C"=>array(50,'/'),"RPM_Idle_Ld2_C"=>array(50,'/'),"MAP_RPMOffset_PkLmt_C",
					"Max_Air_Trq_RPM_C"=>array(50,'/'),"MAP_Air_RPM_LwrLmt_C"=>array(50,'/'),"MAP_Air_RPM_PkLmt_C"=>array(50,'/'),"MAP_Air_EloadOffset_PkLmt_C",
					"MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C","Time_Upld_Frq_C"=>array(3,'/'),"MAP_Type_C",
					"IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_20_40_C"=>array(100,'*'),
					"Fact_Fuel_Run_ELoad_40_60_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_60_80_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_80_100_C"=>array(100,'*'),
					"Fact_Fuel_Idle_ELoad1_C"=>array(100,'*'),"Fact_Fuel_Idle_ELoad2_C"=>array(100,'*'),"Ovr_Run_FuelFact_C"=>array(100,'*'),
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C"=>array(10000,'*'),"gr_6_low_C"=>array(10000,'*'),"gr_6_high_C"=>array(10000,'*'),
					"Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C","Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C",
					"Gr_RPM_Col6_C","Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C",
					"Thr_RPM_Col4_C","Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C",
					"Thr_Pos_Row2_C","Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C","Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C"=>array(20,'*'),"Spd_Chng_Neg_Row4_C"=>array(20,'*'),
					"Spd_Chng_Neg_Row3_C"=>array(20,'*'),"Spd_Chng_Neg_Row2_C"=>array(20,'*'),"Spd_Chng_Neg_Row1_C"=>array(20,'*'),
					"Spd_Chng_Row0_C"=>array(20,'*'),"Spd_Chng_Pos_Row1_C"=>array(20,'*'),"Spd_Chng_Pos_Row2_C"=>array(20,'*'),
					"Spd_Chng_Pos_Row3_C"=>array(20,'*'),"Spd_Chng_Pos_Row4_C"=>array(20,'*'),"Spd_Chng_Pos_Row5_C"=>array(20,'*'),"Ovr_Spd_Lmt_C",
					"Brake_EngSt3_Acc_C"=>array(20,'*'),"Brake_EngSt4_Acc_C"=>array(20,'*'),"Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C",
					"Reserved9","Reserved10","Reserved11","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$dat        = array('Yes'=>'1', 'No' => '0', 'Direct' => '1', 'Indirect' => '0', 'Absolute'=>'1', 'Differential'=>'0');
				//Eng_type
				$arrayData['success'] = TRUE;
				$arrayDataM = array();
				array_push($arrayDataM,$vType);

				$data1   = Fetch::getCaliPramVal($deviceID);

				$dataArr = $data1['calval'];
				$dataArr = json_decode($dataArr, TRUE);
				// print_r();
				$count = count($list);
				for($i = 0; $i<count($list); $i++)
				{
					$val   = trim($list[$i]);
					// print_r($val);die;
					$data  = $dataArr['Eng_eff_C'];
					$data  = (isset($dat[trim($data)])) ? $dat[trim($data)] : $data ;
					$data  = ($data == "") ? 0 : $data ;

					if(isset($listVal[$list[$i]]) && is_array($listVal[$list[$i]]) )
					{
						$calC = $listVal[$list[$i]];
						if($calC[1] == "*")
						{
							$data = $data * $calC[0];
						}
						elseif($calC[1] == "/")
						{
							$data = round($data / $calC[0],0);
						}
					}

					// Encyption Logic for Config data
					if(($i == 82) || ($i == 83))
					{
						if($i == 82)
						{
							$data = 32; // Value 32 says the array is encrypted
						}
						elseif($i == 83)
						{
							$data = $encryptKey;
						}
					}
					else
					{
						if(($data == 0))// || ($data >= 255))
						{
							// Dont Encrypt if the value is Zero
							$data = 0;
						}
						else
						{
							$data = (($data + 26) ^ $encryptKey);
							// $data = $data;
						}
					}
					array_push($arrayDataM,$data);
				}
				$arrayData['data'] = array_values($arrayDataM);
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
		    $arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData,JSON_NUMERIC_CHECK);
    }

	public function set_device_dtc(Request $request)
	{
 		if(!empty($request->data))
        {
           	$obj      = json_decode($json,true);	
			$deviceID = $obj['devID'];
    			
		    // get Device Data
		    $devInfo = Fetch::getdevice($deviceID);
		    if(!empty($devInfo))
		    {
		        $dtc = $array['dtcData'];
				if($dtc != "")
				{
					$dtc = 1;
				}
				else
				{
					$dtc = 0;
				}
				$dtcv = $array['dtcData'];
		        // To update the same row in the database
		        $resultC = Fetch::updatedeviceDtcv($deviceID,$dtcv,$dtc);
		        if($resultC)
		        {
		            // Data Uploaded successfully
		            $arrayData['success'] = TRUE;
		            $arrayData['dtc']     = "DTC Updated!";
		        }
		        else
		        {
		            // Data failed to update
		            $arrayData['success']      = FALSE;
		            $arrayData['errorCode']    = "1003";
		            $arrayData['errorMessage'] = "DTC Update Failed!";
		        }
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
	}

	public function devrawdataupload(Request $request)
	{
 		if(!empty(file_get_contents("php://input")))
        {    
       		$datMe       = file_get_contents("php://input");
     		$array       = json_decode($datMe, TRUE);
       		$logFilePath = getcwd();
			
       		
			$deviceID = $array['devID'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
		    {
	    	    $pathInfo = getcwd();
				// echo "datme = $datMe \n";
				$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/devrawdataupload4w_back.php '$datMe'";
				$cmd.=" ";
				$cmd.="'$datMe'";

				exec($cmd . " > /dev/null &");
				// print_r($cmd);die;
				
				$arrayData['success'] = TRUE;
				$arrayData['status']  = "File Uploaded and database updating!";
		    }
		    else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
			// http_response_code(400);
		}
		echo json_encode($arrayData);
	}

	public function devrawdataupload_2w(Request $request)
	{
 		if(!empty(file_get_contents("php://input")))
        {    
       		$datMe       = file_get_contents("php://input");
     		$array       = json_decode($datMe, TRUE);
       		$logFilePath = getcwd();
			
       		
			$deviceID = $array['devID'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
		    {
		    	    $pathInfo = getcwd();
					$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/devrawdataupload2w_back.php '$datMe'";
					$cmd.=" ";
					$cmd.="'$datMe'";
					exec($cmd . " > /dev/null &");
					// print_r($cmd);die;
					
					$arrayData['success'] = TRUE;
					$arrayData['status']  = "File Uploaded and database updating!";
		    }
		    else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
			// http_response_code(400);
		}
		echo json_encode($arrayData);
	}

	public function devdataupload(Request $request)
	{
 		if(!empty(file_get_contents("php://input")))
        {    
       		$datMe = file_get_contents("php://input");
     		$array     = json_decode($datMe, TRUE);
       		$logFilePath = getcwd();
       		
			$deviceID = $array['devID'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
		    {
	    	    $pathInfo = getcwd();
				// echo "datme = $datMe \n";
				$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-res4w.php '$datMe'";
				$cmd.=" ";
				$cmd.="'$datMe'";

				exec($cmd . " > /dev/null &");
				// print_r($cmd);die;
				
				$arrayData['success'] = TRUE;
				$arrayData['status']  = "File Uploaded and database updating!";
		    }
		    else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
			// http_response_code(400);
		}
		echo json_encode($arrayData);
	}

	public function devdataupload2w(Request $request)
	{
 		if(!empty(file_get_contents("php://input")))
        {    
       		$datMe       = file_get_contents("php://input");
     		$array       = json_decode($datMe, TRUE);
       		$logFilePath = getcwd();
			
			$deviceID = $array['devID'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
		    {
	    	    $pathInfo = getcwd();
				// echo "datme = $datMe \n";
				$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-res2w.php '$datMe'";
				$cmd.=" ";
				$cmd.="'$datMe'";

				exec($cmd . " > /dev/null &");
				// print_r($cmd);die;
				
				$arrayData['success'] = TRUE;
					$arrayData['status']  = "File Uploaded and database updating!";
		    }
		    else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
			// http_response_code(400);
		}
		echo json_encode($arrayData);
	}

	public function devdataupload2w_tvs(Request $request)
	{
 		if(!empty(file_get_contents("php://input")))
        {    
        	$datMe       = file_get_contents("php://input");
        	$array       = json_decode($datMe, TRUE);
           	$logFilePath = getcwd();
           		
			$deviceID = $array['devID'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
		    {
	    	    $pathInfo = getcwd();
				// echo "datme = $datMe \n";
				$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-res2w_tvs.php '$datMe'";
				$cmd.=" ";
				$cmd.="'$datMe'";
				exec($cmd . " > /dev/null &");
				// print_r($cmd);die;
				
				$arrayData['success'] = TRUE;
				$arrayData['status']  = "File Uploaded and database updating!";
		    }
		    else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
			// http_response_code(400);
		}
		echo json_encode($arrayData);
	}

	public function devdatauploadERM(Request $request)
	{
 		if(!empty(file_get_contents("php://input")))
        {    
       		$datMe       = file_get_contents("php://input");
     		$array       = json_decode($datMe, TRUE);
   			// $logFilePath = getcwd();
			// $req_dump    = print_r($datMe, TRUE);
			// $filePath    = $logFilePath. "/"."request.txt";
			
			// $fp = fopen($filePath, 'a');
			// fwrite($fp, "\n---------------- devdatauploadERM - Request Begin --------------------\n");
			// $currTimeStamp = date('Y-m-d H:i:s',time());
			// fwrite($fp, $currTimeStamp);
			// fwrite($fp, "\n");
			// fwrite($fp, $req_dump);
			// fwrite($fp, "\n----------------- devdatauploadERM - Request End --------------------\n");
			// fclose($fp);
       		
			$deviceID = $array['devID'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
		    {
	    	    $pathInfo = getcwd();
				// echo "datme = $datMe \n";
				$cmd = "/usr/bin/timeout 30m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-resERM.php";
				$cmd.=" ";
				$cmd.="'$datMe'";
				exec($cmd . " > /dev/null &");
				// print_r($cmd);die;
				
				$arrayData['success'] = TRUE;
				$arrayData['status']  = "Data Updating!";
		    }
		    else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
			// http_response_code(400);
		}
		echo json_encode($arrayData);
	}

	public function devdatauploadERM_1(Request $request)
	{
 		if(!empty(file_get_contents("php://input")))
        {    
       		$datMe = file_get_contents("php://input");
     		$array = json_decode($datMe, TRUE);
  			// $logFilePath = getcwd();
			// $req_dump = print_r($datMe, TRUE);
			// $filePath = $logFilePath. "/"."request.txt";
			// // print_r($filePath);die;
			// $fp = fopen($filePath, 'a');
			// fwrite($fp, "\n---------------- devdatauploadERM - Request Begin --------------------\n");
			// $currTimeStamp = date('Y-m-d H:i:s',time());
			// fwrite($fp, $currTimeStamp);
			// fwrite($fp, "\n");
			// fwrite($fp, $req_dump);
			// fwrite($fp, "\n----------------- devdatauploadERM - Request End --------------------\n");
			// fclose($fp);
       		
			$deviceID = $array['devID'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
		    {
	    	    $pathInfo = getcwd();
				// echo "datme = $datMe \n";
				$cmd = "/usr/bin/timeout 30m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-resERM.php";
				$cmd.=" ";
				$cmd.="'$datMe'";
				exec($cmd . " > /dev/null &");
				// print_r($cmd);die;
				
				$arrayData['success'] = TRUE;
				$arrayData['status']  = "Data Updating!";
		    }
		    else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
			// http_response_code(400);
		}
		echo json_encode($arrayData);
	}
*/




}
?>