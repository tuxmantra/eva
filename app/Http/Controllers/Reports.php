<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use PDF;

class Reports extends Controller
{
    public function index()
    {
        $user = session('userdata');
        $data['user_type'] = $user['utype'];
        $data['title'] = 'Reports';
        $data['user_category'] = $user['ucategory'];
        $data['reports'] = Fetch::getRepots(session('userdata')['uid'],session('userdata')['ucategory']);
        // print_r( $data['reports']);die;
        $data['repo_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        return view('reports/index')->with($data);
    }

   public function generateMoniteringReport($id, $did)
    {
        ob_clean();
        header("Content-type:application/pdf");
        $report_id = request('id');
        // echo $report_id;die;
        $data['testData'] = Fetch::getTestRepostById($report_id);
        $pdf = PDF::loadView('reports/pdf/test', $data)->setPaper('a4', 'portrait');
        return $pdf->stream($id . '-' . $did . 'calibration-report.pdf');
    }

    public function getTvsCronReport($did, $cron_date, $status)
    {
        ob_clean();
        $report_date = "";
        $no_date = 1;
        if ($status == 1)
        {
            $cron_start = $cron_date . " 00:00:00";
            $cron_end = $cron_date . " 23:59:59";
            $diff = 1;
            $report_date = date('Y-M-d', strtotime($cron_date));
        }
        else
        {
            $dt_cron = explode(';', $cron_date);
            $cron_start = $dt_cron[0];
            $cron_end = $dt_cron[1];
            $st_art = date('Y-m-d', strtotime($cron_start));
            $e_nd = date('Y-m-d', strtotime($cron_end));

            $report_start = date('Y-M-d', strtotime($cron_start));
            $report_end = date('Y-M-d', strtotime($cron_end));

            $diff = $this->dateDiffInDays($st_art, $e_nd);
            if ($diff == 0)
            {
                $diff = 1;
            }
            $report_date = $report_start . " to " . $report_end;
        }
        // PDF Details Push Start
        $idd = Fetch::getIdByDeviceId($did); // Get Device details
        $id = $idd[0]->id;
        $vehicleData = Fetch::getVehicleDataById($id); // Get Vehicle details for the device selected
        // print_r($vehicleData);
        $vehInfo = Fetch::getVehicleInfoDataById($vehicleData->manufact);
        $vehSett = Fetch::getVehicleSettingData();

        /**********************************************************************************************************/
        /* ------------------------------------- First Section Start ------------------------------------------- */
        /**********************************************************************************************************/
        // Vehicle Model
        $vehicle_model = $vehInfo[0]->model;
        $vehicle_model = explode(";", $vehicle_model);

        $vehicle_model = $vehicle_model[$vehicleData->model];
        $data['model'] = $vehicle_model;

        // Engine Capacity
        $eng_cap = explode(';', $vehSett->engine_capacity);
        $eng_cap = $eng_cap[$vehicleData->eng_cap];
        $data['engine_cc'] = $eng_cap;
        // Engine Varient
        $varient = $vehInfo[0]->var;
        $varient = explode(";", $varient);
        $varient = $varient[$vehicleData->varient];
        $data['varient'] = $varient;

        // Vehicle Registration
        $data['registration'] = $vehicleData->reg_no;

        // Chassis Number
        $data['chassis_num'] = $vehicleData->chassis_num;

        // Engine Number
        $data['engine_num'] = $vehicleData->engine_num;

        // Date of Purchase
        $purchase_dt = $vehicleData->purchase_dt;
        $data['date_purchase'] = $purchase_dt;

        // Odometer
        $odometer = $idd[0]->total_dist;
        $data['odometer'] = round($odometer, 0);

        // Last Service
        $last_serv_dt = $vehicleData->last_serv_dt;
        $data['last_service'] = $last_serv_dt;

        /**********************************************************************************************************/
        /* --------------------------------------- First Section End ------------------------------------------- */
        /**********************************************************************************************************/

        /**********************************************************************************************************/
        /* ------------------------------------- Second Section Start ------------------------------------------- */
        /**********************************************************************************************************/

        // Get all data for various variables
        $sumDat = Fetch::getSumForCronVehicle($did, $cron_start, $cron_end);

        // Date Summary
        $data['date_summery'] = $report_date;

        // Total Distance
        $distance = !empty($sumDat[0]->distance) ? $sumDat[0]->distance : "0";
        $data['distance'] = round($distance, 2);

        // distance factor to round up to 100 km
        if ($distance != 0)
        {
            $dist_fact = 100 / $distance;
        }
        else
        {
            $dist_fact = 1;
        }

        // Total Duration of Key On
        $duration = !empty($sumDat[0]->duration) ? $sumDat[0]->duration : "0";
        $data['duration'] = $duration;

        // Total Engine Off Time
        $keyon_time = !empty($sumDat[0]->keyon_time) ? $sumDat[0]->keyon_time : "0";
        $data['keyon_time'] = $keyon_time;

        // Total running duration
        $run_dur = $duration - $keyon_time;
        $data['run_dur'] = $run_dur;

        // Mileage
        $sumData = Fetch::getrowForCronMilage($did, $cron_start, $cron_end);
        if (!empty($sumData))
        {
            $count_mil = count($sumData);
            $st = 0;
            $distsum = 0;
            foreach ($sumData as $sum)
            {
                $st = $st + ($sum->dist * $sum->mil);
                $distsum = $distsum + $sum->dist;
            }
            //Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
            $mileage = $st / $distsum;
            $mileage = round($mileage, 2);
        }
        else
        {
            $mileage = 0;
        }
        $data['milage'] = $mileage;

        /**********************************************************************************************************/
        /* -------------------------------------- Second Section End -------------------------------------------- */
        /**********************************************************************************************************/

        /**********************************************************************************************************/
        /* ------------------------------------- Third Section Start ------------------------------------------- */
        /**********************************************************************************************************/

        // Avg Distance and Maximum Distance and Minimum Distance
        // $avg_distance         = !empty($sumDat[0]->avg_distance)? $sumDat[0]->avg_distance:"0";
        // $avg_distance         = $avg_distance/$diff;
        //$max_distance     = !empty($sumDat[0]->max_distance)? $sumDat[0]->max_distance:"0";
        //$min_distance         = !empty($sumDat[0]->min_distance)? $sumDat[0]->min_distance:"0";
        // if(!empty($sumData)){
        //     $st          = 0;
        //     $dursum      = 0;
        //     foreach($sumData as $avg){
        //         $st      = $st + ($avg->dist * $avg->dur);
        //         $dursum  = $dursum + $avg->dur;
        //     }
        //     //Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
        //     $avg_distance = round($st/$dursum,2);
        // }else{
        //     $avg_distance = 0;
        // }
        // $data['avg_distance'] = $avg_distance;
        // Maximum Distance and Minimum Distance
        $day_array = array();
        $day_array_avg = 0;
        $tot_dur = 0;
        $day_distance = Fetch::getMaxMinDistanceDays($did, $cron_start, $cron_end);

        if (empty($day_distance))
        {
            $avg_distance = 0;
            $max_distance = 0;
            $min_distance = 0;
        }
        else
        {
            foreach ($day_distance as $key => $value)
            {
                $day_array[] = $value->dist;
                $day_array_avg = $day_array_avg + (($value->dist) * ($value->dur));
                $tot_dur = $tot_dur + $value->dur;
            }

            $avg_distance = round(($day_array_avg) / ($tot_dur) , 1);
            $max_distance = round(max($day_array) , 1);
            $min_distance = round(min($day_array) , 1);
        }
        $data['avg_distance'] = $avg_distance;
        $data['max_distance'] = $max_distance;
        $data['min_distance'] = $min_distance;

        // Average Speed
        // $avg_speed            = !empty($sumDat[0]->avg_speed)? $sumDat[0]->avg_speed:"0";
        if (!empty($sumData))
        {
            $st = 0;
            $dursum = 0;
            foreach ($sumData as $avg)
            {
                $st = $st + ($avg->avg_speed * $avg->dur);
                $dursum = $dursum + $avg->dur;
            }
            $avg_speed = round($st / $dursum, 0);
        }
        else
        {
            $avg_speed = 0;
        }
        $data['avg_speed'] = $avg_speed;

        // Max Speed
        $max_speed = !empty($sumDat[0]->max_speed) ? $sumDat[0]->max_speed : "0";
        $data['max_speed'] = $max_speed;

        // Standard Devation for Vehicle Speed
        $cond = " and F43 in('2','3','4')";
        $sumStdDev = Fetch::getStandDevatFromDeviceSataByDevId($did, $cron_start, $cron_end, $cond);
        if (!empty($sumStdDev))
        {
            $st = 0;
            foreach ($sumStdDev as $sum)
            {
                $st = $st + (pow(($sum->vspd - $avg_speed) , 2));
            }
            $varience = $st / (count($sumStdDev) - 1);
            $stdDevVehSpd = sqrt($varience);
        }
        else
        {
            $stdDevVehSpd = 0;
        }
        $data['stdDevVehSpd'] = round($stdDevVehSpd, 0);
        //print_r($sumDat);
        if ($duration != 0)
        {
            // Idle Time
            $idle_time = !empty($sumDat[0]->idle_time) ? $sumDat[0]->idle_time : "0";
            $idle_time = $idle_time * 100 / $run_dur;
            $data['idle_time'] = $idle_time;

            // Accelaration
            $accelaration_time = !empty($sumDat[0]->accelaration_time) ? $sumDat[0]->accelaration_time : "0";
            $accelaration_time = $accelaration_time * 100 / $run_dur;
            $data['accelaration_time'] = $accelaration_time;

            // Cruise Time
            $cruise_time = !empty($sumDat[0]->cruise_time) ? $sumDat[0]->cruise_time : "0";
            $cruise_time = $cruise_time * 100 / $run_dur;
            $data['cruise_time'] = $cruise_time;

            // Coastdown Time
            //$coast_downtime     = !empty($sumDat[0]->coast_downtime)? $sumDat[0]->coast_downtime:"0";
            //$coast_downtime     = $coast_downtime*100/$run_dur;
            //$data['coast_downtime'] = $coast_downtime;
            // Brake Time
            $brake_time = !empty($sumDat[0]->brake_time) ? $sumDat[0]->brake_time : "0";
            $brake_time = $brake_time * 100 / $run_dur;
            // print_r($brake_time);die;
            $data['brake_time'] = $brake_time;
        }
        else
        {
            $data['idle_time'] = '0';
            $data['accelaration_time'] = '0';
            $data['cruise_time'] = '0';
            //$data['coast_downtime']    = '0';
            $data['brake_time'] = '0';
        }

        // No of Starts
        $no_of_starts = !empty($sumDat[0]->no_starts) ? $sumDat[0]->no_starts : "0";
        $no_starts = round($no_of_starts * $dist_fact, 0);
        $data['no_starts'] = $no_starts;

        // Average Cranks
        if (!empty($sumDat[0]->sum_cranks))
        {
            if ($no_of_starts != 0)
            {
                $avg_cranks = ($sumDat[0]->sum_cranks) / $no_of_starts;
            }
            else
            {
                $avg_cranks = 0;
            }
        }
        else
        {
            $avg_cranks = 0;
        }
        $data['avg_cranks'] = round($avg_cranks, 0);

        // Max Cranks
        $max_cranks = !empty($sumDat[0]->max_cranks) ? $sumDat[0]->max_cranks : "0";
        $data['max_cranks'] = round($max_cranks, 0);

        // Cranks > 3 to start
        $crankCount = Fetch::getCranksGreaterThanThree($did, $cron_start, $cron_end);
        $q = array();
        foreach ($crankCount as $crn)
        {
            if ($crn->max_crnk_cnt > 3)
            {
                $q[] = $crn->max_crnk_cnt;
            }
        }
        $crankGreater = !empty($q) ? count($q) : "0";
        $crankGreater = round($dist_fact * $crankGreater, 0);
        $data['crankGreater'] = $crankGreater;

        // No of stalls
        $no_stalls = !empty($sumDat[0]->no_stalls) ? $sumDat[0]->no_stalls : "0";
        $no_stalls = round($dist_fact * $no_stalls, 0);
        $data['no_stalls'] = $no_stalls;

        /**********************************************************************************************************/
        /* -------------------------------------- Third Section End -------------------------------------------- */
        /**********************************************************************************************************/

        /**********************************************************************************************************/
        /* ------------------------------------- Fourth Section Start ------------------------------------------ */
        /**********************************************************************************************************/

        // Average RPM
        if (!empty($sumStdDev))
        {
            $st = 0;
            foreach ($sumStdDev as $sum)
            {
                $st = $st + $sum->rpm;
            }

            $avg_rpm = $st / count($sumStdDev);
        }
        else
        {
            $avg_rpm = 0;
        }
        $data['avg_rpm'] = round($avg_rpm, 0);

        // Max RPM
        $max_rpm = !empty($sumDat[0]->max_rpm) ? $sumDat[0]->max_rpm : "0";
        $data['max_rpm'] = round($max_rpm, 0);

        // Min RPM
        $min_rpm = !empty($sumDat[0]->min_rpm) ? $sumDat[0]->min_rpm : "0";
        $data['min_rpm'] = round($min_rpm, 0);

        // Standard Devation for RPM Speed
        if (!empty($sumStdDev))
        {
            $st = 0;
            foreach ($sumStdDev as $sum)
            {
                $st = $st + (pow(($sum->rpm - $avg_rpm) , 2));
            }
            $varience = $st / (count($sumStdDev) - 1);
            $stdDevRpm = sqrt($varience);
        }
        else
        {
            $stdDevRpm = 0;
        }
        $data['stdDevRpm'] = round($stdDevRpm, 0);

        // Rev Limit Crossing
        $lmt_cross = !empty($sumDat[0]->lmt_cross) ? $sumDat[0]->lmt_cross : "0";
        $lmt_cross = round($dist_fact * $lmt_cross, 0);
        $data['lmt_cross'] = $lmt_cross;

        // Average Throttle
        // $avg_throttle         = !empty($sumDat[0]->avg_throttle)? $sumDat[0]->avg_throttle:"0";
        if (!empty($sumData))
        {
            $st = 0;
            $dursum = 0;
            foreach ($sumData as $avg)
            {
                $st = $st + ($avg->avg_throttle * $avg->dur);
                $dursum = $dursum + $avg->dur;
            }
            $avg_throttle = round($st / $dursum, 0);
        }
        else
        {
            $avg_throttle = 0;
        }
        $data['avg_throttle'] = $avg_throttle;

        // Max Throttle
        $max_throttle = !empty($sumDat[0]->max_throttle) ? $sumDat[0]->max_throttle : "0";
        $data['max_throttle'] = $max_throttle;

        // Min Throttle
        $min_throttle = !empty($sumDat[0]->min_throttle) ? $sumDat[0]->min_throttle : "0";
        $data['min_throttle'] = $min_throttle;

        // Standard Devation for Throttle
        if (!empty($sumStdDev))
        {
            $st = 0;
            foreach ($sumStdDev as $sum)
            {
                if ($sum->throt != '')
                {
                    $st = $st + (pow(($sum->throt - $avg_throttle) , 2));
                }
            }
            $varience = $st / (count($sumStdDev) - 1);
            $stdDevThrottle = sqrt($varience);
        }
        else
        {
            $stdDevThrottle = 0;
        }
        $data['stdDevThrottle'] = round($stdDevThrottle);

        // Power Delivery //check
        $cond = " and F43 in('3','4')";
        $sumStdDev0 = Fetch::getStandDevatFromDeviceSataByDevId($did, $cron_start, $cron_end, $cond);
        if (!empty($sumStdDev0))
        {
            $st = 0;
            foreach ($sumStdDev0 as $sum)
            {
                if (empty($sum->Fpower_hp))
                {
                    //Do nothing
                    
                }
                else
                {
                    $st = $st + $sum->Fpower_hp;
                }
            }
            $power_del = $st / count($sumStdDev0); // Power in HP
            $power_del = $power_del / 1.34; // Power in KW
            
        }
        else
        {
            $power_del = 0;
        }
        $power_del = round((($dist_fact * $power_del) / 100) , 2);
        $data['power_del'] = $power_del;

        // Torque Delivery
        if (!empty($sumStdDev0))
        {
            $st = 0;
            foreach ($sumStdDev0 as $sum)
            {
                if (empty($sum->Ftorque))
                {

                }
                else
                {
                    $st = $st + $sum->Ftorque;
                }
            }
            $torque_del = $st / count($sumStdDev0);
        }
        else
        {
            $torque_del = 0;
        }
        $torque_del = round((($dist_fact * $torque_del) / 100) , 2);
        $data['torque_del'] = $torque_del;

        // Max Power
        $max_power = !empty($sumDat[0]->max_power) ? $sumDat[0]->max_power : "0";
        $data['max_power'] = round($max_power, 2);

        // Max Torque
        $max_torque = !empty($sumDat[0]->max_torque) ? $sumDat[0]->max_torque : "0";
        $data['max_torque'] = round($max_torque, 2);

        // Avg Coolant Temp
        // $avg_cool           = !empty($sumDat[0]->avg_cool)? $sumDat[0]->avg_cool:"0";
        if (!empty($sumData))
        {
            $st = 0;
            $dursum = 0;
            foreach ($sumData as $avg)
            {
                $st = $st + ($avg->avg_cool * $avg->dur);
                $dursum = $dursum + $avg->dur;
            }
            $avg_cool = round($st / $dursum, 0);
        }
        else
        {
            $avg_cool = 0;
        }
        $data['avg_cool'] = $avg_cool;

        // Max Coolant Temp
        $max_cool = !empty($sumDat[0]->max_cool) ? $sumDat[0]->max_cool : "0";
        $data['max_cool'] = round($max_cool, 0);

        /**********************************************************************************************************/
        /* --------------------------------------- Fourth Section End ------------------------------------------ */
        /**********************************************************************************************************/

        $sqlData5 = "max(gr0_max_rpm) as gr0_max_rpm,IFNULL(MIN(NULLIF(gr0_min_rpm, 0)), 0) as gr0_min_rpm,max(gr1_max_rpm) as gr1_max_rpm,IFNULL(min(NULLIF(gr1_min_rpm,0)),0) as gr1_min_rpm,max(gr2_max_rpm) as gr2_max_rpm,IFNULL(min(NULLIF(gr2_min_rpm,0)),0) as gr2_min_rpm,max(gr3_max_rpm) as gr3_max_rpm,IFNULL(min(NULLIF(gr3_min_rpm,0)),0) as gr3_min_rpm,max(gr4_max_rpm) as gr4_max_rpm,IFNULL(min(NULLIF(gr4_min_rpm,0)),0) as gr4_min_rpm,max(gr5_max_rpm) as gr5_max_rpm,IFNULL(min(NULLIF(gr5_min_rpm,0)),0) as gr5_min_rpm,max(gr6_max_rpm) as gr6_max_rpm,IFNULL(MIN(NULLIF(gr6_min_rpm, 0)), 0) as gr6_min_rpm";

        $data5 = Fetch::getHeatMapData('device_stats_gear', $did, $cron_start, $cron_end, $sqlData5);

        $data['gear'] = $data5[0];
        // Average RPM in each gear
        $data['avg_rpm_gear0'] = Fetch::getStandDevatFromDeviceSataByDevId1($did, $cron_start, $cron_end, 0);
        $data['avg_rpm_gear1'] = Fetch::getStandDevatFromDeviceSataByDevId1($did, $cron_start, $cron_end, 1);
        $data['avg_rpm_gear2'] = Fetch::getStandDevatFromDeviceSataByDevId1($did, $cron_start, $cron_end, 2);
        $data['avg_rpm_gear3'] = Fetch::getStandDevatFromDeviceSataByDevId1($did, $cron_start, $cron_end, 3);
        $data['avg_rpm_gear4'] = Fetch::getStandDevatFromDeviceSataByDevId1($did, $cron_start, $cron_end, 4);
        $data['avg_rpm_gear5'] = Fetch::getStandDevatFromDeviceSataByDevId1($did, $cron_start, $cron_end, 5);
        $data['avg_rpm_gear6'] = Fetch::getStandDevatFromDeviceSataByDevId1($did, $cron_start, $cron_end, 6);

        $gearChange = Fetch::getGearChangeHealth($did, $cron_start, $cron_end);

        // No Of Gear Shift
        $gear_shift = !empty($gearChange[0]->gear_shift) ? $gearChange[0]->gear_shift : "0";
        // $gear_shift         = ($dist_fact*$gear_shift);
        $data['gear_shift'] = ceil($dist_fact * $gear_shift);

        // Half Clutchtime
        $hlf_clh = !empty($gearChange[0]->half_clutch) ? $gearChange[0]->half_clutch : "0";
        if ($duration != 0)
        {
            $data['half_clutch'] = round(($hlf_clh * 100 / $run_dur) , 0);
        }
        else
        {
            $data['half_clutch'] = 0;
        }

        // Clutch Power Loss
        if (!empty($sumStdDev0))
        {
            $powerloss = 0;
            foreach ($sumStdDev0 as $sum)
            {
                if ($sum->pwr_loss != '')
                {
                    $powerloss = $powerloss + $sum->pwr_loss;
                }

            }
        }
        else
        {
            $powerloss = 0;
        }
        $data['pwr_loss'] = round($dist_fact * $powerloss, 0);

        // False neutral events
        $netural = !empty($gearChange[0]->netural) ? $gearChange[0]->netural : "0";
        $data['netural'] = round($dist_fact * $netural, 0);

        /**********************************************************************************************************/
        /* -------------------------------------- Fifth Section Start ------------------------------------------ */
        /**********************************************************************************************************/

        // Avg Voltage
        $cond = " and F43 in('2','3','4')";
        $sumStdDev1 = Fetch::getStandDevatFromDeviceSataByDevId($did, $cron_start, $cron_end, $cond);
        foreach ($sumStdDev1 as $dev)
        {
            $fde[] = $dev->F34;
        }
        if (!empty($fde))
        {
            $avg_voltage = round((array_sum($fde) / count($fde)) , 1);
        }
        else
        {
            $avg_voltage = 0;
        }
        $data['avg_voltage'] = $avg_voltage;

        // Avg Voltage of Minimum Voltages
        $avg_voltage_low = !empty($sumDat[0]->avg_batt_volt) ? $sumDat[0]->avg_batt_volt : "0";
        $data['avg_voltage_low'] = round($avg_voltage_low, 1);

        // Max Voltage
        $data['max_batt_volt'] = !empty($sumDat[0]->max_batt_volt) ? round(($sumDat[0]->max_batt_volt) , 1) : "0";

        // Min Voltage
        $data['min_batt_volt'] = !empty($sumDat[0]->min_batt_volt) ? round(($sumDat[0]->min_batt_volt) , 1) : "0";

        // Count Voltage < low limit
        $cond = " and F34 < '13.7' and F4 > '2000'";
        $sumStdDev3 = Fetch::getStandDevatFromDeviceSataByDevIdCount($did, $cron_start, $cron_end, $cond);
        $voltage_low_less_count = $sumStdDev3[0]->count ? $sumStdDev3[0]->count : "0";
        $data['avg_voltage_low_less'] = round(($dist_fact * $voltage_low_less_count) , 0);

        // Count Voltage > high limit
        $cond = " and F34 > '14.6' and F43 in('2','3','4')";
        $sumStdDev4 = Fetch::getStandDevatFromDeviceSataByDevIdCount($did, $cron_start, $cron_end, $cond);
        $voltage_low_high_count = $sumStdDev4[0]->count ? $sumStdDev4[0]->count : "0";
        $data['avg_voltage_low_high'] = round(($dist_fact * $voltage_low_high_count) , 0);

        /**********************************************************************************************************/
        /* -------------------------------------- Fifth Section End ------------------------------------------- */
        /**********************************************************************************************************/

        /**********************************************************************************************************/
        /* -------------------------------------- Sixth Section Start ------------------------------------------ */
        /**********************************************************************************************************/

        /* Heat Map 1st Start */
        $sqlData1 = "";
        for ($i = 0;$i < 110;$i++)
        {
            $sqlData1 .= "sum(calstatspdchng$i) as '$i',";
        }
        $sqlData1 = rtrim($sqlData1, ',');

        $heatFirst = Fetch::getHeatMapData('device_stats', $did, $cron_start, $cron_end, $sqlData1);
        $heatSumFirst = 0;
        foreach ($heatFirst[0] as $hf)
        {
            $heatSumFirst = $heatSumFirst + $hf;
        }
        if ($heatSumFirst != 0)
        {
            $table1 = array();
            $columns = 10;
            $rows = 11;

            for ($i = 0;$i < $rows;$i++)
            {
                for ($j = 0;$j < $columns;$j++)
                {
                    $count = (($i * $columns) + $j);
                    //echo "---- i = $i and j = $j \n";
                    $val = $heatFirst['0']->$count;
                    $val = round(($val * 100) / $heatSumFirst, 2);
                    $table1[$i][$j] = $val;
                }
                $max1[] = max($table1[$i]);
            }
            // Sum Horizontal
            foreach ($table1 as $tb)
            {
                $i = 0;
                foreach ($tb as $bey => $t)
                {
                    if ($bey == $i)
                    {
                        $vertical_sum1[$bey][] = $t;
                        $i++;
                    }
                }
            }
            // Cumulative Horizontal
            foreach ($vertical_sum1 as $sum)
            {
                $arr1[] = array_sum($sum);
            }

            $data['vertical_sum1'] = $arr1;
            $data['table1'] = $table1;
            $data['maxHeatMap1'] = max($max1);
        }
        else
        {
            $data['maxHeatMap1'] = 0;
        }

        /* Heat Map 1st End */

        /* Heat Map 2nd Start */
        $sqlData2 = "";
        for ($i = 0;$i < 100;$i++)
        {
            $sqlData2 .= "sum(calstatsthr$i) as '$i',";
        }
        $sqlData2 = rtrim($sqlData2, ',');
        $heatSecond = Fetch::getHeatMapData('device_stats', $did, $cron_start, $cron_end, $sqlData2);
        $heatSumSecond = 0;
        foreach ($heatSecond[0] as $hf)
        {
            $heatSumSecond = $heatSumSecond + $hf;
        }
        if ($heatSumSecond != 0)
        {
            $table2 = array();
            $columns = 10;
            $rows = 10;

            for ($i = 0;$i < $rows;$i++)
            {
                for ($j = 0;$j < $columns;$j++)
                {
                    $count = (($i * $columns) + $j);
                    //echo "---- i = $i and j = $j \n";
                    $val = $heatSecond['0']->$count;
                    $val = round(($val * 100) / $heatSumSecond, 2);
                    $table2[$i][$j] = $val;
                }
                $max2[] = max($table2[$i]);
            }
            // Sum Horizontal
            foreach ($table2 as $tb)
            {
                $i = 0;
                foreach ($tb as $bey => $t)
                {
                    if ($bey == $i)
                    {
                        $vertical_sum2[$bey][] = $t;
                        $i++;
                    }
                }
            }

            foreach ($vertical_sum2 as $sum)
            {
                $arr2[] = array_sum($sum);
            }
            $data['vertical_sum2'] = $arr2;
            $data['table2'] = $table2;
            $data['maxHeatMap2'] = max($max2);
        }
        else
        {
            $data['maxHeatMap2'] = 0;
        }
        /* Heat Map 2nd End */

        /* Heat Map 3rd Start */
        $heatColumns = Fetch::getGearChangeHeatMapColumns();
        $sqlData3 = "";
        foreach ($heatColumns as $key => $value)
        {
            $col[] = $value->Field;
            $sqlData3 .= "sum($value->Field) as '$value->Field',";
        }
        $sqlData3 = rtrim($sqlData3, ',');
        $heatThird = Fetch::getHeatMapData('device_stats_gear', $did, $cron_start, $cron_end, $sqlData3);
        foreach ($heatThird[0] as $key => $ht)
        {
            $jk = explode('_', $key);
            $hj[] = $jk[2] . "_" . $jk[3];
            $ju[] = ceil($ht * $dist_fact);
        }

        $data['table3'] = $hj;
        $data['tab3_data'] = $ju;
        /* Heat Map 3rd End */

        /* Heat Map 4th Start */
        $sqlData4 = "";
        for ($i = 0;$i < 70;$i++)
        {
            $sqlData4 .= "sum(calstatsgr$i) as '$i',";
        }
        $sqlData4 = rtrim($sqlData4, ',');
        $heatFour = Fetch::getHeatMapData('device_stats_gear', $did, $cron_start, $cron_end, $sqlData4);

        $heatSumFour = 0;
        foreach ($heatFour[0] as $hf)
        {
            $heatSumFour = $heatSumFour + $hf;
        }
        if ($heatSumFour != 0)
        {
            $table4 = array();
            $columns = 10;
            $rows = 7;

            for ($i = 0;$i < $rows;$i++)
            {
                for ($j = 0;$j < $columns;$j++)
                {
                    $count = (($i * $columns) + $j);
                    //echo "---- i = $i and j = $j \n";
                    $val = $heatFour['0']->$count;
                    $val = round(($val * 100) / $heatSumFour, 2);
                    $table4[$i][$j] = $val;
                }
                $max4[] = max($table4[$i]);
            }
            // Sum Horizontal
            foreach ($table4 as $tb)
            {
                $i = 0;
                foreach ($tb as $bey => $t)
                {
                    if ($bey == $i)
                    {
                        $vertical_sum4[$bey][] = $t;
                        $i++;
                    }
                }
            }

            foreach ($vertical_sum4 as $sum)
            {
                $arr4[] = array_sum($sum);
            }
            $data['vertical_sum4'] = $arr4;
            $data['table4'] = $table4;
            $data['maxHeatMap4'] = max($max4);
        }
        else
        {
            $data['maxHeatMap4'] = 0;
        }
        /* Heat Map 4th End */

        /**********************************************************************************************************/
        /* --------------------------------------- Sixth Section End ------------------------------------------- */
        /**********************************************************************************************************/

        /**********************************************************************************************************/
        /* -------------------------------------- Sixth Section Start ------------------------------------------ */
        /**********************************************************************************************************/

        /**********************************************************************************************************/
        /* --------------------------------------- Sixth Section End ------------------------------------------- */
        /**********************************************************************************************************/

        /**********************************************************************************************************/
        /* ------------------------------------- Seventh Section Start ----------------------------------------- */
        /**********************************************************************************************************/

        // Battery and CHarging System Health
        $systemHealth = Fetch::getSystemHealth($did, $cron_start, $cron_end);
        $batt_hlt = !empty($systemHealth[0]->bat_c) ? $systemHealth[0]->bat_c : "0";
        $batt_rate = 0;
        $batHel1['msg'] = "";
        $batHel2['msg'] = "";

        if (($batt_hlt < 10) && ($batt_hlt != "") && ($batt_hlt != 0))
        {
            //$batt_hlt = $batt_hlt;
            if ($batt_hlt == 5)
            {
                $batHel2['msg'] = "";
                $batt_rate = $batt_hlt;
            }
            else
            {
                $batHel2['msg'] = " Alternator Okay.";
                $batt_rate = $batt_hlt;
            }
        }
        elseif (($batt_hlt < 20) && ($batt_hlt != "") && ($batt_hlt != 0))
        {
            $batt_rate = $batt_hlt - 10;
            $batHel2['msg'] = " Please check the Alternator.";
        }
        elseif (($batt_hlt < 30) && ($batt_hlt != "") && ($batt_hlt != 0))
        {
            $batt_rate = $batt_hlt - 20;
            $batHel2['msg'] = " Please check the Alternator Voltage Regulator.";
        }
        elseif (($batt_hlt > 30) && ($batt_hlt != "") && ($batt_hlt != 0))
        {
            $batt_rate = $batt_hlt - 30;
            $batHel2['msg'] = " Running voltage low. Please check Charging System.";
        }

        //Get Battery System comments
        if ($batt_rate == 1)
        {
            $batHel1['msg'] = "Battery Critical. Please check distilled water level and drive to charge. Also consider checking the Starter Motor. If Battery is more than 3 years old consider replacement.";
        }
        elseif ($batt_rate == 2)
        {
            $batHel1['msg'] = "Battery Bad. Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
        }
        elseif ($batt_rate == 3)
        {
            $batHel1['msg'] = "Battery Low. Please check distilled water level and drive to charge.";
        }
        elseif ($batt_rate == 4)
        {
            $batHel1['msg'] = "Battery Fine. Please drive to charge.";
        }
        elseif ($batt_rate == 5)
        {
            $batHel1['msg'] = "";
        }

        //Final Battery Comment
        if ($batt_hlt > 10)
        {
            $data['batteryHealthMsg'] = $batHel2['msg'] . $batHel1['msg'];
        }
        else
        {
            $data['batteryHealthMsg'] = $batHel1['msg'] . $batHel2['msg'];
        }
        $data['batteryHealth'] = $batt_hlt;

        // Clutch Health
        // Injector and Spark Health
        $injectorHealth = !empty($systemHealth[0]->eng_block_ej) ? $systemHealth[0]->eng_block_ej : "0";
        if ($injectorHealth == 1)
        {
            $data['injectorHealthMsg'] = "Injector clogging or spark plug fouling detected. Accordingly, use fuel system flush/injector additives, or replace spark plugs. Check throttle body and belt tensioner.";
        }
        elseif ($injectorHealth == 2)
        {
            $data['injectorHealthMsg'] = "Clean spark plugs and use fuel system flush/injector additives. Clean throttle body. Consider checking belt tensioner";
        }
        elseif ($injectorHealth == 3)
        {
            $data['injectorHealthMsg'] = "Clean spark plugs and use fuel system flush/injector additives. Consider checking throttle body and belt tensioner.";
        }
        elseif ($injectorHealth == 4)
        {
            $data['injectorHealthMsg'] = "Clean spark plugs. Consider fuel system cleaning. Use fuel system flush/injector additives.";
        }
        else
        {
            $data['injectorHealthMsg'] = "";
        }
        $data['injectorHealth'] = $injectorHealth;

        // Air Filter Health
        $idlbc_v = !empty($systemHealth[0]->idlbc_v) ? $systemHealth[0]->idlbc_v : "0";
        $peakb_v = !empty($systemHealth[0]->peakb_v) ? $systemHealth[0]->peakb_v : "0";

        if ($idlbc_v == 1 && $peakb_v == 1)
        {
            $data['airHealth'] = 1;
            $data['airHealthMsg'] = "Air system pressure build-up critical. Replace air filter element. Check Throttle Body.";
        }
        elseif ($idlbc_v == 1 && $peakb_v == 2)
        {
            $data['airHealth'] = 2;
            $data['airHealthMsg'] = "Air system pressure build-up poor. Clean/replace air filter element. Check Throttle Body.";
        }
        elseif ($idlbc_v == 1 && $peakb_v == 3)
        {
            $data['airHealth'] = 3;
            $data['airHealthMsg'] = "Air system pressure build-up below average. Clean/replace air filter element. Check Throttle Body.";
        }
        elseif ($idlbc_v == 2 && $peakb_v == 1)
        {
            $data['airHealth'] = 1;
            $data['airHealthMsg'] = "Air system pressure build-up critical. Replace air filter element. Check Throttle Body.";
        }
        elseif ($idlbc_v == 2 && $peakb_v == 2)
        {
            $data['airHealth'] = 3;
            $data['airHealthMsg'] = "Air system pressure build-up below average. Clean/replace air filter element. Check Throttle Body.";
        }
        elseif ($idlbc_v == 2 && $peakb_v == 3)
        {
            $data['airHealth'] = 4;
            $data['airHealthMsg'] = "Air system pressure build-up average. Clean the air filter element.";
        }
        elseif ($idlbc_v == 3 && $peakb_v == 1)
        {
            $data['airHealth'] = 2;
            $data['airHealthMsg'] = "Air system pressure build-up poor. Clean/replace air filter element. Check Throttle Body.";
        }
        elseif ($idlbc_v == 3 && $peakb_v == 2)
        {
            $data['airHealth'] = 3;
            $data['airHealthMsg'] = "Air system pressure build-up below average. Clean/replace air filter element. Check Throttle Body.";
        }
        elseif ($idlbc_v == 3 && $peakb_v == 3)
        {
            $data['airHealth'] = 5;
            $data['airHealthMsg'] = "";
        }
        else
        {
            $data['airHealth'] = 0;
            $data['airHealthMsg'] = "";
        }

        // Oil Health
        $oilHealth = Fetch::getSystemHealth1($did, $cron_start, $cron_end);
        $oilHealth = !empty($oilHealth[0]->oillifep) ? $oilHealth[0]->oillifep : "0";
        if ($oilHealth >= 52 && $oilHealth <= 100)
        {
            $data['oilHealth'] = 5;
        }
        elseif ($oilHealth >= 35)
        {
            $data['oilHealth'] = 4;
        }
        elseif ($oilHealth >= 20)
        {
            $data['oilHealth'] = 3;
        }
        elseif ($oilHealth >= 6)
        {
            $data['oilHealth'] = 2;
        }
        else
        {
            $data['oilHealth'] = 1;
        }

        // Cooling System Health
        $coolingHealth = !empty($systemHealth[0]->coolp) ? $systemHealth[0]->coolp : "0";
        if ($coolingHealth == 1)
        {
            $data['coolingHealthMsg'] = "Engine cooling poor. Check Coolant and Engine Oil Level.";
        }
        elseif ($coolingHealth == 2)
        {
            $data['coolingHealthMsg'] = "Engine cooling inadequate. Check Coolant and Engine Oil Level. Top-up Coolant and Engine Oil if low.";
        }
        elseif ($coolingHealth == 3)
        {
            $data['coolingHealthMsg'] = "Engine cooling below average. Check Coolant and Engine Oil Level. Top-up Coolant and Engine Oil if low.";
        }
        elseif ($coolingHealth == 4)
        {
            $data['coolingHealthMsg'] = "Engine cooling average. Check Coolant and Engine Oil Level.";
        }
        else
        {
            $data['coolingHealthMsg'] = "";
        }
        $data['coolingHealth'] = $coolingHealth;

        // Gear Health
        $data['gearHealth'] = "Parameter Not Supported.";

        // Vibraion Analysis
        $data['vibrationHealth'] = "Initial;Final";
        /**********************************************************************************************************/
        /* --------------------------------------- Seventh Section End ----------------------------------------- */
        /**********************************************************************************************************/
        // print_r($data);die;
        // PDF Details Push End
        $file_name = $did . '-Daily-report-tvs-' . $cron_date . '.pdf';
        $customPaper = array(
            0,
            0,
            1000,
            900
        );
        // $result     = view('reports/pdf/tvs')->with($data);
        // return $pdf->download($file_name);
        $pdf = PDF::loadView('reports/pdf/tvs', $data)->setPaper($customPaper, 'portrait');
        return $pdf->download($file_name);
    }

    public function uploadReportFile(Request $request)
    {

        $did = $request->post('device_id');
        $type = $request->post('type');
        if (!empty($did) && !empty($type) && !empty($_FILES))
        {
            // File Upload Process and DB insert a record for the device
            if (!empty($_FILES['report']['tmp_name']))
            {
                $photo_person = $_FILES['report']['tmp_name'];
                //sprint_r($_FILES['photo_person']['name']);die;
                //$file_ext1       = explode('.',$_FILES['report']['name']);
                //$new_mst1        = Helper::v3generate(5).".".$file_ext1[1];
                $file_name = $_FILES['report']['name'];
                $path = public_path() . "/reports/" . $file_name;
                if (file_exists($path))
                {
                    \Session::flash('message', 'File all ready exist, please change the name for Device Id: ' . $did);
                    return redirect('reports');
                    exit();
                }

                if (move_uploaded_file($photo_person, public_path() . "/reports/" . $file_name))
                {
                    $devId = Fetch::getIdByDeviceId($did);
                    $devId = $devId[0]->id;
                    chmod($path, 0777);
                    $data = array(
                        'type' => $type,
                        'devid' => $did,
                        'did' => $devId,
                        'report_file' => $file_name,
                        'status' => '1',
                        'ad_dt' => date('Y-m-d H:i:s') ,
                        'ad_by' => session('userdata') ['uid'],
                        'mctype' => '4'
                    );
                    Common::insert('report', $data);
                    \Session::flash('message', 'File uploaded Successfully for DeviceID: ' . $did);
                    return redirect('reports');
                }
            }
        }
        else
        {
            \Session::flash('message', 'Fields missing for upload report, for DeviceID: ' . $did);
            return redirect('reports');
        }
    }

    public function activateReports(Request $request)
    {

        if (empty($request->post('data')))
        {
            echo 1;
        }
        else
        {
            $list = $request->post('data');
            for ($i = 0;$i <= count($list) - 1;$i++)
            {
                Fetch::updateActivateReports($list[$i]);
            }

            $cond = " 1 and status = '1'";
            if (session('userdata') ['ucategory'] == 3)
            {
                $deviceList = Fetch::getDeviceListByPartnerId(session('userdata') ['uid']);
                foreach ($deviceList as $key => $value)
                {
                    $plist[] = $value->device_id;
                }
                $plist = implode(',', $plist);
                $cond .= " and devid in ($plist)";
            }
            $data['did'] = $request->post('did');
            $data['report_active'] = Fetch::getReportListAllInactive($cond);
            $result = view('reports/loadActive')->with($data);
            return $result;
        }
    }

    public function deleteReports(Request $request)
    {
        if (empty($request->post('data')))
        {
            echo 1;
        }
        else
        {
            $ids = implode(',', $request->post('data'));
            Fetch::deleteReports($ids);
            echo 0;
        }
    }

    public function dateDiffInDays($date1, $date2)
    {
        // Calulating the difference in timestamps
        $diff = strtotime($date2) - strtotime($date1);

        // 1 day = 24 hours
        // 24 * 60 * 60 = 86400 seconds
        return abs(round($diff / 86400));
    }

    public function loadHeatGraph1(Request $request)
    {

        $did = $request->post('did');
        $valuePart1 = $request->post('valuePart1');
        $valuePart2 = $request->post('valuePart2');
        $namevaluePart1 = $request->post('namevaluePart1');
        $namevaluePart2 = $request->post('namevaluePart2');
        $start = $request->post('start');
        $end = $request->post('end');
        $col = $request->post('col');
        $row = $request->post('row');

        /*        if(!empty($valuePart1) && !empty($valuePart2) && !empty($start) && !empty($end) && count($col) >= 5 && count($row) >= 5){*/
        $unit1 = Fetch::getValuePartById($valuePart1);
        $unit2 = Fetch::getValuePartById($valuePart2);
        // print_r($unit1);die;
        $values = Fetch::getHeatGraphDeviceData1($did, $valuePart1, $valuePart2, $start, $end);
        $columns = $col;
        $rows = $row;
        $time = array(
            array()
        );
        $dur = 0;

        for ($i = 0;$i <= count($row) - 1;$i++)
        {
            for ($j = 0;$j <= count($columns) - 1;$j++)
            {
                $time[$i][$j] = 0;
            }
        }

        for ($z = 0;$z < count($values);$z++)
        {
            for ($i = 0;$i <= count($rows) - 1;$i++)
            {
                for ($j = 0;$j <= count($columns) - 1;$j++)
                {
                    if (($values[$z]->rowdata < $rows[$i]) && ($values[$z]->coldata < $columns[$j]))
                    {
                        $time[$i][$j] = $time[$i][$j] + 1;
                        $dur = $dur + 1;
                        continue 3;
                    }
                }
            }
        }

        $time_dur = array(
            array()
        );
        if ($dur == 0)
        {
            $dur = 1;
        }

        foreach ($time as $key => $val)
        {
            foreach ($val as $bey => $v)
            {
                $time_dur[$key][$bey] = round(($v * 100) / $dur, 2);
            }
        }

        // Sum Vertical
        foreach ($time_dur as $key => $tb)
        {
            $i = 0;
            foreach ($tb as $bey => $t)
            {
                if ($bey == $i)
                {
                    $vertical_sum4[$bey][] = $t;
                    $i++;
                }

                $max4[] = max($vertical_sum4[0]);
            }
        }

        foreach ($vertical_sum4 as $sum)
        {
            $arr2[] = array_sum($sum);
        }

        foreach ($vertical_sum4 as $sum)
        {
            $arr4[] = array_sum($sum);
        }
        $data['namevaluePart1'] = $namevaluePart1;
        $data['namevaluePart2'] = $namevaluePart2;
        $data['vertical_sum'] = $arr2;
        $data['table'] = $time_dur;
        $data['maxHeatMap'] = max($max4);
        $data['rows'] = $row;
        $data['cols'] = $col;
        $data['unit1'] = $unit1[0]->unit;
        $data['unit2'] = $unit2[0]->unit;

        /* Heat Map 4th End */

        return view('reports/loadHeat1')
            ->with($data);
    }
}

