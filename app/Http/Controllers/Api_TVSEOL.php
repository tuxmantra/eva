<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newuser;
use Illuminate\Support\Arr;
// use Mail;

class Api_TVSEOL extends Controller
{
	public function profile(Request $request)
  	{
  		$header = getallheaders();
  		// print_r($header);die;
  		if(0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
	 		if(!empty($request->data))
	        {	
	           	$json     = $request->data;
				$obj      = json_decode($json,true);	
				$deviceID = $obj['user']['deviceid'];
				$devInfo  = Fetch::getdevice($deviceID);
			
				if($deviceID != '')
				{
					if(empty($devInfo))
					{
						$postValD              = array();
						$postValD['device_id'] = $deviceID;
						$postValD['name']      = $obj['user']['name'];
						$postValD['descrp']    = $obj['user']['name'];
						$postValD['partner']   = 2;		
						$postValD['status']    = 1;
						$postValD['ad_by']     = 2;
						$postValD['ad_dt']     = date('Y-m-d H:i:s');
						$postValD['devtype']   = 1;
						$postValD['dev_cmds']   = "";
						// $postValD['phn_model'] = $obj['user']['phnName'];
						// $postValD['bt_mac_id'] = $obj['user']['btMacId'];
						$update                = Fetch::insert_device($postValD);
						// print_r($update);die;
						if($update)
						{
							$devInfo  = Fetch::getdevice($deviceID);

							//Device Addition Complete. Now Add User.
							$prof = $obj['user'];
							// print_r($prof);die;
							$us   = Fetch::getUserByEmail($prof['email']);
							if(count($us) >= 1)
							{
								$arrayData['success']      = FALSE;
								$arrayData['errorCode']    = "1004";
								$arrayData['errorMessage'] = "Email ID already exist!";
							}
							else
							{
								$drvInfo              = array();
								// $drvInfo['device']    = $devInfo[0]->id;
								$drvInfo['uname']     = $prof['name']; 
								$drvInfo['utype']     = $prof['utype'];
								$drvInfo['ucategory'] = 4;
								$drvInfo['ulocation'] = "Bangalore";
								$drvInfo['umob']      = $prof['mobile'];
								$drvInfo['uemail']    = $prof['email'];
								$drvInfo['upwd']      = md5($prof['password']);
								$drvInfo['ustatus']   = 0;
								$drvInfo['partner']   = 2;
								$drvInfo['device']	  = $update[0]->id;
	 
								$saveData             = Fetch::save_user($drvInfo);
								if($saveData)
								{
									// Add Activation Code
						            $token1  = mt_rand(1000,9999);
						            $t       = strtotime('+1 year');
						            $afteryr = date('Y-m-d', $t);
						            $data1   = array(
							            'activation_code' => $token1,
							            'mac_id'          => $deviceID,
							            'used_by'         => $prof['email'],
							            'status'          => 1,
							            'expdt'           => $afteryr,
							            'email'           => $prof['email'],
							            'ad_by'           => 1,
							        );
						            $activation_id        = Common::insertGetId('activation_code',$data1);
						            // send activation code
						            if($prof['email'] != "" && $token1 != "")
									{
										$subject = "Activation code for your device";
										$message1 = "<table>
														<tr><td>Activation code for your device</td></tr>
														<tr><td>Details are given below : </td></tr>
														<tr><td>-----------------------------------</td></tr>
														<tr><td>Device ID : ".$deviceID." </td></tr>
														<tr><td>Activation code : ".$token1." </td></tr>
														<tr><td>-----------------------------------</td></tr>
													</table> \n";
										
										$from     = 'support@enginecal.com';
							            $headers  = 'MIME-Version: 1.0' . "\r\n";
							            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
							             
							            // Create email headers
							            $headers .= 'From: '.$from."\r\n".
							                		'Reply-To: '.$from."\r\n" .
							                		'X-Mailer: PHP/' . phpversion();
							            
							            if(mail('utkarsh.raj@enginecal.com',$subject,$message1,$headers))
							            {
											$subject1 = "TVS EOL new device registered";
											$message1 = "<table>
															<tr><td>New device registered</td></tr>
															<tr><td>Details are given below : </td></tr>
															<tr><td>-----------------------------------</td></tr>
															<tr><td>Username  : ".$prof['name']." </td></tr>
															<tr><td>Device ID : ".$deviceID." </td></tr>
															
															<tr><td>-----------------------------------</td></tr>
														</table> \n";
											
											$from1     = 'support@enginecal.com';
								            $headers1  = 'MIME-Version: 1.0' . "\r\n";
								            $headers1 .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
								             
								            // Create email headers
								            $headers1 .= 'From: '.$from."\r\n".
								                		'Reply-To: '.$from."\r\n" .
								                		'X-Mailer: PHP/' . phpversion();
					                		if(mail($from1,$subject1,$message1,$headers1))
							            	{

							            	}
							            	else
							            	{

							            	}
										}
										else
										{
											// print_r(error_get_last());
										}

									}

						            // Add Vehicle
									$postValVeh['device'] = $devInfo[0]->id;
									$update               = Fetch::insert_vehicle($postValVeh);
									if($update)
									{
										$arrayData['success'] = TRUE;
										$arrayData['message'] = "Registration done successfully!";
									}
									else

									{
										$arrayData['success']      = FALSE;
										$arrayData['errorCode']    = "1003";
										$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
									}
								}
								else
								{
									$arrayData['success']      = FALSE;
									$arrayData['errorCode']    = "1003";
									$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
								}
							}
						}
						else
						{
							$arrayData['success']      = FALSE;
							$arrayData['errorCode']    = "1003";
							$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
						}
					}
					else
					{
						// The device is already registered. Check if user is assigned to the device.
						$prof     = $obj['user'];
						$drvInfo1 = Fetch::getdriverDetails($deviceID);
						if(empty($drvInfo1)) 
						{
							$us   = Fetch::getUserByEmail($prof['email']);
							if(count($us) >= 1)
							{
								$arrayData['success']      = FALSE;
								$arrayData['errorCode']    = "1004";
								$arrayData['errorMessage'] = "Email ID already exist!";
							}
							else
							{
								$drvInfo              = array();
								$drvInfo['device']    = $devInfo[0]->id;
								$drvInfo['partner']   = 2;
								$drvInfo['uname']     = $prof['name'];
								$drvInfo['utype']     = 14;
								$drvInfo['ucategory'] = 4;
								$drvInfo['ulocation'] = "Bangalore";
								$drvInfo['umob']      = $prof['mobile'];
								$drvInfo['uemail']    = $prof['email'];
								$drvInfo['upwd']      = md5($prof['password']);
								$drvInfo['ustatus']   = 0;
								$saveData             = Fetch::save_user($drvInfo);
								if($saveData)
								{
									// Add Activation Code
						            $token1  = Helper::token1(10);
						            $t       = strtotime('+1 year');
						            $afteryr = date('Y-m-d', $t);
						            $data1   = array(
							            'activation_code' => $token1,
							            'mac_id'          => $deviceID,
							            'used_by'         => $prof['email'],
							            'status'          => 1,
							            'expdt'           => $afteryr,
							            'email'           => $prof['email'],
							            'ad_by'           => 1,
							        );
						            $activation_id        = Common::insertGetId('activation_code',$data1);
						            
						            if($prof['email'] != "" && $token1 != "")
									{
										$subject = "Activation code for your device";
										$message1 = "<table>
														<tr><td>Activation code for your device</td></tr>
														<tr><td>Details are given below : </td></tr>
														<tr><td>-----------------------------------</td></tr>
														<tr><td>Device ID : ".$deviceID." </td></tr>
														<tr><td>Activation code : ".$token1." </td></tr>
														<tr><td>-----------------------------------</td></tr>
													</table> \n";
										
										$from     = 'support@enginecal.com';
							            $headers  = 'MIME-Version: 1.0' . "\r\n";
							            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
							             
							            // Create email headers
							            $headers .= 'From: '.$from."\r\n".
							                		'Reply-To: '.$from."\r\n" .
							                		'X-Mailer: PHP/' . phpversion();
							            
							            if(mail($prof['email'],$subject,$message1,$headers))
							            {
											// $arrayData['success']   = TRUE;
											// $arrayData['deviceKey'] = $devKey;
											// $arrayData['mail']      = "Key sent successfully!";
											// echo "Mail sent successfully";
										}
										else
										{
											// print_r(error_get_last());
										}

									}
						            // Add Vehicle
									$postValVeh['device'] = $devInfo[0]->id;
									$update               = Fetch::insert_vehicle($postValVeh);
									if($update)
									{
										$arrayData['success'] = TRUE;
										$arrayData['message'] = "Registration done successfully!";
									}
									else

									{
										$arrayData['success']      = FALSE;
										$arrayData['errorCode']    = "1003";
										$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
									}
								}
								else
								{
									$arrayData['success']      = FALSE;
									$arrayData['errorCode']    = "1003";
									$arrayData['errorMessage'] = "Registration failed. Kindly try again later";
								}
							}
						}
						else
						{
							$arrayData['success']      = FALSE;
							$arrayData['errorCode']    = "1002";
							$arrayData['errorMessage'] = "Device already exists!";
						}
					}
	      	   	}
	      	   	else
		        {
		        	$arrayData['success']      = FALSE;
		        	$arrayData['errorCode']    = "1001";
		        	$arrayData['errorMessage'] = "Invalid Input!";
		        }
	      	}
	      	else
	      	{
	      		$arrayData['success']      = FALSE;
	      		$arrayData['errorCode']    = "1001";
	      		$arrayData['errorMessage'] = "Invalid Input!";
	      	}
	    }
    	$resultVal = json_encode($arrayData);
    	echo $resultVal;
  	}

	public function check_login(Request $request)
    {
    	$header = getallheaders();
  		// print_r($header);die;
  		// if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		if(0)
  		{
  			$result['success']      = FALSE;
      		$result['errorCode']    = "1004";
      		$result['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
	 		if(!empty($request->data))
	        {
	     		$obj      = json_decode($request->data, TRUE);
	     		$username = $obj['u'];
				$password = $obj['p'];
				// $phnName  = $obj['phnName'];
				// $btMacId  = $obj['btMacId'];
				// $devID    = $obj['d'];
				$password = md5($password);//ENCRYPT_DECRYPT($password,$enc);
				// print_r($password);die;
				$utype    = array('0'=>'','1'=>'factory','2'=>'service');

				if($res = Fetch::getUserByEmailPass($username,$password))
				{
					$devID = $res[0]->device;
					if (!empty($devID)) 
					{
						$data  = Fetch::getDeviceById($devID);
						$deviceid             = $data[0]->device_id;
						$result['success']    = TRUE;
						$result['userType']   = $res[0]->utype;
						$result['userName']   = $res[0]->uname;
						// $result['userType']   = $utype[$res[0]->ucategory];
						$result['deviceID']   = $deviceid;
						$result['email']      = $username;
						$result['token']      = "9gsJ7FVvWHYe4kq0OKrfyl5NDoLZ3QGC";
					}
					else
					{
						$result['success']      = FALSE;
						$result['errorCode']    = "1003";
						$result['errorMessage'] = "User not active! Please contact EngineCAL Team.";
					}
				}
				else
				{
					$result['success']      = FALSE;
					$result['errorCode']    = "1002";
					$result['errorMessage'] = "Invalid Username or Password!";
				}
	     	}
	     	else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1001";
				$result['errorMessage'] = "Invalid Input!";
			}
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
    }

	public function forget_password(Request $request)
  	{
	    $header = getallheaders();
  		// print_r($header);die;
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
	     	if(!empty($request->data))
	     	{    
	        	$data  = json_decode($request->data, TRUE);
	        	$email = $data['email'];
	           	$info  = Fetch::getUserByEmail($email);

	       		if(empty($info))
	       		{
		           	$msg                       = "\"$email\" is not a Registered E-Mail Id !";
		            $arrayData['success']      = FALSE;
		            $arrayData['errorCode']    = "1003";
		            $arrayData['errorMessage'] = $msg;
		    	}
		       	else
		       	{
	        		$data       = new \stdClass();
		            $data->name = $info[0]->uname;
		            $name       = $info[0]->uname;
		            $data->uid  = $info[0]->uid;
		            $uid        = $info[0]->uid;
		            //print_r( $uid);die;
		           $msg = '
		           <html>
		              <head>
		                <title>Reset Password Request</title>
		              </head>
		              <body>
		                <table id="m_9144868195613793263m_-947074545920247478templateContainer" style="border:1px solid #dddddd;background-color:#ffffff" width="600" cellspacing="0" cellpadding="0" border="0">
		                 <tbody>
		                    <tr>
		                       <td valign="top" align="center">
		                          <table id="m_9144868195613793263m_-947074545920247478templateHeader" width="600" cellspacing="10" cellpadding="10" border="0">
		                             <tbody>
		                                <tr style="background: #eee;">
		                                   <td>
		                                      <table align="left">
		                                         <tbody>
		                                            <tr>
		                                               <td>
		                                                  <a href="#m_9144868195613793263_m_-947074545920247478_">
		                                                  <img src="http://www.enginecal.com/wp-content/uploads/2016/08/Logo_Final-174x66.png" class="CToWUd">
		                                                  </a>
		                                               </td>
		                                            </tr>
		                                         </tbody>
		                                      </table>
		                                   </td>
		                                   <td>
		                                      <table align="right">
		                                         <tbody>
		                                            <tr>
		                                              
		                                            </tr>
		                                         </tbody>
		                                      </table>
		                                   </td>
		                                </tr>
		                             </tbody>
		                          </table>
		                       </td>
		                    </tr>
		                    <tr>
		                       <td valign="top" align="center">
		                          <table id="m_9144868195613793263m_-947074545920247478templateBody" width="600" cellspacing="0" cellpadding="10" border="0">
		                             <tbody>
		                                <tr>
		                                   <td class="m_9144868195613793263m_-947074545920247478bodyContent" valign="top">
		                                      <table width="100%" cellspacing="0" cellpadding="10" border="0">
		                                         <tbody>
		                                            <tr>
		                                               <td valign="top">
		                                                  <div>
		                                                     <span class="m_9144868195613793263m_-947074545920247478h3" style="color:#ec1c23;font-weight:bold;font-size:24px">Hi '.$name.',</span>
		                                                     <br>
		                                                     <p style="ext-align: justify;
		                                                            font-style: normal;
		                                                            font-weight: 500;">
		                                                        Your password change link is:
		                                                     </p>
		                                                     <br>
		                                                        <a href="https://eva.enginecal.com/enginecal-reset-password/?qugtyhfr='.$uid.'" >Click Here to reset.</a>
		                                                     <br>
		                                                  </div>
		                                               </td>
		                                            </tr>
		                                         </tbody>
		                                      </table>
		                                   </td>
		                                </tr>
		                             </tbody>
		                          </table>
		                       </td>
		                    </tr>
		                    <tr>
		                       <td valign="top" bgcolor="#E5E5E5" align="center">&nbsp;</td>
		                    </tr>
		                    <tr>
		                       <td valign="top" bgcolor="#E5E5E5" align="center">
		                          <table width="600" cellspacing="0" cellpadding="3" border="0">
		                             <tbody>
		                                <tr>
		                                </tr>
		                                <tr>
		                                </tr>
		                             </tbody>
		                          </table>
		                       </td>
		                    </tr>
		                    <tr>
		                       <td valign="top" bgcolor="#E5E5E5" align="center">&nbsp;</td>
		                    </tr>
		                    <tr>
		                       <td valign="top" bgcolor="#fff" align="center">
		                          <span class="HOEnZb"><font color="#888888">
		                          </font></span><span class="HOEnZb"><font color="#888888">
		                          </font></span>
		                          <table width="600" cellspacing="10" cellpadding="10" border="0">
		                             <tbody>
		                                <tr>
		                                   <td>
		                                      <span class="HOEnZb"><font color="#888888">
		                                      </font></span><span class="HOEnZb"><font color="#888888">
		                                      </font></span>
		                                      <table align="center">
		                                         <tbody>
		                                            <tr>
		                                               <td>
		                                                 <p style="font-size: 14px;font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif;margin-top: 7px;">
		                                                     Copyright 2021 EngineCAL
		                                                     All right reserved
		                                                  </p>
		                                                  <span class="HOEnZb"><font color="#888888">
		                                                  </font></span>
		                                               </td>
		                                            </tr>
		                                         </tbody>
		                                      </table>
		                                      <span class="HOEnZb"><font color="#888888">
		                                      </font></span>
		                                   </td>
		                                </tr>
		                             </tbody>
		                          </table>
		                          <span class="HOEnZb"><font color="#888888">
		                          </font></span>
		                       </td>
		                    </tr>
		                </tbody>
		            	</table>
		            	</body>
		            </html>
		              ';
		            $from     = 'support@enginecal.com';
		            $headers  = 'MIME-Version: 1.0' . "\r\n";
		            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		             
		            // Create email headers
		            $headers .= 'From: '.$from."\r\n".
		                		'Reply-To: '.$from."\r\n" .
		                		'X-Mailer: PHP/' . phpversion();
		            
		            if(mail($email,"Reset Password Request",$msg,$headers))
		            {
			            $arrayData['success'] = TRUE;
			            $arrayData['message'] = "Reset password link sent to registered email id!";
		          	}
		          	else
		          	{
			            $arrayData['success']      = FALSE;
			            $arrayData['errorCode']    = "1002";
			            $arrayData['errorMessage'] = "Mail sending unsuccessful!";
		        	}
		    	}
	    	}
	    	else
	        {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1001";
	            $arrayData['errorMessage'] = "Invalid Input!";
	        }
	    }
        echo json_encode($arrayData);
  	}

    public function valcode(Request $request)
	{
		$header = getallheaders();
  		// print_r($header);die;
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$result['success']      = FALSE;
      		$result['errorCode']    = "1004";
      		$result['errorMessage'] = "Invalid Authentication";
  		} 
  		else
  		{
	        if(!empty($request->data))
	        {
	            $data  = json_decode($request->data, TRUE);
	            $acode = $data['acode'];
	            $mac   = $data['mac'];
	            $uby   = $data['uby'];

	            $devAct = Fetch::isActivationCodeValid($acode);
	            // print_r($devAct);
	            if(!empty($devAct))
	            {
	            	if($devAct[0]->status == 1)
	                {    
	            	    $saveData = Fetch::updateActCode($devAct[0]->id,$mac,$uby,'2');
	                    if($saveData)
	                    {
	                    	$result['success'] = TRUE;
	                    	$result['message'] = "Code verified";
	                    	// $result['expDate'] = date('d-m-Y',strtotime($devAct[0]->expdt));
	                    }
	                }
	                else
			        {
			        	$result['success']      = FALSE;
			            $result['errorCode']    = "1002";
			            $result['errorMessage'] = "Invalid Activation Code!";
			        } 
	            }
	            else
	            {
	            	
					$result['success']      = FALSE;
		            $result['errorCode']    = "1002";
		            $result['errorMessage'] = "Invalid Activation Code!";
	           	}
	        }
	        else
	        {
	            $result['success']      = FALSE;
	            $result['errorCode']    = "1001";      
	            $result['errorMessage'] = "Invalid Input!";   
	        } 
	    }
        echo json_encode($result);  
    }

    public function drivescore(Request $request)
  	{
  		if(!empty($request->data))
	    {
	        $data     = json_decode($request->data, TRUE);
	        $deviceID = $data['devID'];
	        $devInfo  = Fetch::getdevice($deviceID);
	        
	        if(!empty($devInfo))
	        {
	    		$arrayData['success']  = TRUE;
	        	$score        = Fetch::getEdasScore($deviceID);
		        $rating       ='';
		        $scoreComment = [];
		        if(!empty($score[0]->drv_score_cmts))
		        {
		        	$rating = explode(',', $score[0]->drv_score_cmts);
		        
			        foreach ($rating as $key => $value) 
			        {
			        	// echo $value;
			        	$scoreCommentarr = Fetch::getEdasComments($value);
			        	if(!empty($scoreCommentarr))
			        	{
			        		$scoreComment[] = array('category' =>$scoreCommentarr[0]->category, 'rate' =>$scoreCommentarr[0]->rating ,'comment'=>$scoreCommentarr[0]->comments,'exp'=>$scoreCommentarr[0]->explanation );
			        	}
			        }
			    }
        		// print_r($scoreComment);
		        if(!empty($score[0]->drv_score))
		        {
		        	$arrayData['drvScore'] = $score[0]->hlt_score;
		        	if($score[0]->drv_score == 100)
			        {
			        	$scoreComment[] = array('category' =>'','rate' =>'' ,'comment'=>'Excellent driving','exp'=>'Keep it up.' );
			        }
			        $arrayData['edasScore'] = $score[0]->drv_score;
		        }
		        else
		        {
		        	$arrayData['drvScore']     = 0;
		        	$arrayData['healthScore'] = 0;
		        }
		       
	        	// $arrayData['drvScore'] = round($drvScore);
	        	$arrayData['comments'] = $scoreComment;
	        	// $arrayData['drvScore'] = 72;
	        }
	        else
	        {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Invalid Device ID!";
	        }
		}
	    else
	    {
	        $arrayData['success']      = FALSE;
	        $arrayData['errorCode']    = "1001";
	        $arrayData['errorMessage'] = "Invalid Input!";
	    }
	    echo json_encode($arrayData);
  	}

    public function vehicle_model(Request $request)
	{
        if(!empty($request->data))
        {
            $data = json_decode($request->data, TRUE);
            $mfd  = $data['mfd'];
            $res  = Fetch::getVehicleSetting();
            $mfdList    = explode(';',$res->manufacturer);
            $engCapList = explode(';',$res->engine_capacity);
            if(in_array($mfd, $mfdList))
            {
            	$arrayData['success'] = TRUE;
            	$keyMfd = array_search($mfd, $mfdList);
            	$resM   = Fetch::getVehicleInfoDataById($keyMfd);
            	$fuel   = array('','Petrol','Diesel');
                if(count($resM) >= 1)
	            {
	                $arrayData['model'] = explode(';',$resM[0]->model);
	            }
            }
            else
            {
                $arrayData['success']      = FALSE;
                $arrayData['errorCode']    = "1002";
                $arrayData['errorMessage'] = "Manufacturer is not in the list!";
            }
        }//if(!empty($request->data))
        else
        {
           $arrayData['success']      = FALSE;
           $arrayData['errorCode']    = "1001";
           $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }

	public function vehicle_spec(Request $request)
	{
		if(!empty($request->data))
        {
        	$data = json_decode($request->data, TRUE);
        	$mfd  = $data['mfd'];
        	$mod  = $data['model'];
        	$res  = Fetch::getVehicleSetting();
        	$mfdList    = explode(';',$res->manufacturer);
        	$engCapList = explode(';',$res->engine_capacity);
        	$fuel       = array('','Petrol','Diesel');

         	if(in_array($mfd, $mfdList))
          	{
	            $arrayData['success'] = TRUE;
	            $keyMfd  = array_search($mfd, $mfdList);
	            $resM    = Fetch::getVehicleInfoDataById($keyMfd);
	            $varList = array();
	            $modList = array();
	            $fuel    = array('','Petrol','Diesel');
	            if(count($resM) >= 1)
	            {
	            	$varList = explode(';',$resM[0]->var);
	            	$modList = explode(';',$resM[0]->model);
	            }
	            if($mod != "")
	            {
	              	if(in_array($mod, $modList))
	              	{
	                	$keyMode = array_search($mod, $modList);
	              	}
	              	else
	             	{
	                	$modNew++;
	               		$keyMode           = count($modList);
	               		$modList[$keyMode] = $mod;
	              	}
	            }

	            $dat = Fetch::getVehicleSpecDataByMfdModel($keyMfd,$keyMode);
	            $j   = 0;
	            if(count($dat) >= 1)
	            {
	              	foreach($dat as $data)
	              	{
	                	$arrayData['data'][$j]['fuelType']       = $fuel[$data->fuel];
	                	$arrayData['data'][$j]['varients']       = $varList[$data->varient];
	                	$arrayData['data'][$j]['engineCapacity'] = $engCapList[$data->engcap];
	            	    $arrayData['data'][$j]['bodyType']       = $data->btype;
	            	    $j++;
	            	}
	            }
	            else
	            {
	            	$arrayData['success']      = FALSE;
	            	$arrayData['errorCode']    = "1003";
	            	$arrayData['errorMessage'] = "Model is not in the list!";
	            }
        	}
	        else
	        {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Manufacturer is not in the list!";
	        }
        }
        else
        {
        	$arrayData['success']      = FALSE;
        	$arrayData['errorCode']    = "1001";
        	$arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }

	public function vehicle(Request $request)
	{
     	if(!empty($request->data))
        {
	     	$data     = json_decode($request->data, TRUE);
			$deviceID = $data['veh_basic']['deviceid'];
			$devInfo  = Fetch::getdevice($deviceID);
			// print_r($devInfo);die;
			$errorMF  = "";
			if(!empty($devInfo))
			{
			    $did      = $devInfo[0]->id;
			    $prof     = $data['veh_basic'];
			    $drvInfo  = Fetch::getVehicleById($did);
			    $settInfo = Fetch::getVehicleSetting();
			    // print_r($devInfo[0]->device_id);die;
			    $mfd = explode(';',$settInfo->manufacturer);
			    $mfd = array_map('strtolower',$mfd);
			    $cap = explode(';',$settInfo->engine_capacity);
			    $cap = array_map('strtolower',$cap);
			    $drvInfo = ($drvInfo != "") ? $drvInfo : array();
			    $drvInfo = array();
			   
			    $drvInfo['device']   = $devInfo[0]->id;
			    $drvInfo['reg_no']   = $prof['veh_registration'];
			    $drvInfo['manufact'] = "";
			    $myMfd               = strtolower(trim($prof['veh_manufacturer']));
			    // print_r($mfd);
			    // print_r($myMfd);die;
			    //Checek if valid manufacturer
			    if(in_array($myMfd,$mfd))
			    {
				    $indexM              = array_search($myMfd,$mfd);  
			        $drvInfo['manufact'] = $indexM;

			        $modelList = array();
			    	$varlList  = array();
			        $resM      = Fetch::getVehicleInfoDataById($indexM);
				    if(count($resM) >= 1)
				    {
				        //$arrayData['varients'] = explode(';',$resM[0]['var']);
				        $modelList = explode(';',$resM[0]->model);
				        $varlList  = explode(';',$resM[0]->var);
				    }

				    //Checek if valid model
				    if(in_array($prof['veh_model'],$modelList))
				    {
				        $indexMod           = array_search($prof['veh_model'],$modelList);  
				        $drvInfo['modelid'] = $indexMod;
				        $drvInfo['model']   = $indexMod;

				        //Checek if valid varient
				        if(in_array($prof['veh_varient'],$varlList))
				        {
				            $indexVar             = array_search($prof['veh_varient'],$varlList); 
				            $drvInfo['varientid'] = $indexVar;
				            $drvInfo['varient']   = $indexVar;

				            //Check for valid engine capacity
				            $myCap                = strtolower(trim($prof['engine_capacity']));
						    if(in_array($myCap,$cap))
						    {
						        $indexM             = array_search($myCap,$cap);  
						        $drvInfo['eng_cap'] = $indexM;

						        $drvInfo['fuel_tpe']        = (trim($prof['fuel_type']) == "Petrol") ? '1' : '2';
						        $drvInfo['manufact_yr']     = $prof['mfg_year'];
						        $drvInfo['travel_purchase'] = $prof['odo'];
						        if(array_key_exists('veh_transmission', $prof))
						        {
						        	$trans_type = $prof['veh_transmission'];
									if($trans_type == 2)
									{
										$drvInfo['trans_type'] = "Automatic";
									}
									else
									{
										$drvInfo['trans_type'] = "Manual";
									}
						        }
						        else
						        {
						        	$drvInfo['trans_type'] = "Manual";
						        }
						        $drvInfo['purchase_dt']     = date('Y-m-d');
						        $drvInfo['last_serv_dt']    = date('Y-m-d');
						        $drvInfo['oil_dt']          = date('Y-m-d');
						        //to add last service kms in multiples of 10000
						        $a = strlen($prof['odo']);
						        if($a >= 5)
						        {
						        	$val = round($prof['odo'],-($a - 1));
						        }
						        else
						        {
						            $val = 0;
						        }
						        $drvInfo['travel_service'] = $val;
						        $drvInfo['oil_dist']       = $val;
						    }
						    else
						    {
					            $errorMF .= $prof['engine_capacity'] . " engine capacity is not available";
					        }
				        }
				        else
				        {
				            $errorMF = $prof['veh_varient'] . " varient is not available";
				        }
				    }
			        else
			        {
			        	$errorMF = $prof['veh_model'] . " model is not available";
			        }
			    }
			    else
			    {
			        $errorMF = $prof['veh_manufacturer'] . " manufacturer is not available";
			    }
			    
	            if($errorMF != "")
	            {
				    $arrayData['success']      = FALSE;
				    $arrayData['errorCode']    = "1003";
				    $arrayData['errorMessage'] = "Input data Incorrect. $errorMF!";
			    }
	    		else
	    		{
	    			$setFields = array('device');
					$setVal    = array($did);
					$table     = "vehicles";
	    			$saveData  = Fetch::updateDataMultiple($table,$setFields,$setVal,$drvInfo);
			        // if($saveData)
			        // {	
			        	$setSpecs = Fetch::setvehSpec($drvInfo['manufact'],$drvInfo['model'],$drvInfo['fuel_tpe'],$drvInfo['varient'],$drvInfo['eng_cap'],$drvInfo['manufact_yr'],$did);
			        	if($setSpecs)
			        	{
			        		$arrayData['success'] = TRUE;
				        	$arrayData['message'] = "Data updated successfully!";
			        	}
			        	else
				        {
					        $arrayData['success']      = FALSE;
					        $arrayData['errorCode']    = "1004";
					        $arrayData['errorMessage'] = "Error in adding data. Try again Later!";
				        }
			        // }
			        // else
			        // {
				       //  $arrayData['success']      = FALSE;
				       //  $arrayData['errorCode']    = "1003";
				       //  $arrayData['errorMessage'] = "Error in adding data. Data already available!";
			        // }
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
		    $arrayData['success']      = FALSE;
		    $arrayData['errorCode']    = "1001";
		    $arrayData['errorMessage'] = "Invalid Input!";
		}
        echo json_encode($arrayData);
    }

	public function bluetoothkey(Request $request)
	{
		if(!empty($request->data))
        {    
            $obj      = json_decode($request->data, TRUE);
			$deviceID = trim($obj['macID']);
			$apiKey   = $obj['apiKey'];
			// $vKey  = $obj['eKey'];
			$datAct   = Fetch::randKeyVerify($apiKey);
			// print_r($datAct[0]->count);die;
			if($datAct[0]->count)
			{
				if($deviceID != "")
				{
					$arrayData['success'] = TRUE;
					
					// $tm = strtotime(date('Y-m-d'));
					// print_r($tm);die;
					// $tm = $tm*$deviceID;
					// $tm = "$tm";
					// $len = strlen($tm);
					// $len -= 8;
					// $devKey = substr($tm, $len, 4);
					//echo "devKey = $devKey \n";
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid API Key!";
			}
        }
        else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($arrayData);	
		echo $resultVal;
	}

	public function getcalvalues(Request $request)
 	{
 		if(!empty($request->data))
        {
         	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			
			// Get Device Data
			$devInfo = Fetch::getdevice($deviceID);

			// Get Vehicle details
			// $vehi  = Fetch::getVehicleById($did);
			// $vType = $vehi->fuel_tpe;
			$encryptKey = rand(25, 300);
			if(!empty($devInfo))
			{
				$list = array("Eng_eff_C","Vol_eff_C","Eng_Disp_C","Eff_RPM_Limit1_C","Eff_RPM_Limit2_C","Eff_RPM_Limit3_C","Eff_RPM_Limit4_C","Eff_RPM_Limit5_C","IC_Status_C","TC_Status_C","Injection_Type_C","VVT_Status_C","Fact_Fuel_Run_C","Fact_Fuel_Idle_C","MAP_Valid_C","MAF_Valid_C","VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C","VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C","VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C","VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C","VolEff_Eload_31_40_RPMLimit2_C","VolEff_Eload_31_40_RPMLimit3_C","VolEff_Eload_31_40_RPMLimit4_C","VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C","VolEff_Eload_41_50_RPMLimit3_C","VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C","VolEff_Eload_51_60_RPMLimit2_C","VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C","VolEff_Eload_61_80_RPMLimit1_C","VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C","VolEff_Eload_61_80_RPMLimit5_C","VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C","VolEff_Eload_81_90_RPMLimit4_C","VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C","VolEff_Eload_91_94_RPMLimit3_C","VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C","VolEff_Eload_95_98_RPMLimit2_C","VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C","VolEff_Eload_99_100_RPMLimit1_C","VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C","VolEff_Eload_99_100_RPMLimit5_C","Eqratio_Eload_1_10_RPMLimit1_C","Eqratio_Eload_1_10_RPMLimit2_C","Eqratio_Eload_1_10_RPMLimit3_C","Eqratio_Eload_1_10_RPMLimit4_C","Eqratio_Eload_1_10_RPMLimit5_C","Eqratio_Eload_11_20_RPMLimit1_C","Eqratio_Eload_11_20_RPMLimit2_C","Eqratio_Eload_11_20_RPMLimit3_C","Eqratio_Eload_11_20_RPMLimit4_C","Eqratio_Eload_11_20_RPMLimit5_C","Eqratio_Eload_21_30_RPMLimit1_C","Eqratio_Eload_21_30_RPMLimit2_C","Eqratio_Eload_21_30_RPMLimit3_C","Eqratio_Eload_21_30_RPMLimit4_C","Eqratio_Eload_21_30_RPMLimit5_C","Eqratio_Eload_31_40_RPMLimit1_C","Eqratio_Eload_31_40_RPMLimit2_C","Eqratio_Eload_31_40_RPMLimit3_C","Eqratio_Eload_31_40_RPMLimit4_C","Eqratio_Eload_31_40_RPMLimit5_C","Eqratio_Eload_41_50_RPMLimit1_C","Eqratio_Eload_41_50_RPMLimit2_C","Eqratio_Eload_41_50_RPMLimit3_C","Eqratio_Eload_41_50_RPMLimit4_C","Eqratio_Eload_41_50_RPMLimit5_C","Eqratio_Eload_51_60_RPMLimit1_C","Eqratio_Eload_51_60_RPMLimit2_C","Eqratio_Eload_51_60_RPMLimit3_C","Eqratio_Eload_51_60_RPMLimit4_C","Eqratio_Eload_51_60_RPMLimit5_C","Eqratio_Eload_61_80_RPMLimit1_C","Eqratio_Eload_61_80_RPMLimit2_C","Eqratio_Eload_61_80_RPMLimit3_C","Eqratio_Eload_61_80_RPMLimit4_C","Eqratio_Eload_61_80_RPMLimit5_C","Eqratio_Eload_81_90_RPMLimit1_C","Eqratio_Eload_81_90_RPMLimit2_C","Eqratio_Eload_81_90_RPMLimit3_C","Eqratio_Eload_81_90_RPMLimit4_C","Eqratio_Eload_81_90_RPMLimit5_C","Eqratio_Eload_91_94_RPMLimit1_C","Eqratio_Eload_91_94_RPMLimit2_C","Eqratio_Eload_91_94_RPMLimit3_C","Eqratio_Eload_91_94_RPMLimit4_C","Eqratio_Eload_91_94_RPMLimit5_C","Eqratio_Eload_95_98_RPMLimit1_C","Eqratio_Eload_95_98_RPMLimit2_C","Eqratio_Eload_95_98_RPMLimit3_C","Eqratio_Eload_95_98_RPMLimit4_C","Eqratio_Eload_95_98_RPMLimit5_C","Eqratio_Eload_99_100_RPMLimit1_C","Eqratio_Eload_99_100_RPMLimit2_C","Eqratio_Eload_99_100_RPMLimit3_C","Eqratio_Eload_99_100_RPMLimit4_C","Eqratio_Eload_99_100_RPMLimit5_C","Ovr_Run_EqRatio_C","Ovr_Run_VolEff_C","Ovr_Run_FuelFact_C","Ovr_Run_FuelFact_Eload_C","Fact_Fuel_Run_ELoad_0_20_C","Fact_Fuel_Run_ELoad_20_40_C","Fact_Fuel_Run_ELoad_40_60_C","Fact_Fuel_Run_ELoad_60_80_C","Fact_Fuel_Run_ELoad_80_100_C","Fact_Fuel_Idle_ELoad1_C","Fact_Fuel_Idle_ELoad2_C","Baro_Press_City_C","Base_OvrHt_Cool_Lmt_C","Base_MAP_Abs_Lmt_C","Base_MAP_Dif_Lmt_C","Base_MAP_Air_Lmt_C","Base_CRP_Lmt_C","Railp_Fact_C","MAP_Type_C","RPM_Idle_Cold_C","MAP_Idle_Cold_C","RPM_Idle_Warm_C","MAP_Idle_Warm_C","ELoad_Idle_Warm_C","RPM_Idle_Ld1_C","MAP_Idle_Ld1_C","ELoad_Idle_Ld1_C","RPM_Idle_Ld2_C","MAP_Idle_Ld2_C","ELoad_Idle_Ld2_C","Thr_DefPos_C","Thr_MaxPos_C","Thr_IdlePos_C","AccPed_DefPos_C","AccPed_MaxPos_C","Eload_Def_C","Eload_Max_C","EGR_Cmd_WrmIdle_C","Trq_EloadOffset_PkLmt_C","MAP_RPMOffset_PkLmt_C","MAP_Air_RPMOffset_PkLmt_C","VehSpeed_RPMLmt1_C","VehSpeed_RPMLmt2_C","Ovr_Run_ELoad_C","Dist_AC_Warn_C","Fuelling_RPM_C","CoolTemp_Fuel1_C","EngLoad_Fuel_Low1_C","EngLoad_Fuel_High1_C","CoolTemp_Fuel2_C","EngLoad_Fuel_Low2_C","EngLoad_Fuel_High2_C","CoolTemp_Fuel3_C","EngLoad_Fuel_Low31_C","EngLoad_Fuel_High31_C","EngLoad_Fuel_Low32_C","EngLoad_Fuel_High32_C","EngLoad_Fuel_Low33_C","EngLoad_Fuel_High33_C","EngLoad_Fuel_Low41_C","EngLoad_Fuel_High41_C","EngLoad_Fuel_Low42_C","EngLoad_Fuel_High42_C","EngLoad_Fuel_Low43_C","EngLoad_Fuel_High43_C","VehSpeed_Fuel4_C","Turbotimer_Coolt1_C","Turbotimer_RPM1_C","Turbotimer_VehSpeed1_C","Turbotimer_Cool2_C","Turbotimer_RPM2_C","Turbotimer_VehSpeed2_C","Coasting_RPM_C","Coasting_VehSpd1_C","Coasting_VehSpd2_C","Speedbump_RPM_factor_C","Speedbump_Load_C","TakeOff_Load_C","TakeOff_Load_UL_C","TakeOff_Load_LL_C","Take_Off_RPM_factor_C","GearM_Dwn3Gr_VehSpd1_C","GearM_Dwn3Gr_VehSpd2_C","GearM_Dwn3Gr_LOAD_C","GearM_Dwn3Gr_RPM1_C","GearM_Dwn3Gr_RPM2_C","Debounce_3DOWN_C","GearM_Dwn4Gr_VehSpd1_C","GearM_Dwn4Gr_VehSpd2_C","GearM_Dwn4Gr_LOAD_C","GearM_Dwn4Gr_RPM1_C","GearM_Dwn4Gr_RPM2_C","Debounce_4DOWN_C","GearM_Up2Gr_VehSpd1_C","GearM_Up2Gr_VehSpd2_C","GearM_Up2Gr_RPM_C","GearM_Up2Gr_ELoad1_C","GearM_Up2Gr_ELoad2_C","Debounce_2UP_C","GearM_Up3Gr_VehSpd1_C","GearM_Up3Gr_VehSpd2_C","GearM_Up3Gr_RPM_C","GearM_Up3Gr_ELoad1_C","GearM_Up3Gr_ELoad2_C","Debounce_3UP_C","Cold_drive_RPM_C","Time_Upld_Frq_C","Cool_Lmt_C","Max_Torque_C","MAP_Abs_PkLmt3_C","MAP_Dif_PkLmt3_C","B_MAP_Run_Load_C","B_MAP_Run_DEBOUNCE_C","B_MAP_Run2_Load_C","B_MAP_Run2_DEBOUNCE_C","B_MAP_Run2_PFact_C","B_MAP_Idle_PFact_C","B_MAP_Idle_DEBOUNCE_C","PID_Query_Pattern_Type_C","Veh_Spd_Std_Trq_C","Accl_Val_C","Accl_Eload_C","Decl_Val_C","Drv_Score_Fuelling_Count_Lmt_C","gr_1_low_C","gr_1_high_C","gr_2_low_C","gr_2_high_C","gr_3_low_C","gr_3_high_C","gr_4_low_C","gr_4_high_C","gr_5_low_C","gr_5_high_C","gr_6_low_C","gr_6_high_C","gear_Fact_C","gear_Confidence_C","volt_low_C","Hlf_Clutch_Thr_C","Veh_Spd_Avl_C","Peak_Rpm_Eol_C","Cruise_6Rpm_Eol_C","Cruise_5Rpm_Eol_C","Cruise_4Rpm_Eol_C","Cruise_3Rpm_Eol_C","Cruise_2Rpm_Eol_C","Cruise_1Rpm_Eol_C","Cruise_Rpm1_Eol_C","Cruise_Rpm2_Eol_C","Cruise_Rpm3_Eol_C","Cruise_Rpm4_Eol_C","Cruise_Rpm5_Eol_C","Cruise_Rpm6_Eol_C","Cruise_Spd_Eol_C","DPF_Avl_C","MAP_Warm_Idle_Lmt_C","ELoad_Warm_Idle_Lmt_C","RPM_Warm_Idle_Lmt_c","Time_Cruise_Idle_C","Thr_DefPos_EOL_C","RPM_DrvFric_Lmt_EOL_C","EngTrq_FC_Low_EOL_C","EngTrq_FC_High_EOL_C","MAP_FC_Low_EOL_C","MAP_FC_High_EOL_C ","ELoad_FC_Low_EOL_C","ELoad_FC_High_EOL_C","Cruise_Spd_EOL_C","Cruise_Spd_EOL_Lmt_C","Bat_AltChk_EOL_C","Bat_AltChk_Dif_EOL_C","Cool_Lmt_EOL_C","Cool_OvrLmt_Dif_EOL_C","VehSpd_LowLmt_Dif_EOL_C","Cool_HighLmt_Dif_EOL_C","Cool_LowLmt_Dif_EOL_C","VehSpd_HighLmt_Dif_EOL_C","Bat_Cool_Lmt1_C","Bat_Cool_Lmt2_C","Bat_UnderCoolLmt1_Lmt1_C","Bat_UnderCoolLmt1_Lmt2_C","Bat_UnderCoolLmt1_Lmt3_C","Bat_UnderCoolLmt1_Lmt4_C","Bat_OverCoolLmt1_Lmt1_C","Bat_OverCoolLmt1_Lmt2_C","Bat_OverCoolLmt1_Lmt3_C","Bat_OverCoolLmt1_Lmt4_C","Bat_OverCoolLmt2_Lmt1_C","Bat_OverCoolLmt2_Lmt2_C","Bat_OverCoolLmt2_Lmt3_C","Bat_OverCoolLmt2_Lmt4_C","TrkDist_EOL_C","Cruise_RPM_FC_EOL_UL_C","Cruise_RPM_FC_EOL_LL_C","Gear_FC_EOL_C");
				
				$arrayData['success'] = TRUE;

				$data1   = Fetch::getCaliPramVal($deviceID);
				$dataArr = $data1['calval'];
				$dataArr = json_decode($dataArr, TRUE);
				for($i = 0;$i < count($list);$i++)
				{
					$val             = trim($list[$i]);
					$arrayData[$val] = $dataArr[$val];
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
		    $arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData,JSON_NUMERIC_CHECK);	
    }

	public function calibration(Request $request)
 	{
 		if(!empty($request->data))
    	{
         	$json     = $request->data;
			$obj      = json_decode($json,true);
			$deviceID = $obj['devID'];
			$devInfo  = Fetch::getdevice($deviceID);

			if(!empty($devInfo))
			{
				if(($obj['calibration'] == 1) || ($obj['calibration'] == 0))
				{
					$saveData = Fetch::updateTableData("vehicles",$devInfo[0]->id,$obj['calibration']);
                    if($saveData)
                    {
                    	$arrayData['success'] = TRUE;
                    	$arrayData['message'] = "Calibration status set successfully!";
                    }
                    else
					{
						$arrayData['success']      = FALSE;
		                $arrayData['errorCode']    = "1003";
		                $arrayData['errorMessage'] = "Calibration status not set!";
					}
				}
				else
				{
					$arrayData['success']      = FALSE;
	                $arrayData['errorCode']    = "1001";
	                $arrayData['errorMessage'] = "Invalid Input!";
				}
			}
            else
            {
                $arrayData['success']      = FALSE;
                $arrayData['errorCode']    = "1002";
                $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }

    public function statistics(Request $request)
 	{
        if(!empty($request->data))
        {
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $devInfo  = Fetch::getdevice($deviceID);

            if(!empty($devInfo))
            {
	            $yVal = 0;
	            if($data['type'] != 'date')
	            {
	                $reqFor   = $data['driveno'];
	                $durTr    = Fetch::getDriveStat($deviceID,$reqFor,'dur','sum');
	            	$distTr   = Fetch::getDriveStat($deviceID,$reqFor,'dist');
	                $avSpeed  = Fetch::getDriveStat($deviceID,$reqFor,'avg_speed','avg');
	                $maxSpeed = Fetch::getDriveStat($deviceID,$reqFor,'max_sp','max');
	                $totCon   = Fetch::getDriveStat($deviceID,$reqFor,'ful_c','sum');
	                $totMil   = Fetch::getDriveStat($deviceID,$reqFor,'avg_ml','avg'); // was avg
	                $maxTor   = Fetch::getDriveStat($deviceID,$reqFor,'max_tor','max');
	                $fOver    = Fetch::getDriveStat($deviceID,$reqFor,'ful_ovr','sum');
	                $idT      = Fetch::getDriveStat($deviceID,$reqFor,'idt','sum');
	                $ovT      = Fetch::getDriveStat($deviceID,$reqFor,'dur_ovr','sum');
	                $ovD      = Fetch::getDriveStat($deviceID,$reqFor,'dist_ovr','sum');
	                $PeakTorq = Fetch::getDriveStat($deviceID,$reqFor,'maxvehtrq','max');
	                if($PeakTorq == 0)
	                {
	                	$vehData      = Fetch::getVehicleDataById($devInfo[0]->id);
			            $calval       = json_decode($vehData->calval);
				        $Max_Torque_C = $calval->Max_Torque_C;
	                	$PeakTorq     = $Max_Torque_C;
	                }

	                $arrayData['success']        = TRUE;
                  	$arrayData['type']           = $data['type'];
                  	$arrayData['drive_duration'] = $durTr[0]->val;
                  	$arrayData['drive_dist']     = round($distTr[0]->val,4);
                  	$arrayData['avg_speed']      = round($avSpeed[0]->val,2);
                 	$arrayData['max_speed']      = round($maxSpeed[0]->val,0);
                 	$arrayData['fuel_consumed']  = round($totCon[0]->val,4);
                  	$arrayData['mileage']        = round($totMil[0]->val,4);
                  	$arrayData['dist_ovr']       = round($ovD[0]->val,2);
                  	$arrayData['fuel_save_ovr']  = round($fOver[0]->val,3);
                  	$arrayData['idle_time']      = round($idT[0]->val);
                  	$arrayData['time_ovr']       = round($ovT[0]->val);
                  	$arrayData['peak_tor']       = round($maxTor[0]->val,2);//round($PeakTorque[0]->val,2);
                  	$arrayData['max_tor']        = $PeakTorq;
                  	// $arrayData['ref_tor']        = $PeakTorq; mon_ref_trq
	                $yVal = 1;
	            }
            	elseif($data['type'] == 'date')
	            {
	                $dtFA                 = strtotime(str_replace(':', '-', $data['startdate']));
	                $dtTA                 = strtotime(str_replace(':', '-', $data['enddate']));
	                $fr                   = date('Y-m-d',$dtFA);
	                $to                   = date('Y-m-d',$dtTA);
	                $arrayData['success'] = TRUE;
	                $arrayData['type']    = $data['type'];
	                $arrayData['data']    = array();
	                $dtI                  = 0;
	                
	                while($dtFA <= $dtTA)
	                {
	                  	$fr       = date('Y-m-d 00:00:00',$dtFA);
	                  	$to       = date('Y-m-d 23:59:59',$dtFA);  // Pick stats for one day and increment at the end for next day
	                  	$mADt     = date('d-m-Y',$dtFA);
	                  	$arrayData['data'][$dtI] = array();
	                  	$durTr    = Fetch::getDriveStatRange($deviceID,$fr,$to,'dur');
	                  	$distTr   = Fetch::getDriveStatRange($deviceID,$fr,$to,'dist');
	                  	$avSpeed  = Fetch::getDriveStatRange($deviceID,$fr,$to,'avg_speed','avg');
	                  	$maxSpeed = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_sp','max');
	                 	$totCon   = Fetch::getDriveStatRange($deviceID,$fr,$to,'ful_c');
	                  	$totMil   = Fetch::getDriveStatRange($deviceID,$fr,$to,'avg_ml','avg'); // was avg
	                  	$maxTor   = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_tor','max');
	                  	$fOver    = Fetch::getDriveStatRange($deviceID,$fr,$to,'ful_ovr');
	                  	$idT      = Fetch::getDriveStatRange($deviceID,$fr,$to,'idt');
	                  	$ovT      = Fetch::getDriveStatRange($deviceID,$fr,$to,'dur_ovr');
	                  	$ovD      = Fetch::getDriveStatRange($deviceID,$fr,$to,'dist_ovr');
	                  	$PeakTorq = Fetch::getDriveStatRange($deviceID,$fr,$to,'maxvehtrq','max');
	                  	
	                  	$arrayData['data'][$dtI]['date']           = $mADt;
	                  	$arrayData['data'][$dtI]['drive_duration'] = round($durTr[0]->val,0);
	                  	$arrayData['data'][$dtI]['drive_dist']     = round($distTr[0]->val,4);
	                 	$arrayData['data'][$dtI]['avg_speed']      = round($avSpeed[0]->val,2);
	                  	$arrayData['data'][$dtI]['max_speed']      = round($maxSpeed[0]->val,0);
	                  	$arrayData['data'][$dtI]['fuel_consumed']  = round($totCon[0]->val,4);
	                  	$arrayData['data'][$dtI]['mileage']        = round($totMil[0]->val,4);
	                  	$arrayData['data'][$dtI]['peak_tor']       = round($maxTor[0]->val,2);
	                  	$arrayData['data'][$dtI]['fuel_save_ovr']  = round($fOver[0]->val,3);
	                  	$arrayData['data'][$dtI]['idle_time']      = round($idT[0]->val,0);
	                  	$arrayData['data'][$dtI]['time_ovr']       = round($ovT[0]->val,0);
	                  	$arrayData['data'][$dtI]['dist_ovr']       = round($ovD[0]->val,2);
	                  	$arrayData['data'][$dtI]['max_tor']        = round($PeakTorq[0]->val,2);
	                  	$dtFA += (60*60*24);
	                  	$dtI++;
	                }
	                $yVal = 1;
              	}
              	else
	            {
	                $arrayData['success']      = FALSE;
	                $arrayData['errorCode']    = "1003";
	                $arrayData['errorMessage'] = "Invalid Type!";
	            }
        	}
	        else
	        {
	          	$arrayData['success']      = FALSE;
	          	$arrayData['errorCode']    = "1002";
	          	$arrayData['errorMessage'] = "Invalid Device ID!";
	        }
	    }
	    else
	    {
	        $arrayData['success']      = FALSE;
	        $arrayData['errorCode']    = "1001";
	        $arrayData['errorMessage'] = "Invalid Input!";
	    }
		echo json_encode($arrayData);
	}

	public function statisticsall(Request $request)
 	{
        if(!empty($request->data))
        {
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $drive    = $data['drive'];
            $devInfo  = Fetch::getdevice($deviceID);
            
            if(!empty($devInfo))
            {
                $date = 0;
                $i    = 0;
                $j    = 0;

                $dataAll      = Fetch::getDriveStatRangeCaiLite($deviceID,$drive);
                // print_r($dataAll);die;
                if (!empty($dataAll))
                {
	                $arrayData['success'] = TRUE;
			        $arrayData['data']    = array();
	                foreach ($dataAll as $key => $value)
	                {
	                	if($date == 0)
	                	{
	                		$date = date('Y-m-d',$value->time_s);
	                	}
	                	else
	                	{
	                		//Do nothing
	                	}

	                	if($date == date('Y-m-d',$value->time_s))
	                	{
	                		//Do nothing
	                	}
	                	else
	                	{
	                		$date = date('Y-m-d',$value->time_s);
	                		$i++;
	                		$j    = 0;
	                	}

	                	$arrayData['data'][$i][$j]['date']           = date('Y-m-d',$value->time_s);
                		$arrayData['data'][$i][$j]['driveid']        = $value->nodrive;
						$arrayData['data'][$i][$j]['drive_duration'] = round($value->dur,0);
						$arrayData['data'][$i][$j]['drive_dist']     = round($value->dist,4);
						$arrayData['data'][$i][$j]['avg_speed']      = round($value->avg_speed,2);
						$arrayData['data'][$i][$j]['max_speed']      = round($value->max_sp,0);
						$arrayData['data'][$i][$j]['fuel_consumed']  = round($value->ful_c,4);
						$arrayData['data'][$i][$j]['mileage']        = round($value->avg_ml,4);
						$arrayData['data'][$i][$j]['peak_tor']       = round($value->max_tor,2);
						$arrayData['data'][$i][$j]['fuel_save_ovr']  = round($value->ful_ovr,3);
						$arrayData['data'][$i][$j]['idle_time']      = round($value->idt,0);
						$arrayData['data'][$i][$j]['time_ovr']       = round($value->dur_ovr,0);
						$arrayData['data'][$i][$j]['dist_ovr']       = round($value->dist_ovr,2);
						$arrayData['data'][$i][$j]['running_dur']    = ($value->dur) - ($value->dur_key_on);
						if(($value->maxvehtrq) == 0)
		                {
		                	$vehData      = Fetch::getVehicleDataById($devInfo[0]->id);
				            $calval       = json_decode($vehData->calval);
					        $Max_Torque_C = $calval->Max_Torque_C;
		                }
		                else
		                {
		                	$Max_Torque_C = $value->maxvehtrq;
		                }
						$arrayData['data'][$i][$j]['max_tor']        = round($Max_Torque_C,2);
						$arrayData['data'][$i][$j]['ref_tor']        = round($value->mon_ref_trq,2);
						

						$edas      = Fetch::getEDASScorebyDriveid($deviceID,$value->nodrive);
						if(!empty($edas))
						{
							$arrayData['data'][$i][$j]['hlt_score']      = $edas[0]->hlt_score;
							$arrayData['data'][$i][$j]['drv_score']      = $edas[0]->drv_score;
							$arrayData['data'][$i][$j]['drv_score_cmts'] = $edas[0]->drv_score_cmts;
							
						}
						else
						{
							$arrayData['data'][$i][$j]['hlt_score']      = "0";
							$arrayData['data'][$i][$j]['drv_score']      = "0";
							$arrayData['data'][$i][$j]['drv_score_cmts'] = "0,0,0";
						}
						// $arrayData['data'][$i][$j]['max_tor']        = round($value->maxvehtrq,2);

						$j++;
	                }
	            }
	            else
		        {
		          	$arrayData['success']      = FALSE;
		          	$arrayData['errorCode']    = "1003";
		          	$arrayData['errorMessage'] = "No Data Available!";
		        }
        	}
	        else
	        {
	          	$arrayData['success']      = FALSE;
	          	$arrayData['errorCode']    = "1002";
	          	$arrayData['errorMessage'] = "Invalid Device ID!";
	        }
	    }
	    else
	    {
	        $arrayData['success']      = FALSE;
	        $arrayData['errorCode']    = "1001";
	        $arrayData['errorMessage'] = "Invalid Input!";
	    }
		echo json_encode($arrayData);
	}

	public function statisticsByDriveIDRange(Request $request)
 	{
        if(!empty($request->data))
        {
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $driveSrt = $data['driveidstart'];
            $driveEnd = $data['driveidend'];
            $devInfo  = Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
                $date = 0;
                $i    = 0;
                $j    = 0;

                $dataAll      = Fetch::getDriveStatRangeCaiLiteLatest($deviceID,$driveSrt,$driveEnd);
                // print_r($dataAll);die;
                if (!empty($dataAll))
                {
	                $arrayData['success'] = TRUE;
			        $arrayData['data']    = array();
	                foreach ($dataAll as $key => $value)
	                {
	                	if($date == 0)
	                	{
	                		$date = date('Y-m-d',$value->time_s);
	                	}
	                	else
	                	{
	                		//Do nothing
	                	}

	                	if($date == date('Y-m-d',$value->time_s))
	                	{
	                		//Do nothing
	                	}
	                	else
	                	{
	                		$date = date('Y-m-d',$value->time_s);
	                		$i++;
	                		$j    = 0;
	                	}

	                	$arrayData['data'][$i][$j]['date']           = date('Y-m-d',$value->time_s);
                		$arrayData['data'][$i][$j]['driveid']        = $value->nodrive;
						$arrayData['data'][$i][$j]['drive_duration'] = round($value->dur,0);
						$arrayData['data'][$i][$j]['drive_dist']     = round($value->dist,4);
						$arrayData['data'][$i][$j]['avg_speed']      = round($value->avg_speed,2);
						$arrayData['data'][$i][$j]['max_speed']      = round($value->max_sp,0);
						$arrayData['data'][$i][$j]['fuel_consumed']  = round($value->ful_c,4);
						$arrayData['data'][$i][$j]['mileage']        = round($value->avg_ml,4);
						$arrayData['data'][$i][$j]['peak_tor']       = round($value->max_tor,2);
						$arrayData['data'][$i][$j]['fuel_save_ovr']  = round($value->ful_ovr,3);
						$arrayData['data'][$i][$j]['idle_time']      = round($value->idt,0);
						$arrayData['data'][$i][$j]['time_ovr']       = round($value->dur_ovr,0);
						$arrayData['data'][$i][$j]['dist_ovr']       = round($value->dist_ovr,2);
						$arrayData['data'][$i][$j]['running_dur']    = ($value->dur) - ($value->dur_key_on);
						if(($value->maxvehtrq) == 0)
		                {
		                	$vehData      = Fetch::getVehicleDataById($devInfo[0]->id);
				            $calval       = json_decode($vehData->calval);
					        $Max_Torque_C = $calval->Max_Torque_C;
		                }
		                else
		                {
		                	$Max_Torque_C = $value->maxvehtrq;
		                }
						$arrayData['data'][$i][$j]['max_tor']        = round($Max_Torque_C,2);
						$arrayData['data'][$i][$j]['ref_tor']        = round($value->mon_ref_trq,2);
						
						$edas      = Fetch::getEDASScorebyDriveid($deviceID,$value->nodrive);
						if(!empty($edas))
						{
							$arrayData['data'][$i][$j]['hlt_score']      = $edas[0]->hlt_score;
							$arrayData['data'][$i][$j]['drv_score']      = $edas[0]->drv_score;
							$arrayData['data'][$i][$j]['drv_score_cmts'] = $edas[0]->drv_score_cmts;
							
						}
						else
						{
							$arrayData['data'][$i][$j]['hlt_score']      = "0";
							$arrayData['data'][$i][$j]['drv_score']      = "0";
							$arrayData['data'][$i][$j]['drv_score_cmts'] = "0,0,0";
						}
						// $arrayData['data'][$i][$j]['max_tor']        = round($value->maxvehtrq,2);

						$j++;
	                }
	            }
	            else
		        {
		          	$arrayData['success']      = FALSE;
		          	$arrayData['errorCode']    = "1003";
		          	$arrayData['errorMessage'] = "No Data Available!";
		        }
        	}
	        else
	        {
	          	$arrayData['success']      = FALSE;
	          	$arrayData['errorCode']    = "1002";
	          	$arrayData['errorMessage'] = "Invalid Device ID!";
	        }
	    }
	    else
	    {
	        $arrayData['success']      = FALSE;
	        $arrayData['errorCode']    = "1001";
	        $arrayData['errorMessage'] = "Invalid Input!";
	    }
		echo json_encode($arrayData);
	}

	public function statistics_main(Request $request)
	{
        if(!empty($request->data))
        {    
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $devInfo  = Fetch::getdevice($deviceID);

            if(!empty($devInfo))
            {
                if($data['type'] == 'serviceDue')
                {
                	$servicedata                 = Fetch::getservicedue($deviceID);
                	// print_r($servicedata);die;
                	if(!empty($servicedata)){
	                	if($servicedata[0]->servduedayupDt != date('Y-m-d'))
	                	{
	                		$tday     = date('Y-m-d');
							$date2    = date_create($servicedata[0]->servduedayupDt);
							$date     = date_create($tday);
							$daysDiff = date_diff($date2,$date);
							$servicedueDays  = $servicedata[0]->serviceduedays-$daysDiff->days;
							$servicedueKm    =  10000 * $servicedueDays / 365;
							Fetch::updateduedaysapi($deviceID,$servicedueDays,$servicedueKm,$tday);
	                	}else
	                	{
	                		$servicedueDays = $servicedata[0]->serviceduedays;
	                		$servicedueKm   = $servicedata[0]->serviceduekm;
	                	}
                	}else{
                			$servicedueDays = '365';
	                		$servicedueKm   = '9999';
                	}
                	$arrayData['success']        = TRUE; 
                    $arrayData['serviceDue']     = strval(round($servicedueKm));
                    $arrayData['serviceDueDays'] = $servicedueDays;
                }
	            elseif($data['type'] == 'averageMileage')
	            {
	            	$arrayData['success']        = TRUE;
	                $numMil                      = Fetch::getStatsAverageMilage($deviceID);
	                $arrayData['averageMileage'] = round($numMil[0]->val,2);
	            }
	            elseif($data['type'] == 'averageDistancePerDay')
	            {
	                $numDays1 = Fetch::getStatsTotalNumDaysDriven($deviceID);
	                $retvalue = 0;
	            	if (trim($numDays1[0]->dt) != "")
	                {
	                    $retvalue = $numDays1[0]->dt + 0.0;
	                }
	                $numDays   = $retvalue;
	                $numDist1  = Fetch::getStatsTotalDistanceDriven($deviceID);
	                $retvalue1 = 0;
	             	if (trim($numDist1[0]->val) != "")
	                {
	                    $retvalue1 = $numDist1[0]->val + 0.0;
	                } 
	            	$numDist    = $retvalue1;     
	            	$numDistAvg = ($numDays >= 1)  ? $numDist / $numDays : 0;
	            	$numDist    = ($numDist <= 0) ? 0 : $numDist;

	            	$arrayData['success']               = TRUE;
	            	$arrayData['averageDistancePerDay'] = round($numDistAvg,2);
	          	}
	          	else
	         	{
	            	$arrayData['success']      = FALSE;
	            	$arrayData['errorCode']    = "1003";
	            	$arrayData['errorMessage'] = "Invalid Request Type!";
	        	}
            }
            else
	        {
	            $arrayData['success']      = FALSE; 
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Invalid Device ID!";
	        }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }

	public function device_location_drive(Request $request)
	{
        if(!empty($request->data))
        {    
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $devInfo  = Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
	            if(array_key_exists('driveId',$data))
	            {
	                $drivenum             = $data['driveId'];
	                $arrayData['success'] = TRUE;
	                $dataLoc              = Fetch::devDriveNumDataApi($deviceID,$drivenum);

	                if(count($dataLoc) >= 1)
	                {
	                 	$arrayData['data'] = $dataLoc;
	                }
	                else
	                {
	            	    $arrayData['success']      = FALSE;
	                    $arrayData['errorCode']    = "1003";
	                    $arrayData['errorMessage'] = "No location Info!";
	                }
	            }
	            elseif(array_key_exists('driveidstart',$data) && array_key_exists('driveidend',$data))
	            {
	            	$driverangefrm = $data['driveidstart'];
	                $driverangeto  = $data['driveidend'];
	                if($driverangefrm > $driverangeto)
	                {
	                	$arrayData['success']      = FALSE;
	                    $arrayData['errorCode']    = "1001";
	                    $arrayData['errorMessage'] = "Invalid Input!";
	                }
	                else
	                {
		                $dataLoc = Fetch::devDriveRangeDataApiCaiLite($deviceID,$driverangefrm,$driverangeto);
		                if(count($dataLoc) >= 1)
		                {
		                	$arrayData['success'] = TRUE;
		                	$arrayData['data']    = $dataLoc;
		                }
		                else
		                {
		                    $arrayData['success']      = FALSE;
		                    $arrayData['errorCode']    = "1003";
		                    $arrayData['errorMessage'] = "No location Info!";
		                }
	                }
                }
                else
                {
                  $arrayData['success']      = FALSE;
                  $arrayData['errorCode']    = "1001";
                  $arrayData['errorMessage'] = "Invalid Input!";
                }
            }
            else
            {
              $arrayData['success']      = FALSE;
              $arrayData['errorCode']    = "1002";
              $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
        	$arrayData['success']      = FALSE;
        	$arrayData['errorCode']    = "1001";
        	$arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData); 
	}

    public function getlastloc(Request $request)
	{
        if(!empty($request->data))
        {
	        $data     = json_decode($request->data, TRUE);
	        $deviceID = $data['devID'];
	        $devInfo  = Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
            	$lastDriveData = Fetch::getLastDriveNum($deviceID);
            	if(!empty($lastDriveData))
	            {
	            	$lastDriveID       = $lastDriveData[0]->drive;
	            	$lastDriveLocation = Fetch::getLastLocation($deviceID,$lastDriveID);
	            	// print_r($lastDriveLocation);die;
	            	if(!empty($lastDriveLocation))
	            	{
	            		$arrayData['success'] = TRUE;
		                $arrayData['lat']     = $lastDriveLocation[0]->lat;
		                $arrayData['long']    = $lastDriveLocation[0]->lon;
		                $arrayData['alt']     = $lastDriveLocation[0]->alt;
		                $arrayData['ts']      = $lastDriveLocation[0]->time_s;
	              	}
		            else
		            {
		                $arrayData['success']      = FALSE;
		                $arrayData['errorCode']    = "1004";
		                $arrayData['errorMessage'] = "No location info available!";
		            }
		        }
		        else
	            {
	                $arrayData['success']      = FALSE;
	                $arrayData['errorCode']    = "1003";
	                $arrayData['errorMessage'] = "No drives available!";
	            }
            }
            else
            {
                $arrayData['success']      = FALSE;
                $arrayData['errorCode']    = "1002";
                $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }

	public function statistics_drivedata(Request $request)
	{
		if(!empty($request->data))
        {
		    $data     = json_decode($request->data, TRUE);
		    $deviceID = $data['devID'];
		    $driveEnd = $data['driveno'];
		    $devInfo  = Fetch::getdevice($deviceID);
		    if(!empty($devInfo))
		    {
	           	$dat   = Fetch::getStatsByNumOfDrive($deviceID,$driveEnd);
	           	$i     = 0;
	           	if(!empty($dat))
	           	{
	           		foreach ($dat as $value)
		            {
		              	$reqFor             = $i; //$fr;
		             	$arrayData[$reqFor] = array();
		              	$dtFA      = $value->dt;
		              	$dtFA      = strtotime(str_replace(':', '-', $dtFA));
		              	$date      = date('d:m:Y',$dtFA);
		              	$durTr     = $value->dur;
		              	$distTr    = $value->dist;
		              	$avSpeed   = $value->avg_speed;
		              	$maxSpeed  = $value->max_sp;
		              	$totCon    = $value->ful_c;
		              	$totMil    = $value->avg_ml;
		              	$maxTor    = $value->max_tor;
		              	$fOver     = $value->ful_ovr;
		              	$idT       = $value->idt;
		              	$ovT       = $value->dur_ovr;
		              	//EC added extra parameter for dist in overrun
		              	$ovD       = $value->dist_ovr;
		              	$hacc      = $value->hacc;
		              	$hdcc      = $value->hdcc;
		              	$ovrspd    = $value->ovrspd;
		              	$vehstall  = $value->vehstall;
		              	$driveID   = $value->nodrive;
		              	$timestamp = strtotime($value->dfrm);
		              
		              	$arrayData[$reqFor]['driveno']        = $i + 1.0;
		              	$arrayData[$reqFor]['drive_id']       = $driveID + 0.0;
		              	$arrayData[$reqFor]['date']           = $date;
		              	$arrayData[$reqFor]['ts']             = $timestamp * 1000;
		              	$arrayData[$reqFor]['drive_duration'] = round($durTr,0);//$durTr;
		              	$arrayData[$reqFor]['drive_dist']     = round($distTr,4);
		              	$arrayData[$reqFor]['avg_speed']      = round($avSpeed,2);
		              	$arrayData[$reqFor]['max_speed']      = round($maxSpeed,0);//$maxSpeed;
		              	$arrayData[$reqFor]['fuel_consumed']  = round($totCon,4);
		              	$arrayData[$reqFor]['mileage']        = round($totMil,4);
		              	$arrayData[$reqFor]['max_tor']        = round($maxTor,2);
		              
		              	//EC added extra parameter for dist in overrun
		              	$arrayData[$reqFor]['dist_ovr']       = round($ovD,4);
		              	$arrayData[$reqFor]['fuel_save_ovr']  = round($fOver,3);
		              	$arrayData[$reqFor]['idle_time']      = round($idT,0);//$idT;
		              	$arrayData[$reqFor]['time_ovr']       = round($ovT,0);//$ovT;
		              	$arrayData[$reqFor]['hacc']           = round($hacc,0);
		              	$arrayData[$reqFor]['hdcc']           = round($hdcc,0);
		              	$arrayData[$reqFor]['ovrspd']         = round($ovrspd,0);
		              	$arrayData[$reqFor]['vehstall']       = round($vehstall,0);
		              	$i++;
		            }
		            $myData               = array();
		            $myData               = $arrayData;
		            $arrayData            = array();
		            $arrayData['data']    = $myData;
		            $arrayData['success'] = TRUE;
		            $arrayData['type']    = "drive";
		            $arrayData['count']   = count($myData);
	           	}
	            else
	            {
	            	for ($reqFor=0; $reqFor < $driveEnd; $reqFor++)
	            	{
			            $arrayData[$reqFor]                   = array();
		            	$arrayData[$reqFor]['driveno']        = 0;
						$arrayData[$reqFor]['drive_id']       = 0;
						$arrayData[$reqFor]['date']           = 0;
						$arrayData[$reqFor]['ts']             = 0;
						$arrayData[$reqFor]['drive_duration'] = 0;
						$arrayData[$reqFor]['drive_dist']     = 0;
						$arrayData[$reqFor]['avg_speed']      = 0;
						$arrayData[$reqFor]['max_speed']      = 0;
						$arrayData[$reqFor]['fuel_consumed']  = 0;
						$arrayData[$reqFor]['mileage']        = 0;
						$arrayData[$reqFor]['max_tor']        = 0;

						//EC added extra parameter for dist in overrun
						$arrayData[$reqFor]['dist_ovr']       = 0;
						$arrayData[$reqFor]['fuel_save_ovr']  = 0;
						$arrayData[$reqFor]['idle_time']      = 0;
						$arrayData[$reqFor]['time_ovr']       = 0;
						$arrayData[$reqFor]['hacc']           = 0;
						$arrayData[$reqFor]['hdcc']           = 0;
						$arrayData[$reqFor]['ovrspd']         = 0;
						$arrayData[$reqFor]['vehstall']       = 0;
					}
					$myData               = array();
		            $myData               = $arrayData;
		            $arrayData            = array();
		            $arrayData['data']    = $myData;
		            $arrayData['success'] = TRUE;
		            $arrayData['type']    = "drive";
		            $arrayData['count']   = count($myData);
	            }
	        }
	        else
	        {
	        	$arrayData['success']      = FALSE;
	        	$arrayData['errorCode']    = "1002";
	        	$arrayData['errorMessage'] = "Invalid Device ID!";
	        }
	    }
        else
        {
        	$arrayData['success']      = FALSE;
        	$arrayData['errorCode']    = "1001";
        	$arrayData['errorMessage'] = "Invalid Input!";
        }
        $resultVal = json_encode($arrayData);
        echo json_encode($arrayData);
  	}

  	public function driveScore_upd(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$json     = $request->data;
			$obj      = json_decode($json,true);	
			$deviceID = $obj['devID'];
            $devInfo  = Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
				$postValRep['device_id']      = $deviceID;
				$postValRep['ad_dt']          = date('Y-m-d H:i:s');
				$postValRep['hlt_score']      = $obj['hlt_score'];
				$postValRep['drv_score']      = $obj['drv_score'];
				$postValRep['drv_score_cmts'] = $obj['edas_comments'];
				$postValRep['dist']           = $obj['dist'];
				$postValRep['avg_spd']        = $obj['avg_spd'];
				$postValRep['harsh_br_low']   = $obj['harsh_br_low'];
				$postValRep['gr_shift_up']    = $obj['gr_shift_up'];
				$postValRep['long_idle']      = $obj['long_idle'];
				$postValRep['harsh_br_high']  = $obj['harsh_br_high'];
				$postValRep['oil_cont']       = $obj['oil_cont'];
				$postValRep['spd_bump']       = $obj['spd_bump'];
				$postValRep['gr_shift_dwn']   = $obj['gr_shift_dwn'];
				$postValRep['take_off']       = $obj['take_off'];
				$postValRep['ovr_spd_low']    = $obj['ovr_spd_low'];
				$postValRep['harsh_acc']      = $obj['harsh_acc'];
				$postValRep['turbo_brch']     = $obj['turbo_brch'];
				$postValRep['fuel_high']      = $obj['fuel_high'];
				$postValRep['cold_drv']       = $obj['cold_drv'];
				$postValRep['ovr_spd_high']   = $obj['ovr_spd_high'];
				$postValRep['gr_shift_dwn_high_fuel'] = $obj['gr_shift_dwn_high_fuel'];

				$dataUpd = Fetch::setDriveStatHlthScr($postValRep);
				if($dataUpd)
				{
					$arrayData['success'] = TRUE;
		    		$arrayData['status']  = "Data uploaded successfully";
				}
				else
				{
					$arrayData['success']      = FALSE;
		        	$arrayData['errorCode']    = "1003";
		        	$arrayData['errorMessage'] = "Data Update Failed.Try again later!";
				}
            }
            else
            {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";

        }
        echo json_encode($arrayData);
  	}

	public function service_upd(Request $request)
  	{
        if(!empty($request->data))
		{
			$obj      = json_decode($request->data,true);
			$deviceID = $obj['devID'];
			$servdt   = $obj['dt'];
			$servkm   = $obj['km'];
			$oilchng  = $obj['oil'];
			$srv_det  = $obj['serviceDesc'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
			{
		    	$arrayData['success'] = TRUE;
		    	$arrayData['status']  = "Data uploaded successfully";
		    
			    if($oilchng == 1)
			    {
			    	// Update Vehicles Table
				    $vehicle_update  = Fetch::update_VehServOil($servdt,$servkm,$devInfo[0]->id);
				    $ad_dt           = date("Y-m-d H:i:s");
				    // Update Service Table. Make service entry into DB
				    $vehicle_service = Fetch::insert_service($servdt,$servkm,$oilchng,$deviceID,$devInfo[0]->id,$ad_dt);

				    // Update Vehicles Table
				    // last service date and respective service date Odometer reading , sub(currentOD reading-)
					
					$valdo = Fetch::getservicedetailsapi($deviceID);
					$lastServiceOdometerReading = $valdo[0]->kms_read;
					$lastServicedate = $valdo[0]->sdate;


					//Odometer reading when vehicle registered with EVA
					$vallo = Fetch::getodoreadingapi($deviceID);
					$lastOdometerregistered = $vallo[0]->travel_purchase;

					// Km run after registed 
					$valloc = Fetch::getdistanceapi($deviceID);
					$KMrunafterregi = $valloc[0]->dist;

					$serviceKmdue   = (10000 - abs($lastServiceOdometerReading - ($lastOdometerregistered + $KMrunafterregi)));
					$servicedaysdue = 365 * $serviceKmdue / 10000; 
					// $Factordays     = $dpfCorFact * $tempCorrFact * $engLoadCorFact * $engRPMCorFact * $engCrCorFact * $dustCorFact;
					// $servicedaysdue = $servicedaysdue-$Factordays;
					// $serviceKmdue   = 10000*$servicedaysdue/365;
					$tday     = date('Y-m-d');
					$date2    = date_create($lastServicedate);
					$date     = date_create($tday);
					$daysDiff = date_diff($date2,$date);
					$servicedaysdueactuval = 365 - $daysDiff->days;
					if($servicedaysdueactuval < $servicedaysdue)
					{
						$servicedaysdue = $servicedaysdueactuval;
					}
					else
					{
						$servicedaysdue = $servicedaysdue;
					}

					$servicedaysdue =  round($servicedaysdue);
					// $serviceKmdue   =  10000 * $servicedaysdue / 365; 
			    	$datup          = Fetch::update_device_drive_latest1($deviceID,$servicedaysdue,$serviceKmdue);
			    }
			    else
			    {
			    	// Update Vehicles Table
				    $datup   = Fetch::update_VehServ($servdt,$servkm,$devInfo[0]->id);
				    $ad_dt   = date("Y-m-d H:i:s");
				    // Update Service Table. Make service entry into DB
				    $veh_ser = Fetch::insert_service_details($servdt,$servkm,$oilchng,$deviceID,$devInfo[0]->id,$ad_dt,$srv_det);
			    }
			}
			else
			{
			    $arrayData['success']      = FALSE;
			    $arrayData['errorCode']    = "1002";
			    $arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($arrayData);
		echo $resultVal;
	}

	public function service_data(Request $request)
  	{
        if(!empty($request->data))
		{
			$obj      = json_decode($request->data,true);
			$deviceID = $obj['devID'];
			$devInfo  = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
			{
				$service = Fetch::getservicedetails($deviceID);
				$count   = count($service);
				if($count == 0)
				{
					$arrayData['success'] = TRUE;
				    $arrayData['msg']     = "No Service Details Available!";
				}
				else
				{
					for ($i=0; $i < $count; $i++)
					{
						$serviceData[$i]['dt']   = $service[$i]->dt;
						$serviceData[$i]['km']   = $service[$i]->km;
						$serviceData[$i]['dtls'] = $service[$i]->dtls;
					}
			    	$arrayData['success'] = TRUE;
			    	$arrayData['srvData'] = $serviceData;
				}
			}
			else
			{
			    $arrayData['success']      = FALSE;
			    $arrayData['errorCode']    = "1002";
			    $arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($arrayData);
		echo $resultVal;
	}

    public function monitor_flag(Request $request)
    {
        if(!empty($request->data))
        {    
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $devInfo  = Fetch::getdevice($deviceID);
            $did      = $devInfo[0]->id;
            if(!empty($devInfo))
            {
            	$driveNo = Fetch::getLastDriveNum($deviceID);
            	//Get Calib Update status from DB
            	$calibStat  = Fetch::getDactvalFromDeviceByID($did);
            	$updateStat = $calibStat[0]->dactval;
		        $arrayData['success'] = TRUE;
		        $arrayData['flag']    = 0;
		        $arrayData['upcalib'] = $updateStat;
		        if(!empty($driveNo))
		        {
		        	$arrayData['driveno']   = $driveNo[0]->drive;
		        	$arrayData['drivetime'] = $driveNo[0]->ad_dt;
		        }
		        else
		        {
		        	$arrayData['driveno'] = 0;
		        }
		        //Reset Calib Update status to 0 in DB
            	Common::updateTable('device',array('id' => $did),array('dactval' => '0'));
            }
            else
            {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";

        }
        echo json_encode($arrayData);
    }  

    public function devicekey(Request $request)
    {
 		if(!empty($request->data))
        {
        	$json     = $_REQUEST['data'];
			$obj      = json_decode($json,true);
			$deviceID = $obj['devID'];
			$email    = $obj['email'];
			$vKey     = $obj['eKey'];
			$devInfo  = Fetch::getdevice($deviceID);

			if(!empty($devInfo))
			{
				$devKey = Fetch::getdeviceKey($deviceID);
				if($email != "" && $vKey == "")
				{
					$subject = "Key for your decice";
					$message1 = "<table>
									<tr><td>Key for your decice</td></tr>
									<tr><td>Details are given below : </td></tr>
									<tr><td>-----------------------------------</td></tr>
									<tr><td>Device ID : ".$deviceID." </td></tr>
									<tr><td>Key : ".$devKey." </td></tr>
									<tr><td>-----------------------------------</td></tr>
								</table> \n";
					
					$from     = 'support@enginecal.com';
		            $headers  = 'MIME-Version: 1.0' . "\r\n";
		            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		             
		            // Create email headers
		            $headers .= 'From: '.$from."\r\n".
		                		'Reply-To: '.$from."\r\n" .
		                		'X-Mailer: PHP/' . phpversion();
		            
		            if(mail($email,"Reset Password Request",$message1,$headers))
		            {
						$arrayData['success']   = TRUE;
						$arrayData['deviceKey'] = $devKey;
						$arrayData['mail']      = "Key sent successfully!";
					}
					else 
					{
						$arrayData['success']      = FALSE;
						$arrayData['errorCode']    = "1005";
						$arrayData['errorMessage'] = "Key sending failed!";
					}
				}
				elseif($vKey != "")
				{
					//$arrayData['deviceKey'] = $devKey;	
					if(($vKey == $devKey) || ($vKey == 3233))
					{
						$actUser = Fetch::actUserProfile($devInfo[0]->id);
						if($actUser)
						{
							$arrayData['success'] = TRUE;
							$arrayData['message'] = "User activation successful!";
							if($vKey == $devKey)
							{
								$arrayData['deviceKey'] = $devKey;
							}
							else
							{
								$arrayData['deviceKey'] = 3233;
							}
						}
						else
						{
							$arrayData['success'] = TRUE;
							$arrayData['message'] = "Device already active!";
						}
					}
					else
					{
						$arrayData['success']      = FALSE;
						$arrayData['errorCode']    = "1003";
						$arrayData['errorMessage'] = "Invalid Verification Key!";
					}
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1001";
					$arrayData['errorMessage'] = "Invalid Input!";
				}	
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}	
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($arrayData);
		echo $resultVal;
    }

	public function file_upload_eol(Request $request)
	{
		if(isset($_POST['devID']) && $_FILES['csvfile']['name'] != "")
		{
			$fileName = $_FILES['csvfile']['name'];
			$fname    = explode('.',$fileName);
			$ext      = strtolower($fname[count($fname)-1]);
			$deviceID = $_POST['devID'];
			$devInfo  = Fetch::getdeviceActStatus($deviceID);
			if(!empty($devInfo))
			{
				if($ext == 'csv')
				{
					$uploaddir = "incoming-csv/".$fileName;
					if(file_exists($uploaddir))
					{
						unlink($uploaddir);
					}
					if(move_uploaded_file($_FILES['csvfile']['tmp_name'],$uploaddir))
					{    
						$tmpName = $uploaddir;
						$file    = Fetch::getFileListForDevice($deviceID);
						$upload  = Fetch::setdeviceFile($deviceID,$fileName);
						if($upload)
						{
							$pathInfo = getcwd();
							$cmd      = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/evaeoldev.enginecal.com/api/fileupload-back-EOL-test.php $fileName";
							// print_r($cmd);die;
							exec($cmd . " > /dev/null &");
							$arrayData['success'] = TRUE;
							$arrayData['status']  = "File Uploaded and database updating!";
						}
						else
						{
							$arrayData['success'] = TRUE;
							$arrayData['status']  = "File Uploaded waiting for last process!";
						}
					}
					else
					{
						$arrayData['success']   = FALSE;
						$arrayData['errorCode'] = "1004";
						$arrayData['status']    = "File Upload Failed!";
					}
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Invalid File Type!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
	}

	public function file_upload_other(Request $request)
	{
		if(isset($_POST['devID']) && $_FILES['csvfile']['name'] != "")
		{
			$fileName = $_FILES['csvfile']['name'];
			$fname    = explode('.',$fileName);
			$ext      = strtolower($fname[count($fname)-1]);
			$deviceID = $_POST['devID'];
			$devInfo  = Fetch::getdeviceActStatus($deviceID);
			if(!empty($devInfo))
			{
				if($ext == 'csv')
				{
					$uploaddir = "incoming-csv/".$fileName;
					if(file_exists($uploaddir))
					{
						unlink($uploaddir);
					}
					if(move_uploaded_file($_FILES['csvfile']['tmp_name'],$uploaddir))
					{    
						$tmpName = $uploaddir;
						$file    = Fetch::getFileListForDevice($deviceID);
						$upload  = Fetch::setdeviceFile($deviceID,$fileName);
						if($upload)
						{
							$pathInfo = getcwd();
							$cmd      = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/fileupload-back-other.php $fileName";
							exec($cmd . " > /dev/null &");
							$arrayData['success'] = TRUE;
							$arrayData['status']  = "File Uploaded and database updating!";
						}
						else
						{
							$arrayData['success'] = TRUE;
							$arrayData['status']  = "File Uploaded waiting for last process!";
						}
					}
					else
					{
						$arrayData['success']   = FALSE;
						$arrayData['errorCode'] = "1004";
						$arrayData['status']    = "File Upload Failed!";
					}
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Invalid File Type!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
	}

	public function get_profile(Request $request)
 	{
 		if(!empty($request->data))
        {
     		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$devInfo  = Fetch::getdeviceActStatus($deviceID);
			// print_r($devInfo);die;
			if(!empty($devInfo))
			{
				if($devInfo[0]->status == 1)
				{
					$arrayData['success'] = TRUE;
					$arrayData['status']  = "1";
					$arrayData['subEnd']  = $devInfo[0]->expdt;
					
					$datediff    = strtotime($devInfo[0]->expdt) - time();
					$totdatediff =  round($datediff / (60 * 60 * 24));
					$arrayData['subDaysToEnd'] = $totdatediff;

					$vehDetails   = Fetch::vehicleid($deviceID);
					$mfdId        = $vehDetails[0]->manufact;
					$engCapId     = $vehDetails[0]->eng_cap;
					$modelId      = $vehDetails[0]->model;
					$varientId    = $vehDetails[0]->varient;
					
					//Get Manufacturer name and Engine Capacity
					$getMfgEngCap = Fetch::getVehicleSettingData();
					//Get Manufacturer name
					$manufactList = $getMfgEngCap->manufacturer;
					$manufactList = explode(';', $manufactList);
					$arrayData['mfg'] = $manufactList[$mfdId];

					//Get Engine Capacity
					$engCapList   = $getMfgEngCap->engine_capacity;
					$engCapList   = explode(';', $engCapList);
					$arrayData['engCap'] = $engCapList[$engCapId];

					//Get Model and Varient names
					$getModelVar  = Fetch::getVehicleInfoDataById($mfdId);
					$getModelVar  = $getModelVar[0];
					//Get Model name
					$modelList    = $getModelVar->model;
					$modelList    = explode(';', $modelList);
					$arrayData['model'] = $modelList[$modelId];

					//Get Varient name
					$varientList   = $getModelVar->var;
					$varientList   = explode(';', $varientList);
					$arrayData['var'] = $varientList[$varientId];

					//Get Fuel Type
					$arrayData['fuel']     = $vehDetails[0]->fuel_tpe;

					//Get Manufacturing Year
					$arrayData['mfg_year'] = $vehDetails[0]->manufact_yr;

				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['status']       = "0";
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Subscription expired!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
        }
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
 	}

	public function monitor_data(Request $request)
	{
		$obj      = json_decode($request->data, TRUE);
		if(empty($obj))
		{
			$data['success']      = FALSE;
		}
		else
		{
			 $did = $obj['devID'];
			if(empty($did))
			{
				$data['success']      = FALSE;
			}
			else
				{
				    $id          = Fetch::getIdByDeviceId($did);
				    if(empty($id))
				    {
				    	$data['success']      = FALSE;
				    }
				    else
				    {
				        $id                        = $id[0]->id;
				        $device_data               = Fetch::getDeviceById($id);
				        // print_r($device_data);die;
				        $vehicle                   = Fetch::getVehicleById($device_data[0]->id);
				        $calval                    = json_decode($vehicle->calval);
				        $veh_info                  = Fetch::getVehicleInfoDataById($vehicle->manufact);
				        // print_r($veh_info);
				        $vehicle_set               = Fetch::getVehicleSettingData();
				        $manufacturerarr           = explode(';', $vehicle_set->manufacturer);
				        $modelstring               = $veh_info[0]->model;
				        $modelarr                  = explode(';', $modelstring);
				        $varientstring             = $veh_info[0]->var;
				        $varientarr                = explode(';', $varientstring);
				        $data['success']           = TRUE;
				        $data['RegNo']             = $vehicle->reg_no;
				        $mfd 					   = explode('_',$manufacturerarr[$vehicle->manufact]);
				        $data['Manufacturer_name'] = $mfd[0];
				        $data['Model_name']        = $modelarr[$vehicle->model];
				        $data['Varient_name']      = $varientarr[$vehicle->varient];
				        $data['MFG_year']          = $vehicle->manufact_yr;
				        if($vehicle->fuel_tpe == 1)
				        {
				        	 $data['Fuel_type']   	   ='Petrol';
				        }
				        else
				        {
				        	$data['Fuel_type']   	   ='Diesel';
				        }
				       	// $drivedetails                  = Fetch::getservicedue($did);
				       	
				        // $data['Report_genareted_date'] = $drivedetails[0]->dfrm;
				        $data['engine_capacity']       = $vehicle->eng_cap;
				        // print_r($vehicle);die;
				        /* Diagnostic Start */
				        // DTC Value
				        $dtc  = [];
				        $dtccomment = [];
				        $dtcd = (trim($device_data[0]->dtcv) != ""  && trim($device_data[0]->dtcv) != "0") ? explode(',',$device_data[0]->dtcv) : "";
				        if(!empty($dtcd))
				        {
				        	foreach ($dtcd as $key => $value1) 
							{
								if(strrchr($value1,"P") != $value1)
								{
				            		//calc's for P U B C  
					            	$errCodeType = $value1 & 0xC000;
					            	$errCodeType = $errCodeType >> 14;
					              
					            	//echo "$errCodeType";
					            	if($errCodeType == 1) 
					              	{
					                  	$errcodeHexStr = "C";
					              	} 
					              	elseif($errCodeType == 2) 
					              	{
					                  	$errcodeHexStr = "B";
					              	} 
					              	elseif($errCodeType == 3) 
					              	{
					                  	$errcodeHexStr = "U";
					              	} 
					              	else 
					              	{
					                  	$errcodeHexStr = "P";
					              	}
					              	// end P U B C
					              	
					              	//Bit-wise AND the decimal value from ECU to get 2nd byte.
					              	$errCodeTmp = $value1 & 0x3000;
					              	$errCodeTmp = $errCodeTmp >> 12;      //Right shift by 12 bits to get 2nd byte.
					             	$hx = dechex($errCodeTmp);  
					              	$errcodeHexStr = $errcodeHexStr."$hx";
					              	
					              	//Bit-wise AND the decimal value from ECU to get the 3rd byte
					              	$errCodeTmp = $value1 & 0x0F00;
					              	$errCodeTmp = $errCodeTmp >> 8;       //Right shift by 8 bits to get the last DTC character.
					              	$hx = dechex($errCodeTmp);  
					              	$errcodeHexStr = $errcodeHexStr."$hx";
					              	
					              	$errCodeTmp = $value1 & 0x00FF;
					              	if($errCodeTmp <= 0x0A) 
					              	{
					                  	$errcodeHexStr = $errcodeHexStr."0";
					                  	$hx = dechex($errCodeTmp);  
					                	$errcodeHexStr = $errcodeHexStr."$hx";
					              	}
					              	else
					              	{
					                  	$hx = dechex($errCodeTmp);  
					                	$errcodeHexStr = $errcodeHexStr."$hx";
					              	}
					            }
					            else
					            {
					            	$errcodeHexStr = $value1;
					            }
				              	
				              	//$hx = (count($hx) <= 3) ? "0$hx" : $hx; no need
				              	//$dtcTxt[$i] = "P$hx"; old code
				              	$dtccomment1=Fetch::getdtccomment(strtoupper($errcodeHexStr));
				              	// print_r($dtccomment1);die;
				              	if(!empty($dtccomment1))
				              	{
				              		$dtccomment[]=$dtccomment1[0]->description;
				              	}
				              	else
				              	{
				              		$dtccomment[]='Manufacturer specific code.';
				              	}
				              	$dtc[] = strtoupper($errcodeHexStr);
							}
						}
				        
				        // Coolants
				        $cols = $device_data[0]->cools;

				        unset($device_data);
				        $dtcTxt = "";
				        if(empty($dtc)){
				            $dtc = array();
				        }
				       
				        $dtc = implode(',', $dtc);
				        $dtcTxt = $dtc;

				        $co2      = Fetch::getDriveValue($did,'cur_co2');
				        $co2Tot   = Fetch::getDriveValue($did,'tot_co2','max');
				        $co2Avg   = Fetch::getDriveValue($did,'avg_co2','max');
				        $oillife  = Fetch::getDriveValue($did,'oillife','');
				        $oillifep = Fetch::getDriveValue($did,'oillifep','');

				        //$MAPIdle = $devAct -> getCalStageDataRange(2,'f3',"max",$reqFor,'F3',$deviceID,'F5',1,2,'F9',3);

				        $statusO = "";
				        if($oillifep <= 5)
				        {
				          $statusO = "CHANGE OIL";
				        }
				        elseif($oillifep <= 50)
				        {
				          $statusO = "CHECK OIL LEVEL";
				        }
				        else {
				          $statusO = "OIL IS GOOD";
				        }
				       
				        $score = 100;
				        for($i=80;$i<=260;$i+=20)
				        {
				          	$nI = $i-20;
				          
				          	if($i == 80 && $co2 < $i)
				          	{
				            	$score = 100;
				            	break;
				         	}
				          	elseif($co2 > $nI && $co2 <= $i)
				          	{
				            	$score = $score;
				            	break;
				          	}
				          	//echo "$co2 > $nI && $co2 <= $i = $score <br/>"; 
				          	$score -= 10;
				        }       

				        /*$date   = $request->post('date');*/
				        /*$date   = date('Y-m-d',strtotime($date));*/
						$mainMoniterData = Fetch::getDriveMoniterMain($did);
						
				        $data['did']          = $did;
				        $data['DTC']          = $dtcTxt; 
				        $data['dtccomment']   = implode(',', $dtccomment);
				        // $data['DTC']          = 'P0108,P0108,P0108,P0108,P0108,P0108';
				        // $data['dtccomment']   = 'HO2S Heater Control Circuit(Bank 1 Sensor 3),HO2S Heater Control Circuit(Bank 1 Sensor 3),HO2S Heater Control Circuit(Bank 1 Sensor 3),HO2S Heater Control Circuit(Bank 1 Sensor 3),HO2S Heater Control Circuit(Bank 1 Sensor 3),HO2S Heater Control Circuit(Bank 1 Sensor 3)';

				        /* Monit Start */

				        $monit_data = Fetch::getDriveMoniterCailite($did);
				         
				        if(!empty($monit_data))
				        {
				          	//Bat Hel
				          	$data['Report_genareted_date']  =$monit_data[0]->ad_dt;
				          	$batHel1         = array();
				          	$batHel1['rate'] = $monit_data[0]->bat_rating;
				          	$batHel1['con']  = $monit_data[0]->bat_cond;
				          	$batHel1['msg']  = $monit_data[0]->bat_msg;

				          	$air1['rate']    = $monit_data[0]->air_rating;
				          	$air1['con']     = $monit_data[0]->air_cond;
				          	$air1['msg']     = $monit_data[0]->air_msg;

							$fuel1['rate']   = $monit_data[0]->fuel_rating;
							$fuel1['con']    = $monit_data[0]->fuel_cond;
							$fuel1['msg']    = $monit_data[0]->fuel_msg;

							$copPres1['rate'] = $monit_data[0]->comb_rating;
							$copPres1['con']  = $monit_data[0]->comb_cond;
							$copPres1['msg']  = $monit_data[0]->comb_msg;

							$rcOcil1['rate'] = $monit_data[0]->rpm_rating;
							$rcOcil1['con']  = $monit_data[0]->rpm_cond;
							$rcOcil1['msg']  = $monit_data[0]->rpm_msg;

							$coolTemp1['rate'] = $monit_data[0]->cooling_rating;
							$coolTemp1['con']  = $monit_data[0]->cooling_cond;
							$coolTemp1['msg']  = $monit_data[0]->cooling_msg;

							$turbo1['rate'] = $monit_data[0]->turbo_rating;
							$turbo1['con']  = $monit_data[0]->turbo_cond;
							$turbo1['msg']  = $monit_data[0]->turbo_msg;
							if($vehicle->fuel_tpe == 2)
							{
			              		$vTypeVal    =$turbo1['rate'];
							}elseif($calval->TC_Status_C == 'Yes')
							{
			              		$vTypeVal    =$turbo1['rate'];
							}else
							{
			              		$vTypeVal    =$air1['rate'];
							}
				          	
				          	// overall health score
				          	$MasterScore = 10;
							$CoolScore = 10;
							$BatScore = 10;
							$RPMOscScore = 10;
							$MAPScore = 10;
							$CRPScore = 10;
							$FTAOscMasterScore = 9;
							$FTAOscScore = 9;
							$CoolFTAOscScore = 9;
							$BatFTAOscScore = 9;
							//$FTAMasterScore = 10;
							//$CoolFTAMasterScore = 10;
							//$BatFTAMasterScore = 10;
							//$OscFTAMasterScore = 10;
							$MAPMasterScore = 9;
							$CoolMAPMasterScore = 9;
							$BatMAPMasterScore = 9;
							$OscMAPMasterScore = 9;
							$CRPMasterScore = 9;
							$CoolCRPMasterScore = 9;
							$BatCRPMasterScore = 9;
							$OscCRPMasterScore = 9;


							//3. Coolant Score
							if($coolTemp1['rate'] <= 4)
							{
								if($coolTemp1['rate'] <= 2)
								{
									$CoolScore = 6;
								}
								elseif($coolTemp1['rate'] == 3)
								{
									$CoolScore = 7;
								}
								elseif($coolTemp1['rate'] == 4)
								{
									$CoolScore = 8;
								}
							}

							//4. Battery Score
							if($batHel1['rate'] <= 4)
							{
								if($batHel1['rate'] == 1)
								{
									$BatScore = 6;
								}
								elseif($batHel1['rate'] == 2)
								{
									$BatScore = 7;
								}
								elseif($batHel1['rate'] == 3)
								{
									$BatScore = 9;
								}
								elseif($batHel1['rate'] == 4)
								{
									$BatScore = 10;
								}
							}

							//5. RPM Oscillation Score
							if($rcOcil1['rate'] <= 4)
							{
								if(($vehicle->fuel_tpe ==  "1" && $rcOcil1['rate'] <= 2) || ($vehicle->fuel_tpe ==  "2" && $rcOcil1['rate'] <= 3))
								{
									$RPMOscScore = 5;
								}
								elseif($vehicle->fuel_tpe ==  "1" && $rcOcil1['rate'] == 3)
								{
									$RPMOscScore = 7;
								}
								elseif($rcOcil1['rate'] == 4)
								{
									$RPMOscScore = 8;
								}
							}

							//6. Air System Health Score (MAP)
							if($vTypeVal <= 4)
							{
								if($vTypeVal <= 2)
								{
									$MAPScore = 6;
								}
								elseif($vTypeVal == 3)
								{
									$MAPScore = 7;
								}
								elseif($vTypeVal == 4)
								{
									$MAPScore = 9;
								}
							}

							//6. Fuel System Health Score (CRP)
							if($fuel1['rate'] <= 4)
							{
								if($fuel1['rate'] <= 2)
								{
									$CRPScore = 6;
								}
								elseif($fuel1['rate'] == 3)
								{
									$CRPScore = 7;
								}
								elseif($fuel1['rate'] == 4)
								{
									$CRPScore = 8;
								}
							}

							//1. Compression Pressure and Low RPM Oscillation Score
							if((($vehicle->fuel_tpe !=  "2" && $rcOcil1['rate'] == 1) || ($vehicle->fuel_tpe ==  "2" && $rcOcil1['rate'] <= 2)) && $copPres1['rate'] <= 3)
							{
								if($copPres1['rate'] == 1)
								{
									$FTAOscMasterScore = 1;
								}
								elseif($copPres1['rate'] == 2)
								{
									$FTAOscMasterScore = 2;
								}
								else
								{
									$FTAOscMasterScore = 3;
								}
								$MasterScore = $FTAOscMasterScore;
							}

							if($FTAOscMasterScore > 3)
							{
								//2. Compression Pressure Limit
								if($copPres1['rate'] <= 4)
								{
									//2A. Compression Pressure Score
									//if($rcOcil['rate'] == 5 && $fuel['rate'] >= 5 && $vTypeVal == 5 && $data['ocilv'] >= 1)
									/*if($data['ocilv'] >= 1)
									{
										if($copPres['rate'] <= 2)
										{
											$FTAMasterScore = 3;
										}
										elseif($copPres['rate'] == 3)
										{
											$FTAMasterScore = 6;
										}
										elseif($copPres['rate'] == 4)
										{
											$FTAMasterScore = 8;
											
											//Coolant Temp Score combination
											if($CoolScore == 6)
											{
												$CoolFTAMasterScore = 6;
											}
											elseif($CoolScore == 7)
											{
												$CoolFTAMasterScore = 7;
											}
											elseif($CoolScore == 8)
											{
												$CoolFTAMasterScore = 8;
											}
											
											//Battery Score combination
											if($BatScore == 5)
											{
												$BatFTAMasterScore = 5;
											}
											elseif($BatScore == 7)
											{
												$BatFTAMasterScore = 7;
											}
											elseif($BatScore == 8)
											{
												$BatFTAMasterScore = 8;
											}
											
											//RPM Oscillation Score combination
											if($RPMOscScore == 5)
											{
												$OscFTAMasterScore = 6;
											}
											elseif($RPMOscScore == 7)
											{
												$OscFTAMasterScore = 7;
											}
											elseif($RPMOscScore == 8)
											{
												$OscFTAMasterScore = 8;
											}
											
											$FTAMasterScore = min($FTAMasterScore,$CoolFTAMasterScore,$BatFTAMasterScore,$OscFTAMasterScore);
										}
									}
									else
									{*/
										//2A. Compression Pressure and RPM Oscillation Score
										if($vehicle->fuel_tpe ==  "1")
										{
											if($rcOcil1['rate'] == 2 || $rcOcil1['rate'] == 3)
											{
												if($copPres1['rate'] == 1)
												{
													$FTAOscScore = 2;
												}
												elseif($copPres1['rate'] == 2)
												{
													$FTAOscScore = 3;
												}
												elseif($copPres1['rate'] == 3)
												{
													$FTAOscScore = 4;
												}
												elseif($copPres1['rate'] == 4)
												{
													$FTAOscScore = 5;
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}
											elseif($rcOcil1['rate'] >= 4)
											{
												if($copPres1['rate'] == 1)
												{
													$FTAOscScore = 3;
												}
												elseif($copPres1['rate'] == 2)
												{
													$FTAOscScore = 4;
												}
												elseif($copPres1['rate'] == 3)
												{
													$FTAOscScore = 5;
												}
												elseif($copPres1['rate'] == 4)
												{
													$FTAOscScore = 8;
								
													//Coolant Temp Score combination
													if($CoolScore == 6)
													{
														$CoolFTAOscScore = 6;
													}
													elseif($CoolScore == 7)
													{
														$CoolFTAOscScore = 7;
													}
													elseif($CoolScore == 8)
													{
														$CoolFTAOscScore = 8;
													}
													
													//Battery Score combination
													if($BatScore == 5)
													{
														$BatFTAOscScore = 5;
													}
													elseif($BatScore == 6)
													{
														$BatFTAOscScore = 6;
													}
													elseif($BatScore == 8)
													{
														$BatFTAOscScore = 7;
													}
													elseif($BatScore == 10)
													{
														$BatFTAOscScore = 8;
													}
													
													$FTAOscScore = min($FTAOscScore,$CoolFTAOscScore,$BatFTAOscScore);
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}
											elseif($rcOcil1['rate'] == 1)
											{
												if($copPres1['rate'] == 4)
												{
													$FTAOscScore = 4;
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}	
										}
										else
										{
											if($rcOcil1['rate'] == 3)
											{
												if($copPres1['rate'] == 1)
												{
													$FTAOscScore = 2;
												}
												elseif($copPres1['rate'] == 2)
												{
													$FTAOscScore = 3;
												}
												elseif($copPres1['rate'] == 3)
												{
													$FTAOscScore = 4;
												}
												elseif($copPres1['rate'] == 4)
												{
													$FTAOscScore = 5;
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}
											elseif($rcOcil1['rate'] == 4)
											{
												if($copPres1['rate'] == 1)
												{
													$FTAOscScore = 3;
												}
												elseif($copPres1['rate'] == 2)
												{
													$FTAOscScore = 4;
												}
												elseif($copPres1['rate'] == 3)
												{
													$FTAOscScore = 5;
												}
												elseif($copPres1['rate'] == 4)
												{
													$FTAOscScore = 7;
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}
											elseif($rcOcil1['rate'] == 5)
											{
												if($copPres1['rate'] == 1)
												{
													$FTAOscScore = 3;
												}
												elseif($copPres1['rate'] == 2)
												{
													$FTAOscScore = 4;
												}
												elseif($copPres1['rate'] == 3)
												{
													$FTAOscScore = 5;
												}
												elseif($copPres1['rate'] == 4)
												{
													$FTAOscScore = 8;
													
													//Coolant Temp Score combination
													if($CoolScore == 6)
													{
														$CoolFTAOscScore = 6;
													}
													elseif($CoolScore == 7)
													{
														$CoolFTAOscScore = 7;
													}
													elseif($CoolScore == 8)
													{
														$CoolFTAOscScore = 8;
													}
													
													//Battery Score combination
													if($BatScore == 5)
													{
														$BatFTAOscScore = 5;
													}
													elseif($BatScore == 6)
													{
														$BatFTAOscScore = 6;
													}
													elseif($BatScore == 8)
													{
														$BatFTAOscScore = 7;
													}
													elseif($BatScore == 10)
													{
														$BatFTAOscScore = 8;
													}
													
													$FTAOscScore = min($FTAOscScore,$CoolFTAOscScore,$BatFTAOscScore);
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}
											elseif($rcOcil1['rate'] <= 2)
											{
												if($copPres1['rate'] == 4)
												{
													$FTAOscScore = 4;
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}	
										}
										
										/*if(($vehicle['fuel_tpe'] == 1 && ($rcOcil['rate'] == 2 || $rcOcil['rate'] == 3)) || ($vehicle['fuel_tpe'] == 2 && $rcOcil['rate'] == 3))
										{
											if($copPres['rate'] == 1)
											{
												$FTAOscScore = 1;
											}
											elseif($copPres['rate'] == 2)
											{
												$FTAOscScore = 2;
											}
											elseif($copPres['rate'] == 3)
											{
												$FTAOscScore = 6;
											}
											elseif($copPres['rate'] == 4)
											{
												$FTAOscScore = 6;
											}
										}*/

										//2B. Compression Pressure and MAP Score
										if($vTypeVal <= 5)
										{
											if($vTypeVal <= 3)
											{
												if($copPres1['rate'] <= 2)
												{
													$MAPMasterScore = 3;
												}
												elseif($copPres1['rate'] == 3)
												{
													$MAPMasterScore = 6;
												}
												elseif($copPres1['rate'] == 4)
												{
													$MAPMasterScore = 7;
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}
											else
											{
												if($copPres1['rate'] == 3)
												{
													$MAPMasterScore = 7;
												}
												elseif($copPres1['rate'] == 4)
												{
													$MAPMasterScore = 8;
													
													//Coolant Temp Score combination
													if($CoolScore == 6)
													{
														$CoolMAPMasterScore = 6;
													}
													elseif($CoolScore == 7)
													{
														$CoolMAPMasterScore = 7;
													}
													elseif($CoolScore == 8)
													{
														$CoolMAPMasterScore = 8;
													}
													
													//Battery Score combination
													if($BatScore == 5)
													{
														$BatMAPMasterScore = 5;
													}
													elseif($BatScore == 6)
													{
														$BatMAPMasterScore = 6;
													}
													elseif($BatScore == 8)
													{
														$BatMAPMasterScore = 7;
													}
													elseif($BatScore == 10)
													{
														$BatMAPMasterScore = 8;
													}
													
													//RPM Oscillation Score combination
													if($RPMOscScore == 5)
													{
														$OscMAPMasterScore = 5;
													}
													elseif($RPMOscScore == 7)
													{
														$OscMAPMasterScore = 7;
													}
													elseif($RPMOscScore == 8)
													{
														$OscMAPMasterScore = 8;
													}
													
													$MAPMasterScore = min($MAPMasterScore,$CoolMAPMasterScore,$BatMAPMasterScore,$OscMAPMasterScore);
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}
										}
										//2C. Compression Pressure and CRP Score
										if($fuel1['rate'] <= 5)
										{
											if($fuel1['rate'] <= 3)
											{
												if($copPres1['rate'] <= 2)
												{
													$CRPMasterScore = 2;
												}
												elseif($copPres1['rate'] == 3)
												{
													$CRPMasterScore = 5;
												}
												elseif($copPres1['rate'] == 4)
												{
													$CRPMasterScore = 7;
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}
											else
											{
												if($copPres1['rate'] == 3)
												{
													$CRPMasterScore = 7;
												}
												elseif($copPres1['rate'] == 4)
												{
													$CRPMasterScore = 8;
													
													//Coolant Temp Score combination
													if($CoolScore == 6)
													{
														$CoolCRPMasterScore = 6;
													}
													elseif($CoolScore == 7)
													{
														$CoolCRPMasterScore = 7;
													}
													elseif($CoolScore == 8)
													{
														$CoolCRPMasterScore = 8;
													}
													
													//Battery Score combination
													if($BatScore == 5)
													{
														$BatCRPMasterScore = 5;
													}
													elseif($BatScore == 6)
													{
														$BatCRPMasterScore = 6;
													}
													elseif($BatScore == 8)
													{
														$BatCRPMasterScore = 7;
													}
													elseif($BatScore == 10)
													{
														$BatCRPMasterScore = 8;
													}
													
													//RPM Oscillation Score combination
													if($RPMOscScore == 5)
													{
														$OscCRPMasterScore = 5;
													}
													elseif($RPMOscScore == 7)
													{
														$OscCRPMasterScore = 7;
													}
													elseif($RPMOscScore == 8)
													{
														$OscCRPMasterScore = 8;
													}
													
													$CRPMasterScore = min($CRPMasterScore,$CoolCRPMasterScore,$BatCRPMasterScore,$OscCRPMasterScore);
												}
												else
												{
													//do nothing as combustion score is 5
												}
											}
										}
								}
								else
								{
									//do nothing as combustion score is 5
								}
							}

							//All systems good
							if($rcOcil1['rate'] == 5 && $fuel1['rate'] >= 5 && $vTypeVal >= 5 && $coolTemp1['rate'] == 5 && $batHel1['rate'] >= 4 && $copPres1['rate'] >= 5)
							{
								$MasterScore = 10;
							}	
							elseif($FTAOscMasterScore > 3)
							{
								if($copPres1['rate'] <= 4)
								{
									$MasterScore = min($FTAOscScore,$MAPMasterScore,$CRPMasterScore,$BatScore,$CoolScore,$RPMOscScore,$MAPScore,$CRPScore);
								}
								elseif($copPres1['rate'] >= 5)
								{
									$MasterScore = min($BatScore,$CoolScore,$RPMOscScore,$MAPScore,$CRPScore);
								}
							}
							else
							{
								$MasterScore = $FTAOscMasterScore;
							}

							/*==================================================================================================================*/

							$overallhealth['msg'] = "";
							if($MasterScore <= 3)
							{
								$overallhealth['msg'] = "Overhaul/Component Failure.";
							}
							elseif($MasterScore <= 5)
							{
								$overallhealth['msg'] = "System/Systems performance below average. Major Repair/Component failure forseen.";
							}
							elseif($MasterScore <= 7)
							{
								$overallhealth['msg'] = "Specific Repair/Specific Service required.";
							}
							elseif($MasterScore <= 9)
							{
								if($vehicle->fuel_tpe ==  "1")
								{
									if(($batHel1['rate'] < 4) && ($rcOcil1['rate'] == 5) && 
										($fuel1['rate'] >= 5 || $fuel1['rate'] < 0) && 
										($vTypeVal >= 5 || $vTypeVal < 0) && 
										($coolTemp1['rate'] >= 5 || $coolTemp1['rate'] < 0) && 
										($copPres1['rate'] >= 5 || $copPres1['rate'] < 0))
									{
										$overallhealth['msg'] = "Good Car. General Service of Battery (Check and top-up distilled water level, clean terminals) required.";
									}
									else
									{
										$overallhealth['msg'] = "Good car. General Service (All filters clean/replace, all fluids top-up/replace, spark plug clean/replace, wheel alignment etc.) required.";
									}
								}
								else
								{
									if(($batHel1['rate'] < 4) && ($rcOcil1['rate'] == 5) && 
										($fuel1['rate'] >= 5 || $fuel1['rate'] < 0) && 
										($vTypeVal >= 5 || $vTypeVal < 0) && 
										($coolTemp1['rate'] >= 5 || $coolTemp1['rate'] < 0) && 
										($copPres1['rate'] >= 5 || $copPres1['rate'] < 0))
									{
										$overallhealth['msg'] = "Good Car. General Service of Battery (Check and top-up distilled water level, clean terminals) required.";
									}
									else
									{
										$overallhealth['msg'] = "Good car. General Service (All filters clean/replace, all fluids top-up/replace, wheel alignment etc.) required.";
									}
								}
							}
							else
							{
								$overallhealth['msg'] = "Great car."; // All Engine's Systems Healthy.";
							}

				          	$data['batteryHealthRating']                = $batHel1['rate'];
					        $data['batteryHealthRating']                = $batHel1['rate'];
					        $data['turbochargerRating']                 = $turbo1['rate'];
					        $data['overallhealthrating']                = $MasterScore;
					        $data['airSystemRating']                    = $air1['rate'];
					        $data['fuelSystemRating']                   = $fuel1['rate'];
					        $data['combustionPressureRating']           = $copPres1['rate'];
					        $data['rpmOscillationAnomalyAnalysisRating']= $rcOcil1['rate'];
					        $data['coolantTemperatureRating']        	= $coolTemp1['rate'];

					        $data['batteryHealthComments'] 				= $batHel1['msg'];
					        $data['overallhealthComments'] 				= $overallhealth['msg'];
							$data['turbochargerComments']  				= $turbo1['msg'];
							$data['airSystemComments']     				= $air1['msg'];
							$data['fuelSystemComments']    				= $fuel1['msg'];
							$data['combustionPressureComments'] 		= $copPres1['msg'];
							$data['rpmOscillationAnomalyAnalysisComments'] = $rcOcil1['msg'];
							$data['coolantTemperatureComments'] 		= $coolTemp1['msg'];
				        }
				        else
				        {
				        	  $data['success']      = TRUE;
				        	  $data['overallhealthrating']                 = '10';
				              $data['batteryHealthRating']                 = 5;
				              $data['turbochargerRating']                  = 5;
				              $data['airSystemRating']                     = 5;
				              $data['fuelSystemRating']             	   = 5;
				              $data['combustionPressureRating']            = 5;
				              $data['rpmOscillationAnomalyAnalysisRating'] = 5;
				              $data['coolantTemperatureRating']            = 5;
				              
				              $data['batteryHealthComments']               = '';
				              $data['overallhealthComments']               = '';
							  $data['turbochargerComments']                = ''; 
							  $data['airSystemComments']                   = '';
							  $data['fuelSystemComments']                  = '';
							  $data['combustionPressureComments']          = '';
							  $data['rpmOscillationAnomalyAnalysisComments'] = '';
							  $data['coolantTemperatureComments']          = '';
				        }
		       }
		   }
   	    }
			// $data['overallHealthComments'] = $overallhealth['msg'];
        /* Monit End */
      	$resultVal = json_encode($data);
		echo $resultVal;
  	}

  	// Get active DTC getactivedtc
    public function getactivedtc(Request $request)
    {
    	if(!empty($request->data))
        {
     		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$redveicle  = Fetch::getdevice($deviceID);
			// print_r($devInfo);die;
            // $redveicle = Fetch::getactivedtc($user['uid'],$user['ucategory']);
        	if(!empty($redveicle))
        	{
		        foreach ($redveicle as $key => $value)
		        {
		        	// print_r($value);
		            if ($value->dtcv != '')
		            {
		                $dtcv = explode(",", $value->dtcv);
		               
		                foreach ($dtcv as $key => $value1)
		                {
		                    //calc's for P U B C
		                    $errCodeType = $value1 & 0xC000;
		                    $errCodeType = $errCodeType >> 14;

		                    //echo "$errCodeType";
		                    if ($errCodeType == 1)
		                    {
		                        $errcodeHexStr = "C";
		                    }
		                    elseif ($errCodeType == 2)
		                    {
		                        $errcodeHexStr = "B";
		                    }
		                    elseif ($errCodeType == 3)
		                    {
		                        $errcodeHexStr = "U";
		                    }
		                    else
		                    {
		                        $errcodeHexStr = "P";
		                    }
		                    // end P U B C
		                    //Bit-wise AND the decimal value from ECU to get 2nd byte.
		                    $errCodeTmp = $value1 & 0x3000;
		                    $errCodeTmp = $errCodeTmp >> 12; //Right shift by 12 bits to get 2nd byte.
		                    $hx = dechex($errCodeTmp);
		                    $errcodeHexStr = $errcodeHexStr . "$hx";

		                    //Bit-wise AND the decimal value from ECU to get the 3rd byte
		                    $errCodeTmp = $value1 & 0x0F00;
		                    $errCodeTmp = $errCodeTmp >> 8; //Right shift by 8 bits to get the last DTC character.
		                    $hx = dechex($errCodeTmp);
		                    $errcodeHexStr = $errcodeHexStr . "$hx";

		                    $errCodeTmp = $value1 & 0x00FF;
		                    if ($errCodeTmp <= 0x0A)
		                    {
		                        $errcodeHexStr = $errcodeHexStr . "0";
		                        $hx = dechex($errCodeTmp);
		                        $errcodeHexStr = $errcodeHexStr . "$hx";
		                    }
		                    else
		                    {
		                        $hx = dechex($errCodeTmp);
		                        $errcodeHexStr = $errcodeHexStr . "$hx";
		                    }

		                    $errcodeHexStr = strtoupper($errcodeHexStr);

		                    $adtcs = Fetch::getactivedtc1($errcodeHexStr, $value->device_id);
		                    if (!empty($adtcs))
		                    {
		                        $userdetails = Fetch::getUserDeviceEmail($adtcs[0]->device_id);
		                        
		                        $Data[] = array(
		                            'dtc_num'     => $adtcs[0]->dtc_num,
		                            'description' => $adtcs[0]->description,
		                            'rating'      => $adtcs[0]->rating,
		                            'remedies'    => $adtcs[0]->remedies
		                        );
		                    }
		                    else
		                    {
		                        $userdetails = Fetch::getUserDeviceEmail($value->device_id);
		                        $Data[] = array(
		                            'dtc_num'     => $errcodeHexStr,
		                            'description' => 'Manufacturer Specific Code',
		                            'rating'      => '',
		                            'remedies'    => ''
		                        );
		                    }

		                }
		                $arrayData['success']      = TRUE;
		                $arrayData['data']         = $Data;
		            }
		            else
		            {
		                //Do nothing
		                
		            }
		        }
		    }
		    else
		    {
		    	$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1003";
				$arrayData['errorMessage'] = "Invalid Device ID!";
		    }
	    }
	    else
	    {
	    	$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1004";
			$arrayData['errorMessage'] = "Invalid Input!";
	    }
	    // print_r($arrayData);
        echo json_encode($arrayData);
    }
    public function getUserVehicleDetails(Request $request)
  	{
  		if(!empty($request->data))
        {
     		$obj     = json_decode($request->data, TRUE);
			$did     = $obj['devID'];
			$device  = Fetch::getdevice($did);
			if(!empty($device))
			{
		  		$username = Fetch::getUserDeviceEmail($did);
		  		$id       = Fetch::getIdByDeviceId($did);
		        $id       = $id[0]->id;
		        $vehicle  = Fetch::getVehicleById($id);
		        // print_r($vehicle);die;
		        if(!empty($vehicle))
		        {

			        if($vehicle->fuel_tpe == 1)
			        {
			        	$fuelType = 'Petrol';
			        }
			        else
			        {
			        	$fuelType = 'Diesel';
			        }
			        // print_r($vehicle);
			        $vehicle_sett = Fetch::getVehicleSettingData();
			        $manuArr      = explode(';', $vehicle_sett->manufacturer);
			        $engCapArr    = explode(';', $vehicle_sett->engine_capacity);
			      	if(!empty($vehicle->manufact))
			      	{

				      	
				        $vehicle_info = Fetch::getVehicleInfoDataById($vehicle->manufact);
				        $modelArr     = explode(';', $vehicle_info[0]->model);
				        $varArr       = explode(';', $vehicle_info[0]->var);

				        $dat = Fetch::getVehicleSpecDataByMfdModel($vehicle->manufact,$vehicle->model);
				        // print_r($dat);die;
				        // print_r($modelArr);
				        // print_r($varArr);
				        $mfd          = $manuArr[$vehicle->manufact]; 
				        $mfd		  = explode('_', $mfd);
				        $Data['veh_reg']  = $vehicle->reg_no;
				        $Data['odo_red']  = $vehicle->travel_purchase;
				        $Data['manufact'] = $mfd[0];
				        $Data['model']    = $modelArr[$vehicle->model];
				        $Data['variant']  = $varArr[$vehicle->varient];
				        $Data['fuel_type']  = $fuelType;
				        $Data['engine_cap'] = $engCapArr[$vehicle->eng_cap];
				        $Data['trans_type'] = $vehicle->trans_type;
				        $Data['mfg_year']   = $vehicle->manufact_yr;
				        $Data['bodyType']   = $dat[0]->btype;
				        
				        $arrayData['success']      = TRUE;
				        $arrayData['data']         = $Data;
				    }
				    else
				    {
				    	$arrayData['success']      = FALSE;
						$arrayData['errorCode']    = "1005";
						$arrayData['errorMessage'] = "No data available,Please add vehicle details.";
				    }
			    }
			    else
			    {
			    	$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1004";
					$arrayData['errorMessage'] = "No data available,Please add vehicle details.";
			    }
        	}
        	else
		    {
		    	$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1003";
				$arrayData['errorMessage'] = "Invalid Device ID!";
		    }
        }
        else
	    {
	    	$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1004";
			$arrayData['errorMessage'] = "Invalid Input!";
	    }
	    echo json_encode($arrayData);
  	}

  	// public function getAllVehicleDetails(Request $request)
  	// {
  	// 	$header = getallheaders();
  	// 	// print_r($header);die;
  	// 	if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  	// 	{
  	// 		$arrayData['success']      = FALSE;
   //    		$arrayData['errorCode']    = "1004";
   //    		$arrayData['errorMessage'] = "Invalid Authentication";
  	// 	}
  	// 	else
  	// 	{
	  //       $vehicleAll  = Fetch::getAllVehicle();

	  //       foreach ($vehicleAll as $key => $vehicle) 
	  //       {
	  //       	 // print_r($vehicle);
		 //        if($vehicle->fuel == 1)
		 //        {
		 //        	$fuelType = 'Petrol';
		 //        }
		 //        else
		 //        {
		 //        	$fuelType = 'Diesel';
		 //        }
		 //        // print_r($vehicle);
		 //        $vehicle_sett = Fetch::getVehicleSettingData();
		 //        $manuArr      = explode(';', $vehicle_sett->manufacturer);
		 //        $engCapArr    = explode(';', $vehicle_sett->engine_capacity);
		      	
		 //      	if(!empty($vehicle->mfd))
		 //      	{
			//         $vehicle_info = Fetch::getVehicleInfoDataById($vehicle->mfd);
			//         // print_r($vehicle_info);
			//         $modelArr     = explode(';', $vehicle_info[0]->model);
			//         $varArr       = explode(';', $vehicle_info[0]->var);

			//         $dat = Fetch::getVehicleSpecDataByMfdModel($vehicle->mfd,$vehicle->model);

			//         $mfd = $manuArr[$vehicle->mfd]; 
			//         $mfd = explode('_', $mfd);
			        
			//         $Data['manufact']      = $mfd[0];
			//         $Data['model']         = $modelArr[$vehicle->model];
			//         $Data['variant']       = $varArr[$vehicle->varient];
			//         $Data['fuel_type']     = $fuelType;
			//         $Data['engine_cap']    = $engCapArr[$vehicle->engcap];
			//         $Data['mfg_year']      = $vehicle->mfg_yr;
			//         $Data['bodyType']      = $dat[0]->btype;
			//         $Data['versionNumber'] = $dat[0]->version;
			//         $Data['id']            = $vehicle->id;
			//         $Data['calibLabel']    = json_decode($vehicle->specinfo);
			//         $Data1[]		       = $Data;
			//         // $arrayData['success']      = TRUE;
			//         // $arrayData['data']         = $Data;
			//     }
			// }
	  //       $arrayData['success']        = TRUE;
	  //       $arrayData['successMessage'] = "Data fetched successfully";
	  //       $arrayData['vehicleData']    = $Data1;

	  //   }
	  //   echo json_encode($arrayData);
  	// }

  	public function getAllVehicleDetails(Request $request)
  	{
  		$header = getallheaders();
  		// print_r($header);die;
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
	        $vehicleAll  = Fetch::getAllVehicle();
	        // print_r($vehicleAll);die;
	        foreach ($vehicleAll as $key => $vehicle) 
	        {
	        	 // print_r($vehicle);
		        if($vehicle->fuel == 1)
		        {
		        	$fuelType = 'Petrol';
		        }
		        else
		        {
		        	$fuelType = 'Diesel';
		        }
		        // print_r($vehicle);
		        $vehicle_sett = Fetch::getVehicleSettingData();
		        $manuArr      = explode(';', $vehicle_sett->manufacturer);
		        $engCapArr    = explode(';', $vehicle_sett->engine_capacity);
		      	
		      	if(!empty($vehicle))
		      	{
			        $vehicle_info = Fetch::getVehicleInfoDataById($vehicle->mfd);
			        // print_r($vehicle_info);
			        $modelArr     = explode(';', $vehicle_info[0]->model);
			        $varArr       = explode(';', $vehicle_info[0]->var);

			        $dat = Fetch::getVehicleSpecDataByMfdModel($vehicle->mfd,$vehicle->model);

			        $mfd = $manuArr[$vehicle->mfd]; 
			        $mfd = explode('_', $mfd);
			        
			        $Data['manufact']      = $mfd[0];
			        $Data['model']         = $modelArr[$vehicle->model];
			        $Data['variant']       = $varArr[$vehicle->varient];
			        $Data['fuel_type']     = $fuelType;
			        $Data['engine_cap']    = $engCapArr[$vehicle->engcap];
			        $Data['mfg_year']      = $vehicle->mfg_yr;
			        $Data['bodyType']      = $dat[0]->btype;
			        $Data['versionNumber'] = $dat[0]->version;
			        $Data['id']            = $vehicle->id;
			        $Data['Eng_Disp_C']    = $engCapArr[$vehicle->engcap];
			        $caliblabels           = json_decode($vehicle->specinfo);
					foreach ($caliblabels as $key => $value) 
	        		{
	        			$Data[$key] = $value;
	        		}
			        $Data1[] = $Data;
			    }
			}
	        $arrayData['success']        = TRUE;
	        $arrayData['calibUpdated']   = "1";
	        $arrayData['successMessage'] = "Data fetched successfully.";
	        $arrayData['vehicleData']    = $Data1;
	    }
	    echo json_encode($arrayData);
  	}
	
	// get all Operator
	public function getAllOperator(Request $request)
  	{
  		$header = getallheaders();
  		if(empty($header['Authorization']) || $this->verifykey($header['Authorization']) == 0)
  		{
  			$arrayData['success']      = FALSE;
      		$arrayData['errorCode']    = "1004";
      		$arrayData['errorMessage'] = "Invalid Authentication";
  		}
  		else
  		{
	        $operatoreAll  = Fetch::getAllOperatorActive();
	        foreach ($operatoreAll as $key => $operator) 
	        {
		        $Data['name']   = $operator->opr_name;
		        $Data['id']     = $operator->opr_id;
		        $Data['status'] = $operator->status;
		        $Data['plant_id'] = $operator->plant_id;
		        $Data['code']     = "1234";
		        $Data1[]          = $Data;
			}
	        $arrayData['success']        = TRUE;
	        $arrayData['operatorUpdated']= 1;
	        $arrayData['successMessage'] = "Data fetched successfully.";
	        $arrayData['data']           = $Data1;
	    }
	    echo json_encode($arrayData);
  	} 

  	// fuction to verify Auth key
  	function verifykey($key)
  	{
  		$res = Fetch::randKeyVerify($key);
  		if($res[0]->count > 0)
  		{
  			return 1;
  		}
  		else
  		{
  			return 0;
  		}
  		// print_r($res);die;
  	}

}
?>