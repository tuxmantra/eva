<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newuser;
use Illuminate\Support\Arr;
// use Mail;


class Api_device_v1 extends Controller
{
	public function get_validsuberm_1(Request $request)
	{
 		if(!empty($request->data))
       	{
         		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$currVer  = $obj['currVer'];
			$devInfo = Fetch::getdeviceActStatus($deviceID);
			if($deviceID != "" && $devInfo != "" && $devInfo[0]->status == 1)
			{
				$arrayData['success'] = TRUE;
				$arrayData['status'] = "1";
				$arrayData['subEnd'] = $devInfo[0]->expdt;
				$arrayData['cmds'] = $devInfo[0]->dev_cmds;
				$dev_cmds="";
				$update=Fetch::updateDeviceCmdCol($deviceID,$dev_cmds,$currVer);	
			}
			elseif($deviceID != "" && $devInfo != "" && $devInfo[0]->status == 0)
			{
				$arrayData['success']      = FALSE;
				$arrayData['status']       = "0";
				$arrayData['errorCode']    = "1003";
				$arrayData['errorMessage'] = "Subscription expired!";
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1001";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
         	}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1002";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
 	}

 	public function devdatauploadERM(Request $request)
 	{
 		if(!empty(file_get_contents("php://input")))
       	{    
           		$datMe = file_get_contents("php://input");
         		$array     = json_decode($datMe, TRUE);
           		$logFilePath = getcwd();
			$req_dump = print_r($datMe, TRUE);
			$filePath = $logFilePath. "/"."request.txt";
			// print_r($filePath);die;
			$fp = fopen($filePath, 'a');
			fwrite($fp, "\n---------------- devdatauploadERM - Request Begin --------------------\n");
			$currTimeStamp = date('Y-m-d H:i:s',time());
			fwrite($fp, $currTimeStamp);
			fwrite($fp, "\n");
			fwrite($fp, $req_dump);
			fwrite($fp, "\n----------------- devdatauploadERM - Request End --------------------\n");
			fclose($fp);
    		
			$deviceID = $array['devID'];
			$devInfo = Fetch::getdevice($deviceID);
			if(!empty($devInfo))
			{
			    	$pathInfo = getcwd();
				// echo "datme = $datMe \n";
				$cmd = "/usr/bin/timeout 30m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-resERM.php";
				$cmd.=" ";
				$cmd.="'$datMe'";

				exec($cmd . " > /dev/null &");
				// print_r($cmd);die;
				
				$arrayData['success'] = TRUE;
				$arrayData['status'] = "Data Updating!";
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1001";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success'] = FALSE;
			$arrayData['errorCode'] = "1002";
			$arrayData['errorMessage'] = "Invalid Input!";
			// http_response_code(400);
		}
		echo json_encode($arrayData);
	}

	public function devdatauploadERM_1(Request $request)
	{
 		  if(!empty(file_get_contents("php://input")))
           {    
           		$datMe = file_get_contents("php://input");
         		$array     = json_decode($datMe, TRUE);
           		$logFilePath = getcwd();
				$req_dump = print_r($datMe, TRUE);
				$filePath = $logFilePath. "/"."request.txt";
				// print_r($filePath);die;
				$fp = fopen($filePath, 'a');
				fwrite($fp, "\n---------------- devdatauploadERM - Request Begin --------------------\n");
				$currTimeStamp = date('Y-m-d H:i:s',time());
				fwrite($fp, $currTimeStamp);
				fwrite($fp, "\n");
				fwrite($fp, $req_dump);
				fwrite($fp, "\n----------------- devdatauploadERM - Request End --------------------\n");
				fclose($fp);
           		
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			    	    $pathInfo = getcwd();
						// echo "datme = $datMe \n";
						$cmd = "/usr/bin/timeout 30m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-resERM.php";
						$cmd.=" ";
						$cmd.="'$datMe'";

						exec($cmd . " > /dev/null &");
						// print_r($cmd);die;
						
						$arrayData['success'] = TRUE;
						$arrayData['status'] = "Data Updating!";
			    }
			    else
				{
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
				// http_response_code(400);
			}
			echo json_encode($arrayData);
		}

		public function set_device_dtc(Request $request){

 		  if(!empty(file_get_contents("php://input")))
           {

           	 $datMe = file_get_contents("php://input");
         	 $array    = json_decode($datMe, TRUE);
           	 $deviceID = $array['devID'];
    			
		    // get Device Data
		     $devInfo = Fetch::getdevice($deviceID);
		    if(!empty($devInfo))
		    {
		        $dtc = $array['dtcData'];
				if($dtc != "")
				{
					$dtc = 1;
				}
				else
				{
					$dtc = 0;
				}
				$dtcv = $array['dtcData'];
		        // print_r($devInfo);die;
		        // //To update the same row in the database
		        // $setFields = array('device_id');
		        // $setVal = array($deviceID);
		        $resultC = Fetch::updatedeviceDtcv($deviceID,$dtcv,$dtc);
		        if($resultC)
		        {
		            // Data Uploaded successfully
		            $arrayData['success'] = TRUE;
		            $arrayData['dtc'] = "DTC Updated!";
		        }
		        else
		        {
		            // Data failed to update
		            $arrayData['success'] = FALSE;
		            $arrayData['errorCode'] = "1004";
		            $arrayData['errorMessage'] = "DTC Update Failed!";
		        }
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1001";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
           }
           else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
			}
			echo json_encode($arrayData);
       }

	public function devrawdataupload(Request $request){

 		  if(!empty(file_get_contents("php://input")))
           {    
           		$datMe = file_get_contents("php://input");
         		$array     = json_decode($datMe, TRUE);
           		$logFilePath = getcwd();
				
           		
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			    	    $pathInfo = getcwd();
						// echo "datme = $datMe \n";
							$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/devrawdataupload4w_back.php '$datMe'";
						$cmd.=" ";
						$cmd.="'$datMe'";

						exec($cmd . " > /dev/null &");
						// print_r($cmd);die;
						
						$arrayData['success'] = TRUE;
						$arrayData['status'] = "File Uploaded and database updating!";
			    }
			    else
				{
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
				// http_response_code(400);
			}
			echo json_encode($arrayData);
		}

		public function devrawdataupload_2w(Request $request){

 		  if(!empty(file_get_contents("php://input")))
           {    
           		$datMe = file_get_contents("php://input");
         		$array     = json_decode($datMe, TRUE);
           		$logFilePath = getcwd();
				
           		
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			    	    $pathInfo = getcwd();
						// echo "datme = $datMe \n";
							$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/devrawdataupload2w_back.php '$datMe'";
						$cmd.=" ";
						$cmd.="'$datMe'";

						exec($cmd . " > /dev/null &");
						// print_r($cmd);die;
						
						$arrayData['success'] = TRUE;
						$arrayData['status'] = "File Uploaded and database updating!";
			    }
			    else
				{
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
				// http_response_code(400);
			}
			echo json_encode($arrayData);
		}

		public function devdataupload(Request $request){

 		  if(!empty(file_get_contents("php://input")))
           {    
           		$datMe = file_get_contents("php://input");
         		$array     = json_decode($datMe, TRUE);
           		$logFilePath = getcwd();
				
           		
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			    	    $pathInfo = getcwd();
						// echo "datme = $datMe \n";
							$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-res4w.php '$datMe'";
						$cmd.=" ";
						$cmd.="'$datMe'";

						exec($cmd . " > /dev/null &");
						// print_r($cmd);die;
						
						$arrayData['success'] = TRUE;
						$arrayData['status'] = "File Uploaded and database updating!";
			    }
			    else
				{
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
				// http_response_code(400);
			}
			echo json_encode($arrayData);
		}

		public function devdataupload2w(Request $request){

 		  if(!empty(file_get_contents("php://input")))
           {    
           		$datMe = file_get_contents("php://input");
         		$array     = json_decode($datMe, TRUE);
           		$logFilePath = getcwd();
				
           		
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			    	    $pathInfo = getcwd();
						// echo "datme = $datMe \n";
							$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-res2w.php '$datMe'";
						$cmd.=" ";
						$cmd.="'$datMe'";

						exec($cmd . " > /dev/null &");
						// print_r($cmd);die;
						
						$arrayData['success'] = TRUE;
						$arrayData['status'] = "File Uploaded and database updating!";
			    }
			    else
				{
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
				// http_response_code(400);
			}
			echo json_encode($arrayData);
		}

		public function devdataupload2w_tvs(Request $request){

 		  if(!empty(file_get_contents("php://input")))
           {    
           		$datMe = file_get_contents("php://input");
         		$array     = json_decode($datMe, TRUE);
           		$logFilePath = getcwd();
				
           		
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			    	    $pathInfo = getcwd();
						// echo "datme = $datMe \n";
							$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/process-mon-res2w_tvs.php '$datMe'";
						$cmd.=" ";
						$cmd.="'$datMe'";

						exec($cmd . " > /dev/null &");
						// print_r($cmd);die;
						
						$arrayData['success'] = TRUE;
						$arrayData['status'] = "File Uploaded and database updating!";
			    }
			    else
				{
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
				// http_response_code(400);
			}
			echo json_encode($arrayData);
		}

 	public function device_event(Request $request){

 		  if(!empty(file_get_contents("php://input")))
           {
           		$datMe = file_get_contents("php://input");
         		$array     = json_decode($datMe, TRUE);
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			        $gpsData = array();
			        $gpsData['device_id'] = $array['devID'];
			        $gpsData['lat'] = $array['lat'];
			        $gpsData['lon'] = $array['lon'];
			        $gpsData['alt'] = $array['alt'];
			        if($array['ts'] == 0)
			        {
			            $array['ts'] = time();
			        }
			        else
			        {
			            $array['ts'] = $array['ts'] + 946684800; // To convert time from UTC since 2000 to UTC since 1970   
			        }
			        $gpsData['time_s']  = ($array['ts'] * 1000);
			        $gpsData['ad_dt']   = date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10));
			        $gpsData['event']   = $array['ev'];
			        $gpsData['nodrive'] = $array['dn'];
			        $gpsData['seqno']   = $array['s'];
			        $gpsData['gps']   = $array['gps'];
			        $saveData = Fetch::insertDbSingle_event($gpsData);
			        if($saveData)
			        {
			            //Data Uploaded successfully
			            // Also Update "device" table to get the last know location of the device.
			            $setLoc=Fetch::setDeviceLastLocation($array['lat'],$array['lon'],$array['alt'],date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10)),$deviceID);
			            $arrayData['success'] = TRUE;
			            $arrayData['message'] = "Event updated successfully";
			        }
			        else
			        {
			            //Data failed to upload
			            $arrayData['success']      = FALSE;
			            $arrayData['errorCode']    = "1004";
			            $arrayData['errorMessage'] = "Data cannot be updated kindly try again later";
			        }
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}



		   }
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
			}

		echo json_encode($arrayData);
    	}

    	public function device_event_2w(Request $request)
    	{
 		if(!empty(file_get_contents("php://input")))
           	{
           		$datMe = file_get_contents("php://input");
         		$array     = json_decode($datMe, TRUE);
				$deviceID = $array['devID'];
				$devInfo = Fetch::getdevice($deviceID);
				if(!empty($devInfo))
			    {
			        $gpsData = array();
			        $gpsData['device_id'] = $array['devID'];
			        $gpsData['lat'] = $array['lat'];
			        $gpsData['lon'] = $array['lon'];
			        $gpsData['alt'] = $array['alt'];
			        if($array['ts'] == 0)
			        {
			            $array['ts'] = time();
			        }
			        else
			        {
			            $array['ts'] = $array['ts'] + 946684800; // To convert time from UTC since 2000 to UTC since 1970   
			        }
			        $gpsData['time_s']  = ($array['ts'] * 1000);
			        $gpsData['ad_dt']   = date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10));
			        $gpsData['event']   = $array['ev'];
			        $gpsData['nodrive'] = $array['dn'];
			        $gpsData['seqno']   = $array['s'];
			        $gpsData['gps']   = $array['gps'];
			        $saveData = Fetch::insertDbSingle_event($gpsData);
			        if($saveData)
			        {
			            //Data Uploaded successfully
			            // Also Update "device" table to get the last know location of the device.
			            $setLoc=Fetch::setDeviceLastLocation($array['lat'],$array['lon'],$array['alt'],date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10)),$deviceID);
			            $arrayData['success'] = TRUE;
			            $arrayData['message'] = "Event updated successfully";
			        }
			        else
			        {
			            //Data failed to upload
			            $arrayData['success']      = FALSE;
			            $arrayData['errorCode']    = "1004";
			            $arrayData['errorMessage'] = "Data cannot be updated kindly try again later";
			        }
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}



		   }
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
			}

		echo json_encode($arrayData);
    	}

 	public function get_valid_sub(Request $request)
 	{
 		if(!empty($request->data))
         	{
         		$obj      = json_decode($request->data, TRUE);
				$deviceID = $obj['devID'];
				$currVer  = $obj['currVer'];
				$devInfo = Fetch::getdeviceActStatus($deviceID);
				if($deviceID != "" && $devInfo != "" && $devInfo[0]->status == 1)
				{
					$arrayData['success'] = TRUE;
					$arrayData['status'] = "1";
					$arrayData['subEnd'] = $devInfo[0]->expdt;
					$arrayData['cmds'] = $devInfo[0]->dev_cmds;
					$dev_cmds="";
					$update=Fetch::updateDeviceCmdCol($deviceID,$dev_cmds,$currVer);	
				}
				elseif($deviceID != "" && $devInfo != "" && $devInfo[0]->status == 0)
				{
					$arrayData['success']      = FALSE;
					$arrayData['status']       = "0";
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = "Subscription expired!";
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
         	}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1002";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
 	}

 	public function file_upload_fleet(Request $request){

 			if(isset($_POST['devID']) && $_FILES['csvfile']['name'] != "")
			{
				$fileName = $_FILES['csvfile']['name'];
				$fname = explode('.',$fileName);
				$ext = strtolower($fname[count($fname)-1]);
				$deviceID = $_POST['devID'];
				$devInfo = Fetch::getdeviceActStatus($deviceID);
				if($deviceID != "" && $devInfo[0] != "" && $ext == 'csv')
				{
					$uploaddir = "incoming-csv/".$fileName;
					if(file_exists($uploaddir))
					{
						unlink($uploaddir);
					}
					if(move_uploaded_file($_FILES['csvfile']['tmp_name'],$uploaddir))
					{    
						$tmpName = $uploaddir;//"alert/ECE201606130001.csv";
						// print_r($tmpName);die;
						$file = Fetch::getFileListForDevice($deviceID);
						// print_r($file[0]);	die;
						$upload = Fetch::setdeviceFile($deviceID,$fileName);
						if($upload)
						{
							$pathInfo = getcwd();
							//$cmd = "/usr/bin/php $pathInfo/fileupload-back.php $fileName";
							$cmd      = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/fileupload-back-fleet.php $fileName";

							//$cmd = "/usr/bin/timeout -k 5m 4m /usr/bin/php $pathInfo/fileupload-back.php $fileName";
							exec($cmd . " > /dev/null &");

							$arrayData['success'] = TRUE;
							$arrayData['status']  = "File Uploaded and database updating!";
						}
						else
						{
							$arrayData['success'] = TRUE;
							$arrayData['status']  = "File Uploaded waiting for last process!";
						}
					}
					else
					{
						$arrayData['success']   = FALSE;
						$arrayData['errorCode'] = "1003";
						$arrayData['status']    = "File Upload Failed!";
					}
				}
				elseif($ext != 'csv')
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1002";
					$arrayData['errorMessage'] = "Invalid File Type!";
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
			}
			echo json_encode($arrayData);

     	}

     	public function mail_test(Request $request)
     	{
     
         	  $data  = new \stdClass();
              $data->name = "Mayur";
              Mail::to(""."mjuttanavar76@gmail.com")->send(new Newuser($data));
		      echo "Basic Email Sent. Check your inbox.";
     	}

 	public function get_cmdres(Request $request)
 	{
 		if(!empty($request->data))
         	{
         		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$res      = $obj['res'];
			$devInfo = Fetch::getdeviceActStatus($deviceID);
			if($deviceID != "" && $devInfo != "" )
			{
				$arrayData['success'] = TRUE;
				$arrayData['msg']     = "Response Updated in DB!";
				$status=Fetch::updateDeviceCmdres($deviceID,$res);
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1001";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1002";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
	}

 	public function get_valid_sub_2w(Request $request)
 	{
 		if(!empty($request->data))
         	{
         		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$currVer  = $obj['currVer'];
			$devInfo = Fetch::getdeviceActStatus($deviceID);
			if($deviceID != "" && $devInfo != "" && $devInfo[0]->status == 1)
			{
				$arrayData['success'] = TRUE;
				$arrayData['status'] = "1";
				$arrayData['subEnd'] = $devInfo[0]->expdt;
				$arrayData['cmds'] = $devInfo[0]->dev_cmds;
				$dev_cmds="";
				$update=Fetch::updateDeviceCmdCol($deviceID,$dev_cmds,$currVer);	
			}
			elseif($deviceID != "" && $devInfo != "" && $devInfo[0]->status == 0)
			{
				$arrayData['success']      = FALSE;
				$arrayData['status']       = "0";
				$arrayData['errorCode']    = "1003";
				$arrayData['errorMessage'] = "Subscription expired!";
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1001";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
         	}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1002";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
 	}

 	public function get_valid_sub_ERM(Request $request)
 	{
 		if(!empty($request->data))
         	{
         		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			$currVer  = $obj['currVer'];
			$devInfo = Fetch::getdeviceActStatus($deviceID);
			if($deviceID != "" && $devInfo != "" && $devInfo[0]->status == 1)
			{
				$arrayData['success'] = TRUE;
				$arrayData['status'] = "1";
				$arrayData['subEnd'] = $devInfo[0]->expdt;
				$arrayData['cmds'] = $devInfo[0]->dev_cmds;
				$dev_cmds="";
				$update=Fetch::updateDeviceCmdCol($deviceID,$dev_cmds,$currVer);	
			}
			elseif($deviceID != "" && $devInfo != "" && $devInfo[0]->status == 0)
			{
				$arrayData['success']      = FALSE;
				$arrayData['status']       = "0";
				$arrayData['errorCode']    = "1003";
				$arrayData['errorMessage'] = "Subscription expired!";
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1001";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
         	}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1002";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData);
 	}

 	public function get_device_config(Request $request)
 	{
 		if(!empty($request->data))
       	{
         		// Valid Bearer Key Found.
         		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			
			// Get Device Data
			$devInfo = Fetch::getdevice($deviceID);
			// print_r($devInfo);
			$did     = $devInfo[0]->id;
			
			// Get Vehicle details
			$vehi  = Fetch::getVehicleById($did);
			$vType = $vehi->fuel_tpe;
			$encryptKey = rand(25, 300);
			if(!empty($devInfo))
			{
				$yVal = 0;
				$list = array("Eng_eff_C","Eng_Disp_C","TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C","Eff_RPM_Limit2_C","Eff_RPM_Limit3_C","Eff_RPM_Limit4_C","Eff_RPM_Limit5_C",
					"Eqratio_Eload_1_10_RPMLimit1_C","Eqratio_Eload_1_10_RPMLimit2_C","Eqratio_Eload_1_10_RPMLimit3_C","Eqratio_Eload_1_10_RPMLimit4_C",
					"Eqratio_Eload_1_10_RPMLimit5_C","Eqratio_Eload_11_20_RPMLimit1_C","Eqratio_Eload_11_20_RPMLimit2_C","Eqratio_Eload_11_20_RPMLimit3_C",
					"Eqratio_Eload_11_20_RPMLimit4_C","Eqratio_Eload_11_20_RPMLimit5_C","Eqratio_Eload_21_30_RPMLimit1_C","Eqratio_Eload_21_30_RPMLimit2_C",
					"Eqratio_Eload_21_30_RPMLimit3_C","Eqratio_Eload_21_30_RPMLimit4_C","Eqratio_Eload_21_30_RPMLimit5_C","Eqratio_Eload_31_40_RPMLimit1_C",
					"Eqratio_Eload_31_40_RPMLimit2_C","Eqratio_Eload_31_40_RPMLimit3_C","Eqratio_Eload_31_40_RPMLimit4_C","Eqratio_Eload_31_40_RPMLimit5_C",
					"Eqratio_Eload_41_50_RPMLimit1_C","Eqratio_Eload_41_50_RPMLimit2_C","Eqratio_Eload_41_50_RPMLimit3_C","Eqratio_Eload_41_50_RPMLimit4_C",
					"Eqratio_Eload_41_50_RPMLimit5_C","Eqratio_Eload_51_60_RPMLimit1_C","Eqratio_Eload_51_60_RPMLimit2_C","Eqratio_Eload_51_60_RPMLimit3_C",
					"Eqratio_Eload_51_60_RPMLimit4_C","Eqratio_Eload_51_60_RPMLimit5_C","Eqratio_Eload_61_80_RPMLimit1_C","Eqratio_Eload_61_80_RPMLimit2_C",
					"Eqratio_Eload_61_80_RPMLimit3_C","Eqratio_Eload_61_80_RPMLimit4_C","Eqratio_Eload_61_80_RPMLimit5_C","Eqratio_Eload_81_90_RPMLimit1_C",
					"Eqratio_Eload_81_90_RPMLimit2_C","Eqratio_Eload_81_90_RPMLimit3_C","Eqratio_Eload_81_90_RPMLimit4_C","Eqratio_Eload_81_90_RPMLimit5_C",
					"Eqratio_Eload_91_94_RPMLimit1_C","Eqratio_Eload_91_94_RPMLimit2_C","Eqratio_Eload_91_94_RPMLimit3_C","Eqratio_Eload_91_94_RPMLimit4_C",
					"Eqratio_Eload_91_94_RPMLimit5_C","Eqratio_Eload_95_98_RPMLimit1_C","Eqratio_Eload_95_98_RPMLimit2_C","Eqratio_Eload_95_98_RPMLimit3_C",
					"Eqratio_Eload_95_98_RPMLimit4_C","Eqratio_Eload_95_98_RPMLimit5_C","Eqratio_Eload_99_100_RPMLimit1_C","Eqratio_Eload_99_100_RPMLimit2_C",
					"Eqratio_Eload_99_100_RPMLimit3_C","Eqratio_Eload_99_100_RPMLimit4_C","Eqratio_Eload_99_100_RPMLimit5_C","Ovr_Run_EqRatio_C",
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C",
					"VolEff_Eload_31_40_RPMLimit4_C","VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C",
					"VolEff_Eload_41_50_RPMLimit3_C","VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C",
					"VolEff_Eload_51_60_RPMLimit2_C","VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C",
					"VolEff_Eload_61_80_RPMLimit1_C","VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C",
					"VolEff_Eload_61_80_RPMLimit5_C","VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C",
					"VolEff_Eload_81_90_RPMLimit4_C","VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C",
					"VolEff_Eload_91_94_RPMLimit3_C","VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C",
					"VolEff_Eload_95_98_RPMLimit2_C","VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C",
					"VolEff_Eload_99_100_RPMLimit1_C","VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C",
					"VolEff_Eload_99_100_RPMLimit5_C","Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C",
					"Accl_Eload_C","Decl_Val_C","Eload_Def_C","Eload_Max_C","Max_Torque_C","ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C","Osc_RPM_Lmt2_C","Osc_RPM_Lmt3_C","Osc_RPM_Lmt4_C","CRP_RPM_PkLmt_C","CRP_Type_C",
					"CRP_MaxLmt3_C","CRP_MaxLmt1_C","Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C","RPM_Idle_Ld1_C","RPM_Idle_Ld2_C","MAP_RPMOffset_PkLmt_C","Max_Air_Trq_RPM_C","MAP_Air_RPM_LwrLmt_C",
					"MAP_Air_RPM_PkLmt_C","MAP_Air_EloadOffset_PkLmt_C","MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C",
					"Time_Upld_Frq_C","MAP_Type_C","IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C","Fact_Fuel_Run_ELoad_20_40_C","Fact_Fuel_Run_ELoad_40_60_C",
					"Fact_Fuel_Run_ELoad_60_80_C","Fact_Fuel_Run_ELoad_80_100_C","Fact_Fuel_Idle_ELoad1_C","Fact_Fuel_Idle_ELoad2_C","Ovr_Run_FuelFact_C",
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C","gr_6_low_C","gr_6_high_C","Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C",
					"Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C","Gr_RPM_Col6_C",
					"Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C","Thr_RPM_Col4_C",
					"Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C","Thr_Pos_Row2_C",
					"Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C",
					"Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C","Spd_Chng_Neg_Row4_C","Spd_Chng_Neg_Row3_C","Spd_Chng_Neg_Row2_C",
					"Spd_Chng_Neg_Row1_C","Spd_Chng_Row0_C","Spd_Chng_Pos_Row1_C","Spd_Chng_Pos_Row2_C","Spd_Chng_Pos_Row3_C","Spd_Chng_Pos_Row4_C",
					"Spd_Chng_Pos_Row5_C","Ovr_Spd_Lmt_C","Brake_EngSt3_Acc_C","Brake_EngSt4_Acc_C","Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C","Reserved9",
					"Reserved10","Reserved11","Reserved12","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$listVal = array("Eng_eff_C","Eng_Disp_C"=>array(100,'*'),"TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C"=>array(50,'/'),
					"Eff_RPM_Limit2_C"=>array(50,'/'),"Eff_RPM_Limit3_C"=>array(50,'/'),"Eff_RPM_Limit4_C"=>array(50,'/'),"Eff_RPM_Limit5_C"=>array(50,'/'),
					"Eqratio_Eload_1_10_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_1_10_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_21_30_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_51_60_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_91_94_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit5_C"=>array(200,'*'),"Ovr_Run_EqRatio_C"=>array(100,'*'),
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C","VolEff_Eload_31_40_RPMLimit4_C",
					"VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C","VolEff_Eload_41_50_RPMLimit3_C",
					"VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C","VolEff_Eload_51_60_RPMLimit2_C",
					"VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C","VolEff_Eload_61_80_RPMLimit1_C",
					"VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C","VolEff_Eload_61_80_RPMLimit5_C",
					"VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C","VolEff_Eload_81_90_RPMLimit4_C",
					"VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C","VolEff_Eload_91_94_RPMLimit3_C",
					"VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C","VolEff_Eload_95_98_RPMLimit2_C",
					"VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C","VolEff_Eload_99_100_RPMLimit1_C",
					"VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C","VolEff_Eload_99_100_RPMLimit5_C",
					"Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C"=>array(20,'*'),"Accl_Eload_C",
					"Decl_Val_C"=>array(20,'*'),"Eload_Def_C","Eload_Max_C","Max_Torque_C"=>array(5,'/'),"ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C"=>array(50,'/'),"Osc_RPM_Lmt2_C"=>array(50,'/'),"Osc_RPM_Lmt3_C"=>array(50,'/'),
					"Osc_RPM_Lmt4_C"=>array(50,'/'),"CRP_RPM_PkLmt_C"=>array(50,'/'),"CRP_Type_C"=>array(1000,'/'),"CRP_MaxLmt3_C"=>array(500,'/'),
					"CRP_MaxLmt1_C"=>array(500,'/'),"Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C"=>array(50,'/'),"RPM_Idle_Ld1_C"=>array(50,'/'),"RPM_Idle_Ld2_C"=>array(50,'/'),"MAP_RPMOffset_PkLmt_C",
					"Max_Air_Trq_RPM_C"=>array(50,'/'),"MAP_Air_RPM_LwrLmt_C"=>array(50,'/'),"MAP_Air_RPM_PkLmt_C"=>array(50,'/'),"MAP_Air_EloadOffset_PkLmt_C",
					"MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C","Time_Upld_Frq_C"=>array(3,'/'),"MAP_Type_C",
					"IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_20_40_C"=>array(100,'*'),
					"Fact_Fuel_Run_ELoad_40_60_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_60_80_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_80_100_C"=>array(100,'*'),
					"Fact_Fuel_Idle_ELoad1_C"=>array(100,'*'),"Fact_Fuel_Idle_ELoad2_C"=>array(100,'*'),"Ovr_Run_FuelFact_C"=>array(100,'*'),
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C"=>array(10000,'*'),"gr_6_low_C"=>array(10000,'*'),"gr_6_high_C"=>array(10000,'*'),
					"Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C","Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C",
					"Gr_RPM_Col6_C","Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C",
					"Thr_RPM_Col4_C","Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C",
					"Thr_Pos_Row2_C","Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C","Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C"=>array(20,'*'),"Spd_Chng_Neg_Row4_C"=>array(20,'*'),
					"Spd_Chng_Neg_Row3_C"=>array(20,'*'),"Spd_Chng_Neg_Row2_C"=>array(20,'*'),"Spd_Chng_Neg_Row1_C"=>array(20,'*'),
					"Spd_Chng_Row0_C"=>array(20,'*'),"Spd_Chng_Pos_Row1_C"=>array(20,'*'),"Spd_Chng_Pos_Row2_C"=>array(20,'*'),
					"Spd_Chng_Pos_Row3_C"=>array(20,'*'),"Spd_Chng_Pos_Row4_C"=>array(20,'*'),"Spd_Chng_Pos_Row5_C"=>array(20,'*'),"Ovr_Spd_Lmt_C",
					"Brake_EngSt3_Acc_C"=>array(20,'*'),"Brake_EngSt4_Acc_C"=>array(20,'*'),"Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C","Reserved9","Reserved10",
					"Reserved11","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$dat        = array('Yes'=>'1', 'No' => '0', 'Direct' => '1', 'Indirect' => '0', 'Absolute'=>'1', 'Differential'=>'0');
				//Eng_type
				$arrayData['success'] = TRUE;
				$arrayDataM = array();
				array_push($arrayDataM,$vType);

				$data1   = Fetch::getCaliPramVal($deviceID);

				$dataArr = $data1['calval'];
				$dataArr=json_decode($dataArr, TRUE);
				// print_r();
				$count = count($list);
				for($i=0; $i<count($list); $i++)
				{

					$val   = trim($list[$i]);
					// print_r($val);die;
					$data  = $dataArr['Eng_eff_C'];
					$data  = (isset($dat[trim($data)])) ? $dat[trim($data)] : $data ;
					$data  = ($data == "") ? 0 : $data ;

					if(isset($listVal[$list[$i]]) && is_array($listVal[$list[$i]]) )
					{
						$calC = $listVal[$list[$i]];
						if($calC[1] == "*")
						{
							$data = $data * $calC[0];
						}
						elseif($calC[1] == "/")
						{
							$data = round($data / $calC[0],0);
						}
					}

					// Encyption Logic for Config data
					if(($i == 82) || ($i == 83))
					{
						if($i == 82)
						{
							$data = 32; // Value 32 says the array is encrypted
						}
						elseif($i == 83)
						{
							$data = $encryptKey;
						}
					}
					else
					{
						if(($data == 0))// || ($data >= 255))
						{
							// Dont Encrypt if the value is Zero
							$data = 0;
						}
						else
						{
							$data = (($data + 26) ^ $encryptKey);
							// $data = $data;
						}
					}
					
					array_push($arrayDataM,$data);
				}
				$arrayData['data'] = array_values($arrayDataM);
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1001";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}

         	}
         	else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData,JSON_NUMERIC_CHECK);	
     	}

     	public function get_device_config_ERM(Request $request)
     	{
 		if(!empty($request->data))
         	{
         		// Valid Bearer Key Found.
         		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			// print_r($obj);die;
			// Get Device Data
			$devInfo = Fetch::getdevice($deviceID);
			// print_r($devInfo);die;
			$did     = $devInfo[0]->id;
			
			// Get Vehicle details
			$vehi  = Fetch::getVehicleById($did);
			$vType = $vehi->fuel_tpe;
			$encryptKey = rand(25, 300);
			if(!empty($devInfo))
			{
				$yVal = 0;
				$list = array("Eng_eff_C","Eng_Disp_C","TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C","Eff_RPM_Limit2_C","Eff_RPM_Limit3_C","Eff_RPM_Limit4_C","Eff_RPM_Limit5_C",
					"Eqratio_Eload_1_10_RPMLimit1_C","Eqratio_Eload_1_10_RPMLimit2_C","Eqratio_Eload_1_10_RPMLimit3_C","Eqratio_Eload_1_10_RPMLimit4_C",
					"Eqratio_Eload_1_10_RPMLimit5_C","Eqratio_Eload_11_20_RPMLimit1_C","Eqratio_Eload_11_20_RPMLimit2_C","Eqratio_Eload_11_20_RPMLimit3_C",
					"Eqratio_Eload_11_20_RPMLimit4_C","Eqratio_Eload_11_20_RPMLimit5_C","Eqratio_Eload_21_30_RPMLimit1_C","Eqratio_Eload_21_30_RPMLimit2_C",
					"Eqratio_Eload_21_30_RPMLimit3_C","Eqratio_Eload_21_30_RPMLimit4_C","Eqratio_Eload_21_30_RPMLimit5_C","Eqratio_Eload_31_40_RPMLimit1_C",
					"Eqratio_Eload_31_40_RPMLimit2_C","Eqratio_Eload_31_40_RPMLimit3_C","Eqratio_Eload_31_40_RPMLimit4_C","Eqratio_Eload_31_40_RPMLimit5_C",
					"Eqratio_Eload_41_50_RPMLimit1_C","Eqratio_Eload_41_50_RPMLimit2_C","Eqratio_Eload_41_50_RPMLimit3_C","Eqratio_Eload_41_50_RPMLimit4_C",
					"Eqratio_Eload_41_50_RPMLimit5_C","Eqratio_Eload_51_60_RPMLimit1_C","Eqratio_Eload_51_60_RPMLimit2_C","Eqratio_Eload_51_60_RPMLimit3_C",
					"Eqratio_Eload_51_60_RPMLimit4_C","Eqratio_Eload_51_60_RPMLimit5_C","Eqratio_Eload_61_80_RPMLimit1_C","Eqratio_Eload_61_80_RPMLimit2_C",
					"Eqratio_Eload_61_80_RPMLimit3_C","Eqratio_Eload_61_80_RPMLimit4_C","Eqratio_Eload_61_80_RPMLimit5_C","Eqratio_Eload_81_90_RPMLimit1_C",
					"Eqratio_Eload_81_90_RPMLimit2_C","Eqratio_Eload_81_90_RPMLimit3_C","Eqratio_Eload_81_90_RPMLimit4_C","Eqratio_Eload_81_90_RPMLimit5_C",
					"Eqratio_Eload_91_94_RPMLimit1_C","Eqratio_Eload_91_94_RPMLimit2_C","Eqratio_Eload_91_94_RPMLimit3_C","Eqratio_Eload_91_94_RPMLimit4_C",
					"Eqratio_Eload_91_94_RPMLimit5_C","Eqratio_Eload_95_98_RPMLimit1_C","Eqratio_Eload_95_98_RPMLimit2_C","Eqratio_Eload_95_98_RPMLimit3_C",
					"Eqratio_Eload_95_98_RPMLimit4_C","Eqratio_Eload_95_98_RPMLimit5_C","Eqratio_Eload_99_100_RPMLimit1_C","Eqratio_Eload_99_100_RPMLimit2_C",
					"Eqratio_Eload_99_100_RPMLimit3_C","Eqratio_Eload_99_100_RPMLimit4_C","Eqratio_Eload_99_100_RPMLimit5_C","Ovr_Run_EqRatio_C",
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C",
					"VolEff_Eload_31_40_RPMLimit4_C","VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C",
					"VolEff_Eload_41_50_RPMLimit3_C","VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C",
					"VolEff_Eload_51_60_RPMLimit2_C","VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C",
					"VolEff_Eload_61_80_RPMLimit1_C","VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C",
					"VolEff_Eload_61_80_RPMLimit5_C","VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C",
					"VolEff_Eload_81_90_RPMLimit4_C","VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C",
					"VolEff_Eload_91_94_RPMLimit3_C","VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C",
					"VolEff_Eload_95_98_RPMLimit2_C","VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C",
					"VolEff_Eload_99_100_RPMLimit1_C","VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C",
					"VolEff_Eload_99_100_RPMLimit5_C","Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C",
					"Accl_Eload_C","Decl_Val_C","Eload_Def_C","Eload_Max_C","Max_Torque_C","ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C","Osc_RPM_Lmt2_C","Osc_RPM_Lmt3_C","Osc_RPM_Lmt4_C","CRP_RPM_PkLmt_C","CRP_Type_C",
					"CRP_MaxLmt3_C","CRP_MaxLmt1_C","Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C","RPM_Idle_Ld1_C","RPM_Idle_Ld2_C","MAP_RPMOffset_PkLmt_C","Max_Air_Trq_RPM_C","MAP_Air_RPM_LwrLmt_C",
					"MAP_Air_RPM_PkLmt_C","MAP_Air_EloadOffset_PkLmt_C","MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C",
					"Time_Upld_Frq_C","MAP_Type_C","IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C","Fact_Fuel_Run_ELoad_20_40_C","Fact_Fuel_Run_ELoad_40_60_C",
					"Fact_Fuel_Run_ELoad_60_80_C","Fact_Fuel_Run_ELoad_80_100_C","Fact_Fuel_Idle_ELoad1_C","Fact_Fuel_Idle_ELoad2_C","Ovr_Run_FuelFact_C",
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C","gr_6_low_C","gr_6_high_C","Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C",
					"Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C","Gr_RPM_Col6_C",
					"Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C","Thr_RPM_Col4_C",
					"Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C","Thr_Pos_Row2_C",
					"Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C",
					"Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C","Spd_Chng_Neg_Row4_C","Spd_Chng_Neg_Row3_C","Spd_Chng_Neg_Row2_C",
					"Spd_Chng_Neg_Row1_C","Spd_Chng_Row0_C","Spd_Chng_Pos_Row1_C","Spd_Chng_Pos_Row2_C","Spd_Chng_Pos_Row3_C","Spd_Chng_Pos_Row4_C",
					"Spd_Chng_Pos_Row5_C","Ovr_Spd_Lmt_C","Brake_EngSt3_Acc_C","Brake_EngSt4_Acc_C","Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C","Reserved9",
					"Reserved10","Reserved11","Reserved12","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$listVal = array("Eng_eff_C","Eng_Disp_C"=>array(100,'*'),"TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C"=>array(50,'/'),
					"Eff_RPM_Limit2_C"=>array(50,'/'),"Eff_RPM_Limit3_C"=>array(50,'/'),"Eff_RPM_Limit4_C"=>array(50,'/'),"Eff_RPM_Limit5_C"=>array(50,'/'),
					"Eqratio_Eload_1_10_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_1_10_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_21_30_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_51_60_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_91_94_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit5_C"=>array(200,'*'),"Ovr_Run_EqRatio_C"=>array(100,'*'),
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C","VolEff_Eload_31_40_RPMLimit4_C",
					"VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C","VolEff_Eload_41_50_RPMLimit3_C",
					"VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C","VolEff_Eload_51_60_RPMLimit2_C",
					"VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C","VolEff_Eload_61_80_RPMLimit1_C",
					"VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C","VolEff_Eload_61_80_RPMLimit5_C",
					"VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C","VolEff_Eload_81_90_RPMLimit4_C",
					"VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C","VolEff_Eload_91_94_RPMLimit3_C",
					"VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C","VolEff_Eload_95_98_RPMLimit2_C",
					"VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C","VolEff_Eload_99_100_RPMLimit1_C",
					"VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C","VolEff_Eload_99_100_RPMLimit5_C",
					"Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C"=>array(20,'*'),"Accl_Eload_C",
					"Decl_Val_C"=>array(20,'*'),"Eload_Def_C","Eload_Max_C","Max_Torque_C"=>array(5,'/'),"ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C"=>array(50,'/'),"Osc_RPM_Lmt2_C"=>array(50,'/'),"Osc_RPM_Lmt3_C"=>array(50,'/'),
					"Osc_RPM_Lmt4_C"=>array(50,'/'),"CRP_RPM_PkLmt_C"=>array(50,'/'),"CRP_Type_C"=>array(1000,'/'),"CRP_MaxLmt3_C"=>array(500,'/'),
					"CRP_MaxLmt1_C"=>array(500,'/'),"Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C"=>array(50,'/'),"RPM_Idle_Ld1_C"=>array(50,'/'),"RPM_Idle_Ld2_C"=>array(50,'/'),"MAP_RPMOffset_PkLmt_C",
					"Max_Air_Trq_RPM_C"=>array(50,'/'),"MAP_Air_RPM_LwrLmt_C"=>array(50,'/'),"MAP_Air_RPM_PkLmt_C"=>array(50,'/'),"MAP_Air_EloadOffset_PkLmt_C",
					"MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C","Time_Upld_Frq_C"=>array(3,'/'),"MAP_Type_C",
					"IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_20_40_C"=>array(100,'*'),
					"Fact_Fuel_Run_ELoad_40_60_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_60_80_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_80_100_C"=>array(100,'*'),
					"Fact_Fuel_Idle_ELoad1_C"=>array(100,'*'),"Fact_Fuel_Idle_ELoad2_C"=>array(100,'*'),"Ovr_Run_FuelFact_C"=>array(100,'*'),
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C"=>array(10000,'*'),"gr_6_low_C"=>array(10000,'*'),"gr_6_high_C"=>array(10000,'*'),
					"Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C","Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C",
					"Gr_RPM_Col6_C","Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C",
					"Thr_RPM_Col4_C","Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C",
					"Thr_Pos_Row2_C","Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C","Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C"=>array(20,'*'),"Spd_Chng_Neg_Row4_C"=>array(20,'*'),
					"Spd_Chng_Neg_Row3_C"=>array(20,'*'),"Spd_Chng_Neg_Row2_C"=>array(20,'*'),"Spd_Chng_Neg_Row1_C"=>array(20,'*'),
					"Spd_Chng_Row0_C"=>array(20,'*'),"Spd_Chng_Pos_Row1_C"=>array(20,'*'),"Spd_Chng_Pos_Row2_C"=>array(20,'*'),
					"Spd_Chng_Pos_Row3_C"=>array(20,'*'),"Spd_Chng_Pos_Row4_C"=>array(20,'*'),"Spd_Chng_Pos_Row5_C"=>array(20,'*'),"Ovr_Spd_Lmt_C",
					"Brake_EngSt3_Acc_C"=>array(20,'*'),"Brake_EngSt4_Acc_C"=>array(20,'*'),"Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C","Reserved9","Reserved10",
					"Reserved11","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$dat        = array('Yes'=>'1', 'No' => '0', 'Direct' => '1', 'Indirect' => '0', 'Absolute'=>'1', 'Differential'=>'0');
				//Eng_type
				$arrayData['success'] = TRUE;
				$arrayDataM = array();
				array_push($arrayDataM,$vType);

				$data1   = Fetch::getCaliPramVal($deviceID);

				$dataArr = $data1['calval'];
				$dataArr=json_decode($dataArr, TRUE);
				// print_r();
				$count = count($list);
				for($i=0; $i<count($list); $i++)
				{

					$val   = trim($list[$i]);
					// print_r($val);die;
					$data  = $dataArr['Eng_eff_C'];
					$data  = (isset($dat[trim($data)])) ? $dat[trim($data)] : $data ;
					$data  = ($data == "") ? 0 : $data ;

					if(isset($listVal[$list[$i]]) && is_array($listVal[$list[$i]]) )
					{
						$calC = $listVal[$list[$i]];
						if($calC[1] == "*")
						{
							$data = $data * $calC[0];
						}
						elseif($calC[1] == "/")
						{
							$data = round($data / $calC[0],0);
						}
					}

					// Encyption Logic for Config data
					if(($i == 82) || ($i == 83))
					{
						if($i == 82)
						{
							$data = 32; // Value 32 says the array is encrypted
						}
						elseif($i == 83)
						{
							$data = $encryptKey;
						}
					}
					else
					{
						if(($data == 0))// || ($data >= 255))
						{
							// Dont Encrypt if the value is Zero
							$data = 0;
						}
						else
						{
							$data = (($data + 26) ^ $encryptKey);
							// $data = $data;
						}
					}
					
					array_push($arrayDataM,$data);
				}
				$arrayData['data'] = array_values($arrayDataM);
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1001";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}

         	}
         	else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData,JSON_NUMERIC_CHECK);	
     	}
     	
     	public function get_device_config_w2(Request $request)
     	{
 		if(!empty($request->data))
         	{
         		// Valid Bearer Key Found.
         		$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];
			// print_r($obj);die;
			// Get Device Data
			$devInfo = Fetch::getdevice($deviceID);
			// print_r($devInfo);die;
			$did     = $devInfo[0]->id;
			
			// Get Vehicle details
			$vehi  = Fetch::getVehicleById($did);
			$vType = $vehi->fuel_tpe;
			$encryptKey = rand(25, 300);
			if(!empty($devInfo))
			{
				$yVal = 0;
				$list = array("Eng_eff_C","Eng_Disp_C","TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C","Eff_RPM_Limit2_C","Eff_RPM_Limit3_C","Eff_RPM_Limit4_C","Eff_RPM_Limit5_C",
					"Eqratio_Eload_1_10_RPMLimit1_C","Eqratio_Eload_1_10_RPMLimit2_C","Eqratio_Eload_1_10_RPMLimit3_C","Eqratio_Eload_1_10_RPMLimit4_C",
					"Eqratio_Eload_1_10_RPMLimit5_C","Eqratio_Eload_11_20_RPMLimit1_C","Eqratio_Eload_11_20_RPMLimit2_C","Eqratio_Eload_11_20_RPMLimit3_C",
					"Eqratio_Eload_11_20_RPMLimit4_C","Eqratio_Eload_11_20_RPMLimit5_C","Eqratio_Eload_21_30_RPMLimit1_C","Eqratio_Eload_21_30_RPMLimit2_C",
					"Eqratio_Eload_21_30_RPMLimit3_C","Eqratio_Eload_21_30_RPMLimit4_C","Eqratio_Eload_21_30_RPMLimit5_C","Eqratio_Eload_31_40_RPMLimit1_C",
					"Eqratio_Eload_31_40_RPMLimit2_C","Eqratio_Eload_31_40_RPMLimit3_C","Eqratio_Eload_31_40_RPMLimit4_C","Eqratio_Eload_31_40_RPMLimit5_C",
					"Eqratio_Eload_41_50_RPMLimit1_C","Eqratio_Eload_41_50_RPMLimit2_C","Eqratio_Eload_41_50_RPMLimit3_C","Eqratio_Eload_41_50_RPMLimit4_C",
					"Eqratio_Eload_41_50_RPMLimit5_C","Eqratio_Eload_51_60_RPMLimit1_C","Eqratio_Eload_51_60_RPMLimit2_C","Eqratio_Eload_51_60_RPMLimit3_C",
					"Eqratio_Eload_51_60_RPMLimit4_C","Eqratio_Eload_51_60_RPMLimit5_C","Eqratio_Eload_61_80_RPMLimit1_C","Eqratio_Eload_61_80_RPMLimit2_C",
					"Eqratio_Eload_61_80_RPMLimit3_C","Eqratio_Eload_61_80_RPMLimit4_C","Eqratio_Eload_61_80_RPMLimit5_C","Eqratio_Eload_81_90_RPMLimit1_C",
					"Eqratio_Eload_81_90_RPMLimit2_C","Eqratio_Eload_81_90_RPMLimit3_C","Eqratio_Eload_81_90_RPMLimit4_C","Eqratio_Eload_81_90_RPMLimit5_C",
					"Eqratio_Eload_91_94_RPMLimit1_C","Eqratio_Eload_91_94_RPMLimit2_C","Eqratio_Eload_91_94_RPMLimit3_C","Eqratio_Eload_91_94_RPMLimit4_C",
					"Eqratio_Eload_91_94_RPMLimit5_C","Eqratio_Eload_95_98_RPMLimit1_C","Eqratio_Eload_95_98_RPMLimit2_C","Eqratio_Eload_95_98_RPMLimit3_C",
					"Eqratio_Eload_95_98_RPMLimit4_C","Eqratio_Eload_95_98_RPMLimit5_C","Eqratio_Eload_99_100_RPMLimit1_C","Eqratio_Eload_99_100_RPMLimit2_C",
					"Eqratio_Eload_99_100_RPMLimit3_C","Eqratio_Eload_99_100_RPMLimit4_C","Eqratio_Eload_99_100_RPMLimit5_C","Ovr_Run_EqRatio_C",
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C",
					"VolEff_Eload_31_40_RPMLimit4_C","VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C",
					"VolEff_Eload_41_50_RPMLimit3_C","VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C",
					"VolEff_Eload_51_60_RPMLimit2_C","VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C",
					"VolEff_Eload_61_80_RPMLimit1_C","VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C",
					"VolEff_Eload_61_80_RPMLimit5_C","VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C",
					"VolEff_Eload_81_90_RPMLimit4_C","VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C",
					"VolEff_Eload_91_94_RPMLimit3_C","VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C",
					"VolEff_Eload_95_98_RPMLimit2_C","VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C",
					"VolEff_Eload_99_100_RPMLimit1_C","VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C",
					"VolEff_Eload_99_100_RPMLimit5_C","Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C",
					"Accl_Eload_C","Decl_Val_C","Eload_Def_C","Eload_Max_C","Max_Torque_C","ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C","Osc_RPM_Lmt2_C","Osc_RPM_Lmt3_C","Osc_RPM_Lmt4_C","CRP_RPM_PkLmt_C","CRP_Type_C",
					"CRP_MaxLmt3_C","CRP_MaxLmt1_C","Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C","RPM_Idle_Ld1_C","RPM_Idle_Ld2_C","MAP_RPMOffset_PkLmt_C","Max_Air_Trq_RPM_C","MAP_Air_RPM_LwrLmt_C",
					"MAP_Air_RPM_PkLmt_C","MAP_Air_EloadOffset_PkLmt_C","MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C",
					"Time_Upld_Frq_C","MAP_Type_C","IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C","Fact_Fuel_Run_ELoad_20_40_C","Fact_Fuel_Run_ELoad_40_60_C",
					"Fact_Fuel_Run_ELoad_60_80_C","Fact_Fuel_Run_ELoad_80_100_C","Fact_Fuel_Idle_ELoad1_C","Fact_Fuel_Idle_ELoad2_C","Ovr_Run_FuelFact_C",
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C","gr_6_low_C","gr_6_high_C","Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C",
					"Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C","Gr_RPM_Col6_C",
					"Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C","Thr_RPM_Col4_C",
					"Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C","Thr_Pos_Row2_C",
					"Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C",
					"Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C","Spd_Chng_Neg_Row4_C","Spd_Chng_Neg_Row3_C","Spd_Chng_Neg_Row2_C",
					"Spd_Chng_Neg_Row1_C","Spd_Chng_Row0_C","Spd_Chng_Pos_Row1_C","Spd_Chng_Pos_Row2_C","Spd_Chng_Pos_Row3_C","Spd_Chng_Pos_Row4_C",
					"Spd_Chng_Pos_Row5_C","Ovr_Spd_Lmt_C","Brake_EngSt3_Acc_C","Brake_EngSt4_Acc_C","Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C","Reserved9",
					"Reserved10","Reserved11","Reserved12","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$listVal = array("Eng_eff_C","Eng_Disp_C"=>array(100,'*'),"TC_Status_C","Injection_Type_C","Eff_RPM_Limit1_C"=>array(50,'/'),
					"Eff_RPM_Limit2_C"=>array(50,'/'),"Eff_RPM_Limit3_C"=>array(50,'/'),"Eff_RPM_Limit4_C"=>array(50,'/'),"Eff_RPM_Limit5_C"=>array(50,'/'),
					"Eqratio_Eload_1_10_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_1_10_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_1_10_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_11_20_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_11_20_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_21_30_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_21_30_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_31_40_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_31_40_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_41_50_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_41_50_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_51_60_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_51_60_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_61_80_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_61_80_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_81_90_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_81_90_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit2_C"=>array(200,'*'),
					"Eqratio_Eload_91_94_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_91_94_RPMLimit5_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit1_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit3_C"=>array(200,'*'),
					"Eqratio_Eload_95_98_RPMLimit4_C"=>array(200,'*'),"Eqratio_Eload_95_98_RPMLimit5_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit1_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit2_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit3_C"=>array(200,'*'),"Eqratio_Eload_99_100_RPMLimit4_C"=>array(200,'*'),
					"Eqratio_Eload_99_100_RPMLimit5_C"=>array(200,'*'),"Ovr_Run_EqRatio_C"=>array(100,'*'),
					"VolEff_Eload_1_10_RPMLimit1_C","VolEff_Eload_1_10_RPMLimit2_C","VolEff_Eload_1_10_RPMLimit3_C","VolEff_Eload_1_10_RPMLimit4_C",
					"VolEff_Eload_1_10_RPMLimit5_C","VolEff_Eload_11_20_RPMLimit1_C","VolEff_Eload_11_20_RPMLimit2_C","VolEff_Eload_11_20_RPMLimit3_C",
					"VolEff_Eload_11_20_RPMLimit4_C","VolEff_Eload_11_20_RPMLimit5_C","VolEff_Eload_21_30_RPMLimit1_C","VolEff_Eload_21_30_RPMLimit2_C",
					"VolEff_Eload_21_30_RPMLimit3_C","VolEff_Eload_21_30_RPMLimit4_C","VolEff_Eload_21_30_RPMLimit5_C","VolEff_Eload_31_40_RPMLimit1_C",
					"VolEff_Eload_31_40_RPMLimit2_C","Config_Encrypt_Status","Config_Encrypt_Key","VolEff_Eload_31_40_RPMLimit3_C","VolEff_Eload_31_40_RPMLimit4_C",
					"VolEff_Eload_31_40_RPMLimit5_C","VolEff_Eload_41_50_RPMLimit1_C","VolEff_Eload_41_50_RPMLimit2_C","VolEff_Eload_41_50_RPMLimit3_C",
					"VolEff_Eload_41_50_RPMLimit4_C","VolEff_Eload_41_50_RPMLimit5_C","VolEff_Eload_51_60_RPMLimit1_C","VolEff_Eload_51_60_RPMLimit2_C",
					"VolEff_Eload_51_60_RPMLimit3_C","VolEff_Eload_51_60_RPMLimit4_C","VolEff_Eload_51_60_RPMLimit5_C","VolEff_Eload_61_80_RPMLimit1_C",
					"VolEff_Eload_61_80_RPMLimit2_C","VolEff_Eload_61_80_RPMLimit3_C","VolEff_Eload_61_80_RPMLimit4_C","VolEff_Eload_61_80_RPMLimit5_C",
					"VolEff_Eload_81_90_RPMLimit1_C","VolEff_Eload_81_90_RPMLimit2_C","VolEff_Eload_81_90_RPMLimit3_C","VolEff_Eload_81_90_RPMLimit4_C",
					"VolEff_Eload_81_90_RPMLimit5_C","VolEff_Eload_91_94_RPMLimit1_C","VolEff_Eload_91_94_RPMLimit2_C","VolEff_Eload_91_94_RPMLimit3_C",
					"VolEff_Eload_91_94_RPMLimit4_C","VolEff_Eload_91_94_RPMLimit5_C","VolEff_Eload_95_98_RPMLimit1_C","VolEff_Eload_95_98_RPMLimit2_C",
					"VolEff_Eload_95_98_RPMLimit3_C","VolEff_Eload_95_98_RPMLimit4_C","VolEff_Eload_95_98_RPMLimit5_C","VolEff_Eload_99_100_RPMLimit1_C",
					"VolEff_Eload_99_100_RPMLimit2_C","VolEff_Eload_99_100_RPMLimit3_C","VolEff_Eload_99_100_RPMLimit4_C","VolEff_Eload_99_100_RPMLimit5_C",
					"Ovr_Run_VolEff_C","MAP_Valid_C","MAF_Valid_C","Baro_Press_City_C","MAP_Abs_PkLmt3_C","Accl_Val_C"=>array(20,'*'),"Accl_Eload_C",
					"Decl_Val_C"=>array(20,'*'),"Eload_Def_C","Eload_Max_C","Max_Torque_C"=>array(5,'/'),"ELoad_Idle_Warm_C","ELoad_Idle_Ld1_C","ELoad_Idle_Ld2_C",
					"Osc_EloadOffset_Lmt_C","Cool_Lmt_C","Osc_RPM_Lmt1_C"=>array(50,'/'),"Osc_RPM_Lmt2_C"=>array(50,'/'),"Osc_RPM_Lmt3_C"=>array(50,'/'),
					"Osc_RPM_Lmt4_C"=>array(50,'/'),"CRP_RPM_PkLmt_C"=>array(50,'/'),"CRP_Type_C"=>array(1000,'/'),"CRP_MaxLmt3_C"=>array(500,'/'),
					"CRP_MaxLmt1_C"=>array(500,'/'),"Base_OvrHt_Cool_Lmt_C","Cool_Temp_Lmt1_C","Cool_Temp_Lmt2_C","Cool_Temp_Lmt3_C","Cool_Temp_Lmt4_C",
					"RPM_Idle_Warm_C"=>array(50,'/'),"RPM_Idle_Ld1_C"=>array(50,'/'),"RPM_Idle_Ld2_C"=>array(50,'/'),"MAP_RPMOffset_PkLmt_C",
					"Max_Air_Trq_RPM_C"=>array(50,'/'),"MAP_Air_RPM_LwrLmt_C"=>array(50,'/'),"MAP_Air_RPM_PkLmt_C"=>array(50,'/'),"MAP_Air_EloadOffset_PkLmt_C",
					"MAP_Air_IdleLmt3_C","MAP_Air_IdleLmt1_C","MAP_Air_PkLmt3_C","MAP_Air_PkLmt1_C","Time_Upld_Frq_C"=>array(3,'/'),"MAP_Type_C",
					"IC_Status_C","Fact_Fuel_Run_ELoad_0_20_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_20_40_C"=>array(100,'*'),
					"Fact_Fuel_Run_ELoad_40_60_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_60_80_C"=>array(100,'*'),"Fact_Fuel_Run_ELoad_80_100_C"=>array(100,'*'),
					"Fact_Fuel_Idle_ELoad1_C"=>array(100,'*'),"Fact_Fuel_Idle_ELoad2_C"=>array(100,'*'),"Ovr_Run_FuelFact_C"=>array(100,'*'),
					"Ovr_Run_FuelFact_Eload_C","gr_5_high_C"=>array(10000,'*'),"gr_6_low_C"=>array(10000,'*'),"gr_6_high_C"=>array(10000,'*'),
					"Eng_Rev_Lmt_C","Cool_Temp_AvgLmt_C","Gr_RPM_Col0_C","Gr_RPM_Col1_C","Gr_RPM_Col2_C","Gr_RPM_Col3_C","Gr_RPM_Col4_C","Gr_RPM_Col5_C",
					"Gr_RPM_Col6_C","Gr_RPM_Col7_C","Gr_RPM_Col8_C","Gr_RPM_Col9_C","Thr_RPM_Col0_C","Thr_RPM_Col1_C","Thr_RPM_Col2_C","Thr_RPM_Col3_C",
					"Thr_RPM_Col4_C","Thr_RPM_Col5_C","Thr_RPM_Col6_C","Thr_RPM_Col7_C","Thr_RPM_Col8_C","Thr_RPM_Col9_C","Thr_Pos_Row0_C","Thr_Pos_Row1_C",
					"Thr_Pos_Row2_C","Thr_Pos_Row3_C","Thr_Pos_Row4_C","Thr_Pos_Row5_C","Thr_Pos_Row6_C","Thr_Pos_Row7_C","Thr_Pos_Row8_C","Thr_Pos_Row9_C",
					"Veh_Spd_Col0_C","Veh_Spd_Col1_C","Veh_Spd_Col2_C","Veh_Spd_Col3_C","Veh_Spd_Col4_C","Veh_Spd_Col5_C","Veh_Spd_Col6_C","Veh_Spd_Col7_C","Veh_Spd_Col8_C","Veh_Spd_Col9_C","Spd_Chng_Neg_Row5_C"=>array(20,'*'),"Spd_Chng_Neg_Row4_C"=>array(20,'*'),
					"Spd_Chng_Neg_Row3_C"=>array(20,'*'),"Spd_Chng_Neg_Row2_C"=>array(20,'*'),"Spd_Chng_Neg_Row1_C"=>array(20,'*'),
					"Spd_Chng_Row0_C"=>array(20,'*'),"Spd_Chng_Pos_Row1_C"=>array(20,'*'),"Spd_Chng_Pos_Row2_C"=>array(20,'*'),
					"Spd_Chng_Pos_Row3_C"=>array(20,'*'),"Spd_Chng_Pos_Row4_C"=>array(20,'*'),"Spd_Chng_Pos_Row5_C"=>array(20,'*'),"Ovr_Spd_Lmt_C",
					"Brake_EngSt3_Acc_C"=>array(20,'*'),"Brake_EngSt4_Acc_C"=>array(20,'*'),"Hlf_Clutch_Thr_C","Gr_Cnt_Debounce_C","Veh_Spd_Avl_C","Trq_RPM_RSE_C","Trq_Eload_RSE_C","Accln_Debounce_C","Reserved9","Reserved10",
					"Reserved11","Reserved13","Reserved14","Reserved15","Reserved16","Reserved17");
				
				$dat        = array('Yes'=>'1', 'No' => '0', 'Direct' => '1', 'Indirect' => '0', 'Absolute'=>'1', 'Differential'=>'0');
				//Eng_type
				$arrayData['success'] = TRUE;
				$arrayDataM = array();
				array_push($arrayDataM,$vType);

				$data1   = Fetch::getCaliPramVal($deviceID);

				$dataArr = $data1['calval'];
				$dataArr=json_decode($dataArr, TRUE);
				// print_r();
				$count = count($list);
				for($i=0; $i<count($list); $i++)
				{

					$val   = trim($list[$i]);
					// print_r($val);die;
					$data  = $dataArr['Eng_eff_C'];
					$data  = (isset($dat[trim($data)])) ? $dat[trim($data)] : $data ;
					$data  = ($data == "") ? 0 : $data ;

					if(isset($listVal[$list[$i]]) && is_array($listVal[$list[$i]]) )
					{
						$calC = $listVal[$list[$i]];
						if($calC[1] == "*")
						{
							$data = $data * $calC[0];
						}
						elseif($calC[1] == "/")
						{
							$data = round($data / $calC[0],0);
						}
					}

					// Encyption Logic for Config data
					if(($i == 82) || ($i == 83))
					{
						if($i == 82)
						{
							$data = 32; // Value 32 says the array is encrypted
						}
						elseif($i == 83)
						{
							$data = $encryptKey;
						}
					}
					else
					{
						if(($data == 0))// || ($data >= 255))
						{
							// Dont Encrypt if the value is Zero
							$data = 0;
						}
						else
						{
							$data = (($data + 26) ^ $encryptKey);
							// $data = $data;
						}
					}
					
					array_push($arrayDataM,$data);
				}
				$arrayData['data'] = array_values($arrayDataM);
			}
			else
			{
				$arrayData['success']      = FALSE;
				$arrayData['errorCode']    = "1001";
				$arrayData['errorMessage'] = "Invalid Device ID!";
			}
         	}
         	else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1001";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
		echo json_encode($arrayData,JSON_NUMERIC_CHECK);	
     	}
}