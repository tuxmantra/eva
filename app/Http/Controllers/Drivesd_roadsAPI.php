<?php
    
    namespace App\Http\Controllers;
    
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Mail;
    use Helper;
    use App\Model\Common;
    use App\Model\Fetch;
    
    class Drivesd extends Controller
    {
        public function index(){
            $user                = session('userdata');
            $data['user_type']   = $user['utype'];
            $deviceList          = Fetch::getDeviceListByUserType($user['uid'],$user['ucategory']);
            if(!empty(session('device_id')))
            {
                $did           = session('device_id');
            }else
            {       
                 $did          = $deviceList[0]->device_id;
            }
            $date                = date('Y-m-d');
            $driveDates          = Fetch::getDriveDatesByDeviceIdEvent($did);
            $driveLatLong        = Fetch::getDriveLatLongByIdEvent($did,$date);
            
            $string              = '';
            $start               = "";
            if(!empty($driveLatLong)){
                $i=1;
                foreach($driveLatLong as $key => $list){
                    
                    if($key == 0){
                        $start.=  "{lat: $list->lat, lng: $list->lon}";
                    }
                    
                    $string.=  "{lat: $list->lat, lng: $list->lon},";
                    $i++;
                }
            }
            
            $data_array          = "";
            foreach($driveDates as $sdt){
                $data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',";
            }

            $data = array(
              'title'            => 'Drive',
              'device_list'      => $deviceList,
              'user_category'    => $user['ucategory'],
              'did'              => $did,
              'drive_dates'      => rtrim($data_array,','),
              'drive_latlon'     => rtrim($string,','),
              'start_point'      => $start
            );
            $data['drivesd']  = '1';
            $data['menu']     = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
            return view('drivesd/index')->with($data);
        }
        
          public function getSdDriveLoad(Request $request){
           
           
            $did    = $request->post('did');
            session(['device_id' => $did]);
            $date   = $request->post('date');
            $date   = date('Y-m-d',strtotime($date));
            $driveLatLong        = Fetch::getDriveLatLongByIdEvent($did,$date);
            
            $driveDates          = Fetch::getDriveDatesByDeviceIdEvent($did);

           
            $gapikey      = Fetch::getgapikey(); //google api key
            $appicount    = Fetch::updategapicount($gapikey[0]->api_key,$gapikey[0]->count+1); // update google api key count

            $data_array   = "";
            $string       = '';
            $start        = "";
            $start_lat    = '';
            $start_lon    = '';
            $end_lat      = '';
            $end_lon      = '';
            $i            = 0;
            foreach($driveDates as $sdt){
                $data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',";
            }

            if(!empty($driveLatLong)){

                foreach($driveLatLong as $key => $list){

                   
                    
                    if($key == 0){
                        $start.=  "{lat: $list->lat, lng: $list->lon}";
                        $start_lat = $list->lat;
                        $start_lon = $list->lon;
                    }
                    if($key == count($driveLatLong)-1){
                        $end_lat = $list->lat;
                        $end_lon = $list->lon;
                    }
                   
                    $string=  "{lat: ".$list->lat.", lng: ".$list->lon."},";
                     $string2[]=  array(
                       'lat'=>  $list->lat,
                      'lng'=> $list->lon,
                       
                   );
                      $dd= date("Y-m-d H:i:s", substr($list->time_s, 0, 10));
                      // gmdate('r', $list->time_s);
                     $string1[]=
                array('title'=>  'Device ID '.$list->device_id,
                       'lat'=>  $list->lat,
                      'lng'=> $list->lon,
                       'description'=>   'Device ID '.$list->device_id.' Date-'.$dd
                   );
               
                }
                
                 
 
               $startEnd =  "['$did Start @ $date', $start_lat, $start_lon, 0],['$did End @ $date', $end_lat, $end_lon, 1]";
                
                   $data   = array(
                    'did'              => $did,
                    'drive_latlon'     => $string1,
                    'drive_latlon1'     => $string2,
                    'start_point'      => $start,
                    'start_end'        => $startEnd,
                    'drive_dates'      => rtrim($data_array,','),
                    'gapikey'          => $gapikey[0]->api_key
                   );
               
                
            }else{
                $string    = "{lat: '', lng: ''},";
                $string2[] = array(
                    'lat'=>'',
                    'lng'=>'',
                );
                $string1[] = 
                 array('title'=> 'Device ID ',
                        'lat'=> '20.9745',
                        'lng'=> '77.5968',

                        'description'=> 'No data Available !'
                    );
            $start = "{lat: '', lng: ''}";
            $data  = array(
               
                    'did'              => $did,
                    'drive_latlon'     => $string1,
                    'drive_latlon1'    => $string2,
                    'start_point'      => $start,
                    'start_end'        => '',
                    'start_end'        => '',
                    'drive_dates'      => rtrim($data_array,','),
                    'gapikey'          => $gapikey[0]->api_key
                  
            );
            }
            $string = rtrim($string,',');
        
        $result = view('drivesd/load')->with($data);
        return $result;
    }

    
    }

