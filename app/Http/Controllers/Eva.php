<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Helper;
use App\Model\Common;
use App\Model\Fetch;

class Eva extends Controller
{
    public function index(request $request)
    {
        $user                = session('userdata');
        $data['user_type']   = $user['utype'];
        $deviceList          = Fetch::getDeviceListByUserType_obd($user['uid'],$user['ucategory']);
        $date                = $request->post('date');
        if(empty($date))
        {
            $date            = date('m/d/Y');
        }
        $date                = date('m/d/Y',strtotime($date));
        $vin_no              = '';
        $vehicle             = '';
        if(!empty($deviceList))
        {
            $did            = $request->post('devID');
            // echo $did;die;
            if(!empty($request->post('devID')))
            {
                $did            = $request->post('devID');
                $date           = $request->post('date');
                $date           = date('m/d/Y',strtotime($date));
                $vin_no         = $request->post('vin_no');
                $vehicle        = $request->post('vehicle');
                // echo $date;die;
            }
            else
            {
                $did            = $deviceList[0]->device_id;
            }
            if(!empty(session('device_id')) && empty($request->post('devID')))
            {
                $data['did']     = $did = session('device_id');
                $date            = session('date');
            }
            else
            {       
                $data['did']     = $did;
                session(['date' => $date]);
            }
            // echo $did.$date.$vin_no;die;
            // Value Part data fetch
            $driveDates          = Fetch::getDriveDatesByDeviceId($did);
            $data_array = "";
            foreach($driveDates as $sdt){
               $data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',"; 
            }

        }
        else
        {
            $did='';
            $data_array = '';
        }
    	$valuePartList       = Fetch::getValuePartListobd(); // selParam 
        $data['title']         = 'EVA';
        $data['device_list']   = $deviceList;
        $data['user_category'] = $user['ucategory'];       
        // if(!empty(session('device_id')))
        // {
        //     $data['did']           = session('device_id');
        // }else
        // {       
        //     $data['did']           = $did;
        // }
        $data['drive_dates']   = rtrim($data_array,',');
        $data['valuePartList'] = $valuePartList;
        $data['today']         = $date;
        $data['eva_open']      = '1';
        $data['obd_open']      = '1';
        $vinlist               = '';
        $vehiclelist           = Fetch::getvehiclelistbydevid(date('Y-n-d',strtotime($date)),$did);
        if(count($vehiclelist) == 1)
        {
            $vinlist           = Fetch::getvinlistbydevid(date('Y-n-d',strtotime($date)),$did,$vehiclelist[0]->vehicle);
        }
        else
        {
            $vinlist           = Fetch::getvinlistbydevid(date('Y-n-d',strtotime($date)),$did,$vehicle);
        }
        

        $testlist = Fetch::getTestlistbydevid(date('Y-m-d',strtotime($date)),$did,$vehicle,$vin_no);
        // print_r($testlist);die;
        $data['testlist']      = $testlist;
        $data['vinlist']       = $vinlist;
        $data['vehiclelist']   = $vehiclelist;
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    	return view('eva/index')->with($data);
    }

    public function getportdata(request $request)
    {
        $did            = $request->post('did');
        $valuePartList1 = Fetch::getValuePartList1($did);
        $string         = '';
        $select         = '';
        $valuePartList  = Fetch::getValuePartList(); // selParam ;
        foreach ($valuePartList as $vpl)
        {
            if($vpl->id == '4')
            {
                $select = 'selected';
            }
            else
            { 
                $select = ''; 
            }
            $string.='<option value="'.$vpl->id.'">'.$vpl->title.'</option>';
        }                       
                        
           
        if(!empty($valuePartList1))
        {
           if($valuePartList1[0]->P0name !=''){
            $string.='<option value="P0">'.$valuePartList1[0]->P0name.'</option>';
           }
           if($valuePartList1[0]->P1name !=''){
            $string.='<option value="P1">'.$valuePartList1[0]->P1name.'</option>';
           }
           if($valuePartList1[0]->P2name !=''){
            $string.='<option value="P2">'.$valuePartList1[0]->P2name.'</option>';
           }
            if($valuePartList1[0]->P3name !=''){
            $string.='<option value="P3">'.$valuePartList1[0]->P3name.'</option>';
           }
           if($valuePartList1[0]->P4name !=''){
            $string.='<option value="P4">'.$valuePartList1[0]->P4name.'</option>';
           }
           if($valuePartList1[0]->P5name !=''){
            $string.='<option value="P5">'.$valuePartList1[0]->P5name.'</option>';
           }
           if($valuePartList1[0]->P6name !=''){
            $string.='<option value="P6">'.$valuePartList1[0]->P6name.'</option>';
           }
           if($valuePartList1[0]->P7name !=''){
            $string.='<option value="P7">'.$valuePartList1[0]->P7name.'</option>';
           }
        }
        print_r($string);

    }

    public function getUservehicleDetails(request $request)
    {
        $did      = $request->post('did');
        $date     = $request->post('date');
        $vehicle1  = $request->post('vehicle');
        $vin_no   = $request->post('vin_no');
        $date     = date('Y-m-d',strtotime($date));
        $username = Fetch::getUserDeviceEmail($did);
        $id       = Fetch::getIdByDeviceId($did);
        $id       = $id[0]->id;
        $vehicle  = Fetch::getVehicleById($id);
        if($vehicle->fuel_tpe == 1)
        {
            $fuelType = 'Petrol';
        }
        else
        {
            $fuelType = 'Diesel';
        }
        // print_r($vehicle);
        $vehicle_sett = Fetch::getVehicleSettingData();
        $manuArr      = explode(';', $vehicle_sett->manufacturer);
        $engCapArr    = explode(';', $vehicle_sett->engine_capacity);

        $vehicle_info = Fetch::getVehicleInfoDataById($vehicle->manufact);
        $modelArr     = explode(';', $vehicle_info[0]->model);
        $varArr       = explode(';', $vehicle_info[0]->var);
        // print_r($modelArr);
        // print_r($varArr);
        $resulttext   = "";
        $datetag      = "";
        $vehiclelist  = Fetch::getTestResultByVin($date,$did,$vehicle1,$vin_no);
        // print_r($vehiclelist);die;
        if(!empty($vehiclelist))
        {
            if($vehiclelist[0]->test_result == 1)
            {
              $resulttext = $date." / Passed";
              $datetag    = "Test Date / Result";
            }
            elseif($vehiclelist[0]->test_result == 0)
            {
              $resulttext = $date." / Failed";
              $datetag    = "Test Date / Result";
            }
            else
            {
              $resulttext = $date;
              $datetag    = "Test Date";
            }
        }
        else
        {
            $resulttext = $date;
            $datetag    = "Test Date";
        }
        $mfd          = $manuArr[$vehicle->manufact]; 
        $mfd          = explode('_', $mfd);
        $string ='<div class="col-md-4">
                        <p style="color: black;margin-bottom: 0rem;" id="uname"><b>User Name</b> : '.$username[0]->uname.'</p>
                        <p style="color: black;margin-bottom: 0rem;" id="title"><b>DeviceID</b> : '.$did.'</p>
                      </div>
                      <div class="col-md-4">
                      </div>
                      <div class="col-md-4" >
                        <p style="color: black;margin-bottom: 0rem;" id="uname"><b>Vehicle Name</b> : '.$modelArr[$vehicle->model].' '.$varArr[$vehicle->varient].'</p>
                        <p style="color: black;margin-bottom: 0rem;" id="testdate"><b>'.$datetag.'</b> : '.$resulttext.'</p>
                      </div>';

                      echo $string;
    }
    public function getTestType(Request $request)
    {
        $dev = $request->post('did');
        $dt  = $request->post('date');
        $vehicle     = $request->post('vehicle');
        $vin_no      = $request->post('vin_no');
        $vehiclelist = Fetch::getTestlistbydevid(date('Y-m-d',strtotime($dt)),$dev,$vehicle,$vin_no);
        $list='<option class="dropdown-item" value="" selected>All Test</option>';
        if(!empty($vehiclelist))
        {
            foreach ($vehiclelist as $key => $vahicle) 
            {
                if(!empty($vahicle->F43))
                {   
                    if($vahicle->F43 == 3)
                    {
                        $list.='<option class="dropdown-item"  value="'.$vahicle->F43.'">FC Test</option>';
                    }
                    elseif($vahicle->F43 == 4)
                    {
                         $list.='<option class="dropdown-item"  value="'.$vahicle->F43.'">Additional Test</option>';
                    }
                }
            }
        }
        echo $list;
    }
    public function getVinNo(Request $request)
    {
        $dev = $request->post('did');
        $dt  = $request->post('date');
        $vehicle     = $request->post('vehicle');
        $vehiclelist = Fetch::getvinlistbydevid(date('Y-m-d',strtotime($dt)),$dev,$vehicle);
        $list='<option class="dropdown-item" value="" selected>All VIN NO</option>';
        if(!empty($vehiclelist))
        {
            foreach ($vehiclelist as $key => $vahicle) 
            {
                if(!empty($vahicle->vin_no))
                {   
                    
                    $list.='<option class="dropdown-item"  value="'.$vahicle->vin_no.'">'.$vahicle->vin_no.'</option>';
                }
            }
        }
        echo $list;
    }
    public function getVehicle(Request $request)
    {
        $dev = $request->post('did');
        $dt  = $request->post('date');
        $vehiclelist          = Fetch::getvehiclelistbydevid(date('Y-m-d',strtotime($dt)),$dev);
        $list='<option class="dropdown-item" value="" selected>All Vehicles</option>';
        if(!empty($vehiclelist))
        {
            foreach ($vehiclelist as $key => $vahicle) 
            {
                if(!empty($vahicle->vehicle))
                {   
                    $selected ='';
                    if(count($vehiclelist) == 1)
                    {
                        $selected = 'selected';
                    }
                    $list.='<option class="dropdown-item" '.$selected.' value="'.$vahicle->vehicle.'">'.$vahicle->vehicle.'</option>';
                }
            }
        }
        echo $list;
    }
    public function eva_can()
    {
        $user              = session('userdata');
        $data['user_type'] = $user['utype'];
        $deviceList        = Fetch::getDeviceListByUserType_can($user['uid'],$user['ucategory']);
        $date                = date('m/d/Y');
        if(!empty($deviceList))
        {
            $did                 = $deviceList[0]->device_id;
            if(!empty(session('device_id')))
            {
                $data['did']     = $did = session('device_id');
                $date            = session('date');
            }else
            {       
                $data['did']     = $did;
                session(['date' => $date]);
            }
            // Value Part data fetch
            $driveDates          = Fetch::getDriveDatesByDeviceId($did);
            $data_array = "";
            foreach($driveDates as $sdt){
               $data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',"; 
            }

        }else
        {
        	$did='';
        	$data_array = '';
        }
        // Value Part data fetch
        $vehiclelist           = Fetch::getvehiclelistbydevid(date('Y-n-d',strtotime($date)),$did);
        $data['vehiclelist']      = $vehiclelist;
        $valuePartList     = Fetch::getValuePartList(); // selParam 
    	// $date                = date('Y-m-d');

        $data['title']         = 'EVA';
        $data['device_list']   = $deviceList;
        $data['user_category'] = $user['ucategory'];       
        $data['did']           = $did;
        $data['drive_dates']   = rtrim($data_array,',');
        $data['valuePartList'] = $valuePartList;
        $data['today']         = $date;
        $data['eva_open']      = '1';
        $data['can_open']      = '1';
        $data['menu']          = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('eva/candata')->with($data);
    }

    public function getEvaGraph(Request $request)
    {
        $user              = session('userdata');
        $data['user_type'] = $user['utype'];
        if(empty($user['utype'])){
            echo redirect('/login');
        }
        // Basic requirnment to load graph divice id and date
        $dev               = $request->post('did');
        if(session('device_id') == $dev)
        {
            $dt                = $request->post('date');
            session(['date' => $dt]);
        }
        else
        {
            $dt                = date('m/d/Y');
            session(['date' => $dt]);
            session(['device_id' => $dev]);
        }
        $data['date']         = $dt;
        $data['today']        = $dt;
        $dd_date              = $request->post('dd_date');

        // Drive dates load for the device
         $data_array          = "";
         if($dd_date)
         {  
            $driveDates          = Fetch::getDriveDatesByDeviceId($dev);
            foreach($driveDates as $sdt){
                $data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',"; 
            }
             session(['dd_date' => $data_array]);
        }
        
        // State - On Page Load 1, On datepicker or Device List Click load view 2
        $state = $request->post('state');
        // Select box values for red,blue,yellow,green
        $pma   = $request->post('g1');
        $pmb   = $request->post('g2');
        $pmc   = $request->post('g3');
        $vehicle   = $request->post('vehicle');
        $vin_no      = $request->post('vin_no');
        $test_type   = $request->post('test_type');
        // Dropdown A
        $d1    = $this->processGraphData($pma,$pmb,$pmc,$dt,$dev,$vehicle,$vin_no,$test_type);
        $datAA = (is_array($d1[0])) ? $d1[0] : array();
        $datA  = implode(",",$datAA);
        $datBB = (is_array($d1[1])) ? $d1[1] : array(); //$d2[0];
        $datB  = implode(",",$datBB);
        $datCC = (is_array($d1[2])) ? $d1[2] : array(); //$d2[0];
        $datC  = implode(",",$datCC);
        

        if($pmb=='P0' || $pmb=='P1' || $pmb=='P2' || $pmb=='P3' || $pmb=='P4' || $pmb=='P5' || $pmb=='P6' || $pmb=='P7')
        {
        	$colb = $pmb.'name';
         	$unit = $pmb.'unit';
        
	        $valpa             = Fetch::getValuePart($dev,$colb,$unit);
	        $paramValB         = $valpa[0];
	        $data['paramValB'] = $paramValB;
        }
        else
        {
            $paramValB         = $this->getPramVal($pmb);
            $data['paramValB'] = $paramValB;
        }
        
        if($pma=='P0' || $pma=='P1' || $pma=='P2' || $pma=='P3' || $pma=='P4' || $pma=='P5' || $pma=='P6' || $pma=='P7')
        {
        	$cola = $pma.'name';
        	$unit = $pma.'unit';
        
	        $valpa             = Fetch::getValuePart($dev,$cola,$unit);
	        $paramValA         = $valpa[0];
	        $data['paramValA'] = $paramValA;
        }
        else
        {
            $paramValA         = $this->getPramVal($pma);
            $data['paramValA'] = $paramValA;
        }
        
        if($pmc=='P0' || $pmc=='P1' || $pmc=='P2' || $pmc=='P3' || $pmc=='P4' || $pmc=='P5' || $pmc=='P6' || $pmc=='P7')
        {
	        $colc = $pmc.'name';
	        $unit = $pmc.'unit';
	        
	        $valpc             = Fetch::getValuePart($dev,$colc,$unit);
	        $paramValC         = $valpc[0];
	        $data['paramValC'] = $paramValC;
        }
        else
        {
            $paramValC         = $this->getPramVal($pmc);
            $data['paramValC'] = $paramValC;
        }
        
        $data['datAA'] = $datAA;
        $data['datA']  = $datA;
        $data['datB']  = $datB;
        $data['datC']  = $datC;
        $data['dt']    = $dt;
        
        $data['drive_dates'] = rtrim(session('dd_date'),',');
        if($state == 1){ 
           $graph = view('eva/evagraph2')->with($data);           
        }elseif($state == 2){
           $graph = view('eva/evagraph2')->with($data);
        }
        echo $graph;
    }

    public function processGraphData($val,$val1,$val2,$dt='',$dev='',$vehicle,$vin_no,$test_type)
    {
        $dt     = date('Y-m-d',strtotime($dt));
        $fName  = '';
        $fName1 = '';
        $fName2 = '';
        
        if($val != "")
        {   
            if($val=='P0' || $val=='P1' || $val=='P2' || $val=='P3' || $val=='P4' || $val=='P5' || $val=='P6' || $val=='P7'){
                $fName = $val;
            }else{
                $fName = "F$val";
            }
        }  
       	if($val1 != "")
        {   
            if($val1=='P0' || $val1=='P1' || $val1=='P2' || $val1=='P3' || $val1=='P4' || $val1=='P5' || $val1=='P6' || $val1=='P7'){
                $fName1 = $val1;
            }else{
                $fName1 = "F$val1";
            }  
        }           
      	if($val2 != "")
        {   
            if($val2=='P0' || $val2=='P1' || $val2=='P2' || $val2=='P3' || $val2=='P4' || $val2=='P5' || $val2=='P6' || $val2=='P7'){
                $fName2 = $val2;
            }else{
                $fName2 = "F$val2";
            }
        }  
        $drF = $dt . " 00:00:00"; // from date since morning
        $drT = $dt . " 23:59:59"; // to date uptill night
       
        $graphData = Fetch::getDeviceDataGraph($dev,$drF,$drT,$fName,$fName1,$fName2,$vehicle,$vin_no,$test_type);
        $valA      = array();
        $valB      = array();
        $valC      = array();
        $i         = 0;
        foreach($graphData as $data)
        {
           	if(!empty($fName))
           	{
            	if($data->{$fName} !=''){
	                $valMe    = "[".$data->time_s.",".$data->{$fName}."]";        
	                $valA[$i] = $valMe;
              	}                   
            }
            if(!empty($fName1))
            {
                if($data->{$fName1} !=''){
	                $valMe    = "[".$data->time_s.",".$data->{$fName1}."]";        
	                $valB[$i] = $valMe;
               	} 
            }
            if(!empty($fName2))
            {
                if($data->{$fName2} !=''){
	                $valMe    = "[".$data->time_s.",".$data->{$fName2}."]";        
	                $valC[$i] = $valMe;
            	}
            }
            $i++;
        }
        return array($valA,$valB,$valC);
 	}

    public function getPramVal($pdode)
    {
        if($pdode >= 1)
        {	
            $selParam = Fetch::getValuePartById($pdode);
            return $selParam[0];
        }
        else
        {
            if(empty($pdode)){
                $ret = array('title'=>'','unit'=>'');
            }else{
                $ret = array('title'=>$adFT[$pdode],'unit'=>$adFU[$pdode]);
            }
            return $ret;
        }
    }

    public function downloadCsvEva()
    {
        $date  = date('Y-m-d',strtotime(request('date')));
        if(!empty(session('device_id')))
        {
            $did   = session('device_id');
        }else
        {       
            $did   = request('did');
        }
        $vehicle   = request('vehicle');
        $vin_no    = request('vin_no');
        $test_type = request('test_type');
        $start = $date." 00:00:00";
        $end   = $date." 23:59:59";
       
        //Get data to be put into csv from device_data table
        $records         = Fetch::retriveCsvEva($start,$end,$did,$vehicle,$vin_no,$test_type);
        // print_r($records);die;
        $device_data_col = array();
        
        if(!empty($records)) {
            $columns = Fetch::getfields('device_data');
            unset($columns[0]);
            foreach($columns as $sdt){
                $device_data_col[] = $sdt->Field; 
            }
          
            $csvheaderArr   = array_values($device_data_col);
            $csvheaderNames = Fetch::getValuePartList();
            foreach($csvheaderNames as $names){
                $csvheaderName[$names->id] = $names->device1;
            }
            
            foreach ($csvheaderArr as $key => $value)
            {    
                if (substr($value, 0, 1) == 'F')
                {
                    $i = substr($value, 1);
                    if(is_numeric($i))
                    {
                        if(isset($csvheaderName[$i]))
                        {
                            $csvheader[$key] = $csvheaderName[$i];
                        }
                        else
                        {
                            $csvheader[$key] = "";
                        }
                    }
                    else
                    {
                        $csvheader[$key] = $csvheaderArr[$key];
                    }
                }
                else
                {
                    $csvheader[$key] = $csvheaderArr[$key];
                }
            }
            $csvheader[83]='obdRuntimeSinceEngineStart';
            
            //Generate folder for Device ID if not previously available
            $pathInfo = getcwd();
            $folder   = "$pathInfo/datacsv/".$did;
            
            if(!file_exists($folder))
            {
              mkdir($folder,0777,true);
            }
            chmod($folder, 0777);

            //Generate file name for download
            $fileN = "$folder/". date('dmY',strtotime($start)) . ".csv";
            $fh    = fopen("$fileN",'w');
            
            //write out the headers
            fputcsv($fh, $csvheader);
            
            //write out the data in the csv
            $row = array();
            foreach($records as $row){
                $row = (array) $row;        //Make object into an array
                array_shift($row);
                fputcsv($fh, $row);
            }
            fclose($fh);
            chmod($fileN, 0777);
            unset($device_data);
        }
        else
        {
            // Do nothing
        }
    }
}

?>