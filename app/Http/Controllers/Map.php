<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;

class Map extends Controller
{
    public function index(){

        $user                = session('userdata');
        $deviceList       = Fetch::getDevicesLatLong($user['uid'],$user['ucategory']);
        $vehicleSettings  = Fetch::getVehicleSettingData();
        $mfdList          = explode(';',$vehicleSettings->manufacturer);
        $string           = array();
        // print_r($deviceList);die;
        // print_r($deviceList);die;
        if(!empty($deviceList)){
            $i=1;
            // print_r($deviceList);die;
            foreach($deviceList as $list){

              // $etime = date("H:i:sa",$list->etime);
              // $etime= date("H:i:s", substr($list->etime, 0, 10));
               // print_r($etime);die;
               $string[]=
                array('title'=>  $list->device_name."-".$list->device_id."@".$list->etime,
                       'lat'=>  $list->latitude,
                      'lng'=> $list->longitude,
                       'description'=>   $list->device_name."-".$list->device_id."</br>@".$list->etime
                   );
               
               $i++;
            }
        }
        $data = array(
            'title'        => 'Map',
            'device_data'  => $string
        );
        $data['map_open'] = '1';
        // $data['menu']     = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        // print_r($string);die;
    
        $data['menu']     = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    	return view('map/index')->with($data);
    }
}
