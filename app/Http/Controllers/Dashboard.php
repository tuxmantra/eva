<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Helper;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
class Dashboard extends Controller
{
    public function index($year = "", $month = "")
    {

        if (empty($year))
        {
            $year = date('Y');
        }

        if (empty($month))
        {
            $month = date('m');
        }
        $month1 = $month;
        $string = "";
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $user = session('userdata');
        // Statistics
        $data['total_test_done'] = Fetch::getTotalTest($user['uid'], $user['ucategory']);
        $data['passed'] = Fetch::getPassedTest($user['uid'], $user['ucategory']);
        $data['failed'] = Fetch::getFailedTest($user['uid'], $user['ucategory']);
        $data['device_act'] = Fetch::getDeviceActive($user['uid'], $user['ucategory']);

        $data['year']   = $year;
        $data['month']  = $month1;
        $data['title']  = 'Dashboard';

        
        // print_r($data);
        return view('main/dashboard')->with($data);
    }

    public function getGraphData1()
    {
        $string = "";
        $year = request('year');
        $graph1 = Fetch::getGraphData1($year);
        foreach ($graph1 as $gr)
        {
            $string .= $gr->Total . ", ";
        }
        $string = trim($string, ' ');
        $string = trim($string, ',');
        echo $string;
    }

    /*    public function profile(){
        
        $data = array(
        'title' => 'Profile Page'  
        );
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('main/profile')->with($data);
    }*/
}

