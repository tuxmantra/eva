<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Helper;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
class Device extends Controller
{
    // load add device form.
    public function add()
    {
        $partnerList = Fetch::getPartnerListById(3);
        $data = array(
            'title' => 'Add Device',
            'partner_list' => $partnerList,
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        return view('devices/adddevice')->with($data);
    }

    public function delete_app_device(Request $request) 
    {
    	$did = $request->post('did');
        $id = $request->post('appid');
        // echo $id;die;
        Fetch::delete_app_device($id);
        \Session::flash('message', 'App Deleted');
         return redirect('device_manager/' . $did);
    }

    //Deactive APP delete_app
    public function delete_app(Request $request) 
    {
        $id = $request->post('appid');
        // echo $id;die;
        Fetch::delete_app($id);
        \Session::flash('message', 'App Deleted');
        return redirect('install_app');
    }

    public function delete_firm_device(Request $request) 
    {
    	$did = $request->post('did');
        $id = $request->post('appid');
        // echo $id;die;
        Fetch::delete_firm_device($id);
        \Session::flash('message', 'Firmware Deleted');
         return redirect('device_manager/' . $did);
    }

    //Deactive FRM delete_frm
    public function delete_firm(Request $request) 
    {
        $id = $request->post('appid');
        // echo $id;die;
        Fetch::delete_firm($id);
        \Session::flash('message', 'Firmware Deleted');
        return redirect('firmware_mast');
    }

    public function deactive_app(Request $request) 
    {
        $id = $request->post('id');
        Fetch::deactive_app($id);
        \Session::flash('message', 'App Deactivated');
        return redirect('install_app');
    }

   //active APP
    public function deactive_app1(Request $request) 
    {
        $id = $request->post('id');
        Fetch::deactive_app1($id);
        \Session::flash('message', 'App Activated');
        return redirect('install_app');
    }

    //Deactive FRM
    public function deactive_firm(Request $request) 
    {
        $id = $request->post('id');
        Fetch::deactive_firm($id);
        \Session::flash('message', 'Firmware Deactivated');
        return redirect('firmware_mast');
    }

   //active FRM
    public function deactive_firm1(Request $request) 
    {
        $id = $request->post('id');
        Fetch::deactive_firm1($id);
        \Session::flash('message', 'Firmware Activated');
        return redirect('firmware_mast');
    }

    // Device form submit to add device
    public function saveDevice(Request $request)
    {
        $token = request('form_token');
        if ($token == session('form_token'))
        {
            session()->forget('form_token');
            $device_id = $request->post('device_id');
            $name = $request->post('name');
            $desc = $request->post('desc');
            $partner_id = $request->post('partner_id');
            $type = $request->post('type');
            $status = $request->post('status');
            $dev_cmds = $request->post('dev_cmds');
            $added_by = session('userdata') ['uid'];
            $state = Fetch::getDeviceStatus($device_id);
            if (!empty($state))
            {
                \Session::flash('message', 'Sorry Device Id Already Present.');
                return redirect('add-device');
                exit;
            }
            if (empty($device_id) || empty($name) || empty($desc) || empty($partner_id) || empty($type) || empty($status))
            {
                \Session::flash('message', 'Fields required.');
                return redirect('add-device');
                exit;
            }
            $data = array(
                'device_id' => $device_id,
                'name' => $name,
                'descrp' => $desc,
                'partner' => $partner_id,
                'devtype' => $type,
                'status' => $status,
                'dev_cmds' => $dev_cmds,
                'ad_by' => $added_by
            );
            // print_r($data);die;
            Common::insert('device', $data);
            \Session::flash('message', 'Device Added Successfully, Now add User.');
            return redirect('add-user?device='.$device_id.'&partner='.$partner_id);
            exit;
        }
        else
        {
            session()->forget('form_token');
            \Session::flash('message', 'Sorry, token expired now try.');
            return redirect('add-device');
        }
    }
    // Edit Device load
    public function edit($id)
    {
        $id = Helper::crypt(2, $id);
        $partnerList = Fetch::getPartnerListById(3);
        $deviceData = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'Edit User',
            'partner_list' => $partnerList,
            'device_data' => $deviceData,
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        return view('devices/editdevice')->with($data);
    }
    public function edit_apikey_save(Request $request)
    {
        $id1 = $request->post('id');
        $id = Helper::crypt(2, $id1);
        $key = $request->post('apikey');
        $name = $request->post('name');
        $exdat = $request->post('exdat');
        $status = $request->post('status');
        $ad_by = session('userdata') ['uid'];
        $ad_date = date("Y-m-d");
        // echo $ad_by;
        $newDate = date("Y-m-d", strtotime($exdat));
        // echo $exdat;die;
        $update = Fetch::updateapikey($id, $key, $name, $newDate, $status, $ad_by, $ad_date);
        \Session::flash('message', 'API Key Update.');
        return redirect('edit_apikey/' . $id1);
        // exit;
        
    }
    public function save_apikey(Request $request)
    {
        $key = $request->post('apikey');
        $name = $request->post('name');
        $exdat = $request->post('exdat');
        $status = $request->post('status');
        $ad_by = session('userdata') ['uid'];
        $ad_date = date("Y-m-d");
        // echo $ad_by;
        $newDate = date("Y-m-d", strtotime($exdat));
        // echo $exdat;die;
        $update = Fetch::saveapikey($key, $name, $newDate, $status, $ad_by, $ad_date);
        \Session::flash('message', 'API Key Saved.');
        return redirect('API_key');
        // exit;
        
    }
    public function edit_apikey($id)
    {
        $id = Helper::crypt(2, $id);
        $data = array(
            'title' => 'API key',
            'form_token' => Helper::token(8) ,
            'mast_open' => 1,
            'api_open' => 1,
            'id' => $id
        );
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['api_key_data'] = Fetch::getapikeyid($id);
        return view('devices/edit_apikey')->with($data);
    }
    public function api_key()
    {
        $data = array(
            'title' => 'API key',
            'form_token' => Helper::token(8) ,
            'mast_open' => 1,
            'api_open' => 1
        );
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['api_key'] = Fetch::getapikey();
        // print_r($data);
        return view('devices/API_key')->with($data);
    }
    public function add_apikey()
    {
        $data = array(
            'title' => 'API key',
            'form_token' => Helper::token(8) ,
            'api_token' => Helper::token(32) ,
            'mast_open' => 1,
            'api_open' => 1
        );
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        // print_r($data);
        return view('devices/add_apikey')->with($data);
    }
    // Save edit device form
    public function editdevice(Request $request)
    {
        $token = request('form_token');
        $id = request('id');
        if ($token == session('form_token'))
        {
            session()->forget('form_token');
            $device_id = $request->post('device_id');
            $name = $request->post('name');
            $desc = $request->post('desc');
            $partner_id = $request->post('partner_id');
            $type = $request->post('type');
            $status = $request->post('status');
            $dev_cmds = $request->post('dev_cmds');
            // echo $device_id."<br>";
            // echo $name."<br>";
            // echo $desc."<br>";
            // echo $partner_id."<br>";
            // echo $type."<br>";
            // echo $status."<br>";
            
            if (empty($device_id) || empty($name) || empty($desc) || empty($partner_id) || empty($type))
            {
                \Session::flash('message', 'Fields required.');
                return redirect('device_manager/' . $id);
                exit;
            }
            $data = array(
                'device_id' => $device_id,
                'name' => $name,
                'descrp' => $desc,
                'partner' => $partner_id,
                'devtype' => $type,
                'status' => $status,
                'dev_cmds' => $dev_cmds
            );
            // print_r($data);die;
            $idd = Helper::crypt(2, $id);
            $where = array(
                'id' => $idd
            );
            Common::updateTable('device', $where, $data);
            \Session::flash('message', 'Device Updated Successfully.');
            return redirect('device_manager/' . $id);
        }
        else
        {
            session()->forget('form_token');
            \Session::flash('message', 'Sorry, token expired. Try again.');
            return redirect('device_manager/' . $id);
        }
    }
    public function firmaware_app(Request $request)
    {
        $token = request('form_token');
        // $id    = request('id');
        // print_r($_POST);die;
        session()->forget('form_token');
        $name = $request->post('vname');
        $ver = $request->post('version');
        $file = $request->post('file');
        // $devids   = $request->post('devid');
        $path = $request->post('path');
        $devid = $request->post('devid');
        // $dev_cmds   = $request->post('dev_cmds');
        if (in_array("-1", $devid))
        {
            $devall = "-1";
            $devid = array();
            $devidss = Fetch::getDevices_new();
            foreach ($devidss as $key => $value)
            {
                array_push($devid, $value->device_id);
            }
            // print_r($devids);die;
        }
        else
        {
            $devall = implode(":", $devid);
            $devid = $devid;
        }
        $devids = array_filter($devid);
        // print_r($request->post());die;
        if (empty($name) || empty($ver))
        {
            \Session::flash('message', 'Please Enter all the fields');
            return redirect('firmware_mast');
            exit();
        }
        else
        {
            if (!empty($_FILES['file']['name']))
            {
                $file_name = $_FILES['file']['name'];
                $photo_person = $_FILES['file']['tmp_name'];
                move_uploaded_file($photo_person, public_path() . "/firm_release/" . $file_name);
                // $appid = Fetch::insertnewapp($name,$ver,$file_name,$path,$devall);
                
            }
            else
            {
                $file_name = "";
            }
            $appid = Fetch::insertnewfirm($name, $ver, $file_name, $path, $devall);
            foreach ($devids as $key => $value)
            {
                $partnerList = Fetch::insertfirmdevice($name, md5($name) , $value, $appid, $ver, $path, $file_name);
            }
            \Session::flash('message', 'Firmware Uploaded');
            return redirect('firmware_mast');
            exit();
            //   if(!empty($_FILES['file']['tmp_name'])){
            //     $file_name         = $_FILES['file']['name'];
            //    $photo_person      = $_FILES['file']['tmp_name'];
            //   move_uploaded_file($photo_person,public_path()."/firm_release/".$file_name);
            //    $partnerList = Fetch::insertnewfirm($name,$ver,$file_name,$path,$devall);
            //   \Session::flash('message','App Uploaded');
            //   return redirect('firmware_mast');
            //                 exit();
            // }else{
            //    \Session::flash('message','Please select file to Upload');
            //                 return redirect('install_app');
            //                 exit();
            // }
            
        }
    }
    public function save_app(Request $request)
    {
        $token = request('form_token');
        // $id    = request('id');
        // print_r($_POST);die;
        if (1)
        {
            session()->forget('form_token');
            $name = $request->post('vname');
            $ver = $request->post('version');
            $file = $request->post('file');
            // $devids   = $request->post('devid');
            $path = $request->post('path');
            $devid = $request->post('devid');
            // $dev_cmds   = $request->post('dev_cmds');
            // print_r($devid);die;
            if (in_array("-1", $devid))
            {
                $devall = "-1";
                $devid = array();
                $devidss = Fetch::getDevices_new();
                foreach ($devidss as $key => $value)
                {
                    array_push($devid, $value->device_id);
                }
                // print_r($devids);die;
                
            }
            else
            {
                $devall = implode(":", $devid);
                $devid = $devid;
            }
            $devids = array_filter($devid);
            // print_r($devall);die;
            if (empty($name) || empty($ver))
            {
                \Session::flash('message', 'Please Enter all the fields');
                return redirect('install_app');
                exit();
            }
            if (!empty($_FILES['file']['name']))
            {
                $file_name = $_FILES['file']['name'];
                $photo_person = $_FILES['file']['tmp_name'];
                move_uploaded_file($photo_person, public_path() . "/app_release/" . $file_name);
                
                $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                $file = basename($file_name,".".$ext);
                // echo $file;die;

                //The URL that accepts the file upload.
                $url = 'http://172.105.41.167/install_apps/upload.php';

                //The name of the field for the uploaded file.
                $uploadFieldName = 'user_file';

                //The full path to the file that you want to upload
                $filePath = public_path().'/app_release/'.$file_name;


                //Initiate cURL
                $ch = curl_init();

                //Set the URL
                curl_setopt($ch, CURLOPT_URL, $url);

                //Set the HTTP request to POST
                curl_setopt($ch, CURLOPT_POST, true);

                //Tell cURL to return the output as a string.
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //If the function curl_file_create exists
                if(function_exists('curl_file_create')){
                    //Use the recommended way, creating a CURLFile object.
                    $filePath = curl_file_create($filePath);
                } else{
                    //Otherwise, do it the old way.
                    //Get the canonicalized pathname of our file and prepend
                    //the @ character.
                    $filePath = '@' . realpath($filePath);
                    //Turn off SAFE UPLOAD so that it accepts files
                    //starting with an @
                    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
                }

                //Setup our POST fields
                $postFields = array(
                    $uploadFieldName => $filePath,
                    'blahblah' => 'Another POST FIELD'
                );

                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

                //Execute the request
                $result = curl_exec($ch);

                //If an error occured, throw an exception
                //with the error message.
                if(curl_errno($ch)){
                    throw new Exception(curl_error($ch));
                }

                //Print out the response from the page
                // echo $result; die;
                $path = 'http://172.105.41.167/install_apps/'.$file.'/'.$file_name;
                $appid = Fetch::insertnewapp($name, $ver, $file_name, $path, $devall);
                foreach ($devids as $key => $devid)
                {
                    $partnerList = Fetch::insertappdevice($name, md5($name) , $devid, $appid, $ver, $path, $file_name);
                }
                // die;
                \Session::flash('message', 'App Uploaded');
                return redirect('install_app');
                exit();
            }
            else
            {
                $file_name = "";
                $appid = Fetch::insertnewapp($name, $ver, $file_name, $path, $devall);
                foreach ($devids as $key => $devid)
                {
                    $partnerList = Fetch::insertappdevice($name, md5($name) , $devid, $appid, $ver, $path, $file_name);
                }
                \Session::flash('message', 'App Uploaded');
                return redirect('install_app');
                exit();
            }
        }
    }
    public function app_ver_save(Request $request)
    {
        $token = request('form_token');
        // $id    = request('id');
        // print_r($_POST);die;
        $appid = request('appid');
        if (1)
        {
            session()->forget('form_token');
            $name = $request->post('vname');
            $ver = $request->post('version');
            $file = $request->post('file');
            // $devids   = $request->post('devid');
            $path = $request->post('path');
            $devid = $request->post('devid');
            // $dev_cmds   = $request->post('dev_cmds');
            // print_r($devid);die;
            if (in_array("-1", $devid))
            {
                $devall = "-1";
                $devid = array();
                $devidss = Fetch::getDevices_new();
                foreach ($devidss as $key => $value)
                {
                    array_push($devid, $value->device_id);
                }
                // print_r($devids);die;
                
            }
            else
            {
                $devall = implode(":", $devid);
                $devid = $devid;
            }
            $devids = array_filter($devid);
            // print_r($devall);die;
            if (empty($name) || empty($ver))
            {
                \Session::flash('message', 'Please Enter all the fields');
                return redirect("update_verion?appid=" . $appid);
                exit();
            }
            if (!empty($_FILES['file']['name']))
            {
                $file_name = $_FILES['file']['name'];
                $photo_person = $_FILES['file']['tmp_name'];
                move_uploaded_file($photo_person, public_path() . "/app_release/" . $file_name);
                
                $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                $file = basename($file_name,".".$ext);
                // echo $file;die;

                //The URL that accepts the file upload.
                $url = 'http://172.105.41.167/install_apps/upload.php';

                //The name of the field for the uploaded file.
                $uploadFieldName = 'user_file';

                //The full path to the file that you want to upload
                $filePath = public_path().'/app_release/'.$file_name;


                //Initiate cURL
                $ch = curl_init();

                //Set the URL
                curl_setopt($ch, CURLOPT_URL, $url);

                //Set the HTTP request to POST
                curl_setopt($ch, CURLOPT_POST, true);

                //Tell cURL to return the output as a string.
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //If the function curl_file_create exists
                if(function_exists('curl_file_create')){
                    //Use the recommended way, creating a CURLFile object.
                    $filePath = curl_file_create($filePath);
                } else{
                    //Otherwise, do it the old way.
                    //Get the canonicalized pathname of our file and prepend
                    //the @ character.
                    $filePath = '@' . realpath($filePath);
                    //Turn off SAFE UPLOAD so that it accepts files
                    //starting with an @
                    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
                }

                //Setup our POST fields
                $postFields = array(
                    $uploadFieldName => $filePath,
                    'blahblah' => 'Another POST FIELD'
                );

                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

                //Execute the request
                $result = curl_exec($ch);

                //If an error occured, throw an exception
                //with the error message.
                if(curl_errno($ch)){
                    throw new Exception(curl_error($ch));
                }

                //Print out the response from the page
                // echo $result; die;
                $path = 'http://172.105.41.167/install_apps/'.$file.'/'.$file_name;
                $appid = Fetch::insertnewapp($name, $ver, $file_name, $path, $devall);
                foreach ($devids as $key => $devid)
                {
                    $partnerList = Fetch::insertappdevice($name, md5($name) , $devid, $appid, $ver, $path, $file_name);
                }
                // die;
                \Session::flash('message', 'App Uploaded');
                return redirect('install_app');
                exit();
            }
            else
            {
                $file_name = "";
            }
            $updateapp = Fetch::updateappverion($appid, $name, $file_name, $ver, $path, $devall);
            foreach ($devids as $key => $value)
            {
                $count = Fetch::checkappdevice($appid, $value);
                // echo $count[0]->coun."<br>";
                if ($count[0]->coun > '0')
                {
                    $partnerList = Fetch::updateappdevice($name, md5($name) , $value, $appid, $ver, $path, $file_name);
                }
                else
                {
                    $partnerList = Fetch::insertappdevice($name, md5($name) , $value, $appid, $ver, $path, $file_name);
                }
            }
            // die;
            \Session::flash('message', 'App Verion Updated');
            return redirect("update_verion?appid=" . $appid);
            exit();
            // }else{
            //  $file_name="";
            //  $appid = Fetch::insertnewapp($name,$ver,$file_name,$path,$devall);
            // foreach ($devids as $key => $devid) {
            //    $partnerList = Fetch::insertappdevice($name,md5($name),$devid,$appid,$ver,$path,$file_name);
            //   }
            //    \Session::flash('message','App Uploaded');
            //                 return redirect('install_app');
            //                 exit();
            // }
            
        }
    }
    public function firm_ver_update(Request $request)
    {
        $token = request('form_token');
        // $id    = request('id');
        // print_r($_POST);die;
        $appid = request('appid');
        $path = $request->post('path');
        $ver = $request->post('ver');
        $file = $request->post('file');
        $devid = $request->post('devid');
        $file_p = $request->post('file_p');
        $url = $request->post('url');
        // print_r($devid); die;
        if (!empty($_FILES['file']['name']))
        {
            $file_name = $_FILES['file']['name'];
            $photo_person = $_FILES['file']['tmp_name'];
            move_uploaded_file($photo_person, public_path() . "/app_release/" . $file_name);
            // $appid = Fetch::insertnewapp($name,$ver,$file_name,$path,$devall);
            
        }
        else
        {
            $file_name = $file_p;
        }
        $update = Fetch::update_firm_ver($appid, $devid, $file_name, $ver, $path);
        \Session::flash('message', 'Firm Verion Updated');
        return redirect('device-list');
        // print_r($file); die;
        
    }
    public function app_ver_update(Request $request)
    {
        $token = request('form_token');
        // $id    = request('id');
        // print_r($_POST);die;
        $appid = request('appid');
        $path = $request->post('path');
        $ver = $request->post('ver');
        $file = $request->post('file');
        $devid = $request->post('devid');
        $file_p = $request->post('file_p');
        $url = $request->post('url');
        if (!empty($_FILES['file']['name']))
        {
            $file_name = $_FILES['file']['name'];
            $photo_person = $_FILES['file']['tmp_name'];
            move_uploaded_file($photo_person, public_path() . "/app_release/" . $file_name);
            
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            $file = basename($file_name,".".$ext);
            // echo $file;die;

            //The URL that accepts the file upload.
            $url = 'http://172.105.41.167/install_apps/upload.php';

            //The name of the field for the uploaded file.
            $uploadFieldName = 'user_file';

            //The full path to the file that you want to upload
            $filePath = public_path().'/app_release/'.$file_name;


            //Initiate cURL
            $ch = curl_init();

            //Set the URL
            curl_setopt($ch, CURLOPT_URL, $url);

            //Set the HTTP request to POST
            curl_setopt($ch, CURLOPT_POST, true);

            //Tell cURL to return the output as a string.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //If the function curl_file_create exists
            if(function_exists('curl_file_create')){
                //Use the recommended way, creating a CURLFile object.
                $filePath = curl_file_create($filePath);
            } else{
                //Otherwise, do it the old way.
                //Get the canonicalized pathname of our file and prepend
                //the @ character.
                $filePath = '@' . realpath($filePath);
                //Turn off SAFE UPLOAD so that it accepts files
                //starting with an @
                curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
            }

            //Setup our POST fields
            $postFields = array(
                $uploadFieldName => $filePath,
                'blahblah' => 'Another POST FIELD'
            );

            curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

            //Execute the request
            $result = curl_exec($ch);

            //If an error occured, throw an exception
            //with the error message.
            if(curl_errno($ch)){
                throw new Exception(curl_error($ch));
            }

            //Print out the response from the page
            // echo $result; die;
            $path = 'http://172.105.41.167/install_apps/'.$file.'/'.$file_name;
            $appid = Fetch::insertnewapp($name, $ver, $file_name, $path, $devall);
            foreach ($devids as $key => $devid)
            {
                $partnerList = Fetch::insertappdevice($name, md5($name) , $devid, $appid, $ver, $path, $file_name);
            }
            // die;
            \Session::flash('message', 'App Uploaded');
            return redirect('install_app');
            exit();
        }
        else
        {
            $file_name = $file_p;
        }
        $update = Fetch::update_app_ver($appid, $devid, $file_name, $ver, $path);
        \Session::flash('message', 'App Verion Updated');
        return redirect('device-list');
        // print_r($file); die;
        
    }
    public function firm_ver_save(Request $request)
    {
        $token = request('form_token');
        // $id    = request('id');
        // print_r($_POST);die;
        $appid = request('appid');
        if (1)
        {
            session()->forget('form_token');
            $name = $request->post('vname');
            $ver = $request->post('version');
            $file = $request->post('file');
            // $devids   = $request->post('devid');
            $path = $request->post('path');
            $devid = $request->post('devid');
            // $dev_cmds   = $request->post('dev_cmds');
            // print_r($devid);die;
            // print_r($devall);die;
            if (empty($name) || empty($ver) || empty($devid))
            {
                \Session::flash('message', 'Please Enter all the fields');
                return redirect("update_verion_firm?appid=" . $appid);
                exit();
            }
            if (in_array("-1", $devid))
            {
                $devall = "-1";
                $devid = array();
                $devidss = Fetch::getDevices_new();
                foreach ($devidss as $key => $value)
                {
                    array_push($devid, $value->device_id);
                }
                // print_r($devids);die;
                
            }
            else
            {
                $devall = implode(":", $devid);
                $devid = $devid;
            }
            $devids = array_filter($devid);
            if (!empty($_FILES['file']['name']))
            {
                $file_name = $_FILES['file']['name'];
                $photo_person = $_FILES['file']['tmp_name'];
                move_uploaded_file($photo_person, public_path() . "/app_release/" . $file_name);
                // $appid = Fetch::insertnewapp($name,$ver,$file_name,$path,$devall);
                
            }
            else
            {
                $file_name = "";
            }
            $updateapp = Fetch::updatefirmverion($appid, $name, $file_name, $ver, $path, $devall);
            foreach ($devids as $key => $value)
            {
                $count = Fetch::checkfirmdevice($appid, $value);
                // echo $count[0]->coun."<br>";
                if ($count[0]->coun > '0')
                {
                    $partnerList = Fetch::updatefirmdevice($name, md5($name) , $value, $appid, $ver, $path, $file_name);
                }
                else
                {
                    $partnerList = Fetch::insertfirmdevice($name, md5($name) , $value, $appid, $ver, $path, $file_name);
                }
            }
            // die;
            \Session::flash('message', 'Firmware Verion Updated');
            return redirect("update_verion_firm?appid=" . $appid);
            exit();
        }
    }
    public function device_manager($id)
    {
        $id = Helper::crypt(2, $id);
        $partnerList = Fetch::getPartnerListById(3);
        $deviceData = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'Device Manager',
            'id' => $id,
            'partner_list' => $partnerList,
            'device_data' => $deviceData,
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        return view('devices/device_manager')->with($data);
    }
    public function save_cmd(Request $request)
    {
        $key   = $request->post('key');
        $value = $request->post('value');
        $devid = $request->post('devid');
        $deviceData = Fetch::getdevice($devid);
        if($deviceData[0]->devtype == 2)
        {
            $cmd   = $value[0];
        }
        else
        {
            $cmd1  = array_combine($key, $value);
            $cmd   = json_encode($cmd1);
        }
        
        $save  = Fetch::updatecmd($devid, $cmd);
        // echo $cmd.$devid; die;
        return $save;
    }
    public function firmware($id)
    {
        $id = Helper::crypt(2, $id);
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'Firmware Manager',
            'id' => $id,
            'partner_list' => '',
            'device_data' => $deviceData,
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getfirmdetailsdevice($deviceData[0]->device_id);
        return view('devices/firmware')
            ->with($data);
    }
    public function install_app_version($id, request $request)
    {
        $id = Helper::crypt(2, $id);
        $appid = $request->post('appid');
        // print_r($appid);die;
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'App Manager',
            'id' => $id,
            'partner_list' => '',
            'device_data' => $deviceData,
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getappdetailsbyid($appid, $deviceData[0]->device_id);
        // print_r(Fetch::getappdetailsbyid($appid));die;
        return view('devices/install_app_version')
            ->with($data);
    }

    public function uninstall_app($id, request $request)
    {
        // $id = Helper::crypt(2, $id);
        $appid = $request->post('id');
        Fetch::uninstall_app($appid);
        \Session::flash('message', 'App Un-installed');
        // return redirect('device_manager/' . $id);
    }

    public function uninstall_firm($id, request $request)
    {
        // $id = Helper::crypt(2, $id);
        $appid = $request->post('id');
        Fetch::uninstall_firm($appid);
        \Session::flash('message', 'Firmware Un-installed');
        // return redirect('device_manager/' . $id);
    }

    public function install_firm_version($id, request $request)
    {
        $id = Helper::crypt(2, $id);
        $appid = $request->post('appid');
        // print_r($appid);die;
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'Firmware Manager',
            'id' => $id,
            'partner_list' => '',
            'device_data' => $deviceData,
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getfirmdetailsbyid($appid, $deviceData[0]->device_id);
        // print_r(Fetch::getappdetailsbyid($appid));die;
        return view('devices/install_firm_version')
            ->with($data);
    }

    public function firmware_mast()
    {
        // $id          = Helper::crypt(2,$id);
        // $firmware = Fetch::getfirmware($id);
        // $deviceData  = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'Firmware Manager',
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['frm_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['device'] = Fetch::getDevicesAll1();
        $data['app_details'] = Fetch::getfirmdetails();
        return view('devices/firmware_master')->with($data);
    }

    public function install_app()
    {
        //    $id          = Helper::crypt(2,$id);
        // // $firmware = Fetch::getfirmware($id);
        // $deviceData  = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'App Manager',
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['app_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getappdetails_new();
        $data['device'] = Fetch::getDevicesAll1();
        return view('devices/install_app')->with($data);
    }

    public function update_verion(request $request)
    {
        //    $id          = Helper::crypt(2,$id);
        // // $firmware = Fetch::getfirmware($id);
        // $deviceData  = Fetch::getDeviceById($id);
        $appid = $request->post('appid');
        $data = array(
            'title' => 'App version',
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['app_open'] = '1';
        $data['appid'] = $appid;
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getappdetailsbyids($appid);
        $data['device'] = Fetch::getDevicesAll1();
        return view('devices/update_verion')->with($data);
    }

    public function update_verion_firm(request $request)
    {
        //    $id          = Helper::crypt(2,$id);
        // // $firmware = Fetch::getfirmware($id);
        // $deviceData  = Fetch::getDeviceById($id);
        $appid = $request->post('appid');
        $data = array(
            'title' => 'Firmware version',
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['frm_open'] = '1';
        $data['appid'] = $appid;
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getfirmdetailsbyids($appid);
        $data['device'] = Fetch::getDevicesAll1();
        return view('devices/update_verion_firm')->with($data);
    }

    public function device_log($id)
    {
        $id = Helper::crypt(2, $id);
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'Device Manager',
            'id' => $id,
            'partner_list' => '',
            'device_data' => $deviceData,
            'device_id' => $deviceData[0]->device_id,
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['log_details'] = Fetch::getlogdetails($deviceData[0]->device_id);
        // echo "Mayur";
        return view('devices/device_log')
            ->with($data);
    }

    public function device_conf($id)
    {
        $id = Helper::crypt(2, $id);
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'Device Manager',
            'id'    => $id,
            'partner_list' => '',
            'device_data'  => $deviceData,
            'device_id'    => $deviceData[0]->device_id,
            'form_token'   => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getappstatdetails($deviceData[0]->device_id);
        $data['app_Update'] = Fetch::appupdate($deviceData[0]->device_id);
        $data['firm_Update'] = Fetch::firmupdate($deviceData[0]->device_id);
        // $data['Com_Update'] = Fetch::commupdate($deviceData[0]->device_id);
        // $data['conf_Update'] = Fetch::confupdate($deviceData[0]->device_id);
        $data['firm_details'] = Fetch::getfirmstatdetails($deviceData[0]->device_id);
        $data['bt_dev'] = Fetch::getbtdevicedetails($deviceData[0]->device_id);
        $data['bt_details'] = Fetch::getbtdetailsbyid1($deviceData[0]->device_id);
        return view('devices/device_conf')
            ->with($data);
    }

    public function app_update($id)
    {
        $id = Helper::crypt(2, $id);
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'Device Manager',
            'id' => $id,
            'partner_list' => '',
            'device_data' => $deviceData,
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getappdetails($deviceData[0]->device_id);
        return view('devices/app_update')
            ->with($data);
    }

    public function add_bt($id)
    {
        $id = Helper::crypt(2, $id);
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'Device Manager',
            'id' => $id,
            'partner_list' => '',
            'device_data' => $deviceData,
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getbtdevicedetails($deviceData[0]->device_id);
        return view('devices/add_bt')
            ->with($data);
    }

    public function remove_bt_device($id, request $request)
    {
        $id = Helper::crypt(2, $id);
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $btid = $request->post('appid');
        $delete = Fetch::delete_bt_id($btid);
    }

    public function add_bt_dev($id)
    {
        $id = Helper::crypt(2, $id);
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $data = array(
            'title' => 'Device Manager',
            'id' => $id,
            'partner_list' => '',
            'device_data' => $deviceData,
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getappdetails($deviceData[0]->device_id);
        return view('devices/add_bt_dev')
            ->with($data);
    }

    public function update_bt_device($id, request $request)
    {
        $id = Helper::crypt(2, $id);
        // $btid =
        // // $firmware = Fetch::getfirmware($id);
        // $deviceData = Fetch::getDeviceById($id);
        $bt_id = $request->post('appid');
        $data = array(
            'title' => 'Device Manager',
            'partner_list' => '',
            'form_token' => Helper::token(8)
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        $data['app_details'] = Fetch::getbtdetailsbyid($bt_id);
        return view('devices/update_bt_device')->with($data);
    }

    public function update_bt_dev($id, request $request)
    {
        $id = Helper::crypt(2, $id);
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $postva = array();
        $dmake = $request->post('dmake');
        $btid = $request->post('btid');
        $dtype = $request->post('dtype');
        $mac_id = $request->post('mac_id');
        $port = $request->post('p');
        $oper = $request->post('op');
        $value = $request->post('value');
        $name = $request->post('name');
        $unit = $request->post('unit');
        if ($dtype == 'MetaMotionR')
        {
            $postva['bt_dev_make'] = $dmake;
            $postva['bt_dev_type'] = $dtype;
            $postva['mac_id'] = $mac_id;
            $postva['device_id'] = $deviceData[0]->device_id;
            // print_r($postva);
            // $count=count($port);
            $save = Fetch::update_bt_device($postva, $btid);
        }
        else
        {
            for ($i = 0;$i < count($port);$i++)
            {
                $postva['P' . $i . 'type'] = $port[$i];
                $postva['P' . $i . 'op'] = $oper[$i];
                $postva['P' . $i . 'val'] = $value[$i];
                $postva['P' . $i . 'name'] = $name[$i];
                $postva['P' . $i . 'unit'] = $unit[$i];
            }
            $postva['bt_dev_make'] = $dmake;
            $postva['bt_dev_type'] = $dtype;
            $postva['mac_id'] = $mac_id;
            $postva['update_status'] = '1';
            $postva['device_id'] = $deviceData[0]->device_id;
            // print_r($postva);
            // $count=count($port);
            $save = Fetch::update_bt_device($postva, $btid);
        }
    }

    public function save_bt_dev($id, request $request)
    {
        $id = Helper::crypt(2, $id);
        // $firmware = Fetch::getfirmware($id);
        $deviceData = Fetch::getDeviceById($id);
        $postva = array();
        $dmake = $request->post('dmake');
        $dtype = $request->post('dtype');
        $mac_id = $request->post('mac_id');
        $port = $request->post('p');
        $oper = $request->post('op');
        $value = $request->post('value');
        $name = $request->post('name');
        $unit = $request->post('unit');
        if ($dtype == 'MetaMotionR')
        {
            $postva['bt_dev_make'] = $dmake;
            $postva['bt_dev_type'] = $dtype;
            $postva['mac_id'] = $mac_id;
            $postva['device_id'] = $deviceData[0]->device_id;
            // print_r($postva);
            // $count=count($port);
            $check = Fetch::check_bt_device($mac_id, $deviceData[0]->device_id);
            if ($check[0]->count == 0)
            {
                $save = Fetch::save_bt_device($postva);
                return 'Data Saved';
            }
            else
            {
                return 'Mac ID Already Exist !';
            }
        }
        else
        {
            for ($i = 0;$i < count($port);$i++)
            {
                $postva['P' . $i . 'type'] = $port[$i];
                $postva['P' . $i . 'op'] = $oper[$i];
                $postva['P' . $i . 'val'] = $value[$i];
                $postva['P' . $i . 'name'] = $name[$i];
                $postva['P' . $i . 'unit'] = $unit[$i];
            }
            $postva['bt_dev_make'] = $dmake;
            $postva['bt_dev_type'] = $dtype;
            $postva['mac_id'] = $mac_id;
            $postva['update_status'] = '1';
            $postva['device_id'] = $deviceData[0]->device_id;
            // print_r($postva);
            // $count=count($port);
            $check = Fetch::check_bt_device($mac_id, $deviceData[0]->device_id);
            if ($check[0]->count == 0)
            {
                $save = Fetch::save_bt_device($postva);
                return 'Data Saved';
            }
            else
            {
                // \Session::flash('message','Mac ID Already Exist !');
                return 'Mac ID Already Exist !';
            }
        }
    }

    // Listing device list
    public function list()
    {
        $user                = session('userdata');
        $deviceList = Fetch::getDevicesAll($user['uid'],$user['ucategory']);
        $data = array(
            'title' => 'List Device',
            'device_list' => $deviceList
        );
        $data['mast_open'] = '1';
        $data['devi_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        return view('devices/listdevice')->with($data);
    }

    // while creating device checking status wheather device already present or not ajax request while adding device.
    public function checkDeviceIdStatus(Request $request)
    {
        $device_id = $request->post('device_id');
        $device = Fetch::getDeviceStatus($device_id);
        echo !empty($device) ? '1' : '0';
    }

    // device pending start
    public function devicePending()
    {
        $pendingList = Fetch::getDevicesPending();
        $data = array(
            'title' => 'List Device Pending',
            'device_pending' => $pendingList
        );
        $data['mast_open'] = '1';
        $data['devipen_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        return view('devices/devicepending')->with($data);
    }

    public function apiDevicePending(Request $request)
    {
        $devID = $request->post('did');
        $fileName = (is_array($request->post('files')) && $request->post('files') != "") ? implode(';', $request->post('files')) : "";
        //echo "update device set file_list = '$fileName' where device_id = '$devID'";die;
        // Update file list in table delete
        if (!empty($fileName))
        {
            //DB::select("update device set file_list = '$fileName' where device_id = '$devID'");
            Common::updateTable('device', array(
                'device_id' => $devID
            ) , array(
                'file_list' => $fileName
            ));
        }
        if ($fileName != "")
        {
            $file = explode(';', $fileName);
            for ($i = 0;$i <= count($file) - 1;$i++)
            {
                $fle = $file[$i];
                $pathInfo = getcwd();
                $host = request()->getHttpHost();
                $cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/fileupload-back-app.php $fle";
                Common::updateTable('device', array(
                    'id' => $devID
                ) , array(
                    'act' => $cmd
                ));
                // print_r($cmd);die;
                exec($cmd . " > /dev/null &");
            }
        }
    }

    // device pending end
    public function apiDevicePending_delete(Request $request)
    {
        $devID = $request->post('did');
        $fileName = (is_array($request->post('files')) && $request->post('files') != "") ? implode(';', $request->post('files')) : "";
        //echo "update device set file_list = '$fileName' where device_id = '$devID'";die;
        // Update file list in table delete
        if (!empty($fileName))
        {
            //DB::select("update device set file_list = '$fileName' where device_id = '$devID'");
            // Common::updateTable('device', array('device_id' => $devID),array('file_list' => $fileName));
            return Common::updateTable('device', array(
                'id' => $devID
            ) , array(
                'file_list' => ''
            ));
        }
    }

    public function getDeviceList()
    {
        $search = request('search');
        $did = request('did');
        $user = session('userdata');
        $user_type = $user['utype'];
        $user_category = session('userdata') ['ucategory'];
        $device_list = Fetch::getDeviceListByUserType($user['uid'], $user_category, $search);
        $data = "";
        $data .= '
<tbody>
   ';
        if ($user_category == 1 || $user_category == 2 || $user_category == 3)
        {
            if (!empty($device_list))
            {
                $list['sata'] = array(
                    'did' => $did,
                    'device_list' => $device_list
                );
                $data .= view('devices/devicelist')->with($list, $did);
            }
            else
            {
                $data .= '
   <tr>
      <td>No Results</td>
   </tr>
   ';
            }
        }
        else
        {
            $data .= '
   <tr>
      <td>No Results</td>
   </tr>
   ';
        }
        $data .= '
</tbody>
';
        return $data;
    }

    public function getDeviceListobd()
    {
        $search = request('search');
        $did = request('did');
        // $dtype = $user['type'];
        $user = session('userdata');
        $user_type = $user['utype'];
        $user_category = session('userdata') ['ucategory'];
        $device_list = Fetch::getDeviceListByUserType_obd($user['uid'], $user_category, $search);
        // print_r($device_list);
        $data = "";
        $data .= '
<tbody>
   ';
        if ($user_category == 1 || $user_category == 2 || $user_category == 3)
        {
            if (!empty($device_list))
            {
                $list['sata'] = array(
                    'did' => $did,
                    'device_list' => $device_list
                );
                $data .= view('devices/devicelist')->with($list, $did);
            }
            else
            {
                $data .= '
   <tr>
      <td>No Results</td>
   </tr>
   ';
            }
        }
        else
        {
            $data .= '
   <tr>
      <td>No Results</td>
   </tr>
   ';
        }
        $data .= '
</tbody>
';
        return $data;
    }

    public function getDeviceListcan()
    {
        $search = request('search');
        $did = request('did');
        // $dtype = request('type');
        $user = session('userdata');
        $user_type = $user['utype'];
        $user_category = session('userdata') ['ucategory'];
        $device_list = Fetch::getDeviceListByUserType_can($user['uid'], $user_category, $search);
        $data = "";
        $data .= '
<tbody>
   ';
        if ($user_category == 1 || $user_category == 2 || $user_category == 3)
        {
            if (!empty($device_list))
            {
                $list['sata'] = array(
                    'did' => $did,
                    'device_list' => $device_list
                );
                $data .= view('devices/devicelist')->with($list, $did);
            }
            else
            {
                $data .= '
   <tr>
      <td>No Results</td>
   </tr>
   ';
            }
        }
        else
        {
            $data .= '
   <tr>
      <td>No Results</td>
   </tr>
   ';
        }
        $data .= '
</tbody>
';
        return $data;
    }

    public function getDeviceList1()
    {
        $search = request('search');
        $did = request('did');
        $user = session('userdata');
        $user_type = $user['utype'];
        $user_category = session('userdata') ['ucategory'];
        $device_list = Fetch::getDeviceListByUserType($user['uid'], $user_category, $search);
        $data = "";
        if ($user_category == 1 || $user_category == 2 || $user_category == 3)
        {
            if (!empty($device_list))
            {
                foreach ($device_list as $key => $dl)
                {
                    $data .= '<div ';
                    if ($did == $dl->device_id)
                    {
                        $data .= 'style="background-color: aliceblue;"';
                    }
                    $data .= ' id="row_' . $dl->device_id . '" onclick="device_row(' . "'" . $dl->device_id . "'" . ');"  class="detailed-db safe">
<p style="font-size: 13px; margin-bottom: 0;"><a class="nav-link" href="javascript:void(0)">' . $dl->device_name . '" (Device ID "' . $dl->device_id . '")
   "' . Helper::getManufacturerById($dl->manu) . '</a>
</p>
</div>';
                }
            }
            else
            {
                $data .= '
<div class="detailed-db">No Devices</div>
';
            }
        }
        else
        {
            $data .= '
<div class="detailed-db">No Devices</div>
';
        }
        return $data;
    }
}

