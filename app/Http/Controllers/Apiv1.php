<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newuser;
use Illuminate\Support\Arr;


class Apiv1 extends Controller
{

  public function test_login(Request $request){
      $response = array();
      $email    = $request->post('email');
      $password = $request->post('password');
      // echo  $email; exit;
        // varify
        if(empty($email)){
            $response['error'] = '0';
            $response['data']  = 'Email required.';
        }elseif(empty($password)){
            $response['error'] = '0';
            $response['data']  = 'Password required.';
        }
        else{
        // process
          $password = md5($password);
          $token    = Helper::token1(10);
          $exist_email = Fetch::getUserByEmail($email);
          // print_r($password); echo "Mayue";
          // print_r($exist_email[0]->upwd); 
          // die;
          if(!empty($exist_email)){
               if($password != $exist_email[0]->upwd){
                  
                   $response['data']  = 'Wrong password entered.';
               }elseif($password == $exist_email[0]->upwd){
                  // Login Success
                  $login_data    = array(
                  'user_id'      =>  $exist_email[0]->uid,
                  'login_token'  => $token,
                  'ip'           => '',
                  'mode'         => 'Mobile',
                  'login_status' => '1'
                  );
                  $login_id = Common::insertGetId('login_session',$login_data);
                  $userdata = array(
                  'uid'          =>  $exist_email[0]->uid,
                  'utype'        =>  $exist_email[0]->utype,
                  'uemail'       =>  $exist_email[0]->uemail,
                  'login_token'  => $token,
                  'ucategory'    =>  $exist_email[0]->ucategory,
                  'login_status' => '1'
                  );
                  $response['success'] = '1';
                  $response['data']    = $userdata;
               }else{
                   $response['error'] = "0";
                   $response['data']  = 'User not exist.';
               }
            }else{
                   $response['error'] = "0";
                   $response['data']  = 'User not exist.';
            }
        }
        echo json_encode($response);
    }

     public function monitor_data(Request $request){
        
         $data = json_decode($request->data, TRUE);
         $deviceID=$data['devID']; 
         $devInfo=Fetch::getdevice($deviceID);

         if(!empty($devInfo))
          {
            $dat = Fetch::getDriveMoniter($deviceID);
            $data = $dat;
            // print_r( $data);die;
            $batHel = array();
            if($data[0]->bat_st == 1)
            {
              $batHel['rate'] = "1";
              $batHel['con'] = "Bad";
              $batHel['msg'] = "\"Battery Critical\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
            }
            elseif($data[0]->bat_st == 2)
            {
              $batHel['rate'] = "2";
              $batHel['con'] = "Bad–Okay";
              $batHel['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
            }
            elseif($data[0]->bat_st == 3)
            {
              $batHel['rate'] = "3";
              $batHel['con'] = "Bad-Okay";
              $batHel['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
            }
            elseif($data[0]->bat_st == 4)
            {
              $batHel['rate'] = "4";
              $batHel['con'] = "Bad";
              $batHel['msg'] = "\"Battery Low\" Please drive to charge.";
            }
            elseif($data[0]->bat_st == 5)
            {
              $batHel['rate'] = "5";
              $batHel['con'] = "Good";
              $batHel['msg'] = "";
            }


            //Turbo
    $turbo =array();
        $turbo['rate'] = "";
        $turbo['con'] = "";
        $turbo['msg'] = "";
    if($data[0]->idlb_c == 1)
    {
      if($data[0]->pekb_c == 1)
      {
        $turbo['rate'] = "1";
        $turbo['con'] = "Bad";
        $turbo['msg'] = "";
      }
      elseif($data[0]->pekb_c == 2)
      {
        $turbo['rate'] = "2";
        $turbo['con'] = "Bad-Okay";
        $turbo['msg'] = "";
      }
      elseif($data[0]->pekb_c == 3)
      {
        $turbo['rate'] = "3";
        $turbo['con'] = "Okay";
        $turbo['msg'] = "";
      }
    }
    elseif($data[0]->idlb_c== 2)
    {
      if($data[0]->pekb_c == 1)
      {
        $turbo['rate'] = "1";
        $turbo['con'] = "Bad";
        $turbo['msg'] = "";
      }
      elseif($data[0]->pekb_c == 2)
      {
        $turbo['rate'] = "3";
        $turbo['con'] = "Okay";
        $turbo['msg'] = "";
      }
      elseif($data[0]->pekb_c == 3)
      {
        $turbo['rate'] = "4";
        $turbo['con'] = "Okay-Good";
        $turbo['msg'] = "";
      }
    }
    elseif($data[0]->pekb_c == 3)
    {
      if($data[0]->pekb_c == 1)
      {
        $turbo['rate'] = "2";
        $turbo['con'] = "Bad-Okay";
        $turbo['msg'] = "";
      }
      elseif($data[0]->pekb_c == 2)
      {
        $turbo['rate'] = "3";
        $turbo['con'] = "Okay";
        $turbo['msg'] = "";
      }
      elseif($data[0]->pekb_c == 3)
      {
        $turbo['rate'] = "5";
        $turbo['con'] = "Good";
        $turbo['msg'] = "";
      }
    }
    $turbo['msg'] = (trim($data[0]->idlb_m) != trim($data[0]->pekb_m) ) ? trim($data[0]->idlb_m) ."<br/>". trim($data[0]->pekb_m) :  trim($data[0]->pekb_m);

      $air =array();
        $air['rate'] = "";
        $air['con'] = "";
        $air['msg'] = "";
    if($data[0]->idlbc_v == 1)
    {
      if($data[0]->peakb_v == 1)
      {
        $air['rate'] = "1";
        $air['con'] = "Bad";
        $air['msg'] = "";
      }
      elseif($data[0]->peakb_v == 2)
      {
        $air['rate'] = "2";
        $air['con'] = "Bad-Okay";
        $air['msg'] = "";
      }
      elseif($data[0]->peakb_v == 3)
      {
        $air['rate'] = "3";
        $air['con'] = "Okay";
        $air['msg'] = "";
      }
    }
    elseif($data[0]->idlbc_v == 2)
    {
      if($data[0]->peakb_v == 1)
      {
        $air['rate'] = "1";
        $air['con'] = "Bad";
        $air['msg'] = "";
      }
      elseif($data[0]->peakb_v == 2)
      {
        $air['rate'] = "3";
        $air['con'] = "Okay";
        $air['msg'] = "";
      }
      elseif($data[0]->peakb_v == 3)
      {
        $air['rate'] = "4";
        $air['con'] = "Okay-Good";
        $air['msg'] = "";
      }
    }
    elseif($data[0]->idlbc_v == 3)
    {
      if($data[0]->peakb_v == 1)
      {
        $air['rate'] = "2";
        $air['con'] = "Bad-Okay";
        $air['msg'] = "";
      }
      elseif($data[0]->peakb_v == 2)
      {
        $air['rate'] = "3";
        $air['con'] = "Okay";
        $air['msg'] = "";
      }
      elseif($data[0]->peakb_v == 3)
      {
        $air['rate'] = "5";
        $air['con'] = "Good";
        $air['msg'] = "";
      }
    }
    $air['msg'] = (trim($data[0]->idlbc_m) != trim($data[0]->peakb_m) ) ? trim($data[0]->idlbc_m) ."<br/>". trim($data[0]->peakb_m) : trim($data[0]->idlbc_m);

    $fuel =array();
         $fuel['rate'] = "";
        $fuel['con'] = "";
        $fuel['msg'] = "";
        // $rcOcil['msg'];
    if($data[0]->idl_crp == 1)
    {
      if($data[0]->peak_crp == 1)
      {
        $fuel['rate'] = "1";
        $fuel['con'] = "Bad";
        $fuel['msg'] = "";
      }
      elseif($data[0]->peak_crp == 2)
      {
        $fuel['rate'] = "2";
        $fuel['con'] = "Bad-Okay";
        $fuel['msg'] = "";
      }
      elseif($data[0]->peak_crp == 3)
      {
        $fuel['rate'] = "3";
        $fuel['con'] = "Okay";
        $fuel['msg'] = "";
      }
    }
    elseif($data[0]->idl_crp == 2)
    {
      if($data[0]->peak_crp == 1)
      {
        $fuel['rate'] = "1";
        $fuel['con'] = "Bad";
        $fuel['msg'] = "";
      }
      elseif($data[0]->peak_crp == 2)
      {
        $fuel['rate'] = "3";
        $fuel['con'] = "Okay";
        $fuel['msg'] = "";
      }
      elseif($data[0]->peak_crp == 3)
      {
        $fuel['rate'] = "4";
        $fuel['con'] = "Okay-Good";
        $fuel['msg'] = "";
      }
    }
    elseif($data[0]->idl_crp == 3)
    {
      if($data[0]->peak_crp == 1)
      {
        $fuel['rate'] = "2";
        $fuel['con'] = "Bad-Okay";
        $fuel['msg'] = "";
      }
      elseif($data[0]->peak_crp == 2)
      {
        $fuel['rate'] = "3";
        $fuel['con'] = "Okay";
        $fuel['msg'] = "";
      }
      elseif($data[0]->peak_crp == 3)
      {
        $fuel['rate'] = "5";
        $fuel['con'] = "Good";
        $fuel['msg'] = "";
      }
    }
    $fuel['msg'] = $data[0]->crpm;

    //backPre
    $backPre = array();
    if($data[0]->ergsmd_ms == 1)
    {
      $backPre['rate'] = "1";
      $backPre['con'] = "Bad";
      $backPre['msg'] = "";
    }
    elseif($data[0]->ergsmd_ms == 2)
    {
      $backPre['rate'] = "2";
      $backPre['con'] = "Bad–Okay";
      $backPre['msg'] = "";
    }
    elseif($data[0]->ergsmd_ms == 3)
    {
      $backPre['rate'] = "3";
      $backPre['con'] = "Okay";
      $backPre['msg'] = "";
    }
    elseif($data[0]->ergsmd_ms == 4)
    {
      $backPre['rate'] = "4";
      $backPre['con'] = "Okay-Good";
      $backPre['msg'] = "";
    }
    elseif($data[0]->ergsmd_ms == 5)
    {
      $backPre['rate'] = "5";
      $backPre['con'] = "Good";
      $backPre['msg'] = "";
    }
    else 
    {
      $backPre['rate'] = "";
      $backPre['con'] = "";
      $backPre['msg'] = $data[0]->ergsmd_ms;
    }

    //combustionPressure
    $copPres = array();
    if($data[0]->comp_pres == 1)
    {
      $copPres['rate'] = "1";
      $copPres['con'] = "Bad";
    }
    elseif($data[0]->comp_pres == 2)
    {
      $copPres['rate'] = "2";
      $copPres['con'] = "Bad–Okay";
    }
    elseif($data[0]->comp_pres == 3)
    {
      $copPres['rate'] = "3";
      $copPres['con'] = "Okay";
    }
    elseif($data[0]->comp_pres == 4)
    {
      $copPres['rate'] = "4";
      $copPres['con'] = "Okay-Good";
    }
    elseif($data[0]->comp_pres == 5)
    {
      $copPres['rate'] = "5";
      $copPres['con'] = "Good";
    }
    else
    {
      $copPres['msg'] = $data[0]->comp_pres;
    }

    //rpmOscillationAnomalyAnalysis
    $rcOcil = array();
    $rcOcil['msg']='';
    if($data[0]->eng_block_ej == 1)
    {
      $rcOcil['rate'] = "1";
      $rcOcil['con'] = "Bad";
    }
    elseif($data[0]->eng_block_ej == 2)
    {
      $rcOcil['rate'] = "2";
      $rcOcil['con'] = "Bad–Okay";
    }
    elseif($data[0]->eng_block_ej == 3)
    {
      $rcOcil['rate'] = "3";
      $rcOcil['con'] = "Okay";
    }
    elseif($data[0]->eng_block_ej == 4)
    {
      $rcOcil['rate'] = "4";
      $rcOcil['con'] = "Okay-Good";
    }
    elseif($data[0]->eng_block_ej == 5)
    {
      $rcOcil['rate'] = "5";
      $rcOcil['con'] = "Good";
    }

    //falseOdometerReading
    $falOdMr = array();
    $falOdMr['rate'] = "";
    $falOdMr['con'] = "";
    $falOdMr['msg'] = $data[0]->falsemeter;
    
    //getVehSpeedSence
    $vehSpeed = array();
    $vehSpeed['rate'] = "";
    $vehSpeed['con'] = "";
    $vehSpeed['msg'] = ($data[0]->vehspeed <= 0) ? "Vehicle Speed Sensor Failure possibility" : "Vehicle Speed Okay";
    
    // cool Temp
    $coolTemp = array();
    $coolTemp['msg']='';
    if($data[0]->coolp == 1)
    {
      $coolTemp['rate'] = "1";
      $coolTemp['con'] = "Bad";
    }
    elseif($data[0]->coolp == 2)
    {
      $coolTemp['rate'] = "2";
      $coolTemp['con'] = "Bad–Okay";
    }
    elseif($data[0]->coolp == 3)
    {
      $coolTemp['rate'] = "3";
      $coolTemp['con'] = "Okay";
    }
    elseif($data[0]->coolp == 4)
    {
      $coolTemp['rate'] = "4";
      $coolTemp['con'] = "Okay-Good";
    }
    elseif($data[0]->coolp == 5)
    {
      $coolTemp['rate'] = "5";
      $coolTemp['con'] = "Good";
    }
    else
    {
      $coolTemp['msg'] = $data[0]->coolp;
    }

 if($copPres['rate'] == 5 || $copPres['rate'] == "")
    {
      $copPres['msg'] = "";
    }
    elseif($copPres['rate'] <= 4)
    {
      if((($turbo['rate']==5 || $air['rate']==5) && $fuel['rate'] ==5 && $rcOcil['rate']<=4) || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] <=0 )  || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] == 5)  || (($turbo['rate']==5 || $air['rate']==5)&& $rcOcil['rate']<=5 && $fuel['rate'] <=4 )  || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] <=4 ))
      {
        if($data[0]->ocilv >= 1)
        {
          $copPres['msg'] = "It appears your engine's combustion pressures are not good. This is attributed to possibly the engine's compression pressure";
        }
        else 
        {
          $n = array();
          if($turbo['rate'] >= 1)
          {
            $n = array($turbo['rate'],$fuel['rate']);
          }
          else {
            $n = array($air['rate'],$fuel['rate']);
          }
          $nVal = min($n);
          
          if($nVal[0] == $nVal[1])
          {
            if($nVal <= 3)
            { 
              $copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your High Pressure Fuel System and/or Air System.";
            }
          }
          else
          {
            if($fuel['rate'] <= 0)
            {
              if($nVal[0] == 5)
              { 
                $copPres['msg'] = "It appears your engine's combustion pressures are not good. We could not check your High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
              }
              else 
              {
                $copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your Air System.";
              }
            }
            else 
            {
              if($nVal[0]>$nVal[1])
              {
                $copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your High Pressure Fuel System.";
              }
              else
              {
                $copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your Air System.";
              }
            }
          }
        }
      }
    }
    
    $arrayData['success'] = TRUE;
    
    $arrayData['batteryHealth'] = array('ratings'=>$batHel['rate'],'condition'=>$batHel['con'],'message'=>$batHel['msg']);
    
    $arrayData['turbochargerHealth'] = array('ratings'=>$turbo['rate'],'condition'=>$turbo['con'],'message'=>$turbo['msg']);
    
    $arrayData['airSystem'] = array('ratings'=>$air['rate'],'condition'=>$air['con'],'message'=>$air['msg']);
    
    $arrayData['fuelSystem'] = array('ratings'=>$fuel['rate'],'condition'=>$fuel['con'],'message'=>$fuel['msg']);
    
    $arrayData['exhaustSystem'] = array('ratings'=>$backPre['rate'],'condition'=>$backPre['con'],'message'=>$backPre['msg']);
    
    $arrayData['combustionPressure'] = array('ratings'=>$copPres['rate'],'condition'=>$copPres['con'],'message'=>$copPres['msg']);
    
    $arrayData['engineBlockInjectors'] = array('ratings'=>$rcOcil['rate'],'condition'=>$rcOcil['con'],'message'=>$rcOcil['msg']);
    
    $arrayData['falseOdometerReadingDetection'] = array('ratings'=>$falOdMr['rate'],'condition'=>$falOdMr['con'],'message'=>$falOdMr['msg']);
    
    $arrayData['coolantTemperature'] = array('ratings'=>$coolTemp['rate'],'condition'=>$coolTemp['con'],'message'=>$coolTemp['msg']);
    
    $arrayData['vehicleSpeedSensorMalfunction'] = array('ratings'=>$vehSpeed['rate'],'condition'=>$vehSpeed['con'],'message'=>$vehSpeed['msg']);
    
  }
  else
  {
    $arrayData['success'] = FALSE;
    $arrayData['errorCode'] = "1002";
    $arrayData['errorMessage'] = "Invalid Device ID!";
  }


       echo json_encode($arrayData);

       }//Public

        public function vehicle_model(Request $request){

         if(!empty($request->data))
         {
            $data = json_decode($request->data, TRUE);
            $mfd = $data['mfd'];
            $res = Fetch::getVehicleSetting();
            $mfdList = explode(';',$res->manufacturer);
            $engCapList = explode(';',$res->engine_capacity);
            if(in_array($mfd, $mfdList))
             {
               $arrayData['success'] = TRUE; 
               $keyMfd = array_search($mfd, $mfdList); 
               $resM = Fetch::getVehicleInfoDataById($keyMfd);
               $fuel = array('','Petrol','Diesel');
                if(count($resM) >= 1)
                  {
                    $arrayData['model'] = explode(';',$resM[0]->model);
                  }
              }
              else
               {
                  $arrayData['success'] = FALSE;
                  $arrayData['errorCode'] = "1002";     
                  $arrayData['errorMessage'] = "Manufacturer is not in the list!";
               }

         }//if(!empty($request->data))
         else
        {
           $arrayData['success'] = FALSE;
           $arrayData['errorCode'] = "1001";     
           $arrayData['errorMessage'] = "Invalid Input!";    
        }
        echo json_encode($arrayData);  
      }//Public

       public function monitor_flag(Request $request){

         if(!empty($request->data))
         {    
             $data = json_decode($request->data, TRUE);
             $deviceID = $data['devID'];
             $devInfo =Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
              $driveNo = Fetch::getLastDriveNum($deviceID);
              // print_r($driveNo);die;
              $arrayData['success'] = TRUE;
              $arrayData['flag'] = 0;
              $arrayData['upcalib'] = 0;
              $arrayData['driveno'] = $driveNo[0]->drive;
            }
            else
            {
              $arrayData['success'] = FALSE;
              $arrayData['errorCode'] = "1002";
              $arrayData['errorMessage'] = "Invalid Device ID!";
            }
          }
          else
          {
              $arrayData['success'] = FALSE;
              $arrayData['errorCode'] = "1001";
              $arrayData['errorMessage'] = "Invalid Input!";

          }
           echo json_encode($arrayData);  
        }  

      public function valcode(Request $request){

         if(!empty($request->data))
         {    
             $data = json_decode($request->data, TRUE);
             $acode = $data['acode'];
             $mac = $data['mac'];
             $uby = $data['uby'];

             $devAct = Fetch::isActivationCodeValid($acode);
            if($devAct[0]->id != "")
                {
                  if($devAct[0]->status == 1)
                  {
                    
                    $saveData = Fetch::updateDataMultiple($devAct[0]->id,$mac,$uby,'2');
                          
                    if($saveData)
                    {
                      $msg= "Activation code updated successfully";
                      $result['success'] = TRUE;
                      $result['message'] = $msg;
                      $result['expDate'] = date('d-m-Y',strtotime($devAct[0]->expdt));
             
                    }
                  }
                  elseif($devAct[0]->status == 2)
                  {
                    if($devAct[0]->mac_id == $mac && $devAct[0]->used_by == $uby)
                    {
                      $table = "activation_code"; 
                     
                              $saveData = Fetch::updateDataMultiple($devAct[0]->id,$mac,$uby,'2');
                              if($saveData)
                              {
                        $msg= "Activation code restored successfully";
                        $result['success'] = TRUE;
                        $result['message'] = $msg;
                        $result['expDate'] = date('d-m-Y',strtotime($devAct[0]->expdt));
                        $expStat = (strtotime($devAct[0]->expdt) < strtotime(date('d-m-Y'))) ? TRUE : FALSE;
                        $result['expStatus'] = $expStat;
                             
                      }
                    }
                    else
                    {
                      $result['success'] = FALSE;
                      $result['errorCode'] = "1003";      
                      $result['errorMessage'] = "Invalid Device!";
                    } 
                  }
                  else
                  {
                    $result['success'] = FALSE;
                    $result['errorCode'] = "1001";      
                    $result['errorMessage'] = "Invalid Activation Code!"; 
                  }
                  
                }
                else
                {
                   $result['success'] = FALSE;
                    $result['errorCode'] = "1001";
                    $result['errorMessage'] = "Invalid Activation Code!";
               } 

          }
         else{
            $result['success'] = FALSE;
            $result['errorCode'] = "1002";      
            $result['errorMessage'] = "Invalid Input!";   
        } 
        echo json_encode($result);  
     }

       public function device_location_drive(Request $request){

         if(!empty($request->data))
         {    
             $data = json_decode($request->data, TRUE);
            
             $deviceID =$data['devID'];
             $devInfo = Fetch::getdevice($deviceID);
             $dev = $deviceID;
             if(!empty($devInfo))
                {
                
                  if(array_key_exists('drivenum',$data))
                  {   
                      $drivenum = $data['drivenum'];
                      $arrayData['success'] = TRUE;
                      $dataLoc = Fetch::devDriveNumDataApi($deviceID,$drivenum);
                      if(count($dataLoc) >= 1)
                      {
                        $arrayData['data'] = $dataLoc;
                      }
                      else
                      {
                        $arrayData['success'] = FALSE;
                        $arrayData['errorCode'] = "1003";
                        $arrayData['errorMessage'] = "No location Info!";
                      }
                    }
                    elseif(array_key_exists('driveidstart',$data) && array_key_exists('driveidend',$data))
                    {
                      $driverangefrm = $data['driveidstart'];
                      $driverangeto = $data['driveidend'];
                      $arrayData['success'] = TRUE;
                      $dataLoc = Fetch::devDriveRangeDataApi($deviceID,$driverangefrm,$driverangeto);
                      if(count($dataLoc) >= 1)
                      {
                        $arrayData['data'] = $dataLoc;
                      }
                      elseif ($driverangefrm > $driverangeto)
                      {
                        $arrayData['success'] = FALSE;
                        $arrayData['errorCode'] = "1002";
                        $arrayData['errorMessage'] = "Invalid Input!";
                      }
                      else
                      {
                        $arrayData['success'] = FALSE;
                        $arrayData['errorCode'] = "1003";
                        $arrayData['errorMessage'] = "No location Info!";
                      }
                    }
                    else
                    {
                      $arrayData['success'] = FALSE;
                      $arrayData['errorCode'] = "1002";
                      $arrayData['errorMessage'] = "Invalid Input!";
                    }
                  }
                    else
                    {
                      $arrayData['success'] = FALSE;
                      $arrayData['errorCode'] = "1001";
                      $arrayData['errorMessage'] = "Invalid Device ID!";
                    }
                  }
                  else
                  {
                      $arrayData['success'] = FALSE;
                      $arrayData['errorCode'] = "1002";
                      $arrayData['errorMessage'] = "Invalid Input!";
                  }
           echo json_encode($arrayData); 
        }

     public function forget_password(Request $request){
             if(!empty($request->data))
         {    
             $data = json_decode($request->data, TRUE);
             $email = $data['email'];
             // $mac = $data['mac'];
             // $uby = $data['uby'];
       // $email = request('email');
       $info  = Fetch::getUserByEmail($email);

       if(empty($info)){
           $msg = "\"$email\" is not a Registered E-Mail Id !";
            $arrayData['success'] = FALSE;
            $arrayData['errorCode'] = "1002";
            $arrayData['errorMessage'] = $msg;
       }else{
        $data  = new \stdClass();
            $data->name = $info[0]->uname;
            $name = $info[0]->uname;
            $data->uid  = $info[0]->uid;
            $uid= $info[0]->uid;
            //print_r( $uid);die;
           $msg = '
           <html>
<head>
  <title>Reset Password Request</title>
</head>
<body>
           <table id="m_9144868195613793263m_-947074545920247478templateContainer" style="border:1px solid #dddddd;background-color:#ffffff" width="600" cellspacing="0" cellpadding="0" border="0">
   <tbody>
      <tr>
         <td valign="top" align="center">
            <table id="m_9144868195613793263m_-947074545920247478templateHeader" width="600" cellspacing="10" cellpadding="10" border="0">
               <tbody>
                  <tr style="background: #eee;">
                     <td>
                        <table align="left">
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="#m_9144868195613793263_m_-947074545920247478_">
                                    <img src="http://www.enginecal.com/wp-content/uploads/2016/08/Logo_Final-174x66.png" class="CToWUd">
                                    </a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                     <td>
                        <table align="right">
                           <tbody>
                              <tr>
                                
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td valign="top" align="center">
            <table id="m_9144868195613793263m_-947074545920247478templateBody" width="600" cellspacing="0" cellpadding="10" border="0">
               <tbody>
                  <tr>
                     <td class="m_9144868195613793263m_-947074545920247478bodyContent" valign="top">
                        <table width="100%" cellspacing="0" cellpadding="10" border="0">
                           <tbody>
                              <tr>
                                 <td valign="top">
                                    <div>
                                       <span class="m_9144868195613793263m_-947074545920247478h3" style="color:#ec1c23;font-weight:bold;font-size:24px">Hi '.$name.',</span>
                                       <br>
                                       <p style="ext-align: justify;
                                              font-style: normal;
                                              font-weight: 500;">
                                          Thank you, Your password change link is:
                                       </p>
                                       <br>
                                          <a href="http://172.105.58.115/enginecal-reset-password/?qugtyhfr='.$uid.'" >Click Here to reset.</a>
                                       <br>
                                    </div>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td valign="top" bgcolor="#E5E5E5" align="center">&nbsp;</td>
      </tr>
      <tr>
         <td valign="top" bgcolor="#E5E5E5" align="center">
            <table width="600" cellspacing="0" cellpadding="3" border="0">
               <tbody>
                  <tr>
                     <td align="center">
                        <img src="https://ci4.googleusercontent.com/proxy/7h4oRb2xmmTOc-HNW3u6sUbUvjzR_ec9YdzagYNwQBADcggdbUjAt9QqD0horKVV8EBc-KkkjYzKyqyPGEBMmu0zuOLIuD_4SX5beaZp7Nje2DKtRoNhjW4Ym83bZDIy1YlbuSuATNJd2S9mVhHK0qFTb1MvnZBrLP8=s0-d-e1-ft#https://gallery.mailchimp.com/3db3451049326f0537a284787/images/8b82aa20-857c-463d-aca8-0bde636d0f9a.png" class="CToWUd">
                     </td>
                  </tr>
                  <tr>
                     <!--<td align="center">
                        <div>You can always reach our call center for questions or comments,<br> they’re there for you – <span class="m_9144868195613793263m_-947074545920247478pink-call"><b style="color:#ec1c23">90360 14226</b></span></div>
                     </td>-->
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td valign="top" bgcolor="#E5E5E5" align="center">&nbsp;</td>
      </tr>
      <tr>
         <td valign="top" bgcolor="#fff" align="center">
            <span class="HOEnZb"><font color="#888888">
            </font></span><span class="HOEnZb"><font color="#888888">
            </font></span>
            <table width="600" cellspacing="10" cellpadding="10" border="0">
               <tbody>
                  <tr>
                     <td>
                        <span class="HOEnZb"><font color="#888888">
                        </font></span><span class="HOEnZb"><font color="#888888">
                        </font></span>
                        <table align="center">
                           <tbody>
                              <tr>
                                 <td>
                                   <p style="font-size: 14px;font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif;margin-top: 7px;">
                                       Copyright 2020 EngineCAL
                                       All right reserved
                                    </p>
                                    <span class="HOEnZb"><font color="#888888">
                                    </font></span>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <span class="HOEnZb"><font color="#888888">
                        </font></span>
                     </td>
                  </tr>
               </tbody>
            </table>
            <span class="HOEnZb"><font color="#888888">
            </font></span>
         </td>
      </tr>
   </tbody>
</table>
</body>
</html>
';
            $from     = 'support@enginecal.com';
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
             
            // Create email headers
            $headers .= 'From: '.$from."\r\n".
                'Reply-To: '.$from."\r\n" .
                'X-Mailer: PHP/' . phpversion();
            // $msg=load('users/resetpassword')->with($data);
            // print_r($graph);die;
            if(mail($email,"Reset Poassword Request",$msg,$headers)){
            // exit;
        // Mail::to(""."mjuttanavar76@gmail.com")->send(new Resetrequest($data));
            $msg = "Password sent successfully!";
            $arrayData['success'] = TRUE;
            $arrayData['message'] = $msg;
          }else{

            $msg = "Mail sending unsuccessful!";
            $arrayData['success'] = FALSE;
            $arrayData['errorCode'] = "1003";
            $arrayData['errorMessage'] = $msg;
          
          }


       }

     }else
          {
            $arrayData['success'] = FALSE;
            $arrayData['errorCode'] = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";
          }
          echo json_encode($arrayData);
    }

      public function statistics_main(Request $request){

         if(!empty($request->data))
         {    
             $data = json_decode($request->data, TRUE);
             $deviceID =$data['devID'];
             $devInfo = Fetch::getdevice($deviceID);
   
             $dev = $deviceID;
             if(!empty($devInfo))
                {
                  $yVal = 0;
                  $arrayData['success'] = TRUE;
                  
                  if($data['type'] == 'serviceDue')
                  {
                     $arrayData['serviceDue'] = 0;
                     $arrayData['serviceDueDays'] = 0;
                  }
              elseif($data['type'] == 'averageMileage')
              {
                $numMil = Fetch::getStatsAverageMilage($deviceID);
                $arrayData['averageMileage'] = $numMil[0]->val;
              }
              elseif($data['type'] == 'averageDistancePerDay')
              {
                $numDays1 = Fetch::getStatsTotalNumDaysDriven($deviceID);
                $retvalue = 0;
                if (trim($numDays1[0]->dt) != "")
                {
                    $retvalue = $numDays1[0]->dt + 0.0;
                }
                $numDays=$retvalue;
                $numDist1 = Fetch::getStatsTotalDistanceDriven($deviceID);
                $retvalue1 = 0;
                 if (trim($numDist1[0]->val) != "")
                   {
                        $retvalue1 = $numDist1[0]->val + 0.0;
                    } 
                $numDist=$retvalue1;     
                $numDistAvg = ($numDays >= 1)  ? $numDist / $numDays : 0;
                $numDist = ($numDist <= 0) ? 0 : $numDist;
                $arrayData['averageDistancePerDay'] = $numDistAvg;
              }
              else
              {
                $arrayData['success'] = FALSE;
                $arrayData['errorCode'] = "1001";
                $arrayData['errorMessage'] = "Invalid Request Type!";
              }
            }

         }
         else
        {
            $arrayData['success'] = FALSE;
            $arrayData['errorCode'] = "1002";
            $arrayData['errorMessage'] = "Invalid Input!";
        }
          echo json_encode($arrayData);
       }

        public function drivescrore_sd(Request $request){

         if(!empty($request->data))
         {
             $data = json_decode($request->data, TRUE);
             $deviceID  = $data['devID'];
             $devInfo = Fetch::getdevice($deviceID);
             
             if(!empty($devInfo))
            {
              $arrayData['success'] = TRUE;
              $datLastScore = Fetch::getRawData($deviceID);
              $drvScore = $datLastScore[0]->mon_ptp_hlth;
              $arrayData['drvScore'] = round($drvScore);
            }
            else
            {
                $arrayData['success'] = FALSE;
                $arrayData['errorCode'] = "1001";
                $arrayData['errorMessage'] = "Invalid Device ID!";
            }

          }
          else
          {
              $arrayData['success'] = FALSE;
              $arrayData['errorCode'] = "1002";
              $arrayData['errorMessage'] = "Invalid Input!";
          }
          echo json_encode($arrayData);

      }

       public function statistics(Request $request){

         if(!empty($request->data))
         {
             $data = json_decode($request->data, TRUE);
             $deviceID  = $data['devID'];
             $devInfo = Fetch::getdevice($deviceID);
             
             if(!empty($devInfo))
            {
              $yVal = 0;
              if($data['type']!= 'date')
              {
                $reqFor = $data['driveno'];
                $durTr = Fetch::getDriveStat($deviceID,$reqFor,'dur','sum');
                $distTr = Fetch::getDriveStat($deviceID,$reqFor,'dist');
                $avSpeed = Fetch::getDriveStat($deviceID,$reqFor,'avg_speed','avg');
                $maxSpeed = Fetch::getDriveStat($deviceID,$reqFor,'max_sp','max');
                $totCon = Fetch::getDriveStat($deviceID,$reqFor,'ful_c','sum');
                $totMil = Fetch::getDriveStat($deviceID,$reqFor,'avg_ml','avg'); // was avg
                $maxTor = Fetch::getDriveStat($deviceID,$reqFor,'max_tor','max');
                //$overRun = $devAct -> overRun(1);
                $fOver = Fetch::getDriveStat($deviceID,$reqFor,'ful_ovr','sum');
                $idT = Fetch::getDriveStat($deviceID,$reqFor,'idt','sum');
                $ovT = Fetch::getDriveStat($deviceID,$reqFor,'dur_ovr','sum');
                //EC added extra parameter for dist in overrun
                $ovD = Fetch::getDriveStat($deviceID,$reqFor,'dist_ovr','sum');
                $hacc = Fetch::getDriveStat($deviceID,$reqFor,'hacc','sum');
                $hdcc = Fetch::getDriveStat($deviceID,$reqFor,'hdcc','sum');
                $ovrspd = Fetch::getDriveStat($deviceID,$reqFor,'ovrspd','sum');
                $vehstall = Fetch::getDriveStat($deviceID,$reqFor,'vehstall','sum');

                $PeakMAF = Fetch::getDriveStat($deviceID,$reqFor,'max_maf','max');
                $PeakTorque  = $maxTor;//$devAct -> driveValue($reqFor,'max_maf');
                $LowestBatteryVoltage = Fetch::getDriveStat($deviceID,$reqFor,'min_batv','min');
                $PeakRailPressure = Fetch::getDriveStat($deviceID,$reqFor,'max_railp','max');
                $PeakMAP = Fetch::getDriveStat($deviceID,$reqFor,'max_map','max');
                $yVal = 1;
              }
            
            elseif($data['type'] == 'date')
              {
                //$dtF = explode(':',$obj['startdate']);
                //$dtT = explode(':',$obj['endtdate']);
                $dtFA = strtotime(str_replace(':', '-', $data['startdate']));
                $dtTA = strtotime(str_replace(':', '-', $data['enddate']));
                $fr = date('Y-m-d',$dtFA);
                $to = date('Y-m-d',$dtTA);
                $arrayData['success'] = TRUE;
                $arrayData['type'] = $data['type'];
                $arrayData['data'] = array();
                //echo "$fr,$to";
                $dtI = 0;
                while($dtFA <= $dtTA)
                {
                  $fr = date('Y-m-d 00:00:00',$dtFA);
                  $to = date('Y-m-d 23:59:59',$dtFA);  // Pick stats for one day and increment at the end for next day
                  $mADt = date('d-m-Y',$dtFA);
                  $arrayData['data'][$dtI] = array();
                  $durTr = Fetch::getDriveStatRange($deviceID,$fr,$to,'dur');
                  $distTr = Fetch::getDriveStatRange($deviceID,$fr,$to,'dist');
                  $avSpeed = Fetch::getDriveStatRange($deviceID,$fr,$to,'avg_speed','avg');
                  $maxSpeed = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_sp','max');
                  $totCon = Fetch::getDriveStatRange($deviceID,$fr,$to,'ful_c');
                  $totMil = Fetch::getDriveStatRange($deviceID,$fr,$to,'avg_ml','avg'); // was avg
                  $maxTor = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_tor','max');
                  //$overRun = $devAct -> overRun(1);
                  $fOver = Fetch::getDriveStatRange($deviceID,$fr,$to,'ful_ovr');
                  $idT = Fetch::getDriveStatRange($deviceID,$fr,$to,'idt');
                  $ovT = Fetch::getDriveStatRange($deviceID,$fr,$to,'dur_ovr');

                  //EC added extra parameter for dist in overrun
                  $ovD = Fetch::getDriveStatRange($deviceID,$fr,$to,'dist_ovr');

                  $PeakMAF = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_maf','max');
                  $PeakTorque  = $maxTor;//$devAct -> driveValuedt($fr,$to,'max_maf');
                  $LowestBatteryVoltage = Fetch::getDriveStatRange($deviceID,$fr,$to,'min_batv','min');
                  $PeakRailPressure = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_railp','max');
                  $PeakMAP = Fetch::getDriveStatRange($deviceID,$fr,$to,'max_map','max');

                  $arrayData['data'][$dtI]['date'] = $mADt;
                  $arrayData['data'][$dtI]['drive_duration'] = round($durTr[0]->val,0);
                  $arrayData['data'][$dtI]['drive_dist'] = round($distTr[0]->val,4);
                  $arrayData['data'][$dtI]['avg_speed'] = round($avSpeed[0]->val,2);
                  $arrayData['data'][$dtI]['max_speed'] = round($maxSpeed[0]->val,0);
                  $arrayData['data'][$dtI]['fuel_consumed'] = round($totCon[0]->val,4);
                  $arrayData['data'][$dtI]['mileage'] = round($totMil[0]->val,4);
                  $arrayData['data'][$dtI]['max_tor'] = round($maxTor[0]->val,2);

                  //EC added extra parameter for dist in overrun
                  //$arrayData['data'][$dtI]['fuel_save_ovr'] = $fOver;
                  $arrayData['data'][$dtI]['dist_ovr'] = round($ovD[0]->val,2);
                  $arrayData['data'][$dtI]['fuel_save_ovr'] = round($fOver[0]->val,3);
                  $arrayData['data'][$dtI]['idle_time'] = round($idT[0]->val,0);
                  $arrayData['data'][$dtI]['time_ovr'] = round($ovT[0]->val,0);

                  $arrayData['data'][$dtI]['peak_maf'] = round($PeakMAF[0]->val,2);
                  $arrayData['data'][$dtI]['peak_tor'] = round($PeakTorque[0]->val,2);
                  $arrayData['data'][$dtI]['low_bat_vol'] = round($LowestBatteryVoltage[0]->val);
                  $arrayData['data'][$dtI]['peak_rail_p'] = round($PeakRailPressure[0]->val,0);
                  $arrayData['data'][$dtI]['peak_map'] = round($PeakMAP[0]->val,0);
                  $dtFA += (60*60*24);
                  $dtI++;
                }
                $yVal = 1;
              }
              if($yVal == 0)
              {
                $arrayData['success'] = FALSE;
                $arrayData['errorCode'] = "1003";
                $arrayData['errorMessage'] = "Invalid Type!";
              }
              else
              {
                if($data['type'] == 'drive')
                {
                  $arrayData['success'] = TRUE;
                  $arrayData['type'] = $data['type'];
                  $arrayData['drive_duration'] = $durTr[0]->val;
                  $arrayData['drive_dist'] = round($distTr[0]->val,4);
                  $arrayData['avg_speed'] = round($avSpeed[0]->val,2);
                  $arrayData['max_speed'] = $maxSpeed[0]->val;
                  $arrayData['fuel_consumed'] = round($totCon[0]->val,4);
                  $arrayData['mileage'] = round($totMil[0]->val,4);
                  $arrayData['max_tor'] = round($maxTor[0]->val,2);

                  //EC added extra parameter for dist in overrun
                  //$arrayData['fuel_save_ovr'] = $fOver;
                  $arrayData['dist_ovr'] = round($ovD[0]->val,2);
                          $arrayData['fuel_save_ovr'] = round($fOver[0]->val,3);
                  $arrayData['idle_time'] = round($idT[0]->val);
                  $arrayData['time_ovr'] = round($ovT[0]->val);
                  $arrayData['peak_maf'] = round($PeakMAF[0]->val,2);
                  $arrayData['peak_tor'] = round($PeakTorque[0]->val,2);
                  $arrayData['low_bat_vol'] = round($LowestBatteryVoltage[0]->val);
                  $arrayData['peak_rail_p'] = round($PeakRailPressure[0]->val);
                  $arrayData['peak_map'] = round($PeakMAP[0]->val);
                          //print_r($arrayData);die;
                }
              }
                      
         }
         else
        {
          $arrayData['success'] = FALSE;
          $arrayData['errorCode'] = "1001";
          $arrayData['errorMessage'] = "Invalid Device ID!";
        }
   }
    else
    {
        $arrayData['success'] = FALSE;
        $arrayData['errorCode'] = "1002";
        $arrayData['errorMessage'] = "Invalid Input!";
    }

echo json_encode($arrayData);
}

         public function vehicle_spec(Request $request){

         if(!empty($request->data))
         {
        $data = json_decode($request->data, TRUE);
         $mfd  = $data['mfd'];
         $mod  = $data['model'];
         $res=Fetch::getVehicleSetting();
         $mfdList = explode(';',$res->manufacturer);
         $engCapList = explode(';',$res->engine_capacity);
         $fuel = array('','Petrol','Diesel');

         if(in_array($mfd, $mfdList))
          {
            $arrayData['success'] = TRUE; 
            $keyMfd = array_search($mfd, $mfdList);
            $resM=Fetch::getVehicleInfoDataById($keyMfd);
            // print_r($resM[0]->var);die;
            $varList = array();
            $modList = array();
            $fuel = array('','Petrol','Diesel');
            if(count($resM) >= 1)
            {
              $varList = explode(';',$resM[0]->var);
              $modList = explode(';',$resM[0]->model);
            }
            if($mod != "")
            {
              if(in_array($mod, $modList))
              {
                $keyMode = array_search($mod, $modList);        
              }
              else
              {
                $modNew++;  
                $keyMode = count($modList);
                $modList[$keyMode] = $mod;
              }
            }

            $dat = Fetch::getVehicleSpecDataByMfdModel($keyMfd,$keyMode);
            $j = 0;
            if(count($dat) >= 1)
            {
              foreach($dat as $data)
              { 
                $arrayData[$mod][$j]['fuelType'] = $fuel[$data->fuel]; 
                $arrayData[$mod][$j]['varients'] = $varList[$data->varient];      
                $arrayData[$mod][$j]['engineCapacity'] = $engCapList[$data->engcap];
                $arrayData[$mod][$j]['bodyType'] = $data->btype;
                $j++;
              }
            }
            else
            {
              $arrayData['success'] = FALSE;
              $arrayData['errorCode'] = "1003";     
              $arrayData['errorMessage'] = "Model is not in the list!";
            } 
        }
          else
          {
            $arrayData['success'] = FALSE;
            $arrayData['errorCode'] = "1002";     
            $arrayData['errorMessage'] = "Manufacturer is not in the list!";
          }
        }
        else
       {
         $arrayData['success'] = FALSE;
         $arrayData['errorCode'] = "1001";     
         $arrayData['errorMessage'] = "Invalid Input!";    
        } 
        echo json_encode($arrayData);  

       }//Public

    public function vehicle(Request $request){
        
         $data = json_decode($request->data, TRUE);
         //print_r($data['veh_basic']['deviceid']);
         $deviceID =$data['veh_basic']['deviceid'];
         $devInfo = Fetch::getdevice($deviceID);
         // print_r($devInfo[0]->device_id);die;
         $errorMF = "";
         if($deviceID != "" && $devInfo != "")
            {
              $did = $devInfo[0]->id;
              $prof=$data['veh_basic'];
              $drvInfo= Fetch::getVehicleById($did);
              $settInfo=Fetch::getVehicleSetting();
              // print_r($devInfo[0]->device_id);die;
              $mfd = explode(';',$settInfo->manufacturer);
              $mfd = array_map('strtolower',$mfd);
              $cap = explode(';',$settInfo->engine_capacity);
              $cap = array_map('strtolower',$cap);
              $drvInfo = ($drvInfo != "") ? $drvInfo : array();
              $drvInfo= array();
              $didd=$devInfo[0]->device_id;
              $mff= $prof['veh_registration'];
             
              $drvInfo['device'] = $devInfo[0]->id;
              $drvInfo['reg_no'] = $prof['veh_registration'];
              $drvInfo['manufact'] = "";
              $myMfd = strtolower(trim($prof['veh_manufacturer']));
              // print_r($drvInfo);die;
              if(in_array($myMfd,$mfd))
              {
                $indexM = array_search($myMfd,$mfd);  
                $drvInfo['manufact'] = $indexM;
              }
              else
              {
                $errorMF = $prof['veh_manufacturer'] . " manufacturer is not available";
              }
              $modelList = array();
              $varlList = array();

              $resM=Fetch::getVehicleInfoDataById($indexM);

              if(count($resM) >= 1)
                  {
                    //$arrayData['varients'] = explode(';',$resM[0]['var']);
                    $modelList = explode(';',$resM[0]->model);
                    $varlList  = explode(';',$resM[0]->var);
                  }

              if(in_array($prof['veh_model'],$modelList))
                  {
                    $indexMod           = array_search($prof['veh_model'],$modelList);  
                    $drvInfo['modelid'] = $indexMod;
                    $drvInfo['model']   = $indexMod;
                  }
                  else
                  {
                    $errorMF = $prof['veh_model'] . " model is not available";
                  }

                  if(in_array($prof['veh_varient'],$varlList))
                  {
                    $indexVar             = array_search($prof['veh_varient'],$varlList); 
                    $drvInfo['varientid'] = $indexVar;
                    $drvInfo['varient']   = $indexVar;
                  }
                  else
                  {
                    $errorMF = $prof['veh_varient'] . " varient is not available";
                  }
                  // print_r($prof);die;
                  $drvInfo['fuel_tpe']        = (trim($prof['fuel_type']) == "Petrol") ? '1' : '2';
                  $drvInfo['manufact_yr']     = $prof['mfg_year'];
                  $drvInfo['travel_purchase'] = $prof['odo'];
                  $drvInfo['eng_cap']         = "";
                  $drvInfo['trans_type']      = "Manual";
                  $myCap                      = strtolower(trim($prof['engine_capacity']));

                  if(in_array($myCap,$cap))
                  {
                    $indexM             = array_search($myCap,$cap);  
                    $drvInfo['eng_cap'] = $indexM;
                  }
                  else
                  {
                    $errorMF .= ($errorMF != "" ) ? ", " : "";
                    $errorMF .= $prof['engine_capacity'] . " engine capacity is not available";
                  }

                  $drvInfo['purchase_dt']  = date('Y-m-d');
                  $drvInfo['last_serv_dt'] = date('Y-m-d');
                  $drvInfo['oil_dt']       = date('Y-m-d');
                  //to add last service kms in multiples of 10000
                  $a = strlen($prof['odo']);
                  if($a >= 5)
                  {
                    $val = round($prof['odo'],-($a - 1));
                  }
                  else
                  {
                    $val = 0;
                  }
                  $drvInfo['travel_service'] = $val;
                  $drvInfo['oil_dist'] = $val;
                  // print_r($drvInfo);die;
                  $table   = "vehicles";

                  if($errorMF != "") {
      $err                       = "Input data Incorrect!";
      $arrayData['success']      = FALSE;
      $arrayData['errorCode']    = "1003";
      $arrayData['errorMessage'] = $err;
    }
    else
    { //echo "01";die;
      if(true)
      {
        // echo "01";die;
        /*$dev = $db->select("select * from users where uemail = '".$drvInfo['uemail']."'");
        if(count($dev)>=1)
        {
          //$err="Email already exist";
          $arrayData['success'] = FALSE;
          $arrayData['errorCode'] = "1005";
          $arrayData['errorMessage'] = "Email already exist!";
        }
        else*/
        
        $saveData = Fetch::insertDbSingle($drvInfo);
        // print_r($saveData);die;
        if($saveData)
        {
          /*$spec = $devAct -> setvehSpec($drvInfo['manufact'],$drvInfo['modelid'],$drvInfo['fuel_tpe'],$drvInfo['varientid'],$drvInfo['eng_cap'],$drvInfo['device'],$drvInfo['manufact_yr'],1);
          $spec = ($spec) ? "Vehicle Specification done" : " Vehicle Specification faild";
          $msg="Data added successfully $spec";
          //echo "<script>alert('$msg')</script>";
          //echo "<script>location.replace('users-view.php')</script>";
          //exit;*/
          $msg                  = "Data updated successfully!";
          $arrayData['success'] = TRUE;
          $arrayData['message'] = $msg;
        }
        else
        {
          $err                       = "Error in adding data";
          $arrayData['success']      = FALSE;
          $arrayData['errorCode']    = "1003";
          $arrayData['errorMessage'] = $err;
        }
      }
    }
  }else{
    $err                       = "Input data Incorrect!";
      $arrayData['success']      = FALSE;
      $arrayData['errorCode']    = "1003";
      $arrayData['errorMessage'] = $err;
  }
             
        echo json_encode($arrayData);
    }

     public function device_location(Request $request){
        if(!empty($request->data)){
         $data = json_decode($request->data, TRUE);
         $deviceID=$data['devID'];
         $devInfo = Fetch::getdevice($deviceID);

                   if($deviceID != "" && $devInfo != "")
            {
              $msg= "Data updated successfully";
              $arrayData['success'] = TRUE;

              $lastLocation =Fetch::locationLastApi($deviceID);
              // print_r($lastLocation[0]->lat);Die;
              if(!empty($lastLocation))
              {
                $arrayData['lat']  = $lastLocation[0]->lat;
                $arrayData['long'] = $lastLocation[0]->lon;
                $arrayData['alt']  = $lastLocation[0]->alt;
                $arrayData['ts']   = $lastLocation[0]->time_s;
              }
              else
              {
                $arrayData['success'] = FALSE;
                $arrayData['errorCode'] = "1003";
                $arrayData['errorMessage'] = "No location Info!";
              }
            } 
          }
            else
              {
                $arrayData['success'] = FALSE;
                $arrayData['errorCode'] = "1003";
                $arrayData['errorMessage'] = "Invalid Input!";
              }
        echo json_encode($arrayData);
    }

public function drived_range(Request $request){
    $data = json_decode($request->data, TRUE);
    $deviceID=$data['devID'];
    $driveStart=$data['driveidstart'];
    $driveEnd=$data['driveidend'];
    $devInfo = Fetch::getdevice($deviceID);
    // $did = $devInfo[0]->id;
    $yVal = 0;
    if(!empty($devInfo)){
           //echo "Mayur";
           $dat1 = Fetch::getStatsByDriveRange($deviceID,$driveStart,$driveEnd);
          // print_r($dat);getStatsByDriveRange_data
           $dat = Fetch::getStatsByDriveRange_data($deviceID,$driveStart,$driveEnd);
           $fr  = 1;
           $to  = $dat1[0]->count;
           $arryI = 0;
           $i=0;
            foreach ($dat as $value){
             
              $reqFor    = $i; //$fr;
              $arrayData[$reqFor] = array();
              $arryI++;
              $dtFA      = $value->dt;
              $dtFA      = strtotime(str_replace(':', '-', $dtFA));
              $date      = date('d:m:Y',$dtFA);
              $durTr     = $value->dur;
              $distTr    = $value->dist;
              $avSpeed   = $value->avg_speed;
              $maxSpeed  = $value->max_sp;
              $totCon    = $value->ful_c;
              $totMil    = $value->avg_ml;
              $maxTor    = $value->max_tor;
              $fOver     = $value->ful_ovr;
              $idT       = $value->idt;
              $ovT       = $value->dur_ovr;
              //EC added extra parameter for dist in overrun
              $ovD       = $value->dist_ovr;
              $hacc      = $value->hacc;
              $hdcc      = $value->hdcc;
              $ovrspd    = $value->ovrspd;
              $vehstall  = $value->vehstall;
              $driveID   = $value->nodrive;
              $timestamp = strtotime($value->dfrm);
              
              // if($timestamp > $durTr)
              // {
              //  $timestamp = $timestamp - $durTr;
              // }
              // else
              // {
              //  //Do nothing
              // }
              
              $arrayData[$reqFor]['driveno']        = $driveID + 0.0;
              $arrayData[$reqFor]['drive_id']       = $driveID + 0.0;
              $arrayData[$reqFor]['date']           = $date;
              $arrayData[$reqFor]['ts']             = $timestamp * 1000;
              $arrayData[$reqFor]['drive_duration'] = round($durTr,0);//$durTr;
              $arrayData[$reqFor]['drive_dist']     = round($distTr,4);
              $arrayData[$reqFor]['avg_speed']      = round($avSpeed,2);
              $arrayData[$reqFor]['max_speed']      = round($maxSpeed,0);//$maxSpeed;
              $arrayData[$reqFor]['fuel_consumed']  = round($totCon,4);
              $arrayData[$reqFor]['mileage']        = round($totMil,4);
              $arrayData[$reqFor]['max_tor']        = round($maxTor,2);
              
              //EC added extra parameter for dist in overrun
              $arrayData[$reqFor]['dist_ovr']       = round($ovD,4);
              $arrayData[$reqFor]['fuel_save_ovr']  = round($fOver,3);
              $arrayData[$reqFor]['idle_time']      = round($idT,0);//$idT;
              $arrayData[$reqFor]['time_ovr']       = round($ovT,0);//$ovT;
              $arrayData[$reqFor]['hacc']           = round($hacc,0);
              $arrayData[$reqFor]['hdcc']           = round($hdcc,0);
              $arrayData[$reqFor]['ovrspd']         = round($ovrspd,0);
              $arrayData[$reqFor]['vehstall']       = round($vehstall,0);
              $i++;
            }
              $myData = array();
              $myData = $arrayData;
              $arrayData = array();
              $arrayData['data'] = $myData;

              $arrayData['success'] = TRUE;
              $arrayData['type']    = "drive";
              $arrayData['count']   = count($myData);
              //print_r($value);echo "</br>";
            
      
           
    }else{
        $arrayData['success'] = FALSE;
          $arrayData['errorCode'] = "1003";
          $arrayData['errorMessage'] = "Invalid Type!";
        }
       // print_r(dd( $arrayData));
          $resultVal = json_encode($arrayData);
           echo json_encode($arrayData);
         // print_r($resultVal);
    }


    public function login(Request $request){
      // echo "i am here";
      
      $response = array();
        // input
      $email    = $request->post('email');
      $password = $request->post('password');
      // print_r($$email);die;
        // varify
        if(empty($email)){
            $response['error'] = '0';
            $response['data']  = 'Email required.';
        }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $response['error'] = '0';
            $response['data']  = 'Enter valid email.';
        }elseif(empty($password)){
            $response['error'] = '0';
            $response['data']  = 'Password required.';
        }else{
        // process
          $password = md5($password);
          $token    = Helper::token1(10);
          $users    = DB::select("select uid,uname,utype,uemail,ucategory,ustatus,(CASE WHEN uemail = '$email' AND upwd != '$password' THEN '1' WHEN uemail != '$email' AND upwd = '$password' THEN '2' WHEN uemail = '$email' AND upwd = '$password' THEN '3' ELSE '4' END) as status from users where uemail = ?",[$email]);
          if(!empty($users)){
               if($users[0]->status == '1'){
                   $response['error'] = "0";
                   $response['data']  = 'Wrong password entered.';
               }elseif($users[0]->status == '2'){
                   $response['error'] = "0";
                   $response['data']  = 'Wrong email entered.';
               }elseif($users[0]->status == '3'){
                  // Login Success
                  $login_data    = array(
                  'user_id'      => $users[0]->uid,
                  'login_token'  => $token,
                  'ip'           => '',
                  'mode'         => 'Mobile',
                  'login_status' => '1'
                  );
                  $login_id = Common::insertGetId('login_session',$login_data);
                  $userdata = array(
                  'uid'          => $users[0]->uid,
                  'utype'        => $users[0]->utype,
                  'uemail'       => $users[0]->uemail,
                  'login_token'  => $token,
                  'ucategory'    => $users[0]->ucategory,
                  'login_status' => '1'
                  );
                  $response['success'] = '1';
                  $response['data']    = $userdata;
               }else{
                   $response['error'] = "0";
                   $response['data']  = 'User not exist.';
               }
            }else{
                   $response['error'] = "0";
                   $response['data']  = 'User not exist.';
            }
        }
        echo json_encode($response);
    }

    public function addUser(Request $request){
           
       $response = array();
       // input
       $user_type = $request->post('user_type');
       $name      = $request->post('name');
       $mobile    = $request->post('mobile');
       $email     = $request->post('email');
       $password  = $request->post('password');
       $address   = $request->post('address');
       $status    = $request->post('status');
       $pd_status = $request->post('partner_device_status');
       $partner   = '0';
       $device    = '0';
       // Varify
       // emil validation
       $exist_email = Fetch::getUserByEmail($email);
       if(!empty($exist_email)){
            $response['error'] = "0";
            $response['data']  = 'User already exist.';
            echo json_encode($response);
            exit;
       }

       if(empty($user_type)){
           $response['error'] = "0";
         $response['data']  = 'User Type required.';
         echo json_encode($response);
         exit;
       }elseif(empty($name)){
           $response['error'] = "0";
         $response['data']  = 'Name required.';
         echo json_encode($response);
         exit;
       }elseif(empty($mobile)){
           $response['error'] = "0";
         $response['data']  = 'Mobile required.';
         echo json_encode($response);
         exit;
       }elseif(empty($email)){
           $response['error'] = "0";
         $response['data']  = 'Email required.';
         echo json_encode($response);
         exit;
       }elseif(empty($password)){
           $response['error'] = "0";
         $response['data']  = 'Password required.';
         echo json_encode($response);
         exit;
       }elseif(empty($address)){
         $response['error'] = "0";
         $response['data']  = 'Address required.';
         echo json_encode($response);
         exit;
       }

       // partner and device validation if selected
       if($pd_status == 1){
            $partner = $request->post('partner');
            $device  = $request->post('device');
            if(empty($partner)){
              $response['error'] = "0";
              $response['data']  = 'Partner field required.';
              echo json_encode($response);
              exit;
            }elseif(empty($device)){
              $response['error'] = "0";
              $response['data']  = 'Device field required.';
              echo json_encode($response);
              exit;
            }
       } 
       // Process

       $category = Fetch::getUserCategoryByUserType($user_type);

       $data = array(
       'utype'     => $user_type,
       'ucategory' => $category[0]->u_category,
       'uname'     => $name,
       'umob'      => $mobile,
       'uemail'    => $email,
       'upwd'      => md5($password),
       'ulocation' => $address,
       'ustatus'   => $status,
       'ad_by'     => '1',
       'partner'   => $partner,
       'device'    => $device
       ); 
       Common::insert('users',$data);
       //$to = "praveen.rao@enginecal.com,support@enginecal.com";
       $response['success'] = "1";
       $response['data']    = 'User created.';
       $data  = new \stdClass();
       $data->name = $name;
       Mail::to("".$email)->send(new Newuser($data));
       echo json_encode($response);
    }

    public function checkValidSub(Request $request){
        $response = array();
        $devId    = $request->get('data');
        $devId    = json_decode($devId);

        if(!empty($devId)){
            $result   = Fetch::getdeviceActStatus($devId->devID);
            if($result[0] != "" && $result[0]->status == 1)
            {
              $response['success'] = TRUE;
              $response['status']  = "1";
              $response['subEnd']  = $result[0]->expdt;
            }
            elseif($result[0] != "" && $result[0]->status == 0)
            {
              $response['success']      = FALSE;
              $response['status']       = "0";
              $response['errorCode']    = "1003";
              $response['errorMessage'] = "Subscription expired!";
            }
            else
            {
              $response['success']      = FALSE;
              $response['errorCode']    = "1001";
              $response['errorMessage'] = "Invalid Device ID!";
            }
        }else{
            $response['success']       = FALSE;
            $response['errorCode']     = "1002";
            $response['errorMessage']  = "Invalid Input!";
        }

        echo json_encode($response);
    }

    public function monitorFlag(Request $request){
        // echo "i am here";
        $response = array();
        $devId    = $request->get('data');
        // print_r($devId);die;
        $devId    = json_decode($devId);

        if(!empty($devId)){
            $devInfo = Fetch::getIdByDeviceId($devId->devID);
            if(!empty($devInfo))
            {
              $result   = Fetch::getLastDriveNum($devId->devID);
              $currStat = $result->fetch_assoc();
              $driveNo  = 0;
              if (trim($currStat['nodrive']) != "")
              {
                  $driveNo = $currStat['nodrive'] + 0;
              }
              $arrayData['success'] = TRUE;
              $arrayData['flag']    = 0;
              $arrayData['upcalib'] = 0;
              $arrayData['driveno'] = $driveNo;
            }
            else
            {
              $response['success']      = FALSE;
              $response['errorCode']    = "1001";
              $response['errorMessage'] = "Invalid Device ID!";
            }
        }else{
            $response['success']       = FALSE;
            $response['errorCode']     = "1002";
            $response['errorMessage']  = "Invalid Input!";
        }

        echo json_encode($response);
    }
}
