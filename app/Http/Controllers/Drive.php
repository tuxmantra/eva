<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Helper;
use App\Model\Common;
use App\Model\Fetch;

class Drive extends Controller
{
    public function index()
    {
        $data_array        = "";
        $user              = session('userdata');
        $data['user_type'] = $user['utype'];
        $deviceList        = Fetch::getDeviceListByUserType($user['uid'],$user['ucategory']);
        $date              = date('m/d/Y');
        if(!empty($deviceList))
        {
            $did                 = $deviceList[0]->device_id;
            if(!empty(session('device_id')))
            {
                $data['did']     = $did = session('device_id');
                $date            = session('date');
            }else
            {       
                $data['did']     = $did;
                session(['date' => $date]);
            }
            // Value Part data fetch
            $driveDates          = Fetch::getDriveDatesByDeviceId($did);
            $data_array = "";
            foreach($driveDates as $sdt){
               $data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',"; 
            }
            $valuePartList     = Fetch::getValuePartList();
        }else{
               $date              = date('Y-m-d');
               $driveDates        = '';
               $valuePartList     = Fetch::getValuePartList();
               $did               ='';
           }

        $data = array(
            'title'            => 'Drive',
            'device_list'      => $deviceList,
            'user_category'    => $user['ucategory'],
            'did'              => $did,
            'today'            => $date,
            'ptype'            => $valuePartList,
            'drive_dates'      => rtrim($data_array,',')
            
        );
        $data['drive'] = '1';
        $data['menu']  = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('drive/index')->with($data);
    }
         
    public function processGraphData($val,$dt='',$dev='',$noData='', $time_s)
    {
        $dt = date('Y-m-d',strtotime($dt));
        if($val != "")
        {
            $fName = "F$val";
            $drF = $dt . " 00:00:00"; // from date since morning
            $drT = $dt . " 23:59:59"; // to date uptill night
            
            // Get graph data from device data table
            // print_r($time_s);
            $graphData = Fetch::getDeviceDataGraph($dev,$drF,$drT,$fName);

            $valA      = array();
            $data1     = array();
            $valB      = array();
            // $valMe     = array();
            $i         = 0;
            foreach($graphData as $data)
            {
                $valAX  = $data->$fName;
                $tM     = strtotime($data->time_v);
                $dtF    = strtotime(date('Y-m-d 00:00:00',$tM )) * 1000;
                $dtT    = strtotime(date('Y-m-d H:i:s',$tM )) * 1000;
                $bal    = $dtT - $dtF;
                $jBal   = $bal;
                $balNew = $data->time_s - $dtT;

                $jBal += ($balNew >= 1) ? $balNew : 0;  
        
                if($i==0 || $valB[$i-1] != $jBal)
                {
                    $epdt = $data->time_s; 
                    
                    $valB[$i] = $epdt;
                    if($epdt == $time_s)
                    {
                        $valMe = array('val'=> $valAX);
                    }
                    else
                    {
                        $valMe = array('val'=> '');
                    }

                    $valA[$i] = $valMe;
                    $i++;
                } //die;
            }
            return array($valMe);
        }
    }

    public function getPramVal($pdode,$adFT='',$adFU='')
    {
        // print_r($pdode);die;
        if($pdode >= 1)
        {   
            $selParam = Fetch::getValuePartById($pdode);
            return $selParam[0];
        }
        else
        {
            if(empty($pdode))
            {
                $ret = array('title'=>'','unit'=>'');
            }
            else
            {
                $ret = array('title'=>$adFT[$pdode],'unit'=>$adFU[$pdode]);
            }
            return $ret;
        }
    }

    public function google_map_key()
    {
        $data = array(
            'title'            => 'EngineCal',
            'mast_open'        => '1',
            'gkey_open'        => '1'
                    
        );
        $data['gapikey']      = Fetch::getgapikeyall(); //google api key
        $data['menu']  = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('drive/gkey')->with($data);

    }

    public function update_gkey(Request $request)
    {
         $update          = $request->post('update'); 
         $key          = $request->post('key');
         // Update key
         if($update == 1)
         { 
              $id          = $request->post('id');
              return $update=Fetch::update_gkey($id,$key);
          }
          // Create new Key
          if($update == 0)
         { 
              $create=Fetch::create_gkey($key);
              return 1;
          }
          if($update == 2)
         { 
              $id          = $request->post('id');
              $create=Fetch::delete_gkey($id);
              return 1;
          }
        

    }

    public function getDriveLoad(Request $request)
    {
        $did          = $request->post('did');
        $dev          = $did;
        $dev          = $request->post('did');
        if(session('device_id') == $dev)
        {
            $dt       = $request->post('date');
            session(['date' => $dt]);
        }else
        {
            $dt                = date('m/d/Y');
            session(['date' => $dt]);
            session(['device_id' => $dev]);
        }
        
        $date         = $dt;
        $date         = date('Y-m-d',strtotime($date));
        // $dt           = $request->post('date');
        $pma          = $request->post('ptype1');
        $pmb          = $request->post('ptype2');
        $pmc          = $request->post('ptype3');
        $stroad       = $request->post('stroad');
        // $getap        = Fetch::getparametr($pma,$pmb,$pmc);
        $pmav = "F".$pma;
        $pmbv = "F".$pmb;
        $pmcv = "F".$pmc;
        if($did == '60000403' || $did == '60000402')
        {
            $driveDates          = Fetch::getDriveDatesByDeviceIdEvent($did);
            $driveLatLong        = Fetch::getDriveLatLongByIdEvent($did,$date);
        }
        else
        {
            $driveLatLong = Fetch::getDriveLatLongById($did,$date,$pmav,$pmbv,$pmcv);
            $driveDates   = Fetch::getDriveDatesByDeviceId($did);
            $getap        = Fetch::getparametr($pma,$pmb,$pmc);
        }
        // $driveLatLong = Fetch::getDriveLatLongById($did,$date,$pmav,$pmbv,$pmcv);
        // $driveDates   = Fetch::getDriveDatesByDeviceId($did);
        $gapikey      = Fetch::getgapikey(); //google api key
        $appicount    = Fetch::updategapicount($gapikey[0]->api_key,$gapikey[0]->count+1); // update google api key count
        $data_array   = "";
        $string       = '';
        $start        = "";
        $start_lat    = '';
        $start_lon    = '';
        $end_lat      = '';
        $end_lon      = '';
        $i            = 0;
        $data_array   = "";
        foreach($driveDates as $sdt)
        {
            $data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',";
        }

        if(!empty($driveLatLong))
        {
                 if($stroad=='1')
                 {
                        foreach($driveLatLong as $key => $list)
                        {
                            if($key == 0)
                            {
                                $start.=  "{lat: '0', lng: '0'}";
                                $start_lat = $list->lat;
                                $start_lon = $list->lon;
                            }
                            if($key == count($driveLatLong)-1)
                            {
                                $end_lat = $list->lat;
                                $end_lon = $list->lon;
                            }

                            
                           
                            if($list->lat !='' && $list->lat !='0')
                            {
                                if($did == '60000403' || $did == '60000402')
                                {
                                   $string1[]   = array(
                                            'title'       => 'Device ID '.$list->device_id,
                                            'lat'         => $list->lat,
                                            'lng'         => $list->lon,
                                            'description' =>'Device ID '.$list->device_id
                                   );
                                            
                                    
                                }
                                else
                                {    $dd = $list->time_v;
                                     $string1[] = array(
                                                        'title'       => 'Device ID '.$list->device_id,
                                                        'lat'         => $list->lat,
                                                        'lng'         => $list->lon,
                                                        'description' => "Date-".$dd."<br>".$getap[0]->title.'-'.$list->$pmav.' '.$getap[0]->unit.'<br>'. $getap[1]->title.'-'.$list->$pmbv.' '. $getap[1]->unit.'<br>'. $getap[2]->title.'-'.$list->$pmcv.' '. $getap[2]->unit
                                                        );
                                }
                            }
                            else
                            {
                               
                            }
                        }
                }else{
                            $count_lat_long      = count($driveLatLong);
                            // echo $count_lat_long;
                            $skip_lat_long       = round($count_lat_long/85);
                            // echo $skip_lat_long;
                            $i  = 1;
                            $ct = 0;
                            foreach($driveLatLong as $key => $list)
                            {
                                if(!empty($list->lat))
                                { 
                                    if($ct == $skip_lat_long )
                                    {
                                        // echo $i."<br>";
                                        $string.=  "$list->lat,$list->lon|";
                                        $ct = 0;
                                    }
                                    $i++;
                                    $ct++;
                                }
                            }
                       
                        $string = rtrim($string,'|');
                        $string = $string;
                        // echo $string;
                        // call google roads api to get exact road lat longs
                        $handle = curl_init();
                        $url = "https://roads.googleapis.com/v1/snapToRoads?path=$string&interpolate=true&key=AIzaSyABmH85QxA6UsfF96kz17AMV5xDOESzPIE";
                        curl_setopt($handle, CURLOPT_URL, $url);
                        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                        $output = curl_exec($handle);
                        curl_close($handle);
                        // echo $output;
                        $latlongs  = json_decode($output);
                        foreach ($latlongs as $key => $value) 
                        {
                            if($key == 'error')
                            {
                                // Error No data exist.

                                 $data     = array(
                                        'did'              => $did,
                                        'drive_latlon'     => '',
                                        'start_point'      => '',
                                        'start_end'        => '',
                                        'drive_dates'      => rtrim($data_array,',')
                                );
                            }else{
                                    if($key == 'warningMessage'){
                                       // Ignore warning string in json from google api.
                                    }else{
                                        foreach ($value as $bey => $val) 
                                        {

                                            if($bey == 'warningMessage')
                                            {
                                              // Ignore warning string in json from google api.
                                            }else{
                                                if($bey == 1)
                                                {
                                                    $start_lat = $val->location->latitude;
                                                    $start_lon = $val->location->longitude;
                                                }
                                                if($bey == count($value)-1)
                                                {
                                                   $end_lat = $val->location->latitude;
                                                   $end_lon = $val->location->longitude;
                                                }
                                                 $string1[] = array(
                                                            'title'       => 'Device ID ',
                                                            'lat'         => $val->location->latitude,
                                                            'lng'         => $val->location->longitude,
                                                            'description' => ""
                                                        );
                                                $string.=  "{lat: ".$val->location->latitude.", lng: ".$val->location->longitude."},";
                                            }
                                            
                                        }
                                        $startEnd =  "['$did Start @ $date', $start_lat, $start_lon, 0],['$did End @ $date', $end_lat, $end_lon, 1]";
                                   }
                                }
                       }
                        // print_r($latlongs->location);die;

                   }
                        if(empty($string1))
                        {
                            $string1[] = array(
                                'title'       => 'Device ID ',
                                'lat'         => '20.9745',
                                'lng'         => '77.5968',
                                'description' => 'No data Available !'
                            );
                        }
                        $data = array(
                            'did'              => $did,
                            'drive_latlon'     => $string1, 
                            'drive_dates'      => rtrim($data_array,','),
                            'gapikey'          => $gapikey[0]->api_key
                        );

        }
        else
        {
            $string1[] = array(
                'title'       => 'Device ID ',
                'lat'         => '20.9745',
                'lng'         => '77.5968',
                'description' => 'No data Available !'
            );
            $start = "{lat: '', lng: ''}";
            
            $data  = array(
                'did'              => $did,
                'drive_latlon'     => $string1,
                
                'drive_dates'      => rtrim($data_array,','),
                'gapikey'          => $gapikey[0]->api_key

            );
        }
        $data['date']         = $dt;
        $data['today']        = $dt;
        $string = rtrim($string,',');
        if($stroad==1){
              $result = view('drive/load')->with($data);
            }else{
                 $result = view('drive/load1')->with($data);
            }
        return $result;
    }
}
?>