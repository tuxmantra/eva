<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\Activationcode;

class Alerts extends Controller
{
    public function index(request $request)
    {
        $user = session('userdata');
        $data['user_type'] = $user['utype'];
        $user_category = $user['ucategory'];
        if (!empty($request->input('tab')))
        {
            $tab = $request->input('tab');
        }
        else
        {
            $tab = '';
        }

        $deviceList = Fetch::getDeviceListByUserType($user['uid'], $user_category);
        if (!empty($deviceList))
        {
            $did = $deviceList[0]->device_id;
        }
        else
        {
            $did = '';
        }

        $alert_list = Fetch::getAlertAll();
        $data = array(
            'title' => 'Alerts',
            'device_list' => $deviceList,
            'user_category' => $user_category,
            'did' => $did,
            'alert_list' => $alert_list,
            'tab' => $tab
        );
        $data['allr_open'] = '1';
        $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata') ['utype']);
        return view('settings/alerts/index')->with($data);
    }

    public function getFailedTest(request $request)
    {
        $failedTest = Fetch::getFailedList(session('userdata')['uid'],session('userdata')['ucategory']);
        $result = '';
        $result = '<div class="material-datatables">
        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
          <thead>
            <tr>
           
              <th class="table-heading-font">S.No</th>
              <th class="table-heading-font">User</th>
              <th class="table-heading-font">Vehicle</th>
              <th class="table-heading-font">VIN No</th>
              <th class="table-heading-font">Date</th>
              <th class="table-heading-font">Operator</th>
              <th class="table-heading-font" width="20%">Result</th>
              <th class="table-heading-font" style = "width: 154px !important;">Actions</th>
            </tr>
          </thead>
          
        <tbody>';
        if (!empty($failedTest))
        {
            $resultc = ""; $id = 1; 
            foreach ($failedTest as $report)
            { 
             
                $user = Fetch::getUserDeviceEmail($report->device_id);
                if($report->test_result){ $test_result = "Passed"; $resultc = '1'; }else{ $test_result = "Failed"; $resultc = '2';}
                $result = $result.'<tr>
             
                  <td class="table-heading-font" style="background-color: #f5f5f5;">'.$id.'</td>
                  <td style="background-color: #ffffff;" class="table-heading-font">'.$user[0]->uname .'</td>
                  <td style="background-color: #f5f5f5;" class="table-heading-font">'.$report->vehicles.'</td>
                  <td style="background-color: #ffffff;" class="table-heading-font">'.$report->vin_no.'</td>
                
                  <td class="table-heading-font" style="background-color: #f5f5f5;;">'.$report->end_time.'</td>
                  <td class="table-heading-font" style="background-color: #ffffff;"> '.$report->rider_name.'</td>
                  <td style="background-color: #f5f5f5;" class="table-heading-font">'.$test_result.'</td>
                  <td class="table-heading-font" style="background-color: #ffffff;">
                  <a data-toggle="tooltip" title="Graph" href="/eva?vin_no='.$report->vin_no.'&devID='.$report->device_id.'&date='.$report->end_time.'&vehicle='.$report->vehicles.'&result='.$resultc.'" class="btn btn-link btn-danger btn-just-icon edit"><i class="material-icons">insights</i></a>
                  <a data-toggle="tooltip" title="Download" target="_blank" href="/testreport/'.$report->id.'/'.$report->device_id.'" class="btn btn-link btn-danger btn-just-icon edit"><i class="material-icons">cloud_download</i></a>
                  
                </tr>';
                $id++;   
            }
        }
        else
        {
            $result= $result.'<tr><td colspan="9" align="center">No test Exist.</td></tr>';
        }
                        
                       
                       
                      $result= $result.'</tbody>
                    </table>
                  </div>
                </div>';
        echo $result;

    }
    public function getIncompleateTest(request $request)
    {
        $failedTest = Fetch::getIncompleateTest(session('userdata')['uid'],session('userdata')['ucategory']);
        $result = '';
        $result = '<div class="material-datatables">
        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
          <thead>
            <tr>
           
              <th class="table-heading-font">S.No</th>
              <th class="table-heading-font">User</th>
              <th class="table-heading-font">Vehicle</th>
              <th class="table-heading-font">VIN No</th>
              <th class="table-heading-font">Date</th>
              <th class="table-heading-font">Operator</th>
              <th class="table-heading-font" width="20%">Status</th>
              <th class="table-heading-font" style = "width: 154px !important;">Actions</th>
            </tr>
          </thead>
          
        <tbody>';
        if (!empty($failedTest))
        {
            $resultc = ""; $id = 1; 
            foreach ($failedTest as $report)
            { 
             
                $user = Fetch::getUserDeviceEmail($report->device_id);
                if($report->test_status){ $test_status = "Completed";}else{ $test_status = "Incomplete";}
                if($report->test_result){ $test_result = "Passed"; $resultc = '1'; }else{ $test_result = "Failed"; $resultc = '2';}
                $result = $result.'<tr>
             
                  <td class="table-heading-font" style="background-color: #f5f5f5;">'.$id.'</td>
                  <td style="background-color: #ffffff;" class="table-heading-font">'.$user[0]->uname.'</td>
                  <td style="background-color: #f5f5f5;" class="table-heading-font">'.$report->vehicles.'</td>
                  <td style="background-color: #ffffff;" class="table-heading-font">'.$report->vin_no.'</td>
                
                  <td class="table-heading-font" style="background-color: #f5f5f5;;">'.$report->end_time.'</td>
                  <td class="table-heading-font" style="background-color: #ffffff;"> '.$report->rider_name .'</td>
                  <td style="background-color: #f5f5f5;" class="table-heading-font">'.$test_status.'</td>
                  <td class="table-heading-font" style="background-color: #ffffff;">
                  <a data-toggle="tooltip" title="Graph" href="/eva?vin_no='.$report->vin_no.'&devID='.$report->device_id.'&date='.$report->end_time.'&vehicle='.$report->vehicles.'&result='.$resultc.'" class="btn btn-link btn-danger btn-just-icon edit"><i class="material-icons">insights</i></a>
                  <a data-toggle="tooltip" title="Download" target="_blank" href="/testreport/'.$report->id.'/'.$report->device_id.'" class="btn btn-link btn-danger btn-just-icon edit"><i class="material-icons">cloud_download</i></a>
                  
                </tr>';
                $id++;   
            }
        }
        else
        {
            $result= $result.'<tr><td colspan="9" align="center">No test Exist.</td></tr>';
        }
                        
                       
                       
                      $result= $result.'</tbody>
                    </table>
                  </div>
                </div>';
        echo $result;

    }
}

