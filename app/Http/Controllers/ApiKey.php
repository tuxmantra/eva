<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
// use Illuminate\Support\Facades\Mail;
// use PDF;

class ApiKey extends Controller
{
	public function index(Request $request)
	{
		// print_r($request->data);die;
		if(!empty($request->data))
        {
        	// print_r($request->data);die;
			$obj = json_decode($request->data,true);	
			$acode = $obj['acode'];
			$mac = $obj['mac'];
			$uby = $obj['uby'];
			$activationcode=Fetch::getactivationcode($acode);
			// print_r(expression)
			if($activationcode[0]->status == 1){
				Fetch::updateactivationcode($mac,$uby,$acode);
				$msg= "Activation code updated successfully";
				$result['success'] = TRUE;
				$result['message'] = $msg;
				$result['expDate'] = date('d-m-Y',strtotime($activationcode[0]->expdt));
			}else{
				$result['success'] = FALSE;
				$result['errorCode'] = "1001";			
				$result['errorMessage'] = "Activation Code Is Already Used!";	
			}
			// print_r($activationcode);die;
        }else
		{
			$result['success'] = FALSE;
			$result['errorCode'] = "1002";			
			$result['errorMessage'] = "Invalid Input!";		
		}	
		$resultVal = json_encode($result);	
		echo $resultVal;
	}
}

?>