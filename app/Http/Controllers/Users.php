<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newuser;
use Helper;
use App\Model\Common;
use App\Model\Fetch;
use App\Mail\Passwordchange;
use App\Mail\Resetrequest;

class Users extends Controller
{
    // load add user page
    public function add(Request $request){
    	$userTypeList      = Fetch::getUserTypeList();
    	$data = array(
	        'title'        => 'Add User',
	        'user_type'    => $userTypeList,
	        'form_token'   => Helper::token(8),
          'device'   => $request->get('device'),
          'partner'  => $request->get('partner')  
      );
      $data['mast_open'] = '1';
      $data['user_open'] = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    	return view('users/adduser')->with($data);
    }
    // save user data 
    public function create(Request $request){
      $token = request('form_token');
    	if($token == session('form_token')){
           session()->forget('form_token');
           $user_type = $request->post('user_type');
           $name      = $request->post('name');
           $mobile    = $request->post('mobile');
           $email     = $request->post('email');
           $password  = $request->post('password');
           $address   = $request->post('address');
           $status    = $request->post('status');
           $pd_status = $request->post('partner_device_status');
          

           // emil validation
           $exist_email    = Fetch::getUserByEmail($email);
           if(!empty($exist_email)){
              \Session::flash('message','Email already exist.');
              return redirect('add-user');
              exit;
           }
           // partner and device validation
           $did  = $request->get('device');
           $partner = $request->get('partner');
           // if(!empty($device)){
           //      $partner = $request->get('partner');
           //      // $device  = $request('device');
           //      if(empty($partner)){
           //        \Session::flash('message','Partner Field required.');
           //        return redirect('add-user');
           //        exit;
           //      }elseif(empty($device)){
           //        \Session::flash('message','Device Field required.');
           //        return redirect('add-user');
           //        exit;
           //      }
           // }

           // Other field validation

           if(empty($user_type) || empty($name) || empty($mobile) || empty($email) || empty($password) || empty($address)){
               \Session::flash('message','Fields required.');
               return redirect('add-user');
               exit;
           }
 
           $category = Fetch::getUserCategoryByUserType($user_type);
           $device = Fetch::getdevicebydid($did);
           $deviced='';
           if(!empty($device)){
            $deviced=$device[0]->id;
           }

           $data = array(
           'utype'     => $user_type,
           'ucategory' => $category[0]->u_category,
           'uname'     => $name,
           'umob'      => $mobile,
           'uemail'    => $email,
           'upwd'      => md5($password),
           'ulocation' => $address,
           'ustatus'   => $status,
           'ad_by'     => session('userdata')['uid'],
           'partner'   => $partner,
           'device'    => $deviced
           ); 

           Common::insert('users',$data);
           
           if($did != ''){

               // Activation Code
               // $devid   = Fetch::getDeviceById($device);
               $token1  = Helper::token1(10);
               $t       = strtotime('+1 year');
               $afteryr = date('Y-m-d', $t);
               $data1   = array(
               'activation_code' => $token1,
               'mac_id'          => $did,
               'used_by'         => $email,
               'status'          => 1,
               'expdt'           => $afteryr,
               'email'           => $email,
               'ad_by'           => session('userdata')['uid']
               );
               $activation_id = Common::insertGetId('activation_code',$data1);
            }
           // Activation Code
           
           //$to = "praveen.rao@enginecal.com,support@enginecal.com";
          $data  = new \stdClass();
          $data->name = $name;
          //Mail::to("".$email)->send(new Newuser($data));

           if($did == ''){
            \Session::flash('message','User Added Successfully.');
              return redirect('add-user');
           }else{
              \Session::flash('message','User Added Successfully, Now Activate User.');
              $act_id = Helper::crypt(1,$activation_id);
              return redirect('edit-active/'.$act_id);
           }
       }else{
       	   session()->forget('form_token');
           \Session::flash('message','Sorry, token expired. Try Again.');
            return redirect('add-user');
       }
    }

    // save operator data 
    public function createOperator(Request $request){
       
         $name      = $request->post('name');
         $status    = $request->post('status');
         $id        = $request->post('id');
         $plant     = $request->post('plant');
        
         $data = array(
         'opr_name'  => $name,
         'opr_id'    => $id,
         'status'   => $status,
         'plant_id'   => $plant
         ); 

         Common::insert('operator',$data);
          \Session::flash('message','Operator Added Successfully.');
          return redirect('operator');
      
    }
    // edit user page load
    public function edit($uid){
      
      $uid = Helper::crypt(2,$uid);
      $userTypeList    = Fetch::getUserTypeList();
    	$userData        = Fetch::getUserById($uid);

    	$data = array(
	        'title'      => 'Edit User',
	        'user_data'  => $userData,
          'user_type'  => $userTypeList,
	        'form_token' => Helper::token(8) 
      );
      $data['mast_open'] = '1';
      $data['user_open'] = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    	return view('users/edituser')->with($data);
    }
    // edit user operator load
    public function editOperator($uid){
      
      $uid = Helper::crypt(2,$uid);
     
      $userData        = Fetch::getOperatorById($uid);

      $data = array(
          'title'      => 'Edit Operator',
          'user_data'  => $userData,
          
          'form_token' => Helper::token(8) 
      );
      $data['mast_open'] = '1';
      $data['user_open'] = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('users/edit-operator')->with($data);
    }

    // edit user data
    public function editUser(Request $request){
      $token     = request('form_token');
      $uuid      = $request->post('uid');
      $uid       = Helper::crypt(2,$uuid);
      if($token == session('form_token')){
           session()->forget('form_token');
           $user_type = $request->post('user_type');
           $name      = $request->post('name');
           $mobile    = $request->post('mobile');
           $email     = $request->post('email');
           $password  = $request->post('password');
           $address   = $request->post('address');
           $status    = $request->post('status');
           $pd_status = $request->post('partner_device_status');
           $partner   = '0';
           $device    = '0';


           // partner and device validation
           if($pd_status == 1){
                $partner = $request->post('partner');
                $device  = $request->post('device');
                if(empty($partner)){
                  \Session::flash('message','Partner Field required.');
                  return redirect('edit-user/'.$uuid);
                  exit;
                }elseif(empty($device)){
                  \Session::flash('message','Device Field required.');
                  return redirect('edit-user/'.$uuid);
                  exit;
                }
           }

           // Other field validation
           if(empty($user_type) || empty($name) || empty($mobile) || empty($email) || empty($password) || empty($address)){
               \Session::flash('message','Fields required.');
               return redirect('edit-user/'.$uuid);
               exit;
           }

           $category = Fetch::getUserCategoryByUserType($user_type);
           // password varify
           $userData        = Fetch::getUserById($uid);
           if($userData[0]->upwd != $password){
                $password = md5($password); 
                    $data = array(
               'utype'     => $user_type,
               'ucategory' => $category[0]->u_category,
               'uname'     => $name,
               'umob'      => $mobile,
               'uemail'    => $email,
               'upwd'      => $password,
               'ulocation' => $address,
               'ustatus'   => $status,
               'partner'   => $partner,
               'device'    => $device
               );
           }else{ 
               $data = array(
               'utype'     => $user_type,
               'ucategory' => $category[0]->u_category,
               'uname'     => $name,
               'umob'      => $mobile,
               'uemail'    => $email,
               'ulocation' => $address,
               'ustatus'   => $status,
               'partner'   => $partner,
               'device'    => $device
               );
           }
           Common::updateTable('users',array('uid' => $uid),$data);
        
           \Session::flash('message','User Updated Successfully.');
            return redirect('user-list');

       }else{
           session()->forget('form_token');
           \Session::flash('message','Sorry, token expired. Try again.');
            return redirect('edit-user/'.$uuid);
       }
    }

    public function updateOperator(Request $request){
      $token     = request('form_token');
      $uuid      = $request->post('id');
      $uid       = Helper::crypt(2,$uuid);
      $name      = $request->post('name');
      $opr_id    = $request->post('opr_id');
      $plant_id  = $request->post('plant');
      $staus     = $request->post('status');
       


   
       $data = array(
       'opr_name'    => $name,
       'opr_id'    => $opr_id,
       'status'    => $staus,
       'plant_id'  => $plant_id
       );
   Common::updateTable('operator',array('id' => $uid),$data);

   \Session::flash('message','Operator Updated Successfully.');
    return redirect('operator');

    }

    // list user data
    public function list(){
      $user                = session('userdata');
    	$userList = Fetch::getUserAll($user['uid'],$user['ucategory']);
    	$data = array(
	        'title'      => 'List User',
	        'user_list'  => $userList
      );
      $data['mast_open'] = '1';
      $data['user_open'] = '1';
      $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    	return view('users/listuser')->with($data);
    }
    // Operator list
    public function operator(){
      $user                = session('userdata');
      $userList = Fetch::getAllOperator($user['uid'],$user['ucategory']);
      $data = array(
          'title'      => 'Operator List',
          'user_list'  => $userList
      );
      $data['mast_open'] = '1';
      $data['operator_open'] = '1';
      $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('users/operator')->with($data);
    }
    // load add user page
    public function addOperator(Request $request){
      $userTypeList      = Fetch::getUserTypeList();
      $data = array(
          'title'        => 'Add Operator',
          'user_type'    => $userTypeList,
          'form_token'   => Helper::token(8),
          'device'   => $request->get('device'),
          'partner'  => $request->get('partner')  
      );
      $data['mast_open'] = '1';
      $data['operator_open'] = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('users/add-operator')->with($data);
    }
    // change user password page load
    public function changepassword(){

    	$data = array(
	        'title'      => 'Change Password',
	        'form_token' => Helper::token(8) 
      );
      $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    	return view('users/changepassword')->with($data);
    }
    // save user password
    public function savepassword()
    {
        $token = request('form_token');
    	if($token == session('form_token')){
           session()->forget('form_token');
           $confir   = request('confir');
           $password = request('password');
           
           if(empty($password || empty($confir))){
		        \Session::flash('message','Fields required.');
		        return redirect('change-password');
           }elseif($password != $confir){
                \Session::flash('message','Password not matching.');
		        return redirect('change-password');
           }
           Common::updateTable('users',array('uid' => session('userdata')['uid']),array('upwd' => md5($password)));
           \Session::flash('message','Password Changed Successfully.');
        $uid   = session('userdata')['uid'];
        $info  = Fetch::getUserById($uid);
        $data  = new \stdClass();
        $data->name = $info[0]->uname;
        Mail::to("".$info[0]->uemail)->send(new Passwordchange($data));

		return redirect('change-password');
        }else{
        	session()->forget('form_token');
           \Session::flash('message','Sorry, token expired. Try again.');
            return redirect('change-password');
        }
    	
    }
    // reset password page load outside login
    public function resetpasswordouter(){
    	$data = array(
	        'title'      => 'Change Password EngineCAL',
	        'form_token' => Helper::token(8) 
      );
      $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
    	return view('users/resetpassword')->with($data);
    }
    // email id got from request from rest page process
    public function resetpasswordrequest(){
    	 $email = request('email');
    	 $info  = Fetch::getUserByEmail($email);

    	 if(empty($info)){
            echo 0;
    	 }else{
    	 	$data  = new \stdClass();
            $data->name = $info[0]->uname;
            $name = $info[0]->uname;
            $data->uid  = $info[0]->uid;
            $uid= $info[0]->uid;
            //print_r( $uid);die;
           $msg = '
           <html>
<head>
  <title>Reset Password Request</title>
</head>
<body>
           <table id="m_9144868195613793263m_-947074545920247478templateContainer" style="border:1px solid #dddddd;background-color:#ffffff" width="600" cellspacing="0" cellpadding="0" border="0">
   <tbody>
      <tr>
         <td valign="top" align="center">
            <table id="m_9144868195613793263m_-947074545920247478templateHeader" width="600" cellspacing="10" cellpadding="10" border="0">
               <tbody>
                  <tr style="background: #eee;">
                     <td>
                        <table align="left">
                           <tbody>
                              <tr>
                                 <td>
                                    <a href="#m_9144868195613793263_m_-947074545920247478_">
                                    <img src="http://www.enginecal.com/wp-content/uploads/2016/08/Logo_Final-174x66.png" class="CToWUd">
                                    </a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                     <td>
                        <table align="right">
                           <tbody>
                              <tr>
                                
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td valign="top" align="center">
            <table id="m_9144868195613793263m_-947074545920247478templateBody" width="600" cellspacing="0" cellpadding="10" border="0">
               <tbody>
                  <tr>
                     <td class="m_9144868195613793263m_-947074545920247478bodyContent" valign="top">
                        <table width="100%" cellspacing="0" cellpadding="10" border="0">
                           <tbody>
                              <tr>
                                 <td valign="top">
                                    <div>
                                       <span class="m_9144868195613793263m_-947074545920247478h3" style="color:#ec1c23;font-weight:bold;font-size:24px">Hi '.$name.',</span>
                                       <br>
                                       <p style="ext-align: justify;
                                              font-style: normal;
                                              font-weight: 500;">
                                          Thank you, Your password change link is:
                                       </p>
                                       <br>
                                          <a href="http://172.105.58.115/enginecal-reset-password/?qugtyhfr='.$uid.'" >Click Here to reset.</a>
                                       <br>
                                    </div>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td valign="top" bgcolor="#E5E5E5" align="center">&nbsp;</td>
      </tr>
      <tr>
         <td valign="top" bgcolor="#E5E5E5" align="center">
            <table width="600" cellspacing="0" cellpadding="3" border="0">
               <tbody>
                  <tr>
                     <td align="center">
                        <img src="https://ci4.googleusercontent.com/proxy/7h4oRb2xmmTOc-HNW3u6sUbUvjzR_ec9YdzagYNwQBADcggdbUjAt9QqD0horKVV8EBc-KkkjYzKyqyPGEBMmu0zuOLIuD_4SX5beaZp7Nje2DKtRoNhjW4Ym83bZDIy1YlbuSuATNJd2S9mVhHK0qFTb1MvnZBrLP8=s0-d-e1-ft#https://gallery.mailchimp.com/3db3451049326f0537a284787/images/8b82aa20-857c-463d-aca8-0bde636d0f9a.png" class="CToWUd">
                     </td>
                  </tr>
                  <tr>
                     <!--<td align="center">
                        <div>You can always reach our call center for questions or comments,<br> they’re there for you – <span class="m_9144868195613793263m_-947074545920247478pink-call"><b style="color:#ec1c23">90360 14226</b></span></div>
                     </td>-->
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
      <tr>
         <td valign="top" bgcolor="#E5E5E5" align="center">&nbsp;</td>
      </tr>
      <tr>
         <td valign="top" bgcolor="#fff" align="center">
            <span class="HOEnZb"><font color="#888888">
            </font></span><span class="HOEnZb"><font color="#888888">
            </font></span>
            <table width="600" cellspacing="10" cellpadding="10" border="0">
               <tbody>
                  <tr>
                     <td>
                        <span class="HOEnZb"><font color="#888888">
                        </font></span><span class="HOEnZb"><font color="#888888">
                        </font></span>
                        <table align="center">
                           <tbody>
                              <tr>
                                 <td>
                                   <p style="font-size: 14px;font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif;margin-top: 7px;">
                                       Copyright 2021 EngineCAL
                                       All right reserved
                                    </p>
                                    <span class="HOEnZb"><font color="#888888">
                                    </font></span>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <span class="HOEnZb"><font color="#888888">
                        </font></span>
                     </td>
                  </tr>
               </tbody>
            </table>
            <span class="HOEnZb"><font color="#888888">
            </font></span>
         </td>
      </tr>
   </tbody>
</table>
</body>
</html>
';
            $from     = 'support@enginecal.com';
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
             
            // Create email headers
            $headers .= 'From: '.$from."\r\n".
                'Reply-To: '.$from."\r\n" .
                'X-Mailer: PHP/' . phpversion();
            // $msg=load('users/resetpassword')->with($data);
            // print_r($graph);die;
            mail($email,"Reset Poassword Request",$msg,$headers);
            // exit;
    	 	// Mail::to(""."mjuttanavar76@gmail.com")->send(new Resetrequest($data));
            echo 1;
    	 }
    }
    // password reset page load from email link
    public function passwordreset(){
    	$data = array(
	        'title'      => 'Change Password EngineCAL',
	        'form_token' => Helper::token(8) 
      );
      $data['menu'] = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
       $data['uid']     = request('qugtyhfr');
    	return view('users/reset')->with($data);
    }
    // reset password save
    public function resetpasswordsave(){
        $password = request('password');
        $uid      = request('qugtyhfr');
    	if(!empty($password)){
          Common::updateTable('users',array('uid' => $uid),array('upwd' => md5($password)));
    	    $info  = Fetch::getUserById($uid);
	        $data  = new \stdClass();
	        $data->name = $info[0]->uname;
	        // Mail::to("".$info[0]->uemail)->send(new Passwordchange($data));
	       return view('users/reset_success');
    	   // exit;
    	}
    	echo 0;
    }
    // ajax request to know category for user add form load partner dropdown if drive
    public function getCategoryPartner(){
        $utype    = request('utype');
        $category = Fetch::getUserCategoryByUserType($utype);
        echo ($category[0]->u_category == '4')? '1':'0';

    }
    // ajax request to load partners dropdown in user add form
    public function getUserPartner(){
         $utype = request('utype');
         $category = Fetch::getUserCategoryByUserType($utype);

         if($category[0]->u_category == '4'){
              $partnerlist = Fetch::getPartnerListById(3);
              if(!empty($partnerlist)){
                  $data = "";
                  $data.= '<select onchange="updatePartnerDevice()" class="selectpicker partner" data-style="select-with-transition" title="Select Partner" data-size="7" id="partner" name="partner" tabindex="-98">
                            <option disabled=""> Partner Options</option>';
                  foreach ($partnerlist as $key => $pl) {
                      $data.= '<option value="'.$pl->uid.'">'.$pl->uname.'</option>';
                  }
                  $data.= '</select>';
                  echo $data;
                  exit;
              }
         }
         echo 0;
    }
    // ajax request to load device id list as per partner dropdown in add user form
    public function getPartnerDevice(){
        $partner     = request('partner');
        $device_list = Fetch::getDeviceListByPartnerId($partner);
        if(!empty($device_list)){
              $data = "";
              $data.= '<select class="selectpicker device" data-style="select-with-transition" data-live-search="true" title="Select Device Id" data-size="7" id="device" name="device" tabindex="-98">
                            <option disabled=""> Device Id Options</option>';
                  foreach ($device_list as $key => $dl) {
                      $data.= '<option value="'.$dl->id.'">'.$dl->device_id.'</option>';
                  }
                  $data.= '</select>';
                  echo $data;
                  exit;
        }
        echo 0;
        
    }
}
