<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;

class Statistics extends Controller
{
    public function index(){
        $user                = session('userdata');
        $data['user_type']   = $user['utype'];
        $deviceList          = Fetch::getDeviceListByUserType($user['uid'],$user['ucategory']);
        if(!empty($deviceList)){
            $did                 = $deviceList[0]->device_id;
            if(!empty(session('device_id')))
            {
                $data['did']     = $did = session('device_id');
            }else
            {       
                $data['did']     = $did;
            }
            $data = array(
                'title'                   => 'Statistics',
                'device_list'             => $deviceList,
                'user_category'           => $user['ucategory'],
                'did'                     => $did
              );
        }else{
          $data['title']            = "Statistics";
          $data['user_category']    = $user['ucategory'];
          $data['service_dates']    = "";
          $data['did'] = "";
        }

        $data['stati_open'] = '1';
        $data['menu']       = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
        return view('statistics/index')->with($data);
    }

    public function getStaticticsLoadCountDays(Request $request){
             
    	$days       = $request->post('days');
    	$did        = $request->post('did');
        if(session('device_id') == $did)
        {
        }
        else
        {
            session(['device_id' => $did]);
        }
    	$status     = $request->post('status');
        
        $sc         = Fetch::getDriveStatHlthScr($did);
	    $drvScore   = !empty($sc[0]->drv_score)? $sc[0]->drv_score:"0"; // drive score
	    $hltScore   = !empty($sc[0]->hlt_score)? $sc[0]->hlt_score:"0"; // health score
    	$durTr      = Fetch::getDriveStat($did,$days,'dur'); // Drive DUration
	    $distTr     = Fetch::getDriveStat($did,$days,'dist'); // Distance Travelled
	    // $avSpeed              = Fetch::getDriveStat($did,$days,'avg_speed','avg'); // Average Speed
	    $sumDataSpd = Fetch::getAvgStatsDrive($did,$days,'avg_speed','dur');
        if(!empty($sumDataSpd))
        {
            $st      = 0;
            $distsum = 0;
            foreach($sumDataSpd as $sum)
            {
                $st      = $st + ($sum->dur * $sum->avg_speed);
                $distsum = $distsum + $sum->dur;
            }
			//Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
			if($distsum == 0)
			{
				$avSpeed = 0;
			}
			else
			{
				$avSpeed = $st/$distsum;
           	 	$avSpeed = round($avSpeed,2);
			}
        }
        else
        {
            $avSpeed = 0;
        }

	    $maxSpeed             = Fetch::getDriveStat($did,$days,'max_sp','max'); // Max Speed
	    $totCon               = Fetch::getDriveStat($did,$days,'ful_c'); // fuel consumed
	    $fOver                = Fetch::getDriveStat($did,$days,'ful_ovr'); // fuel saved in over run
	    // $totMil               = Fetch::getDriveStat($did,$days,'avg_ml','avg'); // Milage
	    $maxTor               = Fetch::getDriveStat($did,$days,'max_tor','max'); // Peak Torque
	    $ovT                  = Fetch::getDriveStat($did,$days,'dur_ovr'); // duration of over run
	    $idT                  = Fetch::getDriveStat($did,$days,'idt'); // Idel Time
	    $PeakMAF              = Fetch::getDriveStat($did,$days,'max_maf','max');
	    $LowestBatteryVoltage = Fetch::getDriveStat($did,$days,'min_batv','min'); // Minimum Voltage
	    $PeakRailPressure     = Fetch::getDriveStat($did,$days,'max_railp','max'); // Peak Rail 
	    $PeakMAP              = Fetch::getDriveStat($did,$days,'max_map','max'); // Peak Map
	    $maxBatteryVoltage    = Fetch::getDriveStat($did,$days,'max_batv','max'); // Maximum VOltage
	    $maxCoolantTemp       = Fetch::getDriveStat($did,$days,'max_cool','max'); // Max Coolent

		$sumDataMil        = Fetch::getAvgStatsDrive($did,$days,'dist','ful_c');
        if(!empty($sumDataMil))
        {
            $fuelConSum = 0;
            $distsum    = 0;
            foreach($sumDataMil as $sum)
            {
                $fuelConSum = $fuelConSum + $sum->ful_c;
                $distsum    = $distsum + $sum->dist;
            }
			//Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
			if($fuelConSum == 0)
			{
				$totMil = 0;
			}
			else
			{
				$totMil = $distsum/$fuelConSum;
            	$totMil = round($totMil,2);
			}
        }
        else
        {
            $totMil = 0;
        }

        $data = array(
	        'durTr'    => $durTr,
	        'distTr'   => $distTr,
	        'hltScore' => $hltScore,
	        'drvScore' => $drvScore,
	        'fuelCon'  => $totCon,
	        'mil'      => $totMil,
	        'flOvrRn'  => $fOver,
	        'durOvrRn' => $ovT,
	        'idlT'     => $idT,
	        'maxSpd'   => $maxSpeed,
	        'avgSpd'   => $avSpeed,
	        'pkTor'    => $maxTor,
	        'pkMap'    => $PeakMAP,
	        'maxCool'  => $maxCoolantTemp,
	        'pkRaPre'  => $PeakRailPressure,
	        'minVolt'  => $LowestBatteryVoltage,
	        'maxVolt'  => $maxBatteryVoltage,
	        'did'      => $did,
	        'utype'    => session('userdata')['utype']
	    );
	    
	    if($status == 1){
           $result = view('statistics/load')->with($data);
	    }elseif($status == 3){
           $result = view('statistics/load3')->with($data);
	    }else{
	       $result = view('statistics/load2')->with($data);
	    }
	    
	    return $result;
    }

    public function getStaticticsLoadRangeDays(Request $request)
    {
        $start = $request->post('start');
        $end   = $request->post('end');
        $did   = $request->post('did');
        if(session('device_id') == $did)
        {
        }
        else
        {
            session(['device_id' => $did]);
        }
        $sc    = Fetch::getDriveStatHlthScr($did);
        if(!empty($sc))
        {
	       $drvScore = $sc[0]->drv_score; // drive score
	       $hltScore = $sc[0]->hlt_score; // health score
	    }
	    else
	    {
	       $drvScore = 0; // drive score
	       $hltScore = 0; // health score
	    }
    	$durTr  = Fetch::getDriveStatRange($did,$start,$end,'dur'); 	// Drive Duration
	    $distTr = Fetch::getDriveStatRange($did,$start,$end,'dist'); 	// Distance Travelled
	    // $avSpeed = Fetch::getDriveStatRange($did,$start,$end,'avg_speed','avg'); // Average Speed
	    $sumDataSpd = Fetch::getAvgStatsDays($did,$start,$end,'avg_speed','dur');
        if(!empty($sumDataSpd))
        {
            $st      = 0;
            $distsum = 0;
            foreach($sumDataSpd as $sum)
            {
                $st      = $st + ($sum->dur * $sum->avg_speed);
                $distsum = $distsum + $sum->dur;
            }
			//Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
			if($distsum == 0)
			{
				$avSpeed = 0;
			}
			else
			{
				$avSpeed = $st/$distsum;
           	 	$avSpeed = round($avSpeed,2);
			}
        }
        else
        {
            $avSpeed = 0;
        }
	    $maxSpeed             = Fetch::getDriveStatRange($did,$start,$end,'max_sp','max'); // Max Speed
	    $totCon               = Fetch::getDriveStatRange($did,$start,$end,'ful_c'); // fuel consumed
	    $fOver                = Fetch::getDriveStatRange($did,$start,$end,'ful_ovr'); // fuel saved in over run
	    // $totMil               = Fetch::getDriveStatRange($did,$start,$end,'avg_ml','avg'); // Milage
	    $maxTor               = Fetch::getDriveStatRange($did,$start,$end,'max_tor','max'); // Peak Torque
	    $ovT                  = Fetch::getDriveStatRange($did,$start,$end,'dur_ovr'); // duration of over run
	    $idT                  = Fetch::getDriveStatRange($did,$start,$end,'idt'); // Idel Time
	    $PeakMAF              = Fetch::getDriveStatRange($did,$start,$end,'max_maf','max');
	    $LowestBatteryVoltage = Fetch::getDriveStatRange($did,$start,$end,'min_batv','min'); // Minimum Voltage
	    $PeakRailPressure     = Fetch::getDriveStatRange($did,$start,$end,'max_railp','max'); // Peak Rail 
	    $PeakMAP              = Fetch::getDriveStatRange($did,$start,$end,'max_map','max'); // Peak Map
	    $maxBatteryVoltage    = Fetch::getDriveStatRange($did,$start,$end,'max_batv','max'); // Maximum VOltage
	    $maxCoolantTemp       = Fetch::getDriveStatRange($did,$start,$end,'max_cool','max'); // Max Coolent 

	    $sumDataMil           = Fetch::getAvgStatsDays($did,$start,$end,'dist','ful_c');
        if(!empty($sumDataMil))
        {
            $fuelConSum = 0;
            $distsum    = 0;
            foreach($sumDataMil as $sum)
            {
                $fuelConSum = $fuelConSum + $sum->ful_c;
                $distsum    = $distsum + $sum->dist;
            }
			//Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
			if($fuelConSum == 0)
			{
				$totMil = 0;
			}
			else
			{
				$totMil = $distsum/$fuelConSum;
            	$totMil = round($totMil,2);
			}
        }
        else
        {
            $totMil   = 0;
        }

	    $data = array(
	        'durTr'    => $durTr,
	        'distTr'   => $distTr,
	        'hltScore' => $hltScore,
	        'drvScore' => $drvScore,
	        'fuelCon'  => $totCon,
	        'mil'      => $totMil,
	        'flOvrRn'  => $fOver,
	        'durOvrRn' => $ovT,
	        'idlT'     => $idT,
	        'maxSpd'   => $maxSpeed,
	        'avgSpd'   => $avSpeed,
	        'pkTor'    => $maxTor,
	        'pkMap'    => $PeakMAP,
	        'maxCool'  => $maxCoolantTemp,
	        'pkRaPre'  => $PeakRailPressure,
	        'minVolt'  => $LowestBatteryVoltage,
	        'maxVolt'  => $maxBatteryVoltage,
	        'did'      => $did,
	        'utype'    => session('userdata')['utype'],
	        'start'    => date('Y-M-d',strtotime($start)),
	        'end'      => date('Y-M-d',strtotime($end)),
        );
	    $result = view('statistics/load3')->with($data);
	    return $result;
    }


    public function loadgraph(Request $request)
    {
        $year   = $request->post('year');
        $month  = $request->post('month');
        $ptype1 = $request->post('ptype1');
        $ptype2 = $request->post('ptype2');
        $did    = $request->post('did');
        if(session('device_id') == $did)
        {
        }
        else
        {
            session(['device_id' => $did]);
        }
        $sc = Fetch::getDriveStatHlthScr($did);
        if(!empty($sc))
        {
	       $drvScore = $sc[0]->drv_score; // drive score
	       $hltScore = $sc[0]->hlt_score; // health score
	    }
	    else
	    {
	       $drvScore = 0; // drive score
	       $hltScore = 0; // health score 
	    }

	    if($ptype1 == 1)
	    {
	    	if($month == 0)
	    	{
	    		$durTr = Fetch::getDriveStatRange1($did,$year,'dur'); // Drive DUration

	    		foreach ($durTr as $key => $value)
	    		{
	    			$date[$value->date] = $value->date;
	    			$val[$value->date]  = round(($value->val/3600),2);
	    		}
	    		$data['lables'] = $date;
	    		$data['values'] = $val;
	    	}
	    	else
	    	{
		    	$durTr = Fetch::getDriveStatRange2($did,$year,$month,'dur');
		    	foreach ($durTr as $key => $value)
		    	{
		    		$date[$value->date] = $value->date;
		    		$val[$value->date]  = round(($value->val/3600),2);
		    	}
		    	$data['lables'] = $date;
		    	$data['values'] = $val;
		    }
	    }

	    if($ptype1 == 2)
	    {
	    	if($month == 0)
	    	{
		    	$durTr = Fetch::getDriveStatRange1($did,$year,'dist'); // Distance Travelled
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
		    		foreach ($durTr as $key => $value)
		    		{
		    			$date[$value->date] = $value->date;
		    			$val[$value->date]  = $value->val;
		    		}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
		    	}
	    	}
	    	else
	    	{
		    	$durTr = Fetch::getDriveStatRange2($did,$year,$month,'dist');
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
			    }
	    	}
	    }
	    
	    if($ptype1 == 3)
	    {
	     	if($month == 0)
	     	{
		    	$durTr = Fetch::getDriveStatRange1($did,$year,'ful_c'); // Distance Travelled
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
		    	}
	    	
	    	}
	    	else
	    	{
				$durTr = Fetch::getDriveStatRange2($did,$year,$month,'ful_c');
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
			    }
	    	}
	    }

	    if($ptype1 == 4)
	    {
        	if($month == 0)
        	{
		    	$sumDataMil = Fetch::getAvgStatsDays1($did,$year,'avg_ml','dist'); // Distance Travelled

		    	if(!empty($sumDataMil))
		    	{
		            $st      = 0;
		            $distsum = 0;
		            foreach($sumDataMil as $sum)
		            {
		                $st      = $st + ($sum->dist * $sum->avg_ml);
		                $distsum = $distsum + $sum->dist;
		                if($distsum == 0)
						{
							$totMil[$sum->date] = 0;
						}
						else
						{
							$totMil1            = $st/$distsum;
			            	$totMil[$sum->date] = round($totMil1,2);
						}
					    $date[$sum->date] = $sum->date;
		            }
					//Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
		        }
		        else
		        {
		            $totMil['0'] = 0;
		            $date['0']   = 0;
		        }
		    	$data['lables'] = $date;
		    	$data['values'] = $totMil;
		    }
		    else
		    {
		    	$sumDataMil = Fetch::getAvgStatsDays2($did,$year,$month,'avg_ml','dist');
		    	if(!empty($sumDataMil))
		    	{
		            $st      = 0;
		            $distsum = 0;
		            foreach($sumDataMil as $sum)
		            {
		                $st      = $st + ($sum->dist * $sum->avg_ml);
		                $distsum = $distsum + $sum->dist;
		                if($distsum == 0)
						{
							$totMil[$sum->date] = 0;
						}
						else
						{
							$totMil1            = $st/$distsum;
			            	$totMil[$sum->date] = round($totMil1,2);
						}
					    $date[$sum->date]=$sum->date;
		            }
					//Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
	            }
	            else
	            {
		            $totMil['0'] = 0;
		            $date['0']   = 0;
	            }
		    	$data['lables'] = $date;
		    	$data['values'] = $totMil;
		    }
	    }

	    if($ptype1 == 5)
	    {
	    	if($month == 0)
	    	{
		    	$durTr = Fetch::getDriveStatRange1($did,$year,'ful_ovr'); // Distance Travelled
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
			    }
	    	}
	    	else
	    	{
		    	$durTr = Fetch::getDriveStatRange2($did,$year,$month,'ful_ovr');
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
			    }
		    }
	    }

	    if($ptype1 == 6)
	    {
	    	if($month == 0)
	    	{
		    	$durTr = Fetch::getDriveStatRange1($did,$year,'dur_ovr'); // Distance Travelled
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
			    }
		    }
		    else
		    {

		    	$durTr = Fetch::getDriveStatRange2($did,$year,$month,'dur_ovr');
		    	if(empty($durTr)){
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}else{
			    	foreach ($durTr as $key => $value) {
			    		
			    		$date[$value->date]=$value->date;
			    		$val[$value->date]=$value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
			    }
		    }
	    }

	    if($ptype1 == 7)
	    {
	    	// $idT                  = Fetch::getDriveStatRange($did,$start,$end,'idt'); // Idel Time
	    	if($month == 0)
	    	{
		    	$durTr = Fetch::getDriveStatRange1($did,$year,'idt'); // Distance Travelled
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
			    }
		    }
		    else
		    {
	    		$durTr = Fetch::getDriveStatRange2($did,$year,$month,'idt');
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value) {
			    		
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
			    }
		    }
	    }

	    if($ptype1 == 8)
	    {
	    	// $maxSpeed             = Fetch::getDriveStatRange($did,$start,$end,'max_sp','max'); // Max Speed
	    	if($month == 0)
	    	{
		    	$durTr = Fetch::getDriveStatRange1($did,$year,'max_sp','max'); // Distance Travelled
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
			    }
		    }
		    else
		    {
		    	$durTr = Fetch::getDriveStatRange2($did,$year,$month,'max_sp','max');
		    	if(empty($durTr))
		    	{
		    		$data['lables'] = '0';
		    	    $data['values'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables'] = $date;
			    	$data['values'] = $val;
			    }
		    }
	    }

	    if($ptype1 == 9)
	    {
	    	// $sumDataSpd           = Fetch::getAvgStatsDays($did,$start,$end,'avg_speed','dur');
	    	if($month == 0)
	    	{
	    		$sumDataSpd = Fetch::getAvgStatsDays1($did,$year,'avg_speed','dur');
		        if(!empty($sumDataSpd))
		        {
		            $st      = 0;
		            $distsum = 0;
		            foreach($sumDataSpd as $sum)
		            {
		            	$date[$value->date] = $value->date;
		                $st                 = $st + ($sum->dur * $sum->avg_speed);
		                $distsum            = $distsum + $sum->dur;
		                if($distsum == 0)
						{
							$avSpeed[$value->date] = 0;
						}
						else
						{
							$avSpeed1              = $st/$distsum;
			           	 	$avSpeed[$value->date] = round($avSpeed1,2);
						}
		            }
					//Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
		        }
		        else
		        {
		            $avSpeed['0'] = 0;
		            $date['0']	  = 0;
		        }
    		}
    		else
    		{
    	   		$sumDataSpd = Fetch::getAvgStatsDays2($did,$year,$month,'avg_speed','dur');
	        	if(!empty($sumDataSpd))
	        	{
		            $st         = 0;
		            $distsum    = 0;
		            foreach($sumDataSpd as $sum)
		            {
		            	$date[$sum->date] = $sum->date;
		                $st               = $st + ($sum->dur * $sum->avg_speed);
		                $distsum          = $distsum + $sum->dur;
		                if($distsum == 0)
						{
							$avSpeed[$sum->date] = 0;
						}
						else
						{
							$avSpeed1 = $st/$distsum;
			           	 	$avSpeed[$sum->date] = round($avSpeed1,2);
						}
	            	}
					//Mileage = Sum of (each (mileage * dist)) / (Sum of all dist)
	        	}
	        	else
	        	{
		            $avSpeed['0'] = 0;
		            $date['0']	  = 0;
		        }
		    }
            $data['lables'] = $date;
	    	$data['values'] = $avSpeed;
	   	}

//par 2
	    if($ptype2 == 1)
	    {
	    	if($month == 0)
	    	{
		    	$durTr = Fetch::getDriveStatRange1($did,$year,'dur'); // Drive DUration
		    	if(empty($durTr))
		    	{
		    		$data['lables2'] = '0';
		    	    $data['values2'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = round(($value->val/3600),2);
			    	}
			    	$data['lables2'] = $date;
			    	$data['values2'] = $val;
			    }
		    }
		    else
		    {
		    	$durTr = Fetch::getDriveStatRange2($did,$year,$month,'dur');
		    	if(empty($durTr))
		    	{
		    		$data['lables2'] = '0';
		    	    $data['values2'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = round(($value->val/3600),2);
			    	}
			    	$data['lables2'] = $date;
			    	$data['values2'] = $val;
			    }
		    }
	    }

	    if($ptype2 == 2){
	    	if($month == 0)
	    	{
		    	$durTr = Fetch::getDriveStatRange1($did,$year,'dist'); // Distance Travelled
		    	if(empty($durTr))
		    	{
		    		$data['lables2'] = '0';
		    	    $data['values2'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables2'] = $date;
			    	$data['values2'] = $val;
		    	}
		    }
		    else
		    {
		    	$durTr = Fetch::getDriveStatRange2($did,$year,$month,'dist');
		    	if(empty($durTr))
		    	{
		    		$data['lables2'] = '0';
		    	    $data['values2'] = '0';
		    	}
		    	else
		    	{
			    	foreach ($durTr as $key => $value)
			    	{
			    		$date[$value->date] = $value->date;
			    		$val[$value->date]  = $value->val;
			    	}
			    	$data['lables2'] = $date;
			    	$data['values2'] = $val;
			    }
		    }
	    }
	    
	    if($ptype2 == 3)
	    {
	     	if($month==0)
	     	{
		    	$durTr = Fetch::getDriveStatRange1($did,$year,'ful_c'); // Distance Travelled
		    	if(empty($durTr))
		    	{
		    	    $data['lables2'] = '0';
		    	    $data['values2'] = '0';
		    	}
		    	else
		    	{
		    		foreach ($durTr as $key => $value) 
		    		{
		    		
		    			$date[$value->date] = $value->date;
		    			$val[$value->date]  = $value->val;
		    		}
		    		$data['lables2'] = $date;
		    		$data['values2'] = $val;
		       }
	    	
	       }
	       else
	       {

	    	 	$durTr                = Fetch::getDriveStatRange2($did,$year,$month,'ful_c');
	    		if(empty($durTr))
	    		{
	    	    	$data['lables2'] = '0';
	    	    	$data['values2'] = '0';
	    		}
	    		else
	    		{
	    	    	foreach ($durTr as $key => $value) 
	    	    	{
	    		
	    				$date[$value->date] = $value->date;
	    				$val[$value->date]  = $value->val;
	    	    	}
	    			$data['lables2'] = $date;
	    			$data['values2'] = $val;
	    		}

	    	}

	    }

	    if($ptype2 == 4)
	    {
        	if($month==0)
        	{
	    		$sumDataMil = Fetch::getAvgStatsDays1($did,$year,'avg_ml','dist');// Distance Travelled

	    	 	if(!empty($sumDataMil))
	    	 	{
            		$st         = 0;
            		$distsum    = 0;
            		foreach($sumDataMil as $sum)
            		{
                		$st = $st + ($sum->dist * $sum->avg_ml);
                		$distsum = $distsum + $sum->dist;
                		if($distsum == 0)
						{
							$totMil[$sum->date] = 0;
						}
						else
						{
							$totMil1   = $st/$distsum;
            				$totMil[$sum->date]   = round($totMil1,2);
						}
			    		$date[$sum->date]=$sum->date;
            		}
            	}
            	else 
            	{
            		$totMil['0']   = 0;
             		$date['0']     =0;
            	}

	    		$data['lables2'] = $date;
	    		$data['values2'] = $totMil;
	    	
	    	}
	    	else
	    	{

	    		$sumDataMil = Fetch::getAvgStatsDays2($did,$year,$month,'avg_ml','dist');
	    	 	if(!empty($sumDataMil))
	    	 	{
            		$st         = 0;
            		$distsum    = 0;
            		foreach($sumDataMil as $sum)
            		{
                		$st = $st + ($sum->dist * $sum->avg_ml);
                		$distsum = $distsum + $sum->dist;
                		if($distsum == 0)
						{
							$totMil = 0;
						}
						else
						{
							$totMil1   = $st/$distsum;
            				$totMil  = round($totMil1,2);
						}
						$tt[$sum->date]=$totMil;
			    		$date[$sum->date]=$sum->date;
            		}
            	}
            	else 
            	{
            		$totMil['0']   = 0;
            		$date['0']     =0;
            	}
		    	$data['lables2'] = $date;
		    	$data['values2'] = $tt;

	    	}

	    }

	    if($ptype2 == 5)
	    {
	    	if($month==0)
	    	{
	    		$durTr = Fetch::getDriveStatRange1($did,$year,'ful_ovr'); // Distance Travelled
	    		if(empty($durTr))
	    		{
	    			$data['lables2'] = '0';
	    	    	$data['values2'] = '0';
	    		}
	    		else
	    		{
	    			foreach ($durTr as $key => $value) 
	    			{
	    				$date[$value->date]=$value->date;
	    				$val[$value->date]=$value->val;
	    			}
	    			$data['lables2'] = $date;
	    			$data['values2'] = $val;
	    		}
	    	}
	    	else
	    	{
	    		$durTr = Fetch::getDriveStatRange2($did,$year,$month,'ful_ovr');
	    		if(empty($durTr))
	    		{
	    			$data['lables2'] = '0';
	    	    	$data['values2'] = '0';
	    		}
	    		else
	    		{
	    			foreach ($durTr as $key => $value) 
	    			{
	    				$date[$value->date]=$value->date;
	    				$val[$value->date]=$value->val;
	    			}
	    			$data['lables2'] = $date;
	    			$data['values2'] = $val;
	    		}

	    	}

	    }
	    if($ptype2 == 6)
	    {
	    	if($month==0)
	    	{
	    		$durTr = Fetch::getDriveStatRange1($did,$year,'dur_ovr'); // Distance Travelled
	    		if(empty($durTr))
	    		{
	    			$data['lables2'] = '0';
	    	    	$data['values2'] = '0';
	    		}
	    		else
	    		{
	    			foreach ($durTr as $key => $value) 
	    			{
	    				$date[$value->date]=$value->date;
	    				$val[$value->date]=$value->val;
	    			}
	    			$data['lables2'] = $date;
	    			$data['values2'] = $val;
	    		}
	    	}
	    	else
	    	{
	    		$durTr                = Fetch::getDriveStatRange2($did,$year,$month,'dur_ovr');
	    		if(empty($durTr))
	    		{
	    			$data['lables2'] = '0';
	    	    	$data['values2'] = '0';
	    		}
	    		else
	    		{
	    			foreach ($durTr as $key => $value) 
	    			{
	    				$date[$value->date]=$value->date;
	    				$val[$value->date]=$value->val;
	    			}
	    			$data['lables2'] = $date;
	    			$data['values2'] = $val;
	    		}

	    	}

	    }
	    if($ptype2 == 7)
	    {
	    	if($month==0)
	    	{
	    		$durTr = Fetch::getDriveStatRange1($did,$year,'idt'); // Distance Travelled
	    		if(empty($durTr))
	    		{
	    			$data['lables2'] = '0';
	    	    	$data['values2'] = '0';
	    		}
	    		else
	    		{
	    			foreach ($durTr as $key => $value) 
	    			{
	    				$date[$value->date]=$value->date;
	    				$val[$value->date]=$value->val;
	    			}
	    			$data['lables2'] = $date;
	    			$data['values2'] = $val;
	    		}
	    	
	    	}
	    	else
	    	{
	    		$durTr = Fetch::getDriveStatRange2($did,$year,$month,'idt');
	    		if(empty($durTr))
	    		{
	    			$data['lables2'] = '0';
	    	    	$data['values2'] = '0';
	    		}
	    		else
	    		{
	    			foreach ($durTr as $key => $value) 
	    			{
	    				$date[$value->date]=$value->date;
	    				$val[$value->date]=$value->val;
	    			}
	    			$data['lables2'] = $date;
	    			$data['values2'] = $val;
	   			}

	    	}

	    }
	    if($ptype2 == 8)
	    {
	    	if($month==0)
	    	{
	    		$durTr = Fetch::getDriveStatRange1($did,$year,'max_sp','max'); // Distance Travelled
	    		if(empty($durTr))
	    		{
	    			$data['lables2'] = '0';
	    	    	$data['values2'] = '0';
	    		}
	    		else
	    		{
	    			foreach ($durTr as $key => $value) 
	    			{
	    				$date[$value->date]=$value->date;
	    				$val[$value->date]=$value->val;
	    			}
	    			$data['lables2'] = $date;
	    			$data['values2'] = $val;
	    		}
	    	
	    	}
	    	else
	    	{
	    		$durTr = Fetch::getDriveStatRange2($did,$year,$month,'max_sp','max');
	    		if(empty($durTr))
	    		{
	    			$data['lables2'] = '0';
	    	    	$data['values2'] = '0';
	    		}
	    		else
	    		{
	    			foreach ($durTr as $key => $value) 
	    			{
	    				$date[$value->date]=$value->date;
	    				$val[$value->date]=$value->val;
	    			}
	    			$data['lables2'] = $date;
	    			$data['values2'] = $val;
	    		}
	    	}

	    }
	    if($ptype2 == 9)
	    {
	    	if($month==0)
	    	{
	    		$sumDataSpd = Fetch::getAvgStatsDays1($did,$year,'avg_speed','dur');
        		if(!empty($sumDataSpd))
        		{
            		$st         = 0;
            		$distsum    = 0;
            		foreach($sumDataSpd as $sum)
            		{
            			$date[$value->date]=$value->date;
                		$st = $st + ($sum->dur * $sum->avg_speed);
                		$distsum = $distsum + $sum->dur;
                		if($distsum == 0)
						{
							$avSpeed[$value->date] = 0;
						}
						else
						{
							$avSpeed1 = $st/$distsum;
			           	 	$avSpeed[$value->date] = round($avSpeed1,2);
						}
            		}
			
        		}
        		else 
        		{
            		$avSpeed['0']    = 0;
            		$date['0']	=0;
        		}
    		}
    		else
    		{
    	    	$sumDataSpd = Fetch::getAvgStatsDays2($did,$year,$month,'avg_speed','dur');
        		if(!empty($sumDataSpd))
        		{
            		$st         = 0;
            		$distsum    = 0;
            		foreach($sumDataSpd as $sum)
            		{
		            	$date[$sum->date]=$sum->date;
		                $st = $st + ($sum->dur * $sum->avg_speed);
		                $distsum = $distsum + $sum->dur;
                		if($distsum == 0)
						{
							$avSpeed[$sum->date] = 0;
						}
						else
						{
							$avSpeed1 = $st/$distsum;
			           	 	$avSpeed[$sum->date] = round($avSpeed1,2);
						}
            		}
			
        		}
        		else 
        		{
            		$avSpeed['0']    = 0;
            		$date['0']	=0;
        		}
    		}
            $data['lables2'] = $date;
	    	$data['values2'] = $avSpeed;
	    }

	     //year cal
	    $data['mak1'] ="val1";
        $data['mak2'] ="val2";

	    if($ptype1==1 )
	    {
        	$data['mak1'] ="Drive duration (Hr)";
        }
        if($ptype1==2 )
        {
        	$data['mak1'] ="Distance travelled (km)";
          // $data['mak2'] ="Distance travelled";
        }
        if($ptype1==3 )
        {
        	$data['mak1'] ="Fuel consumed (L)";
          // $data['mak2'] ="Fuel consumed";
        }
        if($ptype1==4 )
        {
        	$data['mak1'] ="Mileage (km/L)";
          // $data['mak2'] ="Mileage(km/L)";
        }
        if($ptype1==5 )
        {
        	$data['mak1'] ="Fuel saved in Overrun (L)";
          // $data['mak2'] ="Fuel saved in Overrun";
        }
        if($ptype1==6 )
        {
        	$data['mak1'] ="Duration of Overrun (s)";
          // $data['mak2'] ="Duration of Overrun";
        }
        if($ptype1==7 )
        {
          	$data['mak1'] ="Idle time (s)";
          // $data['mak2'] ="Idle time";
        }
        if($ptype1==8)
        {
         	$data['mak1'] ="Maximum speed (km/hr)";
          // $data['mak2'] ="Maximum speed";
        }
        if($ptype1==9)
        {
         	$data['mak1'] ="Average speed (km/hr)";
          // $data['mak2'] ="Average speed";
        }
        if($ptype1==10)
        {
        	$data['mak1'] ="Drive duration (s)";
          // $data['mak2'] ="Drive duration";
        }
// val 2
        if($ptype2==1 )
        {
          	$data['mak2'] ="Drive duration (Hr)";
          // $data['mak2'] ="Drive duration";
        }
        if($ptype2==2 )
        {
          	$data['mak2'] ="Distance travelled (km)";
          // $data['mak2'] ="Distance travelled";
        }
        if($ptype2==3 )
        {
          	$data['mak2'] ="Fuel consumed (L)";
          // $data['mak2'] ="Fuel consumed";
        }
        if($ptype2==4 )
        {
         	$data['mak2'] ="Mileage (km/L)";
          // $data['mak2'] ="Mileage(km/L)";
        }
        if($ptype2==5 )
        {
          	$data['mak2'] ="Fuel saved in Overrun (L) ";
          // $data['mak2'] ="Fuel saved in Overrun";
        }
        if($ptype2==6 )
        {
          	$data['mak2'] ="Duration of Overrun (s)";
          // $data['mak2'] ="Duration of Overrun";
        }
        if($ptype2==7 )
        {
          	$data['mak2'] ="Idle time (s)";
          // $data['mak2'] ="Idle time";
        }
        if($ptype2==8)
        {
         	$data['mak2'] ="Maximum speed (km/hr)";
          // $data['mak2'] ="Maximum speed";
        }
        if($ptype2==9)
        {
         	$data['mak2'] ="Average speed (km/hr)";
          // $data['mak2'] ="Average speed";
        }
        if($ptype2==10)
        {
          	$data['mak2'] ="Drive duration (s)";
          // $data['mak2'] ="Drive duration";
        }
        $date11=$data['lables'];
	    $avSpeed11=$data['values'];
	    $avSpeed22=$data['values2'];
	    if($date11 == 0)
	    {
	        $date1['0']='0';
	    }
	    else
	    {
	        $date1=$date11;
	    }
	    if($avSpeed11 == 0)
	    {
	        $avSpeed1['0']='0';
	    }
	    else
	    {
	        $avSpeed1=$avSpeed11;
	    }
	    if($avSpeed22 == 0)
	    {
	        $avSpeed2['0']='0';
	    }
	    else
	    {
	        $avSpeed2=$avSpeed22;
	    }
	         // print_r($date1); die;
	    $currentYear = date('Y');	
	    if($month ==0 )
	    {
        	$data['yLabel'] = "Month";
        	$count=12;
	        if($year == $currentYear)
	        {
	         	$count=date('m');
	        }
	        for($i=1;$i<=$count;$i++)
	        {
	        	if(array_key_exists($i, $date1) )
	        	{
	            	$date1[$i]=date("M", mktime(0, 0, 0, $date1[$i], 10));
	          	    $date1[$i]=$date1[$i];
	          	    $avSpeed1[$i]=round($avSpeed1[$i],2);
	      	      	if(!empty($avSpeed2[$i]))
	      	      	{
	      	    		$avSpeed2[$i]=round($avSpeed2[$i],2);
	      			}
	      			else
	      			{
	      				$avSpeed2[$i]='0';
	      			}
	          	}
	          	else
	          	{
		          	$date1[$i]=date("M", mktime(0, 0, 0, $i, 10));
		          	$avSpeed1[$i]='0';
		          	$avSpeed2[$i]='0';
	          
	          	}
	        
	        }// for loop



      	}
      	else
      	{
      		$data['yLabel']  = "Date";
      		$dateString      = $year.'-'.$month.'-10'; 
      		$lastDateOfMonth = date("Y-m-t", strtotime($dateString));
        	$arr             = explode("-", $lastDateOfMonth);
        	$count           = $arr[2];
        	if($month == date('m') && $year == $currentYear)
        	{ 
        	 	$count = date('d');
        	}
        
        	for($i=1;$i<=$count;$i++)
        	{
          		if (array_key_exists($i, $date1))
          		{
	            	$date1[$i]    = $date1[$i];
	          	    $avSpeed1[$i] = round($avSpeed1[$i],2);
          	      	if(!empty($avSpeed2[$i]))
          	      	{
          	    		$avSpeed2[$i] = round($avSpeed2[$i],2);
          			}
          			else
          			{
          				$avSpeed2[$i] = '0';
          			}
          		}
          		else
          		{
		          	$date1[$i]    = $i;
		          	$avSpeed1[$i] = '0';
		          	$avSpeed2[$i] = '0';
          		}
        	}
    	}

      	ksort($date1);
        ksort($avSpeed1);
        ksort($avSpeed2);
      	$data['lables']  = json_encode(array_values($date1));
	    $data['values']  = json_encode(array_values($avSpeed1));
	    $data['values2'] = json_encode(array_values($avSpeed2));
	    $data['did']     = $did;
	    $data['year']    = $year;
	    $data['month']   = $month;
	    $data['ptype1']  = $ptype1;
	    $data['ptype2']  = $ptype2;
	    $result          = view('statistics/graph')->with($data);
	    return $result;
    }

}
