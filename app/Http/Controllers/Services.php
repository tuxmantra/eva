<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;

class Services extends Controller
{
    public function index(){
        $user                = session('userdata');
        $data['user_type']   = $user['utype'];
        $deviceList          = Fetch::getDeviceListByUserType($user['uid'],$user['ucategory']);
        if(!empty($deviceList)){
              $did                 = $deviceList[0]->device_id;
              if(!empty(session('device_id')))
              {
                  $data['did']     = $did = session('device_id');
                  // $date            = session('date');
              }else
              {       
                  
              }
              $serviceDescSetting  = Fetch::getServiceDescSetting();
              $serviceDescSetting1 = Fetch::getServiceDescSettingTitleNotNull();
              $date                = date('Y-m-d');
              $serviceHistory      = Fetch::getServiceHistoryByDevId($did);
              $serviceDates        = Fetch::getServiceDateByDeviceId($did,$date);
              $data_array          = "";
              foreach($serviceDates as $sdt){
                 $data_array.= "'".date('Y-n-d',strtotime($sdt->sdate))."',"; 
              }

              $data = array(
                'title'                   => 'Services',
                'device_list'             => $deviceList,
                'user_category'           => $user['ucategory'],
                'service_desc_setting'    => $serviceDescSetting,
                'service_desc_setting1'   => $serviceDescSetting1,
                'service_history'         => $serviceHistory,
                'did'                     => $did,
                'service_dates'           => rtrim($data_array,',')
              );
        }else{
          $data['title'] = "Services";
          $data['user_category']    = $user['ucategory'];
          $data['service_dates']    = "";
          $data['did'] = "";
        }
      $data['serv_open'] = '1';
      $data['menu']      = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      // print_r($data);die;
    	return view('services/index')->with($data);
    }

    public function serviceAddSub(Request $request){
        $val  = $request->post('val');
        $data = Fetch::getServiceDescSettingTitleNotNullById($val);
      
        if(!empty($data))
        {
        foreach($data as $da)
        {   
        $sub = explode(';',$da->sub_title);
        //print_r($sub);
        echo '
            <label class="col-sm-2 col-form-label">'.$da->title.'</label>
            <div class="col-sm-10">
             <div class="form-group">
            <input class="form-control"  type="hidden" name="main_id[]" id="main_id'.$da->id.'" value="'.$da->id.'" />
            <input class="form-control"  type="hidden" name="sub_cnt'.$da->id.'" id="sub_cnt'.$da->id.'" value="'.count($sub).'" />';
            for($i=0;$i<count($sub);$i++) {
              echo '<div class="col-md-4 col-sm-4 col-xs-4" style="float: left;">
                        <div class="form-group">
                            <label style="col-sm-2 col-form-label">'.$sub[$i].'</label>
                            <input class="form-control" type="text" name="sub'.$da->id.'[]" id="'.$da->id.'_'.$i.'" value="" />
                        </div>
                    </div>';
            }
         echo '</div></div>
         ';
     } }
    }

    public function add(Request $request){

        $token    = request('form_token');
        if($token == session('form_token')){
           session()->forget('form_token');
            $postVal['devid']         = $did = request('devid');
            $deviceDetail             = Fetch::getIdByDeviceId($postVal['devid']);
            $postVal['did']           = $deviceDetail[0]->id;
            $postVal['type']          = request('type');
            $postVal['details']       = request('details');
            $postVal['provider_name'] = request('provider_name');
            $postVal['kms_read']      = request('kms_read');

            if(empty(request('sdate')) || empty(request('type')) || empty(request('kms_read'))){
                  \Session::flash('message','Fields required.');
                  return redirect('services');
            }

            $main_id = request('main_id');
            if(!empty($main_id)){
                    $val = "";
                    foreach($main_id as $kv=>$vv)
                    {
                        $sCount = "sub_cnt".$vv;
                        $subC   = request($sCount);
                        $sValue ='';
                        $sC     = "sub".$vv;
                        $subVal = request($sC);
                        //print_r($subVal);
                        if (!array_filter($subVal)) {
                            // all values are empty (where "empty" means == false)
                        }
                        else 
                        {
                            foreach($subVal as $skey=>$sval)
                            {
                                    
                                $sValue = ($sValue=='')?$sval:$sValue.','.$sval;
                            }
                            $val = ($val=='')?$vv.':'.$sValue:$val.';'.$vv.':'.$sValue;
                        }
                    }
                    $postVal['descrp_sub'] = $val;
            }
            $postVal['sdate']  = date('Y-m-d',strtotime(request('sdate')));
            $toval             = request('toval');
            $postVal['descrp'] = (isset($toval))  ? implode(',',$toval) : "";
            $aryData           = array();
            if(!empty($toval)){
                foreach($toval as $v)
                {
                    $nv            = "idvName".$v;
                    $aryData["$v"] = empty(request("$nv")) ? "No Description by customer.": request("$nv");
                }
            }

            $postVal['spec']   = json_encode($aryData);
            $postVal['status'] = '1';
            $postVal['ad_by']  = session('userdata')['uid'];
            Common::insert('service',$postVal);
            
          // last service date and respective service date Odometer reading , sub(currentOD reading-)
          
          $valdo = Fetch::getservicedetailsapi($did);
          $lastServiceOdometerReading = $valdo[0]->kms_read;
          $lastServicedate = $valdo[0]->sdate;


          //Odometer reading when vehicle registered with EVA
          $vallo = Fetch::getodoreadingapi($did);
          $lastOdometerregistered = $vallo[0]->travel_purchase;

          // Km run after registed 
          $valloc = Fetch::getdistanceapi($did);
          $KMrunafterregi = $valloc[0]->dist;

          $serviceKmdue   = (10000 - abs($lastServiceOdometerReading - ($lastOdometerregistered + $KMrunafterregi)));
          $servicedaysdue = 365*$serviceKmdue/10000; 
          // $Factordays     = $dpfCorFact * $tempCorrFact * $engLoadCorFact * $engRPMCorFact * $engCrCorFact * $dustCorFact;
          // $servicedaysdue = $servicedaysdue-$Factordays;
          // $serviceKmdue   = 10000*$servicedaysdue/365;
          $tday= date('Y-m-d');
          $date2=date_create($lastServicedate);
          $date =date_create($tday);
          // echo $tday.$date2;
          $daysDiff        = date_diff($date2,$date);
          // print_r($daysDiff->days); die;
          $servicedaysdueactuval = 365 - $daysDiff->days;
          if($servicedaysdueactuval < $servicedaysdue)
          {
            $servicedaysdue = $servicedaysdueactuval;
          }
          else
          {
            $servicedaysdue = $servicedaysdue;
          }


          $servicedaysdue =  round($servicedaysdue);
          $serviceKmdue   =  10000 * $servicedaysdue / 365; 
          $datup           = Fetch::update_device_drive_latest1($did,$servicedaysdue,$serviceKmdue);
            \Session::flash('message','Service Added Successfully.');
            return redirect('services');
      }else{
           session()->forget('form_token');
           \Session::flash('message','Sorry, token expired now try.');
           return redirect('services');
      }
    }

    public function getServiceLoad(Request $request){
       $devId               = $request->post('did');
       if(session('device_id') == $devId)
        {
           
        }else
        {
            
            session(['device_id' => $devId]);
        }
       if(!empty($devId))
        {
               /* Start Listing Load -------------------------- */
           $sdate               = date('Y-m-d',strtotime($request->post('sdate')));
           $serviceDescSetting  = Fetch::getServiceDescSetting();
           $serviceTypeSetting  = Fetch::getServiceTypeSetting();
           $form_token          = Helper::token(8);
           $serviceHistory      = Fetch::getServiceHistoryByDevId($devId,$sdate);
           // print_r($devId);die;
           $serviceDates        = Fetch::getServiceDateByDeviceId($devId,$sdate);
           $data_array          = "";
           foreach($serviceDates as $sdt)
           {
             $data_array.= date('Y-n-d',strtotime($sdt->sdate)).","; 
           }
            $data_array    = rtrim($data_array,',');

            echo '<ul class="nav nav-pills nav-pills-warning" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#link1" role="tablist">
                            Add Service Record
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#link2" role="tablist">
                            Last Service
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link active show" data-toggle="tab" href="#link3" role="tablist">
                            Service History
                          </a>
                        </li>
                      </ul>
                      <div class="tab-content tab-space">
                        <div class="tab-pane" id="link1">
                        

                        <!-- Tab 1 Start -->

                  <form method="post" onsubmit="return addServicesValidate();" action="'.url('/').'/service-record" class="form-horizontal">
                      <input type="hidden" value="'.$form_token.'" name="form_token">
                      <input type="hidden" name="devid" id="devid" value="'.$devId.'">

                      <div class="row">
                          <label class="col-sm-2 col-form-label">Service Date</label>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <input type="date" class="form-control datepicker" name="sdate" id="sdate" placeholder="Service Provider Name">
                            </div>
                          </div>
                        
                          <label class="col-sm-2 col-form-label">Service Type</label>
                          <div class="col-sm-4">
                            <div class="form-group">
                               <select class="selectpicker service_type" data-style="select-with-transition" title="Select Service Type" data-size="7" name="type" id="type" tabindex="-98">
                                <option disabled=""> Service Type</option>';
                                if(!empty($serviceTypeSetting)){
                                    foreach($serviceTypeSetting as $sts){
                                        echo '<option value="'.$sts->id.'">'.$sts->title.'</option>';
                                    }
                                }
                              echo '</select>
                            </div>
                          </div>
                        
                         
                       
                          <label class="col-sm-2 col-form-label">Provider Name</label>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <input type="text" class="form-control datepicker" name="provider_name" id="provider_name" placeholder="Service Provider Name">
                            </div>
                          </div>
                        
                          <label class="col-sm-2 col-form-label">Kms Reading</label>
                          <div class="col-sm-4">
                            <div class="form-group">
                              <input type="text" class="form-control datepicker" name="kms_read" id="kms_read" placeholder="Kms Reading">
                            </div>
                          </div>
                        
                          <label class="col-sm-2 col-form-label">Description</label>
                          <div class="col-sm-4">
                            <div class="form-group">
                             <select class="selectpicker service_type" data-style="select-with-transition" title="Select Description Type" data-size="7" name="select-from" id="select-from" tabindex="-98" multiple="">
                                <option disabled=""> Description Type</option>';
                                if(!empty($serviceDescSetting)){
                                     foreach($serviceDescSetting as $dty){
                                        echo '<option value="'.$dty->id.'">'.$dty->title.'</option>';
                                     }
                                }
                              echo '</select>
                             
                            </div>
                          </div>

                          
                        
                        
                          

                ';
                 

                echo '<div class="col-md-4" align="right" style="margin-top: 30px;">
                  <button type="Submit" class="btn btn-fill btn-rose">Save</button>
                </div>
                </div>
              </form>
              <div class="col-md-12" align="center">
                  <span id="cli_message" class="bmd-form-group" style="color: red; font-size: 13px; font-weight: 500;"></span>
                </div>
                        <!-- Tab 1 End -->


                        </div>
                        <div class="tab-pane" id="link2"> 
                        <!-- Tab2 Start--->

                           <!-- Tab2 Start--->

                         <div class="toolbar">
                        <!--        Here you can write extra buttons/actions for the toolbar              -->
                      </div>
                      <div class="material-datatables">
                        <table id="datatables1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                          <thead>
                            <tr>
                              <th>&nbsp;&nbsp;</th>
                              <th class="table-heading-font">Description</th>
                              <th class="table-heading-font">Date</th>
                         
                              <th class="table-heading-font">Kms Reading</th>
                              <th class="table-heading-font none">Details</th>
                         
                            </tr>
                          </thead>
                          <tfoot>
                            <tr>
                              <th>&nbsp;&nbsp;</th>
                              <th class="table-heading-font">Description</th>
                              <th class="table-heading-font">Date</th>
                         
                              <th class="table-heading-font">Kms Reading</th>
                              <th class="table-heading-font none">Details</th>
                             
                           
                            </tr>
                          </tfoot>
                          <tbody>';
                     
                            if(!empty($serviceDescSetting)){
                            $id = 1;
                            foreach($serviceDescSetting as $sds){ 
                      
                            $ser_id       = $sds->id;
                            $desc = "";
                            
                            $service_data = Helper::getLastServiceForDeviceById($devId,$ser_id);

                            if(empty($service_data)){
                                $dates = "-";
                                $kms   = "-";
                                $spec  = "-";
                                $sub   = "";
                            }else{

                              if(!empty($sds->sub_title)){

                                $despSubServ = explode(';',$service_data[0]->descrp_sub);
                                 
                                foreach($despSubServ as $desSS){
                                   $comma = ',';
                                   if(strpos($desSS,$comma) !== false){
                                       $dd      = explode(':',$desSS);
                                       $ff      = explode(',',$dd[1]);
                                       $sub_tit = Fetch::getSubTitleServicesBYId($dd[0]);
                                       $sub_tit = $sub_tit[0]->sub_title;
                                       $sub_exp =explode(';',$sub_tit);

                                       for($i=0;$i<=count($sub_exp)-1;$i++){
                                           $desc.= "<br>".$sub_exp[$i]." - ".$ff[$i]." ";
                                       }
                                   }else{
                                      $dd      = explode(':',$desSS);
                                      $sub_tit = Fetch::getSubTitleServicesBYId($dd[0]);
                                      $sub_tit = !empty($sub_tit[0]->sub_title)? $sub_tit[0]->sub_title:"";
                                         if(!empty($sub_tit)){
                                        $desc.= "<br>".$sub_tit." - ".$dd[1]." ";
                                      }else{
                                        $desc.= "";
                                      }
                                   }
                                }
                                
                              }

                              $dates   = $service_data[0]->sdate;
                              $kms     = $service_data[0]->kms_read;
                              $spec    = json_decode($service_data[0]->spec);

                              if(!empty($spec->$ser_id)){
                                  $spec    = $spec->$ser_id;
                              }else{
                                $spec = "-";
                              }
                            }

                            echo '<tr>
                              <td>&nbsp;&nbsp;</td>
                              <td class="table-heading-font" style="background-color: #f5f5f5;">Last '.$sds->title.'</td>
                              
                              <td style="background-color: #ffffff;" class="table-heading-font">'.$dates.'</td>
                              
                              <td style="background-color: #f5f5f5;" class="table-heading-font">'.$kms.'</td>
                              
                              <td style="background-color: #ffffff;" class="table-heading-font none">'.$spec." ".$desc.'</td>

                            </tr>';
                        $desc = "";
                        }// endforeach

                        } // close if
                           
                           
                          echo '</tbody>
                        </table>
                      </div>
                  
                    <!-- end content-->

                         <!-- Tab 2 End -->

                         <!-- Tab 2 End -->
                        </div>
                        <div class="tab-pane active show" id="link3">
                            <!-- Tab3 Start -->
                          <div class="toolbar">
                        <!-- Here you can write extra buttons/actions for the toolbar -->
                      </div>
                      <div class="material-datatables">
                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                          <thead>
                            <tr>
                              
                              <th class="table-heading-font">S.No</th>
                              <th class="table-heading-font">Date</th>
                              <th class="table-heading-font">Service Type</th>
                            
                              <th class="table-heading-font">Service Provider</th>
                              <th class="table-heading-font">Kms Reading</th>
                             
                         
                            </tr>
                          </thead>

                          <tbody>';
                            if(!empty($serviceHistory)){
                            $id = 1;
                            foreach($serviceHistory as $sh){  
                              // echo Helper::getServiceDescription($sh->descrp,2);die;
                            echo '<tr>
                             
                              <td class="table-heading-font" style="background-color: #f5f5f5;">'.$id.'</td>
                              <td style="background-color: #ffffff;" class="table-heading-font">'.$sh->sdate.'</td>
                              <td style="background-color: #f5f5f5;" class="table-heading-font">'.Helper::getServiceType($sh->type).'</td>
                              
                              <td style="background-color: #ffffff;" class="table-heading-font">'.$sh->provider_name.'</td>
                              <td style="background-color: #f5f5f5;" class="table-heading-font">'.$sh->kms_read.'</td>
                             
                              

                            </tr>';
                            $id++;  
                            }}else{
                               echo '<tr><td></td><td></td><td></td><td>No History Exist</td><td></td><td></td><td></td></tr>';
                            }
                           
                           
                          echo '</tbody>
                        </table>
                      </div>
                  
                    <!-- end content-->
         

                    <!-- Tab3 End -->

                        </div>
                      </div> ';
               /* End Listing Load ---------------------------- */
           }else{
          echo '<ul class="nav nav-pills nav-pills-warning" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#link1" role="tablist">
                        Add Service Record
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#link2" role="tablist">
                        Last Service
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link active show" data-toggle="tab" href="#link3" role="tablist">
                        Service History
                      </a>
                    </li>
                  </ul>
                  <div class="tab-content tab-space">
                    <div class="tab-pane" id="link1">
                    <div class="col-md-12" align="center">No Device Present</div>
                    </div>
                    <div class="tab-pane" id="link2"><div class="col-md-12" align="center">No Device Present</div></div>
                    <div class="tab-pane" id="link3"><div class="col-md-12" align="center">No Device Present</div></div>
                  </div>';
       }
      
  }
}
