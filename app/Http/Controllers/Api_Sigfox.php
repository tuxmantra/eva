<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Common;
use App\Model\Fetch;
use Helper;
use Illuminate\Support\Facades\Mail;
use App\Mail\Newuser;
use Illuminate\Support\Arr;
// use Mail;

class Api_Sigfox extends Controller
{
	public function monitor_flag(Request $request)
  	{
  		if(!empty($request->data))
        {    
        	$this->updatelog("monitor_flag",$request->data);
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $devInfo  = Fetch::getdevice($deviceID);
            $did      = $devInfo[0]->id;
            if(!empty($devInfo))
            {
            	//Get Calib Update status from DB
            	$calibStat  = Fetch::getDactvalFromDeviceByID($did);
            	$updateStat = $calibStat[0]->dactval;
		        $arrayData['success'] = TRUE;
		        $arrayData['flag']    = 0;
		        $arrayData['devConfigStatus'] = $updateStat;		// Calibration Flag. 1->Update calib; 0->No update available

		        if(!empty($devInfo[0]->dev_cmds))
		        {
		        	$arrayData['cmds'] = $devInfo[0]->dev_cmds;		// Commands to be sent to device
		        	$dev_cmds = '';
		        	$currVer  = '';
					Fetch::updateDeviceCmdCol($deviceID,$dev_cmds,$currVer);
		        }
		        else
		        {
		        	$arrayData['cmds'] = "";
		        }
		        
				$firm = Fetch::firmupdate($deviceID);
				// print_r($devInfo);
		        if(($firm[0]->num) > 0)
		        {
					$arrayData['frmwrUpdStatus'] = 1;
				}
				else
				{
					$arrayData['frmwrUpdStatus'] = 0;
				}

            	$driveNo = Fetch::getLastDriveNum($deviceID);
		        if(!empty($driveNo))
		        {
		        	$arrayData['driveno']   = $driveNo[0]->drive;
		        	$arrayData['drivetime'] = $driveNo[0]->ad_dt;
		        }
		        else
		        {
		        	$arrayData['driveno'] = 0;
		        }
		        //Reset Calib Update status to 0 in DB
            	Common::updateTable('device',array('id' => $did),array('dactval' => '0'));
            }
            else
            {
	            $arrayData['success']      = FALSE;
	            $arrayData['errorCode']    = "1002";
	            $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";

        }
        echo json_encode($arrayData);
  	}

  	public function get_firmwareupdate(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$this->updatelog("get_firmwareupdate",$request->data);
        	$obj       = json_decode($request->data, TRUE);
			$deviceID  = $obj['devID'];
			$appupdate = Fetch::getfirmstatdetails($deviceID);
        	$appNames  = array();
			$appLinks  = array();
			$appVers   = array();
			$cmd       = array();
			$i         = 0;
			if($deviceID == '60000404')
			{
				$appNames = "162.bin";
				$appLinks = "https://eva.enginecal.com/public/firm_release/tmd102bt/";
				$appVers  = "1.62";
			}
			else
			{
				$appNames = "150.bin";
				$appLinks = "https://eva.enginecal.com/public/firm_release/tmd102bt/";
				$appVers  = "1.50";
			}
			// $appNames = "127.bin";
			// $appLinks = "https://eva.enginecal.com/public/firm_release/tmd102bt/";
			// $appVers  = "1.27";
		   	
        	$result['success']            = TRUE;
        	
        	$result['installFirmCnt']     = 1;
		    $result['installFirmName']    = $appNames;
		    $result['installFirmLink']    = $appLinks;
		    $result['installFirmVersion'] = $appVers;
		    // $result['uninstallAppStatus'] = 0;
		    // $result['uinistallAppName']  = "";
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function frmwrupdres(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$this->updatelog("frmwrupdres",$datMe);
        	$obj        = json_decode($datMe, TRUE);
			$deviceID   = $obj['devID'];
			$frmName    = $obj['frmName'];
			$frmVersion = $obj['frmVersion'];
			$frmStatus  = $obj['frmStatus'];
			
			for($i = 0;$i < count($frmStatus);$i++)
			{
				if($frmStatus[$i] == 1)
				{
					$st     = 0;
					$update = Fetch::updatefirmres($deviceID,$frmName[$i],$frmVersion[$i],$st);
				}
				else
				{
					$st = 1;
				}
			    // $update=Fetch::updatefirmres1($deviceID);
		    }
			// $update=Fetch::updatefirmres($deviceID,$frmName,$frmVersion,$frmStatus);
        	$result['success']    = TRUE;
        	$result['successmsg'] = "Data Upload Successful!";
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function get_cmdupdate(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$this->updatelog("get_cmdupdate",$request->data);
        	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];

			$devicestatus = Fetch::devicestatus($deviceID);
			if(!empty($devicestatus))
          	{
	          	$result['success'] = TRUE;

	            $cmd = json_decode($devicestatus[0]->dev_cmds,TRUE);
                if(!empty($cmd))
                {
                    $keys   = array_keys($cmd);
                    $values = array_values($cmd);
                    // print_r($values);die;
                  	for($i=0;$i<count($values);$i++) 
                  	{
                  		$result[$keys[$i]]=$values[$i];
                  	}
              	}
          	}
          	else
          	{
	          	$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "No Commands!";
          	}
      	}
      	else
      	{
      	    $result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
      	}
      	$resultVal = json_encode($result,JSON_UNESCAPED_SLASHES);
		echo $resultVal;
  	}

  	public function cmdres(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$this->updatelog("cmdres",$datMe);
        	$obj      = json_decode($datMe, TRUE);
			$deviceID = $obj['devID'];
			$response = $obj['res'];
			
			$deviceAvail = Fetch::devicestatus($deviceID);
			if(!empty($deviceAvail))
			{
				$devresp = $deviceAvail[0]->dev_res;
				if($devresp == $response)
				{
					//Do nothing as the response is same in the server.
					$result['success'] = TRUE;
					$result['msg']     = "Response Update not required in DB!";
				}
				else
				{
					//Update response given for the device
					$update = Fetch::updateDeviceCmdres($deviceID,$response);
					if($update)
					{
						$result['success'] = TRUE;
						$result['msg']     = "Response Updated in DB!";
					}
		        	else
		        	{
		        		$result['success']      = FALSE;
						$result['errorCode']    = "1003";
						$result['errorMessage'] = "Response Update Failed!";
		        	}
				}
	        }
	        else
	        {
	        	$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
	        }
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function get_devconfig(Request $request)
  	{
  		if(!empty($request->data))
        {
        	$this->updatelog("get_devconfig",$request->data);
        	$obj      = json_decode($request->data, TRUE);
			$deviceID = $obj['devID'];

        	// Get Device Data
			$devInfo = Fetch::devicestatus($deviceID);
			// print_r($devInfo);die;
			$did     = $devInfo[0]->id;
			
			// Get Vehicle details
			$vehi  = Fetch::getVehicleDataById($did);
			// print_r($vehi);die;
			$vType = $vehi->fuel_tpe;

			$encryptKey = rand(25, 300);
			if(!empty($devInfo))
			{
				$dataArr  = array();
				$data     = array();
				$dataArr  = json_decode($vehi->calval);
				$data[0]  = $vType;
				$data[1]  = $dataArr->Eng_eff_C;
				$data[2]  = $dataArr->Eng_Disp_C * 1000;
				if($dataArr->TC_Status_C == "Yes")
				{
					$data[3] = 1;
				}
				else
				{
					$data[3] = 0;
				}
				if($dataArr->Injection_Type_C == "Direct")
				{
					$data[4] = 1;
				}
				else
				{
					$data[4] = 0;
				}
				$data[5]   = round(($dataArr->Eff_RPM_Limit1_C / 50),0);
				$data[6]   = round(($dataArr->Eff_RPM_Limit2_C / 50),0);
				$data[7]   = round(($dataArr->Eff_RPM_Limit3_C / 50),0);
				$data[8]   = round(($dataArr->Eff_RPM_Limit4_C / 50),0);
				$data[9]   = round(($dataArr->Eff_RPM_Limit5_C / 50),0);

				//Equivalence Ratio
				$data[10] = (round(($dataArr->Eqratio_Eload_1_10_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_1_10_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_1_10_RPMLimit3_C * 200),0)) << 16) ;
				$data[11] = (round(($dataArr->Eqratio_Eload_1_10_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_1_10_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_11_20_RPMLimit1_C * 200),0)) << 16); 
				$data[12] = (round(($dataArr->Eqratio_Eload_11_20_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_11_20_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_11_20_RPMLimit4_C * 200),0)) << 16);
				$data[13] = (round(($dataArr->Eqratio_Eload_11_20_RPMLimit5_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit1_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit2_C * 200),0)) << 16);
				$data[14] = (round(($dataArr->Eqratio_Eload_21_30_RPMLimit3_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit4_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_21_30_RPMLimit5_C * 200),0)) << 16);	
				$data[15] = (round(($dataArr->Eqratio_Eload_31_40_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_31_40_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_31_40_RPMLimit3_C * 200),0)) << 16) ;
				$data[16] = (round(($dataArr->Eqratio_Eload_31_40_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_31_40_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_41_50_RPMLimit1_C * 200),0)) << 16);
				$data[17] = (round(($dataArr->Eqratio_Eload_41_50_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_41_50_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_41_50_RPMLimit4_C * 200),0)) << 16);
				$data[18] = (round(($dataArr->Eqratio_Eload_41_50_RPMLimit5_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit1_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit2_C * 200),0)) << 16);
				$data[19] = (round(($dataArr->Eqratio_Eload_51_60_RPMLimit3_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit4_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_51_60_RPMLimit5_C * 200),0)) << 16);
				$data[20] = (round(($dataArr->Eqratio_Eload_61_80_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_61_80_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_61_80_RPMLimit3_C * 200),0)) << 16);
				$data[21] = (round(($dataArr->Eqratio_Eload_61_80_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_61_80_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_81_90_RPMLimit1_C * 200),0)) << 16);
				$data[22] = (round(($dataArr->Eqratio_Eload_81_90_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_81_90_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_81_90_RPMLimit4_C * 200),0)) << 16);
				$data[23] = (round(($dataArr->Eqratio_Eload_81_90_RPMLimit5_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit1_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit2_C * 200),0)) << 16);
				$data[24] = (round(($dataArr->Eqratio_Eload_91_94_RPMLimit3_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit4_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_91_94_RPMLimit5_C * 200),0)) << 16);
				$data[25] = (round(($dataArr->Eqratio_Eload_95_98_RPMLimit1_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_95_98_RPMLimit2_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_95_98_RPMLimit3_C * 200),0)) << 16);
				$data[26] = (round(($dataArr->Eqratio_Eload_95_98_RPMLimit4_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_95_98_RPMLimit5_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_99_100_RPMLimit1_C * 200),0)) << 16);
				$data[27] = (round(($dataArr->Eqratio_Eload_99_100_RPMLimit2_C * 200),0)) | ((round(($dataArr->Eqratio_Eload_99_100_RPMLimit3_C * 200),0)) << 8) | ((round(($dataArr->Eqratio_Eload_99_100_RPMLimit4_C * 200),0)) << 16);
				$data[28] = (round(($dataArr->Eqratio_Eload_99_100_RPMLimit5_C * 200),0)) | ((round(($dataArr->Ovr_Run_Cold_EqRatio_C * 100),0)) << 8) | ((round(($dataArr->Ovr_Run_Hot_EqRatio_C * 100),0)) << 16);

				// Engine Efficiency
				$data[29] = ($dataArr->EngEff_Eload_1_10_RPMLimit1_C) | (($dataArr->EngEff_Eload_1_10_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_1_10_RPMLimit3_C) << 16) ;
				$data[30] = ($dataArr->EngEff_Eload_1_10_RPMLimit4_C) | (($dataArr->EngEff_Eload_1_10_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_11_20_RPMLimit1_C) << 16); 
				$data[31] = ($dataArr->EngEff_Eload_11_20_RPMLimit2_C) | (($dataArr->EngEff_Eload_11_20_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_11_20_RPMLimit4_C) << 16);
				$data[32] = ($dataArr->EngEff_Eload_11_20_RPMLimit5_C) | (($dataArr->EngEff_Eload_21_30_RPMLimit1_C) << 8) | (($dataArr->EngEff_Eload_21_30_RPMLimit2_C) << 16);
				$data[33] = ($dataArr->EngEff_Eload_21_30_RPMLimit3_C) | (($dataArr->EngEff_Eload_21_30_RPMLimit4_C) << 8) | (($dataArr->EngEff_Eload_21_30_RPMLimit5_C) << 16);	
				$data[34] = ($dataArr->EngEff_Eload_31_40_RPMLimit1_C) | (($dataArr->EngEff_Eload_31_40_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_31_40_RPMLimit3_C) << 16) ;
				$data[35] = ($dataArr->EngEff_Eload_31_40_RPMLimit4_C) | (($dataArr->EngEff_Eload_31_40_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_41_50_RPMLimit1_C) << 16); 
				$data[36] = ($dataArr->EngEff_Eload_41_50_RPMLimit2_C) | (($dataArr->EngEff_Eload_41_50_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_41_50_RPMLimit4_C) << 16);
				$data[37] = ($dataArr->EngEff_Eload_41_50_RPMLimit5_C) | (($dataArr->EngEff_Eload_51_60_RPMLimit1_C) << 8) | (($dataArr->EngEff_Eload_51_60_RPMLimit2_C) << 16);
				$data[38] = ($dataArr->EngEff_Eload_51_60_RPMLimit3_C) | (($dataArr->EngEff_Eload_51_60_RPMLimit4_C) << 8) | (($dataArr->EngEff_Eload_51_60_RPMLimit5_C) << 16);
				$data[39] = ($dataArr->EngEff_Eload_61_80_RPMLimit1_C) | (($dataArr->EngEff_Eload_61_80_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_61_80_RPMLimit3_C) << 16);
				$data[40] = ($dataArr->EngEff_Eload_61_80_RPMLimit4_C) | (($dataArr->EngEff_Eload_61_80_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_81_90_RPMLimit1_C) << 16);
				$data[41] = ($dataArr->EngEff_Eload_81_90_RPMLimit2_C) | (($dataArr->EngEff_Eload_81_90_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_81_90_RPMLimit4_C) << 16);
				$data[42] = ($dataArr->EngEff_Eload_81_90_RPMLimit5_C) | (($dataArr->EngEff_Eload_91_94_RPMLimit1_C) << 8) | (($dataArr->EngEff_Eload_91_94_RPMLimit2_C) << 16);
				$data[43] = ($dataArr->EngEff_Eload_91_94_RPMLimit3_C) | (($dataArr->EngEff_Eload_91_94_RPMLimit4_C) << 8) | (($dataArr->EngEff_Eload_91_94_RPMLimit5_C) << 16);
				$data[44] = ($dataArr->EngEff_Eload_95_98_RPMLimit1_C) | (($dataArr->EngEff_Eload_95_98_RPMLimit2_C) << 8) | (($dataArr->EngEff_Eload_95_98_RPMLimit3_C) << 16);
				$data[45] = ($dataArr->EngEff_Eload_95_98_RPMLimit4_C) | (($dataArr->EngEff_Eload_95_98_RPMLimit5_C) << 8) | (($dataArr->EngEff_Eload_99_100_RPMLimit1_C) << 16);
				$data[46] = ($dataArr->EngEff_Eload_99_100_RPMLimit2_C) | (($dataArr->EngEff_Eload_99_100_RPMLimit3_C) << 8) | (($dataArr->EngEff_Eload_99_100_RPMLimit4_C) << 16);
				$data[47] = ($dataArr->EngEff_Eload_99_100_RPMLimit5_C);

				// Volumetric Efficiency
				$data[48] = ($dataArr->VolEff_Eload_1_10_RPMLimit1_C) | (($dataArr->VolEff_Eload_1_10_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_1_10_RPMLimit3_C) << 16) ;
				$data[49] = ($dataArr->VolEff_Eload_1_10_RPMLimit4_C) | (($dataArr->VolEff_Eload_1_10_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_11_20_RPMLimit1_C) << 16); 
				$data[50] = ($dataArr->VolEff_Eload_11_20_RPMLimit2_C) | (($dataArr->VolEff_Eload_11_20_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_11_20_RPMLimit4_C) << 16);
				$data[51] = ($dataArr->VolEff_Eload_11_20_RPMLimit5_C) | (($dataArr->VolEff_Eload_21_30_RPMLimit1_C) << 8) | (($dataArr->VolEff_Eload_21_30_RPMLimit2_C) << 16);
				$data[52] = ($dataArr->VolEff_Eload_21_30_RPMLimit3_C) | (($dataArr->VolEff_Eload_21_30_RPMLimit4_C) << 8) | (($dataArr->VolEff_Eload_21_30_RPMLimit5_C) << 16);	
				$data[53] = ($dataArr->VolEff_Eload_31_40_RPMLimit1_C) | (($dataArr->VolEff_Eload_31_40_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_31_40_RPMLimit3_C) << 16) ;
				$data[54] = ($dataArr->VolEff_Eload_31_40_RPMLimit4_C) | (($dataArr->VolEff_Eload_31_40_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_41_50_RPMLimit1_C) << 16); 
				$data[55] = ($dataArr->VolEff_Eload_41_50_RPMLimit2_C) | (($dataArr->VolEff_Eload_41_50_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_41_50_RPMLimit4_C) << 16);
				$data[56] = ($dataArr->VolEff_Eload_41_50_RPMLimit5_C) | (($dataArr->VolEff_Eload_51_60_RPMLimit1_C) << 8) | (($dataArr->VolEff_Eload_51_60_RPMLimit2_C) << 16);
				$data[57] = ($dataArr->VolEff_Eload_51_60_RPMLimit3_C) | (($dataArr->VolEff_Eload_51_60_RPMLimit4_C) << 8) | (($dataArr->VolEff_Eload_51_60_RPMLimit5_C) << 16);
				$data[58] = ($dataArr->VolEff_Eload_61_80_RPMLimit1_C) | (($dataArr->VolEff_Eload_61_80_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_61_80_RPMLimit3_C) << 16);
				$data[59] = ($dataArr->VolEff_Eload_61_80_RPMLimit4_C) | (($dataArr->VolEff_Eload_61_80_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_81_90_RPMLimit1_C) << 16);
				$data[60] = ($dataArr->VolEff_Eload_81_90_RPMLimit2_C) | (($dataArr->VolEff_Eload_81_90_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_81_90_RPMLimit4_C) << 16);
				$data[61] = ($dataArr->VolEff_Eload_81_90_RPMLimit5_C) | (($dataArr->VolEff_Eload_91_94_RPMLimit1_C) << 8) | (($dataArr->VolEff_Eload_91_94_RPMLimit2_C) << 16);
				$data[62] = ($dataArr->VolEff_Eload_91_94_RPMLimit3_C) | (($dataArr->VolEff_Eload_91_94_RPMLimit4_C) << 8) | (($dataArr->VolEff_Eload_91_94_RPMLimit5_C) << 16);
				$data[63] = ($dataArr->VolEff_Eload_95_98_RPMLimit1_C) | (($dataArr->VolEff_Eload_95_98_RPMLimit2_C) << 8) | (($dataArr->VolEff_Eload_95_98_RPMLimit3_C) << 16);
				$data[64] = ($dataArr->VolEff_Eload_95_98_RPMLimit4_C) | (($dataArr->VolEff_Eload_95_98_RPMLimit5_C) << 8) | (($dataArr->VolEff_Eload_99_100_RPMLimit1_C) << 16);
				$data[65] = ($dataArr->VolEff_Eload_99_100_RPMLimit2_C) | (($dataArr->VolEff_Eload_99_100_RPMLimit3_C) << 8) | (($dataArr->VolEff_Eload_99_100_RPMLimit4_C) << 16);
				$data[66] = ($dataArr->VolEff_Eload_99_100_RPMLimit5_C) | (($dataArr->Ovr_Run_Cold_VolEff_C) << 8) | (($dataArr->Ovr_Run_Hot_VolEff_C) << 16);
				
				$data[67]  = 0;
				$data[68]  = 0;
				$data[69]  = 0;
				$data[70]  = 0;
				$data[71]  = 0;
				$data[72]  = 0;
				$data[73]  = 0;
				$data[74]  = 0;
				$data[75]  = 0;
				$data[76]  = 0;
				$data[77]  = 0;
				$data[78]  = 0;
				$data[79]  = 0;
				$data[80]  = 0;
				$data[81]  = 0;
				$data[82]  = 0;
				$data[83]  = 64;
				$data[84]  = $encryptKey;
				$data[85]  = 0;
				$data[86]  = 0;
				$data[87]  = 0;
				$data[88]  = 0;
				$data[89]  = 0;
				$data[90]  = 0;
				$data[91]  = 0;
				$data[92]  = 0;
				$data[93]  = 0;
				$data[94]  = 0;
				$data[95]  = 0;
				$data[96]  = 0;
				$data[97]  = 0;
				$data[98]  = 0;
				$data[99]  = 0;
				$data[100] = 0;
				$data[101] = 0;
				$data[102] = 0;
				$data[103] = 0;
				$data[104] = 0;
				$data[105] = 0;
				$data[106] = 0;
				$data[107] = 0;
				$data[108] = 0;
				$data[109] = 0;
				$data[110] = 0;
				$data[111] = 0;
				$data[112] = 0;
				$data[113] = 0;
				$data[114] = 0;
				$data[115] = 0;
				$data[116] = 0;
				$data[117] = 0;
				$data[118] = 0;
				$data[119] = 0;
				$data[120] = 0;
				$data[121] = 0;
				$data[122] = 0;
				$data[123] = 0;
				$data[124] = $dataArr->MAP_Valid_C;
				$data[125] = $dataArr->MAF_Valid_C;
				$data[126] = $dataArr->Baro_Press_City_C;
				$data[127] = $dataArr->MAP_Abs_PkLmt3_C;
				$data[128] = round(($dataArr->Accl_Val_C * 20),0);
				$data[129] = $dataArr->Accl_Eload_C;
				$data[130] =round(( $dataArr->Decl_Val_C * 20),0);
				$data[131] = $dataArr->Eload_Def_C;
				$data[132] = $dataArr->Eload_Max_C;
				$data[133] = round(($dataArr->Max_Torque_C / 5),0);
				$data[134] = $dataArr->ELoad_Idle_Warm_C;
				$data[135] = $dataArr->ELoad_Idle_Ld1_C;
				$data[136] = $dataArr->ELoad_Idle_Ld2_C;
				$data[137] = $dataArr->Osc_EloadOffset_Lmt_C;
				$data[138] = $dataArr->Cool_Lmt_C;
				$data[139] = round(($dataArr->Osc_RPM_Lmt1_C / 50),0);
				$data[140] = round(($dataArr->Osc_RPM_Lmt2_C / 50),0);
				$data[141] = round(($dataArr->Osc_RPM_Lmt3_C / 50),0);
				$data[142] = round(($dataArr->Osc_RPM_Lmt4_C / 50),0);
				$data[143] = round(($dataArr->CRP_RPM_PkLmt_C / 50),0);
				$data[144] = round(($dataArr->CRP_Type_C / 1000),0);
				$data[145] = round(($dataArr->CRP_MaxLmt3_C / 500),0);
				$data[146] = round(($dataArr->CRP_MaxLmt1_C / 500),0);
				$data[147] = $dataArr->Base_OvrHt_Cool_Lmt_C;
				$data[148] = $dataArr->Cool_Temp_Lmt1_C;
				$data[149] = $dataArr->Cool_Temp_Lmt2_C;
				$data[150] = $dataArr->Cool_Temp_Lmt3_C;
				$data[151] = $dataArr->Cool_Temp_Lmt4_C;
				$data[152] = round(($dataArr->RPM_Idle_Warm_C / 50),0);
				$data[153] = round(($dataArr->RPM_Idle_Ld1_C / 50),0);
				$data[154] = round(($dataArr->RPM_Idle_Ld2_C / 50),0);
				$data[155] = $dataArr->MAP_RPMOffset_PkLmt_C;
				$data[156] = round(($dataArr->Max_Air_Trq_RPM_C / 50),0);
				$data[157] = round(($dataArr->MAP_Air_RPM_LwrLmt_C / 50),0);
				$data[158] = round(($dataArr->MAP_Air_RPM_PkLmt_C / 50),0);
				$data[159] = $dataArr->MAP_Air_EloadOffset_PkLmt_C;
				$data[160] = $dataArr->MAP_Air_IdleLmt3_C;
				$data[161] = $dataArr->MAP_Air_IdleLmt1_C;
				$data[162] = $dataArr->MAP_Air_PkLmt3_C;
				$data[163] = $dataArr->MAP_Air_PkLmt1_C;
				$data[164] = round(($dataArr->Time_Upld_Frq_C / 3),0);
				$data[165] = $dataArr->gear_Confidence_C;
				$data[166] = $dataArr->gear_Fact_C;
				$data[167] = round(($dataArr->gr_1_low_C  * 10000),0);
				$data[168] = round(($dataArr->gr_1_high_C * 10000),0);
				$data[169] = round(($dataArr->gr_2_low_C  * 10000),0);
				$data[170] = round(($dataArr->gr_2_high_C * 10000),0);
				$data[171] = round(($dataArr->gr_3_low_C  * 10000),0);
				$data[172] = round(($dataArr->gr_3_high_C * 10000),0);
				$data[173] = round(($dataArr->gr_4_low_C  * 10000),0);
				$data[174] = round(($dataArr->gr_4_high_C * 10000),0);
				$data[175] = round(($dataArr->gr_5_low_C  * 10000),0);
				$data[176] = round(($dataArr->gr_5_high_C * 10000),0);
				$data[177] = round(($dataArr->gr_6_low_C  * 10000),0);
				$data[178] = round(($dataArr->gr_6_high_C * 10000),0);
				$data[179] = $dataArr->Eng_Rev_Lmt_C;
				$data[180] = $dataArr->Cool_Temp_AvgLmt_C;
				$data[181] = $dataArr->Gr_RPM_Col0_C;
				$data[182] = $dataArr->Gr_RPM_Col1_C;
				$data[183] = $dataArr->Gr_RPM_Col2_C;
				$data[184] = $dataArr->Gr_RPM_Col3_C;
				$data[185] = $dataArr->Gr_RPM_Col4_C;
				$data[186] = $dataArr->Gr_RPM_Col5_C;
				$data[187] = $dataArr->Gr_RPM_Col6_C;
				$data[188] = $dataArr->Gr_RPM_Col7_C;
				$data[189] = $dataArr->Gr_RPM_Col8_C;
				$data[190] = $dataArr->Gr_RPM_Col9_C;
				$data[191] = $dataArr->Thr_RPM_Col0_C;
				$data[192] = $dataArr->Thr_RPM_Col1_C;
				$data[193] = $dataArr->Thr_RPM_Col2_C;
				$data[194] = $dataArr->Thr_RPM_Col3_C;
				$data[195] = $dataArr->Thr_RPM_Col4_C;
				$data[196] = $dataArr->Thr_RPM_Col5_C;
				$data[197] = $dataArr->Thr_RPM_Col6_C;
				$data[198] = $dataArr->Thr_RPM_Col7_C;
				$data[199] = $dataArr->Thr_RPM_Col8_C;
				$data[200] = $dataArr->Thr_RPM_Col9_C;
				$data[201] = $dataArr->Thr_Pos_Row0_C;
				$data[202] = $dataArr->Thr_Pos_Row1_C;
				$data[203] = $dataArr->Thr_Pos_Row2_C;
				$data[204] = $dataArr->Thr_Pos_Row3_C;
				$data[205] = $dataArr->Thr_Pos_Row4_C;
				$data[206] = $dataArr->Thr_Pos_Row5_C;
				$data[207] = $dataArr->Thr_Pos_Row6_C;
				$data[208] = $dataArr->Thr_Pos_Row7_C;
				$data[209] = $dataArr->Thr_Pos_Row8_C;
				$data[210] = $dataArr->Thr_Pos_Row9_C;
				$data[211] = $dataArr->Veh_Spd_Col0_C;
				$data[212] = $dataArr->Veh_Spd_Col1_C;
				$data[213] = $dataArr->Veh_Spd_Col2_C;
				$data[214] = $dataArr->Veh_Spd_Col3_C;
				$data[215] = $dataArr->Veh_Spd_Col4_C;
				$data[216] = $dataArr->Veh_Spd_Col5_C;
				$data[217] = $dataArr->Veh_Spd_Col6_C;
				$data[218] = $dataArr->Veh_Spd_Col7_C;
				$data[219] = $dataArr->Veh_Spd_Col8_C;
				$data[220] = $dataArr->Veh_Spd_Col9_C;
				$data[221] = round(($dataArr->Spd_Chng_Neg_Row5_C * 20),0);
				$data[222] = round(($dataArr->Spd_Chng_Neg_Row4_C * 20),0);
				$data[223] = round(($dataArr->Spd_Chng_Neg_Row3_C * 20),0);
				$data[224] = round(($dataArr->Spd_Chng_Neg_Row2_C * 20),0);
				$data[225] = round(($dataArr->Spd_Chng_Neg_Row1_C * 20),0);
				$data[226] = round(($dataArr->Spd_Chng_Row0_C * 20),0);
				$data[227] = round(($dataArr->Spd_Chng_Pos_Row1_C * 20),0);
				$data[228] = round(($dataArr->Spd_Chng_Pos_Row2_C * 20),0);
				$data[229] = round(($dataArr->Spd_Chng_Pos_Row3_C * 20),0);
				$data[230] = round(($dataArr->Spd_Chng_Pos_Row4_C * 20),0);
				$data[231] = round(($dataArr->Spd_Chng_Pos_Row5_C * 20),0);
				$data[232] = $dataArr->Ovr_Spd_Lmt_C;
				$data[233] = round(($dataArr->Brake_EngSt3_Acc_C * 20),0);
				$data[234] = round(($dataArr->Brake_EngSt4_Acc_C * 20),0);
				$data[235] = $dataArr->Hlf_Clutch_Thr_C;
				$data[236] = $dataArr->Gr_Cnt_Debounce_C;
				$data[237] = $dataArr->Veh_Spd_Avl_C;
				$data[238] = $dataArr->Trq_RPM_RSE_C;
				$data[239] = $dataArr->Trq_Eload_RSE_C;
				$data[240] = $dataArr->Accln_Debounce_C;
				$data[241] = $dataArr->GPS_Spd_Plaus_C;
				$data[242] = 0;
				$data[243] = 0;
				$data[244] = 0;
				$data[245] = 0;
				$data[246] = 0;
				$data[247] = 0;
				$data[248] = 0;
				$data[249] = 0;
				
				$resultArr = array();
				$resultArr = json_decode(json_encode($data,JSON_NUMERIC_CHECK));
				// print_r($resultArr);die;

				//TODO: Update config status in DB
				Common::updateTable('device',array('id' => $did),array('dactval' => '0'));

				$result['success'] = TRUE;
				$result['data']    = array_values($resultArr);
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
     	$resultVal = json_encode($result,JSON_NUMERIC_CHECK);
		echo $resultVal;
  	}

  	public function posdataupload(Request $request)
  	{
  	 	if(!empty($request->devID))
  	 	{
  	 		$this->updatelog("posdataupload",$request->data);
			$deviceID = $request->devID;
			if ($deviceID == "1F25838") {
				$deviceID = 60000402;
			}
			elseif($deviceID == "1F255AD") {
				$deviceID = 60000403;
			}
			$timestmp = $request->ts;
			$data     = $request->data;
			$seqno    = $request->seqno;

			$deviceAvail = Fetch::devicestatus($deviceID);
			if(!empty($deviceAvail))
          	{
          		date_default_timezone_set('Asia/Kolkata');
          		$line_array = sscanf($data, "%02x%02x%02x%02x%02x%02x%02x%02x");
          		// print_r($line_array);die;
          		$msgID = ($line_array[0] & 0xE0) >> 5;

          		switch ($msgID) {
          			case '0':
          				//Get Previous Drive number
		          		$driveNoPrev  = Fetch::getLastDriveNum($deviceID);
		          		//Current drive number
		          		$driveNoCurr  = ($driveNoPrev[0]->drive) + 1;

						$latSignVal = ($line_array[0] & 0x01);
						if($latSignVal == 0)
						{
							$latSign = "+";
						}
						else
						{
							$latSign = "-";
						}
						$longSignVal = ($line_array[0] & 0x02);
						if($longSignVal == 0)
						{
							$longSign = "+";
						}
						else
						{
							$longSign = "-";
						}

						$latWholeNum = $line_array[1];
						// Latitude Decimal Numbers
						$tmpVar1  = $line_array[2];
						$tmpVar2  = $line_array[3];
						$tmpVar3  = ($line_array[4] & 0xF0) >> 4;
						$latDec   = (($tmpVar1 & 0x000000FF) << 12) | (($tmpVar2 & 0x000000FF) << 4) | (($tmpVar3 & 0x000000FF));
						$latDec   = sprintf("%05d",$latDec);
						$latitude = $latSign.$latWholeNum.'.'.$latDec;
						
						$tmpVar1 = ($line_array[4] & 0x0F);
						$tmpVar2 = ($line_array[5] & 0xF0) >> 4;
						$longWholeNum = ($tmpVar1 << 4) | $tmpVar2;
						// Longitudinal Decimal Numbers
						$tmpVar1 = ($line_array[5] & 0x0F);
						$tmpVar2 = $line_array[6];
						$tmpVar3 = $line_array[7];
						$longDec  = (($tmpVar1 & 0x000000FF) << 16) | (($tmpVar2 & 0x000000FF) << 8) | (($tmpVar3 & 0x000000FF));
						$longDec   = sprintf("%05d",$longDec);
						$longitude = $longSign.$longWholeNum.'.'.$longDec;

						$postval['device_id'] = $deviceID;
						$postval['lat']       = $latitude;
						$postval['lon']       = $longitude;
						$postval['alt']       = 0;
						$postval['time_s']    = $timestmp;
						$postval['event']     = 0;
						$postval['nodrive']   = $driveNoCurr;
						$postval['seqno']     = $seqno;
						$postval['ad_dt']     = date('Y-m-d H:i:s',time());

						$update = Fetch::insertDbSingle_event($postval);
						if($update)
						{
							$result['success'] = TRUE;
		        			$result['status']  = "Location Upload Successful!";
						}
						else
						{
							$result['success']      = FALSE;
							$result['errorCode']    = "1003";
							$result['errorMessage'] = "Location Update Failed! Please try again later.";
						}
          				break;
          			case '1':
						// This is 1st packet. It contains Drive No, Drive Duration, Drive Distance and Max Speed. Update into DB.
						$currentTime = time();
						$postVal     = array();
						$postVal['device_id'] = $deviceID;
						$postVal['time_s']    = $currentTime;
						$postVal['time_v']    = date('Y-m-d H:i:s',$currentTime);
						// Drive Number
						$tmpVar1 = $line_array[0] & 0x1F;
						$tmpVar2 = $line_array[1];
						$tmpVar3 = $line_array[2];
						$driveID = (($tmpVar1 & 0x000000FF) << 16) | (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);
						// Drive Duration
						$tmpVar2 = $line_array[3];
						$tmpVar3 = $line_array[4];
						$drvDur  = (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);
						// Drive Distance
						$tmpVar2 = $line_array[5];
						$tmpVar3 = $line_array[6];
						$drvDist = (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);
						// Max Speed
						$tmpVar3 = $line_array[7];
						$maxSpd  = ($tmpVar3 & 0x000000FF);

						$postVal['nodrive']   = $driveID;
						$postVal['dur']       = $drvDur;
						$postVal['dist']      = $drvDist/10;
						$postVal['max_sp']    = $maxSpd;
						$postVal['ad_dt']     = date('Y-m-d H:i:s');

						// echo "driveID = $driveID | drvDur = $drvDur | drvDist = $drvDist | maxSpd = $maxSpd \n";die;

		                // Calculate and Update Total Distance Travelled in "device" table
		                //Step1: Read "device" and "dashboard" table to get the current values for distance
		                $devTableArr  = $deviceAvail;//getdevice($deviceID);

						$year         = date('Y',$currentTime);
						$month        = date('n',$currentTime);
						$date         = date('j',$currentTime);
		                $dashTableArr = Fetch::getDashboardData($year,$month);
		                if(!empty($dashTableArr))
						{
							$type = 0;		//row is not present. Insert new row
						}
						else
						{
							$type = 1;		//row present. Update values
						}
		                // Step 2: Calculate total distance covered by the vehicle
		                $totalDist_devTable = $devTableArr[0]->total_dist;
		                $currDist_devTable  = $devTableArr[0]->dist_currval;
		                $drvNum_devTable    = $devTableArr[0]->dist_drvnum;

						$dDate     = "d$date";
						$dailyDist = $dashTableArr->$dDate;

		                if ($drvNum_devTable != $driveID)
		                {
		                    // New Drive. Calculate and Update Total Distance
		                    if($postVal['dist'] != 0)
		                    {
		                        $totalDist_devTable = $totalDist_devTable + $postVal['dist'];
		                        $dailyDist          = $dailyDist + $postVal['dist'];
		                        $drvNum_devTable    = $driveID;
		                        $currDist_devTable  = $postVal['dist'];
		                    }
		                    else {
		                        //Do Nothing
		                    } 
		                }
		                elseif ($drvNum_devTable == $driveID)
		                {
		                    // Continued Drive. Calculate and Update Total Distance
		                    if($postVal['dist'] > $currDist_devTable)
		                    {
		                        $totalDist_devTable = $totalDist_devTable + ($postVal['dist'] - $currDist_devTable);
		                        $dailyDist          = $dailyDist + ($postVal['dist'] - $currDist_devTable);
		                        $drvNum_devTable    = $driveID;
		                        $currDist_devTable  = $postVal['dist'];
		                    }
		                    else {
		                        //Do Nothing
		                    }
		                }
		                
		                Fetch::updateDeviceTable($deviceID,$currDist_devTable,$drvNum_devTable,$totalDist_devTable);
		                Fetch::updateDashboard($type,$year,$month,$dDate,$dailyDist);
						
		                /************************************** Save Data to DB *************************************/
						$table = 'device_data_s';
						$drvNo = Fetch::checkDriveNumForDeviceID($deviceID,$driveID,$table);
						if(!empty($drvNo))
						{
							//To update the same row in the database
							$setFields = array('device_id','nodrive');
							$setVal    = array($deviceID,$driveID);
					        $resultC   = Fetch::updateDataMultiple($table,$setFields,$setVal,$postVal);
							if($resultC)
							{
								// Data Uploaded successfully
								//update the table device_drive
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,1,$msgID))
		                        {
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1004";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1004";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						else
						{
							$saveData = Fetch::insertDbSingle($table,$postVal);
							//print_r($saveData);
							if($saveData)
							{
								// Data uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,0,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1005";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1005";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						
						/**************************************** Alerts Start **********************************/
						//Monitoring Coolant Health Low
						/*if($postVal['mon_cool_hlth'] <= 2)
						{
							generateAlertMailAdvMon($deviceID,$driveID,4,2);
						}
						/***************************************** Alerts End ***********************************/
						break;
					case '2':
						// This is 2nd packet. It contains Drive ID, PTP Health Score, Mileage, Max Torque. Update into DB.
						$postVal = array();
						$postVal['device_id'] = $deviceID;

						// Drive Number
						$tmpVar1 = $line_array[0] & 0x1F;
						$tmpVar2 = $line_array[1];
						$tmpVar3 = $line_array[2];
						$driveID = (($tmpVar1 & 0x000000FF) << 16) | (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);

						// PTP Hlt Score
						$tmpVar3     = $line_array[3];
						$ptpHltScore = ($tmpVar3 & 0x000000FF);

						// Mileage
						$tmpVar2 = $line_array[4];
						$tmpVar3 = $line_array[5];
						$mileage = (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);
						
						// Max Torque
						$tmpVar2 = $line_array[6];
						$tmpVar3 = $line_array[7];
						$maxTorq = (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);

						// echo "driveID = $driveID | ptpHltScore = $ptpHltScore | mileage = $mileage | maxTorq = $maxTorq \n";die;

						$postVal['mon_ptp_hlth'] = $ptpHltScore;
						$postVal['avg_ml']       = $mileage/100;
						$postVal['max_tor']      = $maxTorq/10;

						$table = 'device_data_s';
						$drvNo = Fetch::checkDriveNumForDeviceID($deviceID,$driveID,$table);
						if(!empty($drvNo))
						{
							//To update the same row in the database
							$setFields = array('device_id','nodrive');
							$setVal    = array($deviceID,$driveID);
					        $resultC   = Fetch::updateDataMultiple($table,$setFields,$setVal,$postVal);
							if($resultC)
							{
								// Data Uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,1,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1006";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1006";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						else
						{
							//print_r($postVal);
							$saveData = Fetch::insertDbSingle($table,$postVal);
							//print_r($saveData);
							if($saveData)
							{
								// Data Uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,0,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1007";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1007";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						break;
					case '3':
						// This is 3rd packet. It contains Drive ID,Battery Sys Score,Fuel Saved in Overrun,Duration in Overrun.Update into DB.
						$postVal = array();
						$postVal['device_id'] = $deviceID;

						// Drive Number
						$tmpVar1 = $line_array[0] & 0x1F;
						$tmpVar2 = $line_array[1];
						$tmpVar3 = $line_array[2];
						$driveID = (($tmpVar1 & 0x000000FF) << 16) | (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);

						// Battery Sys Score
						$tmpVar3     = $line_array[3];
						$batSysScore = ($tmpVar3 & 0x000000FF);
						
						// Fuel Saved in Overrun
						$tmpVar1        = $line_array[4];
						$tmpVar2        = $line_array[5];
						$fuelSaveOvrrun = (($tmpVar1 & 0x000000FF) << 8) | ($tmpVar2 & 0x000000FF);
						
						//Fuel Consumed
						$tmpVar1  = $line_array[6];
						$tmpVar2  = $line_array[7];
						$fuelCons = (($tmpVar1 & 0x000000FF) << 8) | ($tmpVar2 & 0x000000FF);


						// echo "driveID = $driveID | batSysScore = $batSysScore | fuelSaveOvrrun = $fuelSaveOvrrun | fuelCons = $fuelCons \n";die;

						$postVal['mon_bat_hlth'] = $batSysScore;
						$postVal['ful_ovr']      = $fuelSaveOvrrun/1000;
						$postVal['ful_c']        = $fuelCons/1000;

						$table = 'device_data_s';
						$drvNo = Fetch::checkDriveNumForDeviceID($deviceID,$driveID,$table);
						if(!empty($drvNo))
						{
							//To update the same row in the database
							$setFields = array('device_id','nodrive');
							$setVal    = array($deviceID,$driveID);
					        $resultC   = Fetch::updateDataMultiple($table,$setFields,$setVal,$postVal);
							if($resultC)
							{
								// Data Uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,1,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1008";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1008";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						else
						{
							//print_r($postVal);
							$saveData = Fetch::insertDbSingle($table,$postVal);
							//print_r($saveData);
							if($saveData)
							{
								// Data Uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,0,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1009";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1009";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						break;
					case '4':
						// ------------------------------------------------------------------------------------------------------------
						// This is 4th packet. It conatins Drive ID,Coolant Sys Score,Fuel Sys Score,RPM Osc Score,Idle Time,Air/Turbo 				Score,Duration in Overrun. Update into DB
						// ------------------------------------------------------------------------------------------------------------
						$postVal = array();
						$postVal['device_id'] = $deviceID;

						// Drive Number
						$tmpVar1 = $line_array[0] & 0x1F;
						$tmpVar2 = $line_array[1];
						$tmpVar3 = $line_array[2];
						$driveID = (($tmpVar1 & 0x000000FF) << 16) | (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);

						//Coolant Sys Score
						$tmpVar2      = $line_array[3] & 0xE0;
						$coolSysScore = $tmpVar2 >> 5;

						//Fuel Sys Score
						$tmpVar2    = $line_array[3] & 0x1C;
						$flSysScore = $tmpVar2 >> 2;

						//RPM Osc Sys Score
						$tmpVar2     = $line_array[4] & 0xE0;
						$rpmOscScore = $tmpVar2 >> 5;

						//Idle Time
						$tmpVar1  = ($line_array[4] & 0x1F);
						$tmpVar2  = $line_array[5];
						$idleTime = (($tmpVar1 & 0x000000FF) << 8) | ($tmpVar2 & 0x000000FF);

						//Air Sys Score
						$tmpVar2     = $line_array[6] & 0xE0;
						$airSysScore = $tmpVar2 >> 5;

						//Time spent in Overrun
						$tmpVar1   = ($line_array[6] & 0x1F);
						$tmpVar2   = $line_array[7];
						$durOvrrun = (($tmpVar1 & 0x000000FF) << 8) | ($tmpVar2 & 0x000000FF);

						// echo "driveID = $driveID | coolSysScore = $coolSysScore | flSysScore = $flSysScore | rpmOscScore = $rpmOscScore | idleTime = $idleTime | airSysScore = $airSysScore | durOvrrun = $durOvrrun \n";die;

						$postVal['mon_rpm_osc']        = $rpmOscScore;
						$postVal['mon_air_turbo_hlth'] = $airSysScore;
						$postVal['mon_cool_hlth']      = $coolSysScore;
						$postVal['idt']                = $idleTime/10;
						$postVal['dur_ovr']            = $durOvrrun/10;
		                $postVal['dur_coast_down']     = $durOvrrun/10;

		                $table = 'device_data_s';
						$drvNo = Fetch::checkDriveNumForDeviceID($deviceID,$driveID,$table);
						if(!empty($drvNo))
						{
							//To update the same row in the database
							$setFields = array('device_id','nodrive');
							$setVal    = array($deviceID,$driveID);
					        $resultC   = Fetch::updateDataMultiple($table,$setFields,$setVal,$postVal);
							if($resultC)
							{
								// Data Uploaded successfully
								//update the table device_drive
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,1,$msgID))
		                       {
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1010";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1010";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						else
						{
							//print_r($postVal);
							$saveData = Fetch::insertDbSingle($table,$postVal1);
							//print_r($saveData);
							if($saveData)
							{
								// Data Uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,0,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1011";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1011";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						break;
					default:
						// Do nothing;
						break;
          		}
          		
		  	}
		  	else
		  	{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
		    }
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal;
	}
/*
  	public function posdataupload1(Request $request)
  	{
  	 	if(!empty($request->devID))
  	 	{
			$deviceID = $request->devID;
			if ($deviceID == "1F25838") {
				$deviceID = 60000401;
			}
			elseif($deviceID == "1F255AD") {
				$deviceID = 60000402;
			}
			$timestmp = $request->ts;
			$data     = $request->data;
			$seqno    = $request->seqno;

			$deviceAvail = Fetch::devicestatus($deviceID);
			if(!empty($deviceAvail))
          	{
          		date_default_timezone_set('Asia/Kolkata');
          		$line_array = sscanf($data, "%02x%02x%02x%02x%02x%02x%02x%02x");
          		// print_r($line_array);die;
          		$msgID = ($line_array[0] & 0xE0) >> 5;

          		switch ($msgID) {
          			case '0':
          				//Get Previous Drive number
		          		$driveNoPrev  = Fetch::getLastDriveNum($deviceID);
		          		//Current drive number
		          		$driveNoCurr  = ($driveNoPrev[0]->drive) + 1;

						$latSignVal = ($line_array[0] & 0x01);
						if($latSignVal == 0)
						{
							$latSign = "+";
						}
						else
						{
							$latSign = "-";
						}
						$longSignVal = ($line_array[0] & 0x02);
						if($longSignVal == 0)
						{
							$longSign = "+";
						}
						else
						{
							$longSign = "-";
						}

						$latWholeNum = $line_array[1];
						// Latitude Decimal Numbers
						$tmpVar1  = $line_array[2];
						$tmpVar2  = $line_array[3];
						$tmpVar3  = ($line_array[4] & 0xF0) >> 4;
						$latDec   = (($tmpVar1 & 0x000000FF) << 12) | (($tmpVar2 & 0x000000FF) << 4) | (($tmpVar3 & 0x000000FF));
						$latDec   = sprintf("%05d",$latDec);
						$latitude = $latSign.$latWholeNum.'.'.$latDec;
						
						$tmpVar1 = ($line_array[4] & 0x0F);
						$tmpVar2 = ($line_array[5] & 0xF0) >> 4;
						$longWholeNum = ($tmpVar1 << 4) | $tmpVar2;
						// Longitudinal Decimal Numbers
						$tmpVar1 = ($line_array[5] & 0x0F);
						$tmpVar2 = $line_array[6];
						$tmpVar3 = $line_array[7];
						$longDec  = (($tmpVar1 & 0x000000FF) << 16) | (($tmpVar2 & 0x000000FF) << 8) | (($tmpVar3 & 0x000000FF));
						$longDec   = sprintf("%05d",$longDec);
						$longitude = $longSign.$longWholeNum.'.'.$longDec;

						$postval['device_id'] = $deviceID;
						$postval['lat']       = $latitude;
						$postval['lon']       = $longitude;
						$postval['alt']       = 0;
						$postval['time_s']    = $timestmp;
						$postval['event']     = 0;
						$postval['nodrive']   = $driveNoCurr;
						$postval['seqno']     = $seqno;
						$postval['ad_dt']     = date('Y-m-d H:i:s',time());

						$update = Fetch::insertDbSingle_event($postval);
						if($update)
						{
							$result['success'] = TRUE;
		        			$result['status']  = "Location Upload Successful!";
						}
						else
						{
							$result['success']      = FALSE;
							$result['errorCode']    = "1003";
							$result['errorMessage'] = "Location Update Failed! Please try again later.";
						}
          				break;
          			case '1':
						// This is 1st packet. It contains Drive No, Drive Duration, Drive Distance and Max Speed. Update into DB.
						$currentTime = time();
						$postVal     = array();
						$postVal['device_id'] = $deviceID;
						$postVal['time_s']    = $currentTime;
						$postVal['time_v']    = date('Y-m-d H:i:s',$currentTime);
						// Drive Number
						$tmpVar1 = $line_array[0] & 0x1F;
						$tmpVar2 = $line_array[1];
						$tmpVar3 = $line_array[2];
						$driveID = (($tmpVar1 & 0x000000FF) << 16) | (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);
						// Drive Duration
						$tmpVar2 = $line_array[3];
						$tmpVar3 = $line_array[4];
						$drvDur  = (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);
						// Drive Distance
						$tmpVar2 = $line_array[5];
						$tmpVar3 = $line_array[6];
						$drvDist = (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);
						// Max Speed
						$tmpVar3 = $line_array[7];
						$maxSpd  = ($tmpVar3 & 0x000000FF);

						$postVal['nodrive']   = $driveID;
						$postVal['dur']       = $drvDur;
						$postVal['dist']      = $drvDist/10;
						$postVal['max_sp']    = $maxSpd;
						$postVal['ad_dt']     = date('Y-m-d H:i:s');

						// echo "driveID = $driveID | drvDur = $drvDur | drvDist = $drvDist | maxSpd = $maxSpd \n";die;

		                // Calculate and Update Total Distance Travelled in "device" table
		                //Step1: Read "device" and "dashboard" table to get the current values for distance
		                $devTableArr  = $deviceAvail;//getdevice($deviceID);

						$year         = date('Y',$currentTime);
						$month        = date('n',$currentTime);
						$date         = date('j',$currentTime);
		                $dashTableArr = Fetch::getDashboardData($year,$month);
		                if(!empty($dashTableArr))
						{
							$type = 0;		//row is not present. Insert new row
						}
						else
						{
							$type = 1;		//row present. Update values
						}
		                // Step 2: Calculate total distance covered by the vehicle
		                $totalDist_devTable = $devTableArr[0]->total_dist;
		                $currDist_devTable  = $devTableArr[0]->dist_currval;
		                $drvNum_devTable    = $devTableArr[0]->dist_drvnum;

						$dDate     = "d$date";
						$dailyDist = $dashTableArr->$dDate;

		                if ($drvNum_devTable != $driveID)
		                {
		                    // New Drive. Calculate and Update Total Distance
		                    if($postVal['dist'] != 0)
		                    {
		                        $totalDist_devTable = $totalDist_devTable + $postVal['dist'];
		                        $dailyDist          = $dailyDist + $postVal['dist'];
		                        $drvNum_devTable    = $driveID;
		                        $currDist_devTable  = $postVal['dist'];
		                    }
		                    else {
		                        //Do Nothing
		                    } 
		                }
		                elseif ($drvNum_devTable == $driveID)
		                {
		                    // Continued Drive. Calculate and Update Total Distance
		                    if($postVal['dist'] > $currDist_devTable)
		                    {
		                        $totalDist_devTable = $totalDist_devTable + ($postVal['dist'] - $currDist_devTable);
		                        $dailyDist          = $dailyDist + ($postVal['dist'] - $currDist_devTable);
		                        $drvNum_devTable    = $driveID;
		                        $currDist_devTable  = $postVal['dist'];
		                    }
		                    else {
		                        //Do Nothing
		                    }
		                }
		                
		                Fetch::updateDeviceTable($deviceID,$currDist_devTable,$drvNum_devTable,$totalDist_devTable);
		                Fetch::updateDashboard($type,$year,$month,$dDate,$dailyDist);
						
		                //------------------------------------- Save Data to DB -------------------------------------//
						$table = 'device_data_s';
						$drvNo = Fetch::checkDriveNumForDeviceID($deviceID,$driveID,$table);
						if(!empty($drvNo))
						{
							//To update the same row in the database
							$setFields = array('device_id','nodrive');
							$setVal    = array($deviceID,$driveID);
					        $resultC   = Fetch::updateDataMultiple($table,$setFields,$setVal,$postVal);
							if($resultC)
							{
								// Data Uploaded successfully
								//update the table device_drive
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,1,$msgID))
		                        {
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1004";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1004";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						else
						{
							$saveData = Fetch::insertDbSingle($table,$postVal);
							//print_r($saveData);
							if($saveData)
							{
								// Data uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,0,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1005";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1005";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						
						//------------------------------------- Alerts Start -------------------------------------//
						//Monitoring Coolant Health Low
						if($postVal['mon_cool_hlth'] <= 2)
						{
							generateAlertMailAdvMon($deviceID,$driveID,4,2);
						}
						//------------------------------------- Alerts End -------------------------------------//
						break;
					case '2':
						// This is 2nd packet. It contains Drive ID, PTP Health Score, Mileage, Max Torque. Update into DB.
						$postVal = array();
						$postVal['device_id'] = $deviceID;

						// Drive Number
						$tmpVar1 = $line_array[0] & 0x1F;
						$tmpVar2 = $line_array[1];
						$tmpVar3 = $line_array[2];
						$driveID = (($tmpVar1 & 0x000000FF) << 16) | (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);

						// PTP Hlt Score
						$tmpVar3     = $line_array[3];
						$ptpHltScore = ($tmpVar3 & 0x000000FF);

						// Mileage
						$tmpVar2 = $line_array[4];
						$tmpVar3 = $line_array[5];
						$mileage = (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);
						
						// Max Torque
						$tmpVar2 = $line_array[6];
						$tmpVar3 = $line_array[7];
						$maxTorq = (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);

						// echo "driveID = $driveID | ptpHltScore = $ptpHltScore | mileage = $mileage | maxTorq = $maxTorq \n";die;

						$postVal['mon_ptp_hlth'] = $ptpHltScore;
						$postVal['avg_ml']       = $mileage/100;
						$postVal['max_tor']      = $maxTorq/10;

						$table = 'device_data_s';
						$drvNo = Fetch::checkDriveNumForDeviceID($deviceID,$driveID,$table);
						if(!empty($drvNo))
						{
							//To update the same row in the database
							$setFields = array('device_id','nodrive');
							$setVal    = array($deviceID,$driveID);
					        $resultC   = Fetch::updateDataMultiple($table,$setFields,$setVal,$postVal);
							if($resultC)
							{
								// Data Uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,1,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1006";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1006";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						else
						{
							//print_r($postVal);
							$saveData = Fetch::insertDbSingle($table,$postVal);
							//print_r($saveData);
							if($saveData)
							{
								// Data Uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,0,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1007";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1007";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						break;
					case '3':
						// This is 3rd packet. It contains Drive ID,Battery Sys Score,Fuel Saved in Overrun,Duration in Overrun.Update into DB.
						$postVal = array();
						$postVal['device_id'] = $deviceID;

						// Drive Number
						$tmpVar1 = $line_array[0] & 0x1F;
						$tmpVar2 = $line_array[1];
						$tmpVar3 = $line_array[2];
						$driveID = (($tmpVar1 & 0x000000FF) << 16) | (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);

						// Battery Sys Score
						$tmpVar3     = $line_array[3];
						$batSysScore = ($tmpVar3 & 0x000000FF);
						
						// Fuel Saved in Overrun
						$tmpVar1        = $line_array[4];
						$tmpVar2        = $line_array[5];
						$fuelSaveOvrrun = (($tmpVar1 & 0x000000FF) << 8) | ($tmpVar2 & 0x000000FF);
						
						//Fuel Consumed
						$tmpVar1  = $line_array[6];
						$tmpVar2  = $line_array[7];
						$fuelCons = (($tmpVar1 & 0x000000FF) << 8) | ($tmpVar2 & 0x000000FF);


						// echo "driveID = $driveID | batSysScore = $batSysScore | fuelSaveOvrrun = $fuelSaveOvrrun | fuelCons = $fuelCons \n";die;

						$postVal['mon_bat_hlth'] = $batSysScore;
						$postVal['ful_ovr']      = $fuelSaveOvrrun/1000;
						$postVal['ful_c']        = $fuelCons/1000;

						$table = 'device_data_s';
						$drvNo = Fetch::checkDriveNumForDeviceID($deviceID,$driveID,$table);
						if(!empty($drvNo))
						{
							//To update the same row in the database
							$setFields = array('device_id','nodrive');
							$setVal    = array($deviceID,$driveID);
					        $resultC   = Fetch::updateDataMultiple($table,$setFields,$setVal,$postVal);
							if($resultC)
							{
								// Data Uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,1,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1008";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1008";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						else
						{
							//print_r($postVal);
							$saveData = Fetch::insertDbSingle($table,$postVal);
							//print_r($saveData);
							if($saveData)
							{
								// Data Uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,0,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1009";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1009";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						break;
					case '4':
						// ------------------------------------------------------------------------------------------------------------
						// This is 4th packet. It conatins Drive ID,Coolant Sys Score,Fuel Sys Score,RPM Osc Score,Idle Time,Air/Turbo 				Score,Duration in Overrun. Update into DB
						// ------------------------------------------------------------------------------------------------------------
						$postVal = array();
						$postVal['device_id'] = $deviceID;

						// Drive Number
						$tmpVar1 = $line_array[0] & 0x1F;
						$tmpVar2 = $line_array[1];
						$tmpVar3 = $line_array[2];
						$driveID = (($tmpVar1 & 0x000000FF) << 16) | (($tmpVar2 & 0x000000FF) << 8) | ($tmpVar3 & 0x000000FF);

						//Coolant Sys Score
						$tmpVar2      = $line_array[3] & 0xE0;
						$coolSysScore = $tmpVar2 >> 5;

						//Fuel Sys Score
						$tmpVar2    = $line_array[3] & 0x1C;
						$flSysScore = $tmpVar2 >> 2;

						//RPM Osc Sys Score
						$tmpVar2     = $line_array[4] & 0xE0;
						$rpmOscScore = $tmpVar2 >> 5;

						//Idle Time
						$tmpVar1  = ($line_array[4] & 0x1F);
						$tmpVar2  = $line_array[5];
						$idleTime = (($tmpVar1 & 0x000000FF) << 8) | ($tmpVar2 & 0x000000FF);

						//Air Sys Score
						$tmpVar2     = $line_array[6] & 0xE0;
						$airSysScore = $tmpVar2 >> 5;

						//Time spent in Overrun
						$tmpVar1   = ($line_array[6] & 0x1F);
						$tmpVar2   = $line_array[7];
						$durOvrrun = (($tmpVar1 & 0x000000FF) << 8) | ($tmpVar2 & 0x000000FF);

						// echo "driveID = $driveID | coolSysScore = $coolSysScore | flSysScore = $flSysScore | rpmOscScore = $rpmOscScore | idleTime = $idleTime | airSysScore = $airSysScore | durOvrrun = $durOvrrun \n";die;

						$postVal['mon_rpm_osc']        = $rpmOscScore;
						$postVal['mon_air_turbo_hlth'] = $airSysScore;
						$postVal['mon_cool_hlth']      = $coolSysScore;
						$postVal['idt']                = $idleTime/10;
						$postVal['dur_ovr']            = $durOvrrun/10;
		                $postVal['dur_coast_down']     = $durOvrrun/10;

		                $table = 'device_data_s';
						$drvNo = Fetch::checkDriveNumForDeviceID($deviceID,$driveID,$table);
						if(!empty($drvNo))
						{
							//To update the same row in the database
							$setFields = array('device_id','nodrive');
							$setVal    = array($deviceID,$driveID);
					        $resultC   = Fetch::updateDataMultiple($table,$setFields,$setVal,$postVal);
							if($resultC)
							{
								// Data Uploaded successfully
								//update the table device_drive
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,1,$msgID))
		                       {
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1010";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1010";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						else
						{
							//print_r($postVal);
							$saveData = Fetch::insertDbSingle($table,$postVal1);
							//print_r($saveData);
							if($saveData)
							{
								// Data Uploaded successfully
								if($this->updateDataDBRes($deviceID,$driveID,$postVal,0,$msgID))
								{
		                            //Statistics and monitoring data uploaded successfully
									$result['success']    = TRUE;
				        			$result['successmsg'] = "Data Upload Successful!";
		                        }
		                        else
		                        {
		                            //Data failed to upload
		                            $result['success']      = FALSE;
									$result['errorCode']    = "1011";
									$result['errorMessage'] = "Update Failed! Please try again later.";
		                        }
							}
							else
							{
								//Data failed to upload
	                            $result['success']      = FALSE;
								$result['errorCode']    = "1011";
								$result['errorMessage'] = "Update Failed! Please try again later.";
							}
						}
						break;
					default:
						// Do nothing;
					break;
          		}
		  	}
		  	else
		  	{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
		    }
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal; 
	}
*/

/*
  	public function posdataupload(Request $request)
  	{
  	 	if(!empty($request->devID))
  	 	{
			$deviceID = $request->devID;
			if ($deviceID == "1F25838") {
				$deviceID = 60000401;
			}
			elseif($deviceID == "1F255AD") {
				$deviceID = 60000402;
			}
			$timestmp = $request->ts;
			$data     = $request->data;
			$seqno    = $request->seqno;

			$deviceAvail = Fetch::devicestatus($deviceID);
			if(!empty($deviceAvail))
          	{
          		date_default_timezone_set('Asia/Kolkata');
          		//Get Previous Drive number
          		$driveNoPrev  = Fetch::getLastDriveNum($deviceID);
          		//Current drive number
          		$driveNoCurr  = ($driveNoPrev[0]->drive) + 1;
          		$latitudehex  = substr($data, 0, 8);
          		$longitudehex = substr($data, 8, 16);
          		$latitude  = round(($this->hex2float($latitudehex)),6);
          		$longitude = round(($this->hex2float($longitudehex)),6);
				
				$postval['device_id'] = $deviceID;
				$postval['lat']       = $latitude;
				$postval['lon']       = $longitude;
				$postval['alt']       = 0;
				$postval['time_s']    = $timestmp;
				$postval['event']     = 0;
				$postval['nodrive']   = $driveNoCurr;
				$postval['seqno']     = $seqno;
				$postval['ad_dt']     = date('Y-m-d H:i:s',time());

				$update = Fetch::insertDbSingle_event($postval);
				if($update)
				{
					$result['success'] = TRUE;
        			$result['status']  = "Data Upload Successful!";
				}
				else
				{
					$result['success']      = FALSE;
					$result['errorCode']    = "1003";
					$result['errorMessage'] = "Update Failed! Please try again later.";
				}
		  	}
		  	else
		  	{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
		    }
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal;
	}
*/

  		public function rawdataupload(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
		    //If there are empty strings in the incoming JSON, json_decode will fail. Find and replace empty string with 0
		    $json     = str_replace(",,", ",0,", $datMe);
			$array    = json_decode($json, true);
			$deviceID = $array['devID'];
		    // print_r($array);die;
			if(ctype_alnum($deviceID))
      		{
		       // Get Device Data
				$devInfo = Fetch::devicestatus($deviceID);
				if(!empty($devInfo))
				{
					$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/devrawdataupload_back_sigfox.php '$datMe'";
					exec($cmd . " > /dev/null &");
					
					$result['success'] = TRUE;
					$result['status']  = "Data uploaded successfully!";
				}
				else
				{
					$result['success']      = FALSE;
					$result['errorCode']    = "1002";
					$result['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				$result['success'] = TRUE;
				$result['status']  = "Data uploaded successfully!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal;
  	}

  	public function eodDataUpload(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$this->updatelog("eodDataUpload",$datMe);
		    //If there are empty strings in the incoming JSON, json_decode will fail. Find and replace empty string with 0
		    $json     = str_replace(",,", ",0,", $datMe);
			$array    = json_decode($json, true);
			$deviceID = $array['devID'];
		    // print_r($array);die;

		    // Get Device Data
			$devInfo = Fetch::devicestatus($deviceID);
			if(!empty($devInfo))
			{
				$cmd = "/usr/bin/timeout 60m /usr/bin/php /var/www/html/eva.enginecal.com/api/eod_data_process_sigfox.php '$datMe'";
				exec($cmd . " > /dev/null &");
				
				$result['success'] = TRUE;
				$result['status']  = "Data uploaded successfully!";
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal;
  	}
/*
  	public function dtcdataupload(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$obj      = json_decode($datMe, TRUE);
			$deviceID = $obj['devID'];
			$dtc      = $obj['dtcData'];

			// Get Device Data
			$devInfo = Fetch::devicestatus($deviceID);
			if(!empty($devInfo))
			{
				$DTCAvail = $devInfo[0]->dtcv;
				if ($DTCAvail == $dtc)
				{
					//Do nothing as the dtc is same as in the server.
					$result['success'] = TRUE;
					$result['msg']     = "DTC Update not required in DB!";
				}
				else
				{
					$update  = Fetch::updatedeviceDtcv($deviceID,$dtc,1);
					if($update)
					{
						$result['success']    = TRUE;
	        			$result['successmsg'] = "Data Upload Successful!";
					}
					else
					{
						$result['success']      = FALSE;
						$result['errorCode']    = "1003";
						$result['errorMessage'] = "Update Failed! Please try again later.";
					}
				}
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal;
  	}
*/
  	function updateDataDBRes($deviceID,$driveID,$array,$update,$msgID)
	{
		$array['dt']      = date('Y-m-d');
		$array['drive']   = $driveID;
		$array['nodrive'] = $driveID;
		if($update == 1)
		{
			//update the table
	        //To update the same row in the database
	        $setFields = array('device_id','drive');
	        $setVal    = array($deviceID,$driveID);
	        $resultC   = Fetch::updateDataMultiple('device_drive',$setFields,$setVal,$array);
	        if($resultC)
	        {
	            // Data Uploaded successfully
	            $retVal = TRUE;
	        }
	        else
	        {
	            // Data failed to upload
	            $retVal = FALSE;
	        }
		}
		else
		{
			//insert in the table
			//Save stats data into device drive
			// $oilLifeDataArr          = calcOilEngLife($driveID,$deviceID,$dt,$durTr);
			// $array['oillife']        = $oilLifeDataArr[0];
			// $array['oillifep']       = $oilLifeDataArr[1];
			// $array['serviceduekm']   = $oilLifeDataArr[2];
			// $array['serviceduedays'] = $oilLifeDataArr[3];
			$array['servduedayupDt'] = date('Y-m-d');
			$rowOil = Fetch::getOilLifeDist($deviceID);
			$postValDrive['OilLifeDist'] = $rowOil[0]->OilLifeDist + $array['dist'];

	        $array['dfrm'] = date('Y-m-d H:i:s');
	        $array['dto']  = date('Y-m-d H:i:s');
			$saveData      = Fetch::insertDbSingle('device_drive',$array);
	        if($saveData)
			{
				//Database update successfull
				$retVal = TRUE;
			}
			else
			{
				//Error while Updating Database
				$retVal = FALSE;
			}
		}
		
		//===================================== Monitoring Algorithm Results Starts ==================================//
		if($msgID > 1)
		{
			$monRes    = array();
			// Check for all the monitoring flags and update the device_monitor table accordingly.

		    $calValArr       = Fetch::getCaliPramVal($deviceID);
		    $calValArr       = json_decode($calValArr['calval']);
		    $Osc_RPM_Lmt1_C  = $calValArr->Osc_RPM_Lmt1_C;
		    $Osc_RPM_Lmt2_C  = $calValArr->Osc_RPM_Lmt2_C;
		    $Osc_RPM_Lmt3_C  = $calValArr->Osc_RPM_Lmt3_C;
		    $Osc_RPM_Lmt4_C  = $calValArr->Osc_RPM_Lmt4_C;
		    $Max_Torque_C    = $calValArr->Max_Torque_C;
		    $baro_press_C    = $calValArr->Baro_Press_City_C;
		    $Trq_RPM_RSE_C   = $calValArr->Trq_RPM_RSE_C;
		    $Trq_Eload_RSE_C = $calValArr->Trq_Eload_RSE_C;
			
			$sqlprevresults = Fetch::getLastMonitorResult($deviceID);
			$sqlprevresults = $sqlprevresults[0];
		    if(!empty($sqlprevresults))
			{
				$bat     = $sqlprevresults->bat_st;
				$cool    = $sqlprevresults->coolp;
				$rpm     = $sqlprevresults->eng_block_ej;
				$airidle = $sqlprevresults->idlbc_v;
				$airpeak = $sqlprevresults->peakb_v;
				$comb    = $sqlprevresults->comp_pres;

				// After checking all the monitoring flags, update the database table
				$monRes['device_id'] = $deviceID;
				$monRes['dt']    = $array['dt'];
				$monRes['drive'] = $driveID;
				$monRes['ad_dt'] = date('Y-m-d H:i:s');

				if($msgID == 2)
				{
					// Update Combustion Pressure Rating in device_monitor table
					$CompPresScore = $array['mon_ptp_hlth'];
					if($CompPresScore == 0)
					{
						$CompPresScore = $comb;
					}
					else
					{
						$CompPresScore = $array['mon_ptp_hlth'];
					}
					//echo "comb score = $CompPresScore	\n";
					if($CompPresScore >= 85)
					{
						$monRes['comp_pres'] = 5;
					}
					elseif($CompPresScore >= 73)
					{
						$monRes['comp_pres'] = 4;
					}
					elseif($CompPresScore >= 63)
					{
						$monRes['comp_pres'] = 3;
					}
					elseif($CompPresScore >= 55)
					{
						$monRes['comp_pres'] = 2;
					}
					else
					{
						$monRes['comp_pres'] = 1;
					}
						
					// Update false odometer in device_monitor table
					$monRes['falsemeter'] = "Odometer reading Okay";
					
					// Update Vehicle Speed Sensor in device_monitor table
					$monRes['vehspeed'] = 1;
				}
				elseif($msgID == 3)
				{
					// Update Battery Rating in device_monitor table - i.e bat_st = 1
					if($array['mon_bat_hlth'] == 0)
					{
						$monRes['bat_st'] = $bat;
					}
					else
					{
						$monRes['bat_st'] = $array['mon_bat_hlth'];
					}
				}
				elseif($msgID == 4)
				{
					// Update Coolant System Rating in device_monitor table
					if($array['mon_cool_hlth'] == 0)
					{
						$monRes['coolp'] = $cool;
					}
					else
					{
						$monRes['coolp'] = $array['mon_cool_hlth'];
					}
					
					// Update RPM Oscillation Rating in device_monitor table
					if($array['mon_rpm_osc'] == 0)
					{
						$monRes['eng_block_ej'] = $rpm;
					}
					else
					{
						$monRes['eng_block_ej'] = $array['mon_rpm_osc'];
					}
					
					// Update Turbo/Air System Rating in device_monitor table
					if($array['mon_air_turbo_hlth'] == 0)
					{
						$monRes['idlbc_v'] = $airidle;
						$monRes['peakb_v'] = $airpeak;
					}
					else
					{
						$AirTurboScore = $array['mon_air_turbo_hlth'];
				        // Update Air System Rating in device_monitor table
				        if($AirTurboScore == 1)
				        {
				            $monRes['idlbc_v'] = 1;
				            $monRes['peakb_v'] = 1;
				        }
				        elseif($AirTurboScore == 2)
				        {
				            $monRes['idlbc_v'] = 1;
				            $monRes['peakb_v'] = 2;
				        }
				        elseif($AirTurboScore == 3)
				        {
				            $monRes['idlbc_v'] = 3;
				            $monRes['peakb_v'] = 2;
				        }
				        elseif($AirTurboScore == 4)
				        {
				            $monRes['idlbc_v'] = 2;
				            $monRes['peakb_v'] = 3;
				        }
				        elseif($AirTurboScore == 5)
				        {
				            $monRes['idlbc_v'] = 3;
				            $monRes['peakb_v'] = 3;
				        }
				        else
				        {
				            //Do nothing
				        }
					}
				}

				//Update or Insert into DB			
				if($driveID == ($sqlprevresults->drive))
				{
			        //To update the same row in the database
			        $setFields = array('device_id','drive');
			        $setVal    = array($deviceID,$driveID);
			        $resultC   = Fetch::updateDataMultiple('device_monitor',$setFields,$setVal,$monRes);
			        if($resultC)
			        {
			            // Data Uploaded successfully
			            $retVal = TRUE;
			        }
			        else
			        {
			            // Data failed to upload
			            $retVal = FALSE;
			        }
				}
				else
				{
					//To insert new row in the table
					$saveData = Fetch::insertDbSingle('device_monitor',$monRes);
					if($saveData)
					{
						// $retVal = TRUE;
					}
					else
					{
						// $retVal = FALSE;
					}
				}
			}
			else
			{
				if($msgID == 2)
				{
					// Update Combustion Pressure Rating in device_monitor table
					$CompPresScore = $array['mon_ptp_hlth'];
					if($array['mon_ptp_hlth'] == 0)
					{
						$CompPresScore = 0;
					}
					else
					{
						$CompPresScore = $array['mon_ptp_hlth'];
					}

					if($CompPresScore >= 85)
					{
						$monRes['comp_pres'] = 5;
					}
					elseif($CompPresScore >= 73)
					{
						$monRes['comp_pres'] = 4;
					}
					elseif($CompPresScore >= 63)
					{
						$monRes['comp_pres'] = 3;
					}
					elseif($CompPresScore >= 55)
					{
						$monRes['comp_pres'] = 2;
					}
					elseif($CompPresScore > 0)
					{
						$monRes['comp_pres'] = 1;
					}
					else
					{
						$monRes['comp_pres'] = 5;
					}
						
					// Update false odometer in device_monitor table
					$monRes['falsemeter'] = "Odometer reading Okay";
					
					// Update Vehicle Speed Sensor in device_monitor table
					$monRes['vehspeed'] = 1;
				}
				elseif($msgID == 3)
				{
					// Update Battery Rating in device_monitor table - i.e bat_st = 1
					if($array['mon_bat_hlth'] == 0)
					{
						$monRes['bat_st'] = 5;
					}
					else
					{
						$monRes['bat_st'] = $array['mon_bat_hlth'];
					}
				}
				elseif($msgID == 4)
				{
					// Update Coolant System Rating in device_monitor table
					if($array['mon_cool_hlth'] == 0)
					{
						$monRes['coolp'] = 5;
					}
					else
					{
						$monRes['coolp'] = $array['mon_cool_hlth'];
					}
					
					// Update RPM Oscillation Rating in device_monitor table
					if($array['mon_rpm_osc'] == 0)
					{
						$monRes['eng_block_ej'] = 5;
					}
					else
					{
						$monRes['eng_block_ej'] = $array['mon_rpm_osc'];
					}
					
					// Update Turbo/Air System Rating in device_monitor table
					if($array['mon_air_turbo_hlth'] == 0)
					{
						$monRes['idlbc_v'] = 3;
						$monRes['peakb_v'] = 3;
					}
					else
					{
						$AirTurboScore = $array['mon_air_turbo_hlth'];
				        // Update Air System Rating in device_monitor table
				        if($AirTurboScore == 1)
				        {
				            $monRes['idlbc_v'] = 1;
				            $monRes['peakb_v'] = 1;
				        }
				        elseif($AirTurboScore == 2)
				        {
				            $monRes['idlbc_v'] = 1;
				            $monRes['peakb_v'] = 2;
				        }
				        elseif($AirTurboScore == 3)
				        {
				            $monRes['idlbc_v'] = 3;
				            $monRes['peakb_v'] = 2;
				        }
				        elseif($AirTurboScore == 4)
				        {
				            $monRes['idlbc_v'] = 2;
				            $monRes['peakb_v'] = 3;
				        }
				        elseif($AirTurboScore == 5)
				        {
				            $monRes['idlbc_v'] = 3;
				            $monRes['peakb_v'] = 3;
				        }
				        else
				        {
				            //Do nothing
				        }
					}
				}

		        //Insert new row in the table
		        $saveData = Fetch::insertDbSingle('device_monitor',$monRes);
				if($saveData)
				{
					$retVal = TRUE;
				}
				else
				{
					$retVal = FALSE;
				}
			}
		}
		else
		{
			//Do nothing
		}
		//===================================== Monitoring Algorithm Results Ends ==================================//
		return $retVal;
	}
	
	//===================================== Get Device Last loaction API STARTS==================================//
	public function getlastloc(Request $request)
	{
        if(!empty($request->data))
        {
        	// print_r($request->data);
        	$this->updatelog("getlastloc",$request->data);
	        $data     = json_decode($request->data, TRUE);
	        $deviceID = $data['devID'];
	        $devInfo  = Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
            	$lastDriveData = Fetch::locationLastApi($deviceID);
            	if(!empty($lastDriveData))
	            {
            		$arrayData['success'] = TRUE;
	                $arrayData['lat']     = $lastDriveData[0]->lat;
	                $arrayData['long']    = $lastDriveData[0]->lon;
	                $arrayData['alt']     = $lastDriveData[0]->alt;
	                $arrayData['ts']      = $lastDriveData[0]->time_s;
		        }
		        else
	            {
	                $arrayData['success']      = FALSE;
	                $arrayData['errorCode']    = "1003";
	                $arrayData['errorMessage'] = "No location info available!";
	            }
            }
            else
            {
                $arrayData['success']      = FALSE;
                $arrayData['errorCode']    = "1002";
                $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
            $arrayData['success']      = FALSE;
            $arrayData['errorCode']    = "1001";
            $arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData);
    }
    //===================================== Get Device Last loaction API ENDS ==================================//
    
    //===================================== Get Device drive loaction API STARTS ==================================//
    public function device_location_drive(Request $request)
	{
        if(!empty($request->data))
        {    
        	$this->updatelog("device_location_drive",$request->data);
            $data     = json_decode($request->data, TRUE);
            $deviceID = $data['devID'];
            $devInfo  = Fetch::getdevice($deviceID);
            if(!empty($devInfo))
            {
	            if(array_key_exists('driveId',$data))
	            {
	                $drivenum             = $data['driveId'];
	                $arrayData['success'] = TRUE;
	                $dataLoc              = Fetch::devDriveNumDataApi($deviceID,$drivenum);

	                if(count($dataLoc) >= 1)
	                {
	                 	$arrayData['data'] = $dataLoc;
	                }
	                else
	                {
	            	    $arrayData['success']      = FALSE;
	                    $arrayData['errorCode']    = "1003";
	                    $arrayData['errorMessage'] = "No location Info!";
	                }
	            }
	            elseif(array_key_exists('driveidstart',$data) && array_key_exists('driveidend',$data))
	            {
	            	$driverangefrm = $data['driveidstart'];
	                $driverangeto  = $data['driveidend'];
	                if($driverangefrm > $driverangeto)
	                {
	                	$arrayData['success']      = FALSE;
	                    $arrayData['errorCode']    = "1001";
	                    $arrayData['errorMessage'] = "Invalid Input!";
	                }
	                else
	                {
		                $dataLoc = Fetch::devDriveRangeDataApi($deviceID,$driverangefrm,$driverangeto);
		                if(count($dataLoc) >= 1)
		                {
		                	$arrayData['success'] = TRUE;
		                	$arrayData['data']    = $dataLoc;
		                }
		                else
		                {
		                    $arrayData['success']      = FALSE;
		                    $arrayData['errorCode']    = "1003";
		                    $arrayData['errorMessage'] = "No location Info!";
		                }
	                }
                }
                else
                {
                  $arrayData['success']      = FALSE;
                  $arrayData['errorCode']    = "1001";
                  $arrayData['errorMessage'] = "Invalid Input!";
                }
            }
            else
            {
              $arrayData['success']      = FALSE;
              $arrayData['errorCode']    = "1002";
              $arrayData['errorMessage'] = "Invalid Device ID!";
            }
        }
        else
        {
        	$arrayData['success']      = FALSE;
        	$arrayData['errorCode']    = "1001";
        	$arrayData['errorMessage'] = "Invalid Input!";
        }
        echo json_encode($arrayData); 
	}
	//===================================== Get Device drive loaction API ENDS ==================================//
	//===================================== Update Device loaction in event table starts ==================================//
	public function device_event(Request $request)
	{
		$datMe = file_get_contents("php://input");
        // print_r($datMe);die;
		$datMe = urldecode($datMe);
		$pos = strpos($datMe,"{");
		$datMe = substr($datMe, $pos);

		//if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
		if(isset($datMe) && $datMe !="")
		{
		    //If there are empty strings in the incoming JSON, json_decode will fail. Find and replace empty string with 0
		    $json = str_replace(",,", ",0,", $datMe);
		    $array = json_decode($json, true);
		    $deviceID = $array['devID'];
		    // $regex = is_numeric('[@_!#$%^&*()<>?/|}{~:]', $deviceID);
      		if(ctype_alnum($deviceID))
      		{
         		 // get Device Data
			    $devInfo  = Fetch::getdevice($deviceID);
			    if(!empty($devInfo))
			    {
			        // $gpsData = array();
			        $gpsData['device_id'] = $array['devID'];
			        $gpsData['lat'] = $array['lat'];
			        $gpsData['lon'] = $array['lon'];
			        $gpsData['alt'] = $array['alt'];
			        if($array['ts'] == 0)
			        {
			            $array['ts'] = time();
			        }
			        else
			        {
			            $array['ts'] = $array['ts'] + 946684800; // To convert time from UTC since 2000 to UTC since 1970   
			        }
			        $gpsData['time_s']  = ($array['ts'] * 1000);
			        $gpsData['ad_dt']   = date('Y-m-d H:i:s',substr($gpsData['time_s'],0,10));
			        $gpsData['event']   = $array['ev'];
			        $gpsData['nodrive'] = $array['dn'];
			        $gpsData['seqno']   = $array['s'];
					//update the location in event table
			        $saveData = Fetch::insertDbSingle_event($gpsData);
			        if($saveData)
			        {
			            // Data Uploaded successfully
			            // Also Update "device" table to get the last know location of the device.
			            $deviceid   = $gpsData['device_id'];
					    $lat        = $gpsData['lat'];
					    $long       = $gpsData['lon'];
					    $alt        = $gpsData['alt'];
					    $time_stamp = $gpsData['time_s'];
					    Fetch::setDeviceLastLocation($lat,$long,$alt,$time_stamp,$deviceid);
					    // $sql        = "update device set latitude='$lat',longitude='$long',alt='$alt',time_stamp='$time_stamp' where device_id='$deviceid'";
					    // $device     = $conn->query($sql);
					    
			      //       setDeviceLastLocation($gpsData,$conn);
			            $arrayData['success'] = TRUE;
			            $arrayData['message'] = "Event updated successfully";
			        }
			        else
			        {
			            //Data failed to upload
			            $arrayData['success']      = FALSE;
			            $arrayData['errorCode']    = "1004";
			            $arrayData['errorMessage'] = "Data cannot be updated kindly try again later";
			        }
				}
				else
				{
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1001";
					$arrayData['errorMessage'] = "Invalid Device ID!";
				}
			}
			else
			{
				// TODO: To be chened later after fixing device id from device 
				$arrayData['success'] = TRUE;
			    $arrayData['message'] = "Event updated successfully";
				// $arrayData['success']      = TRUE;
				// $arrayData['errorCode']    = "1002";
				// $arrayData['errorMessage'] = "Invalid Device ID! so ingoring data";
			}
		}
		else
		{
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1002";
			$arrayData['errorMessage'] = "Invalid Input!";
		}

		$resultVal = json_encode($arrayData);
		echo $resultVal;
	}
	//===================================== Update Device loaction in event table ends ==================================//

	//===================================== Update DTC into device table starts ==================================//
	public function dtcdataupload(Request $request)
  	{
  		$datMe = file_get_contents("php://input");
  		if(!empty($datMe))
        {
        	$obj      = json_decode($datMe, TRUE);
        	// print_r($obj);die;
			$deviceID = $obj['devID'];
			$dtc      = $obj['dtcData'];
			// print_r($deviceID);die;
			// Get Device Data
			$devInfo = Fetch::devicestatus($deviceID);
			// print_r($devInfo);die;
			if(!empty($devInfo))
			{
				$update = Fetch::updatedeviceDtcv($deviceID,$dtc,1);
				if($update)
				{
					$result['success']    = TRUE;
        			$result['successmsg'] = "Data Upload Successful!";
				}
				else
				{
					$result['success']      = FALSE;
					$result['errorCode']    = "1003";
					$result['errorMessage'] = "Update Failed! Please try again later.";
				}
			}
			else
			{
				$result['success']      = FALSE;
				$result['errorCode']    = "1002";
				$result['errorMessage'] = "Invalid Device ID!";
			}
        }
        else
		{
			$result['success']      = FALSE;
			$result['errorCode']    = "1001";
			$result['errorMessage'] = "Invalid Input!";
		}
		$resultVal = json_encode($result);
		echo $resultVal;
  	}
  	//===================================== Update DTC into device table ENDS ==================================//
	
	public function updatelog($api,$datMe)
	{
		// Logging request to file
		$logFilePath = getcwd();
		$req_dump = print_r($datMe, TRUE);
		$filePath = $logFilePath. "/"."request_sigfox.txt";
		// print_r($filePath);
		// print_r($req_dump);die;
		$start = "\n----------------$api - Request Begin --------------------\n";
		$end = "\n----------------$api - Request END --------------------\n";
		$fp = fopen($filePath, 'a');
		fwrite($fp, $start);
		$currTimeStamp = date('Y-m-d H:i:s',time());
		fwrite($fp, $currTimeStamp);
		fwrite($fp, "\n");
		fwrite($fp, $req_dump);
		fwrite($fp, $end);
		fclose($fp);
	}

}
?>