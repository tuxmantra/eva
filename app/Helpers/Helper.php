<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use Illuminate\Support\Facades\DB;

class Helper
{
	// used in user listing page start
    public static function shout(string $string)
    {
        return strtoupper($string);
    }

    public static function getUserType($type){
		    $type = DB::table('user_type')->where('u_type', $type)->value('u_name');
		    return !empty($type)? $type:"-";
    }

    public static function getDeviceId($did){
		    $device = DB::table('device')->where('id', $did)->value('device_id');
		    return !empty($device)? $device:"-";
    }

    public static function getUserName(){
            return DB::table('users')->where('uid', session('userdata')['uid'])->value('uname');
    }

    public static function getManufacturerById($mid){
            $manu = DB::table('vehicle_setting')->where('id', '1')->value('manufacturer');
            $manu = explode(";",$manu);
            return $manu[$mid];
    }

    public static function getStatus($status){
        if($status == 1){
            echo "Active";
        }elseif ($status == 2) {
            echo "In Use";
        }else{
             echo "InActive";
        }
		// echo ($status == 1) ? "Active":"InActive";
    }

    // used in user listing page end
    
    // used in device list start
    public static function getDeviceType($status)
    {
        if($status == 1){
            echo "Bluetooth";
        }elseif ($status == 2) {
            echo "GSM";
        }else{
             echo "LINUX";
        }
    }

    //used in value part view
      public static function getType($type){
            echo ($type == 1)? "Map":"Graph";
    }
    // used in device list end

    public static function getPartnerName($uid){
		    $name = DB::table('users')->where('uid', $uid)->value('uname');
		    return !empty($name)? $name:"-";
    }

    public static function crypt(string $action,string $string){
            $output = false;
		    $encrypt_method = "AES-256-CBC";
		    $secret_key = 'QFDyZCvQaMW0RdM29gP8hL0o5oTMe2coJ2DBLvLkrMblmm';
		    $secret_iv = 'p8GQeU1kNwaogUvGFp3DwCgKcRKQ0ycijv';
		    // hash
		    $key = hash('sha256', $secret_key);
		    
		    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		    $iv = substr(hash('sha256', $secret_iv), 0, 16);
		    if ( $action == '1' ) {
		        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
		        $output = base64_encode($output);
		    } else if( $action == '2' ) {
		        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		    }
		    return $output;
    }

    public static function token($length_of_string) 
	{ 
	  
	    // String of all alphanumeric character 
	    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
	    
	    // Shufle the $str_result and returns substring 
	    // of specified length 
	    $token = substr(str_shuffle($str_result),  
	                       0, $length_of_string); 

	    session(['form_token'=>$token]);
	    return $token;
	} 


    public static function token1($length_of_string) 
    { 
      
        // String of all alphanumeric character 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
        
        // Shufle the $str_result and returns substring 
        // of specified length 
        $token = substr(str_shuffle($str_result),  
                           0, $length_of_string); 
        return $token;
    } 

	public static function cryptoJsAesDecrypt($passphrase, $jsonString){
	    $jsondata = json_decode($jsonString, true);
	    $salt = hex2bin($jsondata["s"]);
	    $ct = base64_decode($jsondata["ct"]);
	    $iv  = hex2bin($jsondata["iv"]);
	    $concatedPassphrase = $passphrase.$salt;
	    $md5 = array();
	    $md5[0] = md5($concatedPassphrase, true);
	    $result = $md5[0];
	    for ($i = 1; $i < 3; $i++) {
	        $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
	        $result .= $md5[$i];
	    }
	    $key = substr($result, 0, 32);
	    $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
	    return json_decode($data, true);
    }

    public static function v3generate($uid = null) {

    $uid = sprintf('%04x%04x%02x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xff));
    return $uid;
    }

    public static function createThumb($filepath='',$thumbPath='',$maxwidth='',$maxheight='',$quality=75)
    {   
                $created=false;
                $file_name  = pathinfo($filepath);  
                $format = $file_name['extension'];
                // Get new dimensions
                $newW   = $maxwidth;
                $newH   = $maxheight;
                // Resample
                $thumb = imagecreatetruecolor($newW, $newH);
                $image = imagecreatefromstring(file_get_contents($filepath));
                list($width_orig, $height_orig) = getimagesize($filepath);
                imagecopyresampled($thumb, $image, 0, 0, 0, 0, $newW, $newH, $width_orig, $height_orig);
                // Output
                switch (strtolower($format)) {
                    case 'png':
                    imagepng($thumb, $thumbPath, 9);
                    $created=true;
                    break;
                    case 'gif':
                    imagegif($thumb, $thumbPath);
                    $created=true;
                    break;
                    default:
                    imagejpeg($thumb, $thumbPath, $quality);
                    $created=true;
                    break;
                }
                imagedestroy($image);
                imagedestroy($thumb);
                return $created;    
    }

    // get Alert Category Name
    public static function getAlertCategoryName($id){
       return DB::table('alert_category')->where('alert_category_id', $id)->value('name');
    }

    // get Alert Sub Category Name
    public static function getAlertSubCategoryName($id){
       return DB::table('alert_sub_category')->where('alert_sub_category_id', $id)->value('name');
    }
    
    // get Alert Type status
    public static function getAlertTypeName($id){
       return DB::table('alert_type')->where('alert_type_id', $id)->value('alert_type_name');
    }

    public static function getAlertFrom($alert_frm_id){
       return DB::table('alert_from')->where('alert_from_id', $alert_frm_id)->value('name');
    }

    public static function getAlertColor($alert_clr_id){
      return DB::table('alert_color')->where('alert_color_id', $alert_clr_id)->value('name');
    }
    
    // Service Type
    public static function getServiceType($id){
       return DB::table('service_type_sett')->where('id', $id)->where('status', '1')->value('title');
    }

    // Service Description
    public static function getServiceDescription($desc,$details){
            $descr   = '';
            $descprn = explode(',',$desc);
            for($i=0;$i<count($descprn);$i++)
            {

                $description_type = DB::select("select * from service_desc_sett where status = ? and id = ?",['1',$descprn[$i]]);
                if(!empty($description_type)){
                   $descr = ($descr!='')?$descr.', '.$description_type[0]->title:$description_type[0]->title; 
                }
    
            }
            return $descr." - Details ($details)";
    }

    // get last service for device
    public static function getLastServiceForDeviceById($devid,$serId){
        
        return DB::select("select * from service where status = ? and devid = ? and find_in_set('$serId',descrp) <> 0 ORDER BY sdate desc limit 0,1",['1',$devid]);
       
    }

    // get last service for device
    public static function getLastServiceForDeviceById2($devid,$serId){
        $sql   = "select * from service where status = ? and devid = ?";
        $sql  .= ($serId >= 1) ? " and find_in_set('$serId',descrp) <> 0" : "";
        $sql  .= " and descrp_sub != ''" ;
        $sql  .= " ORDER BY `sdate` desc";
        return DB::select($sql,['1',$devid]);
       
    }


    public static function getVehicleDataActions($did){
        return DB::select("SELECT a.uname AS name, b.id AS did, b.device_id AS devid, c . * 
        FROM device AS b, users AS a, vehicles AS c
        WHERE b.id =  ?
        AND b.id = a.device
        AND b.status =  ?
        AND b.id = c.device",[$did,'1']);
    }

    // user name by device ID 

    public static function getUserByDeviceId($device){
        $id = DB::select("select * from users where device = ?",[$device]);
        if(!empty($id[0]->uname)){
           return $id[0]->uname;
           exit;
        }
        return "-";
    }
}