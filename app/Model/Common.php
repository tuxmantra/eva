<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Common extends Model
{
	//adding a new row
    public static function insert($table,$data){
       DB::table($table)->insert($data);
    }

    public static function insert_vehicle($table,$data){
       DB::table($table)->insert($data);
    }
	
	//insert a new row and get its id
    public static function insertGetId($table,$data){ 
       return DB::table($table)->insertGetId($data);
   }

	//update (replace) the existing table with new data
   public static function updateTable($table,$where,$data){
   	   DB::table($table)->where($where)->update($data);
       
   }
  
   public static function updateTable_vehicle($table,$where,$data,$did){
      $id=$where['id'];
       DB::table($table)->where($where)->update($data);
        date_default_timezone_get("Asia/Calcutta");
        $dtime = date("Y-m-d h:i:s");
        $user  = session('userdata');
        $uid   = $user['uid'];
        
        DB::INSERT("insert into `device_log` (`device_id`, `ad_time`, `log_type`,`log_desc`,`act_by`,`IP`) VALUES ('$did','$dtime','Update into DB $table','Update ID $id','$uid','')");
   }
  
   public static function updateTable_one($table,$where,$data){
       DB::table($table)->where($where)->update($data);
   }
	
	//create a news column in the existing table
   public static function createColumn($table,$column){
      DB::select("ALTER TABLE $table ADD COLUMN $column VARCHAR(255)");
   }
}
?>