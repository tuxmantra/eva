<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

// include 'templates.php';

class Fetch extends Model
{
    // User Level start
    public static function save_user($postVal)
    {
        $columns = array_keys($postVal);
        $values  = array_values($postVal);
        return DB::insert("INSERT INTO users (" . implode(", ", $columns) . ") VALUES ('" . implode("', '", $values) . "')");
    }

    public static function getactivationcode($acode){
        return DB::select("select * from activation_code where activation_code = ?",[$acode]);
    }

    public static function updateactivationcode($mac,$used,$acode){
        return DB::update("update activation_code set mac_id = ?,used_by= ?,status= ? where activation_code = ?",[$mac,$used,'2',$acode]);
    }

    // Activate user
    public static function actUserProfile($devid){
        return DB::update("update users set ustatus = ? where device = ?",['1',$devid]);
    }

    // get user type list to load in dropdown leaving admin
    public static function getUserTypeList(){
        return DB::select("select u_type,u_name from user_type where u_type != ? order by u_name",['1']);
    }

    // google api key
    public static function getgapikey(){
        $date=date('Y-m-d');
        $checkdate=DB::select("select * from gmap_count where date='$date'",['1']);
        
        if(empty($checkdate)){           
         DB::update("update gmap_count set date='$date', count=0 ",['1']);
       }
        return DB::select("select * from gmap_count where count < 90 and date='$date'",['1']);
    }

    public static function getgapikeyall(){
        return DB::select("select * from gmap_count",['1']);
    }

    // delet google api key
    public static function delete_gkey($id){
        return DB::delete("DELETE FROM gmap_count WHERE id = '$id'",['1']); 
    }

    // update google key count
    public static function updategapicount($key,$count){
        return DB::update("update gmap_count set count='$count' where api_key='$key'",['1']);
    }

    // update google key 
    public static function update_gkey($id,$key){
        return DB::update("update gmap_count set api_key='$key' where id='$id'",['1']);
    }

    // Create New google key 
    public static function create_gkey($key){
        $date=date('Y-m-d');
        return DB::insert("insert into gmap_count (`api_key`,`date`,`count`) VALUES ('$key','$date','0')",['1']);
    }
    
    // get user type list to load menu
    public static function getUserTypeByIdMinu($utype){
        return DB::table('user_type')->where('u_type', $utype)->first();
    }
    
    public static function getdevice_moniter($did){
        return DB::select("select * from device_monitor  where device_id=? order by id DESC limit  1",[$did]);
    }
    
    public static function getredvehicle(){
        return DB::select("select distinct devid,name,rating,msg,satype from alert a,alert_sub_category b,alert_type c where a.atype = ? and b.alert_sub_category_id=a.satype and c.alert_type_id=a.ftype",[3]);
    }

    // public static function getactivedtc(){
    //     return DB::select("select * from device where dtc = ? ",[1]);
    // }

    // decative_app
    public static function deactive_app($id){
        return DB::update("update app set status='0' where id= ?  ",[$id]);
    }

    // decative_app
    public static function delete_app($id){
         DB::delete("delete from app_devices  where app= ?  ",[$id]);
        return DB::delete("delete from app where id= ?  ",[$id]);
    }

    // decative_app
    public static function delete_app_device($id){
         return DB::delete("delete from app_devices  where id= ?  ",[$id]);
    }

     public static function delete_firm($id){
         DB::delete("delete from firmware  where firm_id= ?  ",[$id]);
        return DB::delete("delete from firmware_master where id= ?  ",[$id]);
    }

    // decative_app
    public static function delete_firm_device($id){
         return DB::delete("delete from firmware  where id= ?  ",[$id]);
    }

    // decative_app
    public static function deactive_app1($id){
        return DB::update("update app set status='1' where id= ?  ",[$id]);
    }

    // decative_app
    public static function deactive_firm($id){
        return DB::update("update firmware_master set status='0' where id= ?  ",[$id]);
    }

    // decative_app
    public static function deactive_firm1($id){
        return DB::update("update firmware_master set status='1' where id= ?  ",[$id]);
    }

     public static function uninstall_app($id){
        return DB::update("update app_devices set ins_status='0' where id= ?  ",[$id]);
    }

    public static function uninstall_firm($id){
        return DB::update("update firmware set ins_status='0' where id= ?  ",[$id]);
    }

    // decative_app
    public static function uninstall_app1($id){
        return DB::update("update app set status='1' where id= ?  ",[$id]);
    }

    public static function getactivedtc(){
        return DB::select("select * from device where dtc = ? ",[1]);
    }

    public static function getactivedtc1($dtcv,$did){
        return DB::select("select * from dtc_list a,device b  where a.dtc_num= ? and b.device_id=?",[$dtcv,$did]);
    }

    public static function randKeyVerify($key){
        return DB::select('select count(*) as count from api_key where api_key = ?',[$key]);
    }

    public static function getapikey(){
        return DB::select('select * from api_key');
    }

    public static function updateapikey($id,$key,$name,$exdat,$status,$ad_by,$ad_date){
        return DB::UPDATE("update api_key set `api_key`='$key',`used_by`='$name',`status`='$status',`expdt`='$exdat',`ad_by`='$ad_by',`ad_dt`='$ad_date' where id=? ",[$id]);
    }

    public static function saveapikey($key,$name,$exdat,$status,$ad_by,$ad_date){
        return DB::INSERT("insert into api_key (`api_key`,`used_by`,`status`,`expdt`,`ad_by`,`ad_dt`) VALUES ('$key','$name','$status','$exdat','$ad_by','$ad_date')");
    }

    public static function getapikeyid($id){
        return DB::select('select * from api_key where id=?',[$id]);
    }

    public static function getUserById($uid){
        return DB::select("select uid,utype,ucategory,uname,umob,uemail,upwd,ulocation,ustatus,partner,device from users where uid = ?",[$uid]);
    }

    public static function getUserByEmailPass($email,$pass){ 
        // DB::enableQueryLog();
       return DB::select("select * from users where uemail ='$email' and upwd ='$pass'",[$email, $pass]);
        // $query = DB::getQueryLog();
        // print_r($query);
    }

    public static function getUserByEmail($email){
        return DB::select("select * from users where uemail = ?",[$email]);
    }

    public static function getEdasScoreLatest($did)
    {
        return DB::select("SELECT * FROM `driver` where device_id = ? and drv_score > 0 order by id DESC LIMIT 1",[$did]);
    }
    public static function getEDASScorebyDriveid($deviceID,$drive)
    {
        return DB::select("SELECT * FROM `driver` where device_id = ? and nodrive = ? order by id DESC LIMIT 1",[$deviceID,$drive]);   
    }
    public static function getUserAll(){
        return DB::select("select uid,uname,utype,uemail,umob,ustatus,partner,device from users where utype != ? ORDER by uid DESC",['1']);
    }

    public static function getEdasScoreGroupbyMonth($did,$month,$year) 
    {

        return DB::select("SELECT * FROM `driver` where device_id = ? and MONTH(ad_dt)= ? and YEAR(ad_dt)= ? order by id ASC",[$did,$month,$year]);
    }
    
    public static function getEdasComments($value)
    {
        return DB::select("select * from edas_comments where rating = ? ",[$value]);
    }
    public static function getEdasScore($did)
    {
        return DB::select("select * from driver where device_id = ? and drv_score > 0 order by id desc limit 1",[$did]);
    }
    /*core part Listing*/
   
    // get user category by user type
    public static function getUserCategoryByUserType($user_type){
        return DB::select("select u_category from user_type where u_type = ?",[$user_type]);
    }

    // get partner list by category All
    public static function getPartnerListById($cid){
        return DB::select("select uid,uname from users where ucategory = ? order by uname",[$cid]);
    }
    // User level end
    
    // device start

    //Add device in DB
    
    public static function delete_bt_id($btid){
        return DB::delete("DELETE FROM bt_dev WHERE id = ?;",[$btid]);
    }

    public static function insert_device($postVal)
    {
        $columns = array_keys($postVal);
        $values  = array_values($postVal);
        $dd      = $postVal['device_id'];
        // echo "$dd";die;
        //echo "INSERT INTO device (" . implode(", ", $columns) . ") VALUES ('" . implode("', '", $values) . "')";die;
       DB::insert("INSERT INTO device (" . implode(", ", $columns) . ") VALUES ('" . implode("', '", $values) . "')");
       return DB::select("select * from device where device_id = ? ",[$dd]);
    } 
     // get vehicle list by drive in EVA graph
    public static function getvehiclelistbydevid($date,$did)
    {
       // DB::enableQueryLog();
       return DB::select("SELECT DISTINCT vehicle FROM `device_drive` where dt=? and device_id=? and vehicle !=''",[$date,$did]); 
       // dd(DB::getQueryLog());die;
    }
    public static function getvehicletype($uid,$utype)
    {
        if($utype == 1 || $utype == 2)
        {
            return DB::select("SELECT DISTINCT vehicle FROM `device_drive` where vehicle !='' and vin_no !=''",[0]);
        }
        else if($utype == 3)
        {
            return DB::select("SELECT DISTINCT vehicle FROM `device_drive` a,device d where a.vehicle !='' and a.vin_no !='' and a.device_id = d.device_id and d.partner = '$uid'",[0]);
        }   
        else
        {
            return DB::select("SELECT DISTINCT vehicle FROM `device_drive` a,device d,users u where a.vehicle !='' and a.vin_no !='' and  a.device_id = d.device_id and d.id=u.device and u.uid = '$uid'",[0]);
        }
              
    }
    public static function getusername($uid,$utype)
    {
        if($utype == 1 || $utype == 2)
        {
            return DB::select("SELECT DISTINCT uname,uid FROM `summary` s,users u,device d where d.device_id = s.device_id and u.device = d.id ",[0]);  
        }
        else if($utype == 3)
        {
            return DB::select("SELECT DISTINCT uname,uid FROM `summary` s,users u,device d where d.device_id = s.device_id and u.device = d.id and d.partner = '$uid' ",[0]);  
        }
        else
        {
            return DB::select("SELECT DISTINCT uname,uid FROM `summary` s,users u,device d where d.device_id = s.device_id and u.device = d.id and u.uid = '$uid' ",[0]);
        }
            
    }
    public static function getvinlistbydevid($date,$did,$vehicle)
    {
       // DB::enableQueryLog();
        if(!empty($vehicle))
        {
           return DB::select("SELECT DISTINCT vin_no FROM `device_drive` where dt=? and device_id=? and vehicle = ? order by id DESC",[$date,$did,$vehicle]);
        } 
        else
        {
            return DB::select("SELECT DISTINCT vin_no FROM `device_drive` where dt=? and device_id=? ",[$date,$did]);
        }
       // dd(DB::getQueryLog());die;
    }
    public static function getTestlistbydevid($date,$did,$vehicle,$vin_no)
    {
       // DB::enableQueryLog();
        if(date('m', strtotime($date))=='03'){
            $table = 'dd_mar';
        }
        elseif(date('m', strtotime($date))=='04'){
            $table = 'dd_apr';
        }
        elseif(date('m', strtotime($date))=='05'){
            $table = 'dd_may';
        }
        elseif(date('m', strtotime($date))=='06'){
            $table = 'dd_june';
        }
        elseif(date('m', strtotime($date))=='07'){
            $table = 'dd_july';
        }
        elseif(date('m', strtotime($date))=='08'){
            $table = 'dd_aug';
        }
        elseif(date('m', strtotime($date))=='09'){
            $table = 'dd_sep';
        }
        elseif(date('m', strtotime($date))=='10'){
            $table = 'dd_oct';
        }
        elseif(date('m', strtotime($date))=='11'){
            $table = 'dd_nov';
        }
        elseif(date('m', strtotime($date))=='12'){
            $table = 'dd_dec';
        }
        elseif(date('m', strtotime($date))=='01'){
            $table = 'dd_jan';
        }
        elseif(date('m', strtotime($date))=='02'){
            $table = 'dd_feb';
        }
        if(!empty($vin_no))
        {
            return DB::select("SELECT DISTINCT F43 FROM $table a,device_drive b where b.dt = ? and b.device_id=? AND b.vehicle = ? and b.vin_no = ? and a.Fsprint = b.drive  and a.ad_dtin = b.dt",[$date,$did,$vehicle,$vin_no]); 
        }
        elseif(empty($vehicle))
        {
             return DB::select("SELECT DISTINCT F43 FROM $table a,device_drive b where b.dt = ? and b.device_id=? AND a.Fsprint = b.drive  and a.ad_dtin = b.dt",[$date,$did]); 
        }
        else
        {
            return DB::select("SELECT DISTINCT F43 FROM $table a,device_drive b where b.dt = ? and b.device_id=? AND b.vehicle = ? and  a.Fsprint = b.drive  and a.ad_dtin = b.dt",[$date,$did,$vehicle]);
        }
       // dd(DB::getQueryLog());die;
    }

    public static function getbtdetailsbyid($id){
        return DB::select("select * from bt_dev where id = ?",[$id]);
    } 
 
    public static function getbtdetailsbyid1($id){
        return DB::select("select * from bt_dev where device_id = ?",[$id]);
    }

    public static function getbtdevicedetails($did){
        return DB::select("select * from bt_dev where device_id = ?",[$did]);
    }

    public static function update_bt_device($postVal,$btid)
    {
        // print_r($postVal); die;
        $columns = array_keys($postVal);
        $values  = array_values($postVal);
        $string  = '';
        for($i = 0;$i < count($columns);$i++)
        {
            if($i == 0)
            {
                $string.=$columns[$i]."= '".$values[$i]."'";
            }
            else
            {
                $string.=','.$columns[$i]."= '".$values[$i]."'";
            }
        }
        
        $update = DB::UPDATE("UPDATE bt_dev SET $string WHERE id=? ",[$btid]);

        date_default_timezone_get("Asia/Calcutta");
        $dtime = date("Y-m-d h:i:s");
        $user  = session('userdata');
        $uid   = $user['uid'];
        $did   = $postVal['device_id'];
        
        DB::INSERT("insert into `device_log` (`device_id`, `ad_time`, `log_type`,`log_desc`,`act_by`,`IP`) VALUES ('$did','$dtime','Update into DB bt_dev','Update ID $btid','$uid','')");
        return $update;
    }

    public static function check_bt_device($mac_id,$did){
        return DB::select("select count(*) as count from bt_dev where device_id = ? and mac_id= ?",[$did,$mac_id]);
    }

    public static function save_bt_device($postVal)
    {
        $columns = array_keys($postVal);
        $values  = array_values($postVal);
        $insert  = DB::insert("INSERT INTO bt_dev (" . implode(", ", $columns) . ") VALUES ('" . implode("', '", $values) . "')");
        $id      = DB::getPdo()->lastInsertId();
        // $query = DB::getQueryLog();
        date_default_timezone_get("Asia/Calcutta");
        $dtime = date("Y-m-d h:i:s");
        $user  = session('userdata');
        $uid   = $user['uid'];
        $did   = $postVal['device_id'];
        DB::INSERT("insert into `device_log` (`device_id`, `ad_time`, `log_type`,`log_desc`,`act_by`,`IP`) VALUES ('$did','$dtime','Insert into DB bt_dev','Insert ID $id','$uid','')");
        return $insert;
    }
	public static function getDriveMoniterCailite($did)
    {
        return DB::select("SELECT * FROM `monitor_ratings` where device_id=? ORDER BY `id` DESC LIMIT 1",[$did]);
    }
    public static function getDriveMoniter1($did,$year)
    {
        return DB::select("SELECT MONTH(dt) as date,
			IFNULL(MIN(NULLIF(drive, 0)), 0) as drive ,
			IFNULL(MIN(NULLIF(dist, 0)), 0) as dist,
			IFNULL(MIN(NULLIF(press, 0)), 0)  as press,
			IFNULL(MIN(NULLIF(hmdty, 0)), 0)   as hmdty,
			IFNULL(MIN(NULLIF(drvblty, 0)), 0)  as drvblty,
			IFNULL(MIN(NULLIF(bat_st, 0)), 0)  as bat_st,
			IFNULL(MIN(NULLIF(bat_c, 0)), 0)  as bat_c,
			IFNULL(MIN(NULLIF(idlbc_v, 0)), 0)  as idlbc_v,
			IFNULL(MIN(NULLIF(idlbc_m, 0)), 0)  as idlbc_m,
			IFNULL(MIN(NULLIF(peakb_v, 0)), 0)  as peakb_v,
			IFNULL(MIN(NULLIF(peakb_m, 0)), 0)  as peakb_m,
			IFNULL(MIN(NULLIF(comp_pres, 0)), 0)  as comp_pres,
			IFNULL(MIN(NULLIF(tune_msg, 0)), 0)  as tune_msg,  
			IFNULL(MIN(NULLIF(eng_block_ej, 0)), 0)  as eng_block_ej,
			IFNULL(MIN(NULLIF(falsemeter, 0)), 0)  as falsemeter,
			IFNULL(MIN(NULLIF(vehspeed, 0)), 0)  as vehspeed,
			IFNULL(MIN(NULLIF(coolp, 0)), 0)  as coolp,
			IFNULL(MIN(NULLIF(coolm, 0)), 0)  as coolm ,
			IFNULL(MIN(NULLIF(ocilv, 0)), 0)  as ocilv ,
			IFNULL(MIN(NULLIF(bat_curr, 0)), 0)  as bat_curr,
			IFNULL(MIN(NULLIF(air_idle_curr, 0)), 0)  as air_idle_curr,
			IFNULL(MIN(NULLIF(air_peak_curr, 0)), 0)  as air_peak_curr,
			IFNULL(MIN(NULLIF(comp_curr, 0)), 0)  as comp_curr,
			IFNULL(MIN(NULLIF(rpm_curr, 0)), 0)  as rpm_curr,
			IFNULL(MIN(NULLIF(cool_curr, 0)), 0)  as cool_curr,
			IFNULL(MIN(NULLIF(repal, 0)), 0)  as repal,
			IFNULL(MIN(NULLIF(max_drive_torque, 0)), 0)  as max_drive_torque,
			IFNULL(MIN(NULLIF(reference_torque, 0)), 0)  as reference_torque 
			FROM `device_monitor` where device_id= ? and YEAR(dt)= ? 

         GROUP by date ",[$did,$year]);    
    }

    public static function getDriveMoniter2($did,$year,$month)
    {
        return DB::select("SELECT DAY(dt) as date,
            IFNULL(MIN(NULLIF(drive, 0)), 0) as drive ,
            IFNULL(MIN(NULLIF(dist, 0)), 0) as dist,
            IFNULL(MIN(NULLIF(press, 0)), 0)  as press,
            IFNULL(MIN(NULLIF(hmdty, 0)), 0)   as hmdty,
            IFNULL(MIN(NULLIF(drvblty, 0)), 0)  as drvblty,
            IFNULL(MIN(NULLIF(bat_st, 0)), 0)  as bat_st,
            IFNULL(MIN(NULLIF(bat_c, 0)), 0)  as bat_c,
            IFNULL(MIN(NULLIF(idlbc_v, 0)), 0)  as idlbc_v,
            IFNULL(MIN(NULLIF(idlbc_m, 0)), 0)  as idlbc_m,
            IFNULL(MIN(NULLIF(peakb_v, 0)), 0)  as peakb_v,
            IFNULL(MIN(NULLIF(peakb_m, 0)), 0)  as peakb_m,
            IFNULL(MIN(NULLIF(comp_pres, 0)), 0)  as comp_pres,
            IFNULL(MIN(NULLIF(tune_msg, 0)), 0)  as tune_msg,  
            IFNULL(MIN(NULLIF(eng_block_ej, 0)), 0)  as eng_block_ej,
            IFNULL(MIN(NULLIF(falsemeter, 0)), 0)  as falsemeter,
            IFNULL(MIN(NULLIF(vehspeed, 0)), 0)  as vehspeed,
            IFNULL(MIN(NULLIF(coolp, 0)), 0)  as coolp,
            IFNULL(MIN(NULLIF(coolm, 0)), 0)  as coolm ,
            IFNULL(MIN(NULLIF(ocilv, 0)), 0)  as ocilv ,
            IFNULL(MIN(NULLIF(bat_curr, 0)), 0)  as bat_curr,
            IFNULL(MIN(NULLIF(air_idle_curr, 0)), 0)  as air_idle_curr,
            IFNULL(MIN(NULLIF(air_peak_curr, 0)), 0)  as air_peak_curr,
            IFNULL(MIN(NULLIF(comp_curr, 0)), 0)  as comp_curr,
            IFNULL(MIN(NULLIF(rpm_curr, 0)), 0)  as rpm_curr,
            IFNULL(MIN(NULLIF(cool_curr, 0)), 0)  as cool_curr,
            IFNULL(MIN(NULLIF(repal, 0)), 0)  as repal,
            IFNULL(MIN(NULLIF(max_drive_torque, 0)), 0)  as max_drive_torque,
            IFNULL(MIN(NULLIF(reference_torque, 0)), 0)  as reference_torque 
            FROM `device_monitor` where device_id= ? and YEAR(dt)= ? and MONTH(dt)=? 

            GROUP by date ",[$did,$year,$month]);
    }
    
    /* Device info*/
    public static function getdevice($deviceID){
        return DB::select("select * from device where device_id = ?",[$deviceID]);
    }

    public static function getdevice1($deviceID){
        return DB::select("select * from device where id = ?",[$deviceID]);
    }

    // get device list by partner
    public static function getDevicesAll(){
        return DB::select("select * from device ORDER by id DESC");
    }

    public static function getDevicesAll1(){
        return DB::select("select * from device where devtype ='3' ORDER by id DESC");
    }

    public static function getDeviceListByPartnerId($pid){
        return DB::select("select id,device_id from device where partner = ?",[$pid]);
    }

    public static function getDeviceById($id){
        return DB::select("select * from device where id = ?",[$id]);
    }
    public static function getdevicebydid($id){ 
        return DB::select("select * from device where device_id = ?",[$id]);
    }

    public static function getIdByDeviceId($id){
        return DB::select("select id,device_id,name,descrp,partner,status,devtype,total_dist from device where device_id = ?",[$id]);
    }

    public static function devicestatus($id){
        return DB::select("select * from device where device_id = ?",[$id]);
    }

    public static function getDeviceStatus($id){
        return DB::select("select device_id from device where device_id = ?",[$id]);
    }

    public static function getDevicesPending(){
        return DB::select("select * from device where file_list <> ''");
    }

    public static function getFileListForDevice($divid){
        return DB::select("select file_list from device where device_id = ?",[$divid]);
    }

    public static function setdeviceFile($deviceid,$fname)
    {
        $result  = Fetch::getFileListForDevice($deviceid);
        $devFile = get_object_vars($result[0]);
        // print_r($devFile['file_list']);die;
        $find    = explode(";",$devFile['file_list']);
        $exist   = 0;
        if(in_array($fname, $find))
        {
           $exist = 1;
        }
        if($exist == 0)
        {
            $cnt = count($find);
            $cnt = ($cnt >= 1 && $devFile != "") ? $cnt : 0;
            $find[$cnt] = $fname;
        }
        $arr   = array_filter($find);
        $flist = implode(';',$arr);
        // $sql   = "update device set file_list = '$flist' where device_id = '$deviceid'";
        // print_r($sql);die;
        return DB::update("update device set file_list = '$flist' where device_id = '$deviceid'");
    }

    public static function updatecmdapi($devID,$msg){
        // echo"update device set dev_cmd_status = '0', dev_res = '$msg' where device_id = '$devID'";die;
        return DB::update("update device set dev_cmd_status = '0', dev_res = ? where device_id = ?",[$msg,$devID]);
    }

    public static function updatecmdfail($devID,$msg){
        return DB::update("update device set dev_res = ? where device_id = ?",[$msg,$devID]);
    }

    public static function updatebtapi($devID,$msg){
        return DB::update("update bt_dev set update_status = '0', status_msg = ? where device_id = ?",[$msg,$devID]);
    }

    public static function updatebtfail($devID,$msg){
        return DB::update("update bt_dev set status_msg = ? where device_id = ?",[$msg,$devID]);
    }

    public static function updatePhnNameBtMacId($devID,$phnName,$btMacId){
        return DB::update("update device set phn_model = ?, bt_mac_id = ? where id = ?",[$phnName,$btMacId,$devID]);
    }

    public static function getDeviceActive($uid,$utype)
    {
        if($utype == 1 || $utype == 2 ){
            return DB::select("select count(*) as count from device where status = ?",['1']);
        }elseif($utype == 3){
            return DB::select("select count(*) as count from device where status = ? and partner='$uid'",['1']);
        }else{
            return DB::select("select count(*) as count from device a,users b where a.status = ? and a.id = b.device and b.uid = '$uid'",['1']);
        }
    }
    // device end

    // get data from setting table
    // settings & vehicle data start
    public static function getSettingData(){
       return DB::table('settings')->select('*')->where('sett_id','1')->first();
    }

    //Add vehicle in DB
    public static function insert_vehicle($postVal){
        $columns = array_keys($postVal);
        $values  = array_values($postVal);
        // echo "INSERT INTO vehicles (" . implode(", ", $columns) . ") VALUES ('" . implode("', '", $values) . "')";die;
        return DB::insert("INSERT INTO vehicles (" . implode(", ", $columns) . ") VALUES ('" . implode("', '", $values) . "')");
    }

    public static function getVehicleDataById($did){
       return DB::table('vehicles')->select('*')->where('device',$did)->first();
    }

    public static function getVehicleById($did){
       return DB::table('vehicles')->select('*')->where('device',$did)->first();
    }

    public static function getVehicleSettingData(){
       return DB::table('vehicle_setting')->select('*')->where('id','1')->first();
    }

    // get service due days and KM for API 
    public static function getservicedue($deviceID)
    {
        return DB::select("select * from device_drive where device_id = ? order by id DESC limit 1",[$deviceID]);
    }

    public static function getservicedetailsapi($deviceID)
    {
        return DB::select("SELECT kms_read,sdate FROM `service` where devid = ? ORDER BY `id`  DESC LIMIT 1",[$deviceID]);
    }

    public static function getodoreadingapi($deviceID)
    {
        return DB::select("SELECT travel_purchase FROM vehicles a,device b where a.device=b.id and b.device_id = ? ",[$deviceID]);
    } 

    public static function getdistanceapi($deviceID)
    {
        return DB::select("SELECT sum(dist) as dist FROM `device_drive` where device_id= ? ",[$deviceID]);
    } 

    public static function getCaliPramVal($deviceID)
    {
		$vehSett = DB::select("select * from vehicle_setting");
        $vehicle = DB::select("select v.* from vehicles as v, device as d where d.id = v.device and d.device_id = ?",[$deviceID]);
        // print_r($vehicle[0]->calval);die;
       	// if($vehicle->fuel_tpe == 1){
       	//  $vsetVal = json_decode($vehSett->calvalp,true);
       	// }else
       	// {
      	//  $vsetVal = json_decode($vehSett->calvald,true);
       	// }
       	// $pDVar   = ($vehicle->fuel_tpe == 1) ? "p" : "d" ;
       	//  $f1      = "calval".$pDVar;
        // $vsetVal = json_decode($vehSett->"calval".$pDVar,true);
        // $calval  = json_decode($vehicle[0]->calval,true);
        // echo "funct = $calval";
        return array('calval' => $vehicle[0]->calval, 'fuel' => $vehicle[0]->fuel_tpe);
    }

    public static function getVehicleSetting(){
       return DB::table('vehicle_setting')->select('*')->where('id','1')->first();
    }
    
    public static function getVehicleInfoDataById($mfd){
       return DB::select("select * from vehinfo where mfd = ?",[$mfd]);
    }

    public static function getVehicleSpecDataById($mfd){
       return DB::select("select * from vehspec where mfd = ?",[$mfd]);
    }

    public static function getVehicleSpecDataByMfdModelVariant($mfd,$model,$variant,$fuel)
    {
        return DB::select("select * from vehspec where mfd = ? and model = ? and varient = ? and fuel = ?",[$mfd,$model,$variant,$fuel]);
    }

    public static function getVehicleSpecDataByMfdModel($mfd,$model){
       return DB::select("select * from vehspec where mfd = ? and model = ?",[$mfd,$model]);
    }

    public static function getdeviceKey($deviceid){
        $device = DB::select("select * from device where device_id=?",[$deviceid]);
        $tm     = strtotime($device[0]->ad_dt);
        // $tm = strtotime($device['ad_dt']);
        $tm *= $device[0]->id;
        $tm  = "$tm";
        $len = strlen($tm);
        $len -= 4;
        $key = substr($tm, $len);
        return $key;
    }

    public static function getVehicleSpecDataBYFields($keyMfd,$keyMode,$fuel,$keyVar,$keyEngC,$mfg_yr){
        return DB::select("select * from vehspec where mfd = ? and model = ? and fuel = ? and varient = ? and engcap = ? and mfg_yr = ?",[$keyMfd,$keyMode,$fuel,$keyVar,$keyEngC,$mfg_yr]);
    }

    public static function getVehicleSpecDataBYFields1($keyMfd,$keyMode,$fuel,$keyVar,$keyEngC){
        return DB::select("select * from vehspec where mfd = ? and model = ? and fuel = ? and varient = ? and engcap = ?",[$keyMfd,$keyMode,$fuel,$keyVar,$keyEngC]);
    }

    public static function getVehicleSpecDataBYFields2($keyMfd,$keyMode){
        return DB::select("select * from vehspec where mfd = ? and model = ? limit 0,1",[$keyMfd,$keyMode]);
    }

    // Set vehicle specification
    public static function setvehSpec($keyMfd,$keyMode,$fuel,$keyVar,$eng_cap,$mfg_yr,$did)
    {
        $res     = Fetch::getVehicleSpecDataBYFields($keyMfd,$keyMode,$fuel,$keyVar,$eng_cap,$mfg_yr);
        $vehSett = Fetch::getVehicleSettingData();
        $cap     = explode(';',$vehSett->engine_capacity);

        $setSpec = array();
        if(!empty($res))
        {
            $data      = $res;
            $arrayData = array();
            
            $arrayData = json_decode($data[0]->specinfo,true);
            foreach($arrayData as $key => $value)
            {
                $setSpec[$key] = $value;
            }
            $setSpec["MAP_Abs_PkLmt3_C"]  = $data[0]->map;
            $setSpec["MAP_Dif_PkLmt3_C"]  = $data[0]->map;
            $setSpec["Max_Torque_C"]      = $data[0]->maxtog;
            $setSpec["Eng_Disp_C"]        = $cap[$eng_cap];
            $return = Fetch::setCaliPramValue($setSpec,$did);
            return $return;
        }
        else
        {
            $res = Fetch::getVehicleSpecDataBYFields1($keyMfd,$keyMode,$fuel,$keyVar,$eng_cap);
            if(!empty($res))
            {
                $data      = $res;
                $arrayData = array();
                
                $arrayData = json_decode($data[0]->specinfo,true);
                foreach($arrayData as $key => $value)
                {
                    $setSpec[$key] = $value;
                }
                $setSpec["MAP_Abs_PkLmt3_C"]  = $data[0]->map;
                $setSpec["MAP_Dif_PkLmt3_C"]  = $data[0]->map;
                $setSpec["Max_Torque_C"]      = $data[0]->maxtog;
                $setSpec["Eng_Disp_C"]        = $cap[$eng_cap];
                $return = Fetch::setCaliPramValue($setSpec,$did);
                return $return;
            }
            else
            {
                $res       = Fetch::getVehicleSpecDataBYFields2($keyMfd,$keyMode);
                $data      = $res;
                $arrayData = array();                           
                $arrayData = json_decode($data[0]->specinfo,true);
                foreach($arrayData as $key => $value)
                {
                    $setSpec[$key] = "";
                }
                $setSpec["MAP_Abs_PkLmt3_C"]  = "";
                $setSpec["MAP_Dif_PkLmt3_C"]  = "";
                $setSpec["Max_Torque_C"]      = "";
                $setSpec["Eng_Disp_C"]        = $cap[$eng_cap];
                $return = Fetch::setCaliPramValue($setSpec,$did);
                return false;
            }
        }
    }

    public static function setCaliPramValue($param,$did)
    {
        $vehSett = Fetch::getVehicleSettingData();
        $vehicle = Fetch::getVehicleDeviceDataById($did);

        $pDVar   = ($vehicle[0]->fuel_tpe == 1) ? "p" : "d";
        $f1      = "calval$pDVar";

        // $calval  = json_decode($vehicle[0]->calval,true);
        // $calval  = !empty($calval) ? $calval : json_decode($vehSett->$f1,true);
        $calval  = json_decode($vehSett->$f1,true);

        foreach($param as $key => $value)
        {
            // $calval[$key]  = (trim(!empty($param[$key]))) ? $param[$key] : $value;
            $calval[$key] = $param[$key];
        }

        $calval = json_encode($calval);
        Common::updateTable('vehicles',array('id' => $vehicle[0]->id),array('calval' => $calval));
        $return = Fetch::setDactvalFromDevice($did,'1');
        return $return;
    }

    public static function getFuelTypeData(){
        return DB::select("select fuel_type_id,fuel_name from fuel_type where status = ?",['1']);
    }

    // Alert Portion Start

    public function getAlertByStatus($status){
        return DB::select("select * from alert where status = ?",[$status]);
    }

    public static function getAlertMails(){
        return DB::select("select * from alert_mails order by alert_category_id asc");
    }

    public static function getAlertCategory(){
        return DB::select("select alert_category_id,name from alert_category where status = ?",['1']);
    }

    public static function getAlertSubCategoryAdd($id){
        return DB::select("SELECT alert_sub_category_id,name FROM alert_sub_category AS a WHERE alert_category_id = ? AND NOT EXISTS (  SELECT * FROM alert_mails AS b WHERE a.alert_sub_category_id=b.alert_sub_category_id )",[$id]);
    }

    public static function getAlertSubCategoryEdit($id){
        return DB::select("select alert_sub_category_id,name from alert_sub_category where alert_category_id = ? and status = ?",[$id,'1']);
    }

    public static function getAlertType(){
        return DB::select("select alert_type_id,alert_type_name from alert_type where status = ?",['1']);
    }

    public static function getAlertMailById($alert_mail_id){
        return DB::select("select * from alert_mails where alert_mail_id = ?",[$alert_mail_id]);
    }
    
    // Check if sub category row already present in alert mail table
    public static function checkSubCategoryValueExist($alert_sub_category_id){
        return DB::select("select * from alert_mails where alert_sub_category_id = ?",[$alert_sub_category_id]);
    }
    // Alert Portion End
    // settings & vehicle data end

    // configuration start
    public static function getUserTypeDataByTypeId($utype){
        return DB::select("select * from user_type where u_type = ?",[$utype]);
    }

    public static function getUserTypeDataByIdOrderBy(){
        return DB::select("select u_type,u_name from user_type where u_type != ? order by u_name",['1']);
    }

    public static function getUserTypeDataByCategoryId($id){
        return DB::select("select u_type,u_name from user_type where u_category = ?",[$id]);
    }

    /* Get count how many user type is there by user type name */
    public static function getCountUserTypeByName($value){
        return DB::select("select count(u_type) as count from user_type where u_name like '$value%'");
    }

    /* Vehicle and Device table join data for vehicle new form process*/
    public static function getVehicleDeviceDataById($deviceID){
        return DB::select("select b.* from device a INNER JOIN vehicles b ON a.id = b.device WHERE a.id = ?",[$deviceID]);
    }

    /* get Dectval from device for vehicle new form process*/ 
    public static function getDactvalFromDeviceByID($deviceID){
        return DB::select("select dactval from device where id = ? limit 0,1",[$deviceID]);
    }

    /* set Dectval to 1 to denote changed vehicle specs*/ 
    public static function setDactvalFromDevice($did,$act){
        $dactval = Fetch::getDactvalFromDeviceByID($did);
        if($dactval[0]->dactval == 1)
        {
            return TRUE;
        }
        else
        {
            return DB::update("update device set dactval = ? where id = ? ",[$act,$did]);
        }
    }

    /* get main module or menu details */
    public static function getMainModules(){
        return DB::select("select id,name,main,sortorder,u_field from module where main = ? order by sortorder",[0]);
    }

    public static function getSubModule($id){
        return DB::select("select id,name,main,sortorder,u_field from module where main = ?",[$id]);
    }

    public static function getModuleById($id){
        return DB::select("select u_field from module where id = ?",[$id]);
    }
    // configuration end
    
    //data start
    public static function getDataById($id){
        return DB::select("select * from corepart where id = ?",[$id]);
    }
   
    public static function getCorePartAll(){
        return DB::select("select *  from corepart order by id desc");
    }
    /*core part listing end here*/

    //value part
    public static function getValuePartAll(){
        return DB::select("select * from valuepart order by id ASC");
    }
    
    public static function getValueById($id){
        return DB::select("select * from valuepart where id = ?",[$id]);
    }

    public static function getColumnStatus($table,$column){
        return DB::select("SHOW COLUMNS FROM $table LIKE '$column'");
    }

    public static function getColumnStatusAll($table){
        return DB::select("SHOW COLUMNS FROM $table");
    }
    
    //data-end
    public static function addValueColumn($column){
        $table = "device_data";
        Schema::table('table', function ($table) {
        $table->string($column);
        });
    }

    public static function getlogdetails($did){
        return DB::select("select a.*,b.* from device_log a,users b where device_id=? and b.uid=a.act_by order by id desc",[$did]);
    }

    //description type
    public static function getDescPartAll(){
        return DB::select("select * from service_desc_sett");
    }

    public static function getDevices_new(){
        return DB::select("select distinct(device_id) from device");
    }

    public static function vehicleid($devid){
        return DB::select("select v.* from vehicles as v, device as d where d.id = v.device and d.device_id = ? ",[$devid]);
    }
    
    public static function update_vehicle($servdt,$servkm,$id){
        return DB::UPDATE("update vehicles set oil_dt= ? ,oil_dist = ?,last_serv_dt= ? ,travel_service= ? WHERE id = ? ",[$servdt,$servkm,$servdt,$servkm,$id]);
    }

    public static function insertappdevice($app_name,$md5,$devid,$appid,$ver,$path,$file_name)
    {
        DB::INSERT("insert into `app_devices` (`app_name`, `md5`, `device`,`app`,`lat_ver`,`lat_ver_path`,`lat_ver_filename`,`upd_status`) VALUES ('$app_name','$md5','$devid','$appid','$ver','$path','$file_name','1')");
        // return DB::getPdo()->lastInsertId();
        $id = DB::getPdo()->lastInsertId();
        // $query = DB::getQueryLog();
        date_default_timezone_get("Asia/Calcutta");
        $dtime = date("Y-m-d h:i:s");
        $user  = session('userdata');
        $uid   = $user['uid'];
        DB::INSERT("insert into `device_conf` (`app_update`, `firmware_update`, `firmware_id`,`app_id`,`commond_update`,`config_update`) VALUES ('1','0','0','$devid','0','0')");
        DB::INSERT("insert into `device_log` (`device_id`, `ad_time`, `log_type`,`log_desc`,`act_by`,`IP`) VALUES ('$devid','$dtime','insert into DB app_devices','insertid $id','$uid','')");
        return $id;
    }

    public static function insertfirmdevice($app_name,$md5,$devid,$appid,$ver,$path,$file_name)
    {
        DB::INSERT("insert into `firmware` (`firm_name`, `md5`, `device`,`firm_id`,`lat_ver`,`lat_ver_path`,`lat_ver_filename`,`upd_status`) VALUES ('$app_name','$md5','$devid','$appid','$ver','$path','$file_name','1')");
         // $query1="insert into `firmware` (`firm_name`, `md5`, `device`,`firm_id`,`lat_ver`,`lat_ver_path`,`lat_ver_filename`) VALUES ('$app_name','$md5','$devid','$appid','$ver','$path','$file_name')";
        
        $id = DB::getPdo()->lastInsertId();
        // $query = DB::getQueryLog();
        date_default_timezone_get("Asia/Calcutta");
        $dtime = date("Y-m-d h:i:s");
        $user  = session('userdata');
        $uid   = $user['uid'];
        DB::INSERT("insert into `device_conf` (`app_update`, `firmware_update`, `firmware_id`,`app_id`,`commond_update`,`config_update`) VALUES ('0','1','$devid','0','0','0')");
        DB::INSERT("insert into `device_log` (`device_id`, `ad_time`, `log_type`,`log_desc`,`act_by`,`IP`) VALUES ('$devid','$dtime','insert into DB firmware','insertid $id','$uid','')");
        return $id;
    }

    public static function updateappdevice($app_name,$md5,$devid,$appid,$ver,$path,$file_name)
    {
        $id = DB::UPDATE("update  `app_devices` set `app_name`='$app_name', `md5`='$md5',`lat_ver`='$ver',`lat_ver_path`='$path',`lat_ver_filename`='$file_name' where app=? and device=?",[$appid,$devid]);
        // return DB::getPdo()->lastInsertId();
        date_default_timezone_get("UTC");
        $dtime = date("Y-m-d H:i:s");
        $user  = session('userdata');
        $uid   = $user['uid'];
        DB::INSERT("insert into `device_conf` (`app_update`, `firmware_update`, `firmware_id`,`app_id`,`commond_update`,`config_update`) VALUES ('1','0','0','$devid','0','0')");
        DB::INSERT("insert into `device_log` (`device_id`, `ad_time`, `log_type`,`log_desc`,`act_by`,`IP`) VALUES ('$devid','$dtime','update into DB app_devices','Update app_devices $appid','$uid','')");
        return $id;
    }

    public static function updatefirmdevice($app_name,$md5,$devid,$appid,$ver,$path,$file_name)
    {
        $id = DB::UPDATE("update  `firmware` set `firm_name`='$app_name', `md5`='$md5',`lat_ver`='$ver',`lat_ver_path`='$path',`lat_ver_filename`='$file_name' where firm_id=? and device=?",[$appid,$devid]);

        date_default_timezone_get("UTC");
        $dtime = date("Y-m-d H:i:s");
        $user  = session('userdata');
        $uid   = $user['uid'];
        DB::INSERT("insert into `device_conf` (`app_update`, `firmware_update`, `firmware_id`,`app_id`,`commond_update`,`config_update`) VALUES ('0','1','$devid','0','0','0')");
        DB::INSERT("insert into `device_log` (`device_id`, `ad_time`, `log_type`,`log_desc`,`act_by`,`IP`) VALUES ('$devid','$dtime','update into DB firmware','Update firm $appid','$uid','')");
        return $id;
    }

    public static function updateappverion($appid,$name,$file_name,$ver,$path,$devall)
    {
        DB::UPDATE("update `app` set `app_name`='$name', `app_desc`='$file_name', `version`='$ver',`app_path`='$path',`devid`='$devall' where id=?",[$appid]);
        DB::INSERT("insert into `device_conf` (`app_update`, `firmware_update`, `firmware_id`,`app_id`,`commond_update`,`config_update`) VALUES ('1','0','0','$devall','0','0')");

        return DB::getPdo()->lastInsertId();
    }
     
    public static function update_app_ver($appid,$devid,$file_name,$ver,$path)
    {
        DB::UPDATE("update `app_devices` set  `lat_ver_filename`='$file_name', `lat_ver`='$ver',`lat_ver_path`='$path',`upd_status`='1' where id=? and device=?",[$appid,$devid]);
        DB::INSERT("insert into `device_conf` (`app_update`, `firmware_update`, `firmware_id`,`app_id`,`commond_update`,`config_update`) VALUES ('1','0','0','$devid','0','0')");

        return DB::getPdo()->lastInsertId();
    }

    public static function update_firm_ver($appid,$devid,$file_name,$ver,$path){
       
         DB::UPDATE("update `firmware` set  `lat_ver_filename`='$file_name', `lat_ver`='$ver',`lat_ver_path`='$path',`upd_status`='1' where id=? and device=?",[$appid,$devid]);
         
         DB::INSERT("insert into `device_conf` (`app_update`, `firmware_update`, `firmware_id`,`app_id`,`commond_update`,`config_update`) VALUES ('0','1','$devid','0','0','0')");
        return DB::getPdo()->lastInsertId();
    }

    public static function updatefirmverion($appid,$name,$file_name,$ver,$path,$devall){
         DB::UPDATE("update `firmware_master` set `firm_name`='$name', `firm_file_name`='$file_name', `firm_ver`='$ver',`firm_path`='$path',`firm_devid`='$devall' where id=?",[$appid]);
         DB::INSERT("insert into `device_conf` (`app_update`, `firmware_update`, `firmware_id`,`app_id`,`commond_update`,`config_update`) VALUES ('0','1','$devall','0','0','0')");
        return DB::getPdo()->lastInsertId();
    }

    public static function insertnewapp($name,$ver,$file,$path,$devid){
         DB::INSERT("insert into `app` (`app_name`, `app_desc`, `version`,`app_path`,`devid`) VALUES ('$name','$file','$ver','$path','$devid' )");
        return DB::getPdo()->lastInsertId();
    }

    public static function insertnewfirm($name,$ver,$file,$path,$devid){
        return DB::INSERT("insert into `firmware_master` (`firm_name`, `firm_file_name`, `firm_ver`,`firm_path`,`firm_devid`) VALUES ('$name','$file','$ver','$path','$devid' )");
    }

    public static function getappdetails($did){
        return DB::SELECT("select a.*,b.* from app a,app_devices b where b.device= ? and a.id=b.app",[$did]);
    }
    
    /*public static function getfirmstatdetails($did){
        return DB::SELECT("select * from device_conf a,firmware b where b.device= ? and a.firmware_update='1' and b.device=a.firmware_id",[$did]);
    }*/
     
    public static function getfirmstatdetails($did){
        return DB::SELECT("select * from firmware  where device= ? and upd_status='1'",[$did]);
    }
    
    /*public static function getappstatdetails($did){
        return DB::SELECT("select  * from device_conf a,app_devices b where b.device= ? and a.app_update='1' and b.device=a.app_id",[$did]);
    }*/

    public static function getappstatdetails($did){
        return DB::SELECT("select * from app_devices where device = ? and upd_status = '1'",[$did]);
    }

    public static function updatecmd($devid,$cmd){
        return DB::UPDATE("update device set dev_cmds='$cmd' ,dev_cmd_status='1' where device_id=?",[$devid]);

    }

    public static function updatefirmres($deviceID,$frmName,$frmVersion,$frmStatus){
        return DB::UPDATE("update firmware set upd_status='$frmStatus',ins_status='1',curr_run_ver='$frmVersion' where firm_name=? and device=?",[$frmName,$deviceID]);
    }

    public static function updatefirmres1($deviceID){
        $id= DB::UPDATE("update device_conf set firmware_update='0' where firmware_id=?",[$deviceID]);
    }

    public static function updateappres($deviceID,$appName,$appVersion,$appStatus){
        return DB::UPDATE("update app_devices set upd_status='$appStatus',ins_status='1',curr_run_ver='$appVersion'  where app_name=? and device=?",[$appName,$deviceID]);
    }

    public static function updateappres1($deviceID){
        return DB::UPDATE("update device_conf set app_update='0' where app_id=?",[$deviceID]);
    }

    public static function getappdetails_new(){
        return DB::SELECT("select * from app");
    }

    public static function getappdetailsbyid($id,$did){
        return DB::SELECT("select * from app_devices where app = ? and device=?",[$id,$did]);
    }

    public static function appupdate($did){
        return DB::SELECT("select count(*) as num from app_devices where device= ? and upd_status='1'",[$did]); 
    }

    public static function firmupdate($did){
        return DB::SELECT("select count(*)  as num from firmware  where device= ? and upd_status='1'",[$did]);
    }

    public static function getfirmdetailsbyid($id,$did){
        return DB::SELECT("select * from firmware where firm_id = ? and device=?",[$id,$did]);
    }

    public static function checkappdevice($id,$did){
        return DB::SELECT("select count(*) as coun from app_devices where app = ? and device=?",[$id,$did]);
    }

    public static function checkfirmdevice($id,$did){
        return DB::SELECT("select count(*) as coun from firmware where firm_id = ? and device=?",[$id,$did]);
    }

    public static function getappdetailsbyids($id){
        return DB::SELECT("select * from app a,app_devices b where a.id = ? and b.app=a.id ",[$id]);
    }

    public static function getfirmdetailsbyids($id){
        return DB::SELECT("select * from firmware_master where id = ? ",[$id]);
    }

    public static function getfirmdetails(){
        return DB::SELECT("select * from firmware_master");
    }

    public static function getfirmdetailsdevice($did){
        return DB::SELECT("select a.*,b.* from firmware_master a,firmware b where b.device=? and a.id=b.firm_id",[$did]);
    }

    public static function update_service($servdt,$servkm,$deviceID,$ad_dt){
        return DB::INSERT("insert into `service` (`id`, `type`, `provider_name`, `sdate`, `kms_read`, `descrp`, `descrp_sub`, `details`, `devid`, `did`, `status`, `ad_by`, `ad_dt`, `spec`) VALUES (NULL, '1', 'Self', '$servdt', '$servkm', '1', '', '', '$deviceID', '', '1', '84', '$ad_dt', '' )");
    }

    public static function insert_vehicle_drive($servdt,$servkm,$deviceID,$ad_dt){
        return DB::INSERT("insert into `service` (`id`, `type`, `provider_name`, `sdate`, `kms_read`, `descrp`, `descrp_sub`, `details`, `devid`, `did`, `status`, `ad_by`, `ad_dt`, `spec`) VALUES (NULL, '1', 'Self', '$servdt', '$servkm', '', '', '', '$deviceID', '', '1', '84', '$ad_dt', '')");
    }

    public static function getservicedetails($did){
        return DB::SELECT("select sdate as dt,kms_read as km,details as dtls from service where devid = ?",[$did]);
    }
    
    public static function update_vehicle_drive($servdt,$servkm,$id){
        return DB::UPDATE("update `vehicles` set `last_serv_dt`='$servdt',`travel_service`='$servkm' WHERE `id` = '$id'");
    }

    public static function update_device_drive($deviceID,$id){
        return DB::UPDATE("update device_drive set `oillife`='0',`oillifep` = '100' where device_id ='$deviceID' and id = '$id'");
    }

    public static function update_device_drive_latest($deviceID){
        return DB::UPDATE("update device_drive set OilLifeDist=0 ,`oillife`='0',`oillifep` = '100' where device_id ='$deviceID' order by id DESC limit 1");
    }

    public static function getOilLifeDist($deviceID)
    {
        return DB::SELECT("select OilLifeDist from device_drive where device_id = ? order by id desc limit 0,1",[$deviceID]);
    }

    public static function updateduedaysapi($deviceID,$dueDays,$km,$dt){ 
        // echo $dt;die;
        return DB::UPDATE("update device_drive set serviceduedays=$dueDays,serviceduekm=$km,servduedayupDt='$dt' where device_id ='$deviceID' order by id DESC limit 1");
    }

    public static function update_device_drive_latest1($deviceID,$days,$km){
        return DB::UPDATE("update device_drive set serviceduedays=$days,serviceduekm=$km,OilLifeDist=0 ,`oillife`='0',`oillifep` = '100' where device_id ='$deviceID' order by id DESC limit 1");
    }

    // Update Olilife when service is added
    public static function updateOillife($did){
      return  DB::Update('update device_drive set OilLifeDist=0,oillife=0,oillifep=100 where device_id = ? order by id DESC limit 1',[$did]);
    }

    public static function device_drive_id($deviceID){
        return DB::select("select (id*1) as val from device_drive where device_id = ? order by id desc limit 0,1",[$deviceID]);
    }

    public static function getDescById($id){
        return DB::select("select * from service_desc_sett where id = ?",[$id]);
    }

    public static function deleteBefore($date,$device_id)
    {
        $id = DB::table('device_data')->where('time_v', '<=', $date)->where('device_id', '=', $device_id)->delete();
        $dtime = date("Y-m-d H:i:s");
        $user  = session('userdata');
        $uid   = $user['uid'];
        DB::INSERT("insert into `device_log` (`device_id`, `ad_time`, `log_type`,`log_desc`,`act_by`,`IP`) VALUES ('$device_id','$dtime','delete from DB device_data','date before $date','$uid','')");
        return $id;
    }

    public static function deleteBeforeRange($fdate,$todate,$device_id)
    {
        $id    = DB::table('device_data')->whereBetween('time_v', [$fdate, $todate])->where('device_id', '=', $device_id)->delete();
        $dtime = date("Y-m-d H:i:s");
        $user  = session('userdata');
        $uid   = $user['uid'];
        DB::INSERT("insert into `device_log` (`device_id`, `ad_time`, `log_type`,`log_desc`,`act_by`,`IP`) VALUES ('$device_id','$dtime','delete from DB device_data','date between $fdate and $todate','$uid','')");
        return $id;
    }

    //Activation Code Start

    public static function getActiveCodeAll(){
        return DB::select("select * from activation_code order by id desc");
    }

    public static function getActiveById($id){
        return DB::select("select * from activation_code where id = ?",[$id]);
    }

    public static function getAlertAllByDeviceId($devid){
        return DB::select("select * from alert where devid = ? order by id desc",[$devid]);
    }

    public static function getAlertAllByDevice(){
        return DB::select("select * from alert  order by id desc");
    }

    public static function getAlertAll(){
        return DB::select("select * from alert order by id desc");
    }

    public static function getAlertShow(){
        return DB::select("select * from alert order by id desc");
    }

    public static function update_vehicle_val($id,$val){
        return DB::update("update vehicles set calval='$val' where id='$id'");
    }

    public static function select_vehicle(){
        return DB::select("select * from vehicles");
    }

    public static function getModalAlert($id){
        return DB::select("select msg from alert_mails where id = ?",[$id]);
    }

    public static function getActivationAll($devid){
        return DB::select("select * from activation_code where mac_id = ? order by id desc" ,[$devid]);
    }

    public static function getActivationAll_new(){
        return DB::select("select * from activation_code order by id desc");
    }

    public static function getActivationByExp($exp){
        $fdate   = date("Y-m-d");
        $NewDate = date('Y-m-d', strtotime("+$exp days"));
        return DB::select("select * from activation_code where expdt between ? and ?",[$fdate,$NewDate]);  
    }

    public static function getActive(){
      return DB::select("select * from activation_code order by id desc");
    }

    public static function getAlertMsg($msg){
        return DB::select("select b.msg from alert_sub_category a join alert_mails b on a.alert_sub_category_id = b.alert_sub_category_id where a.name='$msg'");
    }

    public static function getAlertMsgByIdFromAlerts($id){
        return DB::select("select msg from alert where id = ?",[$id]);
    }

    public static function deleteAlert($ids){
        return DB::select("delete from alert where id in($ids)");
    }

    public static function updateDeviceCmdCol($deviceid,$dev_cmds,$currVer){
        return DB::update("update device set dev_cmds = ?,curr_ver = ? where device_id = ?",[$dev_cmds,$currVer,$deviceid]);
    }

    public static function updateDeviceCmdres($deviceid,$dev_res){
        return DB::update("update device set dev_res = ? where device_id = ?",[$dev_res,$deviceid]);
    }

    public static function updatedeviceDtcv($deviceID,$dtcv,$dtc){
        return DB::update("update device set dtcv = ?,dtc = ? where device_id = ?",[$dtcv,$dtc,$deviceID]);
    }

    public static function getdeviceActStatus($deviceid){
        return DB::select("select d.status from device as d where d.device_id = ?",[$deviceid]);
    }
    //Activation Code End

    // Glossery Start
    public static function getGlossary(){
        return DB::select("select glossary from glossary where id = '1'");
    }
    // Glossery End

    // services start
    public static function getDeviceListByUserType($user_id,$userCategory,$search = '')
    {
        $sql = "";
        if(!empty($search)){
             $sql = " and (a.uname LIKE '%$search%' or b.device_id LIKE '%$search%')";
        }
        
        if($userCategory == 1){
            // Super Admin
            return DB::select("select a.uname as device_name, b.device_id as device_id, b.total_dist, c.manufact as manu, c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and b.status = ? and b.id=a.device and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1','1',]);
        }elseif($userCategory == 2){
            // Admin
            return DB::select("select a.uname as device_name,b.device_id as device_id, b.total_dist, c.manufact as manu,c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and b.status = ? and b.id=a.device and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1','1',]);
        }elseif($userCategory == 3){
            // Partner
            return DB::select("select a.uname as device_name,b.device_id as device_id, b.total_dist,c.manufact as manu,c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and a.partner = ? and b.status = ? and b.id=a.device and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1',$user_id,'1',]);
        }else{
            return DB::select("select a.uname as device_name,b.device_id as device_id,b.total_dist, c.manufact as manu, c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and a.uid = ? and b.status = ? and b.id=a.device and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1',$user_id,'1',]);
        }
    }

    public static function getDeviceListByUserType_can($user_id,$userCategory,$search = ''){
        $sql = "";
        if(!empty($search)){
             $sql = " and (a.uname LIKE '%$search%' or b.device_id LIKE '%$search%')";
        }
        
        if($userCategory == 1){
            // Super Admin
            return DB::select("select a.uname as device_name, b.device_id as device_id, b.total_dist, c.manufact as manu, c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and b.status = ? and b.id=a.device and b.devtype='3' and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1','1',]);
        }elseif($userCategory == 2){
            // Admin
            return DB::select("select a.uname as device_name,b.device_id as device_id, b.total_dist, c.manufact as manu,c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and b.status = ? and b.id=a.device and b.devtype='3' and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1','1',]);
        }elseif($userCategory == 3){
            // Partner
            return DB::select("select a.uname as device_name,b.device_id as device_id, b.total_dist,c.manufact as manu,c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and a.partner = ? and b.status = ? and b.id=a.device and b.devtype='3' and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1',$user_id,'1',]);
        }else{
            return DB::select("select a.uname as device_name,b.device_id as device_id,b.total_dist, c.manufact as manu, c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and a.uid = ? and b.status = ? and b.id=a.device and b.devtype='3' and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1',$user_id,'1',]);
        }
    }

    public static function getDeviceListByUserType_obd($user_id,$userCategory,$search = '')
    {
        $sql = "";
        if(!empty($search)){
             $sql = " and (a.uname LIKE '%$search%' or b.device_id LIKE '%$search%')";
        }
        echo $sql;
        if($userCategory == 1){
            // Super Admin
            return DB::select("select a.uname as device_name, b.device_id as device_id, b.total_dist, c.manufact as manu, c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and b.status = ? and b.id=a.device and b.devtype !='3' and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1','1',]);
        }elseif($userCategory == 2){
            // Admin
            return DB::select("select a.uname as device_name,b.device_id as device_id, b.total_dist, c.manufact as manu,c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and b.status = ? and b.id=a.device and b.devtype !='3' and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1','1',]);
        }elseif($userCategory == 3){
            // Partner
            return DB::select("select a.uname as device_name,b.device_id as device_id, b.total_dist,c.manufact as manu,c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and a.partner = ? and b.status = ? and b.id=a.device and b.devtype !='3' and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1',$user_id,'1',]);
        }else{
            return DB::select("select a.uname as device_name,b.device_id as device_id,b.total_dist, c.manufact as manu, c.model, c.reg_no,c.varient, c.travel_purchase from users as a, device as b, vehicles as c where a.ustatus = ? and a.uid = ? and b.status = ? and b.id=a.device and b.devtype !='3' and c.device=a.device and c.manufact!='' $sql order by a.uid desc",['1',$user_id,'1',]);
        }
    }

    public static function getServiceTypeSetting(){
        return DB::select("select * from service_type_sett where status = ?",['1']);
    }

    public static function getServiceDescSetting(){
        return DB::select("select * from service_desc_sett where status = ?",['1']);
    }

    public static function getServiceDescSettingTitleNotNull(){
        return DB::select("select * from service_desc_sett where status = ? and sub_title !=''",['1']);
    }

    public static function getServiceDescSettingTitleNotNullById($val){
        return DB::select("select * from service_desc_sett where status = ? and sub_title !='' and id = ?",['1',$val]);
    }

    public static function getServiceHistoryByDevId($devid){
        return DB::select("select * from service where status = ? and devid = ? order by sdate desc",['1',$devid]);
    }
 
    // get service dates by id
    public static function getServiceDateByDeviceId($devId,$sdate){
        return DB::select("select sdate from service where status = ? and devid = ? group by sdate order by sdate asc",['1',$devId,$sdate]);
    }

    public static function getSubTitleServicesBYId($sid){
        return DB::select("select sub_title from service_desc_sett where status = ? and id = ?",['1',$sid]);
    }

    // service end

    // Actions Start    
    public static function getdriverDetails($deviceid){
         return DB::select("select u.* from users as u, device as d where d.id = u.device and d.device_id = ?",[$deviceid]);
    }

    public static function getdriver($deviceid){
         return DB::select("select u.* from users as u, device as d where d.id=u.device and d.device_id = ?",[$deviceid]);
    }

    public static function getVehicleDeviceDetails($deviceid){
        return DB::select("select v.* from vehicles as v, device as d where d.id = v.device and d.device_id = ?",[$deviceid]);
    }
    
    public static function getCarsInRed($category,$user,$devid = ''){
        if($category == 4 || $category == 3){
            return DB::select("select * from action_dev where status = ? and devid in ($devid) order by ad_dt desc",['1']);
        }else{
            return DB::select("select * from action_dev where status = ? order by ad_dt desc",['1']);
        }
    }

    public static function getAssistanceMail($category,$devid = ''){

        if($category == 4 || $category == 3){
            return DB::select("select * from assistance_mail where status = ? and devid in ($devid) order by ad_dt desc",['1']);
        }else{
            return DB::select("select * from assistance_mail where status = ? order by ad_dt desc",['1']);
        }
    }

    public static function getAssistanceMailByStatus($category,$devid = ''){
        if($category == 1){
            return DB::select("select count(id) as count,ANY_VALUE(id) as id,ANY_VALUE(did) as did from assistance_mail where status = ? and super_view = ? and devid in ($devid) group by did order by devid desc",['1','1']);
        }elseif($category == 2){
            return DB::select("select count(id) as count,ANY_VALUE(id) as id,ANY_VALUE(did) as did from assistance_mail where status = ? and admin_view = ? and devid in ($devid) group by did order by devid desc",['1','1']);
        }elseif($category == 3){
            return DB::select("select count(id) as count,ANY_VALUE(id) as id,ANY_VALUE(did) as did from assistance_mail where status = ? and partner_view = ? and devid in ($devid) group by did order by devid desc",['1','1']);
        }
    }
    // Actions End

    // Map
    public static function getDevicesLatLong($uid,$utype){
        if($utype==1 || $utype==2){
            return DB::select("SELECT a.uname AS device_name, b.device_id AS device_id, b.latitude, b.longitude, c.manufact AS manu,b.ad_dt as etime
                FROM users AS a, device AS b, vehicles AS c
                WHERE a.ustatus =  '1'
                AND b.status =  '1'
                AND b.id = a.device
                AND c.device = a.device
                AND b.latitude IS NOT NULL 
                AND b.longitude IS NOT NULL 
                ORDER BY a.uid DESC ");
            }elseif($utype==3){
                return DB::select("SELECT a.uname AS device_name, b.device_id AS device_id, b.latitude, b.longitude, c.manufact AS manu,b.ad_dt as etime
                FROM users AS a, device AS b, vehicles AS c
                WHERE a.ustatus =  '1'
                AND b.status =  '1'
                AND b.id = a.device
                AND c.device = a.device
                and b.partner='$uid'
                AND b.latitude IS NOT NULL 
                AND b.longitude IS NOT NULL 
                ORDER BY a.uid DESC ");
            }else{
                return DB::select("SELECT a.uname AS device_name, b.device_id AS device_id, b.latitude, b.longitude, c.manufact AS manu,b.ad_dt as etime
                FROM users AS a, device AS b, vehicles AS c
                WHERE a.ustatus =  '1'
                AND b.status =  '1'
                AND b.id = a.device
                AND c.device = a.device
                and b.partner='$uid'
                AND b.latitude IS NOT NULL 
                AND b.longitude IS NOT NULL 
                ORDER BY a.uid DESC ");
            }
    }

    // Diagnostics

    public static function getDriveValue($did,$fld,$op='sum'){
        $result = DB::select("select $op($fld*1) as val from ( select $fld from device_drive where device_id = ? and ($fld*1)>=0 and (dur*1)>0 order by id desc, ad_dt desc limit 0,1) as $fld",[$did]);
        if(empty($result)){
           return 0;
           exit();
        }
        return (trim($result[0]->val) != "") ? $result[0]->val:0;
    }

    public static function getDriveMoniterMain($did){
         return DB::select("select * from device_monitormain where device_id = ? and drive<>'' and dur <> '' order by id desc limit 0,1",[$did]);
    }

    public static function getDeviceCalib($did){
         return DB::select("select * from device_calib where device_id = ? and drive <>'' and dur <> '' order by id desc limit 0,1",[$did]);
    }

    public static function getDriveMoniter($did){
        return DB::select("select * from device_monitor where device_id = ? and drive<>'' and dur <> '' order by id desc limit 0,1",[$did]);
    }
    public static function getDriveMoniter_rating($did){
        return DB::select("select * from monitor_ratings where device_id = ? ORDER BY `id` DESC LIMIT 1 ",[$did]);
    }
    // getdtccomment
    public static function getdtccomment($dtc){
        return DB::select("select description from dtc_list where dtc_num = ? ",[$dtc]);
    }
    // get Drive
    public static function getDriveDatesByDeviceId($devId){
        return DB::select("select date(time_v) as date from dd_jan where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_feb where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_mar where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_apr where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_may where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_june where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_july where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_aug where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_sep where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_oct where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_nov where device_id = '$devId'
            UNION
                select date(time_v) as date from dd_dec where device_id = '$devId'

            UNION
                select date(time_v) as date from device_data
                where device_id = '$devId'
                group by date(time_v)");
    }

    // public static function getDriveDatesByDeviceId($devId){
 //        return DB::select("select date(time_v) as date from device_data where device_id = ? group by date(time_v)",[$devId]);
 //    }

    public static function getDriveDatesByDeviceIdEvent($devId){
        return DB::select("select date(ad_dt) as date from event where device_id = ? group by date(ad_dt)",[$devId]);
    }

    public static function getparametr1($Id){
        return DB::select("SELECT * FROM valuepart where id=?",[$Id]);
    }
    public static function getparametr($Id1,$Id2,$Id3){
        return DB::select("SELECT * FROM valuepart where id in (?,?,?)",[$Id1,$Id2,$Id3]);
    }

    public static function getDriveLatLongById($devId,$date,$a,$b,$c)
    {
    	// date('m', strtotime($date));
		if(date('m', strtotime($date))=='03'){
			$table = 'dd_mar';
		}
		elseif(date('m', strtotime($date))=='04'){
			$table = 'dd_apr';
		}
		elseif(date('m', strtotime($date))=='05'){
			$table = 'dd_may';
		}
		elseif(date('m', strtotime($date))=='06'){
			$table = 'dd_june';
		}
		elseif(date('m', strtotime($date))=='07'){
			$table = 'dd_july';
		}
		elseif(date('m', strtotime($date))=='08'){
			$table = 'dd_aug';
		}
		elseif(date('m', strtotime($date))=='09'){
			$table = 'dd_sep';
		}
		elseif(date('m', strtotime($date))=='10'){
			$table = 'dd_oct';
		}
		elseif(date('m', strtotime($date))=='11'){
			$table = 'dd_nov';
		}
		elseif(date('m', strtotime($date))=='12'){
			$table = 'dd_dec';
		}
		elseif(date('m', strtotime($date))=='01'){
			$table = 'dd_jan';
		}
		elseif(date('m', strtotime($date))=='02'){
			$table = 'dd_feb';
		}
		// print_r($table);die;
  		return DB::select("select device_id,lat,lon,$a,$b,$c,date(time_v) as date,time_v from $table where device_id = ? and date(time_v) = ? and lat != ''
            UNION
                select device_id,lat,lon,$a,$b,$c,date(time_v) as date,time_v from device_data
                where device_id = ? and date(time_v) = ? and lat != '' order by time_v asc",[$devId,$date,$devId,$date]);
	}
	
	// public static function getDriveLatLongById($devId,$date){
 //    	return DB::select("select * from device_data where device_id = ? and date(time_v) = ? and alt !=' ' order by time_v asc",[$devId,$date]);
 //    }

    public static function getDriveLatLongByIdEvent($devId,$date){
        return DB::select("select * from event where device_id = ? and date(ad_dt) = ? order by id asc",[$devId,$date]);
    }

    public static function getValuePartById($id){
        return DB::select("select id,title,device1,device2,decim,unit,device_data_column from valuepart where id = ? and type = ? and status = ?",[$id,'2','1']);
    }

    // get Value part list for eva
    public static function getValuePartList(){
        return DB::select("select * from valuepart where type = ? and status = ?",['2','1']);
    }

    public static function getValuePartList1($did){
        return DB::select("select * from bt_dev where device_id = ? ",[$did]);
    }
 
    public static function getValuePart($did,$cal,$unit){
        return DB::select("select $cal as title ,$unit as unit,id from bt_dev where device_id = ? and bt_dev_make='Numato' ",[$did]);
    }

    public static function insertbt_data($queryM){
        return DB::insert("$queryM");
    }

    public static function getValuePartListobd(){
        return DB::select("select * from valuepart where type = ? and status = ? and dtype LIKE '%2%' and dtype LIKE '%1%' ",['2','1']);
    } 

    public static function getCsvDataEva($start,$end,$did)
    {
        ini_set('max_execution_time', '3000');
        ini_set('memory_limit', '100000000M');

        if(date('m', strtotime($start))=='03'){
            $table = 'dd_mar';
        }
        elseif(date('m', strtotime($start))=='04'){
            $table = 'dd_apr';
        }
        elseif(date('m', strtotime($start))=='05'){
            $table = 'dd_may';
        }
        elseif(date('m', strtotime($start))=='06'){
            $table = 'dd_june';
        }
        elseif(date('m', strtotime($start))=='07'){
            $table = 'dd_july';
        }
        elseif(date('m', strtotime($start))=='08'){
            $table = 'dd_aug';
        }
        elseif(date('m', strtotime($start))=='09'){
            $table = 'dd_sep';
        }
        elseif(date('m', strtotime($start))=='10'){
            $table = 'dd_oct';
        }
        elseif(date('m', strtotime($start))=='11'){
            $table = 'dd_nov';
        }
        elseif(date('m', strtotime($start))=='12'){
            $table = 'dd_dec';
        }
        elseif(date('m', strtotime($start))=='01'){
            $table = 'dd_jan';
        }
        elseif(date('m', strtotime($start))=='02'){
            $table = 'dd_feb';
        }

        $sql = "select * from $table where device_id = '$did' and time_v between '$start' and '$end'
            UNION
                select * from device_data where device_id = '$did' and time_v between '$start' and '$end' limit 10";
        return DB::select($sql);
    }
    
    // public static function getCsvDataEva($start,$end,$did){
 //        ini_set('max_execution_time', '3000');
 //        ini_set('memory_limit', '100000000M');
 //        return DB::select("select * from device_data where device_id = '$did' and time_v between '$start' and '$end' limit 10");
 //    }

    public static function getDeviceDataGraph($dev,$drF,$drT,$fName,$fName1,$fName2,$vehicle,$vin_no,$test_type)
    {  
        $vin_query = "";
        $test_query = "";
        if(!empty($vehicle))
        {
            if(!empty($vin_no))
            {
                $vin_query = "and vin_no = '$vin_no'";
            }
            $date = date('Y-m-d', strtotime($drF));
            $sql1    = "select drive from device_drive where vehicle='$vehicle' and dt='$date' $vin_query";
            // echo $sql1;die; 
            $driveid = DB::select($sql1);
            // print_r($driveid);
            // if(!empty($driveid))
            // { 
            //     foreach ( $driveid as $key => $value)
            //     {
            //       // print_r($value->drive);
            //       $driveids[]= $value->drive;
            //     }
            //     $deviceidsstr=implode(',', $driveids);
            // }
            // else
            // {
            //     $deviceidsstr = '0';
            // }
            // echo $deviceidsstr;
        }
        else if(!empty($vin_no))
        {
            $vin_query = "and vin_no = '$vin_no'";
            $date    = date('Y-m-d', strtotime($drF));
	        $sql1    = "select distinct drive from device_drive where dt='$date' $vin_query";
	        $driveid = DB::select($sql1);
        }
        

        $deviceidsstr = "";
        TODO: "CHECK THIS LOGIC";
        if(!empty($driveid))
        {
            if(count($driveid) == 1)
            {
                $deviceidsstr = "'".$driveid[0]->drive."'";
            }
            else
            {
                $i = 1;
                foreach ($driveid as $key => $value)
                {
                    if(count($driveid) == $i)
                    {
                        $deviceidsstr.= "'".$value->drive."'";
                    }
                    else
                    {
                        $deviceidsstr.= "'".$value->drive."',";
                    }
                    // $deviceidsstr.= "'".$value->drive."'";
                    $i++;
                }
                // $deviceidsstr = implode(',', $driveids);
            }
        }
        else
        {
            $deviceidsstr = '0';
        }
        
        if(!empty($test_type))
        {
            if($test_type == 3)
            {
                $test_query = " and F43 >= '3' and F43 <= '3.9'";
            }
            else
            {
                $test_query = " and F43 = '$test_type'";
            }
        }
        $string   = '';
        $sqlwhere = '';
        if(!empty($fName)){
            $string.=$fName.',';
            $sqlwhere.=" $fName <> '' and";
        }
        if(!empty($fName1)){
            $string.=$fName1.',';
            $sqlwhere.= " $fName1 <> '' and";
        }
        if(!empty($fName2)){
            $string.=$fName2.',';
            $sqlwhere.= " $fName2 <> '' and ";
        }
        
        
        $sqlExt = " device_id = '$dev' and (time_v >= '$drF' and time_v <= '$drT')" ;
        
        if(date('m', strtotime($drF))=='03'){
            $table = 'dd_mar';
        }
        elseif(date('m', strtotime($drF))=='04'){
            $table = 'dd_apr';
        }
        elseif(date('m', strtotime($drF))=='05'){
            $table = 'dd_may';
        }
        elseif(date('m', strtotime($drF))=='06'){
            $table = 'dd_june';
        }
        elseif(date('m', strtotime($drF))=='07'){
            $table = 'dd_july';
        }
        elseif(date('m', strtotime($drF))=='08'){
            $table = 'dd_aug';
        }
        elseif(date('m', strtotime($drF))=='09'){
            $table = 'dd_sep';
        }
        elseif(date('m', strtotime($drF))=='10'){
            $table = 'dd_oct';
        }
        elseif(date('m', strtotime($drF))=='11'){
            $table = 'dd_nov';
        }
        elseif(date('m', strtotime($drF))=='12'){
            $table = 'dd_dec';
        }
        elseif(date('m', strtotime($drF))=='01'){
            $table = 'dd_jan';
        }
        elseif(date('m', strtotime($drF))=='02'){
            $table = 'dd_feb';
        }

        // Get data from selected vehicle
        if(!empty($vehicle) || !empty($vin_no)){
            $sql = "select $string time_s,time_v,date(time_v) as date from $table  where $sqlExt and Fsprint in($deviceidsstr) $test_query group by time_s order by time_s asc";
        }
        else
        {
            $sql = "select $string time_s,time_v,date(time_v) as date from $table  where $sqlExt $test_query group by time_s  order by time_s asc";
        }
       
        // echo $sql;
        return DB::select($sql);
    }

     public static function getDeviceDataGraph1($dev,$time_v,$fName)
     {
        // print_r($time_v);die;
        $sqlExt = " and device_id = '$dev' and time_s='$time_v' " ;
        $sql    = "select $fName, time_v, time_s from dd_jan where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_feb where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_mar where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_apr where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_may where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_june where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_july where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_aug where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_sep where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_oct where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_nov where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from dd_dec where $fName <> '' $sqlExt
            UNION
                select $fName, time_v, time_s from device_data
                where $fName <> '' $sqlExt group by time_s order by time_s asc";
        return DB::select($sql);
    }

    public static function getparameter(){
        return DB::select("select * from alert_sub_category",[1]);
    }

    public static function checkpe($id,$did){
        // DB::enableQueryLog();
        return DB::select("select * from alert_dp  where device_id =? and sub_cat= ?",[$did,$id]);  
	}
    
    public static function saveparameter($id,$oper,$value,$did){
        return DB::UPDATE("INSERT into alert_dp  (`sub_cat`, `device_id`,`operator`,`value`,`status`) VALUES('$id','$did','$oper','$value','1') ",[$id]);
    }
    
    public static function saveparameter1($id,$oper,$value,$did){
        return DB::UPDATE("Update alert_dp  set `operator`='$oper',`value`='$value',`status`='1' where device_id =? and sub_cat= ?",[$did,$id]);
    }

	public static function retriveCsvEva($start,$end,$did,$vehicle,$vin_no,$test_type)
    {
        $vin_query = "";
        $test_query = "";
        if(!empty($vehicle))
        {
            if(!empty($vin_no))
            {
                $vin_query = "and vin_no = '$vin_no'";
            }
            $date = date('Y-m-d', strtotime($start));
            $sql1    = "select drive from device_drive where vehicle='$vehicle' and dt='$date' $vin_query";
            // echo $sql1;die; 
            $driveid = DB::select($sql1);
            // print_r($driveid);die;
            
            // echo $deviceidsstr;
        }
        else if(!empty($vin_no))
        {
            $vin_query = "and vin_no = '$vin_no'";
            $date = date('Y-m-d', strtotime($start));
        	$sql1    = "select drive from device_drive where dt='$date' $vin_query";
        	$driveid = DB::select($sql1);
        }
        
        // echo $sql1;die; 
        
        // print_r($driveid);die;
    	if(!empty($driveid))
        { 
        	$deviceidsstr = '';
            if(count($driveid) == 1)
            {
                $deviceidsstr = "'".$driveid[0]->drive."'";
            }
            else
            {
                $i = 1;
                foreach ($driveid as $key => $value)
                {
                    if(count($driveid) == $i)
                    {
                        $deviceidsstr.= "'".$value->drive."'";
                    }
                    else
                    {
                        $deviceidsstr.= "'".$value->drive."',";
                    }
                    // $deviceidsstr.= "'".$value->drive."'";
                    $i++;
                }
                // $deviceidsstr = implode(',', $driveids);
            }
        }
        else
        {
            $deviceidsstr = '0';
        }
        
        if(!empty($test_type))
        {
            if($test_type == 3)
            {
                $test_query = " and F43 >= '3' and F43 <= '3.9'";
            }
            else
            {
                $test_query = " and F43 = '$test_type'";
            }
        }
        if(date('m', strtotime($start))=='03'){
            $table = 'dd_mar';
        }
        elseif(date('m', strtotime($start))=='04'){
            $table = 'dd_apr';
        }
        elseif(date('m', strtotime($start))=='05'){
            $table = 'dd_may';
        }
        elseif(date('m', strtotime($start))=='06'){
            $table = 'dd_june';
        }
        elseif(date('m', strtotime($start))=='07'){
            $table = 'dd_july';
        }
        elseif(date('m', strtotime($start))=='08'){
            $table = 'dd_aug';
        }
        elseif(date('m', strtotime($start))=='09'){
            $table = 'dd_sep';
        }
        elseif(date('m', strtotime($start))=='10'){
            $table = 'dd_oct';
        }
        elseif(date('m', strtotime($start))=='11'){
            $table = 'dd_nov';
        }
        elseif(date('m', strtotime($start))=='12'){
            $table = 'dd_dec';
        }
        elseif(date('m', strtotime($start))=='01'){
            $table = 'dd_jan';
        }
        elseif(date('m', strtotime($start))=='02'){
            $table = 'dd_feb';
        }
        // echo $table;die;
        if(!empty($vehicle) || !empty($vin_no)){
            
            $sql = "select * from $table where device_id = '$did' and time_v between '$start' and '$end' and Fsprint in($deviceidsstr) $test_query GROUP BY 
            time_s";
            // echo $deviceidsstr; Fsprint in($deviceidsstr)
        }
        else
        {
            $sql = "select * from $table where device_id = '$did' and time_v between '$start' and '$end' $test_query GROUP BY 
            time_s";
        }
        // echo $sql;die;
        return DB::select($sql);
    }

    // public static function retriveCsvEva($start,$end,$did)
    // {
    //     $sql = "select * from device_data where device_id = '$did' and time_v between '$start' and '$end'";
    //     return DB::select($sql);
    // }

    public static function getfields($table){
        $sql = "SHOW columns FROM $table";
        return DB::select($sql);
        // print_r($device_data_col);die;
    }

    public static function getTableFields($table){
        $sql             = "SHOW columns FROM $table";
        $device_data_col = DB::select($sql);
        $vestFields = array();
        if (!empty($device_data_col))
        {
            foreach ($device_data_col as $row)
            {
                $vestFields[] = $row->Field;
            }
        }
        return $vestFields;
    }

    // Statistic 

    public static function getDriveStat($did,$drive,$fld,$op='sum'){
        return DB::select("select $op($fld*1) as val from (select $fld from device_drive where device_id = ? and (dur*1) > 0 order by id desc, ad_dt desc limit 0,$drive) as $fld",[$did]);
    }

    public static function getAvgStatsDrive($did,$drive,$fld,$fld1){
        return DB::select("select $fld,$fld1 from device_drive where device_id = ? order by id desc, ad_dt desc limit 0,$drive",[$did]);
    }

    public static function getDriveStatRange($did,$fr,$to,$fld,$op='sum'){
        return DB::select("select $op($fld*1) as val from device_drive where device_id = ? and dt >= ? and dt <= ? and ($fld*1)>=0 and (dur*1)>0 order by id desc, ad_dt",[$did,$fr,$to]);
    }

    public static function getDriveStatRange1($did,$year,$fld,$op='sum'){
        return DB::select("select MONTH(dt) as date ,$op($fld*1) as val from device_drive where device_id = ? and YEAR(dt) = ? and ($fld*1)>=0 and (dur*1)>0 group by date ",[$did,$year]);
    }

    public static function getDriveStatRange2($did,$year,$month,$fld,$op='sum'){
        return DB::select("select DAY(dt) as date ,$op($fld*1) as val from device_drive where device_id = ? and YEAR(dt) = ? and MONTH(dt) = ? and ($fld*1)>=0 and (dur*1)>0 group by date ",[$did,$year,$month]);
    }

    public static function getDriveStatRangeCaiLite($did,$drive){
        // return DB::select("select * from device_data_s where device_id = ? and time_v >= ? and time_v <= ? and (dur*1)>0 order by id desc",[$did,$fr,$to]);
        return DB::select("select * from device_data_s  where device_id = ? and (dur*1) > 0 order by id desc limit 0,$drive",[$did]);
    }
    public static function getDriveStatRangeCaiLitebydrive($did,$drive){
        // return DB::select("select * from device_data_s where device_id = ? and time_v >= ? and time_v <= ? and (dur*1)>0 order by id desc",[$did,$fr,$to]);
        return DB::select("select * from device_data_s where device_id = ? and (dur*1) > 0 and nodrive = $drive",[$did]);
    }
    public static function getDriveStatRangeCaiLiteLatest($deviceID,$driveSrt,$driveEnd){
        // echo "select * from device_data_s where device_id = '$deviceID' and nodrive >= '$driveSrt' and nodrive <= '$driveEnd'";die;
        return DB::select("select * from device_data_s where device_id = ? and nodrive >= ? and nodrive <= ? order by id desc",[$deviceID,$driveSrt,$driveEnd]);
    }

    // For stats of Mileage and avg speed
    public static function getAvgStatsDays($did,$fr,$to,$fld,$fld1){
        return DB::select("select $fld1,$fld from device_drive where device_id = ? and dt >= ? and dt <= ? and ($fld*1)>=0 order by id desc",[$did,$fr,$to]);
    }

    public static function getAvgStatsDays1($did,$year,$fld,$fld1){
        return DB::select("select MONTH(dt) as date, MIN($fld1) as $fld1 ,MIN($fld) as $fld from device_drive where device_id = ? and  YEAR(dt)=? and ($fld*1)>=0 group by date",[$did,$year]);
    }

    public static function getAvgStatsDays2($did,$year,$month,$fld,$fld1){
        return DB::select("select DAY(dt) as date, MIN($fld1) as $fld1 ,MIN($fld) as $fld from device_drive where device_id = ? and  YEAR(dt)=?  and MONTH(dt) =? and ($fld*1)>=0 group by date",[$did,$year,$month]);
    }

    public static function setDriveStatHlthScr($postVal){
        $columns = array_keys($postVal);
        $values  = array_values($postVal);
        return DB::insert("INSERT INTO driver (" . implode(", ", $columns) . ") VALUES ('" . implode("', '", $values) . "')");
    }

    public static function getDriveStatHlthScr($did){
        return DB::select("select hlt_score,drv_score from driver where device_id = ? order by id desc limit 0,1",[$did]);
    }

    // reports
    public static function getReportListByDeviceId($did){
        return DB::select("select a.id as id,a.drive as drive,a.dt as dt,b.vehicle as vehicle from device_monitor a,device_drive b where a.device_id = '$did' and b.device_id = '$did' and b.drive=a.drive order by id desc",['0']);
    }

    public static function getReportListAll($cond = ''){
        return DB::select("select * from report where status = ? $cond order by id desc",['1']);
    }


    public static function getReportListAllInactive($cond=''){
        return DB::select("select * from report where $cond order by id desc");
    }

    public static function deleteReports($ids){
        return DB::select("delete from report where id in($ids)");
    }

    public static function updateActivateReports($ids){
        return DB::select("update report set status = ? where id in($ids)",['1']);
    }

    public static function getMaxMinDistanceDays($did,$cron_start,$cron_end){
        return DB::select("SELECT sum(dist) as dist, sum(dur) as dur FROM `device_data_s` WHERE device_id = ? and time_v between '$cron_start' and '$cron_end' GROUP BY date(time_v)",[$did]);
    }
    
    // Distance and Duration
    public static function getSumForCronVehicle($did,$cron_start,$cron_end)
    {
        return DB::select(
                "select sum(cast(dist as DECIMAL(10,4))) as distance,
                sum(cast(dur as DECIMAL(10,4))) as duration,
                avg(cast(dist as DECIMAL(10,4))) as avg_distance,
                max(cast(dist as DECIMAL(10,4))) as max_distance,
                min(cast(dist as DECIMAL(10,4))) as min_distance,
                avg(cast(avg_speed as DECIMAL(10,4))) as avg_speed,
                max(cast(max_sp as DECIMAL(10,4))) as max_speed,
                sum(cast(idt as DECIMAL(10,4))) as idle_time,
                sum(cast(dur_acc as DECIMAL(10,4))) as accelaration_time,
                sum(cast(dur_cruise as DECIMAL(10,4))) as cruise_time,
                sum(cast(dur_coast_down as DECIMAL(10,4))) as coast_downtime,
                sum(cast(dur_break as DECIMAL(10,4))) as brake_time,
                sum(cast(veh_start_cnt as DECIMAL(10,4))) as no_starts,
                sum(cast(crnk_cnt as DECIMAL(10,4))) as sum_cranks,
                max(cast(max_crnk_cnt as DECIMAL(10,4))) as max_cranks,
                sum(cast(vehstall as DECIMAL(10,4))) as no_stalls,
                max(cast(max_drive_rpm as DECIMAL(10,4))) as max_rpm,
                MIN(NULLIF(min_drive_rpm, 0)) as min_rpm,
                sum(cast(rev_lmt_cross_cnt as DECIMAL(10,4))) as lmt_cross,
                avg(cast(avg_drive_thr as DECIMAL(10,4))) as avg_throttle,
                max(cast(max_drive_thr as DECIMAL(10,4))) as max_throttle,
                min(cast(min_drive_thr as DECIMAL(10,4))) as min_throttle,
                max(cast(max_power as DECIMAL(10,4))) as max_power,
                max(cast(max_tor as DECIMAL(10,4))) as max_torque,
                avg(cast(avg_cool as DECIMAL(10,4))) as avg_cool,
                max(cast(max_cool as DECIMAL(10,4))) as max_cool,
                min(cast(min_batv as DECIMAL(10,4))) as min_batt_volt,
                avg(cast(min_batv as DECIMAL(10,4))) as avg_batt_volt,
                max(cast(max_batv as DECIMAL(10,4))) as max_batt_volt,
                sum(cast(dur_key_on as DECIMAL(10,4))) as keyon_time 
                from device_data_s where 
                device_id = '$did' and 
                time_v between '$cron_start' and '$cron_end'");
    }

    /*public static function getHeatGraphDeviceData1($did,$val1,$val2,$start,$end)
    {
        $val1 = "F".$val1;
        $val2 = "F".$val2;

        return DB::select("select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_jan
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_feb
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_mar
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_apr
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_may
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_june
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_july
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_aug
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_sep
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_oct
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_nov
            UNION
            select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from dd_dec where device_id = ? and time_v between '$start' and '$end'",[$did]);
    }*/

    public static function getHeatGraphDeviceData1($did,$val1,$val2,$start,$end){

        if(date('m', strtotime($start))=='03'){
            $table1 = 'dd_mar';
        }
        elseif(date('m', strtotime($start))=='04'){
            $table1 = 'dd_apr';
        }
        elseif(date('m', strtotime($start))=='05'){
            $table1 = 'dd_may';
        }
        elseif(date('m', strtotime($start))=='06'){
            $table1 = 'dd_june';
        }
        elseif(date('m', strtotime($start))=='07'){
            $table1 = 'dd_july';
        }
        elseif(date('m', strtotime($start))=='08'){
            $table1 = 'dd_aug';
        }
        elseif(date('m', strtotime($start))=='09'){
            $table1 = 'dd_sep';
        }
        elseif(date('m', strtotime($start))=='10'){
            $table1 = 'dd_oct';
        }
        elseif(date('m', strtotime($start))=='11'){
            $table1 = 'dd_nov';
        }
        elseif(date('m', strtotime($start))=='12'){
            $table1 = 'dd_dec';
        }
        elseif(date('m', strtotime($start))=='01'){
            $table1 = 'dd_jan';
        }
        elseif(date('m', strtotime($start))=='02'){
            $table1 = 'dd_feb';
        }

        if(date('m', strtotime($end))=='03' ){
            $table2 = 'dd_mar';
        }
        elseif(date('m', strtotime($end))=='04'){
            $table2 = 'dd_apr';
        }
        elseif(date('m', strtotime($end))=='05'){
            $table2 = 'dd_may';
        }
        elseif(date('m', strtotime($end))=='06'){
            $table2 = 'dd_june';
        }
        elseif(date('m', strtotime($end))=='07'){
            $table2 = 'dd_july';
        }
        elseif(date('m', strtotime($end))=='08'){
            $table2 = 'dd_aug';
        }
        elseif(date('m', strtotime($end))=='09'){
            $table2 = 'dd_sep';
        }
        elseif(date('m', strtotime($end))=='10'){
            $table2 = 'dd_oct';
        }
        elseif(date('m', strtotime($end))=='11'){
            $table2 = 'dd_nov';
        }
        elseif(date('m', strtotime($end))=='12'){
            $table2 = 'dd_dec';
        }
        elseif(date('m', strtotime($end))=='01'){
            $table2 = 'dd_jan';
        }
        elseif(date('m', strtotime($end))=='02'){
            $table2 = 'dd_feb';
        }
        $val1 = "F".$val1;
        $val2 = "F".$val2;
        return DB::select("
                select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from $table2 where device_id = ? and time_v between '$start' and '$end'
            UNION
                select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from $table1 where device_id = ? and time_v between '$start' and '$end'
            UNION
                select $val1 as coldata, $val2 as rowdata,time(time_v) as time_current from device_data
                where device_id = ? and time_v between '$start' and '$end'",[$did,$did,$did]);
    }
    
    // For Mileage
    public static function getrowForCronMilage($did,$cron_start,$cron_end){
        return DB::select("select dist,avg_ml as mil,dur,avg_speed,avg_drive_thr as avg_throttle,avg_cool from device_data_s where device_id = '$did' and time_v between '$cron_start' and '$cron_end'");
    }

    // For Standard Devation
    public static function getStandDevatFromDeviceSataByDevId($did,$cron_start,$cron_end,$cond)
    {
        if(date('m', strtotime($cron_start))=='03' ){
            $table1 = 'dd_mar';
        }
        elseif(date('m', strtotime($cron_start))=='04'){
            $table1 = 'dd_apr';
        }
        elseif(date('m', strtotime($cron_start))=='05'){
            $table1 = 'dd_may';
        }
        elseif(date('m', strtotime($cron_start))=='06'){
            $table1 = 'dd_june';
        }
        elseif(date('m', strtotime($cron_start))=='07'){
            $table1 = 'dd_july';
        }
        elseif(date('m', strtotime($cron_start))=='08'){
            $table1 = 'dd_aug';
        }
        elseif(date('m', strtotime($cron_start))=='09'){
            $table1 = 'dd_sep';
        }
        elseif(date('m', strtotime($cron_start))=='10'){
            $table1 = 'dd_oct';
        }
        elseif(date('m', strtotime($cron_start))=='11'){
            $table1 = 'dd_nov';
        }
        elseif(date('m', strtotime($cron_start))=='12'){
            $table1 = 'dd_dec';
        }
        elseif(date('m', strtotime($cron_start))=='01'){
            $table1 = 'dd_jan';
        }
        elseif(date('m', strtotime($cron_start))=='02'){
            $table1 = 'dd_feb';
        }

        if(date('m', strtotime($cron_end))=='03' ){
            $table2 = 'dd_mar';
        }
        elseif(date('m', strtotime($cron_end))=='04'){
            $table2 = 'dd_apr';
        }
        elseif(date('m', strtotime($cron_end))=='05'){
            $table2 = 'dd_may';
        }
        elseif(date('m', strtotime($cron_end))=='06'){
            $table2 = 'dd_june';
        }
        elseif(date('m', strtotime($cron_end))=='07'){
            $table2 = 'dd_july';
        }
        elseif(date('m', strtotime($cron_end))=='08'){
            $table2 = 'dd_aug';
        }
        elseif(date('m', strtotime($cron_end))=='09'){
            $table2 = 'dd_sep';
        }
        elseif(date('m', strtotime($cron_end))=='10'){
            $table2 = 'dd_oct';
        }
        elseif(date('m', strtotime($cron_end))=='11'){
            $table2 = 'dd_nov';
        }
        elseif(date('m', strtotime($cron_end))=='12'){
            $table2 = 'dd_dec';
        }
        elseif(date('m', strtotime($cron_end))=='01'){
            $table2 = 'dd_jan';
        }
        elseif(date('m', strtotime($cron_end))=='02'){
            $table2 = 'dd_feb';
        }
        return DB::select("select F4 as rpm, F5 as vspd, F7 as throt, F87 as Ftorque, F88 as Fpower_hp, F34, F91 as pwr_loss from $table1 where device_id = ? and time_v between '$cron_start' and '$cron_end'$cond
            UNION
                select F4 as rpm, F5 as vspd, F7 as throt, F87 as Ftorque, F88 as Fpower_hp, F34, F91 as pwr_loss from $table2 where device_id = ? and time_v between '$cron_start' and '$cron_end'$cond
            UNION
                select F4 as rpm, F5 as vspd, F7 as throt, F87 as Ftorque, F88 as Fpower_hp, F34, F91 as pwr_loss from device_data
                where device_id = ? and time_v between '$cron_start' and '$cron_end'$cond",[$did,$did,$did]);
    }

    // // For Standard Devation
    // public static function getStandDevatFromDeviceSataByDevId($did,$cron_start,$cron_end,$cond){
    //     return DB::select("select F4 as rpm, F5 as vspd, F7 as throt, F87 as Ftorque, F88 as Fpower_hp, F34, F91 as pwr_loss from device_data where device_id = ? and time_v between '$cron_start' and '$cron_end'$cond",[$did]);
    // }
    
    public static function getStandDevatFromDeviceSataByDevId1($did,$cron_start,$cron_end,$gear)
    {
         $twotable='';
        if(date('m', strtotime($cron_start))==date('m', strtotime($cron_end)))
        {
            $twotable='1';    
             if(date('m', strtotime($cron_start))=='03'){
                $table = 'dd_mar';
            }
            elseif(date('m', strtotime($cron_start))=='04'){
                $table = 'dd_apr';
            }
            elseif(date('m', strtotime($cron_start))=='05'){
                $table = 'dd_may';
            }
            elseif(date('m', strtotime($cron_start))=='06'){
                $table = 'dd_june';
            }
            elseif(date('m', strtotime($cron_start))=='07'){
                $table = 'dd_july';
            }
            elseif(date('m', strtotime($cron_start))=='08'){
                $table = 'dd_aug';
            }
            elseif(date('m', strtotime($cron_start))=='09'){
                $table = 'dd_sep';
            }
            elseif(date('m', strtotime($cron_start))=='10'){
                $table = 'dd_oct';
            }
            elseif(date('m', strtotime($cron_start))=='11'){
                $table = 'dd_nov';
            }
            elseif(date('m', strtotime($cron_start))=='12'){
                $table = 'dd_dec';
            }
            elseif(date('m', strtotime($cron_start))=='01'){
                $table = 'dd_jan';
            }
            elseif(date('m', strtotime($cron_start))=='02'){
                $table = 'dd_feb';
            }
        }else{
            //table1
             if(date('m', strtotime($cron_start))=='03'){
                $table = 'dd_mar';
            }
            elseif(date('m', strtotime($cron_start))=='04'){
                $table = 'dd_apr';
            }
            elseif(date('m', strtotime($cron_start))=='05'){
                $table = 'dd_may';
            }
            elseif(date('m', strtotime($cron_start))=='06'){
                $table = 'dd_june';
            }
            elseif(date('m', strtotime($cron_start))=='07'){
                $table = 'dd_july';
            }
            elseif(date('m', strtotime($cron_start))=='08'){
                $table = 'dd_aug';
            }
            elseif(date('m', strtotime($cron_start))=='09'){
                $table = 'dd_sep';
            }
            elseif(date('m', strtotime($cron_start))=='10'){
                $table = 'dd_oct';
            }
            elseif(date('m', strtotime($cron_start))=='11'){
                $table = 'dd_nov';
            }
            elseif(date('m', strtotime($cron_start))=='12'){
                $table = 'dd_dec';
            }
            elseif(date('m', strtotime($cron_start))=='01'){
                $table = 'dd_jan';
            }
            elseif(date('m', strtotime($cron_start))=='02'){
                $table = 'dd_feb';
            }

            //table2
             if(date('m', strtotime($cron_end))=='03'){
                $table1 = 'dd_mar';
            }
            elseif(date('m', strtotime($cron_end))=='04'){
                $table1 = 'dd_apr';
            }
            elseif(date('m', strtotime($cron_end))=='05'){
                $table1 = 'dd_may';
            }
            elseif(date('m', strtotime($cron_end))=='06'){
                $table1 = 'dd_june';
            }
            elseif(date('m', strtotime($cron_end))=='07'){
                $table1 = 'dd_july';
            }
            elseif(date('m', strtotime($cron_end))=='08'){
                $table1 = 'dd_aug';
            }
            elseif(date('m', strtotime($cron_end))=='09'){
                $table1 = 'dd_sep';
            }
            elseif(date('m', strtotime($cron_end))=='10'){
                $table1 = 'dd_oct';
            }
            elseif(date('m', strtotime($cron_end))=='11'){
                $table1 = 'dd_nov';
            }
            elseif(date('m', strtotime($cron_end))=='12'){
                $table1 = 'dd_dec';
            }
            elseif(date('m', strtotime($cron_end))=='01'){
                $table1 = 'dd_jan';
            }
            elseif(date('m', strtotime($cron_end))=='02'){
                $table1 = 'dd_feb';
            }
        }
        if($twotable=='1'){ 
           $result = DB::select("select IFNULL(avg(NULLIF(F4, 0)), 0) as avg_rpm from $table where device_id = ? and time_v between '$cron_start' and '$cron_end' and F81 = ?
            UNION
            select IFNULL(avg(NULLIF(F4, 0)), 0) as avg_rpm from device_data
             where device_id = ? and time_v between '$cron_start' and '$cron_end' and F81 = ?",[$did,$gear,$did,$gear]);
        }else{
             $result = DB::select("select IFNULL(avg(NULLIF(F4, 0)), 0) as avg_rpm from $table where device_id = ? and time_v between '$cron_start' and '$cron_end' and F81 = ?
            UNION
            select IFNULL(avg(NULLIF(F4, 0)), 0) as avg_rpm from $table1 where device_id = ? and time_v between '$cron_start' and '$cron_end' and F81 = ?
            UNION
            select IFNULL(avg(NULLIF(F4, 0)), 0) as avg_rpm from device_data
             where device_id = ? and time_v between '$cron_start' and '$cron_end' and F81 = ?",[$did,$gear,$did,$gear,$did,$gear]);
        }
        
        if(array_key_exists(0, $result)){
               return $result[0]->avg_rpm;
         }else
         {
            return 0;
         }
    }

    // public static function getStandDevatFromDeviceSataByDevId1($did,$cron_start,$cron_end,$gear){
    //     return DB::select("select avg(F4) as avg_rpm from device_data where device_id = ? and time_v between '$cron_start' and '$cron_end' and F81 = ?",[$did,$gear]);
    // }

    //Milage avg
    public static function getStatsAverageMilage($deviceID){
        return DB::select("select avg((avg_ml*1)) as val from device_drive where device_id = ? and (dist*1) <> 0 and (avg_ml*1) <> 0",[$deviceID]);
    }

    public static function getStatsTotalNumDaysDriven($deviceID){
        return DB::select("select count(distinct dt) as dt from device_drive where device_id = ? and dist <> '0' ",[$deviceID]);
    }

    public static function getStatsTotalDistanceDriven($deviceID){
        return DB::select("select sum((dist*1)) as val from device_drive where device_id = ? and (dist*1) <> 0",[$deviceID]);
        // return DB::select("select sum((dist*1)) as val from device_drive where device_id = ? and (dist*1) <> 0 and (avg_ml*1) <> 0 ",[$deviceID]);
    }

    // Row Data
    public static function getRawData($deviceID){
        return DB::select("select * from device_data_s where device_id = ? order by id desc limit 0,1 ",[$deviceID]);
    }

    // Crank Value greater than 3 count
    public static function getCranksGreaterThanThree($did,$cron_start,$cron_end){
        return DB::select("select max_crnk_cnt from device_data_s where device_id = '$did' and time_v between '$cron_start' and '$cron_end'");
    }

    public static function getHeatMapData($table,$did,$cron_start,$cron_end,$sqlData){
        return DB::select("select $sqlData from $table where device_id = ? and ad_dt between '$cron_start' and '$cron_end' limit 0,1",[$did]);
    }

    public static function getGearChangeHeatMapColumns(){
        return DB::select("SHOW COLUMNS FROM device_stats_gear LIKE '%cnt_gr%'");
    }

    public static function getSystemHealth($did,$cron_start,$cron_end){
        return DB::select("select max(bat_st) as bat_c,max(coolp) as coolp,max(eng_block_ej) as eng_block_ej,max(idlbc_v) as idlbc_v,max(peakb_v) as peakb_v from device_monitor where device_id = ? and ad_dt between '$cron_start' and '$cron_end' limit 0,1",[$did]);
    }

    public static function getSystemHealth1($did,$cron_start,$cron_end){
        return DB::select("select oillifep from device_drive where device_id = ? and dt between '$cron_start' and '$cron_end' order by id desc limit 0,1",[$did]);
    }

    public static function getGearChangeHealth($did,$cron_start,$cron_end){
        return DB::select("select sum(gr_shft_cnt) as gear_shift, sum(half_clth_dur) as half_clutch, max(false_neutral_cnt) as netural from device_stats_gear where device_id = ? and ad_dt between '$cron_start' and '$cron_end' limit 0,1",[$did]);
    }

    public static function getStandDevatFromDeviceSataByDevIdCount($did,$cron_start,$cron_end,$cond)
    {
        if(date('m', strtotime($cron_start))=='03' ){
            $table1 = 'dd_mar';
        }
        elseif(date('m', strtotime($cron_start))=='04'){
            $table1 = 'dd_apr';
        }
        elseif(date('m', strtotime($cron_start))=='05'){
            $table1 = 'dd_may';
        }
        elseif(date('m', strtotime($cron_start))=='06'){
            $table1 = 'dd_june';
        }
        elseif(date('m', strtotime($cron_start))=='07'){
            $table1 = 'dd_july';
        }
        elseif(date('m', strtotime($cron_start))=='08'){
            $table1 = 'dd_aug';
        }
        elseif(date('m', strtotime($cron_start))=='09'){
            $table1 = 'dd_sep';
        }
        elseif(date('m', strtotime($cron_start))=='10'){
            $table1 = 'dd_oct';
        }
        elseif(date('m', strtotime($cron_start))=='11'){
            $table1 = 'dd_nov';
        }
        elseif(date('m', strtotime($cron_start))=='12'){
            $table1 = 'dd_dec';
        }
        elseif(date('m', strtotime($cron_start))=='01'){
            $table1 = 'dd_jan';
        }
        elseif(date('m', strtotime($cron_start))=='02'){
            $table1 = 'dd_feb';
        }



        if(date('m', strtotime($cron_end))=='03' ){
            $table2 = 'dd_mar';
        }
        elseif(date('m', strtotime($cron_end))=='04'){
            $table2 = 'dd_apr';
        }
        elseif(date('m', strtotime($cron_end))=='05'){
            $table2 = 'dd_may';
        }
        elseif(date('m', strtotime($cron_end))=='06'){
            $table2 = 'dd_june';
        }
        elseif(date('m', strtotime($cron_end))=='07'){
            $table2 = 'dd_july';
        }
        elseif(date('m', strtotime($cron_end))=='08'){
            $table2 = 'dd_aug';
        }
        elseif(date('m', strtotime($cron_end))=='09'){
            $table2 = 'dd_sep';
        }
        elseif(date('m', strtotime($cron_end))=='10'){
            $table2 = 'dd_oct';
        }
        elseif(date('m', strtotime($cron_end))=='11'){
            $table2 = 'dd_nov';
        }
        elseif(date('m', strtotime($cron_end))=='12'){
            $table2 = 'dd_dec';
        }
        elseif(date('m', strtotime($cron_end))=='01'){
            $table2 = 'dd_jan';
        }
        elseif(date('m', strtotime($cron_end))=='02'){
            $table2 = 'dd_feb';
        }
        return DB::select("select count(*) as count from $table1  where device_id = ? and time_v between '$cron_start' and '$cron_end'$cond
            UNION
                select count(*) as count from $table2  where device_id = ? and time_v between '$cron_start' and '$cron_end'$cond
            UNION
                select count(*) as count from device_data
                where device_id = ? and time_v between '$cron_start' and '$cron_end'$cond",[$did,$did,$did]);
    }

    // public static function getStandDevatFromDeviceSataByDevIdCount($did,$cron_start,$cron_end,$cond){
    //     return DB::select("select count(*) as count from device_data where device_id = ? and time_v between '$cron_start' and '$cron_end'$cond",[$did]);
    // }

    // Dashboard
 
    public static function getTotalTest($uid,$utype)
    {
        // echo $utype; die;
            // return DB::select("select count(*) as count from summary",[3]);
            if($utype == 1 || $utype == 2 ){
                return DB::select("select count(*) as count from summary",['1']);
            }
            elseif($utype == 3)
            {
                // echo "I m here";die;
                return DB::select("select count(*) as count from summary a, device b where b.status = ? and b.partner='$uid' and a.device_id = b.device_id",['1']);
            }else{
                return DB::select("select count(*) as count from summary a, device b, users c where b.status = ? and c.uid='$uid' and a.device_id = b.device_id and b.id = c.device",['1']);
            }
    }

    public static function getPassedTest($uid,$utype){
        // return DB::select("select count(*) as count from summary where test_result = 1 ",[1]);

        if($utype == 1 || $utype == 2 )
        {
            return DB::select("select count(*) as count from summary where test_result = 1 ",['1']);
        }
        elseif($utype == 3)
        {
            // echo "I m here";die;
            return DB::select("select count(*) as count from summary a, device b where b.status = ? and b.partner='$uid' and a.device_id = b.device_id and a.test_result = 1",['1']);
        }else{
            return DB::select("select count(*) as count from summary a, device b, users c where b.status = ? and c.uid='$uid' and a.device_id = b.device_id and b.id = c.device and a.test_result = 1",['1']);
        }
    }
    
    public static function getFailedTest($uid,$utype){
        // return DB::select("select count(*) as count from summary where test_result = 0 ",[1]);
        if($utype == 1 || $utype == 2 )
        {
            return DB::select("select count(*) as count from summary where test_result = 0 ",['1']);
        }
        elseif($utype == 3)
        {
            // echo "I m here";die;
            return DB::select("select count(*) as count from summary a, device b where b.status = ? and b.partner='$uid' and a.device_id = b.device_id and a.test_result = 0",['1']);
        }else{
            return DB::select("select count(*) as count from summary a, device b, users c where b.status = ? and c.uid='$uid' and a.device_id = b.device_id and b.id = c.device and a.test_result = 0",['1']);
        }
    }

    public static function getTestRepostById($id){
        return DB::select("select * from summary where id = ? ",[$id]);
    }

    public static function getFailedList($uid,$utype){
        // return DB::select("select * from summary a,device_drive b where a.test_result = ? and a.device_id = b.device_id and a.vin_no = b.vin_no ",[0]);
        if($utype == 1 || $utype == 2 )
        {
            return DB::select("select * from summary where test_result = '0' order by id desc");
        }
        elseif($utype == 3)
        {
            // echo "I m here";die;
            return DB::select("select * from summary a,device_drive b ,device d where a.test_result = ? and a.device_id = b.device_id and b.device_id = d.device_id and d.partner = '$uid'  and a.vin_no = b.vin_no order by a.id desc",[0]);
            // return DB::select("select count(*) as count from summary a, device b where b.status = ? and b.partner='$uid' and a.device_id = b.device_id and a.test_result = 0",['1']);
        }else{
            return DB::select("select * from summary a,device_drive b , device d, users c  where a.test_result = ? and a.device_id = b.device_id and b.device_id = d.device_id and d.id = c.device and c.uid= '$uid'  and a.vin_no = b.vin_no order by a.id desc",[0]);
            // return DB::select("select count(*) as count from summary a, device b, users c where b.status = ? and c.uid='$uid' and a.device_id = b.device_id and b.id = c.device and a.test_result = 0",['1']);
        }
    }
    public static function getIncompleateTest($uid,$utype){
        // return DB::select("select * from summary a,device_drive b where a.test_status = ? and a.device_id = b.device_id and a.vin_no = b.vin_no ",[0]);
        if($utype == 1 || $utype == 2 )
        {
            // return DB::select("select * from summary a,device_drive b where a.test_status = ? and a.device_id = b.device_id and a.vin_no = b.vin_no order by a.id desc",[0]);
            return DB::select("select * from summary  where test_status = '0' order by id desc");
        }
        elseif($utype == 3)
        {
            // echo "I m here";die;
            return DB::select("select * from summary a,device_drive b ,device d where a.test_status = ? and a.device_id = b.device_id and b.device_id = d.device_id and d.partner = '$uid'  and a.vin_no = b.vin_no order by a.id desc",[0]);
            // return DB::select("select count(*) as count from summary a, device b where b.status = ? and b.partner='$uid' and a.device_id = b.device_id and a.test_result = 0",['1']);
        }else{
            return DB::select("select * from summary a,device_drive b , device d, users c  where a.test_status = ? and a.device_id = b.device_id and b.device_id = d.device_id and d.id = c.device and c.uid= '$uid'  and a.vin_no = b.vin_no order by a.id desc",[0]);
            // return DB::select("select count(*) as count from summary a, device b, users c where b.status = ? and c.uid='$uid' and a.device_id = b.device_id and b.id = c.device and a.test_result = 0",['1']);
        }
    }

    // public static function getDeviceOnlineCount(){
    //     return DB::select("select count(distinct device_id) as count from device_data where ad_dt >= DATE_SUB( NOW( ) , INTERVAL 1 HOUR) and ad_dt <= DATE_ADD( NOW( ) , INTERVAL 1 HOUR)");
    // }

    public static function getDeviceInActiveCount(){
        return DB::select("select count(*) as count from device where status = ?",[1]);
        //return DB::select("select count(distinct device_id) as count from device_data where ad_dt <= DATE_SUB( NOW( ) , INTERVAL 5 HOUR )");
    }

    // Graph data 1

    public static function getGraphData1($year){

        $utype=session('userdata')['ucategory'];
        $uid=session('userdata')['uid'];
        // echo $utype."-".$uid;die;
       if($utype == 1 || $utype == 2)
        {
            return DB::select("SELECT ROUND(sum(dist),2) as Total,MONTH(dt) as month FROM `device_drive` where YEAR(dt)= ?  GROUP by MONTH(dt)",[$year]);
        }
        elseif($utype == 3)
        {
             return DB::select("SELECT ROUND(sum(dist),2) as Total,MONTH(dt) as month FROM `device_drive` a,device b where YEAR(dt)= ? and b.device_id = a.device_id and b.partner='$uid' GROUP by MONTH(dt)",[$year]);
        }else
        {
             // return DB::select("SELECT sum(dist) as Total,MONTH(dt) as month FROM `device_drive` where YEAR(dt)= ?  GROUP by MONTH(dt)",[$year]);
        }

    }

    public static function getGraphData2($year,$month){
        $utype=session('userdata')['ucategory'];
        $uid=session('userdata')['uid'];
        // echo $uid;die;
       if($utype == 1 || $utype == 2)
        {
            return DB::select("SELECT ROUND(sum(dist),2) as dist,DAY(dt) as day FROM `device_drive` where YEAR(dt)= ? and MONTH(dt)= ?  GROUP by DAY(dt)",[$year,$month]);
        }
         elseif($utype == 3)
        {
             return DB::select("SELECT ROUND(sum(dist),2) as dist,DAY(dt) as day FROM `device_drive`  a,device b where YEAR(dt)= ? and MONTH(dt)= ? and b.device_id = a.device_id and b.partner='$uid'  GROUP by DAY(dt)",[$year,$month]);
        }else
        {
             // return DB::select("SELECT sum(dist) as Total,MONTH(dt) as month FROM `device_drive` where YEAR(dt)= ?  GROUP by MONTH(dt)",[$year]);
        }
    }

    public static function getLastDriveNum($deviceID){
        $result = DB::select("select * from device_drive where device_id = '$deviceID' and (dur*1) > 0 order by id desc limit 0,1");
        return $result;
    }

    // Drive details
    public static function getStatsByDriveRange($deviceID,$driveStart,$driveEnd){
        return DB::select("select *, count(*) as count from device_drive where device_id='$deviceID' and (dur*1)>0 and drive between $driveStart and $driveEnd order by id desc, ad_dt desc");
    }

    public static function getStatsByDriveRange_new($deviceID){
        return DB::select("select *, count(*) as count from device_drive where device_id='$deviceID'  order by id desc, ad_dt desc");
    }

    public static function getStatsByDriveRange_data($deviceID,$driveStart,$driveEnd){
        return DB::select("select * from device_drive where device_id='$deviceID' and (dur*1)>0 and drive between $driveStart and $driveEnd order by id desc, ad_dt desc");
    }

    public static function getStatsByDriveRange_data_new($deviceID){
        return DB::select("select * from device_drive where device_id='$deviceID' order by id desc, ad_dt desc");
    }
    
    public static function Update_device_desc($uname,$devID){
       return DB::UPDATE("update `device` set `descrp` = ? where `device_id` = ?",[$uname,$devID]);
    }

    public static function update_device($partner1,$devtype1,$status1,$devid){
       return DB::insert("update device set partner=?,devtype=?,status=? where device_id = ?",[$partner1,$devtype1,$status1,$devid]);
    }

    public static function getStatsByNumOfDrive($deviceID,$driveNo){
        return DB::select("select * from device_drive where device_id='$deviceID' order by id desc, ad_dt desc limit 0,$driveNo");
    }
    
    public static function locationLastApi($deviceID){
        return DB::select("select * from event where device_id = '$deviceID' and lat <> '' and lon <> '' order by time_s desc limit 0,1");
    }

    public static function getLastLocation($deviceID,$drivenum)
    {
        $dat   = DB::select("select dt as ddate from device_drive where device_id = ? and drive = ? ",[$deviceID,$drivenum]);

        if(date('m', strtotime($dat[0]->ddate))=='03' ){
            $table1 = 'dd_mar';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='04'){
            $table1 = 'dd_apr';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='05'){
            $table1 = 'dd_may';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='06'){
            $table1 = 'dd_june';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='07'){
            $table1 = 'dd_july';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='08'){
            $table1 = 'dd_aug';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='09'){
            $table1 = 'dd_sep';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='10'){
            $table1 = 'dd_oct';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='11'){
            $table1 = 'dd_nov';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='12'){
            $table1 = 'dd_dec';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='01'){
            $table1 = 'dd_jan';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='02'){
            $table1 = 'dd_feb';
        }

        $data  = DB::select("
                select * from $table1 where device_id = ? and lat <> '' and lon <> '' and Fsprint = ? 
            UNION
                select * from device_data
                where device_id = ? and lat <> '' and lon <> '' and Fsprint = ? order by time_s desc limit 0,1",[$deviceID,$drivenum,$deviceID,$drivenum]);
        
        return $data;
        // return DB::select("select * from event where device_id = '$deviceID' and lat <> '' and lon <> '' order by time_s desc limit 0,1");
    }

    public static function updateActCode($id,$mac_id,$used_by,$status){
       return DB::update("update activation_code set mac_id = ? , used_by = ?, status = ? where id= ?",[$mac_id,$used_by,$status,$id]);
    }
    
    /*public static function updateDataMultiple($id,$mac_id,$used_by,$status){
       return DB::insert("UPDATE activation_code SET mac_id = ? , used_by = ?, status=?
        WHERE id= ?;",[$mac_id,$used_by,$status,$id]);
    }*/

    public static function updateTableData($table,$devID,$calib){
        return DB::update("update $table set calib = ? where device = ?",[$calib,$devID]);
    }

    public static function updateDataMultiple($table,$setField,$setVal,$postVal) // with value 
    {
        $tablefields = Fetch::getTableFields($table);
        $fields      = implode(',',$tablefields);
        $value       = "";

        $lastVal = count($tablefields) - 1;
        $val     = $tablefields;
        for($i = 1;$i < count($tablefields); $i++)
        {
            if(isset($postVal[$val[$i]]))
            {
                $value.= ($value!="") ? "," : "";
                $value.= $val[$i]."='".$postVal[$val[$i]]."'";
            }
        }
        $where = "";
        for($i = 0;$i < count($setField); $i++)
        {
            $where.= " and ".$setField[$i] . "='".$setVal[$i]."'";
        }
        $queryStr = "update " . $table . " set $value where 1=1 $where";
        //If there are empty strings in the query, INSERT will fail. Find and replace empty string with 0
        $queryStrCorrected = str_replace("''", "'0'", $queryStr);
        // echo "$queryStrCorrected";die;
        $vehUpdate = DB::update($queryStrCorrected);
        if($vehUpdate)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function insertDbSingle($table,$postVal)
    { 
        $tablefields = Fetch::getTableFields($table);
        unset($tablefields[0]);

        $tablefields = array_values($tablefields);
        $fields      = implode(',',$tablefields);
        $value       = "";
        $lastVal     = count($tablefields)-1;
        $val         = $tablefields;
        for($i=0; $i<count($tablefields); $i++)
        {
            if(isset($postVal[$val[$i]]))
            {
                $value.= "'".$postVal[$val[$i]]."'";
            }
            elseif(!isset($postVal[$val[$i]]))
            {
                $value.= "''";
            }
            if($i < $lastVal)
            {
                $value.= ",";
            } 
        }

        $queryStr = "insert into " . $table . "($fields) values($value)";
        $queryStrCorrected = str_replace("''", "'0'", $queryStr);
        // print_r($queryStrCorrected);die;
        $insert = DB::insert($queryStrCorrected);
        return $insert;
    }

    public static function isActivationCodeValid($acode){
        return DB::select("select * from activation_code where activation_code = ? ",[$acode]);
    }

    public static function getGPSDevDataTableStart($deviceID,$drivenum)
    {
        return DB::select("select lat,lon,alt from device_data where device_id = ? and lat <> '' and lon <> '' and Fsprint = ? order by time_s asc limit 0,1",[$deviceID,$drivenum]);
    }

    public static function getGPSDevDataTableEnd($deviceID,$drivenum)
    {
        return DB::select("select lat,lon,alt from device_data where device_id = ? and lat <> '' and lon <> '' and Fsprint = ? order by time_s desc limit 0,1",[$deviceID,$drivenum]);
    }
    public static function devDriveRangeDataApi_sigfox($deviceID,$driverangefrm,$driverangeto)
    {
        $dat  = DB::select("select ad_dt as ddate from event where device_id = ? and nodrive= ? LIMIT 1",[$deviceID,$driverangefrm]);
        // print_r($datefordd[0]->ad_dt);
        if(date('m', strtotime($dat[0]->ddate))=='03' ){
            $table1 = 'dd_mar';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='04'){
            $table1 = 'dd_apr';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='05'){
            $table1 = 'dd_may';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='06'){
            $table1 = 'dd_june';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='07'){
            $table1 = 'dd_july';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='08'){
            $table1 = 'dd_aug';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='09'){
            $table1 = 'dd_sep';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='10'){
            $table1 = 'dd_oct';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='11'){
            $table1 = 'dd_nov';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='12'){
            $table1 = 'dd_dec';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='01'){
            $table1 = 'dd_jan';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='02'){
            $table1 = 'dd_feb';
        }
        // echo $table1;die;
        $dat   = DB::select("select DISTINCT time_s,device_id,lat,lon,alt,nodrive from event where device_id = ? and lat <> '' and lon <> '' and nodrive >= ? and nodrive <= ? UNION select DISTINCT time_s,device_id,lat,lon,alt,Fsprint as nodrive from $table1 where device_id = ? and Fsprint>=? and Fsprint<=? and lat <> '' and lon <> '' order by nodrive desc,time_s asc",[$deviceID,$driverangefrm,$driverangeto,$deviceID,$driverangefrm,$driverangeto]);
        $array = array();
        $aI    = 0;
        foreach($dat as $val)
        {
            $array[$aI] = array('ts' => $val->time_s, 'lat' => $val->lat, 'lang' => $val->lon , 'driveid' => $val->nodrive );
            $aI++;
        }
        return $array;
    }
    public static function devDriveNumDataApi_sigfox($deviceID,$drivenum)
    {
        $dat  = DB::select("select ad_dt as ddate from event where device_id = ? and nodrive= ? LIMIT 1",[$deviceID,$drivenum]);
        // print_r($datefordd[0]->ad_dt);
        if(date('m', strtotime($dat[0]->ddate))=='03' ){
            $table1 = 'dd_mar';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='04'){
            $table1 = 'dd_apr';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='05'){
            $table1 = 'dd_may';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='06'){
            $table1 = 'dd_june';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='07'){
            $table1 = 'dd_july';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='08'){
            $table1 = 'dd_aug';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='09'){
            $table1 = 'dd_sep';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='10'){
            $table1 = 'dd_oct';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='11'){
            $table1 = 'dd_nov';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='12'){
            $table1 = 'dd_dec';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='01'){
            $table1 = 'dd_jan';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='02'){
            $table1 = 'dd_feb';
        }
        $date  = DB::select("select DISTINCT time_s,device_id,lat,lon,alt,nodrive from event where device_id = ? and lat <> '' and lon <> '' and nodrive = ? UNION select DISTINCT time_s,device_id,lat,lon,alt,Fsprint as nodrive from $table1 where device_id = ? and lat <> '' and lon <> '' and Fsprint = ? order by time_s asc",[$deviceID,$drivenum,$deviceID,$drivenum]);
        $array = array();
        $aI    = 0;
        foreach($date as $val)
        {
            $array[$aI] = array('ts' => $val->time_s, 'lat' => $val->lat, 'lang' => $val->lon , 'alt' => $val->alt, 'driveid' => $val->nodrive );
            $aI++;
        }
        return $array;
    }
    public static function devDriveNumDataApi($deviceID,$drivenum)
    {
        $date  = DB::select("select DISTINCT time_s,device_id,lat,lon,alt,event,nodrive from event where device_id = ? and lat <> '' and lon <> '' and nodrive = ? order by time_s asc",[$deviceID,$drivenum]);
        $array = array();
        $aI    = 0;
        foreach($date as $val)
        {
            $array[$aI] = array('ts' => $val->time_s, 'lat' => $val->lat, 'lang' => $val->lon , 'alt' => $val->alt, 'driveid' => $val->nodrive );
            $aI++;
        }
        return $array;
    }

    public static function devDriveRangeDataApi($deviceID,$driverangefrm,$driverangeto)
    {
        $dat   = DB::select("select * from event where device_id = ? and lat <> '' and lon <> '' and nodrive >= ? and nodrive <= ? order by nodrive desc,time_s asc",[$deviceID,$driverangefrm,$driverangeto]);
        $array = array();
        $aI    = 0;
        foreach($dat as $val)
        {
            $array[$aI] = array('ts' => $val->time_s, 'lat' => $val->lat, 'lang' => $val->lon , 'driveid' => $val->nodrive );
            $aI++;
        }
        return $array;
    }

    public static function devDriveRangeDataApiCaiLite($deviceID,$driverangefrm,$driverangeto)
    {
         $dat   = DB::select("select * from event where device_id = ? and lat <> '' and lon <> '' and nodrive >= ? and nodrive <= ? order by nodrive desc,time_s asc",[$deviceID,$driverangefrm,$driverangeto]);
        $array = array();
        $aI    = 0;
        foreach($dat as $val)
        {
            if(($val->lat) != 0)
            {
                $array[$aI] = array('ts' => $val->time_s, 'lat' => $val->lat, 'lang' => $val->lon , 'driveid' => $val->nodrive );
                $aI++;
            }
        }
        return $array;
    }

    public static function updateDbSingle_veh($postVal)
    {
        $columns = array_keys($postVal);
        $values  = array_values($postVal);
        return DB::update("update vehicles (" . implode(", ", $columns) . ") VALUES ('" . implode("', '", $values) . "')");
    }

    public static function insertDbSingle_event($postVal)
    {
        $columns = array_keys($postVal);
        $values  = array_values($postVal);
        return DB::insert("INSERT INTO event (" . implode(", ", $columns) . ") VALUES ('" . implode("', '", $values) . "')");
    }

    public static function getLatLongLastDrive($deviceID,$drivenum)
    {
        $dat   = DB::select("select dt as ddate from device_drive where device_id = ? and  nodrive = ? ",[$deviceID,$drivenum]);

         if(date('m', strtotime($dat[0]->ddate))=='03' ){
            $table1 = 'dd_mar';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='04'){
            $table1 = 'dd_apr';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='05'){
            $table1 = 'dd_may';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='06'){
            $table1 = 'dd_june';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='07'){
            $table1 = 'dd_july';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='08'){
            $table1 = 'dd_aug';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='09'){
            $table1 = 'dd_sep';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='10'){
            $table1 = 'dd_oct';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='11'){
            $table1 = 'dd_nov';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='12'){
            $table1 = 'dd_dec';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='01'){
            $table1 = 'dd_jan';
        }
        elseif(date('m', strtotime($dat[0]->ddate))=='02'){
            $table1 = 'dd_feb';
        }

        $data  = DB::select("
                select * from $table1 where device_id = ? and lat <> '' and lon <> '' and Fsprint = ? 
            UNION
                select * from device_data
                where device_id = ? and lat <> '' and lon <> '' and Fsprint = ? order by time_s desc",[$deviceID,$drivenum,$deviceID,$drivenum]);
        
        $array = array();
        $lat   = "";
        $lon   = "";
        $alt   = "";
        $tm    = "";
        foreach($data as $val)
        {
            $lat.= "," . $val->lat;
            $lon.= "," . $val->lon;
            $alt.= "," . $val->alt; 
            $tm.= "," . $val->time_s;
        }
        $lat   = substr($lat, 1);
        $lon   = substr($lon, 1);
        $alt   = substr($alt, 1);
        $tm    = substr($tm, 1);
        $array = array('lat' => $lat, 'lon' => $lon, 'alt' => $alt, 'ts' => $tm);
        return $array;
    }

    public static function setDeviceLastLocation($lat,$lon,$alt,$tm,$did){
       return DB::update("update device set latitude= ?,longitude=?,alt=?,time_stamp=? where device_id=?",[$lat,$lon,$alt,$tm,$did]);
    }

    public static function update_VehServOil($servdt,$servkm,$id){
        return DB::update("update vehicles set oil_dt= ? ,oil_dist = ?,last_serv_dt= ? ,travel_service= ? where device = ? ",[$servdt,$servkm,$servdt,$servkm,$id]);
    }

    public static function update_VehServ($servdt,$servkm,$id){
        return DB::update("update `vehicles` set `last_serv_dt`='$servdt',`travel_service`='$servkm' WHERE `id` = '$id'");
    }

    public static function insert_service($servdt,$servkm,$oil,$deviceID,$did,$ad_dt){
        return DB::insert("insert into `service` (`id`, `type`, `provider_name`, `sdate`, `kms_read`, `descrp`, `descrp_sub`, `details`, `devid`, `did`, `status`, `ad_by`, `ad_dt`, `spec`) VALUES (NULL, '3', 'Self', '$servdt', '$servkm', '$oil', '', '', '$deviceID', '$did', '1', '29', '$ad_dt', '')");
    }

    public static function insert_service_details($servdt,$servkm,$oil,$deviceID,$did,$ad_dt,$srv_det){
        return DB::insert("insert into `service` (`id`, `type`, `provider_name`, `sdate`, `kms_read`, `descrp`, `descrp_sub`, `details`, `devid`, `did`, `status`, `ad_by`, `ad_dt`, `spec`) VALUES (NULL, '3', 'Self', '$servdt', '$servkm', '$oil', '', '$srv_det', '$deviceID', '$did', '1', '29', '$ad_dt', '')");
    }

    public static function getbt1data($devID){
        // echo "select * from bt_dev where device_id = '$devID' and bt_dev_make = 'Numato' order by id desc";die;
        return DB::select("select * from bt_dev where device_id = ? and bt_dev_make = 'Numato' order by id desc",[$devID]);
    }

    public static function getDashboardData($year,$month)
    {
        $result = DB::select("select * from dashboard where year = ? and month = ?",[$year,$month]);
        if(!empty($result)) 
        {
            $dashData = $result[0];
        }
        else
        {
            $dashData = 0;
        }
        return $dashData;
    }

    public static function updateDeviceTable($deviceid,$dist_currval,$dist_drvnum,$total_dist){
        return DB::update("update device set dist_currval = ?,dist_drvnum = ?,total_dist = ? where device_id = ?",[$dist_currval,$dist_drvnum,$total_dist,$deviceid]);
    }

    public static function updateDashboard($type,$year,$month,$dDate,$dailyDist)
    {
        if($type == 0)
        {
            return DB::insert("insert into dashboard (year, month,$dDate) values ('$year', '$month', '$dailyDist')");
        }
        else
        {
           return DB::update("update dashboard set $dDate = ? where year = ? and month = ?",[$dailyDist,$year,$month]);
        }
    }

    public static function checkDriveNumForDeviceID($deviceid,$driveNum,$table)
    {
        return DB::select("select * from $table where device_id = ? and nodrive = ?",[$deviceid,$driveNum]);
        // return $retValue;
    }

    public static function getLastMonitorResult($deviceID)
    {
        return DB::select("select * from device_monitor where device_id = ? and comp_pres != '' order by id desc limit 0,1",[$deviceID]);
        // return $monLastResultArr;
    }

    public static function generateAlertMailAdvMon($did,$driveID,$cat,$subcat)
    {
        //Check if alert is already generated for this drive
        $alertAvail = DB::selct("select * from alert where devid = ? and drive_no = ? and ftype = ? and satype = ?",[$did,$driveID,$cat,$subcat]);
        
        if($alertAvail->num_rows == 0)
        {
            $data         = getAlertMailDetails($cat,$subcat);
            $alertDesc    = $data['description'];
            $alertMsg     = $data['msg'];
            $alert_cat    = $data['alert_category_id'];
            $alert_type   = $data['alert_type'];
            $data         = getUserDeviceEmail($did);
            $email        = $data['uemail'];
            $to           = "sarvesh.adyanthaya@enginecal.com";
            $subject      = "Alert Engine Mail";
            $data['msg']  = $alertMsg;
            
            $message      = getTemplate(1,$data);
            
            // Always set content-type when sending HTML email
            $headers  = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            
            // More headers
            $headers .= 'From: <support@enginecal.com>' . "\r\n";
            $headers .= 'Cc: support@enginecal.com' . "\r\n";
            
            mail($to,$subject,$message,$headers);
            $postVal   = array(
            'devid'    => $did,
            'ftype'    => $alert_cat,
            'atype'    => $alert_type,
            'satype'   => $subcat,
            'msg'      => $alertMsg,
            'drive_no' => $driveID,
            'status'   => '1',
            'dt'       => date('Y-m-d H:i:s'),
            'exp'      => date('Y-m-d')
            );
            $id        = insertDbSingle('alert',$postVal);
            return 1;
        }
        else
        {
            //Don't do anything as mail has already been generated.
            return 0;
        }
    }

    // get User Device Email
    public static function getUserDeviceEmail($did)
    {
        $getid = Fetch::getIdByDeviceId($did);
        $id    = $getid[0]->id;
        return DB::select("select uemail,uname from users where device = '$id'");
       
    }

    // Get details of alert by category and sub category.
    public static function getAlertMailDetails($cat,$subcat)
    {
        $alert = DB::select("select 'desc' as description,msg,to_admin,to_user,to_approval,alert_category_id,alert_type from alert_mails where alert_category_id = ? and alert_sub_category_id = ?",[$cat,$subcat]);
        return $alert->fetch_assoc();
    }

/*    public static function getGraphData1($year){
        return DB::select("SELECT year, SUM( d1 + d2 + d3 + d4 + d5 + d6 + d7 + d8 + d9 + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22 + d23 + d24 + d25 + d26 + d27 + d28 + d29 + d30 + d31 ) AS Total FROM  `dashboard` WHERE year =  ? GROUP BY month",[$year]);
    }

    public static function getGraphData2($year,$month){
        return DB::select("SELECT year,month,d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16,d17,d18,d19,d20,d21,d22,d23,d24,d25,d26,d27,d28,d29,d30,d31 FROM  `dashboard` WHERE year =  ? and month = ?",[$year,$month]);
    }*/

    // Get All the vehicle 
    public static function getAllVehicle()
    {
        return DB::select("SELECT * from vehspec order by id ASC");
    }

    // Get ALL reports
    public static function getRepots($uid,$utype)
    {
        if($utype == 1 || $utype == 2)
        {
            return DB::select("SELECT * FROM `device_drive` a, summary b where a.vin_no !='' and a.vin_no=b.vin_no and a.drive = b.driveno  ORDER BY a.id DESC");
        }
        else if($utype == 3)
        {
            return DB::select("SELECT * FROM `device_drive` a, summary b,device d where a.vin_no !='' and a.vin_no=b.vin_no and a.drive = b.driveno and a.device_id = d.device_id and d.partner = '$uid'   ORDER BY a.id DESC");
        }
        else
        {
            return DB::select("SELECT * FROM `device_drive` a, summary b,device d , users u where a.vin_no !='' and a.vin_no=b.vin_no and a.drive = b.driveno and b.device_id = a.device_id and a.device_id = d.device_id and d.id = u.device and u.uid = '$uid'   ORDER BY a.id DESC");
        }

    }

    public static function getTestResultByVin($date,$did,$vehicle,$vin_no)
    {
        return DB::select("SELECT * FROM `device_drive` a, summary b where a.vin_no=b.vin_no and a.drive = b.driveno and a.dt = '$date' and a.vin_no ='$vin_no' and a.device_id = '$did' and a.vehicle = '$vehicle'");
    }

    public static function getAllOperator()
    {
        return DB::select("SELECT * FROM operator");
    }
    public static function getAllOperatorActive()
    {
        return DB::select("SELECT * FROM operator where status = 1");
    }
    public static function getOperatorById($id)
    {
        return DB::select("SELECT * FROM operator where id = $id");
    }
    
    public static function getReportsByDate($start,$end,$vehcile,$rider,$result,$uid,$utype)
    {
        $startdt = Date('Y-m-d',strtotime($start));
        $enddt   = Date('Y-m-d',strtotime($end));
        $vehicleQ = "";
        $riderQ   = "";
        $resultQ  = "";
        $date     = "";
        if(!empty($vehcile))
        {
            $vehicleQ = " and a.vehicle = '$vehcile'";
        }
        if(!empty($rider))
        {
            $device = DB::select("SELECT device_id FROM `device` d, users u where d.id = u.device and u.uid = '$rider'");
            // print_r($device);die;
            $d = $device[0]->device_id;
            $riderQ   = " and b.device_id = '$d'";
            // echo $riderQ;die;
        }
        if(!empty($result))
        {
            if($result == 2)
            {
                $resultQ = " and b.test_result = '0'";
            }
            else
            {
                $resultQ = " and b.test_result = '1'";
            }
        }
       if(!empty($start))
       {
            $date = "and a.dt >= '$startdt' and a.dt <='$enddt'";
       }
        // return DB::select("SELECT * FROM `device_drive` a, summary b where a.vin_no !='' and a.vin_no=b.vin_no and a.drive = b.driveno $date $vehicleQ $riderQ $resultQ ORDER BY a.id DESC");

        if($utype == 1 || $utype == 2)
        {
            return DB::select("SELECT * FROM `device_drive` a, summary b where a.vin_no !='' and a.vin_no=b.vin_no and a.drive = b.driveno $date $vehicleQ $riderQ $resultQ ORDER BY a.id DESC");
        }
        else if($utype == 3)
        {
            return DB::select("SELECT * FROM `device_drive` a, summary b,device d  where a.vin_no !='' and a.vin_no=b.vin_no and a.drive = b.driveno and b.device_id = a.device_id and a.device_id = d.device_id and d.partner = '$uid'  $date $vehicleQ $riderQ $resultQ ORDER BY a.id DESC");
            // return DB::select("SELECT * FROM `device_drive` a, summary b,device d where a.vin_no !='' and a.vin_no=b.vin_no and a.drive = b.driveno and a.device_id = d.device_id and d.partner = '$uid'   ORDER BY a.id DESC");
        }
        else
        {
            return DB::select("SELECT * FROM `device_drive` a, summary b,device d , users u where a.vin_no !='' and a.vin_no=b.vin_no and a.drive = b.driveno and b.device_id = a.device_id and a.device_id = d.device_id and d.id = u.device and u.uid = '$uid' $date $vehicleQ $riderQ $resultQ ORDER BY a.id DESC");
            // return DB::select("SELECT * FROM `device_drive` a, summary b,device d , users u where a.vin_no !='' and a.vin_no=b.vin_no and a.drive = b.driveno and b.device_id = a.device_id and a.device_id = d.device_id and d.id = u.device and u.uid = '$uid'   ORDER BY a.id DESC");
        }
        

    }
}
