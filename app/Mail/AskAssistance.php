<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AskAssistance extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->from('support@enginecal.com')
                    ->view('email.askassistance')
                   /* ->text('mails.demo_plain')*/
                    ->with(
                      [
                            'name'      => $this->data->name,
                            'device_id' => $this->data->device_id,
                            'vinfo'     => $this->data->vinfo,
                            'topic'     => $this->data->topic,
                            'desc'      => $this->data->desc
                      ])
                      ->attach(public_path('/logo').'/logo.jpg', [
                              'as' => 'logo.jpg',
                              'mime' => 'image/jpeg',
                      ]);
    }
}
