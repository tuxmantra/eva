<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Activationcode extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->data->name;
        return $this->from('support@enginecal.com')
                    ->view('email.activation')
                   /* ->text('mails.demo_plain')*/
                    ->with(
                      [
                            'name' => $name
                      ])
                      ->attach(public_path('/logo').'/logo.jpg', [
                              'as' => 'logo.jpg',
                              'mime' => 'image/jpeg',
                      ]);
    }
}
