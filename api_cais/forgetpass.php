<?php
include('config.php');
header('Content-Type: application/json');
include_once('../php_inc/myclass.php');
include_once('../php_inc/data-class.php');
include_once('../php_inc/alert-class.php');
$db = new myclass();
$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);
	$email = $obj['email'];
	$query = "select * from users where uemail ='$email' and ustatus = 1";
	if($res = $db -> select($query))
	{
		$email_id = $res[0]['uemail'];
		$pass = ENCRYPT_DECRYPT($res[0]['upwd'],$enc); //$res[0]['upwd'];
		// echo " \n ---- pass = $pass ---- \n";
		$subject = "Forgot Password Details";
		$headers = "From: ".$email_id;
		
		$message1 = "<table>
							<tr><td><!--Forgot Password--></td></tr>
							<tr><td>Details are given below : </td></tr>
							<tr><td>-----------------------------------</td></tr>
							<tr><td>User Name : ".$email_id." </td></tr>
							<tr><td>Password : ".$pass." </td></tr>
							<tr><td>-----------------------------------</td></tr>
							</table> \n";
		
		//$to = "santoshmahato@adsoft.co.in";
		$to = $email_id;
		//require_once '../send-mail.php';
		//$result=sendmail($to,$message1,$headers,$subject);
		$deviceID = "EngineCAL";
		//$alertAct = new alertAct($deviceID);
		//$result = $alertAct -> sendAlertMailOther(5,5,'EC_Password_reset_mail.php',$subject,1,$message1,1,$to);
		if($result == 1)
		{
			$msg = "Password sent successfully!";
			$arrayData['success'] = TRUE;
			$arrayData['message'] = $msg;
		}
		else
		{
			$msg = "Mail sending unsuccessful!";
			$arrayData['success'] = FALSE;
			$arrayData['errorCode'] = "1003";
			$arrayData['errorMessage'] = $msg;
		}
	}
	else
	{
		$msg = "\"$email\" is not a Registered E-Mail Id !";
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1001";
		$arrayData['errorMessage'] = $msg;
	}
}
else
{
	$arrayData['success'] = FALSE;
	$arrayData['errorCode'] = "1002";
	$arrayData['errorMessage'] = "Invalid Input!";
}
$resultVal = json_encode($arrayData);
echo $resultVal;
?>

