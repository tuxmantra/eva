<?php
include 'common.php';
header('Content-Type: application/json');
// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$result = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);
	$username = $obj['u'];
	$password = $obj['p'];
	$devID = $obj['d'];
	$password = 1;//ENCRYPT_DECRYPT($password,$enc);
	$utype = array('0'=>'','1'=>'Super Admin','2'=>'Admin','3'=>'Partner','4'=>'Partner 1','5'=>'Driver','6'=>'Premium User 1','7'=>'Premium User 2');
	
	if($res = chkEmailPass($username,$password,$conn))
	{
		$devID = $res['device'];
		$data = getdeviceFromID($devID,$conn);
		$deviceid = $data['device_id'];
		// if($devID == $deviceid)
		// {
			$result['success'] = TRUE;
			$result['userTypeId'] = $res['utype'];
			$result['userType'] = $utype[$res['utype']];
			$result['deviceID'] = $deviceid;
		// }
		// else
		// {
			// $result['success'] = FALSE;
			// $result['errorCode'] = "1003";
			// $result['errorMessage'] = "Invalid Device!";
		// }
	}
	else
	{
		$result['success'] = FALSE;
		$result['errorCode'] = "1001";
		$result['errorMessage'] = "Invalid Username or Password!";
	}
}
else
{
		$result['success'] = FALSE;
		$result['errorCode'] = "1002";
		$result['errorMessage'] = "Invalid Input!";
}
$resultVal = json_encode($result);
echo $resultVal;
?>