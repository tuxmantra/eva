<?php

include 'templates.php';

function getDbHandle(){

	$conn = new mysqli('localhost', 'chikmagalur', 'id$0Wroy', 'mullayanagiri');
	
	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	    $conn = null;
	}
	return $conn;
}

function getdevice($deviceid,$conn)
{
	$sql    = "select * from device where device_id='$deviceid'";
	$result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           $device = $row;
        }
    }
    return $device;
}

function getdeviceFromID($devID,$conn)
{
	$sql    = "select * from device where id='$devID' limit 0,1";
	$result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           $device = $row;
        }
    }
    return $device;
}

function getvehicle($did,$conn)
{
	$sql    = "select * from vehicles where device = '$did'";
	$result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           $vehicle = $row;
        }
    }
    return $vehicle;
}

function getvehicleSet($conn)
{
	$sql    = "select * from vehicle_setting where 1=1";
	$result = $conn->query($sql);
    $vehicle = $result->fetch_assoc();
    return $vehicle;
}

function updateDeviceTable($deviceid,$dist_currval,$dist_drvnum,$total_dist,$conn)
{
    $sql = "update device set dist_currval='$dist_currval',dist_drvnum='$dist_drvnum',total_dist='$total_dist' where device_id='$deviceid'";
    $device = $conn->query($sql);
    return $device;
}

function getDashboardData($year,$month,$conn)
{
	$sql    = "select * from dashboard where year = '$year' and month = '$month'";
	$result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           $dashData = $row;
        }
    }
    else
    {
    	$dashData = 0;
    }
    return $dashData;
}

function updateDashboard($type,$year,$month,$dDate,$dailyDist,$conn)
{
	if($type == 0)
	{
		$sql = "insert into dashboard (year, month,$dDate) values ('$year', '$month', '$dailyDist')";
	}
	else
	{
		$sql = "update dashboard set $dDate = '$dailyDist' where year = '$year' and month = '$month'";
	}
    $dashboard = $conn->query($sql);
    return $dashboard;
}
    
function insertDbSingle($table,$postVal,$conn) //adds a new row
{ 
	$tablefields = getfields($table,$conn);
	unset($tablefields[0]);

	$tablefields = array_values($tablefields);
	$fields = implode(',',$tablefields);
	$value  = "";
	$lastVal = count($tablefields)-1;
	$val = $tablefields;
	for($i=0; $i<count($tablefields); $i++)
	{
		if(isset($postVal[$val[$i]]))
		{
			$value.= "'".$postVal[$val[$i]]."'";
		}
		elseif(!isset($postVal[$val[$i]]))
		{
			$value.= "''";
		}
		if($i < $lastVal)
		{
			$value.= ",";
		} 
	}

	$queryStr = "insert into " . $table . "($fields) values($value)";
    $queryStrCorrected = str_replace("''", "'0'", $queryStr);
    //echo "$queryStrCorrected";die;
    $conn->query($queryStrCorrected);
    return $conn->insert_id;
}

function getfields($table,$conn){
    $sql             = "SHOW columns FROM $table";
    $device_data_col = $conn->query($sql);
    if ($device_data_col->num_rows > 0) {
	    $valLast = $postVal = $oldData = array();
	    while($row = $device_data_col->fetch_assoc()) {
	       $vestFields[] = $row['Field'];
	    }
    }
    return $vestFields;
}

function getSqlTableHeader($table,$conn)
{
	$tablefields = getfields($table,$conn);
	unset($tablefields[0]);
	$fields = implode(',',$tablefields);		
	
	$query  = "insert into " . $table . "($fields) values ";		
	return $query;
}

function getSqlQuery($table,$postVal,$conn)
{
	$tablefields = getfields($table,$conn);
	unset($tablefields[0]);
	$fields  = implode(',',$tablefields);
	$value   = "";
    unset($postVal[0]);
	$lastVal = count($tablefields);
	$val     = $tablefields;
	for($i=1; $i<=count($tablefields); $i++)
	{
		if(isset($postVal[$val[$i]]))
		{
			$value.= "'".$postVal[$val[$i]]."'";
		}
		elseif(!isset($postVal[$val[$i]]))
		{
			$value.= "''";
		}
		if($i < $lastVal)
		{
			$value.= ",";
		} 
	}
	
	$query = "($value)";
	return $query;
}

function getCaliPramVal($deviceID,$conn)
{
	//return 1;
	$sqlSett = "select * from vehicle_setting";
	$result  = $conn->query($sqlSett);
	if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           $vehSett = $row;
        }
    }

	$sql = "select v.* from vehicles as v, device as d where d.id = v.device and d.device_id ='$deviceID'";
	$result = $conn->query($sql);
    if ($result->num_rows > 0) {

        while($row = $result->fetch_assoc()) {
           $vehicle = $row;
        }
    }
    $pDVar   = ($vehicle['fuel_tpe'] == 1) ? "p" : "d" ;
	$f1      = "calval$pDVar";
	$vsetVal = json_decode($vehSett[$f1],true);
	$calval  = json_decode($vehicle['calval'],true);
	// echo "funct = $calval";
	return array('calval' => $calval, 'fuel' => $vehicle['fuel_tpe']);
}

function updateDataMultiple($table,$setField,$setVal,$postVal,$conn) // with value 
{
	$tablefields = getfields($table,$conn);
	$fields      = implode(',',$tablefields);
	$value       = "";

	$lastVal = count($tablefields)-1;
	$val     = $tablefields;
	for($i=1; $i<count($tablefields); $i++)
	{
		if(isset($postVal[$val[$i]]))
		{
			$value.= ($value!="") ? "," : "";
			$value.= $val[$i]."='".$postVal[$val[$i]]."'";
		}
	}
	$where = "";
	for($i=0; $i<count($setField); $i++)
	{
		$where .= " and ".$setField[$i] . "='".$setVal[$i]."'";
	}

	$queryStr = "update " . $table . " set $value where 1=1 $where";
    //If there are empty strings in the query, INSERT will fail. Find and replace empty string with 0
    $queryStrCorrected = str_replace("''", "'0'", $queryStr);
    //echo "$queryStrCorrected";die;
	if($conn->query($queryStrCorrected))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function getdeviceActStatus($deviceid,$conn)
{
	$sql = "select d.status,d.dev_cmds,a.expdt from device as d, activation_code as a where d.device_id = a.mac_id and d.device_id='$deviceid'";
	$result        = $conn->query($sql);
	if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           $device = $row;
        }
    }
	return $device;
}

function checkDriveNumForDeviceID($deviceid,$driveNum,$table,$conn)
{
	$sql = "select nodrive from $table where device_id = '$deviceid' and nodrive = '$driveNum'";
	$result       = $conn->query($sql);
    $retValue     = false;
    if ($result->num_rows > 0) {
        $retValue = true;
    }
	return $retValue;
}

function setDeviceLastLocation($gpsData,$conn)
{
    $deviceid   = $gpsData['device_id'];
    $lat        = $gpsData['lat'];
    $long       = $gpsData['lon'];
    $alt        = $gpsData['alt'];
    $time_stamp = $gpsData['time_s'];
    $sql        = "update device set latitude='$lat',longitude='$long',alt='$alt',time_stamp='$time_stamp' where device_id='$deviceid'";
    $device     = $conn->query($sql);
    return $device;
}

function getLastMonitorResult($deviceID,$conn)
{
    $sql           = "select * from device_monitor where device_id = '$deviceID' and comp_pres != '' order by id desc limit 0,1";
    $monLastResult = $conn->query($sql);
    if ($monLastResult->num_rows > 0) {
        while($row = $monLastResult->fetch_assoc()) {
            $monLastResultArr = $row;
        }
    }
    else {
    	$monLastResultArr = "";
    }
    return $monLastResultArr;
}

function getIdByDeviceId($device_id,$conn){
    $sql   = "select id from device where device_id = '$device_id'";
    $alert = $conn->query($sql);
    $id    = $alert->fetch_assoc();
    return $id['id'];
}

// get User Device Email
function getUserDeviceEmail($did,$conn){
    $id    = getIdByDeviceId($did,$conn);
    $sql   = "select uemail,uname from users where device = '$id'";
    $alert = $conn->query($sql);
    $data  =  $alert->fetch_assoc();
    return $data;
}

// Get details of alert by category and sub category.
function getAlertMailDetails($cat,$subcat,$conn){
    $sql   = "select 'desc' as description,msg,to_admin,to_user,to_approval,alert_category_id,alert_type from alert_mails where alert_category_id = '$cat' and alert_sub_category_id = '$subcat'";
    $alert = $conn->query($sql);
    return $alert->fetch_assoc();
}

// get Id by device id for user device
function generateAlertMailAdvMon($did,$cat,$subcat,$conn){
    
    $data         = getAlertMailDetails($cat,$subcat,$conn);
    $alertDesc    = $data['description'];
    $alertMsg     = $data['msg'];
    $alert_cat    = $data['alert_category_id'];
    $alert_type   = $data['alert_type'];
    $data         = getUserDeviceEmail($did,$conn);
    $email        = $data['uemail'];
    $to           = "sarvesh.adyanthaya@enginecal.com";
    $subject      = "Alert Engine Mail";
    $data['msg']  = $alertMsg;
    
    $message      = getTemplate(1,$data);
    
    // Always set content-type when sending HTML email
    $headers  = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    
    // More headers
    $headers .= 'From: <support@enginecal.com>' . "\r\n";
    $headers .= 'Cc: support@enginecal.com' . "\r\n";
    
    mail($to,$subject,$message,$headers);
    $postVal = array(
    'devid'  => $did,
    'ftype'  => $alert_cat,
    'atype'  => $alert_type,
    'msg'    => $alertMsg,
    'status' => '1',
    'dt'     => date('Y-m-d H:i:s'),
    'exp'    => date('Y-m-d')
    );
    $id      = insertDbSingle('alert',$postVal,$conn);
}

function getOilLifeValue($deviceID,$driveID,$conn)
{
	$driveID = $driveID - 1;
	// echo "select min(F1) as minCool, max(F1) as maxCool, max(F19) as maxEload from device_data where device_id = '$deviceID' and Fsprint = '$driveID'";die;
	$sql     = "select min(F1) as minCool, max(F1) as maxCool, max(F19) as maxEload from device_data where device_id = '$deviceID' and Fsprint = '$driveID'";
	$result  = $conn->query($sql);
	return $result->fetch_assoc();
}

function getTotalDistCovered($deviceID,$conn)
{
	$sql    = "select total_dist from device where device_id = '$deviceID'";
	$result = $conn->query($sql);
	return $result->fetch_assoc();
}

function getOilLifeDays($deviceID,$conn)
{
	$sql    = "select oillife from device_drive where device_id = '$deviceID' order by id desc limit 0,1";
	$result = $conn->query($sql);
	return $result->fetch_assoc();
}

function updateDeviceCmdCol($deviceID,$cond,$conn)
{
	$sql    = "update device set $cond where device_id = '$deviceID'";
	$device = $conn->query($sql);
    return $device;
}

function retriveCsvEva($start,$end,$did,$conn)
{
	$sql    = "select * from device_data where device_id = '$did' and time_v between '$start' and '$end'";
	$result = $conn->query($sql);
	return $result;
}

function getValuePartList($conn){
    $sql    = "select id,device1 from valuepart where type = '2' and status = '1'";
    //SELECT id,title FROM `valuepart` WHERE id IN (1,2,3,4,5)
	$result = $conn->query($sql);
	$col    = array();
	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) {
	    	$id       = $row['id'];
	    	$col[$id] = $row['device1'];
	    }
    }
	return $col;
}

//check whether user email and password are available in the database
function chkEmailPass($username,$password,$conn)
{
	$sql    = "select * from users where uemail ='$username' and ustatus = 1";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           $device = $row;
        }
    }
	return $device;
}

/****************************************************************************/
/******************** Standalone Device APIs Begin **************************/

function getDriveByStats($deviceID,$drive,$conn)
{
	$sql = "select * from device_drive where device_id='$deviceID' and (dur*1) > 0 order by id desc, ad_dt desc limit 0,$drive";
	$result = $conn->query($sql);
		if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           $stats[] = $row;
        }
    }
	return $stats;
}

function getStatsByDriveRange($deviceID,$driveStart,$driveEnd,$conn)
{
	$sql = "select * from device_drive where device_id='$deviceID' and (dur*1) > 0 and drive between $driveStart and $driveEnd order by id desc, ad_dt desc";
	$result = $conn->query($sql);
		if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           $stats[] = $row;
        }
    }
	return $stats;
}

function getDriveScore($deviceID,$drive,$conn)
{
 	$sql = "select * from driver where device_id='$deviceID' order by id desc, ad_dt desc limit 0,1";
 	$result = $conn->query($sql);
		if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
           $drvScore[] = $row;
        }
    }
	return $drvScore;
}

function devDriveNumDataApi($deviceID,$drivenum,$conn)
{
	$sql = "select * from event where device_id = '".$deviceID."' and lat <> '' and lon <> '' and nodrive = '$drivenum' order by time_s asc";
	$dat =$conn->query($sql);
	$array = array();
	$aI = 0;
	foreach($dat as $val)
	{
		$array[$aI]	= array('ts' => $val['time_s'], 'lat' => $val['lat'], 'lang' => $val['lon'] , 'driveid' => $val['nodrive'] );
		$aI++;
	}
	return $array;
}

function devDriveRangeDataApi($deviceID,$driverangefrm,$driverangeto,$conn)
{
	$sql = "select * from event where device_id = '".$deviceID."' and lat <> '' and lon <> '' and nodrive >= '$driverangefrm' and nodrive <= '$driverangeto' order by nodrive desc,time_s asc";
	$dat = $conn->query($sql);
	$array = array();
	$aI = 0;
	foreach($dat as $val)
	{
		$array[$aI]	= array('ts' => $val['time_s'], 'lat' => $val['lat'], 'lang' => $val['lon'] , 'driveid' => $val['nodrive'] );
		$aI++;
	}
	return $array;
}

function locationLastApi($deviceID,$conn)
{
	$sql = "select * from event where device_id = '".$deviceID."' and lat <> '' and lon <> '' order by time_s desc limit 0,1";
	$dat = $conn->query($sql);
	$array = array();
	$aI = 0;
	foreach($dat as $val)
	{
		$array[$aI]	= array('time_s' => $val['time_s'], 'lat' => $val['lat'], 'lon' => $val['lon'] , 'alt' => $val['alt'] );
		$aI++;
	}
	return $array;
}

 function monitorData($deviceID,$conn)
 {
 	$sql = "select * from device_monitor where device_id='$deviceID' and drive<>'' order by id desc limit 0,1";
 	$dat = $conn->query($sql);
		if ($dat->num_rows > 0) {
        while($row = $dat->fetch_assoc()) {
           $result = $row;
        }
    }
	return $result;
 }


 function getVehDetail($mfd,$conn)
 {
 		$sql = "select * from vehicle_setting where id = '1'";
 		$result = $conn->query($sql);
		return $result->fetch_assoc();
 }

 function getVehInfo($keyMfd,$conn)
 {
 	$sql = "select * from vehinfo where mfd = '$keyMfd'";
 	$result = $conn->query($sql);
	return $result->fetch_assoc();
 }

 function getVehSpec($keyMfd,$keyVar,$conn)
 {
 	$sql = "select * from vehspec where mfd = '$keyMfd' and model = '$keyVar'";
 	$dat = $conn->query($sql);
 	if ($dat->num_rows > 0) {
        while($row = $dat->fetch_assoc()) {
           $result[] = $row;
        }
    }
    return $result;
 }

 function getRawData($deviceID,$conn)
 {
 	$sql    = "select * from device_data_s where device_id = '$deviceID' order by id desc limit 0,1";
 	$result = $conn->query($sql);
 	$res    = $result->fetch_assoc();
	return $res;
 }
    
    /// Praveen 28th Jan Edits
function getStatsValueByDrive($conn,$deviceID,$drive,$fld,$op='sum')
{
    $dVal = $deviceID;
    $sql = "select $op($fld*1) as val from (select $fld from device_drive where device_id = '$dVal' and (dur*1) > 0 order by id desc, ad_dt desc limit 0,$drive) as $fld";
    $result = $conn->query($sql);
    $currStat = $result->fetch_assoc();
    $retvalue = 0;
    if (trim($currStat['val']) != "")
    {
        $retvalue = $currStat['val'] + 0.0;
    }
    return $retvalue;
}

function getStatsValueByDate($conn,$deviceID,$fr,$to,$fld,$op='sum')
{
    $dVal = $deviceID;
    $sql = "select $op($fld*1) as val from device_drive where  device_id='$dVal' and dt>='$fr' and dt<='$to' and ($fld*1)>=0 and (dur*1)>0 order by id desc, ad_dt";
    $result = $conn->query($sql);
    $currStat = $result->fetch_assoc();
    $retvalue = 0;
    if (trim($currStat['val']) != "")
    {
        $retvalue = $currStat['val'] + 0.0;
    }
    return $retvalue;
}
    
function getStatsAverageMilage($deviceID,$conn)
{
    $dev = $deviceID;
    $sql = "select avg((avg_ml*1)) as val from device_drive where device_id ='$dev' and (dist*1) <> 0 and (avg_ml*1) <> 0";
    $result = $conn->query($sql);
    $currStat = $result->fetch_assoc();
    $retvalue = 0;
    if (trim($currStat['val']) != "")
    {
        $retvalue = $currStat['val'] + 0.0;
    }
    return $retvalue;
}
    
function getStatsTotalNumDaysDriven($deviceID,$conn)
{
    $dev = $deviceID;
    $sql = "select count(distinct dt) dt from device_drive where device_id='$dev' and dist <> '0' ";
    $result = $conn->query($sql);
    $currStat = $result->fetch_assoc();
    //print_r($currStat); echo " ----\n";
    $retvalue = 0;
    if (trim($currStat['dt']) != "")
    {
        $retvalue = $currStat['dt'] + 0.0;
    }
    return $retvalue;
}
    
function getStatsTotalDistanceDriven($deviceID,$conn)
{
    $dev = $deviceID;
    $sql = "select sum((dist*1)) as val from device_drive where device_id ='$dev' and (dist*1) <> 0 and (avg_ml*1) <> 0";
    $result = $conn->query($sql);
    $currStat = $result->fetch_assoc();
    //print_r($currStat); echo " ----\n";
    $retvalue = 0;
    if (trim($currStat['val']) != "")
    {
        $retvalue = $currStat['val'] + 0.0;
    }
    return $retvalue;
}
    
function getLastDriveNum($deviceID,$conn)
{
    $dev = $deviceID;
    $sql = "select * from device_drive where device_id='$deviceID' and (dur*1)>0 order by id desc limit 0,1";
    $result = $conn->query($sql);
    $currStat = $result->fetch_assoc();
    $retvalue = 0;
    if (trim($currStat['nodrive']) != "")
    {
        $retvalue = $currStat['nodrive'] + 0;
    }
    return $retvalue;
}
    
function isActivationCodeValid($acode,$conn)
{
    $sql = "select * from activation_code where activation_code = '$acode'";
    $dat = $conn->query($sql);
        if ($dat->num_rows > 0) {
        while($row = $dat->fetch_assoc()) {
           $result = $row;
        }
    }
    return $result;
}
/******************** Standalone Device APIs End **************************/
/**************************************************************************/
