<?php
/*This is an API to onboard the user and the device onto the backend system.
 * 
 * API format:
 * http://carnet.enginecal.com/api/profile?data={"user": 
 * {"deviceid":"10000123", "name":"John Smith", "email":"someone@example.com", "password":"*********",
 * "mobile":"+911234567890", "emergency_no1":"+911234567890", "emergency_no2":"+911234567890"}}
 * 
 * This API makes an addition to the user and device tables in the database.
 */
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);	
	$deviceID = $obj['user']['deviceid'];
	$devInfo = getdevice($deviceID);
	//print_r($devInfo);
	if($deviceID != "" )
	{		
		if($devInfo == "")
		{
			$postValD = array();
			$postValD['ad_dt'] = date('Y-m-d H:i:s');
			$postValD['ad_by'] = 2017;
			$postValD['device_id'] = $deviceID;
			$postValD['name'] = $deviceID;
			$postValD['partner'] = 60;
			$postValD['devtype'] = 1;			
			$postValD['status'] = 1;
			$saveData = new saveForm('device',$postValD);
			if($saveData->addData())
			{
				$devInfo = $devAct -> getdevice($deviceID);
			}			
		}
		else
		{
			$devInfo['partner'] = 60;
			$devInfo['devtype'] = 1;							
			$devInfo['status'] = 1;
			$saveData = new saveForm('device',$devInfo);
			$saveData->updateData('id','id');			
		}
		if($devInfo[0] != "")
		{		
			$prof = $obj['user'];	
			$drvInfo = $devAct -> getdriver($deviceID);
			$drvInfo = ($drvInfo[0] != "") ? $drvInfo : array();
			$drvInfo['utype'] = ($drvInfo['utype'] != "") ? $drvInfo['utype'] : 5;
			$drvInfo['device'] = $devInfo[0];
			$drvInfo['partner'] = 60;
			$drvInfo['uname'] = $prof['name'];
			$drvInfo['upwd'] = ENCRYPT_DECRYPT($prof['password'],$enc); //$prof['password'];
			$drvInfo['uemail'] = $prof['email'];
			$drvInfo['umob'] = $prof['mobile'];
			$drvInfo['umob'] .= ($prof['emergency_no1'] != "") ? ",E1".$prof['emergency_no1'] : "";
			$drvInfo['umob'] .= ($prof['emergency_no2'] != "") ? ",E2".$prof['emergency_no2'] : "";
			$drvInfo['ustatus'] = 0;
			$drvInfo['ulocation'] = "Bangalore";		
			$table = "users";
			if($drvInfo['uid'] == "")
			{	
				$dev = $db->select("select * from users where uemail = '".$drvInfo['uemail']."'");
				if(count($dev)>=1)
				{
					//$err="Email already exist";
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1005";			
					$arrayData['errorMessage'] = "Email already exist!";
				}
				else
				{
					$saveData = new saveForm($table,$drvInfo);
					if($saveData->addData())
					{
						$msg="Data added successfully";
						
						$sql = "select u.* from users as u, device as d where d.id=u.device and d.device_id='$deviceID'";
						$dat = $devAct -> select($sql);
						$userName = $dat[0]['uname'];
						$sqlUp = "update `device` set `descrp` = '$userName' where `device_id` = '$deviceID'";
						mysql_query($sqlUp);
						
						//echo "<script>alert('$msg')</script>";
						//echo "<script>location.replace('users-view.php')</script>";
						//exit;
						//$devAct = new devAct($deviceID);
						$devAct -> generateRandomString($drvInfo['uemail']);
						$alertAct = new alertAct($deviceID);
						//$alertAct -> sendAlertMailOther(5,1,'EC_NewAccount_mail.php','Welcome!');
						$alertAct -> sendAlertMailOther(5,8,'EC_NewAccount_mail.php','Welcome!');
						$arrayData['success'] = TRUE;
						$arrayData['message'] = $msg;
					}
					else
					{
						$err="Error in adding data";
						$arrayData['success'] = FALSE;
						$arrayData['errorCode'] = "1003";			
						$arrayData['errorMessage'] = $err;
					}
				}
			}
			elseif($drvInfo['uid'] != "")
			{			
				$devs = $db->select("select * from users where uid != '".$drvInfo['uid']."' and uemail = '".$drvInfo['uemail']."'");
				if(count($devs)>=1)
				{
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1005";			
					$arrayData['errorMessage'] = "Email already exist!";
				}
				else
				{
					$saveData = new saveForm($table,$drvInfo);
					if($saveData->updateData('uid','uid'))
					{
						$msg= "Data updated successfully";
						$arrayData['success'] = TRUE;
						$arrayData['message'] = $msg;
					}
					else
					{
						$err= "Data cannot be updated kindly try again later";					
						$arrayData['success'] = FALSE;
						$arrayData['errorCode'] = "1004";			
						$arrayData['errorMessage'] = $err;
					}
				}
			}
		}
		else
		{
			$arrayData['success'] = FALSE;
			$arrayData['errorCode'] = "1006";			
			$arrayData['errorMessage'] = "Invalid Device ID/ Device ID Not added!";		
		}	
	}
	else
	{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1001";			
		$arrayData['errorMessage'] = "Invalid Device ID!";		
	}	
}
else
{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1002";			
		$arrayData['errorMessage'] = "Invalid Input!";		
}	
$resultVal = json_encode($arrayData);	
echo $resultVal;

?>

