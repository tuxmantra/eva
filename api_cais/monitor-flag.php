<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
    $json = $_REQUEST['data'];
    $obj = json_decode($json,true);
    $deviceID = $obj['devID'];
    $devInfo = getdevice($deviceID,$conn);
    //print_r($devInfo);die;
    $dev = $deviceID;
    if(!empty($devInfo))
    {
		$driveNo = getLastDriveNum($deviceID,$conn);
        $arrayData['success'] = TRUE;
        $arrayData['flag'] = 0;
        $arrayData['upcalib'] = 0;
		$arrayData['driveno'] = $driveNo;
	}
	else
	{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1002";
		$arrayData['errorMessage'] = "Invalid Device ID!";
	}
}
else
{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1001";
		$arrayData['errorMessage'] = "Invalid Input!";
}
$resultVal = json_encode($arrayData);
echo $resultVal;
?>
