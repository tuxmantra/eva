<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);	
	$mfd = trim($obj['mfd']);
	$mod = trim($obj['model']);
	
	$res = getvehicleSet($conn);
	$mfdList = explode(';',$res['manufacturer']);
	$engCapList = explode(';',$res['engine_capacity']);
	$fuel = array('','Petrol','Diesel');
	
	if(in_array($mfd, $mfdList))
	{
		$arrayData['success'] = TRUE;	
		$keyMfd = array_search($mfd, $mfdList);

		$resM = getVehInfo($keyMfd,$conn);
		$varList = array();
		$modList = array();
		$fuel = array('','Petrol','Diesel');
		if(count($resM) >= 1)
		{
			$varList = explode(';',$resM['var']);
			$modList = explode(';',$resM['model']);
		}
		if($mod != "")
		{
			if(in_array($mod, $modList))
			{
				$keyMode = array_search($mod, $modList);				
			}
			else
			{
				$modNew++;	
				$keyMode = count($modList);
				$modList[$keyMode] = $mod;
			}
		}

		$dat = getVehSpec($keyMfd,$keyMode,$conn);
		$j = 0;
		if(count($dat) >= 1)
		{
			foreach($dat as $data)
			{
				$arrayData[$mod][$j]['fuelType'] = $fuel[$data['fuel']]; 
				$arrayData[$mod][$j]['varients'] = $varList[$data['varient']];			
				$arrayData[$mod][$j]['engineCapacity'] = $engCapList[$data['engcap']];
				$arrayData[$mod][$j]['bodyType'] = $data['btype'];
				$j++;
			}
		}
		else
		{
			$arrayData['success'] = FALSE;
			$arrayData['errorCode'] = "1003";			
			$arrayData['errorMessage'] = "Model is not in the list!";
		}	
	}
	else
	{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1002";			
		$arrayData['errorMessage'] = "Manufacturer is not in the list!";
	}
}
else
{
	$arrayData['success'] = FALSE;
	$arrayData['errorCode'] = "1001";			
	$arrayData['errorMessage'] = "Invalid Input!";		
}	
$resultVal = json_encode($arrayData);	
echo $resultVal;	

?>