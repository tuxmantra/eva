<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);	
	// print_r($obj);die;
	$deviceID = $obj['veh_basic']['deviceid'];
	$devInfo = getdevice($deviceID,$conn);
	$did = $devInfo['id'];
	//print_r($devInfo);
	$errorMF = "";
	if($deviceID != "" && $devInfo != "")
	{
		$prof = $obj['veh_basic'];	
		$drvInfo = getvehicle($did,$conn);
		$settInfo = getvehicleSet($conn);

		$mfd = explode(';',$settInfo['manufacturer']);
		$mfd = array_map('strtolower',$mfd);
		$cap = explode(';',$settInfo['engine_capacity']);
		$cap = array_map('strtolower',$cap);
		// $drvInfo = $drvInfo;
		
		$drvInfo = ($drvInfo != "") ? $drvInfo : array();
		//$drvInfo['utype'] = ($drvInfo['utype'] != "") ? $drvInfo['utype'] : 5;
		$drvInfo['device'] = $devInfo['device']['id'];
		$drvInfo['reg_no'] = $prof['veh_registration'];
		$drvInfo['manufact'] = "";
		$myMfd = strtolower(trim($prof['veh_manufacturer']));
		// print_r($drvInfo);die;
		if(in_array($myMfd,$mfd))
		{
			$indexM = array_search($myMfd,$mfd);	
			$drvInfo['manufact'] = $indexM;
		}
		else
		{
			$errorMF = $prof['veh_manufacturer'] . " manufacturer is not available";
		}
		
		//$drvInfo['model'] = $prof['veh_model'];
		//$drvInfo['varient'] = $prof['veh_varient'];
		
		$modelList = array();
		$varlList = array();
		// $sql = "select * from vehinfo where mfd = '$indexM'";
		$resM = getVehInfo($indexM,$conn);
		
		if(count($resM) >= 1)
		{
			//$arrayData['varients'] = explode(';',$resM[0]['var']);
			$modelList = explode(';',$resM['model']);
			$varlList  = explode(';',$resM['var']);
		}
		
		if(in_array($prof['veh_model'],$modelList))
		{
			$indexMod           = array_search($prof['veh_model'],$modelList);	
			$drvInfo['modelid'] = $indexMod;
			$drvInfo['model']   = $indexMod;
		}
		else
		{
			$errorMF = $prof['veh_model'] . " model is not available";
		}
		
		if(in_array($prof['veh_varient'],$varlList))
		{
			$indexVar             = array_search($prof['veh_varient'],$varlList);	
			$drvInfo['varientid'] = $indexVar;
			$drvInfo['varient']   = $indexVar;
		}
		else
		{
			$errorMF = $prof['veh_varient'] . " varient is not available";
		}
		
		$drvInfo['fuel_tpe']        = (trim($prof['fuel_type']) == "Petrol") ? '1' : '2';
		$drvInfo['manufact_yr']     = $prof['mfg_year'];
		$drvInfo['travel_purchase'] = $prof['odo'];
		$drvInfo['eng_cap']         = "";
		$drvInfo['trans_type']      = $prof['veh_transmission'];
		$myCap                      = strtolower(trim($prof['engine_capacity']));
		if(in_array($myCap,$cap))
		{
			$indexM             = array_search($myCap,$cap);	
			$drvInfo['eng_cap'] = $indexM;
		}
		else
		{
			$errorMF .= ($errorMF != "" ) ? ", " : "";
			$errorMF .= $prof['engine_capacity'] . " engine capacity is not available";
		}
		// print_r($cap);die;
		$drvInfo['purchase_dt']  = date('Y-m-d');
		$drvInfo['last_serv_dt'] = date('Y-m-d');
		$drvInfo['oil_dt']       = date('Y-m-d');
		//to add last service kms in multiples of 10000
		$a = strlen($prof['odo']);
		if($a >= 5)
		{
			$val = round($prof['odo'],-($a - 1));
		}
		else
		{
			$val = 0;
		}
		$drvInfo['travel_service'] = $val;
		$drvInfo['oil_dist'] = $val;
		// print_r($drvInfo);die;
		$table   = "vehicles";
		if($errorMF != "") {
			$err                       = "Input data Incorrect!";
			$arrayData['success']      = FALSE;
			$arrayData['errorCode']    = "1003";
			$arrayData['errorMessage'] = $err;
		}
		else
		{
			if($drvInfo['id'] == "")
			{
				/*$dev = $db->select("select * from users where uemail = '".$drvInfo['uemail']."'");
				if(count($dev)>=1)
				{
					//$err="Email already exist";
					$arrayData['success'] = FALSE;
					$arrayData['errorCode'] = "1005";
					$arrayData['errorMessage'] = "Email already exist!";
				}
				else*/
				
				$saveData = insertDbSingle($table,$drvInfo,$conn);
				// print_r($saveData);die;
				if($saveData)
				{
					/*$spec = $devAct -> setvehSpec($drvInfo['manufact'],$drvInfo['modelid'],$drvInfo['fuel_tpe'],$drvInfo['varientid'],$drvInfo['eng_cap'],$drvInfo['device'],$drvInfo['manufact_yr'],1);
					$spec = ($spec) ? "Vehicle Specification done" : " Vehicle Specification faild";
					$msg="Data added successfully $spec";
					//echo "<script>alert('$msg')</script>";
					//echo "<script>location.replace('users-view.php')</script>";
					//exit;*/
					$arrayData['success'] = TRUE;
					$arrayData['message'] = $msg;
				}
				else
				{
					$err                       = "Error in adding data";
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1003";
					$arrayData['errorMessage'] = $err;
				}
			}
			elseif($drvInfo['id'] != "")
			{
				$setField = array('id');
	        	$setVal   = array($drvInfo['id']);
				$saveData = updateDataMultiple($table,$setField,$setVal,$drvInfo,$conn);
				
				if($saveData)
				{
					/*$spec = $devAct -> setvehSpec($drvInfo['manufact'],$drvInfo['modelid'],$drvInfo['fuel_tpe'],$drvInfo['varientid'],$drvInfo['eng_cap'],$drvInfo['device'],$drvInfo['manufact_yr'],1);
					$spec = ($spec) ? "Vehicle Specification done" : " Vehicle Specification faild";*/
					$msg                  = "Data updated successfully!";
					$arrayData['success'] = TRUE;
					$arrayData['message'] = $msg;
				}
				else
				{
					$err = "Data cannot be updated kindly try again later";
					$arrayData['success']      = FALSE;
					$arrayData['errorCode']    = "1004";
					$arrayData['errorMessage'] = $err;
				}
			}
		}
	}
	else
	{
		$arrayData['success']      = FALSE;
		$arrayData['errorCode']    = "1001";
		$arrayData['errorMessage'] = "Invalid Device ID!";
	}
}
else
{
	$arrayData['success']      = FALSE;
	$arrayData['errorCode']    = "1002";
	$arrayData['errorMessage'] = "Invalid Input!";
}
if($errorMF != "")
{
	$arrayData['alert'] = $errorMF;
}
$resultVal = json_encode($arrayData);
echo $resultVal;
?>