<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json     = $_REQUEST['data'];
	$obj      = json_decode($json,true);
	$deviceID = $obj['devID'];
	$drive    = trim($obj['driveno']);
	$type     = trim($obj['type']);
	
	// Get Device Data
	$devInfo = getdevice($deviceID,$conn);

	$did     = $devInfo['id'];
	//print_r($devInfo);
	
	if(!empty($devInfo))
	{
		$yVal = 0;
		if(trim($obj['type']) == 'drive')
		{
			$fr = 1;
			$to = $drive;
			
			$dat = getDriveByStats($deviceID,$drive,$conn);

			// $sql = "select * from device_drive where device_id='$deviceID' and (dur*1) > 0 order by id desc, ad_dt desc limit 0,$drive";
			// $dat = $db -> select($sql);
			//print_r($dat);die;
			$arryI = 0;
			
			// $arrayData['dtc_lastdrive'] = "";
			while($to >= $fr)
			{
				if($dat[$fr - 1]['dur'] == 0)
				{
					$fr = $fr + 1;
                    continue;
				}
				else
				{
					$reqFor    = $arryI; //$fr;
					$arrayData[$reqFor] = array();
					$arryI++;
					$dtFA      = $dat[$fr - 1]['dt'];
					$dtFA      = strtotime(str_replace(':', '-', $dtFA));
					$date      = date('d:m:Y',$dtFA);
					$durTr     = $dat[$fr - 1]['dur'];
					$distTr    = $dat[$fr - 1]['dist'];
					$avSpeed   = $dat[$fr - 1]['avg_speed'];
					$maxSpeed  = $dat[$fr - 1]['max_sp'];
					$totCon    = $dat[$fr - 1]['ful_c'];
					$totMil    = $dat[$fr - 1]['avg_ml'];
					$maxTor    = $dat[$fr - 1]['max_tor'];
					$fOver     = $dat[$fr - 1]['ful_ovr'];
					$idT       = $dat[$fr - 1]['idt'];
					$ovT       = $dat[$fr - 1]['dur_ovr'];
					//EC added extra parameter for dist in overrun
					$ovD       = $dat[$fr - 1]['dist_ovr'];
					$hacc      = $dat[$fr - 1]['hacc'];
					$hdcc      = $dat[$fr - 1]['hdcc'];
					$ovrspd    = $dat[$fr - 1]['ovrspd'];
					$vehstall  = $dat[$fr - 1]['vehstall'];
					$driveID   = $dat[$fr - 1]['nodrive'];
					$timestamp = $dat[$fr - 1]['drive'];
					
					if($timestamp > $durTr)
					{
						$timestamp = $timestamp - $durTr;
					}
					else
					{
						//Do nothing
					}
					
					$arrayData[$reqFor]['driveno']        = $driveID + 0.0;
					$arrayData[$reqFor]['drive_id']       = $driveID + 0.0;
					$arrayData[$reqFor]['date']           = $date;
					$arrayData[$reqFor]['ts']             = $timestamp * 1000;
					$arrayData[$reqFor]['drive_duration'] = round($durTr,0);//$durTr;
					$arrayData[$reqFor]['drive_dist']     = round($distTr,4);
					$arrayData[$reqFor]['avg_speed']      = round($avSpeed,2);
					$arrayData[$reqFor]['max_speed']      = round($maxSpeed,0);//$maxSpeed;
					$arrayData[$reqFor]['fuel_consumed']  = round($totCon,4);
					$arrayData[$reqFor]['mileage']        = round($totMil,4);
					$arrayData[$reqFor]['max_tor']        = round($maxTor,2);
					
					//EC added extra parameter for dist in overrun
					$arrayData[$reqFor]['dist_ovr']       = round($ovD,4);
					$arrayData[$reqFor]['fuel_save_ovr']  = round($fOver,3);
					$arrayData[$reqFor]['idle_time']      = round($idT,0);//$idT;
					$arrayData[$reqFor]['time_ovr']       = round($ovT,0);//$ovT;
					$arrayData[$reqFor]['hacc']           = round($hacc,0);
					$arrayData[$reqFor]['hdcc']           = round($hdcc,0);
					$arrayData[$reqFor]['ovrspd']         = round($ovrspd,0);
					$arrayData[$reqFor]['vehstall']       = round($vehstall,0);

					$fr = $fr + 1;
				}
			}
			// print_r($arrayData);
			// $sql = "select * from driver where device_id='$deviceID' order by id desc, ad_dt desc limit 0,1";
			// $drvScore = $db -> select($sql);
			/*$drvScore = getDriveScore($deviceID,$drive,$conn);
			if ($drvScore[0]['drv_score'] == '')
			{
				$arrayData[0]['eDasScor'] = '0';
			}
			else
			{
				$arrayData[0]['eDasScor'] = $drvScore[0]['drv_score'];
			}
			if ($drvScore[0]['drv_score_cmts'] == '')
			{
				$arrayData[0]['eDasComments'] = '-';
			}
			else
			{
				$arrayData[0]['eDasComments'] = $drvScore[0]['drv_score_cmts'];
			}
			if ($drvScore[0]['hlt_score'] == '')
			{
				$arrayData[0]['eHlt'] = '0';
			}
			else
			{
				$arrayData[0]['eHlt'] = $drvScore[0]['hlt_score'];
			}
			
			$myData = array();
			$myData = $arrayData;*/
			// $arrayData = array();
			// $arrayData['data'] = $myData;
			$myData = array();
			$myData = $arrayData;
			$arrayData = array();
			$arrayData['data'] = $myData;

			$arrayData['success'] = TRUE;
			$arrayData['type']    = $type;
			$arrayData['count']   = count($myData);
		}
		else
		{
			$arrayData['success'] = FALSE;
			$arrayData['errorCode'] = "1003";
			$arrayData['errorMessage'] = "Invalid Type!";
		}
	}
	else
	{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1001";
		$arrayData['errorMessage'] = "Invalid Device ID!";
	}
}
else
{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1002";
		$arrayData['errorMessage'] = "Invalid Input!";
}
$resultVal = json_encode($arrayData);
echo $resultVal;
?>
