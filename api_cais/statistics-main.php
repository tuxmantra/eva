<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
    $json = $_REQUEST['data'];
    $obj = json_decode($json,true);
    $deviceID = $obj['devID'];
    $devInfo = getdevice($deviceID,$conn);
    //print_r($devInfo);die;
	$dev = $deviceID;
	if(!empty($devInfo))
	{
		$yVal = 0;
		$arrayData['success'] = TRUE;
		
		if(trim($obj['type']) == 'serviceDue')
		{
//			$sql = "select * from service where devid = '$deviceID'";
//			$dat = $devAct -> select($sql);
//			if(count($dat) > 0)
//			{
//				$devObj = new devAct('device1');
//				$vehdetls = $devObj -> getvehicle($dev);
//				$vehdetls = $vehdetls[0];
//
//				$purchase_dt = $vehdetls['purchase_dt'];
//				$last_serv_dt1 = $vehdetls['last_serv_dt'];
//				$last_serv_dt = strtotime($vehdetls['last_serv_dt']);
//				$travel_service = $vehdetls['travel_service'];
//				$travel_purchase = $vehdetls['travel_purchase'];
//				$purchase_dt1 = strtotime($purchase_dt);
//				$manufact_yr = $vehdetls['manufact_yr'];
//				$oilLifeKmsC = $vehdetls['oill_dist'];
//				$lastOilChngKm = $vehdetls['oil_dist'];
//				// echo "oilLifeKmsC = $oilLifeKmsC |  | lastOilChngKm = $lastOilChngKm \n";
//
//				$sql = "select sum(dist*1) from device_drive where device_id ='$deviceID' and (dist*1) <> 0 and (avg_ml*1) <> 0";
//				$numDist = mysql_fetch_array(mysql_query($sql));
//				$numDist = round($numDist[0],2);
//				$disttrv = $numDist + $travel_purchase;
//
//				$factdist = floor($disttrv/$oilLifeKmsC);
//				$factoilchng = floor($lastOilChngKm/$oilLifeKmsC);
//				if($factdist == $factoilchng)
//				{
//					$kmSOChng = ($disttrv - ($oilLifeKmsC * $factdist)) - ($lastOilChngKm  - ($oilLifeKmsC * $factoilchng));
//				}
//				else
//				{
//					$kmSOChng = $disttrv - $lastOilChngKm;
//				}
//
//				// echo "\n disttrv = $disttrv | $factdist | $factoilchng | kmSOChng = $kmSOChng \n";
//				//$nextMf = $purchase_dt + 1;
//				//$nextPur2 = strtotime('+1 year',$purchase_dt1);
//
//				// $Oil_Life_Kms_C = 10000;	//$devAct -> getCaliPramVal('Oil_Life_Kms_C',$deviceID);
//				// $factdist = floor($travel_purchase / $Oil_Life_Kms_C);
//				// $kmSOChng = ($travel_purchase - ($Oil_Life_Kms_C * $factdist));
//
//				$lastOilChngDt = ($vehdetls['oil_dt'] != "0000-00-00" && $vehdetls['oil_dt'] != "") ? $vehdetls['oil_dt'] : "2017-01-01"; // set and get frm veh info
//				$sql = "select * from device_drive where device_id ='$dev' order by id desc";
//				$dat = $db -> select($sql);
//				$oilUsed = $dat[0]['oillife'];
//				$daySOUsed = round((strtotime(date('Y-m-d')) - strtotime($lastOilChngDt)) / (60*60*24));
//				// echo"/n daySOUsed = $daySOUsed \n";
//				if($daySOUsed < $oilUsed)
//				{
//					$daySOUsed = $oilUsed;
//				}
//				else
//				{
//					//do nothing;
//				}
//				// echo"/n lastOilChngDt = $lastOilChngDt | oilUsed = $oilUsed | daySOUsed = $daySOUsed \n";
//
//				$Oil_Life_Days_C = 365;
//				$daySOChng = $Oil_Life_Days_C - $daySOUsed;
//				//$yPur = date('Y',$purchase_dt1);
//
//				//$dtTemp = strtotime('1980-01-01');
//				$today = strtotime(date('Y-m-d'));
//				$pend = 0;
//
//				if($last_serv_dt == "" && $travel_service == "") // “Date of Last Service” and “Distance Travelled since Last Service” data are Blank/No data filled
//				{
//					//echo date('d-m-Y',$purchase_dt1) . "|" .date('d-m-Y',$nextPur2);
//
//					if ($today < ($purchase_dt1 + (60*60*24*30))) // Today < Date of Purchase + 1month
//					{
//						$pend = 1000 - $kmSOChng;//Distance due for Service = 1000 - Distance Travelled Since Purchase;
//					}
//					elseif ($today < ($purchase_dt1 + (60*60*24*30*6)))
//					{
//						$pend = 5000 - $kmSOChng;//Distance due for Service = 5000 - Distance Travelled Since Purchase;
//					}
//					elseif ($today < ($purchase_dt1 + (60*60*24*365)))
//					{
//						$pend = 10000 - $kmSOChng;//Distance due for Service = 10000 - Distance Travelled Since Purchase;
//					}
//					elseif ($today == ($purchase_dt1 + (60*60*24*365)))
//					{
//						$pend = 10000;
//					}
//					else
//					{
//						$pend = "No Service Data";
//					}
//					$pend = max($pend,0);
//				}
//				elseif($last_serv_dt != "" && $travel_service != "" )
//				{
//					$date = $last_serv_dt + (60*60*24*365);
//					if ($today < ($last_serv_dt + (60*60*24*365)))
//					{
//						$pend = 10000 - $kmSOChng;	//Distance due for Service = 10000 - Distance Travelled Since Purchase;
//					}
//					else
//					{
//						$pend = 0;
//					}
//					// echo "i am here pend 2 = $pend | $date \n";
//				}
//				// echo "pend = $pend \n";
//				//echo date('d-m-Y',$last_serv_dt) . "|" .date('d-m-Y',$dtTemp);
//				unset($devlist);
//				unset($devN);
//				$vehdetls = "";
//
//				/*$sql = "select dt from device_dataday where device_id ='$dev' and (dist*1) <> 0 and (milavg*1) <> 0  order by dt limit 0,1";
//				$dateFirst = $db->select($sql);
//				$dateFirst = $dateFirst[0][0];
//				$dF = $dateFirst . " 00:00:00";*/
//
//				$pendN = $pend * 1;
//				$updateValNull = "";
//				if($pendN >= 1 && $pend != "No Service Data")
//				{
//					// $pend = $pend - $numDist;
//					//Kms to oil change cannot be negative
//					if($pend < 0)
//					{
//						$pend = 0;
//					}
//					elseif($pend > 10000)
//					{
//						$pend = 10000 * ($daySOChng / 365);
//					}
//					else
//					{
//						//do nothing;
//					}
//					$pend = round($pend);
//				}
//				//Days to oil change cannot be negative
//				if($daySOChng < 0)
//				{
//					$daySOChng = 0;
//				}
//				else
//				{
//					//do nothing
//				}
//				$arrayData['serviceDue'] = $pend;
//				$arrayData['serviceDueDays'] = $daySOChng;
//			}
//			else
//			{
//				$arrayData['serviceDue'] = 0;
//				$arrayData['serviceDueDays'] = 0;
//			}
            //TODO: To be removed LAter once Service Due Logoc is Implemented
            $arrayData['serviceDue'] = 0;
            $arrayData['serviceDueDays'] = 0;
		}
		elseif(trim($obj['type']) == 'averageMileage')
		{
			$numMil = getStatsAverageMilage($deviceID,$conn);
			$arrayData['averageMileage'] = $numMil;
		}
		elseif(trim($obj['type']) == 'averageDistancePerDay')
		{
			$numDays = getStatsTotalNumDaysDriven($deviceID,$conn);
			$numDist = getStatsTotalDistanceDriven($deviceID,$conn);
            
			$numDistAvg = ($numDays >= 1)  ? $numDist / $numDays : 0;
			$numDist = ($numDist <= 0) ? 0 : $numDist;
			$arrayData['averageDistancePerDay'] = $numDistAvg;
		}
		else
		{
			$arrayData['success'] = FALSE;
			$arrayData['errorCode'] = "1001";
			$arrayData['errorMessage'] = "Invalid Request Type!";
		}
	}
}
else
{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1002";
		$arrayData['errorMessage'] = "Invalid Input!";
}
$resultVal = json_encode($arrayData);
echo $resultVal;
?>
