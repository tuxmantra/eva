<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
    
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
    $json = $_REQUEST['data'];
    $obj = json_decode($json,true);
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);	
	$acode = $obj['acode'];
	$mac = $obj['mac'];
	$uby = $obj['uby'];
    
    // Check If Entered Activation Code is VALID
    
	$devAct = isActivationCodeValid($acode,$conn);
	
	if($devAct['id'] != "")
	{
		if($devAct['status'] == 1)
		{
			$table = "activation_code";	
			$devAct['mac_id'] = $mac;
			$devAct['used_by'] = $uby;
			$devAct['status'] = 2;
            $setField = array('id');
            $setVal = array($devAct['id']);
            $saveData = updateDataMultiple($table,$setField,$setVal,$devAct,$conn);
			if($saveData)
			{
				$msg= "Activation code updated successfully";
				$result['success'] = TRUE;
				$result['message'] = $msg;
				$result['expDate'] = date('d-m-Y',strtotime($devAct['expdt']));
//				$alertAct = new alertAct($mac);
//				$subject = "Activation code status";
//				$alertAct -> sendAlertMailOther(5,2,'EC_NewAccount_mail.php',$subject,1,$msg,1);
			}
		}
		elseif($devAct['status'] == 2)
		{
			if($devAct['mac_id'] == $mac && $devAct['used_by'] == $uby)
			{
				$table = "activation_code";	
				//$devAct['mac_id'] = $mac;
				//$devAct['used_by'] = $uby;
				$devAct['status'] = 2;
                $setField = array('id');
                $setVal = array($devAct['id']);
                $saveData = updateDataMultiple($table,$setField,$setVal,$devAct,$conn);
                if($saveData)
                {
					$msg= "Activation code restored successfully";
					$result['success'] = TRUE;
					$result['message'] = $msg;
					$result['expDate'] = date('d-m-Y',strtotime($devAct['expdt']));
					$expStat = (strtotime($devAct['expdt']) < strtotime(date('d-m-Y'))) ? TRUE : FALSE;
					$result['expStatus'] = $expStat;
					//$alertAct = new alertAct($mac);
					//$subject = "Activation code status";
					//$alertAct -> sendAlertMailOther(5,2,'EC_NewAccount_mail.php',$subject,1,$msg,1);					
				}
			}
			else
			{
				$result['success'] = FALSE;
				$result['errorCode'] = "1003";			
				$result['errorMessage'] = "Invalid Device!";
			}	
		}
		else
		{
			$result['success'] = FALSE;
			$result['errorCode'] = "1001";			
			$result['errorMessage'] = "Invalid Activation Code!";	
		}
		
	}
	else
	{
//		if($devActUser['id'] != "" && $acode == "")
//		{
//			if($devActUser['status'] == 1)
//			{
//				$result['success'] = FALSE;
//				$result['errorCode'] = "1001";
//				$result['errorMessage'] = "Invalid Activation Code!";
//			}
//			elseif($devActUser['status'] == 2)
//			{
//				if($devActUser['mac_id'] == $mac && $devActUser['used_by'] == $uby)
//				{
//					$msg= "Activation code restored successfully";
//					$result['success'] = TRUE;
//					$result['message'] = $msg;
//					$result['expDate'] = date('d-m-Y',strtotime($devActUser['expdt']));
//					$expStat = (strtotime($devActUser['expdt']) < strtotime(date('d-m-Y'))) ? TRUE : FALSE;
//					$result['expStatus'] = $expStat;
//				}
//				else
//				{
//					$result['success'] = FALSE;
//					$result['errorCode'] = "1001";
//					$result['errorMessage'] = "Invalid Device!";
//				}
//			}
//			else
//			{
//				$result['success'] = FALSE;
//				$result['errorCode'] = "1001";
//				$result['errorMessage'] = "Subscription Invalid!";
//			}
//		}
//		else
//		{
//			$result['success'] = FALSE;
//			$result['errorCode'] = "1001";
//			$result['errorMessage'] = "Invalid Activation Code!";
//		}
            $result['success'] = FALSE;
            $result['errorCode'] = "1001";
            $result['errorMessage'] = "Invalid Activation Code!";
	}	
}
else
{
		$result['success'] = FALSE;
		$result['errorCode'] = "1002";			
		$result['errorMessage'] = "Invalid Input!";		
}	
$resultVal = json_encode($result);	
echo $resultVal;
?>
