<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);
	$deviceID = $obj['devID'];
	$devInfo = getdevice($deviceID,$conn);
	// print_r($devInfo);die;
	if($deviceID != "" && $devInfo != "")
	{
		$msg= "Data updated successfully";
		$arrayData['success'] = TRUE;

		$lastLocation = locationLastApi($deviceID,$conn);
		if(!empty($lastLocation))
		{
			$arrayData['lat']  = $lastLocation[0]['lat'];
			$arrayData['long'] = $lastLocation[0]['lon'];
			$arrayData['alt']  = $lastLocation[0]['alt'];
			$arrayData['ts']   = $lastLocation[0]['time_s'];
		}
		else
		{
			$arrayData['success'] = FALSE;
			$arrayData['errorCode'] = "1003";
			$arrayData['errorMessage'] = "No location Info!";
		}
	}
	else
	{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1001";
		$arrayData['errorMessage'] = "Invalid Device ID!";
	}
}
else
{
	$arrayData['success'] = FALSE;
	$arrayData['errorCode'] = "1002";
	$arrayData['errorMessage'] = "Invalid Input!";
}

if($errorMF != "")
{
	$arrayData['alert'] = $errorMF;
}
$resultVal = json_encode($arrayData);
echo $resultVal;
?>