<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);
	$deviceID = $obj['devID'];
	$drivenum = $obj['drivenum'];
	$driverangefrm = $obj['driveidstart'];
	$driverangeto = $obj['driveidend'];
	$devInfo = getdevice($deviceID, $conn);
	// print_r($devInfo);die;
	if(!empty($devInfo))
	{
		if(($drivenum >= 0) && (!empty($drivenum)))
		{
			$arrayData['success'] = TRUE;
			$dataLoc = devDriveNumDataApi($deviceID,$drivenum,$conn);
			if(count($dataLoc) >= 1)
			{
				$arrayData['data'] = $dataLoc;
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1003";
				$arrayData['errorMessage'] = "No location Info!";
			}
		}
		elseif(($driverangefrm >= 0) && ($driverangefrm != "") && ($driverangeto >= 0) && ($driverangeto != ""))
		{
			$arrayData['success'] = TRUE;
			$dataLoc = devDriveRangeDataApi($deviceID,$driverangefrm,$driverangeto,$conn);
			if(count($dataLoc) >= 1)
			{
				$arrayData['data'] = $dataLoc;
			}
			elseif ($driverangefrm > $driverangeto)
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1002";
				$arrayData['errorMessage'] = "Invalid Input!";
			}
			else
			{
				$arrayData['success'] = FALSE;
				$arrayData['errorCode'] = "1003";
				$arrayData['errorMessage'] = "No location Info!";
			}
		}
		else
		{
			$arrayData['success'] = FALSE;
			$arrayData['errorCode'] = "1002";
			$arrayData['errorMessage'] = "Invalid Input!";
		}
	}
	else
	{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1001";
		$arrayData['errorMessage'] = "Invalid Device ID!";
	}
}
else
{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1002";
		$arrayData['errorMessage'] = "Invalid Input!";
}
if($errorMF != "")
{
	$arrayData['alert'] = $errorMF;
}
$resultVal = json_encode($arrayData);
echo $resultVal;
?>