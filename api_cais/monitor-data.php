<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);	
	$deviceID = $obj['devID'];
	$devInfo = getdevice($deviceID,$conn);
	// print_r($devInfo);die;
	if(!empty($devInfo))
	{
		// $sql = "select * from device_monitor where device_id='$deviceID' and drive<>'' and dur <> '' order by id desc limit 0,1";
		// $dat = $db -> select($sql);
		$dat = monitorData($deviceID,$conn);
		$data = $dat;
		
		//Bat Hel
		$batHel = array();
		if($data['bat_st'] == 1)
		{
			$batHel['rate'] = "1";
			$batHel['con'] = "Bad";
			$batHel['msg'] = "\"Battery Critical\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
		}
		elseif($data['bat_st'] == 2)
		{
			$batHel['rate'] = "2";
			$batHel['con'] = "Bad–Okay";
			$batHel['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
		}
		elseif($data['bat_st'] == 3)
		{
			$batHel['rate'] = "3";
			$batHel['con'] = "Bad-Okay";
			$batHel['msg'] = "\"Battery Low\" Please check distilled water level and drive to charge. If Battery is more than 3 years old consider replacement.";
		}
		elseif($data['bat_st'] == 4)
		{
			$batHel['rate'] = "4";
			$batHel['con'] = "Bad";
			$batHel['msg'] = "\"Battery Low\" Please drive to charge.";
		}
		elseif($data['bat_st'] == 5)
		{
			$batHel['rate'] = "5";
			$batHel['con'] = "Good";
			$batHel['msg'] = "";
		}
		
		//Turbo
		$turbo =array();
		if($data['idlb_c'] == 1)
		{
			if($data['pekb_c'] == 1)
			{
				$turbo['rate'] = "1";
				$turbo['con'] = "Bad";
				$turbo['msg'] = "";
			}
			elseif($data['pekb_c'] == 2)
			{
				$turbo['rate'] = "2";
				$turbo['con'] = "Bad-Okay";
				$turbo['msg'] = "";
			}
			elseif($data['pekb_c'] == 3)
			{
				$turbo['rate'] = "3";
				$turbo['con'] = "Okay";
				$turbo['msg'] = "";
			}
		}
		elseif($data['idlb_c'] == 2)
		{
			if($data['pekb_c'] == 1)
			{
				$turbo['rate'] = "1";
				$turbo['con'] = "Bad";
				$turbo['msg'] = "";
			}
			elseif($data['pekb_c'] == 2)
			{
				$turbo['rate'] = "3";
				$turbo['con'] = "Okay";
				$turbo['msg'] = "";
			}
			elseif($data['pekb_c'] == 3)
			{
				$turbo['rate'] = "4";
				$turbo['con'] = "Okay-Good";
				$turbo['msg'] = "";
			}
		}
		elseif($data['idlb_c'] == 3)
		{
			if($data['pekb_c'] == 1)
			{
				$turbo['rate'] = "2";
				$turbo['con'] = "Bad-Okay";
				$turbo['msg'] = "";
			}
			elseif($data['pekb_c'] == 2)
			{
				$turbo['rate'] = "3";
				$turbo['con'] = "Okay";
				$turbo['msg'] = "";
			}
			elseif($data['pekb_c'] == 3)
			{
				$turbo['rate'] = "5";
				$turbo['con'] = "Good";
				$turbo['msg'] = "";
			}
		}
		$turbo['msg'] = (trim($data['idlb_m']) != trim($data['pekb_m']) ) ? trim($data['idlb_m']) ."<br/>". trim($data['pekb_m']) :  trim($data['pekb_m']);
		
		//Air
		$air =array();
		if($data['idlbc_v'] == 1)
		{
			if($data['peakb_v'] == 1)
			{
				$air['rate'] = "1";
				$air['con'] = "Bad";
				$air['msg'] = "";
			}
			elseif($data['peakb_v'] == 2)
			{
				$air['rate'] = "2";
				$air['con'] = "Bad-Okay";
				$air['msg'] = "";
			}
			elseif($data['peakb_v'] == 3)
			{
				$air['rate'] = "3";
				$air['con'] = "Okay";
				$air['msg'] = "";
			}
		}
		elseif($data['idlbc_v'] == 2)
		{
			if($data['peakb_v'] == 1)
			{
				$air['rate'] = "1";
				$air['con'] = "Bad";
				$air['msg'] = "";
			}
			elseif($data['peakb_v'] == 2)
			{
				$air['rate'] = "3";
				$air['con'] = "Okay";
				$air['msg'] = "";
			}
			elseif($data['peakb_v'] == 3)
			{
				$air['rate'] = "4";
				$air['con'] = "Okay-Good";
				$air['msg'] = "";
			}
		}
		elseif($data['idlbc_v'] == 3)
		{
			if($data['peakb_v'] == 1)
			{
				$air['rate'] = "2";
				$air['con'] = "Bad-Okay";
				$air['msg'] = "";
			}
			elseif($data['peakb_v'] == 2)
			{
				$air['rate'] = "3";
				$air['con'] = "Okay";
				$air['msg'] = "";
			}
			elseif($data['peakb_v'] == 3)
			{
				$air['rate'] = "5";
				$air['con'] = "Good";
				$air['msg'] = "";
			}
		}
		$air['msg'] = (trim($data['idlbc_m']) != trim($data['peakb_m']) ) ? trim($data['idlbc_m']) ."<br/>". trim($data['peakb_m']) : trim($data['idlbc_m']);
		
		//Fuel
		$fuel =array();
		if($data['idl_crp'] == 1)
		{
			if($data['peak_crp'] == 1)
			{
				$fuel['rate'] = "1";
				$fuel['con'] = "Bad";
				$fuel['msg'] = "";
			}
			elseif($data['peak_crp'] == 2)
			{
				$fuel['rate'] = "2";
				$fuel['con'] = "Bad-Okay";
				$fuel['msg'] = "";
			}
			elseif($data['peak_crp'] == 3)
			{
				$fuel['rate'] = "3";
				$fuel['con'] = "Okay";
				$fuel['msg'] = "";
			}
		}
		elseif($data['idl_crp'] == 2)
		{
			if($data['peak_crp'] == 1)
			{
				$fuel['rate'] = "1";
				$fuel['con'] = "Bad";
				$fuel['msg'] = "";
			}
			elseif($data['peak_crp'] == 2)
			{
				$fuel['rate'] = "3";
				$fuel['con'] = "Okay";
				$fuel['msg'] = "";
			}
			elseif($data['peak_crp'] == 3)
			{
				$fuel['rate'] = "4";
				$fuel['con'] = "Okay-Good";
				$fuel['msg'] = "";
			}
		}
		elseif($data['idl_crp'] == 3)
		{
			if($data['peak_crp'] == 1)
			{
				$fuel['rate'] = "2";
				$fuel['con'] = "Bad-Okay";
				$fuel['msg'] = "";
			}
			elseif($data['peak_crp'] == 2)
			{
				$fuel['rate'] = "3";
				$fuel['con'] = "Okay";
				$fuel['msg'] = "";
			}
			elseif($data['peak_crp'] == 3)
			{
				$fuel['rate'] = "5";
				$fuel['con'] = "Good";
				$fuel['msg'] = "";
			}
		}
		$fuel['msg'] = $data['crpm'];
		
		//backPre
		$backPre = array();
		if($data['ergsmd_ms'] == 1)
		{
			$backPre['rate'] = "1";
			$backPre['con'] = "Bad";
			$backPre['msg'] = "";
		}
		elseif($data['ergsmd_ms'] == 2)
		{
			$backPre['rate'] = "2";
			$backPre['con'] = "Bad–Okay";
			$backPre['msg'] = "";
		}
		elseif($data['ergsmd_ms'] == 3)
		{
			$backPre['rate'] = "3";
			$backPre['con'] = "Okay";
			$backPre['msg'] = "";
		}
		elseif($data['ergsmd_ms'] == 4)
		{
			$backPre['rate'] = "4";
			$backPre['con'] = "Okay-Good";
			$backPre['msg'] = "";
		}
		elseif($data['ergsmd_ms'] == 5)
		{
			$backPre['rate'] = "5";
			$backPre['con'] = "Good";
			$backPre['msg'] = "";
		}
		else 
		{
			$backPre['rate'] = "";
			$backPre['con'] = "";
			$backPre['msg'] = $data['ergsmd_ms'];
		}
		
		
		//combustionPressure
		$copPres = array();
		if($data['comp_pres'] == 1)
		{
			$copPres['rate'] = "1";
			$copPres['con'] = "Bad";
		}
		elseif($data['comp_pres'] == 2)
		{
			$copPres['rate'] = "2";
			$copPres['con'] = "Bad–Okay";
		}
		elseif($data['comp_pres'] == 3)
		{
			$copPres['rate'] = "3";
			$copPres['con'] = "Okay";
		}
		elseif($data['comp_pres'] == 4)
		{
			$copPres['rate'] = "4";
			$copPres['con'] = "Okay-Good";
		}
		elseif($data['comp_pres'] == 5)
		{
			$copPres['rate'] = "5";
			$copPres['con'] = "Good";
		}
		else
		{
			$copPres['msg'] = $data['comp_pres'];
		}
		
		//rpmOscillationAnomalyAnalysis
		$rcOcil = array();
		if($data['eng_block_ej'] == 1)
		{
			$rcOcil['rate'] = "1";
			$rcOcil['con'] = "Bad";
		}
		elseif($data['eng_block_ej'] == 2)
		{
			$rcOcil['rate'] = "2";
			$rcOcil['con'] = "Bad–Okay";
		}
		elseif($data['eng_block_ej'] == 3)
		{
			$rcOcil['rate'] = "3";
			$rcOcil['con'] = "Okay";
		}
		elseif($data['eng_block_ej'] == 4)
		{
			$rcOcil['rate'] = "4";
			$rcOcil['con'] = "Okay-Good";
		}
		elseif($data['eng_block_ej'] == 5)
		{
			$rcOcil['rate'] = "5";
			$rcOcil['con'] = "Good";
		}
		//falseOdometerReading
		$falOdMr = array();
		$falOdMr['rate'] = "";
		$falOdMr['con'] = "";
		$falOdMr['msg'] = $data['falsemeter'];
		
		//getVehSpeedSence
		$vehSpeed = array();
		$vehSpeed['rate'] = "";
		$vehSpeed['con'] = "";
		$vehSpeed['msg'] = ($data['vehspeed'] <= 0) ? "Vehicle Speed Sensor Failure possibility" : "Vehicle Speed Okay";
		
		// cool Temp
		$coolTemp = array();
		if($data['coolp'] == 1)
		{
			$coolTemp['rate'] = "1";
			$coolTemp['con'] = "Bad";
		}
		elseif($data['coolp'] == 2)
		{
			$coolTemp['rate'] = "2";
			$coolTemp['con'] = "Bad–Okay";
		}
		elseif($data['coolp'] == 3)
		{
			$coolTemp['rate'] = "3";
			$coolTemp['con'] = "Okay";
		}
		elseif($data['coolp'] == 4)
		{
			$coolTemp['rate'] = "4";
			$coolTemp['con'] = "Okay-Good";
		}
		elseif($data['coolp'] == 5)
		{
			$coolTemp['rate'] = "5";
			$coolTemp['con'] = "Good";
		}
		else
		{
			$coolTemp['msg'] = $data['coolp'];
		}
		
	//combustion pressure message
		if($copPres['rate'] == 5 || $copPres['rate'] == "")
		{
			$copPres['msg'] = "";
		}
		elseif($copPres['rate'] <= 4)
		{
			if((($turbo['rate']==5 || $air['rate']==5) && $fuel['rate'] ==5 && $rcOcil['rate']<=4) || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] <=0 )  || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] == 5)  || (($turbo['rate']==5 || $air['rate']==5)&& $rcOcil['rate']<=5 && $fuel['rate'] <=4 )  || (($turbo['rate']<=4 || $air['rate']<=4)&& $rcOcil['rate']<=5 && $fuel['rate'] <=4 ))
			{
				if($data['ocilv'] >= 1)
				{
					$copPres['msg'] = "It appears your engine's combustion pressures are not good. This is attributed to possibly the engine's compression pressure";
				}
				else 
				{
					$n = array();
					if($turbo['rate'] >= 1)
					{
						$n = array($turbo['rate'],$fuel['rate']);
					}
					else {
						$n = array($air['rate'],$fuel['rate']);
					}
					$nVal = min($n);
					
					if($nVal[0] == $nVal[1])
					{
						if($nVal <= 3)
						{	
							$copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your High Pressure Fuel System and/or Air System.";
						}
					}
					else
					{
						if($fuel['rate'] <= 0)
						{
							if($nVal[0] == 5)
							{	
								$copPres['msg'] = "It appears your engine's combustion pressures are not good. We could not check your High Pressure Fuel System. We have no access to it over the OBD. Please consider getting it checked.";
							}
							else 
							{
								$copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your Air System.";
							}
						}
						else 
						{
							if($nVal[0]>$nVal[1])
							{
								$copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your High Pressure Fuel System.";
							}
							else
							{
								$copPres['msg'] = "It appears your engines combustion pressures are not good. Please consider checking your Air System.";
							}
						}
					}
				}
			}
		}
		
		$arrayData['success'] = TRUE;
		
		$arrayData['batteryHealth'] = array('ratings'=>$batHel['rate'],'condition'=>$batHel['con'],'message'=>$batHel['msg']);
		
		$arrayData['turbochargerHealth'] = array('ratings'=>$turbo['rate'],'condition'=>$turbo['con'],'message'=>$turbo['msg']);
		
		$arrayData['airSystem'] = array('ratings'=>$air['rate'],'condition'=>$air['con'],'message'=>$air['msg']);
		
		$arrayData['fuelSystem'] = array('ratings'=>$fuel['rate'],'condition'=>$fuel['con'],'message'=>$fuel['msg']);
		
		$arrayData['exhaustSystem'] = array('ratings'=>$backPre['rate'],'condition'=>$backPre['con'],'message'=>$backPre['msg']);
		
		$arrayData['combustionPressure'] = array('ratings'=>$copPres['rate'],'condition'=>$copPres['con'],'message'=>$copPres['msg']);
		
		$arrayData['engineBlockInjectors'] = array('ratings'=>$rcOcil['rate'],'condition'=>$rcOcil['con'],'message'=>$rcOcil['msg']);
		
		$arrayData['falseOdometerReadingDetection'] = array('ratings'=>$falOdMr['rate'],'condition'=>$falOdMr['con'],'message'=>$falOdMr['msg']);
		
		$arrayData['coolantTemperature'] = array('ratings'=>$coolTemp['rate'],'condition'=>$coolTemp['con'],'message'=>$coolTemp['msg']);
		
		$arrayData['vehicleSpeedSensorMalfunction'] = array('ratings'=>$vehSpeed['rate'],'condition'=>$vehSpeed['con'],'message'=>$vehSpeed['msg']);
		
	}
	else
	{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1002";
		$arrayData['errorMessage'] = "Invalid Device ID!";
	}
}
else
{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1001";
		$arrayData['errorMessage'] = "Invalid Input!";
}
$resultVal = json_encode($arrayData);
echo $resultVal;
?>