<?php
include 'common.php';
header('Content-Type: application/json');
// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$result = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json     = $_REQUEST['data'];
	$obj      = json_decode($json,true);
	$apikey   = $obj['apikey'];
	$validKey = getAPIKey($apikey,$conn);		//CHeck whether apikey input key is available in DB
	if($validKey)
	{
		$deviceIDs         = getUnregDev($conn);
		$result['success'] = TRUE;
		$result['devID']   = $deviceIDs;
	}
	else
	{
		$result['success']      = FALSE;
		$result['errorCode']    = "1002";
		$result['errorMessage'] = "Access Denied!";
	}
}
else
{
	$result['success']      = FALSE;
	$result['errorCode']    = "1001";
	$result['errorMessage'] = "Invalid Input!";
}
$resultVal = json_encode($result);
echo $resultVal;
?>