<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);	
	$deviceID = $obj['devID'];
	$devInfo = getdevice($deviceID,$conn);
    //print_r($devInfo);die;
	if(!empty($devInfo))
	{
		$yVal = 0;
		if(trim($obj['type']) == 'drive')
		{
			$reqFor = $obj['driveno'];
			$durTr = getStatsValueByDrive($conn,$deviceID,$reqFor,'dur','sum');
			$distTr = getStatsValueByDrive($conn,$deviceID,$reqFor,'dist');
			$avSpeed = getStatsValueByDrive($conn,$deviceID,$reqFor,'avg_speed','avg');
			$maxSpeed = getStatsValueByDrive($conn,$deviceID,$reqFor,'max_sp','max');
			$totCon = getStatsValueByDrive($conn,$deviceID,$reqFor,'ful_c','sum');
			$totMil = getStatsValueByDrive($conn,$deviceID,$reqFor,'avg_ml','avg'); // was avg
			$maxTor = getStatsValueByDrive($conn,$deviceID,$reqFor,'max_tor','max');
			//$overRun = $devAct -> overRun(1);
			$fOver = getStatsValueByDrive($conn,$deviceID,$reqFor,'ful_ovr','sum');
			$idT = getStatsValueByDrive($conn,$deviceID,$reqFor,'idt','sum');
			$ovT = getStatsValueByDrive($conn,$deviceID,$reqFor,'dur_ovr','sum');
			//EC added extra parameter for dist in overrun
			$ovD = getStatsValueByDrive($conn,$deviceID,$reqFor,'dist_ovr','sum');
			$hacc = getStatsValueByDrive($conn,$deviceID,$reqFor,'hacc','sum');
			$hdcc = getStatsValueByDrive($conn,$deviceID,$reqFor,'hdcc','sum');
			$ovrspd = getStatsValueByDrive($conn,$deviceID,$reqFor,'ovrspd','sum');
			$vehstall = getStatsValueByDrive($conn,$deviceID,$reqFor,'vehstall','sum');

			$PeakMAF = getStatsValueByDrive($conn,$deviceID,$reqFor,'max_maf','max');
			$PeakTorque  = $maxTor;//$devAct -> driveValue($reqFor,'max_maf');
			$LowestBatteryVoltage = getStatsValueByDrive($conn,$deviceID,$reqFor,'min_batv','min');
			$PeakRailPressure = getStatsValueByDrive($conn,$deviceID,$reqFor,'max_railp','max');
			$PeakMAP = getStatsValueByDrive($conn,$deviceID,$reqFor,'max_map','max');
			$yVal = 1;
		}
		elseif(trim($obj['type']) == 'date')
		{
			//$dtF = explode(':',$obj['startdate']);
			//$dtT = explode(':',$obj['endtdate']);
			$dtFA = strtotime(str_replace(':', '-', $obj['startdate']));
			$dtTA = strtotime(str_replace(':', '-', $obj['enddate']));
			$fr = date('Y-m-d',$dtFA);
			$to = date('Y-m-d',$dtTA);
			$arrayData['success'] = TRUE;
			$arrayData['type'] = trim($obj['type']);
			$arrayData['data'] = array();
			//echo "$fr,$to";
			$dtI = 0;
			while($dtFA <= $dtTA)
			{
				$fr = date('Y-m-d 00:00:00',$dtFA);
				$to = date('Y-m-d 23:59:59',$dtFA);  // Pick stats for one day and increment at the end for next day
				$mADt = date('d-m-Y',$dtFA);
				$arrayData['data'][$dtI] = array();
				$durTr = getStatsValueByDate($conn,$deviceID,$fr,$to,'dur');
				$distTr = getStatsValueByDate($conn,$deviceID,$fr,$to,'dist');
				$avSpeed = getStatsValueByDate($conn,$deviceID,$fr,$to,'avg_speed','avg');
				$maxSpeed = getStatsValueByDate($conn,$deviceID,$fr,$to,'max_sp','max');
				$totCon = getStatsValueByDate($conn,$deviceID,$fr,$to,'ful_c');
				$totMil = getStatsValueByDate($conn,$deviceID,$fr,$to,'avg_ml','avg'); // was avg
				$maxTor = getStatsValueByDate($conn,$deviceID,$fr,$to,'max_tor','max');
				//$overRun = $devAct -> overRun(1);
				$fOver = getStatsValueByDate($conn,$deviceID,$fr,$to,'ful_ovr');
				$idT = getStatsValueByDate($conn,$deviceID,$fr,$to,'idt');
				$ovT = getStatsValueByDate($conn,$deviceID,$fr,$to,'dur_ovr');

				//EC added extra parameter for dist in overrun
				$ovD = getStatsValueByDate($conn,$deviceID,$fr,$to,'dist_ovr');

				$PeakMAF = getStatsValueByDate($conn,$deviceID,$fr,$to,'max_maf','max');
				$PeakTorque  = $maxTor;//$devAct -> driveValuedt($fr,$to,'max_maf');
				$LowestBatteryVoltage = getStatsValueByDate($conn,$deviceID,$fr,$to,'min_batv','min');
				$PeakRailPressure = getStatsValueByDate($conn,$deviceID,$fr,$to,'max_railp','max');
				$PeakMAP = getStatsValueByDate($conn,$deviceID,$fr,$to,'max_map','max');

				$arrayData['data'][$dtI]['date'] = $mADt;
				$arrayData['data'][$dtI]['drive_duration'] = round($durTr,0);
				$arrayData['data'][$dtI]['drive_dist'] = round($distTr,4);
				$arrayData['data'][$dtI]['avg_speed'] = round($avSpeed,2);
				$arrayData['data'][$dtI]['max_speed'] = round($maxSpeed,0);
				$arrayData['data'][$dtI]['fuel_consumed'] = round($totCon,4);
				$arrayData['data'][$dtI]['mileage'] = round($totMil,4);
				$arrayData['data'][$dtI]['max_tor'] = round($maxTor,2);

				//EC added extra parameter for dist in overrun
				//$arrayData['data'][$dtI]['fuel_save_ovr'] = $fOver;
				$arrayData['data'][$dtI]['dist_ovr'] = round($ovD,2);
				$arrayData['data'][$dtI]['fuel_save_ovr'] = round($fOver,3);
				$arrayData['data'][$dtI]['idle_time'] = round($idT,0);
				$arrayData['data'][$dtI]['time_ovr'] = round($ovT,0);

				$arrayData['data'][$dtI]['peak_maf'] = round($PeakMAF,2);
				$arrayData['data'][$dtI]['peak_tor'] = round($PeakTorque,2);
				$arrayData['data'][$dtI]['low_bat_vol'] = $LowestBatteryVoltage;
				$arrayData['data'][$dtI]['peak_rail_p'] = round($PeakRailPressure,0);
				$arrayData['data'][$dtI]['peak_map'] = round($PeakMAP,0);
				$dtFA += (60*60*24);
				$dtI++;
			}
			$yVal = 1;
		}
		if($yVal == 0)
		{
			$arrayData['success'] = FALSE;
			$arrayData['errorCode'] = "1003";
			$arrayData['errorMessage'] = "Invalid Type!";
		}
		else
		{
			if(trim($obj['type']) == 'drive')
			{
				$arrayData['success'] = TRUE;
				$arrayData['type'] = trim($obj['type']);
				$arrayData['drive_duration'] = $durTr;
				$arrayData['drive_dist'] = round($distTr,4);
				$arrayData['avg_speed'] = round($avSpeed,2);
				$arrayData['max_speed'] = $maxSpeed;
				$arrayData['fuel_consumed'] = round($totCon,4);
				$arrayData['mileage'] = round($totMil,4);
				$arrayData['max_tor'] = round($maxTor,2);

				//EC added extra parameter for dist in overrun
				//$arrayData['fuel_save_ovr'] = $fOver;
				$arrayData['dist_ovr'] = round($ovD,2);
                $arrayData['fuel_save_ovr'] = round($fOver,3);
				$arrayData['idle_time'] = $idT;
				$arrayData['time_ovr'] = $ovT;
				$arrayData['peak_maf'] = round($PeakMAF,2);
				$arrayData['peak_tor'] = round($PeakTorque,2);
				$arrayData['low_bat_vol'] = $LowestBatteryVoltage;
				$arrayData['peak_rail_p'] = $PeakRailPressure;
				$arrayData['peak_map'] = $PeakMAP;
                //print_r($arrayData);die;
			}
		}
	}
	else
	{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1001";
		$arrayData['errorMessage'] = "Invalid Device ID!";
	}
}
else
{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1002";
		$arrayData['errorMessage'] = "Invalid Input!";
}
$resultVal = json_encode($arrayData);
echo $resultVal;
?>

