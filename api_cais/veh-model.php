<?php
header('Content-Type: application/json');
include 'common.php';

// Call Fuction to get DB Handle. This Function is in common.php
$conn = getDbHandle();
if($conn == null){
  http_response_code(400);
  exit();
}

$arrayData = array();
if(isset($_REQUEST['data']) && trim($_REQUEST['data']) != "")
{
	$json = $_REQUEST['data'];
	$obj = json_decode($json,true);	
	$mfd = $obj['mfd'];
	// print_r($mfd);die;
	// $sql = "select * from vehicle_setting where id = '1'";
	$res = getVehDetail($mfd,$conn);
	// print_r($res);die;
	$mfdList = explode(';',$res['manufacturer']);
	// print_r($mfdList);die;
	$engCapList = explode(';',$res['engine_capacity']);
	if(in_array($mfd, $mfdList))
	{
		$arrayData['success'] = TRUE;	
		$keyMfd = array_search($mfd, $mfdList);	
		// $sql = "select * from vehinfo where mfd = '$keyMfd'";
		$resM = getVehInfo($keyMfd,$conn);
		$varList = array();
		$modList = array();
		$fuel = array('','Petrol','Diesel');
		if(count($resM) >= 1)
		{
			//$arrayData['varients'] = explode(';',$resM[0]['var']);
			$arrayData['model'] = explode(';',$resM['model']);
		}
	}
	else
	{
		$arrayData['success'] = FALSE;
		$arrayData['errorCode'] = "1002";			
		$arrayData['errorMessage'] = "Manufacturer is not in the list!";
	}
}
else
{
	$arrayData['success'] = FALSE;
	$arrayData['errorCode'] = "1001";			
	$arrayData['errorMessage'] = "Invalid Input!";		
}
$resultVal = json_encode($arrayData);	
echo $resultVal;	

?>