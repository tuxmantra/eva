<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Helper;
use App\Model\Common;
use App\Model\Fetch;

class Drivesd extends Controller
{
   public function index(){
        $user                = session('userdata');
        $data['user_type']   = $user['utype'];
        $deviceList          = Fetch::getDeviceListByUserType($user['uid'],$user['ucategory']);
        $did                 = $deviceList[0]->device_id;
        $date                = date('Y-m-d');
        $driveDates          = Fetch::getDriveDatesByDeviceIdEvent($did);
        $driveLatLong        = Fetch::getDriveLatLongByIdEvent($did,$date);

        $string              = '';
        $start               = "";
        if(!empty($driveLatLong)){
            $i=1;
            foreach($driveLatLong as $key => $list){

                if($key == 0){
                  $start.=  "{lat: $list->lat, lng: $list->lon}";
                }

                $string.=  "{lat: $list->lat, lng: $list->lon},";
                $i++;
            }
        }
        
        $data_array          = "";
        foreach($driveDates as $sdt){
           $data_array.= "'".date('Y-n-d',strtotime($sdt->date))."',"; 
        }

        $data = array(
          'title'            => 'Drive',
          'device_list'      => $deviceList,
          'user_category'    => $user['ucategory'],        
          'did'              => $did,
          'drive_dates'      => rtrim($data_array,','),
          'drive_latlon'     => rtrim($string,','),
          'start_point'      => $start
        );
        $data['drivesd']  = '1';
        $data['menu']     = Fetch::getUserTypeByIdMinu(session('userdata')['utype']);
      return view('drivesd/index')->with($data);
    }

  public function getSdDriveLoad(Request $request){

        $did    = $request->post('did');
        $date   = $request->post('date');
        $date   = date('Y-m-d',strtotime($date));
        $driveLatLong        = Fetch::getDriveLatLongByIdEvent($did,$date);
        $count_lat_long      = count($driveLatLong);
        
        $skip_lat_long       = round($count_lat_long/90);  

        $string              = '';
        $start               = "";
        $last                = "";
        if(!empty($driveLatLong)){
            $i  = 1;
            $ct = 0;
            foreach($driveLatLong as $key => $list){

                if($key == 0){
                  //$start.=  "{lat: $list->lat, lng: $list->lon}";
                  $start   = "{lat: -35.2784167, lng: 149.1294692}";
                }
                if($key == count($driveLatLong)-1){
                   $last = "|$list->lat,$list->lon";
                }
                if($ct == $skip_lat_long){
                      $string.=  "$list->lat,$list->lon|";
                      $ct = 0;
                }
                $i++;
                $ct++;
            }
        }else{
          $start   = "{lat: -35.2784167, lng: 149.1294692}";
        }
        $string = rtrim($string,'|');
        $string = $string.$last;

        // call google roads api to get exact road lat longs

        $handle = curl_init();
/*        $url = "https://roads.googleapis.com/v1/snapToRoads?path=$string&interpolate=true&key=AIzaSyABmH85QxA6UsfF96kz17AMV5xDOESzPIE
";*/
        $url = 'http://eva.enginecal.com/api/test.php';
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);  
        $output = curl_exec($handle);
        curl_close($handle);
        
        $output = '{
  "snappedPoints": [
    {
      "location": {
        "latitude": -35.2784167,
        "longitude": 149.1294692
      },
      "originalIndex": 0,
      "placeId": "ChIJoR7CemhNFmsRQB9QbW7qABM"
    },
    {
      "location": {
        "latitude": -35.280321693840129,
        "longitude": 149.12908274880189
      },
      "originalIndex": 1,
      "placeId": "ChIJiy6YT2hNFmsRkHZAbW7qABM"
    },
    {
      "location": {
        "latitude": -35.2803415,
        "longitude": 149.1290788
      },
      "placeId": "ChIJiy6YT2hNFmsRkHZAbW7qABM"
    },
    {
      "location": {
        "latitude": -35.2803415,
        "longitude": 149.1290788
      },
      "placeId": "ChIJI2FUTGhNFmsRcHpAbW7qABM"
    },
    {
      "location": {
        "latitude": -35.280451499999991,
        "longitude": 149.1290784
      },
      "placeId": "ChIJI2FUTGhNFmsRcHpAbW7qABM"
    },
    {
      "location": {
        "latitude": -35.2805167,
        "longitude": 149.1290879
      },
      "placeId": "ChIJI2FUTGhNFmsRcHpAbW7qABM"
    },
    {
      "location": {
        "latitude": -35.2805901,
        "longitude": 149.1291104
      },
      "placeId": "ChIJI2FUTGhNFmsRcHpAbW7qABM"
    },
    {
      "location": {
        "latitude": -35.2805901,
        "longitude": 149.1291104
      },
      "placeId": "ChIJW9R7smlNFmsRMH1AbW7qABM"
    },
    {
      "location": {
        "latitude": -35.280734599999995,
        "longitude": 149.1291517
      },
      "placeId": "ChIJW9R7smlNFmsRMH1AbW7qABM"
    },
    {
      "location": {
        "latitude": -35.2807852,
        "longitude": 149.1291716
      },
      "placeId": "ChIJW9R7smlNFmsRMH1AbW7qABM"
    },
    {
      "location": {
        "latitude": -35.2808499,
        "longitude": 149.1292099
      },
      "placeId": "ChIJW9R7smlNFmsRMH1AbW7qABM"
    },
    {
      "location": {
        "latitude": -35.280923099999995,
        "longitude": 149.129278
      },
      "placeId": "ChIJW9R7smlNFmsRMH1AbW7qABM"
    },
    {
      "location": {
        "latitude": -35.280960897210818,
        "longitude": 149.1293250692261
      },
      "originalIndex": 2,
      "placeId": "ChIJW9R7smlNFmsRMH1AbW7qABM"
    }
  ]
}';

        $string   = '';
        $latlongs = json_decode($output);
        foreach ($latlongs as $key => $value) {
            foreach ($value as $bey => $val) {
               $string.=  "{lat: ".$val->location->latitude.", lng: ".$val->location->longitude."},";
            }
        }
        $string = rtrim($string,',');
        $data   = array(
          'did'              => $did,
          'drive_latlon'     => $string,
          'start_point'      => $start
        );

        $result = view('drivesd/load')->with($data);
        return $result;
  }
}
